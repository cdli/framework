import unittest
import os

# These are CSS Lint tests for framework.
class RunPhpLintTests(unittest.TestCase):

    def test_php_lint_for_src(self):
        self.maxDiff = None
        csslint_exce = os.system('php-cs-fixer fix --config=.php-cs-fixer.dist.php --dry-run --diff')
        if(csslint_exce!=0):
            print(csslint_exce)
        self.assertEqual(csslint_exce, 0)
        
if __name__ == '__main__':
    unittest.main()