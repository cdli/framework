# User's Search Documentation

## Simple Search

Simple Search can perform a generalised form of search under different search domains as given below:
*  Free Search
*  Publications
*  Collections
*  Proveniences
*  Periods
*  Inscriptions
*  Identification Numbers

Each domain performs a search in specific attributes related to that domain in the CDLI database.

* ### Free search 
    It is a generic search field which can return results for all the domains along with additional fields given below.
    - Dates
    - Credits
    - Projects
    - Archives

* ### Publications
    - Designation Exact Reference
    - Designation
    - Bibtex key
    - Year
    - Publisher
    - School
    - Series
    - Title
    - Book Title
    - Chapter
    - Journal
    - Editors
    - Authors

* ### Collections
    - Collections

* ### Proveniences
    - Proveniences
    - Written in
    - Region

* ### Periods
    - Periods

* ### Inscriptions
    - atf
    - Translation
    - Transliteration

* ### Identification numbers
    - Publication Designation
    - Artifact Designation
    - Excavation number
    - Designation number
    - Bibtex key
    - Composite number
    - Seal number
    - Museum number
    - Artifact Id

    &emsp; **Features of Identification numbers search field:**
    1. Fuzzy Id's Search:\
    &emsp; Search query `A747` will return result for `A*747` *( \* represents whitespace or digits)*\
    &emsp; Eg. `A 747`, `A00747`, `A12747`
    2. Multi Id's search:\
    &emsp; To search list of ID's, mention each identification numbers seperated by commas (,)\
    &emsp; Eg. `P122,P123,P111223,P111441,P112588,P122521,P123456,P212155`

**Note:** Multiple search fields (upto 4) can be added to perform search with different operators *(OR, AND)* by clicking on `Add search field` button on Home page.

## Advanced Search

Advanced Search can be used to perform a **specific** search under different domains.\
Eg. `CDLI` in **Authors** field AND `europe` in **Collections** field.

**Transilteration Permutation** is an advanced search field which returns all sign readings per sign.\
Eg. `szesz4` in **Transilteration Permutation** will return results for *`szesz4`* or *`EREN` (sign reading)*.

*Note: All other search features of each search field in **Advanced Search** are identical to **Simple Search** except for **Transilteration Permutation**.*
