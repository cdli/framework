#!/bin/bash

# set this script to run in cron daily
### crontab -e
###  55 23 * * *  bash /path/to/script/orchestrator.sh

# feed in credentials and path specifics
cd ${ORCHESTRATOR_PATH}
source ./config.txt

# dump all databases
### cdli_db
docker exec cdlidev_mariadb_1 /usr/bin/mysqldump -u ${MARIADB_USER} --password="${MARIADB_PASS}" cdli_db | gzip -9 > ${DATA_BASE_PATH}cdli_db.sql.gz

# dump full and public cat
# will have to make a view first

# dump full and public atf
### atf
#### full
#docker exec cdlidev_mariadb_1 /usr/bin/mysql -u ${MARIADB_USER} --password="${MARIADB_PASS}" cdli_db "SELECT concat_ws(atf,'\n') FROM inscriptions WHERE is_latest=1"  > ${DATA_BASE_PATH}cdliatf_full.atf
#### public
#docker exec cdlidev_mariadb_1 /usr/bin/mysql -u ${MARIADB_USER} --password="${MARIADB_PASS}" cdli_db "SELECT concat_ws(atf,'\n') FROM inscriptions join artifacts on artifacts.id  = inscriptions.artifact_id WHERE is_latest=1 AND artifacts.is_atf_public = 1"  > ${DATA_BASE_PATH}cdliatf_public.atf


# conc version


# dump annotations in all formats

# cdli-conll

# conll-u

#

# dump words and signs per period

# dump entity types
