// Set default margins, width and height
var margin = {left: 100, top: 100, right: 100, bottom: 100};
var width = Math.min(800, window.innerWidth - 10) - margin.left - margin.right;
var height = Math.min(800, window.innerHeight - margin.top - margin.bottom - 20);

// Set the attributes of the Radar Chart
var controls = {
    w: width,			    //Width of the circle
    h: height,				//Height of the circle
    margin: margin,         //The margins of the SVG
    translateX: (width/2 + margin.left),  //Translate in X-direction
    translateY: (height/2 + margin.top),   //Translate in Y-direction
    levels: 5,				//How many levels or inner circles should there be drawn
    maxValue: 0.5,          //What is the value that the biggest circle will represent (Random value, calculated later)
	labelFactor: 2,       //How much farther than the radius of the outer circle should the labels be placed
	wrapWidth: 60, 		    //The number of pixels after which a label needs to be given a new line
	opacityArea: 0.35, 	    //The opacity of the area of the blob
	dotRadius: 4, 			//The size of the colored circles of each blog
	opacityCircles: 0.1, 	//The opacity of the circles of each blob
	strokeWidth: 2, 		//The width of the stroke around each blob
	roundStrokes: true,	    //If true the area and stroke will follow a round path (cardinal-closed)
	color: d3.scaleOrdinal(d3.schemeCategory10)	//Color function
};

// Add SVG to the Radar Chart container
var svg = d3.select("#radar-chart")
    .append("svg")
        .attr("width",  controls.w + controls.margin.left + controls.margin.right)
        .attr("height", controls.h + controls.margin.top + controls.margin.bottom)

// Append Group element to the SVG and translate it to center 		
var g = svg.append("g")
    .attr("transform", "translate(" + controls.translateX + "," + controls.translateY + ")");


// Calculate the maximum value required to be represented in the Radar Chart
// If the supplied maxValue is smaller than the actual one, replace it by the max in the data
var maxValue = Math.max(controls.maxValue, d3.max(data, function(i) {
    return d3.max(i.map(function(o) {
        return o.value;
    }))
}));

// Get names of all the surrounding axis
var allAxis = (data[0].map(function(i, j) {
    return i.axis
}));

var total = allAxis.length;					         // The number of different axes
var radius = Math.min(controls.w/2, controls.h/2); 	 // Radius of the outermost circle
var angleSlice = Math.PI * 2 / total;		         // The width in radians of each "slice"
	
// Create Scale for the radius
var rScale = d3.scaleLog()
    .domain([1, maxValue])
    .range([0, radius])
    .base(2);


// Add Legend
// Legend Options are taken from backend directly
// Create the title for the legend
var text = g.append("text")
	.attr("class", "title")
	.attr("transform", "translate(" + (-controls.translateX + 90) + "," + (-controls.translateY) + ")") 
	.attr("x", width - 70)
	.attr("y", 10)
	.attr("font-size", "12px")
	.attr("fill", "#404040")
	.text("Regions")
        .style('font-weight', 'bold');
		
// Initiate Legend	
var legend = g.append("g")
	.attr("class", "legend")
	.attr("height", 100)
	.attr("width", 200)
    .attr("transform", "translate(" + (-controls.translateX + 85) + "," + (-controls.translateY + 25) + ")");
	
// Create colour squares to label legend options
legend.selectAll('rect')
    .data(legendOptions)
    .enter()
    .append("rect")
        .attr("x", width - 65)
        .attr("y", function(d, i) { return i * 20; })
        .attr("width", 10)
        .attr("height", 10)
        .style("fill", function(d, i) { return controls.color(i); });
	
// Add text next to squares
legend.selectAll('text')
    .data(legendOptions)
	.enter()
	.append("text")
        .attr("x", width - 52)
        .attr("y", function(d, i) { return i * 20 + 9; })
        .attr("font-size", "11px")
        .attr("fill", "#737373")
        .text(function(d) { return d; });	


// Glow Filter
// Create Filter for the outside glow
var filter = g.append('defs')
    .append('filter')
    .attr('id', 'glow');

var feGaussianBlur = filter.append('feGaussianBlur')
    .attr('stdDeviation', '2.5')
    .attr('result', 'coloredBlur');

var feMerge = filter.append('feMerge');

var feMergeNode_1 = feMerge.append('feMergeNode')
    .attr('in', 'coloredBlur');

var feMergeNode_2 = feMerge.append('feMergeNode')
    .attr('in', 'SourceGraphic');


// Draw the circular grid
// Create Wrapper for the grid & axes
var axisGrid = g.append("g")
    .attr("class", "axisWrapper");
	
// Draw the background circles
axisGrid.selectAll(".levels")
    .data(d3.range(1,(controls.levels+1)).reverse())
	.enter()
	.append("circle")
        .attr("class", "gridCircle")
        .attr("r", function(d, i) { return radius/controls.levels * d; })
        .style("fill", "#CDCDCD")
        .style("stroke", "#CDCDCD")
        .style("fill-opacity", controls.opacityCircles)
        .style("filter" , "url(#glow)");

// Add Text indicating at what number each level is present
axisGrid.selectAll(".axisLabel")
    .data(d3.range(1,(controls.levels+1)).reverse())
	.enter()
    .append("text")
        .attr("class", "axisLabel")
        .attr("x", 4)
        .attr("y", function(d) { return -d * radius/controls.levels; })
        .attr("dy", "0.4em")
        .style("font-size", "10px")
        .attr("fill", "#737373")
        .text(function(d, i) { return Math.round(rScale.invert(radius * d/controls.levels)); });


// Draw the axes
// Create straight lines radiating outward from the center
var axis = axisGrid.selectAll(".axis")
    .data(allAxis)
	.enter()
	.append("g")
	   .attr("class", "axis");

// Append lines
axis.append("line")
	.attr("x1", 0)
	.attr("y1", 0)
	.attr("x2", function(d, i){ return rScale(maxValue*1.1) * Math.cos(angleSlice * i - Math.PI/2); })
	.attr("y2", function(d, i){ return rScale(maxValue*1.1) * Math.sin(angleSlice * i - Math.PI/2); })
	.attr("class", "line")
	.style("stroke", "white")
	.style("stroke-width", "2px");

// Append the labels at each axis
axis.append("text")
	.attr("class", "legend")
	.style("font-size", "11px")
	.attr("text-anchor", "middle")
	.attr("dy", "0.35em")
	.attr("x", function(d, i) { 
        return rScale(maxValue * controls.labelFactor) * Math.cos(angleSlice * i - Math.PI/2); 
    })
    .attr("y", function(d, i) { 
        return rScale(maxValue * controls.labelFactor) * Math.sin(angleSlice * i - Math.PI/2); 
    })
    .text(function(d) { 
        return d; 
    })
	.call(wrap, controls.wrapWidth);


// Draw the radar chart blobs
// Define radial line function
var radarLine = d3.lineRadial()
    .curve(d3.curveBasisClosed)
	.radius(function(d) { return rScale(d.value); })
	.angle(function(d,i) {	return i * angleSlice; });
		
if (controls.roundStrokes) {
	radarLine.curve(d3.curveCardinalClosed);
}
				
// Create a wrapper for the blobs	
var blobWrapper = g.selectAll(".radarWrapper")
	.data(data)
	.enter()
    .append("g")
	   .attr("class", "radarWrapper");
			
// Append backgrounds	
blobWrapper.append("path")
	.attr("class", "radarArea")
	.attr("d", function(d, i) { return radarLine(d); })
	.style("fill", function(d, i) { return controls.color(i); })
	.style("fill-opacity", controls.opacityArea)
	.on('mouseover', function (d, i) {
		// Dim all blobs
        d3.selectAll(".radarArea")
            .transition()
                .duration(200)
            .style("fill-opacity", 0.1); 
        
        // Bring back the hovered over blob
		d3.select(this)
            .transition()
                .duration(200)
            .style("fill-opacity", 0.7);	
    })
	.on('mouseout', function() {
        // Bring back all blobs
		d3.selectAll(".radarArea")
            .transition()
                .duration(200)
            .style("fill-opacity", controls.opacityArea);
    });
		
// Create the outlines	
blobWrapper.append("path")
	.attr("class", "radarStroke")
	.attr("d", function(d, i) { return radarLine(d); })
	.style("stroke-width", controls.strokeWidth + "px")
	.style("stroke", function(d, i) { return controls.color(i); })
	.style("fill", "none")
	.style("filter", "url(#glow)");		
	
// Append the circles
blobWrapper.selectAll(".radarCircle")
	.data(function(d, i) { return d; })
	.enter()
    .append("circle")
		.attr("class", "radarCircle")
		.attr("r", controls.dotRadius)
		.attr("cx", function(d, i) { return rScale(d.value) * Math.cos(angleSlice * i - Math.PI/2); })
		.attr("cy", function(d, i) { return rScale(d.value) * Math.sin(angleSlice * i - Math.PI/2); })
		.style("fill", function(d, i, j) { return controls.color(j); })
		.style("fill-opacity", 0.8);


// Append invisible circles for tooltip
// Create Wrapper for invisible circles on top
var blobCircleWrapper = g.selectAll(".radarCircleWrapper")
	.data(data)
	.enter()
    .append("g")
		.attr("class", "radarCircleWrapper");
		
// Append a set of invisible circles on top for the mouseover pop-up
blobCircleWrapper.selectAll(".radarInvisibleCircle")
	.data(function(d, i) { return d; })
	.enter()
    .append("circle")
		.attr("class", "radarInvisibleCircle")
		.attr("r", controls.dotRadius * 1.5)
		.attr("cx", function(d, i){ return rScale(d.value) * Math.cos(angleSlice * i - Math.PI/2); })
		.attr("cy", function(d, i){ return rScale(d.value) * Math.sin(angleSlice * i - Math.PI/2); })
		.style("fill", "none")
		.style("pointer-events", "all")
		.on("mouseover", function(d, i) {
			newX = parseFloat(d3.select(this).attr('cx')) - 10;
			newY = parseFloat(d3.select(this).attr('cy')) - 10;
					
			tooltip
				.attr('x', newX)
				.attr('y', newY)
				.text(d.value)
				.transition()
                    .duration(200)
				.style('opacity', 1);
		})
		.on("mouseout", function(){
			tooltip
                .transition()
                    .duration(200)
				.style("opacity", 0);
		});

// Set up the small tooltip to display when circle is hovered
var tooltip = g.append("text")
	.attr("class", "tooltip")
	.style("opacity", 0);
	

// Helper Function
// Wraps SVG text	
function wrap(text, width) {
    text.each(function() {
        var text = d3.select(this);
		var words = text.text().split(/\s+/).reverse();
        var word;
        var line = [];
        var lineNumber = 0;
        var lineHeight = 1.4; // in em
        var y = text.attr("y");
        var x = text.attr("x");
        var dy = parseFloat(text.attr("dy"));
        var tspan = text.text(null)
            .append("tspan")
                .attr("x", x)
                .attr("y", y)
                .attr("dy", dy + "em");
			
		while (word = words.pop()) {
            line.push(word);
            tspan.text(line.join(" "));
            
            if (tspan.node().getComputedTextLength() > width) {
                line.pop();
                tspan.text(line.join(" "));
                line = [word];
                tspan = text.append("tspan")
                    .attr("x", x)
                    .attr("y", y)
                    .attr("dy", ++lineNumber * lineHeight + dy + "em")
                    .text(word);
            }
        }
    });
}	