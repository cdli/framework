$(function () {
  var prefixesByUri = Object.keys(prefixes).map(function (prefix) {
    return [prefix, prefixes[prefix]]
  }).sort(function (a, b) {
    return b[1].length - a[1].length
  })

  function displaySparqlUri (value) {
    var prefix = prefixesByUri.find(function (prefix) {
      return value.startsWith(prefix[1])
    })
    return prefix ? prefix[0] + ':' + value.slice(prefix[1].length) : value
  }

  function displaySparqlValue (value) {
    if (!value) {
      return document.createElement('span')
    }

    if (value.type === 'uri') {
      var span = document.createElement('a')
      span.setAttribute('href', value.value)
      span.textContent = displaySparqlUri(value.value)
    } else if (value.type == 'bnode') {
      var span = document.createElement('code')
      span.textContent = value.value
    } else {
      var span = document.createElement('span')
      span.textContent = JSON.stringify(value.value)

      if (value['xml:lang']) {
        span.append('@' + value['xml:lang'])
      }

      if (value.datatype) {
        span.append('^^', displaySparqlValue({ type: 'uri', value: value.datatype }))
      }
    }

    return span
  }

  function displayResultsTable (results) {
    var table = d3.select('#output')
      .append('div')
      .attr('class', 'table-responsive')
      .append('table')

    table
      .attr('class', 'table-bootstrap')
      .append('thead')
      .append('tr')
        .selectAll('th')
        .data(results.head.vars)
        .join('th')
          .text(function (d) { return d })

    var cells = table
      .append('tbody')
        .selectAll('tr')
        .data(results.results.bindings)
        .join('tr')
          .selectAll('td')
          .data(function (d) { return results.head.vars.map(function (k) { return d[k] }) })
          .join('td')
          .append(displaySparqlValue)
  }

  function displayResultsGraphUri (uri) {
    uri = uri.replace('http://qudt.org/vocab/unit#', 'http://qudt.org/vocab/unit/')
    uri = displaySparqlUri(uri)
    uri = uri
      .replace('http://collection.britishmuseum.org/id/', '')
      .replace('http://erlangen-crm.org/current/', 'ecrm:')
      .replace(/^(e?crm:.+\d+i?).+$/, '$1')
    return uri
  }

  function displayResultsGraph (results) {
    // Shape data into graph
    const nodeColumns = results.head.vars.filter(column => !column.endsWith('Label'))

    const nodes = []
    const rdfNodes = {}
    const edges = []

    for (const edge of results.results.bindings) {
      let source

      for (const column of nodeColumns) {
        if (!edge[column]) {
          continue
        }

        const nodeId = edge[column].type === 'uri' || edge[column].type === 'bnode' ? edge[column].value : null
        const node = {
          type: edge[column].type,
          value: edge[column].value
        }

        if (edge[column + 'Label']) {
          node.label = edge[column + 'Label'].value
        }

        if (nodeId && rdfNodes[nodeId]) {
          Object.assign(node, rdfNodes[nodeId])
          nodes[rdfNodes[nodeId].index] = rdfNodes[nodeId] = node
        } else {
          node.index = nodes.length
          node.connections = 0
          rdfNodes[nodeId] = node
          nodes.push(node)
        }

        if (source !== undefined) {
          edges.push({
            source,
            target: node.index,
            type: edge.edgeLabel && edge.edgeLabel.type,
            value: edge.edgeLabel && edge.edgeLabel.value
          })
          nodes[source].connections++
          nodes[node.index].connections++
        } else {
          source = node.index
        }
      }
    }

    // Set up graph
    const alphaMin = 0.01 // 0.001
    const alphaIterations = 1000 // 300

    const simulation = d3.forceSimulation(nodes)
      .alphaMin(alphaMin)
      .alphaDecay(1 - Math.pow(alphaMin, 1 / alphaIterations))
      .force('charge', d3.forceManyBody().distanceMax(500).strength(function (d) {
        if (d.connections === 0) {
          return 50
        } else if (d.connections === 1) {
          return -300
        } else {
          return -50
        }
      }))
      .force('link', d3.forceLink(edges).strength(function (d) {
        return 0.1 / Math.min(d.source.connections, d.target.connections)
      }))
      .force('center', d3.forceCenter())

    // Set up interactivity
    var drag = d3.drag()
      .on('start', function (d) {
        if (!d3.event.active) { simulation.alphaTarget(0.3).restart() }
        d.fx = d3.event.x
        d.fy = d3.event.y
        d3.select(this).raise()
      })
      .on('drag', function (d) {
        d.fx = d3.event.x
        d.fy = d3.event.y
      })
      .on('end', function (d) {
        if (!d3.event.active) { simulation.alphaTarget(0) }
        d.fx = null
        d.fy = null
      })

    var zoom = d3.zoom()
      .on('zoom', function () {
        d3.selectAll('svg > g')
          .attr('transform', d3.event.transform)
      })

    // Render graph
    var width = $('#output').width()
    var height = 2 * width / 3

    var svg = d3.select('#output')
      .append('svg')
      .attr('class', 'bg-white border')
      .attr('width', width)
      .attr('height', height)
      .attr('viewBox', [-width / 2, -height / 2, width, height])
      .call(zoom)

    svg.append('defs')
      .append('marker')
      .attr('id', 'triangle')
      .attr('viewBox', '0 0 10 10')
      .attr('refX', 10)
      .attr('refY', 5)
      .attr('markerWidth', 12)
      .attr('markerHeight', 12)
      .attr('orient', 'auto-start-reverse')
        .append('path')
        .attr('d', 'M 0 0 L 10 5 L 0 10 z')

    var svgEdges = svg.append('g')
      .selectAll('g')
      .data(edges)
      .join('g')

    svgEdges.append('line')
      .attr('stroke', 'black')
      .attr('marker-end', 'url(#triangle)')

    svgEdges.append('text')
      .text(function (d) {
        return d.value ? displayResultsGraphUri(d.value) : undefined
      })

    svgEdges.append('title')
      .text(function (d) {
        return d.value ? displaySparqlUri(d.value) : undefined
      })

    var svgNodes = svg.append('g')
      .selectAll('g')
      .data(nodes)
      .join('g')
        .call(drag)

    svgNodes.append('text')
      .attr('x', 0)
      .attr('y', 5)
      .attr('text-anchor', 'middle')
      .text(function (d) {
        if (d.label) {
          return d.label
        }

        if (d.type === 'uri') {
          return displayResultsGraphUri(d.value)
        } else if (d.type === 'bnode') {
          return '\xA0\xA0\xA0\xA0'
        } else {
          return JSON.stringify(d.value)
        }
      })
      .select(function (d) {
        d.width = this.getBBox().width + 10
        d.height = this.getBBox().height + 5
      })

    svgNodes.append('rect')
      .attr('x', function (d) { return -d.width / 2 })
      .attr('y', function (d) { return -d.height / 2 })
      .attr('width', function (d) { return d.width })
      .attr('height', function (d) { return d.height })
      .attr('fill', function (d) {
        if (d.type === 'uri') {
          return '#dddddd'
        } else if (d.type === 'bnode') {
          return '#ddddff'
        } else {
          return '#ffffff'
        }
      })
      .attr('stroke', 'black')
      .lower()

    svgNodes.append('title')
      .text(function (d) {
        if (d.type === 'uri') {
          return displaySparqlUri(d.value)
        } else if (d.type === 'bnode') {
          return 'Blank node'
        }
      })

    function clipPointToRectangle (point, rectangle) {
      var x1 = rectangle.x - rectangle.width / 2
      var x2 = rectangle.x + rectangle.width / 2
      var y1 = rectangle.y - rectangle.height / 2
      var y2 = rectangle.y + rectangle.height / 2

      if (point.x < rectangle.x) {
        var y = point.y + (rectangle.y - point.y) * (x1 - point.x) / (rectangle.x - point.x)
        if (y >= y1 && y <= y2) {
          return { x: x1, y: y }
        }
      }

      if (point.x > rectangle.x) {
        var y = point.y + (rectangle.y - point.y) * (point.x - x2) / (point.x - rectangle.x)
        if (y >= y1 && y <= y2) {
          return { x: x2, y: y }
        }
      }

      if (point.y < rectangle.y) {
        var x = point.x + (rectangle.x - point.x) * (y1 - point.y) / (rectangle.y - point.y)
        if (x >= x1 && x <= x2) {
          return { x: x, y: y1 }
        }
      }

      if (point.y > rectangle.y) {
        var x = point.x + (rectangle.x - point.x) * (point.y - y2) / (point.y - rectangle.y)
        if (x >= x1 && x <= x2) {
          return { x: x, y: y2 }
        }
      }

      return point
    }

    simulation.on('tick', function () {
      svgNodes
        .attr('transform', function (d) {
          return 'translate(' + d.x + ',' + d.y + ')'
        })

      svgEdges
        .selectAll('line')
          .select(function (d) {
            var target = clipPointToRectangle(d.source, d.target)
            d.x2 = target.x
            d.y2 = target.y
          })

      svgEdges
        .selectAll('line')
          .attr('x1', function (d) { return d.source.x })
          .attr('y1', function (d) { return d.source.y })
          .attr('x2', function (d) { return d.x2 })
          .attr('y2', function (d) { return d.y2 })

      svgEdges
        .selectAll('text')
          .attr('x', function (d) { return (d.source.x + d.target.x) / 2 })
          .attr('y', function (d) { return (d.source.y + d.target.y) / 2 - 5 })
          .attr('text-anchor', 'middle')
    })
  }

  function displayResultsMap (results) {
    var columns = results.head.vars.filter(function (column) {
      return !/^geometry$|Label$/.test(column)
    })

    var map = L.map(d3.select('#output').append('div').attr('style', 'height: 500px;').node())
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map)

    var featureGroup = []
    for (var i = 0; i < results.results.bindings.length; i++) {
      var result = results.results.bindings[i]
      var geometry = { type: 'Point', coordinates: [0, 0] }
      if (result.geometry && result.geometry.datatype === 'http://www.opengis.net/ont/geosparql#geoJSONLiteral') {
        try {
          geometry = JSON.parse(result.geometry.value)
        } catch (_) {
          // fail silently
        }
      }
      var popup = d3.create('div')
      var popupLink = popup.selectAll('div')
        .data(columns.map(function (column) {
          return {
            column: column,
            value: result[column] && result[column].value,
            type: result[column] && result[column].type,
            label: result[column + 'Label'] && result[column + 'Label'].value
          }
        }))
        .join('div')
      popupLink.append('b').text(function (d) {
        return d.column + ': '
      })
      popupLink.append(function (d) {
        var span = displaySparqlValue(d)
        if (d.label) {
          span.textContent = d.label
        }
        return span
      })
      var feature = L.geoJSON(geometry).addTo(map).bindPopup(popup.node())
      featureGroup.push(feature)
    }

    featureGroup = L.featureGroup(featureGroup)

    var bounds = featureGroup.getBounds()
    map.setView(bounds.getCenter(), map.getBoundsZoom(bounds))
  }

  function displayResultsText (results) {
    $('#output').empty()

    d3.select('#output')
      .append('pre')
      .attr('class', 'p-3 bg-white border')
      .append('code')
      .text(results)
  }

  function displayResults (json, display) {
    $('#output').empty()

    if (!json.results) {
      json.head.vars = ["ASK"]
      json.results = {
        bindings: [{ ASK: { value: json.boolean } }]
      }
    }

    if (display === 'graph') {
      displayResultsGraph(json)
    } else if (display === 'table') {
      displayResultsTable(json)
    } else if (display === 'map') {
      displayResultsMap(json)
    } else {
      throw new Error('Unknown display style')
    }
  }

  function displayLoading () {
    $('#output').empty()

    d3.select('#output')
      .append('div')
      .attr('class', 'alert alert-info')
      .text('Loading...')
  }

  function displayError (error) {
    $('#output').empty()

    d3.select('#output')
      .append('div')
      .attr('class', 'alert alert-danger')
      .text(error)
  }

  function runQuery () {
    var query = $('#prefixes').text() + '\n' + $('#query').val()
    var endpoint = $('#endpoint').val()
    var display = $('#display').val()

    displayLoading()

    fetch(endpoint, {
      method: 'POST',
      headers: {
        'accept': 'application/json',
        'content-type': 'application/sparql-query'
      },
      body: query
    }).then(function (response) {
      if (response.status >= 400) {
        response.text().then(displayError)
      } else if (response.headers.get('content-type').split('/')[0] === 'text') {
        response.text().then(displayResultsText)
      } else {
        response.json().then(function (results) {
          displayResults(results, display)
        })
      }
    }).catch(function (error) {
      displayError(error.message)
    })
  }

  $('#input').submit(function (e) {
    if (history && history.pushState) {
      var url = $(this).prop('action') + '?' + $(this).serialize()
      history.pushState({ input: $(this).serializeArray() }, '', url)
    }

    runQuery()

    e.preventDefault()
  })

  $(window).on('popstate', function (e) {
    if (e.originalEvent.state && Array.isArray(e.originalEvent.state.input)) {
      var state = e.originalEvent.state
      for (var i = 0; i < state.input.length; i++) {
        $('#input [name="' + state.input[i].name + '"]').val(state.input[i].value)
      }
      runQuery()
    }
  })

  if ($('#query').val()) {
    if (history && history.replaceState) {
      history.replaceState({ input: $('#input').serializeArray() }, '', document.location.href)
    }
    runQuery()
  }
})
