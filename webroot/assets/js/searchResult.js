$(function () {
    // Filter control
    $('#search-filter-collapse, #search-filter-open').click(function (e) {
        e.preventDefault()

        var open = $('#search-filters').css('display') === 'block'
        if (open) {
            // Open; being closed
            $('#search-filters').removeClass('d-lg-block')
            $('#search-filters').addClass('d-none')
            $('#search-filter-open').removeClass('d-lg-none d-none')
            $('#search-results').removeClass('col-lg-9')
        } else {
            // Closed; being opened
            $('#search-filters').removeClass('d-none')
            $('#search-filter-open').addClass('d-none')
            $('#search-results').addClass('col-lg-9')
        }

        updateTableShadow()
    })

    $('#search-filter-expand-all, #search-filter-collapse-all').removeClass('d-none')
    $('#search-filter-expand-all').click(function (e) {
        e.preventDefault()
        $('#search-filters .accordion-textbox').prop('checked', true)
    })
    $('#search-filter-collapse-all').click(function (e) {
        e.preventDefault()
        $('#search-filters .accordion-textbox').prop('checked', false)
    })

    // Share button
    $('#search-modal-share-copy').click(function (e) {
        e.preventDefault()
        $('#search-modal-share-link').select()
        document.execCommand('copy')
    })

    // Search results: compact layout
    function updateTableShadow () {
        var element = $('#search-results-compact .table-responsive')

        if (!element.length) {
            return
        }

        if (element.scrollLeft()) {
            $('#search-results-compact').addClass('shadow-left')
        } else {
            $('#search-results-compact').removeClass('shadow-left')
        }

        if (element.scrollLeft() < element.get(0).scrollWidth - element.get(0).offsetWidth) {
            $('#search-results-compact').addClass('shadow-right')
        } else {
            $('#search-results-compact').removeClass('shadow-right')
        }
    }

    $('#search-results-compact .table-responsive').scroll(updateTableShadow)
    $(window).resize(updateTableShadow)
    updateTableShadow()
})
