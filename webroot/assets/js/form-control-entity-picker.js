$('form').on('input', '.entity-picker input', function () {
    var searchInput = $(this)
    var entityPicker = searchInput.closest('.entity-picker')
    var searchResults = entityPicker.find('.entity-picker-results ol')
    var idField = $(document.getElementById(entityPicker.attr('data-for')))
    var query = searchInput.val()

    if (query === '') {
        idField.val('')
        searchResults.empty()
        return
    }

    var searchUrl = entityPicker.attr('data-search-url')
    var csrfToken = searchInput.closest('form').find('[name=_csrfToken]').val()

    $.ajax({
        method: 'POST',
        contentType: 'text/plain',
        data: query,
        dataType: 'json',
        url: searchUrl,
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRF-Token', csrfToken)
        }
    }).done(function (results) {
        if (idField.val()) {
            idField.val('')
        }

        searchResults.empty()

        for (var i = 0; i < results.length; i++) {
            var result = results[i]

            var searchResult = $(document.createElement('li'))
            searchResult.text(result.label)
            searchResult.attr('data-value', result.id)
            searchResult.attr('data-display-value', result.label)

            var searchResultSelect = $(document.createElement('button'))
            searchResultSelect.on('click', function (event) {
                idField.val($(this).closest('li').attr('data-value'))
                searchInput.val($(this).closest('li').attr('data-display-value'))
                searchInput.get(0).setCustomValidity('')
                searchResults.empty()
                searchInput.focus()
                event.preventDefault()
            })
            searchResult.append(searchResultSelect)

            searchResults.append(searchResult)
        }
    })
})

$('form').on('blur', '.entity-picker input', function () {
    var searchInput = $(this)
    var entityPicker = searchInput.closest('.entity-picker')
    var idField = $(document.getElementById(entityPicker.attr('data-for')))

    setTimeout(function () {
        if (entityPicker.is(':focus-within')) {
            return
        }

        if (!idField.val()) {
            searchInput.get(0).setCustomValidity('Please select a value')
        }
    }, 20)
})
