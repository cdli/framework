$(function () {
    function setIndex (item, index) {
        item.find('input, textarea, select').each(function () {
            var input = $(this)
            var name = input.attr('name')
            name = name.replace(/\[(\d+|template)\]/, '[' + index + ']')
            input.attr('name', name)

            var id = input.attr('id')
            if (id) {
                var label = item.find('label[for="' + id + '"]')
                var other = item.find('[data-for="' + id + '"]')
                id = id.replace(/-(\d+|template)-/, '-' + index + '-')
                input.attr('id', id)
                label.attr('for', id)
                other.attr('data-for', id)
            }

            if (name.match(/\[sequence\]$/)) {
                input.val(index)
            }
        })
    }

    function renumber (wrapper) {
        wrapper.children(':not(.d-none)').each(function (index) {
            setIndex($(this), index)
        })
    }

    function add (wrapper, template) {
        var copy = template.clone(true)
        copy.removeClass('d-none')
        setIndex(copy, wrapper.children().length)
        copy.find('button').on('click', remove(wrapper))
        wrapper.append(copy)
    }

    function remove (wrapper) {
        return function () {
            $(this).parents('.form-group').remove()
            renumber(wrapper)
        }
    }

    $('.many-to-many button[data-for]').each(function () {
        var wrapper = $('#' + $(this).attr('data-for'))
        wrapper.children(':not(.d-none)').find('button').on('click', remove(wrapper))

        // Detach template so input requirements are not applied
        var template = wrapper.children('.d-none')
        template.detach()

        // Insert dummy hidden variable to ensure an array is sent
        var templateInput = $('<input>').attr('type', 'hidden').attr('name', template.find('[name]').attr('name'))
        wrapper.after(templateInput)

        $(this).on('click', function () {
            add(wrapper, template)
        })
    })
})
