# https://stackoverflow.com/a/74029319/5095300
DIACRITICS = [*0x1DC0..0x1DFF, *0x0300..0x036F, *0xFE20..0xFE2F].pack('U*')
def remove_diacritics(value)
    if value.nil?
        nil
    else
        value
            .unicode_normalize(:nfd)
            .tr(DIACRITICS, '')
            .unicode_normalize(:nfc)
    end
end

def filter(event)
    # Diacritics
    event.set('designation_ascii', remove_diacritics(event.get('designation')))
    event.set('dates_referenced_ascii', remove_diacritics(event.get('dates_referenced')))
    event.set('provenience_ascii', remove_diacritics(event.get('provenience')))
    event.set('written_in_ascii', remove_diacritics(event.get('written_in')))
    event.set('archive_ascii', remove_diacritics(event.get('archive')))

    # IDs
    cdli_id = ['P' + event.get('id').to_s.rjust(6, '0')]

    all_composite_no = []
    composite_no = event.get('composite_no')
    witness_composite_no = event.get('witness_composite_no')
    unless composite_no.nil?
        cdli_id << composite_no
        all_composite_no << composite_no
    end
    unless witness_composite_no.nil?
        all_composite_no += witness_composite_no
    end

    all_seal_no = []
    seal_no = event.get('seal_no')
    impression_seal_no = event.get('impression_seal_no')
    unless seal_no.nil?
        cdli_id << seal_no
        all_seal_no << seal_no
    end
    unless impression_seal_no.nil?
        all_seal_no += impression_seal_no
    end

    event.set('cdli_id', cdli_id)
    event.set('all_composite_no', all_composite_no)
    event.set('all_seal_no', all_seal_no)

    # External resources
    external_resource = []
    external_resource_name = event.get('external_resource_name')
    unless external_resource_name.nil?
        external_resource_url = event.get('external_resource_url')
        external_resource_key = event.get('external_resource_key')
        external_resource_name.each_index { |i|
            external_resource << {
                external_resource: external_resource_name[i],
                url: external_resource_url[i],
                key: external_resource_key[i]
            }
        }
    end
    event.set('external_resource', external_resource)

    # Assets
    asset = []
    asset_id = event.get('asset_id')
    unless asset_id.nil?
        asset_type = event.get('asset_type')
        asset_artifact_aspect = event.get('asset_artifact_aspect')
        asset_file_format = event.get('asset_file_format')
        asset_is_public = event.get('asset_is_public')
        asset_annotations = event.get('asset_annotations')
        asset_type.each_index { |i|
            asset << {
                id: asset_id[i],
                type: asset_type[i],
                artifact_aspect: asset_artifact_aspect[i],
                file_format: asset_file_format[i],
                is_public: !asset_is_public[i].to_i.zero?,
                annotations: if asset_annotations[i].nil? then nil else asset_annotations[i].split(';') end
            }
        }
    end
    event.set('asset', asset)

    # Update events
    update = []
    created = nil
    modified = nil
    update_approved = event.get('update_approved')
    unless update_approved.nil?
        update_external_resource = event.get('update_external_resource')
        update_authors = event.get('update_authors')
        update_comments = event.get('update_comments')
        update_approved.each_index { |i|
            update << {
                approved: update_approved[i],
                comments: update_comments[i],
                external_resource: update_external_resource[i],
                authors: if update_authors[i].nil? then nil else update_authors[i].split(';') end,
                authors_ascii: if update_authors[i].nil? then nil else remove_diacritics(update_authors[i]).split(';') end
            }

            if created.nil? or update_approved[i] < created then
                created = update_approved[i]
            end

            if modified.nil? or update_approved[i] > modified then
                modified = update_approved[i]
            end
        }
    end
    event.set('update', update)
    event.set('created', created)
    event.set('modified', modified)

    # Publications
    publication = []
    publication_type = event.get('publication_type')
    unless publication_type.nil?
        publication_exact_reference = event.get('publication_exact_reference')
        publication_comments = event.get('publication_comments')
        publication_bibtexkey = event.get('publication_bibtexkey')
        publication_designation = event.get('publication_designation')
        publication_year = event.get('publication_year')
        publication_title = event.get('publication_title')
        publication_book_title = event.get('publication_book_title')
        publication_chapter = event.get('publication_chapter')
        publication_series = event.get('publication_series')
        publication_oclc = event.get('publication_oclc')
        publication_address = event.get('publication_address')
        publication_volume = event.get('publication_volume')
        publication_pages = event.get('publication_pages')
        publication_publisher = event.get('publication_publisher')
        publication_journal = event.get('publication_journal')
        publication_entry_type = event.get('publication_entry_type')
        publication_authors = event.get('publication_authors')
        publication_editors = event.get('publication_editors')
        publication_type.each_index { |i|
            publication << {
                type: publication_type[i],
                exact_reference: if publication_designation[i].nil? || publication_exact_reference[i].nil? then nil else publication_designation[i] + ' ' + publication_exact_reference[i] end,
                comments: publication_comments[i],
                bibtexkey: publication_bibtexkey[i],
                designation: publication_designation[i],
                year: publication_year[i],
                title: publication_title[i],
                book_title: publication_book_title[i],
                chapter: publication_chapter[i],
                series: publication_series[i],
                oclc: publication_oclc[i],
                address: publication_address[i],
                volume: publication_volume[i],
                pages: publication_pages[i],
                publisher: publication_publisher[i],
                journal: publication_journal[i],
                entry_type: publication_entry_type[i],
                authors: if publication_authors[i].nil? then nil else publication_authors[i].split(';') end,
                editors: if publication_editors[i].nil? then nil else publication_editors[i].split(';') end,

                designation_ascii: remove_diacritics(publication_designation[i]),
                exact_reference_ascii: if publication_designation[i].nil? || publication_exact_reference[i].nil? then nil else remove_diacritics(publication_designation[i] + ' ' + publication_exact_reference[i]) end,
                series_ascii: remove_diacritics(publication_series[i]),
                journal_ascii: remove_diacritics(publication_journal[i]),
                title_ascii: remove_diacritics(publication_title[i]),
                authors_ascii: if publication_authors[i].nil? then nil else remove_diacritics(publication_authors[i]).split(';') end,
                editors_ascii: if publication_editors[i].nil? then nil else remove_diacritics(publication_editors[i]).split(';') end
            }
        }
    end
    event.set('publication', publication)

    # ATF
    atf = event.get('atf')
    unless atf.nil?
        lines = atf.split(/[\r\n]+/)
        atf_structure = []
        atf_comments = []
        atf_transcription = []
        atf_translation_languages = {}
        atf_transliteration_lines = []
        atf_transliteration = []
        line_marker_surface = nil
        line_marker_section = nil

        lines.each { |line|
            if line.match(/^@column/)
                line_marker_section = line[1..-1]
                atf_structure << line[1..-1]
            elsif line.match(/^@/)
                line_marker_surface = line[1..-1]
                line_marker_section = nil
                atf_structure << line[1..-1]
            elsif line.match(/^#tr\.ts:/)
                atf_transcription << line[8..-1]
            elsif line.match(/^#tr\./)
                language = line[4..5]
                line = line[8..-1].gsub(/ $/, '')
                if atf_translation_languages.key?(language)
                    atf_translation_languages[language] = atf_translation_languages[language] + ' ' + line
                else
                    atf_translation_languages[language] = line
                end
            elsif line.match(/^[#$]/)
                atf_comments << line[1..-1]
            elsif line.match(/^\d/)
                line_number = line.gsub(/^(\d+\'?).*/, '\1')
                line_text = line.gsub(/^\d+\'?\. /, '').gsub(/ $/, '')
                atf_transliteration << line_text
                atf_transliteration_lines << {
                    surface: line_marker_surface,
                    section: line_marker_section,
                    line: line_number,
                    text: line_text
                }

                line_text.scan('($.+?$)') { |match|
                    atf_comments << match[2..-2]
                }
            end
        }

        atf_translation = []
        atf_translation_languages.each_pair { |key, value|
            atf_translation << {
                language: key,
                text: value
            }
        }

        event.set('atf_structure', atf_structure)
        event.set('atf_comments', atf_comments)
        event.set('atf_transcription', atf_transcription)
        event.set('atf_translation', atf_translation)
        event.set('atf_transliteration_lines', atf_transliteration_lines)
        event.set('atf_transliteration', atf_transliteration.join(' '))
    end

    return [event]
end
