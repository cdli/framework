input {
    jdbc {
        type => "artifacts"
        schedule => "50 * * * *"

        # Database connection
        jdbc_validate_connection => true
        jdbc_driver_class => "Java::org.mariadb.jdbc.Driver"
        jdbc_connection_string => "jdbc:mariadb://mariadb:3306/cdli_db"
        jdbc_user => 'root'
        jdbc_password => ''

        # Incremental updates
        # record_last_run => true
        # last_run_metadata_path => "/etc/logstash/jdbc_last_run_metadata_path/mariadb"
        # use_column_value => true
        # tracking_column => "id"

        # SQL
        statement_filepath => "/etc/logstash/resources/artifacts.sql"
    }
}

filter {
    if [retired] {
        mutate {
            add_field => {
                "[@metadata][elasticsearch_action]" => "delete"
            }
            remove_field => ["retired"]
        }
    } else {
        mutate {
            add_field => {
                "[@metadata][elasticsearch_action]" => "index"
            }
            remove_field => ["retired"]
        }
    }

    mutate {
        convert => {
            chemical_data => "boolean"
        }
    }

    mutate {
        split => {
            "material" => "|"
            "material_aspect" => "|"
            "material_color" => "|"
            "collection" => "|"
            "genre" => "|"
            "genre_comments" => "|"
            "language" => "|"
            "external_resource_name" => "|"
            "external_resource_url" => "|"
            "external_resource_key" => "|"
            "asset_id" => "|"
            "asset_type" => "|"
            "asset_artifact_aspect" => "|"
            "asset_file_format" => "|"
            "asset_is_public" => "|"
            "asset_annotations" => "|"

            "witness_composite_no" => "|"
            "impression_seal_no" => "|"

            "publication_type" => "|"
            "publication_exact_reference" => "|"
            "publication_comments" => "|"
            "publication_bibtexkey" => "|"
            "publication_designation" => "|"
            "publication_year" => "|"
            "publication_title" => "|"
            "publication_book_title" => "|"
            "publication_chapter" => "|"
            "publication_series" => "|"
            "publication_oclc" => "|"
            "publication_address" => "|"
            "publication_volume" => "|"
            "publication_pages" => "|"
            "publication_publisher" => "|"
            "publication_journal" => "|"
            "publication_entry_type" => "|"
            "publication_authors" => "|"
            "publication_editors" => "|"

            "update_approved" => "|"
            "update_comments" => "|"
            "update_external_resource" => "|"
            "update_authors" => "|"
        }
    }

    ruby {
        # This aggregates/zips the different arrays describing assets, updates,
        # and publications. E.g. the first value of asset_type and the first
        # value of artifact_is_public is taken to make a new array (called asset)
        # with as its first value a hash with the fields type and is_public.
        #
        # Input:
        # {
        #     asset_type: ['photo', 'lineart'],
        #     asset_is_public: ['1', '0']
        # }
        #
        # Output:
        # {
        #     asset: [
        #         { type: 'photo', is_public: true },
        #         { type: 'lineart', is_public: false }
        #     ]
        # }
        #
        # The code file referenced below is in /dev/data/logstash/resources/artifacts.rb
        path => "/etc/logstash/resources/artifacts.rb"
        remove_field => [
            # Becomes "external_resource"
            "external_resource_name",
            "external_resource_url",
            "external_resource_key",

            # Becomes "asset"
            "asset_id",
            "asset_type",
            "asset_artifact_aspect",
            "asset_file_format",
            "asset_is_public",
            "asset_annotations",

            # Becomes "update"
            "update_approved",
            "update_comments",
            "update_external_resource",
            "update_authors",

            # Becomes "publication"
            "publication_type",
            "publication_exact_reference",
            "publication_comments",
            "publication_bibtexkey",
            "publication_designation",
            "publication_year",
            "publication_title",
            "publication_book_title",
            "publication_chapter",
            "publication_series",
            "publication_oclc",
            "publication_address",
            "publication_volume",
            "publication_pages",
            "publication_publisher",
            "publication_journal",
            "publication_entry_type",
            "publication_authors",
            "publication_editors"
        ]
    }
}

output {
    stdout {
        codec => line {
            format => "P%{[id]}: %{[designation]}"
        }
    }

    elasticsearch {
        hosts => ["elasticsearch:9200"]
        index => "%{type}"
        action => "%{[@metadata][elasticsearch_action]}"
        user => "elastic"
        password  => "elasticchangeme"
        document_id => "%{id}"
        template => "/etc/logstash/resources/artifacts.json"
        template_name => "artifacts"
        template_overwrite => true
    }
}
