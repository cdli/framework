# Server timings

Middleware has been set up to support the use of the [`Server-Timing` header](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Server-Timing).

## Controllers

To use it in the controller, load the `ServerTimingComponent`:

```php
$this->loadComponent('ServerTiming');
```

This automatically enables timings of the action and the render steps of the controller. To
record additional timings, use the following functions:

```php
$this->ServerTiming->start('query', 'Query to gather artifact data'); // optional description
$this->ServerTiming->stop('query');
```

## Views

To use it in views, load the `ServerTimingHelper`:

```php
$this->loadHelper('ServerTiming');
```

This automatically enables timings of the rendering of the view (including sub-views) and
the layout.

To use it in helpers, load it as follows:

```php
public $helpers = ['ServerTiming'];
```

To record timings, use the following functions:

```php
$this->ServerTiming->start('serialize', 'Serialize data'); // optional description
$this->ServerTiming->stop('serialize');
```
