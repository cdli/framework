# Search in the CDLI

The CDLI provides a context-specific search into the catalog data that it maintains.
This means artifacts can be searched structurally (as opposed to free text) based
on annotations and related entities such as proveniences, periods, and materials.
To provide the search capabilities, an [ElasticSearch](https://www.elastic.co/elasticsearch/)
index is used. This index is built from the data in the [MariaDB](https://mariadb.org/)
database, using [Logstash](https://www.elastic.co/logstash/).

## Setting Up ElasticSearch

> This guide assumes the working directory is the `framework` git repository.

Before proceeding further, please make sure you have the latest version of the database.
This can be obtained at https://gitlab.com/cdli/phoenix_database (if necessary,
ask for access in the Digital Cuneiform Slack).

### Enabling the ElasticSearch and Logstash containers

In this project, ElasticSearch and Logstash are run in Docker containers. To
enable these containers in the Docker Compose configuration, follow the following
steps.

1. Copy `dev/config.json.dist` to `dev/config.json`
2. Open `dev/config.json`, search for `"elasticsearch"` or `"logstash"` and
    modify the file as follows:

    ```json
    "elasticsearch": {
        "is_default" : false,
        "enabled": true,
        "scale" : 1
    },
    "logstash": {
        "is_default" : false,
        "enabled": true,
        "scale" : 1
    },
    ```

### Scheduling Logstash

To make (or update) the search index, follow the following steps.

1. Open `dev/data/logstash/pipeline/logstash.conf`, search for `schedule`, and
change `50 * * * *` to e.g. `25 * * * *` if the time is 20 past the hour (10:20,
08:20, etc.). This gives the containers 5 minutes to boot before the indexing
starts. If necessary you can give more time.
2. Run `python3 dev/cdlidev.py -- up elasticsearch logstash`
3. At the indicated time the index should start generating. It first runs an SQL
query, which takes about 15 minutes or more depending on the  machine. At that
point it starts streaming the query results to ElasticSearch. This should show
up in the console.
4. When the indexing job is finished, stop the containers again.

### Exploring the Indices

To use ElasticSearch either in the framework or on its own:

1. Run `python3 dev/cdlidev.py -- up elasticsearch`
2. To preview data in indices visit: http://localhost:9200/artifacts/_search. This
will prompt for username and password, which can be found in `dev/docker-compose-org.yml`
(search for `ELASTICSEARCH_USERNAME`).

## Interacting with ElasticSearch from CakePHP

Search requests go through the following process in CakePHP

1. `app/cake/config/routes.php`: URLs are parsed and the request is sent to the controller.
2. `app/cake/src/Controller/SearchController.php`: Requests are interpreted.
  - `app/cake/src/Datasource/ElasticSearchQuery.php`: The search part of requests are transformed to an ElasticSearch query.
  - `app/cake/src/Datasource/ElasticSearchResultSet.php`: treat ElasticSearch results as DB query results would be treated
  - Additional functionality such as search settings and API downloads are provided.
3. `app/cake/templates/Search`: Unless API results are requested, render search (result) pages.

### `app/cake/config/routes.php`

Currently three routes are relevant for search.

  - `/:controller` maps `/search` to the `index` action of the `SearchController`.
  - `/:controller/*` is used for pages such as `/search/advanced` and `/search/settings`, mapping to the `view` action.
  - `/search/token-list/:kind` is used for token list exports (words and signs).

### `app/cake/src/Controller/SearchController.php`

  - The `index` action performs the search query and displays the search results.
  - The `view` action is used for search settings, advanced search, the commodity visualizer redirect, and the UCLA search redirect.
  - The `resolve` action is used to redirect URLs like `/P123456,Q000001,S000002` to search queries.
  - The `tokenList` action is used for generating token list exports.

### `app/cake/src/Datasource/ElasticSearchQuery.php`

  - The query is instantiated with an index name and access levels.
  - The main part of the query is transformed in `->find()`:

    ![](SEARCH-fig_1.png)

    Most field-specific transformations are done in `_getFieldQuery`, such as transliteration search, wildcard search, etc. are taken care of in `_getFieldQuery` (and `_getFieldQueryLiteral` & `_getFieldQueryTransliteration`).

  - Filters are added with `->where()`.
  - Results are ordered with `->order()`.
  - Filter buckets are obtained with `->aggregate()`.
