To redirect requests to `https://cdli.ucla.edu/`, the CDLI hosts a small
nginx server. This requires SSL certificates, which are generated with
the [`certbot`](https://hub.docker.com/r/certbot/certbot) Docker container.
To generate and renew certificates, the following commands are used.

> This guide assumes the working directory is the `framework` git repository,
> and that the `cdli_env` virtual environment is enabled:
>
>     source ../cdli_env/bin/activate

# Generating certificates

    python3 dev/cdlidev.py -- run --rm certbot certonly --webroot --webroot-path /var/www/certbot --domain cdli.ucla.edu --email cdli@ames.ox.ac.uk --no-eff-email --verbose

The slightly less verbose version looks like this:

    python3 dev/cdlidev.py -- run --rm certbot certonly --webroot -w /var/www/certbot -d cdli.ucla.edu -m cdli@ames.ox.ac.uk --no-eff-email -v

# Renewing certificates

    python3 dev/cdlidev.py -- run --rm certbot renew
