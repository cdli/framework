const syllabary = require('jtf-signlist/syllabary.json')

const express = require('express')
const app = express()

// Build indices of the syllabary
const signToReadings = {}
for (const sign of syllabary.signs.sign) {
    const name = sign.name[0]
    const readings = []

    for (const { logographic = [], syllabic = [] } of sign.values) {
        for (const { value: values } of [...logographic, ...syllabic]) {
            for (const value of values) {
                if (typeof value === 'string') {
                    readings.push(...value.split(','))
                } else if (value._) {
                    readings.push(...value._.split(','))
                }
            }
        }
    }

    signToReadings[name] = readings.map(reading => reading.endsWith('ₓ') ? `${reading}(${name})` : reading)
}

// API setup
app.use(express.json())
app.post('/permutations', async ({ body }, res) => {
    const signReadings = {}
    for (const sign of body) {
        if (!signToReadings[sign]) {
            continue
        }
        const readings = signToReadings[sign].slice()
        if (sign === 'AN') {
            readings.push('d')
        } else if (sign === 'DISZ') {
            readings.push('m')
        }
        signReadings[sign] = readings
    }
    res.json(signReadings)
})

module.exports = {
    app,
    message: `Running sign readings API (${Object.keys(signToReadings).length} signs)...`
}
