import codecs
import sys
import os
sys.path.append('../morphology-pre-annotation-tool')

from cdli_mpa_tool.checker import CONLChecker

def main():
    checker = CONLChecker(0, False)
    checker.check()

if __name__ == "__main__":
    main()
