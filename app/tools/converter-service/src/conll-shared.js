const { spawn } = require('child_process')
const path = require('path')

module.exports.convertConll = function (input) {
    return new Promise((resolve, reject) => {
        const conllU = spawn('python3', [
            path.resolve(__dirname, 'conll-u.py')
        ])

        let stdout = '';
        let stderr = '';
        conllU.stdout.on('data', data => { stdout += data.toString() });
        conllU.stderr.on('data', data => { stderr += data.toString() });

        conllU.on('exit', (code) => {
            if (code > 0) {
                reject({ stdout, stderr })
            } else {
                resolve(stdout)
            }
        })

        conllU.stdin.write(input)
        conllU.stdin.end()
    })
}
