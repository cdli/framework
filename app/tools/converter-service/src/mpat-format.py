import codecs
import sys
import os
sys.path.append('../morphology-pre-annotation-tool')

from cdli_mpa_tool.format import CONLLFormattor

# adapted from converter.py to support STDIN
def main():
    formatter = CONLLFormattor(0)
    with codecs.open(formatter.infile, 'r', 'utf-8') as f:
        for (i, line) in enumerate(f):
            print(formatter._CONLLFormattor__line_process(i+1, line), end="")

if __name__ == "__main__":
    main()
