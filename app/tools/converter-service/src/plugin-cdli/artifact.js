const { util } = require('@citation-js/core')
const { parse: parseDate } = require('@citation-js/date')

function createCdliNumber (id) {
    return 'P' + id.toString().padStart(6, '0')
}

function parseCdliDate (date) {
    return date ? parseDate(date.split('T')[0]) : null
}

module.exports = new util.Translator([
    { source: null, target: 'type', convert: { toTarget () { return 'webpage' } } },
    {
        source: 'designation',
        target: 'title',
        convert: { toTarget (designation) { return `${designation} artifact entry` } }
    },
    {
        source: null,
        target: 'container-title',
        convert: {
            toTarget () {
                return 'Cuneiform Digital Library Initiative (CDLI)'
            }
        }
    },
    {
        source: 'artifacts_updates',
        target: ['issued', 'original-date', 'version'],
        convert: {
            toTarget (updates) {
                const updateEvents = updates
                    .map(update => update.update_event)
                    .filter(Boolean)
                    .sort((a, b) => a.created < b.created ? -1 : a.created > b.created ? 1 : 0)

                const date = parseCdliDate(updateEvents[updateEvents.length - 1].approved)
                const originalDate = updateEvents.length > 1 ? parseCdliDate(updateEvents[0].approved) : undefined
                const version = updateEvents.length

                return [date, originalDate, version]
            }
        }
    },
    {
        source: null,
        target: 'accessed',
        convert: {
            toTarget () { return parseDate(Date.now()) }
        }
    },
    {
        source: 'id',
        target: ['id', 'citation-label', 'number', 'URL'],
        convert: {
            toTarget (id) {
                const number = createCdliNumber(id)
                return [
                    number,
                    `CDLI:${number}`,
                    number,
                    `https://cdli.ucla.edu/${number}`
                ]
            }
        }
    },
    // {
    //     source: 'languages',
    //     target: 'language',
    //     convert: {
    //         toTarget ([language]) {
    //             return language ? language.language : undefined
    //         }
    //     }
    // }
])
