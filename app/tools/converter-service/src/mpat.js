const { spawn } = require('child_process')
const path = require('path')
const express = require('express')
const app = express()

app.use(express.json({ limit: '500kb' }))
app.post('/check', ({ body, query: { base } }, res) => {
    const mpat = spawn('python3', [path.resolve(__dirname, 'mpat-check.py')])

    let stdout = '';
    let stderr = '';
    mpat.stdout.on('data', data => { stdout += data.toString() });
    mpat.stderr.on('data', data => { stderr += data.toString() });

    mpat.on('exit', (code) => {
        if (code > 0) {
            res.status(500).send(stderr)
        } else {
            res.send(stdout)
        }
    })

    mpat.stdin.write(body.annotation)
    mpat.stdin.end()
})

app.post('/format', ({ body, query: { base } }, res) => {
    const mpat = spawn('python3', [path.resolve(__dirname, 'mpat-format.py')])

    let stdout = '';
    let stderr = '';
    mpat.stdout.on('data', data => { stdout += data.toString() });
    mpat.stderr.on('data', data => { stderr += data.toString() });

    mpat.on('exit', (code) => {
        if (code > 0) {
            res.status(500).send('Formatting failed')
        } else {
            res.send(stdout)
        }
    })

    mpat.stdin.write(body.annotation)
    mpat.stdin.end()
})

app.post('/pre-annotate', ({ body, query: { base } }, res) => {
    const mpat = spawn('python3', [path.resolve(__dirname, 'mpat.py')])

    let stdout = '';
    let stderr = '';
    mpat.stdout.on('data', data => { stdout += data.toString() });
    mpat.stderr.on('data', data => { stderr += data.toString() });

    mpat.on('exit', (code) => {
        if (code > 0) {
            res.status(500).send('Annotating failed')
        } else {
            res.send(stdout)
        }
    })

    mpat.stdin.write(body.annotation)
    mpat.stdin.end()
})

module.exports = {
    app,
    message: `Running Morphology Pre-Annotation Tool...`
}
