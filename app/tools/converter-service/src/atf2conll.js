const { spawn } = require('child_process')
const path = require('path')
const express = require('express')
const app = express()

app.use(express.text({ limit: '500kb' }))
app.post('/convert/:file', ({ body, params: { file } }, res) => {
    const atf2conll = spawn('python3', [
        path.resolve(__dirname, 'atf2conll.py'),
        file
    ])

    let stdout = '';
    let stderr = '';
    atf2conll.stdout.on('data', data => { stdout += data.toString() });
    atf2conll.stderr.on('data', data => { stderr += data.toString() });

    atf2conll.on('exit', (code) => {
        if (code > 0) {
            res.status(500).send('Processing atf2conll failed')
        } else {
            res.send(stdout)
        }
    })

    atf2conll.stdin.write(body)
    atf2conll.stdin.end()
})

module.exports = {
    app,
    message: `Running atf2conll...`
}
