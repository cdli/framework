const express = require('express')
const app = express()

const { convertConll } = require('./conll-shared.js')

app.use(express.json({ limit: '500kb' }))
app.post('/format/u', ({ body, query: { base } }, res) => {
    return convertConll(body.annotation)
        .then(output => { res.send(output) })
        .catch(() => { res.status(500).send('Processing CDLI-CoNLL failed') })
})

module.exports = {
    app,
    message: `Running CoNLL-U...`
}
