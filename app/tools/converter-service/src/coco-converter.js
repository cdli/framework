const { spawn } = require('child_process')
const path = require('path')
const express = require('express')
const app = express()

app.use(express.json({ limit: '500kb' }))
app.post('/convert_to_w3c', ({ body, query: { base } }, res) => {
    if (body == null) {
        res.end()
        return
    }

    const coco_to_w3c = spawn('python3', [
        path.resolve(__dirname, 'coco_pipeline.py')
    ])

    let stdout = '';
    let stderr = '';
    coco_to_w3c.stdout.on('data', data => { stdout += data.toString() });
    coco_to_w3c.stderr.on('data', data => { stderr += data.toString() });
    coco_to_w3c.on('exit', (code) => {
        if (stdout.length > 0) {
            res.send(stdout)
        } else {
            res.write(JSON.stringify(stderr));
            res.send();
        }
    })

    coco_to_w3c.stdin.write(JSON.stringify(body))
    coco_to_w3c.stdin.end()
})

app.post('/via_saved_file_to_w3c', ({ body, query: { base } }, res) => {
    if (body == null) {
        res.end()
        return
    }

    const via_to_w3c = spawn('python3', [
        path.resolve(__dirname, 'saving_json_converter.py')
    ])

    let stdout = '';
    let stderr = '';
    via_to_w3c.stdout.on('data', data => { stdout += data.toString() });
    via_to_w3c.stderr.on('data', data => { stderr += data.toString() });
    via_to_w3c.on('exit', (code) => {
        if (stdout.length > 0) {
            res.send(stdout)
        } else {
            res.write(JSON.stringify(stderr));
            res.send();
        }
    })

    via_to_w3c.stdin.write(JSON.stringify(body))
    via_to_w3c.stdin.end()
})

module.exports = {
    app,
    message: `Running COCO-to-W3C converter...`
}
