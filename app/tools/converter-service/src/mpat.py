import codecs
import sys
import os
sys.path.append('../morphology-pre-annotation-tool')

from cdli_mpa_tool.annotate import CONLLAnnotator

# adapted from converter.py to support STDIN
def main():
    annotator = CONLLAnnotator(0)
    annotator.jsonfile = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'annotated_morph_dict_v2.json')
    annotator._CONLLAnnotator__load_annotations()
    with codecs.open(annotator.infile, 'r', 'utf-8') as f:
        for (i, line) in enumerate(f):
            line = annotator._CONLLAnnotator__line_process(i+1, line)
            print(line, end="")

if __name__ == "__main__":
    main()
