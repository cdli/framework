const { spawn } = require('child_process')
const fs = require('fs').promises
const os = require('os')
const path = require('path')
const express = require('express')
const app = express()

// alignInput, alignOutput:
// An issue with conll-merge causes deletions at the end of the diff to not be
// processed. By appending a token to both files and removing it afterwords,
// the diff is processed correctly.
function alignInput (file, alignCol) {
    const row = Array(alignCol).fill('?')
    row[alignCol] = 'ENDTOKEN'
    return file.replace(/\n*$/, '\n' + row.join('\t') + '\n')
}

function alignOutput (file) {
    return file.replace(/\n.*ENDTOKEN.*\n/, '\n')
}

function prepareInput (file, alignCol) {
    file = file.replace(/\r\n/g, '\n')
    const header = file.split(/\n/g)[1]
    const width = header.split('\t').length
    // Fix the number of columns in each row
    return alignInput(file, alignCol).replace(/^([^#].*)/gm, line => {
        const missingColCount = width - line.split('\t').length
        return line + '\t' + Array(missingColCount).fill('?').join('\t')
    })
}

function prepareOutput (file, comments, alignCol, inputCount) {
    const crlf = /\r\n/.test(file)
    const lines = alignOutput(file.replace(/\r\n/g, '\n')).replace(/^\n+|\n+$/g, '').split('\n')
    let width
    const output = comments.slice()

    for (const line of lines) {
        // Skip comments
        if (line.startsWith('#')) { continue }

        const columns = line.split('\t')

        // Skip RETOK lines
        if (columns[alignCol].startsWith('*RETOK*-')) { continue }

        if (!width) { width = columns.length / inputCount }
        const row = []

        // For each column...
        for (let x = 0; x < width; x++) {
            let value = '_'
            // ...go through each of the files backwards...
            for (let i = inputCount - 1; i >= 0; i--) {
                const candidate = columns[x + i * width]
                // ...and if the value isn't empty or '?', output that value.
                if (candidate !== '?' && candidate !== '') {
                    value = candidate
                    break
                }
            }
            row.push(value)
        }

        output.push(row.join('\t'))
    }

    return output.join(crlf ? '\r\n' : '\n')
}

app.use(express.json({ limit: '500kb' }))
app.post('/merge', async ({ body, query: { base } }, res) => {
    const directory = await fs.mkdtemp(os.tmpdir() + '/')
    const files = body.files.map((content, index) => [
        path.join(directory, index.toString()),
        prepareInput(content, body.alignCol)
    ])
    await Promise.all(files.map(([file, content]) => fs.writeFile(file, content)))

    const comments = files[0][1].match(/^#.*/gm)

    const args = [
        ...files.map(([file]) => file),
        // Align on column
        body.alignCol, body.alignCol,
        // Drop columns manually instead (see prepareOutput)
        '-drop', 'none'
    ]

    const conllMerge = spawn(path.resolve(__dirname, '../../conll-merge/cmd/merge.sh'), args)

    let stdout = '';
    let stderr = '';
    conllMerge.stdout.on('data', data => { stdout += data.toString() });
    conllMerge.stderr.on('data', data => { stderr += data.toString() });

    conllMerge.on('exit', async (code) => {
        await fs.rm(directory, { recursive: true })

        if (code > 0) {
            res.status(500).send('Processing CDLI-CoNLL failed')
        } else {
            res.send(prepareOutput(stdout, comments, body.alignCol, files.length))
        }
    })
})

module.exports = {
    app,
    message: `Running CoNLL-merge...`
}
