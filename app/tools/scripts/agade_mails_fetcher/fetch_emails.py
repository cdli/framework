import os
import mysql.connector
from email import policy
import imaplib
import email as em
import datetime

user = 'cdli.subscriptions@gmail.com'
password = 'subscriptions@cdli'
imap_url = 'imap.gmail.com'


def get_body(msg):
    """
        Extracts and decodes the email message 
        Returns:
            email message body
                type: string
    """
    content = msg.get_payload(None, True).decode('utf-8')
    return content


def search(key, value, con):
    """
        Search for a key value pair
    """
    result, data = con.search(None, key, '"{}"'.format(value))
    return data


def get_emails(result_bytes):
    """
        Get the list of emails under this label
    """
    msgs = []
    for num in result_bytes[0].split():
        typ, data = con.fetch(num, '(RFC822)')
        msgs.append(data)

    return msgs


def parse_datetime(date):
    """
        Parse datetime in the format required to save in SQL database
    """
    tup = em.utils.parsedate(date)
    d = datetime.datetime(*(tup[0:6]))
    dStr = d.strftime('%Y-%m-%d %H:%M:%S')

    return dStr


# this is done to make SSL connnection with GMAIL
con = imaplib.IMAP4_SSL(imap_url)

# logging the user in
con.login(user, password)

# calling function to check for email under this label
con.select('Inbox')

# fetching emails from the mailing list
msgs = get_emails(search('FROM', 'Agade@listserv.unc.edu', con))

# Finding the required content from our msgs
all_emails = []
for msg in msgs:
    raw_email = msg[0][1]
    raw_email_string = raw_email.decode('utf-8')
    email_message = em.message_from_string(
        raw_email_string, policy=policy.default)
    all_emails.append(email_message)


formatted_emails = []
exceptions = []
for email in all_emails[0:30]:
    try:
        formatted_email = {}
        # get title and category from the email subject
        subject = email['subject'].strip().replace("[agade] ", "")
        colon_pos = subject.find(':')
        dot_pos = subject.find('.')

        # For email subject having categories seperated by "." 
        # (Ex. "[agade] EVENTS. March at ISAW")
        if ((dot_pos < colon_pos and dot_pos != -1) or colon_pos == -1):
            subject_split = subject.split(".")
            formatted_email['category'] = subject_split[0].strip()
            formatted_email['title'] = subject.replace(formatted_email['category'] + '.', '').strip()

        # For email subject having categories seperated by ":" 
        # (Ex. "[agade] EVENTS: March at ISAW")
        else:
            subject_split = subject.split(":")
            formatted_email['category'] = subject_split[0].strip()
            formatted_email['title'] = subject.replace(formatted_email['category'] + ':', '').strip()

        # For now, exclude the error formatting entries
        temp = subject_split[1]

        # Formatting in required way
        formatted_email['date'] = parse_datetime(email['date'])
        formatted_email['id'] = int(email['Message-ID'].split('-')[2])
        formatted_email['content'] = get_body(email) 
        #For removing the subscription message
        index = formatted_email['content'].find("--- You are")
        formatted_email['content'] = formatted_email['content'][:index]
        formatted_emails.append(formatted_email)

    except:
        exceptions.append(email)


# Insert into Database
mydb = mysql.connector.connect(host='mariadb',
                               user='root',
                               passwd='',
                               port=3306,
                               database='cdli_db')


mycursor = mydb.cursor()

# Check if the database "cdli_db" exists
mycursor.execute("SHOW DATABASES")
database_list = mycursor.fetchall()
database_list = [x[0] for x in database_list]
if ( 'cdli_db' in database_list):
    # Create the table, if it doesn't exist
    mycursor.execute("SHOW TABLES")
    table_list = mycursor.fetchall()
    table_list = [x[0] for x in table_list]
    if('agade_mails' not in table_list):
        mycursor.execute("CREATE TABLE agade_mails ( \
                            id INT NOT NULL PRIMARY KEY, \
                            title VARCHAR(400), \
                            date DATETIME, \
                            category VARCHAR(100), \
                            content TEXT(30000), \
                            is_public BOOL);")

    if('cdli_tags' not in table_list):
        mycursor.execute("CREATE TABLE cdli_tags ( \
                            id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, \
                            cdli_tag VARCHAR(100));") 

    if('agade_mails_cdli_tags' not in table_list):
        mycursor.execute("CREATE TABLE agade_mails_cdli_tags ( \
                            id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, \
                            agade_mail_id INTEGER NOT NULL, \
                            cdli_tag_id INTEGER NOT NULL, \
                            FOREIGN KEY (agade_mail_id) REFERENCES agade_mails (id) ON DELETE RESTRICT ON UPDATE CASCADE, \
                            FOREIGN KEY (cdli_tag_id) REFERENCES cdli_tags (id) ON DELETE RESTRICT ON UPDATE CASCADE);")                                       


    # Fetching the latest email id (to get the latest mail)
    mycursor.execute("SELECT id FROM agade_mails;")
    results = mycursor.fetchall()
    if(len(results) == 0):
        filtered_emails = formatted_emails

    else:
        id_list = list(map(lambda x: x[0], results))
        filtered_emails = []
        for email in formatted_emails:
            if email['id'] not in id_list:
                filtered_emails.append(email)

    for email in filtered_emails:
        sql = "INSERT INTO agade_mails (id, title, category, \
                date, content, is_public) \
                VALUES (%s, %s, %s, %s, %s, %s)"
        val = (email['id'],
            email['title'],
            email['category'],
            email['date'],
            email['content'],
            0)  # by default, the entries are hidden from public
        mycursor.execute(sql, val)
        mydb.commit()

    # logging the emails fetching
    os.system('sh /srv/agade_mails_fetcher/script.sh')

else:
    os.system('echo "$(date): Agade emails cannot be stored. \n Database does not exist" >> /var/log/cron.log 2>&1')    