<?php

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    $routes->addExtensions([
        'pdf',
        'json',
        'txt',
        'svg',
        'ndjson',

        // Linked data
        'xml',
        'rdf',
        'ttl',
        'nt',
        'jsonld',

        // Tables
        'csv',
        'tsv',
        'xlsx',

        // Bibliographies
        'bib',
        'ris'
    ]);

    // Admin prefix
    Router::prefix('admin', function ($routes) {

        // Pages for each controller
        $routes->connect('/:controller', ['action' => 'index']);
        $routes->connect('/:controller/edit/*', ['action' => 'edit']);
        $routes->connect('/:controller/add/*', ['action' => 'add']);
        $routes->connect('/:controller/delete/*', ['action' => 'delete']);
        $routes->connect('/:controller/export/*', ['action' => 'export']);
        $routes->connect('/:controller/*', ['action' => 'view']);

        // Dashboard
        $routes->connect('/dashboard', ['controller' => 'Home', 'action' => 'dashboard']);

        // Routes for Admin journals dashboard.
        $routes->connect('/periods/token-lists', ['controller' => 'Periods', 'action' => 'tokenLists']);
        $routes->connect('/periods/:id/token-list/:kind', ['controller' => 'Periods', 'action' => 'tokenList'])
            ->setPass(['id', 'kind']);
        $routes->connect('/articles/link/publications', ['controller' => 'Articles', 'action' => 'link_publications']);
        $routes->connect('/api/view/link/:article/:publication', ['controller' => 'Articles', 'action' => 'view_link']);
        $routes->connect('/api/link/:article/:publication', ['controller' => 'Articles', 'action' => 'complete_link']);
        $routes->connect('/api/view/links/:id/all/:type', ['controller' => 'Articles', 'action' => 'view_all_article_links']);
        $routes->connect('/api/unlink/:article/:publication', ['controller' => 'Articles', 'action' => 'complete_unlink']);
        $routes->connect('/api/get/:id/:type', ['controller' => 'Articles', 'action' => 'get_article_by_id']);
        $routes->connect('/api/suggest/:key/:type', ['controller' => 'Articles', 'action' => 'link_suggest']);

        // Routes for Journals display at admin dashboard.
        $routes->connect('/articles/add', ['controller' => 'Articles', 'action' => 'add']);
        $routes->connect('/articles/add/:type', ['controller' => 'Articles', 'action' => 'add'], ['_name' => 'addSpecific'])
            ->setPass(['type']);
        $routes->connect('/articles/edit/:type/:article', ['controller' => 'Articles', 'action' => 'edit'])
            ->setPass(['type', 'article']);
        $routes->connect('/articles/delete/:type/:article', ['controller' => 'Articles', 'action' => 'delete'])
            ->setPass(['type', 'article']);
        $routes->post('/articles/preview', ['controller' => 'Articles', 'action' => 'preview']);

        $routes->connect('/articles/convert-bibtex', ['controller' => 'Articles', 'action' => 'convertBibtex']);
        $routes->connect('/articles/convert-latex', ['controller' => 'Articles', 'action' => 'convertLatex']);

        // Routes for Admin Author Ajax.
        $routes->connect('/authors/search/:author', ['controller' => 'Authors', 'action' => 'author_search_ajax'])
            ->setPass(['author']);
        $routes->connect('/authors/add/:author', ['controller' => 'Authors', 'action' => 'add_author_ajax'])
            ->setPass(['author']);

        $routes->connect('/uploads/geturl', ['controller' => 'Uploads ', 'action' => 'geturl']);
        $routes->connect('/uploads/dashboard', ['controller' => 'Uploads ', 'action' => 'dashboard']);

        // Routes for Admin CdliTablet
        $routes->connect('/cdli-tablet/delete-all', ['controller' => 'CdliTablet', 'action' => 'deleteAll']);

        // Route for Retired Artifacts Index
        $routes->connect('/artifacts/retired', ['controller' => 'Artifacts', 'action' => 'retired']);

        // Assets
        $routes->connect('/artifact-assets/missing-files', ['controller' => 'ArtifactAssets', 'action' => 'missingFiles']);
        $routes->connect('/artifact-assets/update-index', ['controller' => 'ArtifactAssets', 'action' => 'updateIndex']);

        //Route for Add/Edit Featured Seals
        $routes->connect('/artifacts/featured-seals/add', ['controller' => 'Artifacts' , 'action' => 'featuredSealsAdd']);
        $routes->connect('/artifacts/featured-seals/edit/:id', ['controller' => 'Artifacts' , 'action' => 'featuredSealsEdit'], ['pass' => ['id']]);

        //Route for Seal Chemistry
        $routes->connect('/artifacts/seal-chemistry/add', ['controller' => 'Artifacts' , 'action' => 'sealChemistryAdd']);
        //$routes->connect('/artifacts/seal-chemistry/edit/:id', ['controller' => 'Artifacts' , 'action' => 'sealChemistryEdit'],['pass' => ['id']]);
        $routes->connect('/artifacts/calibration/add', ['controller' => 'Artifacts' , 'action' => 'calibrationAdd']);

        // Other routes
        $routes->connect('/users/forgot-password/*', ['controller' => 'Users', 'action' => 'forgotPassword']);

        $routes->fallbacks('DashedRoute');
    });

    // Docs
    $routes->connect('/docs/audience/*', ['controller' => 'Docs', 'action' => 'audience']);
    $routes->connect('/docs/category/*', ['controller' => 'Docs', 'action' => 'category']);
    $routes->connect('/docs/:document/:version', ['controller' => 'Docs', 'action' => 'otherDocuments'])
        ->setPatterns(['document' => 'schema|vocab'])
        ->setPass(['document', 'version']);

    // Permalinks
    $routes->connect('/:id', ['controller' => 'Artifacts', 'action' => 'resolve'])
        ->setPatterns(['id' => '[PQS]\d{6}|S\d{6}\.\d'])
        ->setPass(['id']);
    $routes->connect('/:id', ['controller' => 'Search', 'action' => 'resolve'])
        ->setPatterns(['id' => '([PQS]\d{6}|S\d{6}\.\d)(,([PQS]\d{6}|S\d{6}\.\d))+'])
        ->setPass(['id']);

    // Artifacts (see also controllers below)
    $routes->connect('/artifact-assets/authorize', ['controller' => 'ArtifactAssets', 'action' => 'authorize']);
    $routes->connect('/artifact-assets/resolve', ['controller' => 'ArtifactAssets', 'action' => 'resolve']);
    $routes->connect('/artifacts/:id/images', ['controller' => 'Artifacts', 'action' => 'images'])->setPass(['id']);
    $routes->connect('/artifacts/:id/reader/:image', ['controller' => 'Artifacts', 'action' => 'reader'])->setPass(['id', 'image']);
    $routes->connect('/artifacts/:id/annotations/:image', ['controller' => 'Artifacts', 'action' => 'annotations'])->setPass(['id', 'image']);
    $routes->connect(
        '/artifacts/:id/history/:type',
        ['controller' => 'Artifacts', 'action' => 'history']
    )->setPass(['id', 'type'])->setPersist(['id', 'type']);
    $routes->connect(
        '/artifacts/:id/inscription',
        ['controller' => 'Artifacts', 'action' => 'resolveInscription']
    )->setPass(['id']);
    $routes->connect(
        '/artifacts/:id/inscription/:format',
        ['controller' => 'Artifacts', 'action' => 'resolveInscription'],
        ['_name' => 'resolveInscriptionWithFormat']
    )->setPass(['id']);
    $routes->connect('/artifacts/composites', ['controller' => 'Artifacts' , 'action' => 'composites']);
    $routes->connect('/artifacts/composites/:compositetype', ['controller' => 'Artifacts' , 'action' => 'composites'], ['pass' => ['compositetype']]);
    $routes->connect('/artifacts/composites-score/:compositenumber', ['controller' => 'Artifacts' , 'action' => 'compositesScore'], ['pass' => ['compositenumber']]);
    $routes->connect('/artifacts/seals', ['controller' => 'Artifacts' , 'action' => 'seals']);
    $routes->connect('/artifacts/:id/chemistry/csv', ['controller' => 'Artifacts', 'action' => 'downloadChemistryCSV'], ['pass' => ['id']]);
    $routes->connect('/artifacts/:id/:artifactId/delete', ['controller' => 'Artifacts', 'action' => 'deleteChemistry'], ['pass' => ['id','artifactId']]);
    $routes->connect('/artifacts/seal-chemistry/add', ['controller' => 'Artifacts' , 'action' => 'sealChemistryAdd']);
    $routes->connect('/artifacts/seal-chemistry/edit/:id', ['controller' => 'Artifacts' , 'action' => 'sealChemistryEdit'], ['pass' => ['id']]);
    $routes->connect('/artifact-asset-annotations/delete/:id', ['controller' => 'ArtifactAssetAnnotations', 'action' => 'delete'])->setPass(['id']);

    // Publications
    $routes->connect('/publications/merge', ['controller' => 'Publications', 'action' => 'merge']);
    $routes->connect('/publications/search', ['controller' => 'Publications', 'action' => 'search']);
    $routes->connect('/publications/upload', ['controller' => 'Publications', 'action' => 'upload']);
    $routes->connect('/publications/delete/:id', ['controller' => 'Publications', 'action' => 'delete'])->setPass(['id']);

    // Search API
    $routes->connect('/search/token-list/:kind', ['controller' => 'Search', 'action' => 'tokenList'])->setPass(['kind']);

    // Update events
    $routes->connect('/update-events/approve/:id', ['controller' => 'UpdateEvents', 'action' => 'approve'])->setPass(['id']);
    $routes->connect('/update-events/submit/:id', ['controller' => 'UpdateEvents', 'action' => 'submit'])->setPass(['id']);
    $routes->connect('/update-events/decline/:id', ['controller' => 'UpdateEvents', 'action' => 'decline'])->setPass(['id']);
    $routes->connect('/update-events/delete/:id', ['controller' => 'UpdateEvents', 'action' => 'delete'])->setPass(['id']);
    $routes->connect('/update-events/cancel', ['controller' => 'UpdateEvents', 'action' => 'cancel']);

    // Articles
    $routes->connect('/articles/:type', ['controller' => 'Articles', 'action' => 'index'])->setPass(['type']);
    $routes->connect('/articles/:type/:id', ['controller' => 'Articles', 'action' => 'view'])->setPass(['type', 'id']);

    // Authors
    $routes->connect('/authors/search', ['controller' => 'Authors', 'action' => 'search']);

    // Visualization routes
    $routes->connect('/stats/:table', ['controller' => 'Stats', 'action' => 'index'], ['pass' => ['table']]);
    $routes->connect('/viz/:action', ['controller' => 'Viz']);
    $routes->connect('/heatmap/post', ['controller' => 'Heatmap', 'action' => 'post']);

    // Authentication
    $routes->connect('/users/profile', ['controller' => 'Users', 'action' => 'profile']);
    $routes->connect('/users/edit/profile', ['controller' => 'Users', 'action' => 'profileEdit']);
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login']);
    $routes->connect('/register', ['controller' => 'Users', 'action' => 'register']);
    $routes->connect('/forgot/:type', ['controller' => 'Forgot', 'action' => 'index'], ['pass' => ['type']]);
    $routes->connect('/forgot/password/reset/:token', ['controller' => 'Forgot', 'action' => 'password'], ['pass' => ['token']]);
    $routes->connect('/users/delete', ['controller' => 'Users', 'action' => 'delete']);

    // Tools
    $routes->connect('/resources/:action/*', ['controller' => 'Resources']);

    // Postings
    $routes->connect('/3dviewer', ['controller' => 'Postings', 'action' => 'view', ['id' => 193]], ['_name' => '3dviewer']);
    $routes->connect('/about', ['controller' => 'Postings', 'action' => 'view', ['id' => 31]], ['_name' => 'about']);
    $routes->connect('/contribute', ['controller' => 'Postings', 'action' => 'view', ['id' => 190]], ['_name' => 'contribute']);

    $routes->redirect('/sign-readings/preferred', '/sign-readings?preferred_reading=1');

    // Shortcuts
    $routes->connect('/browse', ['controller' => 'Home', 'action' => 'browse']);
    $routes->connect('/highlights', ['controller' => 'Postings', 'action' => 'highlights']);
    $routes->connect('/news', ['controller' => 'Postings' , 'action' => 'news']);
    $routes->redirect('/home', ['controller' => 'Home']);

    // Pages for each controller
    $routes->connect('/:controller', ['action' => 'index']);
    $routes->connect('/:controller/add', ['action' => 'add']);
    $routes->connect('/:controller/edit/:id', ['action' => 'edit'])->setPass(['id']);
    $routes->connect('/:controller/history/:id', ['action' => 'history'])->setPass(['id']);
    $routes->connect('/:controller/:id/:format', ['action' => 'view'])->setPass(['id']);
    $routes->connect('/:controller/*', ['action' => 'view']);

    // Home page
    $routes->connect('/', ['controller' => 'Home']);

    //Route for terms of use
    $routes->connect('/terms-of-use', ['controller' => 'Postings', 'action' => 'view', 5]);

    //Route for hiding cookie notification
    $routes->connect('/hide-cookies-notification', ['controller' => 'Home', 'action' => 'hideCookiesNotification']);
});
