<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EntitiesPublicationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EntitiesPublicationsTable Test Case
 */
class EntitiesPublicationsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\EntitiesPublicationsTable
     */
    public $EntitiesPublications;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.entities_publications',
        'app.artifacts',
        'app.publications',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('EntitiesPublications') ? [] : ['className' => EntitiesPublicationsTable::class];
        $this->EntitiesPublications = TableRegistry::getTableLocator()->get('EntitiesPublications', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->EntitiesPublications);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
