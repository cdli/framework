<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CalibrationTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CalibrationTable Test Case
 */
class CalibrationTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CalibrationTable
     */
    protected $Calibration;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Calibration',
        'app.ChemicalData',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Calibration') ? [] : ['className' => CalibrationTable::class];
        $this->Calibration = $this->getTableLocator()->get('Calibration', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Calibration);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\CalibrationTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
