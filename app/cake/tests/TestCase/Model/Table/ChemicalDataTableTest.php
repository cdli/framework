<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ChemicalDataTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ChemicalDataTable Test Case
 */
class ChemicalDataTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ChemicalDataTable
     */
    protected $ChemicalData;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ChemicalData',
        'app.Artifacts',
        'app.Calibration',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ChemicalData') ? [] : ['className' => ChemicalDataTable::class];
        $this->ChemicalData = $this->getTableLocator()->get('ChemicalData', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ChemicalData);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ChemicalDataTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ChemicalDataTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
