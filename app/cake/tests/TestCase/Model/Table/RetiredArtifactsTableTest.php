<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RetiredArtifactsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RetiredArtifactsTable Test Case
 */
class RetiredArtifactsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RetiredArtifactsTable
     */
    public $RetiredArtifacts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.retired_artifacts',
        'app.artifacts',
        'app.new_artifacts',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RetiredArtifacts') ? [] : ['className' => RetiredArtifactsTable::class];
        $this->RetiredArtifacts = TableRegistry::getTableLocator()->get('RetiredArtifacts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RetiredArtifacts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
