<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FeaturedSealsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FeaturedSealsTable Test Case
 */
class FeaturedSealsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FeaturedSealsTable
     */
    protected $FeaturedSeals;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.FeaturedSeals',
        'app.Artifacts',
        'app.Images',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('FeaturedSeals') ? [] : ['className' => FeaturedSealsTable::class];
        $this->FeaturedSeals = $this->getTableLocator()->get('FeaturedSeals', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->FeaturedSeals);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\FeaturedSealsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\FeaturedSealsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
