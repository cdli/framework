<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ImageAnnotationsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ImageAnnotationsTable Test Case
 */
class ImageAnnotationsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ImageAnnotationsTable
     */
    protected $ImageAnnotations;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ImageAnnotations',
        'app.Annotations',
        'app.AnnotationTypes',
        'app.Images',
        'app.Artifacts',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ImageAnnotations') ? [] : ['className' => ImageAnnotationsTable::class];
        $this->ImageAnnotations = $this->getTableLocator()->get('ImageAnnotations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ImageAnnotations);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ImageAnnotationsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ImageAnnotationsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
