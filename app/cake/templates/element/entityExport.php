<?php
use Cake\Utility\Inflector;
?>

<?php
$entity_name = Inflector::humanize(Inflector::tableize($this->request->getParam('controller')));
$params = $this->request->getQueryParams();
$params_current = $this->request->getQueryParams();

if(count($this->Paginator->params()) != 0) {
    $count = $this->Paginator->params()['count'];
    $params['limit'] = $count;
}

?>
    <?php if ($entity_name == "Publications") : ?>
        <div class="export-grid col-4">
        <?= $this->Dropdown->open('Export ' . $entity_name . ' <i class="fa fa-chevron-down" aria-hidden="true"></i>') ?>
            <div class="row" style="min-width: 40rem;">

                <div class="col-lg-6 col-md-6 col-sm-6">
                    <h3 style="color: black;">Download current page</h3>
                    <?php $params_current['format'] = 'txt'; ?>
                    <?= $this->Html->link(
                        __('Download the list of ' . $entity_name . ' in text format to your computer' . ' <i class="fa fa-download" aria-hidden="true"></i>'),
                        ['controller' => $entity_name, 'action' => 'index', '?' => $params_current],
                        ['download'=> $entity_name, 'escape' => false],
                        ['class' => 'ml-3']
                    ) ?>
                    <h3 style="color: black;">Flat Data</h3>
                    <?php $params_current['format'] = 'csv'; ?>
                    <?= $this->Html->link(
                        __('As {0}', 'CSV'),
                        ['controller' => $entity_name, 'action' => 'index', '?' => $params_current],
                        ['class' => 'ml-3']
                    ) ?>
                    <?php $params_current['format'] = 'tsv'; ?>
                    <?= $this->Html->link(
                        __('As {0}', 'TSV'),
                        ['controller' => $entity_name, 'action' => 'index', '?' => $params_current],
                        ['class' => 'ml-3']
                    ) ?>
                    <h3 style="color: black;">Linked Data</h3>
                    <?php $params_current['format'] = 'ttl'; ?>
                    <?= $this->Html->link(
                        __('As {0}', 'TTL'),
                        ['controller' => $entity_name, 'action' => 'index', '?' => $params_current],
                        ['class' => 'ml-3']
                    ) ?>
                    <?php $params_current['format'] = 'rdfjson'; ?>
                    <?= $this->Html->link(
                        __('As {0}', 'RDF/JSON'),
                        ['controller' => $entity_name, 'action' => 'index', '?' => $params_current],
                        ['class' => 'ml-3']
                    ) ?>
                    <h3 style="color: black;">Expanded Data</h3>
                    <?php $params_current['format'] = 'json'; ?>
                    <?= $this->Html->link(
                        __('As {0}', 'JSON'),
                        ['controller' => $entity_name, 'action' => 'index', '?' => $params_current],
                        ['class' => 'ml-3']
                    ) ?>
                    <h3 style="color: black;">Bibliography</h3>
                    <?php $params_current['format'] = 'bib'; ?>
                    <?= $this->Html->link(
                        __('As {0}', 'BibTex'),
                        ['controller' => $entity_name, 'action' => 'index', '?' => $params_current],
                        ['class' => 'ml-3']
                    ) ?>
                    <?php $params_current['format'] = 'ris'; ?>
                    <?= $this->Html->link(
                        __('As {0}', 'RIS'),
                        ['controller' => $entity_name, 'action' => 'index', '?' => $params_current],
                        ['class' => 'ml-3']
                    ) ?>
                </div>


                <div class="col-lg-6 col-md-6 col-sm-6">
                    <h3 style="color: black;">Download all results</h3>
                    <?php if ($this->Paginator->params()['count'] > 1000): ?>
                        <span type="text">To download publications in a file format, visit our data repository on <a href="https://github.com/cdli-gh/data" style="display: inline; margin: 0rem;">Github</a>.</span>
                    
                    <?php else: ?>
                        <?php $params['format'] = 'txt'; ?>
                            <?= $this->Html->link(
                                __('Download the list of ' . $entity_name . ' in text format to your computer' . ' <i class="fa fa-download" aria-hidden="true"></i>'),
                                ['controller' => $entity_name, 'action' => 'index', '?' => $params],
                                ['download'=> $entity_name, 'escape' => false],
                                ['class' => 'ml-3']
                            ) ?>
                            <h3 style="color: black;">Flat Data</h3>
                            <?php $params['format'] = 'csv'; ?>
                            <?= $this->Html->link(
                                __('As {0}', 'CSV'),
                                ['controller' => $entity_name, 'action' => 'index', '?' => $params],
                                ['class' => 'ml-3']
                            ) ?>
                            <?php $params['format'] = 'tsv'; ?>
                            <?= $this->Html->link(
                                __('As {0}', 'TSV'),
                                ['controller' => $entity_name, 'action' => 'index', '?' => $params],
                                ['class' => 'ml-3']
                            ) ?>
                            <h3 style="color: black;">Linked Data</h3>
                            <?php $params['format'] = 'ttl'; ?>
                            <?= $this->Html->link(
                                __('As {0}', 'TTL'),
                                ['controller' => $entity_name, 'action' => 'index', '?' => $params],
                                ['class' => 'ml-3']
                            ) ?>
                            <?php $params['format'] = 'rdfjson'; ?>
                            <?= $this->Html->link(
                                __('As {0}', 'RDF/JSON'),
                                ['controller' => $entity_name, 'action' => 'index', '?' => $params],
                                ['class' => 'ml-3']
                            ) ?>
                            <h3 style="color: black;">Expanded Data</h3>
                            <?php $params['format'] = 'json'; ?>
                            <?= $this->Html->link(
                                __('As {0}', 'JSON'),
                                ['controller' => $entity_name, 'action' => 'index', '?' => $params],
                                ['class' => 'ml-3']
                            ) ?>
                            <h3 style="color: black;">Bibliography</h3>
                            <?php $params['format'] = 'bib'; ?>
                            <?= $this->Html->link(
                                __('As {0}', 'BibTex'),
                                ['controller' => $entity_name, 'action' => 'index', '?' => $params],
                                ['class' => 'ml-3']
                            ) ?>
                            <?php $params['format'] = 'ris'; ?>
                            <?= $this->Html->link(
                                __('As {0}', 'RIS'),
                                ['controller' => $entity_name, 'action' => 'index', '?' => $params],
                                ['class' => 'ml-3']
                            ) ?>
                    <?php endif; ?>
                </div>
            </div>
        <?= $this->Dropdown->close() ?>
    </div>

    <?php else: ?>
    <div class="export-grid col-4">
        <?= $this->Dropdown->open('Export ' . $entity_name . ' <i class="fa fa-chevron-down" aria-hidden="true"></i>') ?>
            <div class="row" style="min-width: 15rem;">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <?php $params['format'] = 'txt'; ?>
                    <?= $this->Html->link(
                        __('Download the list of ' . $entity_name . ' in text format to your computer' . ' <i class="fa fa-download" aria-hidden="true"></i>'),
                        ['controller' => $entity_name, 'action' => 'index', '?' => $params],
                        ['download'=> $entity_name, 'escape' => false],
                        ['class' => 'ml-3']
                    ) ?>
                    <h3 style="color: black;">Flat Data</h3>
                    <?php $params['format'] = 'csv'; ?>
                    <?= $this->Html->link(
                        __('As {0}', 'CSV'),
                        ['controller' => $entity_name, 'action' => 'index', '?' => $params],
                        ['class' => 'ml-3']
                    ) ?>
                    <?php $params['format'] = 'tsv'; ?>
                    <?= $this->Html->link(
                        __('As {0}', 'TSV'),
                        ['controller' => $entity_name, 'action' => 'index', '?' => $params],
                        ['class' => 'ml-3']
                    ) ?>
                    <h3 style="color: black;">Linked Data</h3>
                    <?php $params['format'] = 'ttl'; ?>
                    <?= $this->Html->link(
                        __('As {0}', 'TTL'),
                        ['controller' => $entity_name, 'action' => 'index', '?' => $params],
                        ['class' => 'ml-3']
                    ) ?>
                    <?php $params['format'] = 'rdfjson'; ?>
                    <?= $this->Html->link(
                        __('As {0}', 'RDF/JSON'),
                        ['controller' => $entity_name, 'action' => 'index', '?' => $params],
                        ['class' => 'ml-3']
                    ) ?>
                    <h3 style="color: black;">Expanded Data</h3>
                    <?php $params['format'] = 'json'; ?>
                    <?= $this->Html->link(
                        __('As {0}', 'JSON'),
                        ['controller' => $entity_name, 'action' => 'index', '?' => $params],
                        ['class' => 'ml-3']
                    ) ?>                    
                </div>
            </div>
        <?= $this->Dropdown->close() ?>
    </div>
    <?php endif; ?>
