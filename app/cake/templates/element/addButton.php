<?php
use Cake\Utility\Inflector;
?>
<?php if ($access_granted) : ?>
    <?= $this->Html->link('<span class="fa fa-plus fa-2x" aria-hidden="true"></span>', ['prefix' => 'Admin', 'controller' => $this->request->getParam('controller'), 'action' => 'add'], ['escapeTitle' => false, 'class' => 'd-flex d-lg-none floating-btn']) ?>

    <?= $this->Html->link('+ Add '. Inflector::humanize(Inflector::singularize(Inflector::tableize($this->request->getParam('controller')))), ['prefix' => 'Admin', 'controller' => $this->request->getParam('controller'), 'action' => 'add'], ['escapeTitle' => false, 'class' => 'btn btn-primary cdli-btn-blue mb-3 p-2 w-30 d-none d-lg-inline-block']) ?>
<?php endif; ?>