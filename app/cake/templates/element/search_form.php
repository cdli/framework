<?php
use Cake\Utility\Inflector;
?>
<?= $this->Form->create(null, [
    'type' => 'GET',
    'idPrefix' => 'search',
    'valueSources' => 'query',
    'templates' => [],
]) ?>
<div class="mx-0 boxed ads">
    <div class="">
        <?php
        if (!isset($legend)) {
            $legend = __(Inflector::humanize(Inflector::underscore($this->name)) . ' Search');
        }
        ?>
        <legend class="capital-heading"><?= $legend ?></legend>
        <div class="row">
            <?php foreach ($searchFields as $field => $options) : ?>
                <div class="col-md-6">
                    <?= $this->Form->control($field, $options) ?>
                </div>
            <?php endforeach ?>
        </div>
        <div class="row">
            <div class="col-12">
                <?= $this->Form->button('Search', ['class' => 'btn cdli-btn-blue']) ?>
                <?php if ($_isSearch) : ?>
                    <?= $this->Html->link(
                        __('Reset'),
                        $this->request->getPath(),
                        ['class' => 'btn']
                    ) ?>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>
