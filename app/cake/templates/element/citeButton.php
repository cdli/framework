<?php

use Cake\Utility\Inflector;

$displayName = Inflector::tableize($this->request->getParam('controller'));
$displayName = Inflector::humanize(Inflector::singularize($displayName));

if (!isset($data)) {
    $data = $this->Citation->getDefaultData();
} elseif (!is_array($data)) {
    $data = [$data];
}

$formats = [
    'chicago' => [
        'label' => 'Chicago',
        'format' => 'bibliography',
        'options' => [
            'template' => 'chicago-author-date',
            'format' => 'html',
            'hyperlinks' => true
        ]
    ],
    'apa' => [
        'label' => 'APA',
        'format' => 'bibliography',
        'options' => [
            'template' => 'apa',
            'format' => 'html',
            'hyperlinks' => true
        ]
    ],
    'harvard' => [
        'label' => 'Harvard',
        'format' => 'bibliography',
        'options' => [
            'template' => 'harvard-cite-them-right',
            'format' => 'html',
            'hyperlinks' => true
        ]
    ],
    'bibtex' => [
        'label' => 'BibTeX',
        'format' => 'bibtex',
        'options' => []
    ],
    'ris' => [
        'label' => 'RIS',
        'format' => 'ris',
        'options' => []
    ]
];

foreach ($formats as $key => $format) {
    $formats[$key]['output'] = $this->Citation->formatReferenceAsHtml($data, $format['format'], $format['options']);
}
?>

<button class="cite-button" id="cite-btn">
    <img src="/images/cite_logo.svg" alt="Cite-Logo" style="padding-left:1px"/>
    <?php if ($this->request->getParam('action') === 'index'): ?>
        <span class="cite-button-text">Cite this Webpage</span>
    <?php else:?>
        <span class="cite-button-text">Cite this <?= $displayName ?></span>
    <?php endif; ?>
</button>

<!-- Cite Menu -->
<div class="cite-container">
    <div class="cite-menu" id="cite_menu" style="display:none">
        <div class= "cite-menu-heading">
            <span type="text" class="align-center"><b>
                <?php if ($this->request->getParam('action') === 'index'): ?>
                    <legend>Cite this Webpage</legend>
                <?php else:?>
                    <legend>Cite this <?= $displayName ?></legend>
                <?php endif; ?>
            </b></span>
            <label class="cite-menu-close" aria-label="Close" onclick="closeMenu()"></label>
        </div>

        <div class="row d-flex justify-content-between mx-1 mb-0 pb-0 border-0 cite-menu-tabs">
            <?php foreach (array_values($formats) as $index => $format): ?>
                <div class="tabs<?= $index === 0 ? ' active' : '' ?>" id="tab0<?= $index + 1 ?>">
                    <h6 class="text-muted"><?= $format['label'] ?></h6>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="cite-content">
            <div class="cite-content-text" id="sample">
                <?php foreach (array_values($formats) as $index => $format): ?>
                    <fieldset id="tab0<?= $index + 1 ?>1" class="cite-fieldset <?= $index === 0 ? ' show' : '' ?>">
                        <?= $format['output'] ?>
                    </fieldset>
                <?php endforeach; ?>
            </div>

            <div class="p-0.5" style="text-align: center !important;">
                <a class="copy-to-clipboard" href="#" onclick="CopyToClipboardButton('sample');return false;" id="clipboard-text1">
                    <span class="copyBtn fa fa-thin fa-copy" aria-hidden="true"></span>
                    <span>Copy to Clipboard</span>
                </a>
                <span id="clipboard-text2" style="display: none">Copied!</span>
            </div>
        </div>

        <?php if ($this->request->getParam('controller') === 'Articles' && $this->request->getParam('action') === 'view'): ?>
            <div class="row" style="padding-top:15px;">
                <div class="col"><p class="float-left">BibTeX</p></div>
                <div class="col">
                    <?= $this->Html->link(
                        __('Download'),
                        [$article->article_type, $article->getArticleNumberPath(), '_ext' => 'bib'],
                        [
                            'escapeTitle' => false,
                            'class' => 'btn cdli-btn-blue float-right btn-sm',
                            'role' => 'button',
                            'target' => '_blank'
                        ]
                    ) ?>
                </div>
            </div>
            <div class="row">
                <div class="col"><p class="float-left">RIS</p></div>
                <div class="col">
                    <?= $this->Html->link(
                        __('Download'),
                        [$article->article_type, $article->getArticleNumberPath(), '_ext' => 'ris'],
                        [
                            'escapeTitle' => false,
                            'class' => 'btn cdli-btn-blue float-right  btn-sm',
                            'role' => 'button',
                            'target' => '_blank'
                        ]
                    ) ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if ($this->request->getParam('controller') === 'Publications' && $this->request->getParam('action') === 'view'): ?>
            <div class="row" style="padding-top:15px;">
                <div class="col"><p class="float-left">BibTeX</p></div>
                <div class="col">
                    <?= $this->Html->link(
                        __('Download'),
                        ['prefix' => false, 'controller' => 'Publications', 'action' => 'view', $publication->id, '_ext' => 'bib'],
                        [
                            'escapeTitle' => false,
                            'class' => 'btn cdli-btn-blue float-right btn-sm',
                            'role' => 'button',
                            'target' => '_blank'
                        ]
                    ) ?>
                </div>
            </div>
            <div class="row">
                <div class="col"><p class="float-left">RIS</p></div>
                <div class="col">
                    <?= $this->Html->link(
                        __('Download'),
                        ['prefix' => false, 'controller' => 'Publications', 'action' => 'view', $publication->id, '_ext' => 'ris'],
                        [
                            'escapeTitle' => false,
                            'class' => 'btn cdli-btn-blue float-right btn-sm',
                            'role' => 'button',
                            'target' => '_blank'
                        ]
                    ) ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>

<script>
const targetDiv = document.getElementById('cite_menu');
const btn = document.getElementById('cite-btn');
btn.onclick = function () {
    if (targetDiv.style.display === 'none') {
        targetDiv.style.display = 'block';
    } else {
        targetDiv.style.display = 'none';
    }
};

$(document).ready(function(){
    $('.tabs').click(function(){
        $('.tabs').removeClass('active');
        $('.tabs h6').removeClass('font-weight-bold');
        $('.tabs h6').addClass('text-muted');
        $(this).children('h6').removeClass('text-muted');
        $(this).children('h6').addClass('font-weight-bold');
        $(this).addClass('active');
        var current_fs = $('.active');
        var next_fs = $(this).attr('id');
        var next_fs = '#' + next_fs + '1';
        $('fieldset').removeClass('show');
        $(next_fs).addClass('show');
        current_fs.animate({}, {
            step: function() {
                current_fs.css({
                    'display': 'none',
                    'position': 'relative'
                });
                next_fs.css({
                    'display': 'block'
                });
            }
        });
    });
});

function CopyToClipboardButton(id) {
    var r = document.createRange();
    r.selectNode(document.getElementById(id));
    window.getSelection().removeAllRanges();
    window.getSelection().addRange(r);
    document.execCommand('copy');
    window.getSelection().removeAllRanges();

    document.getElementById('clipboard-text1').style.display = 'none';
    document.getElementById('clipboard-text2').style.display = 'inline';
    setTimeout(reverse, 1000);
}

function reverse() {
    document.getElementById('clipboard-text1').style.display = 'inline';
    document.getElementById('clipboard-text2').style.display = 'none';
}

function closeMenu() {
    if(document.getElementById('cite_menu').style.display === 'block') {
        document.getElementById('cite_menu').style.display ='none';
    }
}

$(window).scroll(function() {
    if ($(window).scrollTop() + $(window).height() > ($(document).height() - 500)) {
        document.getElementById('cite-btn').style.display='none';
        document.getElementById('cite_menu').style.display='none';
    } else {
        document.getElementById('cite-btn').style.display='inline-flex';
    }
});
</script>

<noscript>
  <style>
    .cite-button {
      display: none;
    }
    .cite-container {
      display: none;
    }
    </style>
</noscript>
