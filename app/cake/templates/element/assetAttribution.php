<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactAsset $asset
 * @var \App\Model\Entity\Collection $collection
 */

$attribution = $this->ArtifactAssets->getAttributionInfo($asset, $collection ?? null);
?>

&copy; <?= $attribution['attribution'] ?>

<?php if (!is_null($attribution['license'])): ?>
    (<?= $attribution['license'] ?>)
<?php endif; ?>

<?php if (!is_null($attribution['comment'])): ?>
    <?= $this->Html->link(__('More info'), [
        'prefix' => false,
        'controller' => 'ArtifactAssets',
        'action' => 'view',
        $asset->id,
        '#' => 'copyright',
    ]) ?>.
<?php endif; ?>
