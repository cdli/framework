<?php
use Cake\Utility\Inflector;

?>

<button type="button" class="btn btn-danger float-right" data-toggle="modal" data-target="#confirmModal">
          Delete
</button>

<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="confirmModalLabel">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Are you sure you want to delete <?php echo h(Inflector::humanize(Inflector::singularize(Inflector::tableize($this->request->getParam('controller'))))).' '.'<b>'.h($entity_name).'</b>'.'?' ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <?= $this->Form->postLink(__('Delete'), ['prefix' => 'Admin', 'action' => 'delete', $id], ['class' => 'btn btn-danger float-right'])?>
      </div>
    </div>
  </div>
</div>
