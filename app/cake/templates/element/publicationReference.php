<?php

$append = '';

if ($publication->has('designation')) {
    $append .= $publication->designation . ' ';
}

if (!empty($publication->_joinData) && !empty($publication->_joinData->exact_reference)) {
    $append .= $publication->_joinData->exact_reference . ' ';
}

$bibtexkey = $this->Html->link(
    h($publication->bibtexkey ?? 'Details'),
    ['prefix' => false, 'controller' => 'Publications', 'action' => 'view', $publication->id]
);

if (empty($append)) {
    $append .= $bibtexkey;
} else {
    $append .= '(' . $bibtexkey . ')';
}

?>

<p>
    <?= $this->Citation->formatReferenceAsHtml($publication, 'bibliography', [
        'template' => 'chicago-author-date',
        'format' => 'html',
        'hyperlinks' => true,
        'append' => ' [' . $append . ']'
    ]) ?>
</p>

<?php if (!empty($publication->_joinData) && (!empty($publication->_joinData->publication_type) || !empty($publication->_joinData->publication_comments))): ?>
    <p class="mb-5">
        <b><?= __($publication->_joinData->publication_type ?? 'unknown publication type') ?></b>

        <?php if (!empty($publication->_joinData->publication_comments)): ?>
            &mdash;
            <i><?= h($publication->_joinData->publication_comments) ?></i>
        <?php endif; ?>
    </p>
<?php endif; ?>
