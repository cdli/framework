<?php

use Cake\Utility\Inflector;

if (!isset($data)) {
    $data = $this->Citation->getDefaultData();
} elseif (!is_array($data)) {
    $data = [$data];
}

$formats = [
    'chicago' => [
        'label' => 'Chicago',
        'format' => 'bibliography',
        'options' => [
            'template' => 'chicago-author-date',
            'format' => 'html',
            'hyperlinks' => true
        ]
    ],
    'apa' => [
        'label' => 'APA',
        'format' => 'bibliography',
        'options' => [
            'template' => 'apa',
            'format' => 'html',
            'hyperlinks' => true
        ]
    ],
    'harvard' => [
        'label' => 'Harvard',
        'format' => 'bibliography',
        'options' => [
            'template' => 'harvard-cite-them-right',
            'format' => 'html',
            'hyperlinks' => true
        ]
    ],
    'bibtex' => [
        'label' => 'BibTeX',
        'format' => 'bibtex',
        'options' => []
    ],
    'ris' => [
        'label' => 'RIS',
        'format' => 'ris',
        'options' => []
    ]
];

foreach ($formats as $key => $format) {
    $formats[$key]['output'] = $this->Citation->formatReferenceAsHtml($data, $format['format'], $format['options']);
}
?>

<div class="single-entity-wrapper">
    <h2 class="capital-heading">
        <?php if ($this->request->getParam('action') === 'index'): ?>
            Cite this Webpage
        <?php else:?>
            Cite this <?= Inflector::humanize(Inflector::singularize(Inflector::tableize($this->request->getParam('controller')))) ?>
        <?php endif; ?>
    </h2>

    <button class="btn cdli-btn-blue float-right copy-button" data-for="cite-tabs">
        <span class="fa fa-thin fa-copy" aria-hidden="true"></span>
        <span class="sr-only">Copy</span>
    </button>

    <?php if ($this->request->getParam('controller') === 'Articles' && $this->request->getParam('action') == 'view'): ?>
        <button id="btnGroupDrop1" type="button" class="d-inline p-2 btn btn-sm cdli-btn-blue mx-3 float-right dropdown-toggle cite-download-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Download
        </button>
        <button id="btnGroupDrop1" type="button" class="d-inline p-2 btn btn-sm mx-3 float-right cite-download-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-download" aria-hidden="true" style="font-size: 15px;"></i>
        </button>

        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
            <?= $this->Html->link(
                __('BibTeX'),
                [$article->article_type, $article->getArticleNumberPath(), '_ext' => 'bib'],
                [
                    'escapeTitle' => false,
                    'class' => 'dropdown-item',
                    'role' => 'button',
                    'target' => '_blank'
                ]
            ) ?>
            <?= $this->Html->link(
                __('RIS'),
                [$article->article_type, $article->getArticleNumberPath(), '_ext' => 'ris'],
                [
                    'escapeTitle' => false,
                    'class' => 'dropdown-item',
                    'role' => 'button',
                    'target' => '_blank'
                ]
            ) ?>
        </div>
    <?php endif; ?>

    <?php if ($this->request->getParam('controller') === 'Publications' && $this->request->getParam('action') == 'view'): ?>
        <button id="btnGroupDrop1" type="button" class="d-inline p-2 btn btn-sm cdli-btn-blue mx-3 float-right dropdown-toggle cite-download-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Download
        </button>
        <button id="btnGroupDrop1" type="button" class="d-inline p-2 btn btn-sm mx-3 float-right cite-download-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-download" aria-hidden="true" style="font-size: 15px;"></i>
        </button>

        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
            <?= $this->Html->link(
                __('BibTeX'),
                ['prefix' => false, 'controller' => 'Publications', 'action' => 'view', $publication->id, '_ext' => 'bib'],
                [
                    'escapeTitle' => false,
                    'class' => 'dropdown-item',
                    'role' => 'button',
                    'target' => '_blank'
                ]
            ) ?>
            <?= $this->Html->link(
                __('RIS'),
                ['prefix' => false, 'controller' => 'Publications', 'action' => 'view', $publication->id, '_ext' => 'ris'],
                [
                    'escapeTitle' => false,
                    'class' => 'dropdown-item',
                    'role' => 'button',
                    'target' => '_blank'
                ]
            ) ?>
        </div>
    <?php endif; ?>

    <div class="tabset">
        <?php foreach ($formats as $key => $format): ?>
            <input type="radio"
                name="tabset"
                id="cite-<?= $key ?>-tab"
                aria-controls="cite-<?= $key ?>"
                <?= $key === 'chicago' ? ' checked' : '' ?>>
            <label for="cite-<?= $key ?>-tab" class="pull-left prop ml-2"><?= $format['label'] ?></label>
        <?php endforeach; ?>

        <div class="tab-panels" id="cite-tabs" style="padding: 5px;">
            <?php foreach ($formats as $key => $format): ?>
                <section id="cite-<?= $key ?>" class="tab-panel cite-bottom-color">
                    <?= $format['output'] ?>
                </section>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<script>
$('.copy-button').click(function () {
    var button = $(this)

    var range = document.createRange()
    range.selectNode(document.getElementById(button.attr('data-for')))
    window.getSelection().removeAllRanges()
    window.getSelection().addRange(range)
    document.execCommand('copy')
    window.getSelection().removeAllRanges()

    button.tooltip({ content: 'Copied!' }).tooltip('show')
    setTimeout(function() {
        button.tooltip('hide')
    }, 1000)
})
</script>
