<?php
$searchCategory = [
    'keyword' => 'Free Search',
    'publication' => 'Publications',
    'collection' => 'Collections',
    'provenience' => 'Proveniences',
    'period' => 'Periods',
    'transliteration' => 'Transliterations',
    'translation' => 'Translations',
    'id' => 'Identification numbers'
];
?>

<?= $this->Form->create(null, ['type' => 'GET', 'url' => ['controller' => 'Search', 'action' => 'index']]) ?>
    <?= $this->Form->hideQueryParameters($this->getRequest()->getUri()->getQuery(), '/^simple-(op|value|field)\[\d*\]$/') ?>

    <div class="row m-0">
        <?= $this->Form->control('simple-value[]', [
            'class' => 'form-control w-100',
            'templates' => [
                // TODO move to SCSS
                'inputContainer' => '<div class="input {{type}}{{required}} m-2" style="flex-grow: 1;">{{content}}</div>'
            ],
            'label' => ['class' => 'sr-only'],
            'id' => 'simple-value-0',
            'val' => array_key_exists('simple-value', $query) ? $query['simple-value'][0] : ''
        ]) ?>

        <?= $this->Form->control('simple-field[]', [
            'class' => 'form-control m-2',
            'type' => 'select',
            'label' => ['class' => 'sr-only'],
            'options' => $searchCategory,
            'id' => 'simple-field-0',
            'val' => array_key_exists('simple-field', $query) ? $query['simple-field'][0] : null
        ]) ?>
    </div>

    <?php if (array_key_exists('simple-op', $query)): ?>
        <?php foreach ($query['simple-op'] as $index => $operator): ?>
            <div class="row m-0">
                <?= $this->Form->control('simple-op[]', [
                    'class' => 'form-control m-2',
                    'type' => 'select',
                    'label' => ['class' => 'sr-only'],
                    'options' => ['AND' => 'and', 'OR' => 'or'],
                    'id' => 'simple-op-' . ($index + 1),
                    'val' => $operator
                ]) ?>
                <?= $this->Form->control('simple-value[]', [
                    'class' => 'form-control w-100',
                    'templates' => [
                        // TODO move to SCSS
                        'inputContainer' => '<div class="input {{type}}{{required}} m-2" style="flex-grow: 1;">{{content}}</div>'
                    ],
                    'label' => ['class' => 'sr-only'],
                    'id' => 'simple-value-' . ($index + 1),
                    'val' => $query['simple-value'][$index + 1]
                ]) ?>
                <?= $this->Form->control('simple-field[]', [
                    'class' => 'form-control m-2',
                    'type' => 'select',
                    'label' => ['class' => 'sr-only'],
                    'options' => $searchCategory,
                    'id' => 'simple-field-' . ($index + 1),
                    'val' => $query['simple-field'][$index + 1]
                ]) ?>

                <?= $this->Form->button('<span class="fa fa-close" aria-hidden="true"></span><span class="sr-only">Remove field</span>', [
                    'class' => 'btn cdli-btn-light m-2 search-simple-remove-field',
                    'type' => 'button',
                    'escapeTitle' => false
                ]) ?>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>

    <div class="mt-2 mx-2">
        <?= $this->Form->button(__('Search') . ' <span class="fa fa-search" aria-hidden="true"></span>', [
            'class' => 'btn cdli-btn-blue mr-3',
            'type' => 'submit',
            'escapeTitle' => false
        ]) ?>

        <?= $this->Form->button(__('Add search field'), [
            'id' => 'search-simple-add-field',
            'class' => 'btn cdli-btn-light',
            'type' => 'button'
        ]) ?>

        <div class="float-right">
            <?= $this->Html->link(
                __('Search settings'),
                ['controller' => 'Search', 'action' => 'view', 'settings'],
                ['class' => 'btn']
            ) ?>

            <?= $this->Html->link(
                __('Advanced search'),
                ['controller' => 'Search', 'action' => 'view', 'advanced'],
                ['class' => 'btn']
            ) ?>
        </div>
    </div>
<?= $this->Form->end() ?>

<?php $this->append('script'); ?>
<script type="text/javascript">
    $(function () {
        var template = <?= json_encode(implode('', [
            '<div class="row mx-0">',
            $this->Form->control('simple-op[]', [
                'class' => 'form-control m-2',
                'type' => 'select',
                'label' => ['class' => 'sr-only'],
                'options' => ['AND' => 'and', 'OR' => 'or'],
                'id' => 'simple-op-0',
                'val' => 'AND'
            ]),
            $this->Form->control('simple-value[]', [
                'class' => 'form-control w-100',
                'templates' => [
                    // TODO move to SCSS
                    'inputContainer' => '<div class="input {{type}}{{required}} m-2" style="flex-grow: 1;">{{content}}</div>'
                ],
                'label' => ['class' => 'sr-only'],
                'id' => 'simple-value-0'
            ]),
            $this->Form->control('simple-field[]', [
                'class' => 'form-control m-2',
                'type' => 'select',
                'label' => ['class' => 'sr-only'],
                'options' => $searchCategory,
                'id' => 'simple-field-0',
                'val' => 'keyword'
            ]),
            $this->Form->button('<span class="fa fa-close" aria-hidden="true"></span><span class="sr-only">Remove field</span>', [
                'class' => 'btn cdli-btn-light m-2 search-simple-remove-field',
                'type' => 'button',
                'escapeTitle' => false
            ]),
            '</div>'
        ])) ?>

        $('#search-simple-add-field').click(function (e) {
            e.preventDefault()
            var fields = $.parseHTML(template)
            var lastId = $(this).parent().siblings('.row').last().find('input').attr('id').match(/\d+$/)[0]
            var id = parseInt(lastId) + 1

            $(fields).find('input, select').each(function (_, input) {
                $(input).attr('id', $(input).attr('id').replace(/\d+$/, id))
            })
            $(fields).find('label').each(function (_, label) {
                $(label).attr('for', $(label).attr('for').replace(/\d+$/, id))
            })
            $(fields).find('.search-simple-remove-field').click(removeField)

            $(this).parent().before(fields)
        })

        $('.search-simple-remove-field').click(removeField)

        function removeField (e) {
            e.preventDefault()
            $(this).parent().remove()
        }
    })
</script>
<?php $this->end(); ?>
