<table class="table">
    <tbody>
        <?php foreach ($new->getChanged() as $field): ?>
            <?php if ($new->get($field) === '' && !($old->has($field) && $old->get($field) !== '')): ?>
                <?php continue; ?>
            <?php endif; ?>
            <tr>
                <th><?= h($field) ?></th>
                <td>
                    <?php if ($new->get($field) !== ''): ?>
                        <ins><?= h($new->get($field)) ?></ins>
                    <?php endif; ?>

                    <?php if ($old->has($field) && $old->get($field) !== ''): ?>
                        <del><?= h($old->get($field)) ?></del>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
