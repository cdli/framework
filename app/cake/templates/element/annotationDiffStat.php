<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UpdateEvent $updateEvent
 * @var \App\Model\Entity\ArtifactAssetAnnotation[] $annotations
 */

use \App\Utility\Svg\Annotation as SvgAnnotation;
?>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <th><?= h('Artifact') ?></th>
            <th><?= h('Annotation area') ?></th>
            <th><?= h('Annotation body purpose') ?></th>
            <th><?= h('Annotation body value') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($updateEvent->artifact_asset_annotations ?? $annotations as $i => $annotation): ?>
            <?php $rowspan = $annotation->is_active && count($annotation->bodies) > 0 ? count($annotation->bodies) : 1; ?>
            <tr>
                <td rowspan="<?= $rowspan ?>">
                    <figure>
                        <?= $this->Html->image(DS . $annotation->artifact_asset->getPath(), [
                            'alt' => 'Annotated image',
                            'class' => 'm-2',
                            'style' => 'max-width: 100px; max-height: 100px;'
                        ]) ?>
                        <figcaption>
                            <?= h($annotation->artifact_asset->getDescription()) ?>
                            of
                            <?= $this->Html->link(
                                $annotation->artifact_asset->artifact->getCdliNumber(),
                                ['controller' => 'Artifacts', 'action' => 'view', $annotation->artifact_asset->artifact->id]
                            ) ?>
                        </figcaption>
                    </figure>
                </td>
                <td rowspan="<?= $rowspan ?>">
                    <?= SvgAnnotation::getCutoutSvg($annotation, 100, "annotation-cutout-$i") ?>
                    <br>
                    <?= h($annotation->id) ?>
                </td>
                <?php if ($annotation->is_active): ?>
                    <?php if (count($annotation->bodies) === 0): ?>
                        <td></td>
                    <?php endif; ?>
                    <?php foreach ($annotation->bodies as $j => $body): ?>
                        <?php if ($j >= 1): ?>
                            </tr>
                            <tr>
                        <?php endif; ?>
                            <td><?= $annotation->bodies[$j]->purpose ?? '' ?></td>
                            <td>
                                <?php if ($annotation->bodies[$j]->has('source')): ?>
                                    <?= $this->Html->link(
                                        $annotation->bodies[$j]->value ?? $annotation->bodies[$j]->source,
                                        $annotation->bodies[$j]->source
                                    ) ?>
                                <?php else: ?>
                                    <?= $annotation->bodies[$j]->value ?? '' ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <td colspan="2">
                        <?= __('Deactivated') ?>
                    </td>
                <?php endif; ?>
        <?php endforeach; ?>
    </tbody>
</table>
