<?= $this->Form->create(null, ['url' => ['controller' => 'Home', 'action' => 'hideCookiesNotification']]) ?>
<div id="cookies-notification" class="bg-light pt-3 pb-3 fixed-bottom border-top border-secondary">
    <div class="container lead">
        <div class="row pt-5 pb-5">
            <div class="col page-summary-text pb-3">
                This website uses essential cookies that are necessary for it to work properly. These cookies are enabled by default.
            </div>
            <div class="col-lg-auto text-center">
                <button type="submit" class="btn cdli-btn-blue" id="cookies-close"><?= __('I understand') ?></button>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>

<script>
$('#cookies-close').on('click', function (e) {
    setCookie('hideCookiesNotification', 1, 1);
    $('#cookies-notification').fadeOut();
    e.preventDefault();
    return false;
});
</script>
