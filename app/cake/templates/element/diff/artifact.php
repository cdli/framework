<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UpdateEvent $updateEvent
 */

use \App\Model\Entity\ArtifactsUpdate;

$changedColumns = [];
foreach ($updateEvent->artifacts_updates as $update) {
    foreach ($update->getChanged() as $key) {
        $changedColumns[$key] = true;
    }
}
?>

<div class="table-responsive">
    <table class="table-bootstrap">
        <thead>
            <tr>
                <th><?= __('Artifact') ?></th>
                <?php foreach ($changedColumns as $key => $value): ?>
                    <th><?= h($key) ?></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($updateEvent->artifacts_updates as $update): ?>
                <?php $old = is_null($update->artifact) ? [] : $update->artifact->getFlatData(); ?>
                <tr>
                    <td><?= h($update->getDesignation()) ?></td>
                    <?php foreach ($changedColumns as $field => $value): ?>
                        <?php if ($field != 'artifact'): ?>
                            <?php if ($update->has($field)): ?>
                                <?php $value = $update->get($field); ?>
                                <?php if ($value === '' && array_key_exists($field, $old)): ?>
                                    <td><del><?= h($old[$field]) ?></del></td>
                                <?php elseif (!array_key_exists($field, $old) || is_null($old[$field]) || $old[$field] === ''): ?>
                                    <td><ins><?= h($value) ?></ins></td>
                                <?php elseif ($update->isArrayField($field)): ?>
                                    <td>
                                        <?= $this->Diff->diffArrays(
                                            ArtifactsUpdate::splitField($old[$field]),
                                            ArtifactsUpdate::splitField($value),
                                            '; '
                                        ) ?>
                                    </td>
                                <?php else: ?>
                                    <td>
                                        <del><?= h($old[$field]) ?></del>
                                        <ins><?= h($value) ?></ins>
                                    </td>
                                <?php endif; ?>
                            <?php else: ?>
                                <td></td>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
