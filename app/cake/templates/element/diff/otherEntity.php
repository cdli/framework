<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UpdateEvent $updateEvent
 */

use Cake\Utility\Inflector;
?>

<table class="table-bootstrap">
    <thead>
        <tr>
            <th><?= __('Entity') ?></th>
            <th><?= __('Revision') ?></th>
            <th><?= __('Changes') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($updateEvent->entities_updates ?? [] as $entitiesUpdate): ?>
            <tr>
                <td>
                    <?php if ($entitiesUpdate->has('entity_id')): ?>
                        <?php
                            try {
                                $entity = $entitiesUpdate->getEntity();
                                $entityName = $entity->get($entitiesUpdate->getTable()->getDisplayField());
                                if ($entitiesUpdate->entity_table === 'publications' && empty($entityName)) {
                                    $entityName = $entity->designation ?? $entity->bibtexkey;
                                }
                                $entityName = empty($entityName) ? '' : '"' . $entityName . '"';
                            } catch (\Exception $e) {
                                $entity = null;
                                $entityName = '(deleted)';
                            }
                        ?>
                        <?= $this->Html->link(
                            Inflector::singularize(Inflector::humanize($entitiesUpdate->entity_table)) . ' ' . $entityName,
                            [
                                'controller' => Inflector::camelize($entitiesUpdate->entity_table),
                                'action' => 'view',
                                $entitiesUpdate->entity_id
                            ]
                        ) ?>
                    <?php else: ?>
                        <?= h(Inflector::singularize(Inflector::humanize($entitiesUpdate->entity_table))) ?>
                        (<?= __('New') ?>)
                    <?php endif; ?>
                </td>
                <td>
                    <?php if (!$entitiesUpdate->isNew()): ?>
                        <?= $this->Html->link($entitiesUpdate->id, [
                            'controller' => 'EntitiesUpdates',
                            'action' => 'view',
                            $entitiesUpdate->id,
                        ]) ?>
                    <?php endif; ?>
                </td>
                <td>
                    <?= $this->element('diff/otherEntityUpdate', ['entitiesUpdate' => $entitiesUpdate]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
