<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\EntitiesUpdate|null $entitiesUpdate
 * @var \Cake\ORM\Table|null $table
 * @var array|null $update
 */

if (isset($entitiesUpdate) && !isset($update)) {
    $table = $entitiesUpdate->getTable();
    $update = $entitiesUpdate->getUpdate();
}

if (is_null($update)) {
    echo '<del>[' . __('Entity deleted') . ']</del>';
    return;
}

$primaryKey = $table->getPrimaryKey();
?>

<?php if (count($update) === 2 && count(array_diff(array_keys($update), [$primaryKey, '_joinData'])) === 0): ?>
    <span class="badge badge-success" title="<?= h($update[$primaryKey]) ?>">
        <?= h($table->getDisplayField() ? $table->get($update[$primaryKey])->get($table->getDisplayField()) : $update[$primaryKey]) ?>
    </span>
    <?= $this->element('diff/otherEntityUpdate', [
        'table' => $table,
        'update' => $update['_joinData'],
    ]) ?>
<?php else: ?>
    <table class="table" style="width:auto;">
        <tbody>
            <?php foreach ($update as $key => $value): ?>
                <?php $association = $table->associations()->getByProperty($key); ?>
                <tr>
                    <th><?= h(is_numeric($key) ? $key + 1 : $key) ?></th>
                    <td>
                        <?php if (is_null($value)): ?>
                            <del>&mdash;</del>
                        <?php elseif (is_array($value)): ?>
                            <?= $this->element('diff/otherEntityUpdate', [
                                'table' => $association ? $association->getTarget() : $table,
                                'update' => $value,
                            ]) ?>
                        <?php elseif ($key === $primaryKey): ?>
                            <span class="badge badge-success" title="<?= h($value) ?>">
                                <?= h($table->getDisplayField() ? $table->get($value)->get($table->getDisplayField()) : $value) ?>
                            </span>
                        <?php elseif ($association): ?>
                            <?php $displayField = $table->getTarget()->getDisplayField(); ?>
                            <span class="badge badge-success" title="<?= h($value) ?>">
                                <?= h($displayField ? $table->getTarget()->get($value)->get($displayField) : $value) ?>
                            </span>
                        <?php else: ?>
                            <ins><?= h($value) ?></ins>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>
