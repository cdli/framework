<?php
use App\Model\Entity\Artifact;
use App\Model\Entity\ArtifactsUpdate;
use App\Model\Entity\Inscription;

$artifacts = [];

foreach ($updates as $update) {
    if ($update instanceof ArtifactsUpdate) {
        $id = $update->artifact_id ? Artifact::formatCdliNumber($update->artifact_id) : __('new artifacts');
    } elseif ($update instanceof Inscription) {
        $id = Artifact::formatCdliNumber($update->artifact_id);
    } else {
        $id = __('other entities');
    }
    $artifacts[$id] = ($artifacts[$id] ?? 0) + 1;
}
?>

<?php if (count($artifacts) > 10): ?>
    <?= __('{0,plural,=1{1 change} other{# changes}} to {1}', count($updates), __('various entities')) ?>
<?php else: ?>
    <ul class="mb-0">
        <?php foreach ($artifacts as $artifact => $count): ?>
            <li><?= __('{0,plural,=1{1 change} other{# changes}} to {1}', $count, $artifact) ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
