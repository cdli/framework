<?php
/**
 * @var int $currentState
 */

$states = [
    [
        'label' => __('Confirm'),
    ],
    [
        'label' => __('Details'),
    ],
    [
        'label' => __('Done'),
    ]
];
?>

<div class="row justify-content-center mb-3">
    <div class="col-lg-6 col-md-9">
        <div class="progress-banner">
            <div class="progress-banner-node progress-banner-completed">
                <span class="progress-banner-step">1</span>
                <span class="progress-banner-label"><?= __('Edit') ?></span>
            </div>

            <?php foreach ($states as $i => $state): ?>
                <?php $class = $i < $currentState ? 'progress-banner-completed' : ''; ?>
                <div class="progress-banner-line <?= $class ?>"></div>
                <div class="progress-banner-node <?= $class ?>">
                    <span class="progress-banner-step"><?= $i + 2 ?></span>
                    <span class="progress-banner-label"><?= $state['label'] ?></span>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
