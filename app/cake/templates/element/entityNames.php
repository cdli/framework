<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\ORM\Entity $entity
 */
?>

<?php if (is_array($entity->entities_names) && count($entity->entities_names)): ?>
    <table class="table-bootstrap">
        <thead>
            <tr>
                <th><?= __('Name') ?></th>
                <th><?= __('Language') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($entity->entities_names as $name): ?>
                <tr>
                    <td><?= h($name->name) ?></td>
                    <td><?= h($name->language_ietf) ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>
