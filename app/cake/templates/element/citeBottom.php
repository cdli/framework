<?php

use Cake\Utility\Inflector;

$displayName = Inflector::tableize($this->request->getParam('controller'));
$displayName = Inflector::humanize(Inflector::singularize($displayName));

if (!isset($data)) {
    $data = $this->Citation->getDefaultData();
} elseif (!is_array($data)) {
    $data = [$data];
}

$formats = [
    'chicago' => [
        'label' => 'Chicago',
        'format' => 'bibliography',
        'options' => [
            'template' => 'chicago-author-date',
            'format' => 'html',
            'hyperlinks' => true
        ]
    ],
    'apa' => [
        'label' => 'APA',
        'format' => 'bibliography',
        'options' => [
            'template' => 'apa',
            'format' => 'html',
            'hyperlinks' => true
        ]
    ],
    'harvard' => [
        'label' => 'Harvard',
        'format' => 'bibliography',
        'options' => [
            'template' => 'harvard-cite-them-right',
            'format' => 'html',
            'hyperlinks' => true
        ]
    ],
    'bibtex' => [
        'label' => 'BibTeX',
        'format' => 'bibtex',
        'options' => []
    ],
    'ris' => [
        'label' => 'RIS',
        'format' => 'ris',
        'options' => []
    ]
];

foreach ($formats as $key => $format) {
    $formats[$key]['output'] = $this->Citation->formatReferenceAsHtml($data, $format['format'], $format['options']);
}
?>

<div class="cite-bottom text-left">
    <fieldset>
        <?php if ($this->request->getParam('action') === 'index'): ?>
            <legend>Cite this Webpage</legend>
        <?php else:?>
            <legend>Cite this <?= $displayName ?></legend>
        <?php endif; ?>

        <button class="pull-right copy-clipbaord-btn-bottom" onclick="CopyToClipboardBottom('sample1');return false;" id="copy-button-bottom">
            <span class="copyBtn fa fa-thin fa-copy" aria-hidden="true"></span>
        </button>

        <?php if ($this->request->getParam('controller') === 'Articles' && $this->request->getParam('action') == 'view'): ?>
            <button id="btnGroupDrop1" type="button" class="d-inline p-2 btn btn-sm cdli-btn-blue mx-3 float-right dropdown-toggle cite-download-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Download
            </button>
            <button id="btnGroupDrop1" type="button" class="d-inline p-2 btn btn-sm mx-3 float-right cite-download-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-download" aria-hidden="true" style="font-size: 15px;"></i>
            </button>

            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                <?= $this->Html->link(
                    __('BibTeX'),
                    [$article->article_type, $article->getArticleNumberPath(), '_ext' => 'bib'],
                    [
                        'escapeTitle' => false,
                        'class' => 'dropdown-item',
                        'role' => 'button',
                        'target' => '_blank'
                    ]
                ) ?>
                <?= $this->Html->link(
                    __('RIS'),
                    [$article->article_type, $article->getArticleNumberPath(), '_ext' => 'ris'],
                    [
                        'escapeTitle' => false,
                        'class' => 'dropdown-item',
                        'role' => 'button',
                        'target' => '_blank'
                    ]
                ) ?>
            </div>
        <?php endif; ?>

        <?php if ($this->request->getParam('controller') === 'Publications' && $this->request->getParam('action') == 'view'): ?>
            <button id="btnGroupDrop1" type="button" class="d-inline p-2 btn btn-sm cdli-btn-blue mx-3 float-right dropdown-toggle cite-download-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Download
            </button>
            <button id="btnGroupDrop1" type="button" class="d-inline p-2 btn btn-sm mx-3 float-right cite-download-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-download" aria-hidden="true" style="font-size: 15px;"></i>
            </button>

            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                <?= $this->Html->link(
                    __('BibTeX'),
                    ['prefix' => false, 'controller' => 'Publications', 'action' => 'view', $publication->id, '_ext' => 'bib'],
                    [
                        'escapeTitle' => false,
                        'class' => 'dropdown-item',
                        'role' => 'button',
                        'target' => '_blank'
                    ]
                ) ?>
                <?= $this->Html->link(
                    __('RIS'),
                    ['prefix' => false, 'controller' => 'Publications', 'action' => 'view', $publication->id, '_ext' => 'ris'],
                    [
                        'escapeTitle' => false,
                        'class' => 'dropdown-item',
                        'role' => 'button',
                        'target' => '_blank'
                    ]
                ) ?>
            </div>
        <?php endif; ?>

        <div class="cite-bottom-tabs">
            <div class="tabset">
                <?php foreach ($formats as $key => $format): ?>
                    <input type="radio"
                        name="tabset"
                        id="cite-<?= $key ?>-tab"
                        aria-controls="cite-<?= $key ?>"
                        <?= $key === 'chicago' ? ' checked' : '' ?>>
                    <label for="cite-<?= $key ?>-tab" class="pull-left prop ml-2"><?= $format['label'] ?></label>
                <?php endforeach; ?>

                <div class="tab-panels" id="sample1" style="padding: 5px;">
                    <?php foreach ($formats as $key => $format): ?>
                        <section id="cite-<?= $key ?>" class="tab-panel cite-bottom-color">
                            <?= $format['output'] ?>
                        </section>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </fieldset>
</div>


<script>
function CopyToClipboardBottom(id) {
    var r = document.createRange();
    r.selectNode(document.getElementById(id));
    window.getSelection().removeAllRanges();
    window.getSelection().addRange(r);
    var x = document.execCommand('copy');
    window.getSelection().removeAllRanges();
    setTooltip('Copied!');
    hideTooltip();
}

$('#copy-button-bottom').tooltip({
    trigger: 'click',
    placement: 'right'
});

function setTooltip(message) {
    $('#copy-button-bottom').tooltip('hide')
        .attr('data-original-title', message)
        .tooltip('show');
}

function hideTooltip() {
    setTimeout(function() {
        $('#copy-button-bottom').tooltip('hide');
    }, 1000);
}
</script>

<noscript>
    <style>
        .copy-clipbaord-btn-bottom {
            display: none;
        }
    </style>
</noscript>
