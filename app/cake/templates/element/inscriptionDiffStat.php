<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr align="left">
            <th scope="col"><?= h('Artifact') ?></th>
            <th scope="col"><?= h('Revision') ?></th>
            <th scope="col"><?= h('Changes') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($updateEvent->inscriptions as $update): ?>
            <tr align="left">
                <td><?= $this->Html->link(
                    h($update->artifact->designation . ' (' . $update->artifact->getCdliNumber() . ')'),
                    ['controller' => 'Artifacts', 'action' => 'view', $update->artifact->id]
                ) ?></td>
                <td><?= $this->Html->link(
                    h($update->id),
                    ['controller' => 'Inscriptions', 'action' => 'view', $update->id]
                ) ?></td>
                <td>
                    <?php if (in_array('atf', $updateEvent->update_type)): ?>
                        <?php if ($update->hasAtfWarnings()): ?>
                            <?php foreach ($update->getAtfWarnings() as $warning): ?>
                                <div class="alert alert-<?= h($warning[0]) ?>">
                                    <?= __('ATF') ?>: <?= h($warning[1]) ?>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php if ($update->hasTokenWarnings()): ?>
                            <?php foreach ($update->getTokenWarnings() as $warning): ?>
                                <div class="alert alert-<?= h($warning[0]) ?>">
                                    <?= h($warning[1]) ?>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>

                        <pre><?=
                            $this->Diff->diffInscription($update->artifact->inscription, $update, 'atf')
                        ?></pre>
                    <?php endif; ?>

                    <?php if (in_array('annotation', $updateEvent->update_type)): ?>
                        <?php if ($update->hasAnnotationWarnings()): ?>
                            <?php foreach ($update->getAnnotationWarnings() as $warning): ?>
                                <div class="alert alert-<?= h($warning[0]) ?>">
                                    <?= h($warning[1]) ?>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>

                        <pre><?=
                            $this->Diff->diffInscription($update->artifact->inscription, $update, 'annotation')
                        ?></pre>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
