<?php
/**
* @var \App\View\AppView $this
* @var \App\Model\Entity\Artifact $artifact
*/
?>

<div class="export-grid d-inline-block">
    <?= $this->Dropdown->open('Export artifact <i class="fa fa-chevron-down" aria-hidden="true"></i>') ?>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <h3>Metadata / catalogue</h3>
                <h4 class="ml-4">Flat catalogue</h4>
                <?= $this->Html->link(
                    __('As {0}', 'CSV'),
                    ['controller' => 'Artifacts', 'action' => 'view', 'id' => $artifact->id, 'format' => 'csv'],
                    ['class' => 'ml-5']
                ) ?>
                <?= $this->Html->link(
                    __('As {0}', 'TSV'),
                    ['controller' => 'Artifacts', 'action' => 'view', 'id' => $artifact->id, 'format' => 'tsv'],
                    ['class' => 'ml-5']
                ) ?>
                <h4 class="ml-4">Expanded catalogue</h4>
                <?= $this->Html->link(
                    __('As {0}', 'JSON'),
                    ['controller' => 'Artifacts', 'action' => 'view', 'id' => $artifact->id, 'format' => 'json'],
                    ['class' => 'ml-5']
                ) ?>
                <h4 class="ml-4">Linked catalogue</h4>
                <?= $this->Html->link(
                    __('As {0}', 'TTL'),
                    ['controller' => 'Artifacts', 'action' => 'view', 'id' => $artifact->id, 'format' => 'ttl'],
                    ['class' => 'ml-5']
                ) ?>
                <?= $this->Html->link(
                    __('As {0}', 'JSON-LD'),
                    ['controller' => 'Artifacts', 'action' => 'view', 'id' => $artifact->id, 'format' => 'jsonld'],
                    ['class' => 'ml-5']
                ) ?>
                <?= $this->Html->link(
                    __('As {0}', 'RDF/JSON'),
                    ['controller' => 'Artifacts', 'action' => 'view', 'id' => $artifact->id, 'format' => 'rdfjson'],
                    ['class' => 'ml-5']
                ) ?>
                <?= $this->Html->link(
                    __('As {0}', 'RDF/XML'),
                    ['controller' => 'Artifacts', 'action' => 'view', 'id' => $artifact->id, 'format' => 'rdf'],
                    ['class' => 'ml-5']
                ) ?>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <h3>Text / annotations</h3>
                <h4 class="ml-4">Text data</h4>
                <?php if ($artifact->has('inscription') && $artifact->inscription->has('atf')): ?>
                    <?= $this->Html->link(
                        __('As {0}', 'ATF'),
                        ['_name' => 'resolveInscriptionWithFormat', 'id' => $artifact->id, 'format' => 'atf'],
                        ['class' => 'ml-5']
                    ) ?>
                    <?= $this->Html->link(
                        __('As {0}', 'JTF'),
                        ['_name' => 'resolveInscriptionWithFormat', 'id' => $artifact->id, 'format' => 'jtf'],
                        ['class' => 'ml-5']
                    ) ?>
                <?php endif; ?>
                <?php if ($artifact->has('inscription') && $artifact->inscription->has('annotation')): ?>
                    <h4 class="ml-4">Annotation data</h4>
                    <?= $this->Html->link(
                        __('As {0}', 'CDLI-CoNLL'),
                        ['_name' => 'resolveInscriptionWithFormat', 'id' => $artifact->id, 'format' => 'cdli-conll'],
                        ['class' => 'ml-5']
                    ) ?>
                    <?= $this->Html->link(
                        __('As {0}', 'CoNLL-U'),
                        ['_name' => 'resolveInscriptionWithFormat', 'id' => $artifact->id, 'format' => 'conll-u'],
                        ['class' => 'ml-5']
                    ) ?>
                <?php endif; ?>
                <?php if ($artifact->has('inscription')): ?>
                    <h4 class="ml-4">Linked annotations</h4>
                    <?= $this->Html->link(
                        __('As {0}', 'TTL'),
                        ['_name' => 'resolveInscriptionWithFormat', 'id' => $artifact->id, 'format' => 'ttl'],
                        ['class' => 'ml-5']
                    ) ?>
                    <?= $this->Html->link(
                        __('As {0}', 'JSON-LD'),
                        ['_name' => 'resolveInscriptionWithFormat', 'id' => $artifact->id, 'format' => 'jsonld'],
                        ['class' => 'ml-5']
                    ) ?>
                    <?= $this->Html->link(
                        __('As {0}', 'RDF/JSON'),
                        ['_name' => 'resolveInscriptionWithFormat', 'id' => $artifact->id, 'format' => 'rdfjson'],
                        ['class' => 'ml-5']
                    ) ?>
                    <?= $this->Html->link(
                        __('As {0}', 'RDF/XML'),
                        ['_name' => 'resolveInscriptionWithFormat', 'id' => $artifact->id, 'format' => 'rdf'],
                        ['class' => 'ml-5']
                    ) ?>
                <?php endif; ?>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <h3>Related publications</h3>
                <?= $this->Html->link(
                    __('As {0}', 'CSV'),
                    [
                        'controller' => 'Publications',
                        'action' => 'index',
                        '?' => ['artifact' => $artifact->id],
                        '_ext' => 'csv'
                    ],
                    ['class' => 'ml-5']
                ) ?>
                <?= $this->Html->link(
                    __('As {0}', 'BibTeX'),
                    [
                        'controller' => 'Publications',
                        'action' => 'index',
                        '?' => ['artifact' => $artifact->id],
                        '_ext' => 'bib'
                    ],
                    ['class' => 'ml-5']
                ) ?>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <h3>Chemical Data</h3>
                <h4 class="ml-4">Seal Chemistry</h4>
                <?= $this->Html->link(
                    __('As {0}', 'CSV'),
                    [
                        'controller' => 'Artifacts',
                        'action' => 'downloadChemistryCSV',
                        $artifact->id,
                        '_ext' => 'csv'
                    ],
                    ['class' => 'ml-5']
                ) ?>
            </div>
        </div>
    <?= $this->Dropdown->close() ?>
</div>
