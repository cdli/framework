<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\ORM\Entity $entity
 */
?>

<?php if (is_array($entity->external_resources) && count($entity->external_resources)): ?>
    <table class="table-bootstrap">
        <thead>
            <tr>
                <th><?= __('Resource') ?></th>
                <th><?= __('Link') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($entity->external_resources as $external_resource): ?>
                <tr>
                    <td><?= $this->Html->link($external_resource->external_resource, [
                        'controller' => 'ExternalResources',
                        'action' => 'view',
                        $external_resource->id
                    ]) ?></td>
                    <td>
                        <?php if ($external_resource->has('base_url') && $external_resource->base_url !== ''): ?>
                            <?= $this->Html->link(
                                $external_resource->_joinData->external_resource_key,
                                $external_resource->base_url . $external_resource->_joinData->external_resource_key
                            ) ?>
                        <?php else: ?>
                            <?= h($external_resource->_joinData->external_resource_key) ?>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>
