<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Publication[] $publications
 */
?>

<p><?= __('Publications') ?></p>
<div class="many-to-many">
    <div id="publication-wrapper">
        <?php foreach (array_merge(array_keys($publications ?? []), ['template']) as $x): ?>
            <div class="form-group border rounded pt-3 px-3<?= $x == 'template' ? ' d-none' : '' ?>">
                <?= $this->Form->control("publications.$x.id", ['type' => 'hidden']) ?>
                <div class="entity-picker" data-for="publications-<?= $x ?>-id" data-search-url="/publications/search">
                    <?= $this->Form->control("publications.$x.designation", [
                        'class' => 'form-control w-100 mb-3',
                        'required' => true,
                        'autocomplete' => 'off',
                        'label' => __('Publication'),
                    ]) ?>
                    <div class="entity-picker-results">
                        <ol>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <?= $this->Form->control("publications.$x._joinData.publication_type", [
                            'class' => 'form-control w-100 mb-3',
                            'type' => 'select',
                            'options' => [
                                'history' => 'history',
                                'primary' => 'primary',
                                'electronic' => 'electronic',
                                'citation' => 'citation',
                                'collation' => 'collation',
                                'other' => 'other',
                            ],
                            'required' => true,
                            'label' => __('Type'),
                            'empty' => true,
                        ]) ?>
                    </div>
                    <div class="col">
                        <?= $this->Form->control("publications.$x._joinData.exact_reference", [
                            'class' => 'form-control w-100 mb-3',
                            'type' => 'text',
                            'label' => __('Exact Reference'),
                            'empty' => true,
                        ]) ?>
                    </div>
                </div>
                <?= $this->Form->control("publications.$x._joinData.publication_comments", [
                    'class' => 'form-control w-100 mb-3',
                    'type' => 'textarea',
                    'label' => __('Comments'),
                    'empty' => true,
                    'rows' => 2,
                ]) ?>
                <?= $this->Form->iconButton(__('Remove'), 'times-circle') ?>
            </div>
        <?php endforeach; ?>
    </div>
    <?= $this->Form->iconButton(__('Add Publication'), 'plus-circle', ['data-for' => 'publication-wrapper']) ?>
</div>
