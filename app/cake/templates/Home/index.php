<?php
use Cake\Utility\Inflector;
$this->assign('description', 'By making the form and content of cuneiform texts available online, the CDLI is opening pathways to the rich historical tradition of the ancient Middle East. In close collaboration with researchers, museums and an engaged public, the project seeks to unharness the extraordinary content of these earliest witnesses to our shared world heritage.');
$this->assign('image', '/images/cdlilogo.svg');
?>

<?php $this->append('meta'); ?>
<meta name="google-site-verification" content="VfZwYqTDOWXuVQxPfguwFThqQyhBiHHykwSJvF6z_RI" />
<?php $this->end(); ?>

<main class="container">
    <div>
        <h1 class="display-3 text-left header-txt">The CDLI Collection</h1>
        <p class="text-justify page-summary-text mt-4">By making the form and content of cuneiform texts available online, the CDLI is opening pathways to the rich historical tradition of the ancient Middle East. In close collaboration with researchers, museums and an engaged public, the project seeks to unharness the extraordinary content of these earliest witnesses to our shared world heritage.</p>
    </div>

    <div class="boxed m-0">
        <p class="mx-2 my-0">
            <?= __('Search for publications, provenience, collection number, and more.') ?>
        </p>

        <?= $this->element('simpleSearch', ['query' => []]) ?>
    </div>

    <div class="row mt-4">
        <div class="col-sm-12 col-lg-4">
            <div class="card">
                <div class="card-body">
                    <a href ="/publications">
                    <span class="fa fa-book fa-5x"></span>
                    </a>
                </div>
              <div class="card-footer"><?= $this->Html->link("Bibliography",
                          ['controller' => 'Publications', 'action' => 'index']
                      ); ?></div>
            </div>
        </div>
        <div class="col-sm-12 col-lg-4">
            <div class="card">
                <div class="card-body">
                    <a href ="/browse">
                        <span class="fa fa-columns fa-5x"></span>
                    </a>
                </div>
                <div class="card-footer"><?= $this->Html->link("Browse",
                          ['controller' => 'Home', 'action' => 'browse']
                      ); ?>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-lg-4">
            <div class="card">
                <div class="card-body">
                    <a href ="/docs">
                        <span class="fa fa-question-circle fa-5x"></span>
                    </a>
                </div>
            </div>
            <div class="card-footer"><?= $this->Html->link("Documentation",
                          ['controller' => 'Docs', 'action' => 'index']
                      ); ?>
            </div>
        </div>
    </div>


    <div class="mt-5 mb-3">
        <h2 class="text-left">Latest accepted submissions</h2>
        <div class="text-left mt-3">
            <ul class="list-group">
            <?php foreach ($updateEvents as $updateEvent): ?>
                <li class="list-group-item justify-content-between align-items-center"><?= $this->Html->link(
                        implode(', ', array_map([Inflector::class, 'humanize'], $updateEvent->update_type)),
                        ['controller' => 'UpdateEvents', 'action' => 'view', $updateEvent->id])
                    ?>
                    update by <?= $this->Html->link(h($updateEvent->creator->author),
                                            ['controller' => 'Authors', 'action' => 'view', $updateEvent->creator->id]
                                        ) ?>
                    <?php if ($updateEvent->has('authors') && !empty($updateEvent->authors)): ?>
                            for <?= implode('; ', array_map(function ($author) {
                                return $this->Html->link(
                                    $author->author,
                                    ['controller' => 'Authors', 'action' => 'view', $author->id]
                                );
                            }, $updateEvent->authors)) ?>
                        <?php else: ?>
                            for <?= $this->Html->link(
                                $updateEvent->creator->author,
                                ['controller' => 'Authors', 'action' => 'view', $updateEvent->creator->id]
                            ) ?>
                        <?php endif; ?>
                        <?php if ($updateEvent->has('external_resource')): ?> as part of the project
                            <?= $this->Html->link(
                                h($updateEvent->external_resource->external_resource),
                                ['controller' => 'ExternalResources', 'action' => 'view', $updateEvent->external_resource->id]
                            ) ?>
                        <?php endif; ?>
                        on <?= h($updateEvent->created)?>
                        <?= h($updateEvent->created->timezoneName) ?>
                    <span class="badge badge-secondary badge-pill ml-2">
                        <?= count($updateEvent->artifacts_updates) + count($updateEvent->artifact_asset_annotations) + count($updateEvent->entities_updates) + count($updateEvent->inscriptions) ?>
                    </span>
                    </li>
            <?php endforeach;?>
            </ul>
        </div>
        <div class="text-left mt-3">
            <?= $this->Html->link(
                'View all Updates',
                ['controller' => 'UpdateEvents', 'action' => 'index']
            ) ?>
        </div>
    </div>
    <div class="mt-5">
        <h2 class="text-left">Highlights</h2>
        <div class="row mt-3">
            <?php foreach ($highlights as $highlight): ?>
                <div class="card bs4-cdli-card col-sm-12 col-md-6 col-lg-4 mb-5">
                    <img src="<?= $this->ArtifactAssets->getThumbnail($highlight->artifact) ?>"
                         alt="Highlight <?= $highlight->title . ' ' . $highlight->image_type ?>"
                         class="card-img-top">
                    <div class="card-body">
                        <p class="card-title">
                            <?= $this->Html->link(
                                strlen($highlight->title) > 38 ? substr($highlight->title, 0, 38) . '...' : $highlight->title,
                                ['controller' => 'Postings', 'action' => 'view', $highlight->id]
                            ) ?>
                        </p>
                        <p class="card-text">
                            <?=
                            strlen(strip_tags($highlight->body)) > 100 ? substr(strip_tags($highlight->body), 0, 100) . '...' : strip_tags($highlight->body)
                            // use above to trim extra card-length
                            ?>
                        </p>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
        <div class="text-left">
            <?= $this->Html->link(
                'View all Highlights',
                ['controller' => 'Postings', 'action' => 'highlights']
            ) ?>
        </div>
    </div>

    <div class="mt-5 mb-3">
        <h2 class="text-left">News</h2>
        <div class="text-left mt-3">
            <ul class="list-group">
            <?php foreach ($newsarticles as $news): ?>
                <li class="list-group-item">
                    <?= $this->Html->link($news->title,
                        ['controller' => 'Postings', 'action' => 'view', $news->id]
                    ); ?>
                    <?= $news->publish_start->i18nFormat('yyyy-MM-dd') ?>
                </li>

            <?php endforeach;?>
            </ul>
        </div>
        <div class="text-left mt-3">
            <?= $this->Html->link(
                'View all News',
                ['controller' => 'Postings', 'action' => 'news']
            ) ?>
        </div>
    </div>
</main>
<?= $this->element('smoothscroll'); ?>
<?= $this->element('citeBottom'); ?>
<?= $this->element('citeButton'); ?>
