<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal $journal
 */

$publications = [];
foreach ($journal->publications as $publication) {
    $publications[$publication->volume][$publication->number][] = $publication;
}

krsort($publications);
foreach ($publications as $i => $volume) {
    krsort($volume);
    $publications[$i] = $volume;

    foreach ($volume as $j => $issue) {
        uasort($issue, function ($a, $b) {
            return strnatcmp($a->pages ?? '', $b->pages ?? '');
        });
        $publications[$i][$j] = $issue;
    }
}
?>

<div class="text-left row justify-content-center">
    <div class="col-lg-8">
        <h2><?= __('Journal') ?>: <?= h($journal->journal) ?></h2>
        <hr>

        <?php foreach ($publications as $volume => $volumeContents): ?>
            <?php if (!empty($volume)): ?>
                <h3><?= __('Vol. {0}', $volume) ?></h3>
            <?php endif; ?>

            <?php foreach ($volumeContents as $issue => $issueContents): ?>
                <?php if (!empty($issue)): ?>
                    <h4><?= __('No. {0}', $issue) ?></h4>
                <?php endif; ?>

                <div class="mb-3">
                    <?php foreach ($issueContents as $publication): ?>
                        <div class="row border p-2 mb-2 mx-0">
                            <div class="col-1 px-0 text-right">
                                <span class="small"><?= $publication->pages ?></span>
                            </div>
                            <div class="col-11">
                                <div>
                                    [<?= $this->Html->link(
                                        $publication->bibtexkey,
                                        ['controller' => 'Publications', 'action' => 'view', $publication->id],
                                    ) ?>]
                                    <?= h($publication->designation) ?>
                                </div>

                                <?= $this->Citation->formatReferenceAsHtml($publication, 'bibliography', [
                                    'template' => 'chicago-author-date',
                                    'format' => 'html',
                                    'hyperlinks' => true,
                                ]) ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </div>
</div>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
