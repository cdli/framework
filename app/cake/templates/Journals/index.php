<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal[]|\Cake\Collection\CollectionInterface $journals
 */
?>

<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('Journals') ?></h1>
    <?= $this->element('addButton'); ?>
</div>
<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>
<?= $this->element('search_form') ?>
<div class="table-responsive">
    <table cellpadding="0" cellspacing="0" class="table-bootstrap">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('journal') ?></th>
                <th><?= __('Years active') ?></th>
                <th><?= $this->Paginator->sort('publications_count', __('Publications')) ?></th>
                <?php if ($access_granted): ?>
                    <th scope="col"><?= __('Action') ?></th>
                <?php endif ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($journals as $journal): ?>
                <tr align="left">
                    <td>
                        <?= $this->Html->link(
                            h($journal->journal),
                            ['controller' => 'Journals', 'action' => 'view', $journal->id]
                        ) ?>
                    </td>
                    <td>
                        <?php if (count(array_column($journal->publications, 'year')) > 0): ?>
                            <?= min(array_column($journal->publications, 'year')) ?>
                            &ndash;
                            <?= max(array_column($journal->publications, 'year')) ?>
                        <?php else: ?>
                            &mdash;
                        <?php endif; ?>
                    </td>
                    <td>
                        <?= $journal->publications_count ?>
                    </td>
                    <?php if ($access_granted): ?>
                        <td>
                            <?= $this->Html->link(
                                __('Edit'),
                                ['prefix' => 'Admin', 'action' => 'edit', $journal->id],
                                ['class' => 'btn btn-warning btn-sm']
                            ) ?>
                        </td>
                    <?php endif ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?= $this->element('Paginator') ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
