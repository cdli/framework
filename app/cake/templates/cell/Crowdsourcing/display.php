<div class="dropdown no-js-dd">
    <a class="nav-link dropdown-toggle position-relative" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="sr-only"><?= __('Crowdsourcing') ?></span>
        <?php if ($total): ?>
            <span class="badge badge-danger"><?= $total ?></span>
        <?php else: ?>
            <span class="fa fa-pencil-square-o" aria-hidden="true"></span>
        <?php endif; ?>
    </a>
    <div class="dropdown-menu dropdown-menu-right no-js-dd-content">
        <h6 class="dropdown-header"><?= __('Unsaved changes') ?></h6>
        <?= $this->Dropdown->notificationItem(
            __('Changesets'),
            $total,
            ['prefix' => false, 'controller' => 'Changesets', 'action' => 'index']
        ) ?>

        <hr>
        <h6 class="dropdown-header"><?= __('Update Events') ?></h6>
        <?= $this->Dropdown->notificationItem(__('Drafts'), $created ?? 0, [
            'prefix' => false,
            'controller' => 'UpdateEvents',
            'action' => 'index',
            '?' => [
                'created_by' => $this->Identity->get('author_id'),
                'status' => 'created',
            ],
        ]) ?>
        <?= $this->Dropdown->notificationItem(__('In queue'), $submitted ?? 0, [
            'prefix' => false,
            'controller' => 'UpdateEvents',
            'action' => 'index',
            '?' => [
                'created_by' => $this->Identity->get('author_id'),
                'status' => 'submitted',
            ],
        ]) ?>
        <?= $this->Dropdown->notificationItem(__('Approved'), $approved ?? 0, [
            'prefix' => false,
            'controller' => 'UpdateEvents',
            'action' => 'index',
            '?' => [
                'created_by' => $this->Identity->get('author_id'),
                'status' => 'approved',
            ],
        ], 'badge-light') ?>
    </div>
</div>
