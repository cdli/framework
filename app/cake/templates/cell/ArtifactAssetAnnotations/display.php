<div id="viewer">
    <noscript>
        <img src="<?= DS . $asset->getPath() ?>">
    </noscript>
</div>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@recogito/annotorious-openseadragon@2.7.2/dist/annotorious.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/openseadragon/3.0.0/openseadragon.min.js" integrity="sha512-Dq5iZeGNxm7Ql/Ix10sugr98niMRyuObKlIzKN1SzUysEXBxti479CTsCiTV00gFlDeDO3zhBsyCOO+v6QVwJw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/@recogito/annotorious-openseadragon@2.7.2/dist/openseadragon-annotorious.min.js"></script>
<script type="text/javascript">
$(function() {
    var viewer = OpenSeadragon({
        id: 'viewer',

        // Location of UI images
        prefixUrl: 'https://cdn.jsdelivr.net/npm/openseadragon@3.1/build/openseadragon/images/',

        // Show navigator in top right
        showNavigator: true,
        navigatorPosition:   "BOTTOM_LEFT",
        navigatorSizeRatio: 0.2,

        // Show more controls
        showFlipControl: true,
        showRotationControl: true,

        // Load image
        preserveImageSizeOnResize: true,
        tileSources: {
            type: 'image',
            url: <?= json_encode(DS . $asset->getPath()) ?>
        }
    })

    // Zoom to top center of image
    function setZoom () {
        var zoom = viewer.viewport.imageToViewportZoom(1)
        if (zoom < 1) {
            viewer.viewport.zoomTo(zoom, null, true)
            viewer.viewport.defaultZoomLevel = zoom
            viewer.viewport.minZoomLevel = zoom
        } else {
            var bounds = viewer.viewport._contentBounds
            var aspect = viewer.viewport.getAspectRatio()
            viewer.viewport.fitBoundsWithConstraints(new OpenSeadragon.Rect(
                0,
                0,
                bounds.width,
                Math.min(bounds.width / aspect, bounds.height)
            ), true)
        }
    }
    viewer.addHandler('tile-loaded', setZoom)
    viewer.addHandler('resize', setZoom)

    // Load annotations in W3C WebAnnotation format
    var annotations = <?= json_encode(array_map(function ($annotation) {
        return $annotation->getW3cJson();
    }, $asset->annotations)) ?>;

    function makeLink (label, url) {
        if (!label) {
            label = url
        } else if (!url) {
            return label
        }

        const a = document.createElement('a')
        a.setAttribute('href', url)
        a.textContent = label

        return a
    }

    var DataWidget = function (args) {
        var bodies = {
            classifying: [],
            identifying: [],
            describing: [],
            assessing: [],
        }
        for (var body of args.annotation.bodies) {
            if (body.purpose in bodies) {
                bodies[body.purpose].push(body)
            }
        }

        var container = document.createElement('div')
        container.setAttribute('class', 'table-responsive')
        var table = document.createElement('table')
        table.setAttribute('class', 'table table-sm small')

        for (var purpose in bodies) {
            var row = document.createElement('tr')
            var header = document.createElement('th')
            header.setAttribute('scope', 'row')
            header.setAttribute('rowspan', bodies[purpose].length)
            header.textContent = purpose
            row.appendChild(header)
            for (var body of bodies[purpose]) {
                var cell = document.createElement('td')
                cell.append(makeLink(body.value || (body.source && body.source.label), body.source && body.source.id))
                row.appendChild(cell)
                table.appendChild(row)
                row = document.createElement('tr')
            }
        }

        container.appendChild(table)
        return container
    }
    var CreditsWidget = function (args) {
        var annotation = annotations.find(annotation => annotation.id === args.annotation.id)
        var container = document.createElement('div')
        container.setAttribute('class', 'px-1 border-top')
        container.setAttribute('style', 'font-size: 0.7em;')
        container.append('Authors: ')
        for (var i = 0; i < annotation.creator.length; i++) {
            if (i !== 0) {
                container.append(', ')
            }
            container.append(makeLink(annotation.creator[i].name, '/' + annotation.creator[i].id))
        }
        container.append(document.createElement('br'))
        container.append('Created: ', (new Date(annotation.created)).toLocaleString(), ', ')
        container.append('modified: ', (new Date(annotation.modified)).toLocaleString())
        if (annotation.license) {
            container.append('License: ' + annotation.license)
        }

        <?php if ($canSubmitEdits): ?>
        container.append(document.createElement('br'))
        var editLink = makeLink('Edit', args.annotation.id.replace('annotations/', 'annotations/edit/'))
        editLink.setAttribute('target', '_blank')
        container.appendChild(editLink)
        <?php endif; ?>

        return container
    }
    var anno = OpenSeadragon.Annotorious(viewer, {
        readOnly: true,
        widgets: [
            'TAG',
            DataWidget,
            'COMMENT',
            CreditsWidget,
        ]
    })
    anno.setAnnotations(annotations)
})
</script>
