<div id="viewer" class="text-center" style="background: black; height: 100vh;">
    <p id="viewer-links">
        <span class="py-1 px-3">
            <?= $this->Html->link(
                __('Open image') . ' <span class="fa fa-external-link"></span>',
                DS . $asset->getPath(),
                ['target' => '_blank', 'escapeTitle' => false, 'class' => 'text-white']
            ) ?>

            <?php if (count($asset->annotations)): ?>
                <?= $this->Html->link(
                    __('Show annotations'),
                    ['controller' => 'Artifacts', 'action' => 'annotations', $asset->artifact_id, $asset->id],
                    ['class' => 'ml-5 text-white']
                ) ?>
            <?php endif; ?>
        </span>
    </p>

    <noscript>
        <img src="<?= DS . $asset->getPath() ?>" style="max-height: 100%; max-width: 100%;">
    </noscript>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/openseadragon/3.0.0/openseadragon.min.js" integrity="sha512-Dq5iZeGNxm7Ql/Ix10sugr98niMRyuObKlIzKN1SzUysEXBxti479CTsCiTV00gFlDeDO3zhBsyCOO+v6QVwJw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript">
$(function() {
    var overlay = $('#viewer-links span').get(0)
    $('#viewer-links').remove()

    var viewer = OpenSeadragon({
        id: 'viewer',

        // Location of UI images
        prefixUrl: 'https://cdn.jsdelivr.net/npm/openseadragon@3.1/build/openseadragon/images/',

        // Show navigator in top right
        showNavigator: true,
        navigatorPosition:   "BOTTOM_LEFT",
        navigatorSizeRatio: 0.2,

        // Show more controls
        showFlipControl: true,
        showRotationControl: true,

        // Load image
        preserveImageSizeOnResize: true,
        tileSources: {
            type: 'image',
            url: <?= json_encode(DS . $asset->getPath()) ?>
        }
    })

    viewer.addControl(overlay, { anchor: OpenSeadragon.ControlAnchor.TOP_RIGHT })

    // Zoom to top center of image
    function setZoom () {
        var zoom = viewer.viewport.imageToViewportZoom(1)
        if (zoom < 1) {
            viewer.viewport.zoomTo(zoom, null, true)
            viewer.viewport.defaultZoomLevel = zoom
            viewer.viewport.minZoomLevel = zoom
        } else {
            var bounds = viewer.viewport._contentBounds
            var aspect = viewer.viewport.getAspectRatio()
            viewer.viewport.fitBoundsWithConstraints(new OpenSeadragon.Rect(
                0,
                0,
                bounds.width,
                Math.min(bounds.width / aspect, bounds.height)
            ), true)
        }
    }
    viewer.addHandler('tile-loaded', setZoom)
    viewer.addHandler('resize', setZoom)
})
</script>
