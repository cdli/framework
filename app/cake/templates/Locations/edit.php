<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Location $location
 */
?>

<?= $this->element('changesetStatus', ['currentState' => 0]) ?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($location) ?>
            <legend class="form-heading mb-3">
                <?php if ($location->isNew()): ?>
                    <?= __('Add Location') ?>
                <?php else: ?>
                    <?= __('Edit Location {0}', h($location->location)) ?>
                <?php endif; ?>
            </legend>

            <?= $this->Form->control('location', ['class' => 'form-control w-100 mb-3', 'required' => true]) ?>

            <p><?= __('Names') ?></p>
            <div class="many-to-many">
                <div id="name-wrapper">
                    <?php foreach (array_merge(array_keys($location->entities_names ?? []), ['template']) as $x): ?>
                        <div class="form-group border rounded pt-3 px-3<?= $x == 'template' ? ' d-none' : '' ?>">
                            <?= $this->Form->control("entities_names.$x.id") ?>
                            <div class="form-row">
                                <div class="col">
                                    <?= $this->Form->control("entities_names.$x.name", [
                                        'class' => 'form-control w-100 mb-3',
                                        'type' => 'text',
                                        'label' => __('Name'),
                                    ]) ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $this->Form->control("entities_names.$x.language_ietf", [
                                        'class' => 'form-control w-100 mb-3',
                                        'type' => 'text',
                                        'label' => __('Language (IETF tag)'),
                                    ]) ?>
                                </div>
                            </div>
                            <?= $this->Form->iconButton(__('Remove'), 'times-circle') ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?= $this->Form->iconButton(__('Add Name'), 'plus-circle', ['data-for' => 'name-wrapper']) ?>
            </div>

            <?= $this->Form->control('accuracy', [
                'class' => 'form-control w-100 mb-3',
                'empty' => true,
                'type' => 'select',
                'options' => [
                    '0' => 'not located',
                    '1' => 'tentative',
                    '2' => 'representative',
                    '3' => 'certain',
                ],
            ]) ?>
            <?= $this->Form->control('notes', ['class' => 'form-control w-100 mb-3', 'type' => 'textarea', 'empty' => true]) ?>

            <h2 class="text-uppercase display-4 section-title mt-5 mb-4"><?= __('Coordinates') ?></h2>
            <div class="form-row">
                <div class="col form-group">
                    <?= $this->Form->control('location_longitude_wgs1984', [
                        'class' => 'form-control w-100 mb-3',
                        'label' => __('Longitude (WGS 1984)'),
                    ]) ?>
                </div>
                <div class="col form-group">
                    <?= $this->Form->control('location_latitude_wgs1984', [
                        'class' => 'form-control w-100 mb-3',
                        'label' => __('Latitude (WGS 1984)'),
                    ]) ?>
                </div>
            </div>
            <?= $this->Form->control('location_geoshape_wgs1984', [
                'class' => 'form-control w-100 mb-3',
                'label' => __('GeoJSON (WGS 1984)'),
                'type' => 'textarea',
            ]) ?>

            <h2 class="text-uppercase display-4 section-title mt-5 mb-4"><?= __('Linked information') ?></h2>
            <p><?= __('External Resources') ?></p>
            <div class="many-to-many">
                <div id="external-resources-wrapper">
                    <?php foreach (array_merge(array_keys($location->external_resources ?? []), ['template']) as $x): ?>
                        <div class="form-group border rounded pt-3 px-3<?= $x == 'template' ? ' d-none' : '' ?>">
                            <?= $this->Form->control("external_resources.$x.id", [
                                'class' => 'form-control w-100 mb-3',
                                'type' => 'select',
                                'options' => $externalResources,
                                'label' => __('External Resource'),
                            ]) ?>
                            <?= $this->Form->control("external_resources.$x._joinData.external_resource_key", [
                                'class' => 'form-control w-100 mb-3',
                                'type' => 'text',
                                'label' => __('External Resource Key'),
                                'empty' => true,
                            ]) ?>
                            <?= $this->Form->iconButton(__('Remove'), 'times-circle') ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?= $this->Form->iconButton(__('Add External Resource'), 'plus-circle', ['data-for' => 'external-resources-wrapper']) ?>
            </div>

            <?= $this->Form->button(__('Continue'), ['class'=>'btn btn-primary cdli-btn-blue mr-2']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<?php $this->append('script'); ?>
<?= $this->Html->script('form-control-many-to-many'); ?>
<?php $this->end() ?>
