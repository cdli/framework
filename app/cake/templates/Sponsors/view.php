<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sponsor $sponsor
 * @var bool $isAdmin
 */
?>

<div class="text-left">
    <h1>
        <?= __('Sponsor') ?>: <?= h($sponsor->sponsor) ?>
        <?php if ($isAdmin): ?>
            <?= $this->Html->link(
                __('Edit'),
                ['prefix' => 'Admin', 'action' => 'edit', $sponsor->id],
                ['escape' => false , 'class' => "btn cdli-btn-blue pull-right", 'title' => 'Edit']
            ) ?>
        <?php endif; ?>
    </h1>

    <p><em><?= h($sponsor->sponsor_type->sponsor_type) ?></em></p>

    <hr>

    <div class="row">
        <div class="col-lg-10 col-md-8 col-12 mb-3">
            <div><?= $sponsor->contribution ?></div>
            <a href="<?= $sponsor->url ?>">Visit the home page of <?= h($sponsor->sponsor) ?>.</a>
        </div>
        <div class="col-lg-2 col-md-4 col-12 mb-3">
            <?= $this->Html->image($sponsor->getImage(), ['class' => 'mw-100']) ?>
        </div>
    </div>
</div>

<?= $this->element('citeBottom') ?>
<?= $this->element('citeButton') ?>
