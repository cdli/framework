<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\sponsor[]|\Cake\Collection\CollectionInterface $sponsors
 * @var \App\Model\Entity\sponsor[]|\Cake\Collection\CollectionInterface $sponsor_types
 * @var bool $isAdmin
 */
use Cake\Routing\Router;
?>

<main class="container text-left">
    <div class="d-flex justify-content-between align-items-end mb-2">
        <h1 class="display-3 header-txt text-left"><?= __('Sponsors') ?></h1>
        <?= $this->element('addButton', ['access_granted' => $isAdmin]) ?>
    </div>
    <p class="text-justify page-summary-text mt-4">Over the years, CDLI has attracted support from various institutions and groups which have propelled the initiative in the establishment of state-of-the-art solutions in the areas of digitization, digital preservation, digital sustainability, cultural heritage access, cuneiform studies / Assyriology, social history, history of science, philology, and computational linguistics.</p>

    <h2>Categories</h2>
    <p>Click on a category to see the associated list of sponsors.</p>
    <div class="row">
        <?php foreach ($sponsor_types as $sponsor_type): ?>
            <div class="col">
                <?= $this->Html->link($sponsor_type->sponsor_type, '#' . $sponsor_type->id) ?>
            </div>
        <?php endforeach; ?>
    </div>

    <div>
        <?php foreach ($sponsor_types as $sponsor_type) :?>
            <h3 id="<?= $sponsor_type->id ?>" class="mb-3 mt-3"><?= $sponsor_type->sponsor_type ?></h3>
            <div class="row row-cols-1 row-cols-md-4 staff-row">
                <?php foreach ($sponsors as $sponsor): ?>
                    <?php if ($sponsor->sponsor_type_id == $sponsor_type->id): ?>
                        <div class="col-sm-3 mb-4">
                            <div class="card mb-4 border border-secondary p-3 h-100">
                                <?php if ($isAdmin): ?>
                                    <?= $this->Html->image('/images/edit.svg', [
                                        'alt' => 'Edit',
                                        'url' => ['prefix' => 'Admin', 'action' => 'edit', $sponsor->id],
                                        'title' => 'Edit',
                                        'class' => 'pull-right'
                                    ]) ?>
                                <?php endif; ?>
                                <div class="card-header d-flex h-100">
                                    <p class="my-0 font-weight-normal flex-grow-1">
                                        <span class="font-weight-bold">
                                            <?= $this->Html->link(
                                                $sponsor->sponsor,
                                                ['controller' => 'Sponsors', 'action' => 'view', $sponsor->id]
                                            ) ?>
                                        </p>
                                </div>

                                 <div class="card-body">
                                    <?= $this->Html->image($sponsor->getImage(), ['class' => 'w-100']) ?>
                                 </div>
                             </div>
                         </div>
                    <?php endif; ?>
                <?php endforeach; ?>
             </div>
         <?php endforeach; ?>
    </div>
</main>

<?= $this->element('citeBottom') ?>
<?= $this->element('citeButton') ?>

<button class="p-0 mt-5 btn d-flex align-items-center justify-content-between mx-auto wide-btn">
    <a href="#" class="text-dark btn wide-btn">
        Back to top
        <span class="fa fa-long-arrow-up ml-2" aria-hidden="true"></span>
    </a>
</button>
