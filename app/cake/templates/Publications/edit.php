<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Publication $publication
 * @var \App\Model\Entity\EntryType[] $entry_types
 * @var \App\Model\Entity\ExternalResource[] $external_resources
 * @var \App\Model\Entity\Journal[] $journals
 */

use \Cake\Utility\Hash;
?>

<?= $this->element('changesetStatus', ['currentState' => 0]) ?>

<div class="row justify-content-md-center">
    <div class="col-lg-12 text-left">
        <h2>
            <?php if ($publication->isNew()): ?>
                <?= __('Add Publication') ?>
            <?php else: ?>
                <?= __('Edit Publication') ?>

                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $publication->id], [
                    'class' => 'btn btn-danger float-right',
                    'confirm' => __('Are you sure you want to delete the publication?')
                ]) ?>
            <?php endif; ?>
        </h2>
    </div>

    <div class="col-lg-12 text-left form-wrapper">
        <?= $this->Form->create($publication) ?>

            <?php include 'edit_controls.php'; ?>

            <?= $this->Form->button(__('Continue'), ['class'=>'btn btn-primary cdli-btn-blue mr-2']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
