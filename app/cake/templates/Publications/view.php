<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Publication $publication
 * @var boolean $canSubmitEdits
 */

$this->assign('title', $publication->title ?? $publication->designation);
?>

<?php $this->append('meta'); ?>
    <meta name="citation_title" content="<?= h($publication->title) ?>">
    <?php foreach ($publication->authors as $author): ?>
        <meta name="citation_author" content="<?= h($author->first) ?> <?= h($author->last) ?>">
        <?php if (!empty($author->institution)): ?>
            <meta name="citation_author_institution" content="<?= h($author->institution) ?>">
        <?php endif; ?>
        <?php if (!empty($author->orcid_id)): ?>
            <meta name="citation_author_orcid" content="<?= h($author->orcid_id) ?>">
        <?php endif; ?>
    <?php endforeach; ?>
    <meta name="citation_publication_date" content="<?= $publication->year ?>">

    <?php if ($publication->has('publisher')): ?>
        <meta name="citation_publisher" content="<?= $publication->publisher ?>">
    <?php endif; ?>

    <?php if ($publication->has('journal')): ?>
        <meta name="citation_journal_title" content="<?= $publication->journal->journal ?>">
        <?php if ($publication->has('number')): ?>
            <meta name="citation_issue" content="<?= $publication->number ?>">
        <?php endif; ?>
    <?php endif; ?>

    <?php if ($publication->has('volume')): ?>
        <meta name="citation_volume" content="<?= $publication->volume ?>">
    <?php endif; ?>

    <?php if ($publication->has('pages')): ?>
        <?php if (preg_match('/(\d+)-(\d+)/', $publication->pages, $pages)): ?>
            <meta name="citation_firstpage" content="<?= $pages[1] ?>">
            <meta name="citation_lastpage" content="<?= $pages[2] ?>">
        <?php endif; ?>
    <?php endif; ?>
<?php $this->end(); ?>

<div class="text-left">
    <div class="float-right">
        <?= $this->Html->link(
            __('History'),
            ['history', $publication->id],
            ['class' => 'btn btn-link']
        ) ?>
        <div class="dropdown d-inline-block">
            <button class="btn cdli-btn-blue dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                <?= __('Export data') ?>
            </button>
            <div class="dropdown-menu">
                <h6 class="dropdown-header"><?= __('Flat Data') ?></h6>
                <?= $this->Html->link(__('As {0}', 'CSV'), [$publication->id, 'format' => 'csv'], ['class' => 'dropdown-item']) ?>
                <?= $this->Html->link(__('As {0}', 'TSV'), [$publication->id, 'format' => 'tsv'], ['class' => 'dropdown-item']) ?>
                <h6 class="dropdown-header"><?= __('Linked Data') ?></h6>
                <?= $this->Html->link(__('As {0}', 'TTL'), [$publication->id, 'format' => 'ttl'], ['class' => 'dropdown-item']) ?>
                <?= $this->Html->link(__('As {0}', 'JSON-LD'), [$publication->id, 'format' => 'jsonld'], ['class' => 'dropdown-item'] ) ?>
                <h6 class="dropdown-header"><?= __('Expanded Data') ?></h6>
                <?= $this->Html->link(__('As {0}', 'JSON'), [$publication->id, 'format' => 'json'], ['class' => 'dropdown-item']) ?>
            </div>
        </div>
        <?php if ($canSubmitEdits): ?>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $publication->id], ['class' => 'btn cdli-btn-blue ml-3']) ?>
        <?php endif; ?>
    </div>

    <h1>
        <?= h($publication->title ?? $publication->designation) ?>
    </h1>

    <hr>

    <div class="text-left">
        <?= h($publication->note) ?>
    </div>

    <div class="row">
        <div class="col-lg-6 col-12">
            <table class="table-bootstrap mt-3">
                <tr>
                    <th scope="row"><?= __('Entry type') ?></th>
                    <td>
                        <?php if ($publication->has('entry_type')): ?>
                            <?= $this->Html->link(
                                $publication->entry_type->label,
                                ['action' => 'index', '?' => ['entry_type_id' => $publication->entry_type_id]]
                            ) ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('BibTeX Key') ?></th>
                    <td>
                        <?= h($publication->bibtexkey) ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Designation') ?></th>
                    <td>
                        <?= h($publication->designation) ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Authors') ?></th>
                    <td>
                        <?php foreach ($publication->authors as $author) : ?>
                            <?= $this->Html->link(
                                $author->author,
                                ['controller' => 'Authors', 'action' => 'view', $author->id]
                            ) ?>
                            <br>
                        <?php endforeach; ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Editors') ?></th>
                    <td>
                        <?php foreach ($publication->editors as $editor) : ?>
                            <?= $this->Html->link(
                                $editor->author,
                                ['controller' => 'Authors', 'action' => 'view', $editor->id]
                            ) ?>
                            <br>
                        <?php endforeach; ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Journal') ?></th>
                    <td>
                        <?php if ($publication->has('journal_id')): ?>
                            <?= $this->Html->link(
                                $publication->journal->journal,
                                ['controller' => 'Journals', 'action' => 'view', $publication->journal->id]
                            ) ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Series') ?></th>
                    <td>
                        <?php if ($publication->has('series')): ?>
                            <?= $this->Html->link($publication->series, ['action' => 'index', '?' => ['series' => $publication->series, 'sort' => 'designation']]) ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('URL') ?></th>
                    <td>
                        <?php if ($publication->has('how_published') && str_starts_with($publication->how_published, 'http')): ?>
                            <?= $this->Html->link($publication->how_published, $publication->how_published) ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <!-- tr>
                    <th scope="row"><?= __('Topics') ?></th>
                    <td>
                        <?php foreach ($publication->getTopics() as $topic => $topicEntities): ?>
                            <?php if ($topic === 'artifacts') { continue; } ?>
                            <b><?= h($topic) ?></b>:
                            <?php foreach ($topicEntities as $topicEntity): ?>
                                <?= $this->Html->link(
                                    $topicEntity->id,
                                    ['controller' => 'Publications', 'action' => 'view', $topicEntity->id]
                                ) ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </td>
                </tr -->
            </table>

            <?php if (!empty($publication->external_resources)): ?>
                <h2 class="capital-heading"><?= __('External links') ?></h2>
                <?= $this->element('entityExternalResources', ['entity' => $publication]) ?>
            <?php endif; ?>
        </div>

        <div class="col-lg-6 col-12">
            <?= $this->element('citeBottom', ['data' => $publication]); ?>
        </div>
    </div>

    <?php if (!empty($publication->artifacts)) : ?>
        <div class="boxed mx-0">
            <h2 class="capital-heading"><?= __('Related Artifacts') ?></h2>
            <p>
                <?= $this->Html->link(__('Full search results'), [
                    'controller' => 'Search',
                    'action' => 'index',
                    '?' => ['publication_bibtexkey' => $publication->bibtexkey],
                ]) ?>
            </p>

            <div class="table-responsive">
                <table cellpadding="0" cellspacing="0" class="table-bootstrap mx-0">
                    <thead>
                        <th scope="col"><?= $this->Paginator->sort('id', __('Identifier')) ?></th>
                        <th scope="col"><?= $this->Paginator->sort('designation') ?></th>
                        <th scope="col"><?= __('Museum Collections') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('Periods.period', __('Period')) ?></th>
                        <th scope="col"><?= $this->Paginator->sort('Proveniences.provenience', __('Provenience')) ?></th>
                        <th scope="col"><?= $this->Paginator->sort('ArtifactTypes.artifact_type', __('Artifact Type')) ?></th>
                        <th scope="col"><?= $this->Paginator->sort('EntitiesPublications.publication_type', __('Publication Type')) ?></th>
                        <th scope="col"><?= $this->Paginator->sort('EntitiesPublications.exact_reference', __('Exact Reference')) ?></th>
                        <th scope="col"><?= __('Publication Comments') ?></th>
                    </thead>
                    <tbody>
                        <?php foreach ($publication->artifacts as $artifact) : ?>
                        <tr>
                            <td><?= $this->Html->link(h('P'.substr("00000{$artifact->id}", -6)), ['controller' => 'Artifacts', 'action' => 'view', $artifact->id]) ?>
                            </td>
                            <td><?= h($artifact->designation) ?></td>
                            <td>
                                <?php foreach ($artifact->collections as $collection) : ?>
                                    <?= $this->Html->link($collection->collection, ['controller' => 'Collections', 'action' => 'view', $collection->id]) ?>
                                <?php endforeach; ?>
                            </td>
                            <td>
                                <?php if (!empty($artifact->period)) : ?>
                                    <?= $this->Html->link($artifact->period->period, ['controller' => 'Periods', 'action' => 'view', $artifact->period->id]) ?>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if (!empty($artifact->provenience)) : ?>
                                    <?= $this->Html->link($artifact->provenience->provenience, ['controller' => 'Proveniences', 'action' => 'view', $artifact->provenience->id]) ?>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if (!empty($artifact->artifact_type)) : ?>
                                    <?= $this->Html->link($artifact->artifact_type->artifact_type, ['controller' => 'ArtifactTypes', 'action' => 'view', $artifact->artifact_type->id]) ?>
                                <?php endif; ?>
                            </td>
                            <td><?= h($artifact->entities_publication->publication_type) ?></td>
                            <td><?= h($artifact->entities_publication->exact_reference) ?></td>
                            <td><?= h($artifact->entities_publication->publication_comments) ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endif; ?>

    <?php if (!empty($publication->proveniences)) : ?>
        <div class="boxed mx-0">
            <h2 class="capital-heading"><?= __('Related Proveniences') ?></h2>
            <div class="table-responsive">
                <table cellpadding="0" cellspacing="0" class="table-bootstrap mx-0">
                    <thead>
                        <tr align="left">
                            <th scope="col">Provenience</th>
                            <th scope="col">Region</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($publication->proveniences as $provenience): ?>
                            <tr align="left">
                                <td>
                                    <?= $this->Html->link(
                                        $provenience->provenience,
                                        ['controller' => 'Proveniences', 'action' => 'view', $provenience->id]
                                    ) ?>
                                </td>
                                <td>
                                    <?php if ($provenience->has('region')): ?>
                                        <?= $this->Html->link(
                                            $provenience->region->region,
                                            ['controller' => 'Regions', 'action' => 'view', $provenience->region->id]
                                        ) ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endif; ?>
</div>
