<?php
/**
 * @var \App\View\AppView $this
 */
?>

<?= $this->element('changesetStatus', ['currentState' => 0]) ?>

<div class="row justify-content-md-center">
    <div class="col-lg-12 text-left">
        <h2><?= __('Upload Publications') ?></h2>

        <p>
            Upload references here, as a CSV or BibTeX file. To upload links between publications and artifacts or other entities,
            use <?= $this->Html->link('this form', ['controller' => 'EntitiesPublications', 'action' => 'add']) ?>.
            <?= $this->Html->link('Documentation.', ['controller' => 'Docs', 'action' => 'view', 'adding-and-editing-publications']) ?>
        </p>
    </div>

    <div class="col-lg-12 text-left form-wrapper">
        <?= $this->Form->create(null, ['type' => 'file']) ?>
            <?= $this->Form->control('upload', ['type' => 'file', 'class' => 'form-control']) ?>

            <hr>

            <?= $this->Form->button(__('Continue'), ['class'=>'btn btn-primary cdli-btn-blue mr-2']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
