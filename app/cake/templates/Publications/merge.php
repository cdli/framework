<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Publication $publication
 * @var \App\Model\Entity\Publication[] $publications
 * @var \App\Model\Entity\EntryType[] $entry_types
 * @var \App\Model\Entity\ExternalResource[] $external_resources
 * @var \App\Model\Entity\Journal[] $journals
 * @var string $primary
 */

$publicationTypes = [
    'primary' => 'primary',
    'electronic' => 'electronic',
    'citation' => 'citation',
    'collation' => 'collation',
    'history' => 'history',
    'other' => 'other',
];

$this->assign('title', __('Merge publications'));
?>

<?= $this->element('changesetStatus', ['currentState' => 0]) ?>

<div class="row text-left">
    <div class="col-12">
        <h1><?= __('Merge publications') ?></h1>
        <p>
            Edit the references and publication metadata.
        </p>
    </div>
</div>

<div class="row justify-content-md-center">
    <div class="col-lg-12 text-left form-wrapper">
        <?= $this->Form->create($publication, ['url' => ['?' => ['step' => 'merge']]]) ?>
            <h2><?= __('Linked artifacts') ?></h2>
            <div class="table-responsive">
                <table class="table-bootstrap mx-0">
                    <thead>
                        <tr>
                            <th><?= __('Artifact') ?></th>
                            <th><?= __('Exact Reference') ?></th>
                            <th><?= __('Publication Type') ?></th>
                            <th><?= __('Publication Comments') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($artifacts as $artifact): ?>
                            <tr>
                                <td>
                                    <?= h($artifact->designation) ?>
                                    (<?= $this->Html->link(
                                        $artifact->getCdliNumber(),
                                        ['controller' => 'Artifacts', 'action' => 'view', $artifact->id]
                                    ) ?>)
                                    <?= $this->Form->hidden("artifacts[$artifact->id][_joinData][id]", ['value' => $artifact->_joinData->id]) ?>
                                </td>
                                <td>
                                    <?= $this->Form->input("artifacts[$artifact->id][_joinData][exact_reference]", [
                                        'class' => 'form-control w-100',
                                        'value' => $artifact->_joinData->exact_reference,
                                    ]) ?>
                                </td>
                                <td>
                                    <?= $this->Form->select("artifacts[$artifact->id][_joinData][publication_type]", $publicationTypes, [
                                        'class' => 'form-control form-select w-100',
                                        'value' => $artifact->_joinData->publication_type,
                                    ]) ?>
                                </td>
                                <td>
                                    <?= $this->Form->textarea("artifacts[$artifact->id][_joinData][publication_comments]", [
                                        'class' => 'form-control w-100',
                                        'value' => $artifact->_joinData->publication_comments,
                                        'rows' => 1,
                                    ]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <h2><?= __('Publication') ?></h2>
            <?= $this->Form->hidden('id', ['value' => $publication->id]) ?>
            <?php include 'edit_controls.php'; ?>

            <?php foreach ($publications as $merge): ?>
                <?= $this->Form->hidden('merge[]', ['value' => $merge->id]) ?>
            <?php endforeach; ?>

            <?= $this->Form->button(__('Continue'), ['class'=>'btn btn-primary cdli-btn-blue mr-2']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
