<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Publication[]|\Cake\Collection\CollectionInterface $publications
 * @var \App\Model\Entity\Journal[]|\Cake\Collection\CollectionInterface $journals
 * @var \App\Model\Entity\EntryType[]|\Cake\Collection\CollectionInterface $entryTypes
 * @var array $queryParams
 * @var boolean $canSubmitEdits
 */
?>

<div class="row">
    <div class="col-12 text-left">
        <div class="float-right">
            <?php if ($canSubmitEdits) : ?>
                <?= $this->Html->link('<span class="fa fa-plus fa-2x" aria-hidden="true"></span>', ['action' => 'add'], ['escapeTitle' => false, 'class' => 'd-flex d-lg-none floating-btn']) ?>
                <?= $this->Html->link('+ Add Publication', ['action' => 'add'], ['escapeTitle' => false, 'class' => 'btn cdli-btn-blue d-none d-lg-inline-block']) ?>
                <?= $this->Html->link(__('Upload'), ['action' => 'upload'], ['class' => 'btn cdli-btn-blue']) ?>

                <?= $this->Html->link(
                    __('Merge'),
                    ['action' => 'merge', '?' => $queryParams + ['step' => 'select']],
                    ['class' => 'btn cdli-btn-blue']
                ) ?>
            <?php endif ?>
        </div>

        <h1 class="display-3"><?= __('Publications') ?></h1>

        <p>
            From this page you can access the bibliography relating to cuneiform artifacts and to some entities such as proveniences and periods. Use the search function provided to filter out the publications of your choice and browse below using the pager.
        </p>

        <?= $this->element('entityExport'); ?>
    </div>
</div>

<hr class="line" />

<div class="row text-left">
    <!-- Filters -->
    <div id="search-filters" class="col-lg-3 col-12 pt-3">
        <h3>
            <?= __('Search') ?>

            <button id="search-filter-collapse" class="btn bg-white float-right mx-1">
                <span class="h3 m-0">
                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                </span>
                <span class="sr-only"><?= __('Close') ?></span>
            </button>
        </h3>

        <?= $this->Form->create(['data' => $queryParams, 'schema' => []], ['url' => ['action' => 'index'], 'type' => 'get']) ?>
            <?= $this->Form->button(__('Search'), ['class' => 'btn cdli-btn-blue mb-3', 'type' => 'submit']) ?>
            <?= $this->Html->link(__('Reset'), ['action' => 'index'], ['class' => 'btn cdli-btn-light mb-3']) ?>

            <?= $this->Form->control('bibtexkey', ['label' => 'BibTeX Key', 'class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('title', ['class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('publisher', ['class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('author', ['class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('designation', ['class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('artifact', ['class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('entry_type_id', ['label' => 'Entry Type', 'options' => $entryTypes, 'empty' => true, 'class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('journal_id', ['label' => 'Journal', 'options' => $journals, 'empty' => true, 'class' => 'form-control w-100 mb-3']) ?>

            <div class="row">
                <div class="col">
                    <?= $this->Form->control('from', ['label' => 'From Year', 'class' => 'form-control w-100']) ?>
                </div>
                <div class="col">
                    <?= $this->Form->control('to', ['label' => 'To Year', 'class' => 'form-control w-100']) ?>
                </div>
            </div>
        <?= $this->Form->end() ?>
    </div>

    <div id="search-results" class="col-lg-9 col-12">
        <div class="clearfix">
            <?= $this->Html->link(
                __('Search') . ' <span class="fa fa-chevron-right ml-1 align-middle" aria-hidden="true"></span>',
                '#',
                [
                    'id' => 'search-filter-open',
                    'class' => 'float-md-left my-3 btn cdli-btn-blue d-lg-none',
                    'escapeTitle' => false
                ]
            ) ?>

            <div class="my-3 float-md-right text-center">
                <div class="d-inline-block">
                    <?= $this->element('paginatorSimple') ?>
                </div>
            </div>
        </div>

        <?php if (count($publications) === 0): ?>
            <p><?= __('No Search Results') ?></p>
        <?php else: ?>
            <div class="table-responsive">
                <table class="table-bootstrap mx-0">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('bibtexkey', 'BibTeX key') ?></th>
                            <th scope="col"><?= __('Authors') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('designation') ?></th>
                            <th scope="col"><?= __('Reference') ?></th>
                            <?php if ($canSubmitEdits): ?>
                                <th scope="col"><?= __('Action') ?></th>
                            <?php endif ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($publications as $publication): ?>
                            <tr>
                                <td>
                                    <?= $this->Html->link($publication->bibtexkey, ['action' => 'view', $publication->id]) ?>
                                </td>
                                <td nowrap="nowrap">
                                    <?php foreach ($publication->authors as $author): ?>
                                        <?= $this->Html->link($author->author, ['controller' => 'Authors', 'action' => 'view', $author->id]) ?><br>
                                    <?php endforeach ?>
                                </td>
                                <td><?= h($publication->designation) ?></td>
                                <td>
                                    <?= $this->Citation->formatReferenceAsHtml($publication, 'bibliography', [
                                        'template' => 'chicago-author-date',
                                        'format' => 'html',
                                        'hyperlinks' => true
                                    ]) ?>
                                </td>
                                <?php if ($canSubmitEdits): ?>
                                    <td>
                                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $publication->id], ['class' => 'btn cdli-btn-light']) ?>
                                    </td>
                                <?php endif ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <?= $this->element('Paginator') ?>
        <?php endif ?>
    </div>
</div>

<?php $this->append('library'); ?>
<?= $this->Html->script('searchResult') ?>
<?php $this->end(); ?>
