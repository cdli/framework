<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Publication[]|\Cake\Collection\CollectionInterface $publications
 * @var \App\Model\Entity\Journal[]|\Cake\Collection\CollectionInterface $journals
 * @var \App\Model\Entity\EntryType[]|\Cake\Collection\CollectionInterface $entryTypes
 * @var array $queryParams
 */

$this->assign('title', __('Merge publications'));
?>

<?= $this->element('changesetStatus', ['currentState' => 0]) ?>

<div class="row text-left">
    <div class="col-12">
        <h1><?= __('Merge publications') ?></h1>
        <p>
            Select publications to merge.
        </p>
    </div>
</div>

<div class="row text-left">
    <!-- Filters -->
    <div id="search-filters" class="col-lg-3 col-12 pt-3">
        <h3>
            <?= __('Search') ?>

            <button id="search-filter-collapse" class="btn bg-white float-right mx-1">
                <span class="h3 m-0">
                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                </span>
                <span class="sr-only"><?= __('Close') ?></span>
            </button>
        </h3>

        <?= $this->Form->create(['data' => $queryParams, 'schema' => []], ['url' => ['action' => 'merge', '?' => ['step' => 'select']], 'type' => 'get']) ?>
            <?= $this->Form->button(__('Search'), ['class' => 'btn cdli-btn-blue mb-3', 'type' => 'submit']) ?>
            <?= $this->Html->link(__('Reset'), ['action' => 'index'], ['class' => 'btn cdli-btn-light mb-3']) ?>

            <?= $this->Form->control('bibtexkey', ['label' => 'BibTeX Key', 'class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('title', ['class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('publisher', ['class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('author', ['class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('designation', ['class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('artifact', ['class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('entry_type_id', ['label' => 'Entry Type', 'options' => $entryTypes, 'empty' => true, 'class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('journal_id', ['label' => 'Journal', 'options' => $journals, 'empty' => true, 'class' => 'form-control w-100 mb-3']) ?>

            <div class="row">
                <div class="col">
                    <?= $this->Form->control('from', ['label' => 'From Year', 'class' => 'form-control w-100']) ?>
                </div>
                <div class="col">
                    <?= $this->Form->control('to', ['label' => 'To Year', 'class' => 'form-control w-100']) ?>
                </div>
            </div>
        <?= $this->Form->end() ?>
    </div>

    <div id="search-results" class="col-lg-9 col-12">
        <?= $this->Form->create(null, ['url' => ['?' => ['step' => 'select']]]) ?>
            <div class="clearfix">
                <?= $this->Html->link(
                    __('Search') . ' <span class="fa fa-chevron-right ml-1 align-middle" aria-hidden="true"></span>',
                    '#',
                    [
                        'id' => 'search-filter-open',
                        'class' => 'float-md-left my-3 mr-3 btn cdli-btn-blue d-lg-none',
                        'escapeTitle' => false
                    ]
                ) ?>

                <div class="my-3 float-md-left">
                    <div class="d-inline-block">
                        <?= $this->Form->button(__('Merge'), ['class' => 'btn cdli-btn-blue mb-3', 'type' => 'submit']) ?>
                        <?= $this->Form->button(__('Select all'), ['id' => 'merge-select-all', 'class' => 'btn mb-3', 'type' => 'button']) ?>
                        <?= $this->Form->button(__('Deselect all'), ['id' => 'merge-select-none', 'class' => 'btn mb-3', 'type' => 'button']) ?>
                    </div>
                </div>
            </div>

            <?php if (count($publications) === 0): ?>
                <p><?= __('No Search Results') ?></p>
            <?php else: ?>
                <?php if ($publicationsTotal > $queryLimit): ?>
                    <div class="alert alert-warning">
                        <?= __('Only showing the first {0} of {1} results', $queryLimit, $publicationsTotal) ?>
                    </div>
                <?php endif; ?>

                <div class="table-responsive">
                    <table class="table-bootstrap mx-0">
                        <thead>
                            <tr>
                                <th scope="col"><?= __('BibTeX key') ?></th>
                                <th scope="col"><?= __('Authors') ?></th>
                                <th scope="col"><?= __('Designation') ?></th>
                                <th scope="col"><?= __('Reference') ?></th>

                                <th scope="col"><?= __('Merge') ?></th>
                                <th scope="col"><?= __('Primary') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($publications as $i => $publication): ?>
                                <tr>
                                    <td>
                                        <?= $this->Html->link($publication->bibtexkey, ['action' => 'view', $publication->id]) ?>
                                    </td>
                                    <td nowrap="nowrap">
                                        <?php foreach ($publication->authors as $author): ?>
                                            <?= $this->Html->link($author->author, ['Authors', 'action' => 'view', $author->id]) ?><br>
                                        <?php endforeach ?>
                                    </td>
                                    <td><?= h($publication->designation) ?></td>
                                    <td>
                                        <?= $this->Citation->formatReferenceAsHtml($publication, 'bibliography', [
                                            'template' => 'chicago-author-date',
                                            'format' => 'html',
                                            'hyperlinks' => true
                                        ]) ?>
                                    </td>

                                    <td>
                                        <?= $this->Form->checkbox('merge[]', [
                                            'value' => $publication->id,
                                            'hiddenField' => false,
                                            'checked' => $hasQuery,
                                        ]) ?>
                                    </td>
                                    <td>
                                        <?= $this->Form->radio('primary', array_flip([$publication->id]), [
                                            'hiddenField' => false,
                                            'label' => false,
                                        ]) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?php endif ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<?php $this->append('library'); ?>
<?= $this->Html->script('searchResult') ?>
<?php $this->end(); ?>

<?php $this->append('script'); ?>
<script>
$(function () {
    $('#merge-select-all').on('click', function (event) {
        event.preventDefault();
        $('table input[type="checkbox"]:not([disabled])').prop('checked', true);
    })
    $('#merge-select-none').on('click', function (event) {
        event.preventDefault();
        $('table input[type="checkbox"]:not([disabled])').prop('checked', false);
    })

    $('[name="merge[]"]').on('change', function (event) {
        $(this).closest('tr').find('[name=primary]').prop('disabled', !$(this).prop('checked'));
    })
})
</script>
<?php $this->end(); ?>
