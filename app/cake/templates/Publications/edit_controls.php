<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Publication $publication
 * @var \App\Model\Entity\EntryType[] $entry_types
 * @var \App\Model\Entity\ExternalResource[] $external_resources
 * @var \App\Model\Entity\Journal[] $journals
 */
?>

<div class="row">
    <div class="col-12 col-lg-6">
        <p class="form-heading ml-0 mb-3"><?= __('General') ?></p>

        <div class="row">
            <div class="col">
                <?= $this->Form->control('entry_type_id', [
                    'class' => 'form-control w-100 mb-3',
                    'options' => $entryTypes,
                    'empty' => true,
                ]) ?>
            </div>
            <div class="col">
                <?= $this->Form->control('bibtexkey', ['class' => 'form-control w-100 mb-3', 'label' => __('BibTeX Key')]) ?>
            </div>
        </div>

        <?= $this->Form->control('designation', ['class' => 'form-control w-100 mb-3']) ?>
        <?= $this->Form->control('title', ['class' => 'form-control w-100 mb-3', 'required' => $publication->isNew()]) ?>
        <p class="small text-muted"><?= __('At least one of the "title" and "designation" fields must be provided.') ?></p>

        <div class="row">
            <div class="col">
                <?= $this->Form->control('year', ['class' => 'form-control w-100 mb-3', 'type' => 'text']) ?>
            </div>
            <div class="col">
                <?= $this->Form->control('month', ['class' => 'form-control w-100 mb-3']) ?>
            </div>
        </div>

        <p><?= __('Authors') ?></p>
        <div class="many-to-many">
            <div id="authors-wrapper">
                <?php foreach (array_merge(array_keys($publication->authors ?? []), ['template']) as $i => $x): ?>
                    <div class="form-group border rounded pt-3 px-3<?= $x == 'template' ? ' d-none' : '' ?>">
                        <?= $this->Form->control("authors.$x._joinData.sequence", ['type' => 'hidden', 'value' => $i]) ?>
                        <?= $this->Form->control("authors.$x.id", ['type' => 'hidden']) ?>
                        <div class="entity-picker" data-for="authors-<?= $x ?>-id" data-search-url="/authors/search">
                            <?= $this->Form->control("authors.$x.author", [
                                'class' => 'form-control w-100 mb-3',
                                'autocomplete' => 'off',
                                'label' => __('Author'),
                            ]) ?>
                            <div class="entity-picker-results">
                                <ol>
                                </ol>
                            </div>
                        </div>
                        <?= $this->Form->iconButton(__('Remove'), 'times-circle') ?>
                    </div>
                <?php endforeach; ?>
            </div>
            <?= $this->Form->iconButton(__('Add Author'), 'plus-circle', ['data-for' => 'authors-wrapper']) ?>
        </div>

        <p><?= __('Editors') ?></p>
        <div class="many-to-many">
            <div id="editors-wrapper">
                <?php foreach (array_merge(array_keys($publication->editors ?? []), ['template']) as $i => $x): ?>
                    <div class="form-group border rounded pt-3 px-3<?= $x == 'template' ? ' d-none' : '' ?>">
                        <?= $this->Form->control("editors.$x._joinData.sequence", ['type' => 'hidden', 'value' => $i]) ?>
                        <?= $this->Form->control("editors.$x.id", ['type' => 'hidden']) ?>
                        <div class="entity-picker" data-for="editors-<?= $x ?>-id" data-search-url="/authors/search">
                            <?= $this->Form->control("editors.$x.author", [
                                'class' => 'form-control w-100 mb-3',
                                'autocomplete' => 'off',
                                'label' => __('Editor'),
                            ]) ?>
                            <div class="entity-picker-results">
                                <ol>
                                </ol>
                            </div>
                        </div>
                        <?= $this->Form->iconButton(__('Remove'), 'times-circle') ?>
                    </div>
                <?php endforeach; ?>
            </div>
            <?= $this->Form->iconButton(__('Add Editor'), 'plus-circle', ['data-for' => 'editors-wrapper']) ?>
        </div>

        <p class="form-heading ml-0 mb-3"><?= __('Identifiers') ?></p>
        <?= $this->Form->control('oclc', ['class' => 'form-control w-100 mb-3', 'label' => 'OCLC']) ?>

        <p><?= __('External Resources') ?></p>
        <div class="many-to-many">
            <div id="external-resources-wrapper">
                <?php foreach (array_merge(array_keys($publication->external_resources ?? []), ['template']) as $x): ?>
                    <div class="form-group border rounded pt-3 px-3<?= $x == 'template' ? ' d-none' : '' ?>">
                        <div class="row">
                            <div class="col">
                                <?= $this->Form->control("external_resources.$x.id", [
                                    'class' => 'form-control w-100 mb-3',
                                    'type' => 'select',
                                    'options' => $externalResources,
                                    'label' => __('External Resource'),
                                ]) ?>
                            </div>
                            <div class="col">
                                <?= $this->Form->control("external_resources.$x._joinData.external_resource_key", [
                                    'class' => 'form-control w-100 mb-3',
                                    'type' => 'text',
                                    'label' => __('External Resource Key'),
                                    'empty' => true,
                                ]) ?>
                            </div>
                        </div>
                        <?= $this->Form->iconButton(__('Remove'), 'times-circle') ?>
                    </div>
                <?php endforeach; ?>
            </div>
            <?= $this->Form->iconButton(__('Add External Resource'), 'plus-circle', ['data-for' => 'external-resources-wrapper']) ?>
            <p class="small text-muted"><?= __('Use for ISBN, OCLC, DOI.') ?></p>
        </div>
    </div>
    <div class="col-12 col-lg-6">
        <p class="form-heading ml-0 mb-3"><?= __('Journal/book series') ?></p>
        <?= $this->Form->control('journal_id', ['class' => 'form-control w-100 mb-3', 'options' => $journals, 'empty' => true]) ?>
        <?= $this->Form->control('series', ['class' => 'form-control w-100 mb-3']) ?>

        <div class="row">
            <div class="col">
                <?= $this->Form->control('volume', ['class' => 'form-control w-100 mb-3']) ?>
                <p class="small text-muted"><?= __('Use for journal or book volume.') ?></p>
            </div>
            <div class="col">
                <?= $this->Form->control('number', ['class' => 'form-control w-100 mb-3']) ?>
                <p class="small text-muted"><?= __('Use for journal issue or series number.') ?></p>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <?= $this->Form->control('pages', ['class' => 'form-control w-100 mb-3']) ?>
                <p class="small text-muted"><?= __('Use for page range of articles and chapters.') ?></p>
            </div>
            <div class="col">
                <?= $this->Form->control('chapter', ['class' => 'form-control w-100 mb-3']) ?>
                <p class="small text-muted"><?= __('Use for chapter or section number.') ?></p>
            </div>
        </div>

        <?= $this->Form->control('edition', ['class' => 'form-control w-100 mb-3']) ?>
        <?= $this->Form->control('book_title', ['class' => 'form-control w-100 mb-3']) ?>
        <p class="small text-muted"><?= __('Use for book title of chapter entries.') ?></p>

        <p class="form-heading ml-0 mb-3"><?= __('Publisher') ?></p>
        <?= $this->Form->control('publisher', ['class' => 'form-control w-100 mb-3']) ?>
        <?= $this->Form->control('institution', ['class' => 'form-control w-100 mb-3']) ?>
        <?= $this->Form->control('school', ['class' => 'form-control w-100 mb-3']) ?>
        <?= $this->Form->control('organization', ['class' => 'form-control w-100 mb-3']) ?>
        <?= $this->Form->control('address', ['class' => 'form-control w-100 mb-3']) ?>
        <?= $this->Form->control('how_published', ['class' => 'form-control w-100 mb-3']) ?>

        <p class="form-heading ml-0 mb-3"><?= __('Other info') ?></p>
        <?= $this->Form->control('note', ['class' => 'form-control w-100 mb-3', 'type' => 'textarea']) ?>
    </div>
</div>

<?php $this->append('script'); ?>
<?= $this->Html->script('form-control-entity-picker'); ?>
<?= $this->Html->script('form-control-many-to-many'); ?>
<script>
    $(function () {
        var publication = <?= json_encode($publication->id) ?>;
        $('[name=designation], [name=bibtexkey]').on('change', function () {
            var input = this
            var key = $(this).attr('name')
            var value = $(this).val()
            $.ajax({
                url: '/publications.json?' + encodeURIComponent(key) + '=' + encodeURIComponent(value)
            }).done(function (publications) {
                var duplicate = false
                for (var i = 0; i < publications.length; i++) {
                    if (publication === publications[i].id) {
                        continue
                    }
                    if (publications[i][key] === value) {
                        duplicate = true
                    }
                }
                input.setCustomValidity(duplicate ? 'This value is not unique' : '')
            })
        })
    })
</script>
<?php $this->end() ?>
