<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Publication $publication
 */

$this->assign('title', h($publication->title) . ' - ' . __('Publications'));
?>

<div class="row justify-content-md-center">
    <div class="col-lg-12 text-left">
        <h1><?= __('Metadata history') ?></h1>
        <h2><?= $this->Html->link($publication->title, ['action' => 'view', $publication->id]) ?></h2>
    </div>

    <?php foreach ($updateEvents as $updateEvent): ?>
        <?php $entitiesUpdate = $updateEvent->_matchingData['EntitiesUpdates'] ?>
        <div class="col-lg-12 boxed">
            <?= $this->element('updateEventHeader', ['update_event' => $updateEvent]) ?>

            <?php if (!empty($updateEvent->event_comments)): ?>
                <p><?= h($updateEvent->event_comments) ?></p>
            <?php endif; ?>

            <?= $this->element('diff/otherEntityUpdate', ['entitiesUpdate' => $entitiesUpdate]) ?>
        </div>
    <?php endforeach; ?>
</div>
