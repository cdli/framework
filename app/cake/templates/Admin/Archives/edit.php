<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Archive $archive
 */
use Cake\Utility\Inflector;

?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($archive) ?>
            <legend class="form-heading mb-3"><?= __('Edit Archive') ?></legend>
            <hr class="form-hr mb-4"/>
            <?php
                echo $this->Form->control('archive', ['class' => 'form-control w-100 mb-3', 'required' => true]);
                echo $this->Form->control('provenience_id', ['options' => $proveniences, 'empty' => true, 'class' => 'form-control w-100 mb-3']);
            ?>
        <div>
            <?= $this->Form->button(__('Save'), ['class'=>'btn cdli-btn-blue']) ?>
            <?= $this->Form->end() ?>
            <?= $this->element('deleteModal', [ 'entity_name' => $archive->archive, 'id' => $archive->id]); ?>
        </div>
    </div>
</div>
