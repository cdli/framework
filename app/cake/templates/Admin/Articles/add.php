<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <form>
            <legend class="form-heading mb-3"><?= __('Add article') ?></legend>
            <hr class="form-hr mb-4"/>
            <div class="form-group">
                <label>Select journal</label>
                <select id="add-journal-typeselect" name="journal" class="form-control">
                    <option value="cdlj">CDLJ</option>
                    <option value="cdlp">CDLP</option>
                    <option value="cdlb">CDLB</option>
                    <option value="cdln">CDLN</option>
                </select>
                <button type="submit" class="btn cdli-btn-blue">
                    <?= __('Continue') ?>
                </button>
            </div>
        </form>
    </div>
</div>

<?php $this->append('script'); ?>
<script>
$(function () {
    $('form').on('submit', function (event) {
        event.preventDefault();
        var type = $(event.target.journal).val();
        window.location.href = '<?= $this->Url->build(['_name' => 'addSpecific', '']) ?>' + type;
    })
})
</script>
<?php $this->end(); ?>
