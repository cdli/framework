<div class="row justify-content-md-center">
    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Preview') ?></div>
        <hr>
        <div class="preview" style="height: 80vh; overflow-x: scroll; overflow-y: scroll; background-color: white; padding: 5%">
            <table width="750" border="0" cellpadding="0" cellspacing="5">
                <tr>
                    <td valign="top" width="400">
                        <p style="font-family: Skia, Verdana, Arial, Helvetica, sans-serif; font-size: 9pt">
                            Cuneiform Digital Library Journal <br>
                            <b><?= $article->year ?>:<?= $article->article_no ?></b>
                            <br>
                            <font size="1"> ISSN 1540-8779 <br>
                            &#169; <i> Cuneiform Digital Library Initiative </i> </font> &nbsp;
                        </p>
                    </td>
                    <td width="200" height="200" rowspan="2" align="right" nowrap bgcolor="#99CCCC">
                        <p style="line-height: 15.0pt; font-size: 9pt; color: white; ">
                            <font face="Skia, Verdana, Arial, Helvetica, sans-serif">
                                <a href="/"> CDLI Home </a> <br>
                                <a href=""> CDLI Publications </a> <br>
                                <a href="" target="link"> Editorial Notes </a> <br>
                                <a href="" target="link"> Abbreviations </a> <br>
                                <a href="" target="link"> Bibliography </a> <br>
                                <br>
                                <a href="" target="blank">PDF Version of this Article </a>
                                <br>
                                <a href="" target="link"> Get Acrobat Reader </a> <br>
                                <a href="" target="link"> <font color="#800517"><b>Download Cuneiform Font</b></font> </a>
                            </font>
                        </p>
                    </td>
                    <td width="1" rowspan="2" align="right" nowrap bgcolor="#99CCCC"> &nbsp; </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            <font face="Skia, Verdana, Arial, Helvetica, sans-serif">
                                <h2 style="font-family: Skia, Verdana, Arial, Helvetica, sans-serif">
                                    <br>
                                    <p><?= h($article->title) ?></p>
                                </h2>
                                <b><p>
                                    <?php if ($article->authors): ?>
                                        <?= implode(', ', array_map(function ($author) {
                                            return $author->author;
                                        }, $article->authors)) ?>
                                    <?php endif; ?>
                                </p</b>
                                <br>
                                <i>University</i>
                                <br>
                                <br>
                                <b>Keywords</b><br>
                            </font>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <hr align="center" width="600" size="2">
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="left">
                        <br>
                        <p>
                            <i><b>Abstract</b><br><br></i>
                        </p>
                        <div><?= $article->content_html ?></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
