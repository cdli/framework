<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Utility\Latex\Renderer\Html $renderer
 * @var \App\Utility\Latex\Extractor $extractor
 * @var \PhpLatex_Node $document
 */

$this->disableAutoLayout();
?>

<?= $renderer->render($document) ?>

<?php if (!empty($extractor->footnotes)): ?>
    <hr>
    <p class="text-center">
        <b>Footnotes</b>
    </p>
    <hr>
    <ul class="list-unstyled">
        <?php foreach ($extractor->footnotes as $footnote): ?>
            <li id="f<?= $footnote['index'] ?>">
                <sup>
                    <a href="#r<?= $footnote['index'] ?>">[<?= $footnote['index'] ?>]</a>
                </sup>
                <?= $renderer->renderFootnote($footnote) ?>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

<hr>
<p class="text-center">
    <b>Version:</b> <?= date('Y-m-d') ?>
</p>
<hr>
