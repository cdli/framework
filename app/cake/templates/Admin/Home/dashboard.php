<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artifact $artifact
 */
$roles = $this->Identity->get('roles');
$name = $this->Identity->get('username');

$ifRoleExists = !is_null($roles) ?  array_intersect($roles, [1, 2, 12]) : [];
$ifRoleExists = !empty($ifRoleExists) ? 1 : 0;
?>
<div class="mr-5">
    <div class="left1">
        <span class="display-4 dash-txt ls ml-4">Administrative Dashboard</span>
        <span class="display-4 dash-txt ss">Admin Dashboard</span>
    </div>
    <div class="right-add-btn ls">
        <div class="dropdown">
            <button class="btn cdli-btn-blue" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-plus-circle plus-icon" aria-hidden="true"> <a class="add-btn">Add</a></span></button>
            <div class="dropdown-menu dropdown-menu-right shadow">
                <?= $this->Html->link(
                    'Add Artifact',
                    ['prefix' => false, 'controller' => 'Artifacts', 'action' => 'add'],
                    ['class' => 'dropdown-item']
                );?>
                <?= $this->Html->link(
                    'Add Author',
                    ['controller' => 'Authors', 'action' => 'add'],
                    ['class' => 'dropdown-item']
                );?>
                <?= $this->Html->link(
                    'Add Posting',
                    ['controller' => 'Postings', 'action' => 'add'],
                    ['class' => 'dropdown-item']
                );?>
                <?= $this->Html->link(
                    'Add Publication',
                    ['prefix' => false, 'controller' => 'Publications', 'action' => 'add'],
                    ['class' => 'dropdown-item']
                );?>
            </div>
        </div>
    </div>
</div>
<div class="emp"></div>
<div class="emp ls"></div>

<div class="ml-4 text-left">

        <span class="fa fa-file-text-o"></span> <?php echo  $this->Html->link(
            'Documentation',
            ['prefix' => false, 'controller' => 'Docs', 'action' => 'index'],
        );?>
    </a>
</div>

<div class="accordion my-acc">
    <?= $this->Accordion->partOpen('artifacts', 'Artifacts, Texts and Annotations', 'h2') ?>
        <div>
            <div class="row mt-3">
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Artifacts</h3>
                        <p class="admin-text">Add a single new artifact or upload a csv file containing updates or new entries.</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                'Add New',
                                ['prefix' => false, 'controller' => 'Artifacts', 'action' => 'add'],
                                ['class' => 'btn cdli-btn-blue btn-block']
                            );?>
                            <?= $this->Html->link(
                                'Bulk Add or Update',
                                ['prefix' => false, 'controller' => 'ArtifactsUpdates', 'action' => 'add'],
                                ['class' => 'btn cdli-btn-blue btn-block']
                            );?>
                        </div>
                    </div>
                </div>
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Texts and Annotations</h3>
                        <p class="admin-text">Add a single insciption or annotation, or many.</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                'Single and Bulk Add/Update',
                                ['prefix' => false, 'controller' => 'Inscriptions', 'action' => 'add'],
                                ['class' => 'btn cdli-btn-blue btn-block']
                            );?>
                        </div>
                    </div>
                </div>
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Review changes</h3>
                        <p class="admin-text">Review changes to artifacts and texts submitted by users.</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                __('Review changes'),
                                ['controller' => 'UpdateEvents', 'action' => 'index'],
                                ['class' => 'btn cdli-btn-blue btn-block']
                            );?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Review auto-generated text data</h3>
                        <p class="admin-text">Review auto-generated annotations.</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                __('Review'),
                                ['controller' => 'Inscriptions', 'action' => 'index'],
                                ['class' => 'btn cdli-btn-blue btn-block']
                            );?>
                        </div>
                    </div>
                </div>
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Token lists</h3>
                        <p class="admin-text">Manage sign and word lists for the periods.</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                __('View token lists'),
                                ['controller' => 'Periods', 'action' => 'tokenLists'],
                                ['class' => 'btn cdli-btn-blue btn-block']
                            );?>
                        </div>
                    </div>
                </div>
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Retired Artifacts</h3>
                        <p class="admin-text">View the index page for retired artifacts.</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                'View Index Page',
                                ['controller' => 'Artifacts', 'action' => 'retired'],
                                ['class' => 'btn cdli-btn-blue btn-block']
                            );?>
                        </div>
                    </div>
                </div>
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Featured Seals</h3>
                        <p class="admin-text">Add a single featured seal, or many.</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                'Add New',
                                ['controller' => 'Artifacts', 'action' => 'featuredSealsAdd'],
                                ['class' => 'btn cdli-btn-blue btn-block']
                            );?>
                        </div>
                    </div>
                </div>
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Seal Chemistry</h3>
                        <p class="admin-text">Add chemical composition for various seals</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                'Add seal chemistry',
                                ['controller' => 'Artifacts', 'action' => 'sealChemistryAdd'],
                                ['class' => 'btn cdli-btn-blue btn-block']
                            );?>
                        </div>
                    </div>
                </div>
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Calibration Data</h3>
                        <p class="admin-text">Add standards for chemical data of seals</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                'Add calibration data',
                                ['controller' => 'Artifacts', 'action' => 'calibrationAdd'],
                                ['class' => 'btn cdli-btn-blue btn-block']
                            );?>
                        </div>
                    </div>
                </div>
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Image annotations</h3>
                        <p class="admin-text">Add image annotations.</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                'Upload',
                                ['controller' => 'ArtifactAssetAnnotations', 'action' => 'add', 'prefix' => false],
                                ['class' => 'btn cdli-btn-blue btn-block']
                            );?>
                            <?= $this->Html->link(
                                'Documentation',
                                ['controller' => 'Docs', 'action' => 'view', 'adding-and-editing-annotations', 'prefix' => false],
                                ['class' => 'btn cdli-btn-blue btn-block']
                            );?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?= $this->Accordion->partClose() ?>
    <?= $this->Accordion->partOpen('journals-and-postings', 'CDLI Journals and Postings', 'h2') ?>
        <div>
            <div class="row mt-3">
                <div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Jounal Articles</h3>
                        <p class="admin-text">Our journals: CDLB, J, P and Notes.</p>
                        <div class="mt-auto text-center">
                            <a href="#">
                                <button id="dashboardbtn11" class="btn cdli-btn-light dashboardbtn1" disabled>OJS Dashboard</button>
                            </a>
                            <?= $this->Html->link(
                                'Publish Article',
                                ['prefix' => 'Admin', 'controller' => 'Articles', 'action' => 'add'],
                                ['class' => 'btn cdli-btn-blue btn-block']
                            );?>
                        </div>
                    </div>
                </div>
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Postings</h3>
                        <p class="admin-text">CDLI News, information pages, & Highlights (100's).</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                'Index',
                                ['prefix' => 'Admin', 'controller' => 'Postings'],
                                ['class' => 'btn cbtn cdli-btn-light dashboardbtn1']
                            );?>
                            <?= $this->Html->link(
                                'Add New',
                                ['prefix' => 'Admin', 'controller' => 'Postings', 'action' => 'add'],
                                ['class' => 'btn cdli-btn-blue dashboardbtn2']
                            );?>
                        </div>
                    </div>
                </div>
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">CDLI Tablet</h3>
                        <p class="admin-text">Our mobile app.</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                'Index',
                                ['prefix' => 'Admin', 'controller' => 'CdliTablet'],
                                ['class' => 'btn cbtn cdli-btn-light dashboardbtn1']
                            );?>
                            <?= $this->Html->link(
                                'Add New',
                                ['prefix' => 'Admin', 'controller' => 'CdliTablet', 'action' => 'add'],
                                ['class' => 'btn cdli-btn-blue dashboardbtn2']
                            );?>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    <?= $this->Accordion->partClose() ?>
    <?= $this->Accordion->partOpen('bib', 'Bibliography Management', 'h2') ?>
        <div>
            <div class="row mt-3">
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Authors</h3>
                        <p class="admin-text">Scholars, scientists and other contributors of references, cdl articles, and cdli content.</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                __('Index'),
                                ['prefix' => false, 'controller' => 'Authors', 'action' => 'index'],
                                ['class' => 'btn cdli-btn-light dashboardbtn1']
                            ) ?>
                            <?= $this->Html->link(
                                __('Add New'),
                                ['controller' => 'Authors', 'action' => 'add'],
                                ['class' => 'btn cdli-btn-blue dashboardbtn2']
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">References</h3>
                        <p class="admin-text">Add a new book or journal article to the CDLI extended bibliography.</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                __('Index'),
                                ['prefix' => false, 'controller' => 'Publications', 'action' => 'index'],
                                ['class' => 'btn align-self-end  cdli-btn-light dashboardbtn1']
                            ) ?>
                            <?= $this->Html->link(
                                __('Add New'),
                                ['prefix' => false, 'controller' => 'Publications', 'action' => 'add'],
                                ['class' => 'btn align-self-end  cdli-btn-blue dashboardbtn2']
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Bulk add References</h3>
                        <p class="admin-text">Submit a file of new publications.</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                               'Bulk Add',
                               ['prefix' => false, 'controller' => 'Publications', 'action' => 'upload'],
                               ['class' => 'btn cdli-btn-blue btn-block']
                           );?>
                       </div>
                    </div>
                </div>
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Merge References</h3>
                        <p class="admin-text">References cleaning tool to merge existing references together.</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                __('Select'),
                                ['prefix' => false, 'controller' => 'Publications', 'action' => 'merge', '?' => ['step' => 'select']],
                                ['class' => 'btn cdli-btn-blue dashboardbtn3']
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Entity-Publication links</h3>
                        <p class="admin-text">Manage the association of entities with their publications</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                __('Bulk Add'),
                                ['prefix' => false, 'controller' => 'EntitiesPublications', 'action' => 'add'],
                                ['class' => 'btn align-self-end cdli-btn-blue dashboardbtn2']
                            ) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?= $this->Accordion->partClose() ?>
    <?= $this->Accordion->partOpen('images', 'Image Management', 'h2') ?>
        <div>
            <div class="row mt-3">
                <div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Upload Raw Scans</h3>
                        <p class="admin-text">Digital scans of artifacts uplaod, combining and management.</p>
                        <?= $this->Html->link(
                            __('Add New'),
                            ['controller' => 'Uploads', 'action' => 'index'],
                            ['class' => 'mt-auto btn cdli-btn-blue dashboardbtn3']
                        ) ?>
                    </div>
                </div>
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Manage Web Images</h3>
                        <p class="admin-text">Management of unlinked / orphan images.</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                'List Web Images',
                                ['prefix' => false, 'controller' => 'ArtifactAssets', 'action' => 'index'],
                                ['class' => 'btn cbtn cdli-btn-blue btn-block']
                            ) ?>
                            <?= $this->Html->link(
                                'Manage Web Images',
                                ['prefix' => 'Admin', 'controller' => 'ArtifactAssets', 'action' => 'index'],
                                ['class' => 'btn cbtn cdli-btn-blue btn-block']
                            ) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?= $this->Accordion->partClose() ?>
    <?= $this->Accordion->partOpen('people', 'Users and Staff', 'h2') ?>
        <div>
            <div class="row mt-3">
                <div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Users</h3>
                        <p class="admin-text">Access and users on this website.</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                'Index',
                                ['prefix' => 'Admin', 'controller' => 'Users'],
                                ['class' => 'btn cbtn cdli-btn-light dashboardbtn1']
                            );?>
                        </div>
                    </div>
                </div>
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Staff</h3>
                        <p class="admin-text">Additional information on authors contributing to the cdli.</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                __('Index'),
                                ['prefix' => false, 'controller' => 'Staff', 'action' => 'index'],
                                ['class' => 'btn cdli-btn-light dashboardbtn1']
                            ) ?>
                            <?= $this->Html->link(
                                __('Add New'),
                                ['controller' => 'Staff', 'action' => 'add'],
                                ['class' => 'btn cdli-btn-blue dashboardbtn2']
                            ) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?= $this->Accordion->partClose() ?>
    <?= $this->Accordion->partOpen('data-and-metadata', 'Data and Metadata Entities', 'h2') ?>
        <div>
            <?php if ($ifRoleExists): ?>
                <p class="more-ent-mobile btn cdli-btn-light moreentity-btn dropdown-toggle mt-3" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More Entities</p>
                <ul class="dropdown-menu dropdown-menu1 shadow multi-column columns-3">
                    <div class="row">
                        <div class="col-sm-4">
                            <ul class="multi-column-dropdown">
                                <li><?= $this->Html->link(__('Artifact Types'), ['prefix' => false, 'controller' => 'ArtifactTypes', 'action' => 'index']) ?></li>
                                <li><?= $this->Html->link(__('Regions'), ['prefix' => false, 'controller' => 'Regions', 'action' => 'index']) ?></li>
                                <li><?= $this->Html->link(__('Periods'), ['prefix' => false, 'controller' => 'Periods', 'action' => 'index']) ?></li>
                                <li><?= $this->Html->link(__('Proveniences'), ['prefix' => false, 'controller' => 'Proveniences', 'action' => 'index']) ?></li>
                                <li><?= $this->Html->link(__('Rulers'), ['prefix' => false, 'controller' => 'Rulers', 'action' => 'index']) ?></li>
                                <li><?= $this->Html->link(__('Sponsors'), ['prefix' => false, 'controller' => 'Sponsors', 'action' => 'index']) ?></li>
                            </ul>
                        </div>
                        <div class="col-sm-4">
                            <ul class="multi-column-dropdown">
                                <li><?= $this->Html->link(__('Genres'), ['prefix' => false, 'controller' => 'Genres', 'action' => 'index']) ?></li>
                                <li><?= $this->Html->link(__('Materials'), ['prefix' => false, 'controller' => 'Materials', 'action' => 'index']) ?></li>
                                <li><?= $this->Html->link(__('Materials Aspects'), ['prefix' => false, 'controller' => 'MaterialsAspects', 'action' => 'index']) ?></li>
                                <li><?= $this->Html->link(__('Materials Colors'), ['prefix' => false, 'controller' => 'MaterialsColors', 'action' => 'index']) ?></li>
                            </ul>
                        </div>
                        <div class="col-sm-4">
                            <ul class="multi-column-dropdown">
                                <li><?= $this->Html->link(__('Collections'), ['prefix' => false, 'controller' => 'Collections', 'action' => 'index']) ?></li>
                                <li><?= $this->Html->link(__('Dynasties'), ['prefix' => false, 'controller' => 'Dynasties', 'action' => 'index']) ?></li>
                                <li><?= $this->Html->link(__('Languages'), ['prefix' => false, 'controller' => 'Languages', 'action' => 'index']) ?></li>
                                <li><?= $this->Html->link(__('Dates'), ['prefix' => false, 'controller' => 'Dates', 'action' => 'index']) ?></li>
                            </ul>
                        </div>
                    </div>
                </ul>
            <?php endif; ?>
            <div class="row mt-4">
                <div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Archives</h3>
                        <p class="admin-text">Historical archives of cuneiform tablets.</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                __('Index'),
                                ['prefix' => false, 'controller' => 'Archives', 'action' => 'index'],
                                ['class' => 'btn cdli-btn-light dashboardbtn1']
                            ) ?>
                            <?= $this->Html->link(
                                __('Add New'),
                                ['controller' => 'Archives', 'action' => 'add'],
                                ['class' => 'btn cdli-btn-blue dashboardbtn2']
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Journals</h3>
                        <p class="admin-text">List of periodicals which publish articles relevant to the field.</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                __('Index'),
                                ['prefix' => false, 'controller' => 'Journals', 'action' => 'index'],
                                ['class' => 'btn cdli-btn-light dashboardbtn1']
                            ) ?>
                            <?= $this->Html->link(
                                __('Add New'),
                                ['controller' => 'Journals', 'action' => 'add'],
                                ['class' => 'btn cdli-btn-blue dashboardbtn2']
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">External Resources</h3>
                        <p class="admin-text">Pojects and other ressources which can be linked to artifacts and other entities (eg. BDTNS)</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                __('Index'),
                                ['prefix' => false, 'controller' => 'ExternalResources', 'action' => 'index'],
                                ['class' => 'btn cdli-btn-light dashboardbtn1']
                            ) ?>
                            <?= $this->Html->link(
                                __('Add New'),
                                ['controller' => 'ExternalResources', 'action' => 'add'],
                                ['class' => 'btn cdli-btn-blue dashboardbtn2']
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Abbreviations</h3>
                        <p class="admin-text">List of abbreviation for the field.</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                __('Index'),
                                ['prefix' => false, 'controller' => 'Abbreviations', 'action' => 'index'],
                                ['class' => 'btn cdli-btn-light dashboardbtn1']
                            ) ?>
                            <?= $this->Html->link(
                                __('Add New'),
                                ['controller' => 'Abbreviations', 'action' => 'add'],
                                ['class' => 'btn cdli-btn-blue dashboardbtn2']
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Agade Mails</h3>
                        <p class="admin-text">Cooked version of the agade mails.</p>
                        <?= $this->Html->link(
                            __('Index'),
                            ['prefix' => false, 'controller' => 'AgadeMails', 'action' => 'index'],
                            ['class' => 'mt-auto btn cdli-btn-light dashboardbtn3']
                        ) ?>
                    </div>
                </div>
                <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Sign Readings</h3>
                        <p class="admin-text">Sign readings and management of preferred values.</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                __('Index'),
                                ['prefix' => false, 'controller' => 'SignReadings', 'action' => 'index'],
                                ['class' => 'btn cdli-btn-light dashboardbtn1']
                            ) ?>
                            <?= $this->Html->link(
                                __('Add New'),
                                ['controller' => 'SignReadings', 'action' => 'add'],
                                ['class' => 'btn cdli-btn-blue dashboardbtn2']
                            ) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?= $this->Accordion->partClose() ?>
    <?= $this->Accordion->partOpen('site-status', 'Site status', 'h2') ?>
        <div>
            <div class="row mt-3">
                <div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                    <div class="d-flex flex-column card-body text-left border">
                        <h3 class="mb-4">Error rates</h3>
                        <p class="admin-text">Rates of warnings and errors (CakePHP).</p>
                        <div class="mt-auto text-center">
                            <?= $this->Html->link(
                                'Index',
                                ['prefix' => 'Admin', 'controller' => 'SiteStatus'],
                                ['class' => 'btn cbtn cdli-btn-light dashboardbtn1']
                            );?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?= $this->Accordion->partClose() ?>
</div>
<div class="ss right-add-btn mt-4 mb-4">
    <div class="dropdown">
        <img src="/images/Group146.svg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <div class="dropdown-menu dropdown-menu-right shadow">
            <?= $this->Html->link(
               'Add Artifact',
               ['controller' => 'Artifacts', 'action' => 'add'],
               ['class' => 'dropdown-item']
           );?>
           <?= $this->Html->link(
               'Add Author',
               ['prefix' => 'Admin', 'controller' => 'Authors', 'action' => 'add'],
               ['class' => 'dropdown-item']
           );?>
           <?= $this->Html->link(
               'Add Posting',
               ['prefix' => 'Admin', 'controller' => 'Postings', 'action' => 'add'],
               ['class' => 'dropdown-item']
           );?>
           <?= $this->Html->link(
               'Add Publication',
               ['prefix' => 'Admin', 'controller' => 'Publications', 'action' => 'add'],
               ['class' => 'dropdown-item']
           );?>
       </div>
    </div>
</div>

<?php echo $this->element('smoothscroll'); ?>
