<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactType[]|\Cake\Collection\CollectionInterface $artifactTypes
 */
?>

<div class="d-flex justify-content-between align-items-end mb-2">
<h3 class="display-4 pt-3"><?= __('Artifact Types') ?></h3>
<?= $this->element('addButton'); ?>   
</div>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('artifact_type') ?></th>
            <th scope="col"><?= $this->Paginator->sort('parent_id') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactTypes as $artifactType): ?>
        <tr>
            <!-- <td><?= $this->Number->format($artifactType->id) ?></td> -->
            <td><?= $this->Html->link(
                $artifactType->artifact_type,
                ['prefix' => false, 'controller' => 'ArtifactTypes', 'action' => 'view', $artifactType->id]
            ) ?></td>
            <td><?= $artifactType->has('parent_artifact_type') ? $this->Html->link($artifactType->parent_artifact_type->id, ['controller' => 'ArtifactTypes', 'action' => 'view', $artifactType->parent_artifact_type->artifact_type]) : '' ?></td>
            <td>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $artifactType->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>
