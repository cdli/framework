<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactType $artifactType
 */
?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($artifactType) ?>
            <legend class="form-heading mb-3"><?= __('Add Artifact Type') ?></legend>
            <hr class="form-hr mb-4"/>
            <?php
                echo $this->Form->control('artifact_type', ['class' => 'form-control w-100 mb-3', 'type' => 'text', 'required' => true]);
                echo $this->Form->control('parent_id', ['options' => $parentArtifactTypes, 'empty' => true, 'class' => 'form-control w-100 mb-3 custom-form-select']);
            ?>

        <?= $this->Form->button(__('Submit'),['class'=>'btn cdli-btn-blue']) ?>
        <?= $this->Form->end() ?>

    </div>

</div>
