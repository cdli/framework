<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Author $author
 */
?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($author) ?>
            <legend class="form-heading mb-3"><?= __('Add Author') ?></legend>
            <hr class="form-hr mb-4"/>
                <?php echo $this->Form->control('first', ['class' => 'form-control w-100 mb-3', 'label' => 'First Name', 'type' => 'text', 'maxLength' => 149, 'required' => true]) ?>
                <?php echo $this->Form->control('last', ['class' => 'form-control w-100 mb-3', 'label' => 'Last Name', 'type' => 'text', 'maxLength' => 149, 'required' => true]) ?>
                <?php echo $this->Form->control('east_asian_order', ['class' => 'mb-3 mr-1', 'label' => 'East Asian Order', 'type' => 'checkbox']) ?>
                <?php echo $this->Form->control('email', ['class' => 'form-control w-100', 'label' => 'Email', 'type' => 'text', 'maxLength' => 150, 'error' => false]) ?>
                <div class="mb-3">
                    <?php
                     if($this->Form->isFieldError('email')) {
                         echo $this->Form->error('email','The entered email address is invalid');}
                     ?>
                </div>
                <?php echo $this->Form->control('institution', ['class' => 'form-control w-100 mb-3', 'label' => 'Institution', 'type' => 'text', 'maxLength' => 255]) ?>
                <?php echo $this->Form->control('orcid_id', ['class' => 'form-control w-100', 'label' => 'ORCID ID', 'type' => 'text', 'maxLength' => 19, 'error' => false]) ?>
                <div class="mb-3">
                    <?php
                     if($this->Form->isFieldError('orcid_id')) {
                         echo $this->Form->error('orcid_id','The ORCID ID needs to be a 16 digit number');}
                     if(!$this->Form->isFieldError('orcid_id')) {
                         echo '<small id="orcid_info" class="form-text text-muted">The ORCID ID needs to be a 16 digit number.</small>';}
                     ?>
                </div>
                <?= $this->Form->control('birth_year', ['class' => 'form-control w-100 mb-3', 'max' => '9999']) ?>
                <?= $this->Form->control('death_year', ['class' => 'form-control w-100 mb-3', 'max' => '9999']) ?>
                <?php echo $this->Form->control('biography', ['class' => 'form-control w-100', 'label' => 'Biography', 'type' => 'textarea', 'maxLength' => 65000]) ?>

                <div class="mb-4"></div>

            <div class="pt-2">
                <?= $this->Form->button(__('Submit'),['class'=>'btn btn-primary cdli-btn-blue mr-2', 'id' => 'submit']) ?>
                <?= $this->Form->end() ?>
            </div>
    </div>
</div>

<!-- Add CKEDITOR to Body Field -->
<?= $this->element('ckeditor', ['id' => 'biography']) ?>
