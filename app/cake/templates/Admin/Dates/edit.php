<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Date $date
 */
?>

<div class="row justify-content-md-center">

<div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($date) ?>
            <legend class="form-heading mb-3"><?= __('Edit Date') ?></legend>
            <hr class="form-hr mb-4"/>
            <?php
                echo $this->Form->control('day_no',  ['class' => 'form-control w-100 mb-3', 'type' => 'text', 'required' => true]);
                echo $this->Form->control('day_remarks', ['class' => 'form-control w-100 mb-3', 'type' => 'textarea']);
                echo $this->Form->control('month_id', ['options' => $months, 'empty' => true, 'class' => 'form-control w-100 mb-3 custom-form-select']);
                echo $this->Form->control('is_uncertain');
                echo $this->Form->control('month_no', ['class' => 'form-control w-100 mb-3', 'type' => 'text']);
                echo $this->Form->control('year_id', ['options' => $years, 'empty' => true, 'class' => 'form-control w-100 mb-3 custom-form-select']);
                echo $this->Form->control('dynasty_id', ['options' => $dynasties, 'empty' => true, 'class' => 'form-control w-100 mb-3 custom-form-slect']);
                echo $this->Form->control('ruler_id', ['options' => $rulers, 'empty' => true, 'class' => 'form-control w-100 mb-3 custom-form-select']);
                echo $this->Form->control('absolute_year', ['class' => 'form-control w-100 mb-3', 'type' => 'text']);
            ?>

        <div>
        <?= $this->Form->button(__('Save'),['class'=>'btn cdli-btn-blue']) ?>
        <?= $this->Form->end() ?>
        <?= $this->element('deleteModal', [ 'entity_name' => $date->date, 'id' => $date->id]); ?>
        </div>

    </div>

</div>
