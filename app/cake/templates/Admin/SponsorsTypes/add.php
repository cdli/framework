<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SponsorType $sponsorType
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Sponsor Types'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Sponsor'), ['controller' => 'Sponsor', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sponsor'), ['controller' => 'Sponsor', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="sponsorTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($sponsorType) ?>
    <fieldset>
        <legend><?= __('Add Sponsor Type') ?></legend>
        <?php
            echo $this->Form->control('sponsor_type');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
    <?= $this->Form->end() ?>
</div>
