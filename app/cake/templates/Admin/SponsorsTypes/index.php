<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SponsorType[]|\Cake\Collection\CollectionInterface $sponsorTypes
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sponsor Type'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sponsor'), ['controller' => 'Sponsor', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sponsor'), ['controller' => 'Sponsor', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="sponsorTypes index large-9 medium-8 columns content">
    <h3><?= __('Sponsor Types') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sponsor_type') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($sponsorTypes as $sponsorType): ?>
            <tr>
                <td><?= $this->Number->format($sponsorType->id) ?></td>
                <td><?= h($sponsorType->sponsor_type) ?></td>
                <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view',  $sponsorType->id],['escape' => false,'class'=>"btn btn-primary btn-sm"]) ?>
            <?= $this->Html->link(__('Edit'), ['action' => 'edit',  $sponsorType->id],['escape' => false,'class'=>"btn btn-warning btn-sm"]) ?>
            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete',  $sponsorType->id], ['confirm' => __('Are you sure you want to delete # {0}?',  $sponsorType->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php echo $this->element('Paginator'); ?>
</div>
