<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inscriptions[]|\Cake\Collection\CollectionInterface $inscriptions
 */
?>

<div class="row">
    <div class="boxed col-12">
        <h3 class="display-4 pt-3"><?= __('Annotations needing review') ?></h3>

        <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
            <thead align="left">
                <tr>
                    <th scope="col"><?= __('Artifact') ?></th>
                    <th scope="col"><?= __('Text revision') ?></th>
                    <th scope="col"><?= __('Updated') ?></th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($inscriptions as $inscription): ?>
                    <tr align="left">
                        <td>
                            <?= $this->Html->link(
                                $inscription->artifact->designation,
                                ['prefix' => false, 'controller' => 'Artifacts', 'action' => 'view', $inscription->artifact_id]
                            ) ?>
                        </td>
                        <td>
                            <?= $this->Html->link(
                                $inscription->id,
                                ['controller' => 'Inscriptions', 'action' => 'view', $inscription->id]
                            ) ?>
                        </td>
                        <td><?= $inscription->update_event->approved ?></td>
                        <td>
                            <?= $this->Html->link(
                                __('Review'),
                                ['controller' => 'Inscriptions', 'action' => 'view', $inscription->id],
                                ['class' => 'btn cdli-btn-blue']
                            ) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <?php echo $this->element('Paginator'); ?>
    </div>
</div>
