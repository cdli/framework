<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inscription $inscription
 */
?>

<div class="row justify-content-md-center">
    <div class="col-lg-6 boxed">
        <h3 class="display-4 pt-3"><?= __('Review generated annotation') ?></h3>

        <p>
            The ATF transliteration of this artifact was changed. On the left,
            this change is shown with removed lines highlighted in red and added
            lines highlighted in green. Based on these changes and the existing
            annotation, the CoNLL annotation was updated, with changes highlighted
            in the same way.
        </p>

        <p>
            Please compare the transliteration with its annotated counterpart on
            the right. If the previous and recent annotations have been merged
            successfully, click "<?= __('Resolve') ?>". If not, you can edit the
            annotation by clicking on "<?= __('Edit annotation') ?>".
        </p>

        <div>
            <?= $this->Form->create(null, [
                'class' => 'float-left',
                'url' => ['resolve', $inscription->id]
            ]) ?>
                <?= $this->Form->submit(__('Resolve'), ['class' => 'btn cdli-btn-blue']) ?>
            <?= $this->Form->end() ?>

            <?= $this->Form->create(null, [
                'class' => 'float-right',
                'url' => [
                    'prefix' => false,
                    'controller' => 'Inscriptions',
                    'action' => 'edit',
                    $inscription->id,
                    '?' => ['tab' => 'conll']
                ]
            ]) ?>
                <?= $this->Form->submit(__('Edit annotation'), ['class' => 'btn cdli-btn-gray']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<div class="boxed">
    <div class="row justify-content-md-center">
        <div class="col-lg-12">
            <h2>
                <?= __(
                    '{0} from {1}',
                    $this->Html->link(
                        h($inscription->artifact->designation),
                        ['prefix' => false, 'controller' => 'Artifacts', 'action' => 'view', $inscription->artifact->id]
                    ),
                    $this->element('updateEventHeader', ['update_event' => $inscription->update_event])
                ) ?>
            </h2>
        </div>
    </div>

    <div class="row justify-content-md-center boxed">
        <div class="col-6">
            <h3><?= h('Transliteration') ?></h3>
            <pre><?= $this->Diff->diffInscription($oldInscription, $inscription, 'atf') ?></pre>
        </div>
        <div class="col-6">
            <h3><?= h('Annotation') ?></h3>

            <?php if ($inscription->has('annotation')): ?>
                <?php if (str_contains('+', $inscription->annotation)): ?>
                    <div class="alert alert-danger">
                        <?= __('Annotation has merge conflicts ("+")') ?>
                    </div>
                <?php endif; ?>

                <?php if (str_contains('?', $inscription->annotation)): ?>
                    <div class="alert alert-warning">
                        <?= __('Annotation is missing information ("?")') ?>
                    </div>
                <?php endif; ?>

                <pre><?= $this->Diff->diffInscription($oldInscription, $inscription, 'annotation') ?></pre>
            <?php endif; ?>
        </div>
    </div>
</div>
