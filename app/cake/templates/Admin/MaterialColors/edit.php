<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MaterialColor $materialColor
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($materialColor) ?>
            <legend class="form-heading mb-3"><?= __('Edit Material Color') ?></legend>
            <hr class="form-hr mb-4"/>
            <?php
                echo $this->Form->control('material_color', ['class' => 'form-control w-100 mb-3', 'type' => 'text', 'required' => true]);
            ?>

        <div>
        <?= $this->Form->button(__('Save'), ['class'=>'btn cdli-btn-blue']) ?>
        <?= $this->Form->end() ?>
        <?= $this->element('deleteModal', [ 'entity_name' => $materialColor->material_color, 'id' => $materialColor->id]); ?>
        </div>

    </div>

</div>
