<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Ruler $ruler
 */
?>

<div class="row justify-content-md-center">

<div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($ruler) ?>
            <legend class="form-heading mb-3"><?= __('Edit Ruler') ?></legend>
            <hr class="form-hr mb-4"/>
            <?php
                echo $this->Form->control('sequence', ['class' => 'form-control w-100 mb-3', 'type' => 'text']);
                echo $this->Form->control('ruler', ['class' => 'form-control w-100 mb-3', 'type' => 'text', 'required' => true]);
                echo $this->Form->control('period_id', ['options' => $periods, 'empty' => true, 'class' => 'form-control w-100 mb-3 custom-form-select']);
                echo $this->Form->control('dynasty_id', ['options' => $dynasties, 'empty' => true, 'class' => 'form-control w-100 mb-3 custom-form-select']);
            ?>

        <div>
        <?= $this->Form->button(__('Save'), ['class'=>'btn cdli-btn-blue']) ?>
        <?= $this->Form->end() ?>
        <?= $this->element('deleteModal', [ 'entity_name' => $ruler->ruler, 'id' => $ruler->id]); ?>
        </div>

    </div>

</div>
