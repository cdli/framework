<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sponsor $sponsor
 */

$this->assign('title', ($sponsor->isNew() ? 'New' : h($sponsor->sponsor)) . ' - ' . __('Proveniences'));
?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($sponsor, ['type' => 'file']) ?>
            <legend class="form-heading mb-3">
                <?php if ($sponsor->isNew()): ?>
                    <?= __('Add Sponsor') ?>
                <?php else: ?>
                    <?= __('Edit Sponsor {0}', h($sponsor->sponsor)) ?>
                <?php endif; ?>
            </legend>
            <hr class="form-hr mb-4"/>

            <?= $this->Form->control('sponsor_type_id', ['options' => $sponsorTypes, 'class' => 'form-control w-100 mb-3 custom-form-select']) ?>
            <?= $this->Form->control('sponsor', ['class' => 'form-control w-100 mb-3', 'type' => 'text']) ?>
            <?= $this->Form->control('url', ['class' => 'form-control w-100 mb-3', 'type' => 'text']) ?>
            <?= $this->Form->control('contribution', ['class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('sequence', ['class' => 'form-control w-100 mb-4']) ?>

            <div class="form-group border rounded p-3">
                <p><?= __('Image') ?></p>
                <div class="mb-3">
                    <?= $this->Html->image($sponsor->getImage(), ['id' => 'image-preview', 'width' => 100, 'class' => 'border']) ?>
                </div>

                <?= $this->Form->control('image_delete', ['type' => 'hidden', 'value' => '0']) ?>
                <?= $this->Form->control('image_upload', ['type' => 'file', 'class' => 'form-control w-100 mb-3', 'label' => false, 'accept' => 'image/*']) ?>
                <?= $this->Form->button(__('Remove'), ['id' => 'image-remove', 'class' => 'btn cdli-btn-blue']) ?>
            </div>

            <?= $this->Form->button(__('Save'),['class' => 'btn cdli-btn-blue mr-2', 'id' => 'submit']) ?>
        <?= $this->Form->end() ?>

        <?php if (!$sponsor->isNew()): ?>
            <?= $this->element('deleteModal', ['entity_name' => $sponsor->sponsor, 'id' => $sponsor->id]) ?>
        <?php endif; ?>
    </div>
</div>

<?= $this->element('ckeditor', ['id' => 'contribution']) ?>
<?php $this->append('script'); ?>
<script>
    $(function () {
        $('#image-remove').click(function (event) {
            event.preventDefault()
            $('#image-preview').addClass('d-none')
            $('#image-delete').val('1')
            $('#image-upload').val('')
        })

        $('#image-upload').change(function () {
            if (this.files.length) {
                $('#image-delete').val('0')
                $('#image-preview').attr('src', URL.createObjectURL(this.files[0]))
                $('#image-preview').removeClass('d-none')
            } else {
                $('#image-preview').addClass('d-none')
                $('#image-delete').val('1')
            }
        })
    })
</script>
<?php $this->end('script'); ?>
