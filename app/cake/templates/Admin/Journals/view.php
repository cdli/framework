<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal $journal
 */
?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <h3><?= h($journal->journal) ?></h3>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <tr>
                <th><?= __('Journal') ?></th>
                <td><?= h($journal->journal) ?></td>
            </tr>
            <tr>
                <th><?= __('Id') ?></th>
                <td><?= $this->Number->format($journal->id) ?></td>
            </tr>
        </table>
    </div>

    <div class="col-lg-3 boxed">
        <h4 class="heading"><?= __('Actions') ?></h4>
        <ul>
            <li>
                <?= $this->Html->link(__('Edit Journal'), ['action' => 'edit', $journal->id]) ?>
            </li>
            <li>
                <?= $this->Form->postLink(__('Delete Journal'), ['action' => 'delete', $journal->id], ['confirm' => __('Are you sure you want to delete # {0}?', $journal->id)]) ?>
            </li>
        </ul>
    </div>
</div>

<div class="boxed mx-8">
    <h4><?= __('Related Publications') ?></h4>
    <?php if (!empty($journal->publications)) : ?>
        <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" class="table-bootstrap">
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __('Year') ?></th>
                    <th><?= __('Title') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($journal->publications as $publications) : ?>
                <tr>
                    <td><?= $this->Html->link(
                        h($publications->bibtexkey),
                        ['controller' => 'Publications', 'action' => 'view', $publications->id]
                    ) ?></td>
                    <td><?= h($publications->year) ?></td>
                    <td><?= h($publications->title) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Publications', 'action' => 'view', $publications->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'Publications', 'action' => 'edit', $publications->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'Publications', 'action' => 'delete', $publications->id], ['confirm' => __('Are you sure you want to delete # {0}?', $publications->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
    <?php endif; ?>
</div>
