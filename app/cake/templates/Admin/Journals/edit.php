<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal $journal
 */
?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($journal) ?>
                <legend class="form-heading mb-3"><?= __('Edit Journal') ?></legend>
                <hr class="form-hr mb-4"/>
                <?= $this->Form->control('journal', ['class' => 'form-control  w-100 mb-3', 'required' => true]) ?>
                <div>
                    <?= $this->Form->button(__('Save'), ['class' => 'btn btn-primary cdli-btn-blue mr-2']) ?>
                    <?= $this->Form->end() ?>   
                    <?= $this->element('deleteModal', [ 'entity_name' => $journal->journal, 'id' => $journal->id]); ?> 
                </div>
      </div>
</div>
