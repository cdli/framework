<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal $journal
 */
?>
<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($journal) ?>
                <legend class="form-heading mb-3"><?= __('Add Journal') ?></legend>
                <hr class="form-hr mb-4"/>
                <?= $this->Form->control('journal', ['class' => 'form-control  w-100 mb-3', 'required' => true]) ?>
            <?= $this->Form->button(__('Submit'), ['class' => 'btn cdli-btn-blue']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
