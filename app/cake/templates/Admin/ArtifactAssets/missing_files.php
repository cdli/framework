<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactAsset[]|\Cake\Collection\CollectionInterface $artifactAssets
 */
?>
<div class="row justify-content-md-center">
    <div class="col-12 boxed">
        <h3><?= __('Manage broken links') ?></h3>

        <p><?= count($missingFiles) ?> visual assets in the database no longer have associated files.</p>

        <div id="index-progress" class="progress my-3 d-none">
            <div class="progress-bar" role="progressbar" style="width: 0;"
                 aria-valuenow="0" aria-valuemin="0" aria-valuemax="<?= count($missingFiles) ?>"></div>
        </div>

        <div id="index-progress-message" class="alert my-3 d-none" role="alert"></div>

        <?= $this->Form->button(
            __('Remove entries from database'),
            ['id' => 'index', 'class' => 'btn cdli-btn-blue']
        ) ?>

        <div id="index-link" class="d-none mt-3">
            <?= $this->Html->link(
                __('Go back to index'),
                ['prefix' => false, 'controller' => 'ArtifactAssets', 'action' => 'index'],
                ['class' => 'btn cdli-btn-blue']
            ) ?>
        </div>
    </div>
</div>

<?php $this->append('script') ?>
<script type="text/javascript">
    $(function () {
        const progress = $('#index-progress')
        const progressMessage = $('#index-progress-message')
        const files = <?= json_encode(array_map(function ($id) {
            return $this->Url->build(['action' => 'delete', $id]);
        }, array_values($missingFiles))) ?>;
        const csrfToken = <?= json_encode($this->getRequest()->getAttribute('csrfToken')) ?>;

        async function indexFile (file) {
            const response = await fetch(file, {
                method: 'POST',
                headers: { 'X-CSRF-Token': csrfToken }
            })
            if (response.status >= 400) {
                throw new Error(await response.text())
            }
        }

        async function indexFiles () {
            const total = files.length
            let done = 0
            for (const file of files) {
                await indexFile(file)
                done++
                progress.find('.progress-bar').width((100 * done / total) + '%')
                progress.find('.progress-bar').attr('aria-valuenow', done)
                progress.find('.progress-bar').text(done)
            }
        }

        $('#index').one('click', function () {
            progress.removeClass('d-none')
            indexFiles().then(displaySuccess).catch(displayError)
        })

        function displaySuccess () {
            progressMessage.text('Clean-up complete')
            progressMessage.addClass('alert-success')
            progressMessage.removeClass('d-none')
            progress.find('.progress-bar').addClass('bg-success')
            $('#index-link').removeClass('d-none')
        }

        function displayError (error) {
            progressMessage.text(error.message)
            progressMessage.addClass('alert-danger')
            progressMessage.removeClass('d-none')
            progress.find('.progress-bar').addClass('bg-danger')
        }
    })
</script>
<?php $this->end() ?>
