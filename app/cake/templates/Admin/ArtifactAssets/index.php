<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactAsset[]|\Cake\Collection\CollectionInterface $artifactAssets
 */

$this->assign('title', 'Manage visual assets of artifacts');
?>

<div class="row justify-content-md-center">
    <div class="col-12 boxed">
        <?= $this->Html->link(__('List all artifacts'), [
            'prefix' => false,
            'controller' => 'ArtifactAssets',
            'action' => 'index'
        ], ['class' => 'btn cdli-btn-blue float-right']) ?>
        <h3><?= __('Manage visual assets of artifacts') ?></h3>

        <table class="table-bootstrap my-3">
            <thead>
                <tr>
                    <th><?= __('Problem') ?></th>
                    <th><?= __('Count') ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?= __('Files not in the database') ?></td>
                    <td><?= count($fileOnly) ?></td>
                    <td><?= $this->Html->link(__('Manage'), ['action' => 'updateIndex']) ?></td>
                </tr>
                <tr>
                    <td><?= __('No file for database entry') ?></td>
                    <td><?= count($indexOnly) ?></td>
                    <td><?= $this->Html->link(__('Manage'), ['action' => 'missingFiles']) ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
