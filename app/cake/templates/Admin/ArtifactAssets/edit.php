<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactAsset[]|\Cake\Collection\CollectionInterface $artifactAssets
 */

$this->assign('title', 'Edit access of visual asset');
?>

<div class="row justify-content-md-center posting">
    <div class="col-lg-5 p-5 right-panel text-left">
        <h2><?= $asset->getDescription() ?></h2>
        <p><?= h($asset->artifact->designation) ?> (<?= h($asset->artifact->getCdliNumber()) ?>)</p>
        <?= $this->Form->create($asset) ?>
            <div class="form-group">
                <?= $this->Form->control('is_public', [
                    'class' => 'form-control',
                    'type' => 'toggle',
                    'label' => __('Is this visual asset public?')
                ]) ?>
            </div>

            <h3><?= __('License information') ?></h3>
            <?= $this->Form->control('license_id', [
                'class' => 'form-control w-100 mb-3',
                'label' => __('License identifier (SPDX)'),
                'type' => 'text',
            ]) ?>
            <?= $this->Form->control('license_attribution', [
                'class' => 'form-control w-100 mb-3',
                'label' => __('Copyright attribution'),
            ]) ?>
            <?= $this->Form->control('license_comment', [
                'class' => 'form-control w-100 mb-3',
                'label' => __('Licensing info'),
            ]) ?>

            <h3><?= __('Credits') ?></h3>
            <p><?= __('Authors') ?></p>
            <div class="many-to-many">
                <div id="authors-wrapper">
                    <?php foreach (array_merge(array_keys($asset->authors ?? []), ['template']) as $i => $x): ?>
                        <div class="form-group border rounded pt-3 px-3<?= $x == 'template' ? ' d-none' : '' ?>">
                            <?= $this->Form->control("authors.$x.id", ['type' => 'hidden']) ?>
                            <div class="entity-picker" data-for="authors-<?= $x ?>-id" data-search-url="/authors/search">
                                <?= $this->Form->control("authors.$x.author", [
                                    'class' => 'form-control w-100 mb-3',
                                    'autocomplete' => 'off',
                                    'label' => __('Author'),
                                ]) ?>
                                <div class="entity-picker-results">
                                    <ol>
                                    </ol>
                                </div>
                            </div>
                            <?= $this->Form->control("authors.$x._joinData.role", ['class' => 'form-control w-100 mb-3']) ?>
                            <?= $this->Form->iconButton(__('Remove'), 'times-circle') ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?= $this->Form->iconButton(__('Add Author'), 'plus-circle', ['data-for' => 'authors-wrapper']) ?>
            </div>

            <?= $this->element('formManyToMany/publications', ['publications' => $asset->publications]) ?>

            <?= $this->Form->button(__('Submit'), [
                'class' => 'btn cdli-btn-blue',
                'name' => 'action',
                'value' => 'submitted'
            ]) ?>
        <?= $this->Form->end() ?>
    </div>

    <div class="col-lg-7 p-5 left-panel">
        <?= $this->cell('ArtifactAsset', [$asset->id]) ?>
    </div>
</div>

<?php $this->append('script'); ?>
    <?= $this->Html->script('form-control-entity-picker') ?>
    <?= $this->Html->script('form-control-many-to-many') ?>
<?php $this->end('script'); ?>
