<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MaterialAspect $materialAspect
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($materialAspect) ?>
            <legend class="form-heading mb-3"><?= __('Edit Material Aspect') ?></legend>
            <hr class="form-hr mb-4"/>
            <?php
                echo $this->Form->control('material_aspect', ['class' => 'form-control w-100 mb-3', 'type' => 'text', 'required' => true]);
            ?>

        <div>
        <?= $this->Form->button(__('Save'), ['class'=>'btn cdli-btn-blue']) ?>
        <?= $this->Form->end() ?>
        <?= $this->element('deleteModal', [ 'entity_name' => $materialAspect->material_aspect, 'id' => $materialAspect->id]); ?>
        </div>

    </div>

</div>
