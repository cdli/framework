<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <h2><?= __('Edit User') ?></h2>
        <hr class="form-hr mb-4"/>

        <?= $this->Flash->render() ?>

        <?= $this->Form->create($user) ?>
            <?= $this->Form->control('first_name', ['class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('last_name', ['class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('username', ['class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('email', ['class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('author_id', ['class' => 'form-control w-100', 'empty' => true]) ?>

            <div class="mb-3"></div>
            <hr class="form-hr mb-4"/>
            <div class="form-heading mb-3"><?= __('Roles') ?></div>

            <?php foreach ($roles as $role): ?>
                <div class="form-group">
                    <?= $this->Form->control('roles[][id]', [
                        'value' => $role->id,
                        'checked' => in_array($role->id, array_column($user->roles, 'id')),
                        'label' => ucfirst($role->name),
                        'type' => 'toggle',
                        'class' => 'form-control'
                    ]) ?>
                </div>
            <?php endforeach; ?>

            <hr class="form-hr mb-4"/>

            <div class="form-group">
                <?= $this->Form->control('active', [
                    'type' => 'toggle',
                    'class' => 'form-control'
                ]) ?>
            </div>

            <div class="form-group">
                <?= $this->Form->control('2fa_status', [
                    'label' => 'Two-factor authentication active',
                    'type' => 'toggle',
                    'class' => 'form-control'
                ]) ?>
            </div>

            <hr class="form-hr mb-4"/>

            <div  class="pt-2">
                <?= $this->Form->button(__('Save'), ['class' => 'btn cdli-btn-blue mr-2']) ?>
                <?php if ($user->active): ?>
                    <?= $this->Form->button(__('Forgot Password'), ['class' => 'btn cdli-btn-blue mr-2', 'name' => 'fpbtn']) ?>
                <?php endif; ?>
                <?= $this->Form->end() ?>
                <?= $this->element('deleteModal', [ 'entity_name' => $user->username, 'id' => $user->id]); ?>
            </div>
    </div>
</div>
