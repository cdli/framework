<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<h3 class="display-4 pt-3"><?= __('Users') ?></h3>
<?= $this->element('search_form') ?>
<div>
    <div>
        <table class="table table-hover" cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
            <thead  align="left">
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('username') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('created_at', __('Created On')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('last_login_at', __('Last Login At')) ?></th>
                    <th scope="col"><?= __('Email') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Authors.author', __('Author')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('active') ?></th>
                    <th class="actions" scope="col"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user): ?>
                    <tr align="left">
                        <td>
                            <?= $this->Html->link(
                                __($user->username),
                                ['controller' => 'Users', 'action' => 'view', $user->username]
                            ) ?>
                        </td>
                        <td align="left"><?= h($user->created_at) ?></td>
                        <td align="left"><?= h($user->last_login_at) ?></td>
                        <td align="left"><?= h($user->email) ?></td>
                        <td align="left">
                            <?php if ($user->author) : ?>
                            <?= $this->Html->link(
                                h($user->author->author),
                                ['prefix' => false, 'controller' => 'Authors', 'action' => 'view', $user->author_id]
                            ) ?>
                            <?php endif ?>
                        </td>
                        <td align="left"><?= $user->active ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Deactivated</span>' ?></td>
                        <td class="actions">
                            <?= $this->Html->link(
                                __('Edit'),
                                ['action' => 'edit', $user->username],
                                ['escape' => false, 'class' => 'btn btn-warning btn-sm']
                            ) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('Paginator'); ?>
</div>
