<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Period $period
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($period) ?>
            <legend class="form-heading mb-3"><?= __('Add Period') ?></legend>
            <hr class="form-hr mb-4"/>
            <?php
                echo $this->Form->control('sequence', ['class' => 'form-control w-100 mb-3', 'type' => 'text']);
                echo $this->Form->control('period', ['class' => 'form-control w-100 mb-3', 'type' => 'text', 'required' => true]);
                echo $this->Form->control('name', ['class' => 'form-control w-100 mb-3', 'type' => 'text']);
                echo $this->Form->control('time_range', ['class' => 'form-control w-100 mb-3', 'type' => 'text']);
            ?>

          <?= $this->Form->button(__('Submit'),['class'=>'btn cdli-btn-blue']) ?>
        <?= $this->Form->end() ?>

    </div>
</div>
