<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Period $period
 */

use Cake\I18n\FrozenTime;
?>

<div class="row justify-content-md-center">
    <div class="col-lg-10 boxed">
        <div class="capital-heading">
            <?= __('Periods') ?>

            <div class="float-right">
                <?= $this->Form->postLink(
                    __('(Re)generate all'),
                    null,
                    [
                        'data' => array_reduce($periods->toArray(), function ($data, $period) {
                            $data[$period->id] = ['words', 'signs'];
                            return $data;
                        }),
                        'class' => 'btn cdli-btn-blue'
                    ]
                ) ?>
                <?= $this->Form->postLink(
                    __('Generate unavailable'),
                    null,
                    [
                        'data' => array_reduce($periods->toArray(), function ($data, $period) use ($tokenLists) {
                            $new = array_diff(['words', 'signs'], array_keys($tokenLists[$period->id]));
                            if (!empty($new)) {
                                $data[$period->id] = $new;
                            }
                            return $data;
                        }),
                        'class' => 'btn cdli-btn-grey'
                    ]
                ) ?>
            </div>
        </div>

        <table class="table-bootstrap">
            <thead>
                <tr>
                    <th><?= __('Period') ?></th>
                    <th><?= __('Word list') ?></th>
                    <th><?= __('Sign list') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($periods as $period): ?>
                    <tr>
                        <td><?= $this->Html->link(
                            $period->period,
                            ['prefix' => false, 'controller' => 'Periods', 'action' => 'view', $period->id]
                        ) ?></td>
                        <?php foreach (['words', 'signs'] as $kind): ?>
                            <td>
                                <?php if (isset($tokenLists[$period->id][$kind])): ?>
                                    <?= FrozenTime::createFromTimestamp($tokenLists[$period->id][$kind]) ?>
                                    <?= $this->Form->postLink(
                                        __('Regenerate'),
                                        null,
                                        [
                                            'data' => [$period->id => [$kind]],
                                        ]
                                    ) ?>
                                <?php else: ?>
                                    <?= $this->Form->postLink(
                                        __('Generate'),
                                        null,
                                        [
                                            'data' => [$period->id => [$kind]],
                                            'class' => 'btn cdli-btn-blue'
                                        ]
                                    ) ?>
                                <?php endif; ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
