<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Language $language
 */
?>

<div class="row justify-content-md-center">

<div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($language) ?>
            <legend class="form-heading "><?= __('Edit Language') ?></legend>
            <hr class="form-hr mb-4"/>
            <?php
                echo $this->Form->control('sequence', ['class' => 'form-control w-100 mb-3', 'type' => 'text']);
                echo $this->Form->control('parent_id', ['options' => $parentLanguages, 'empty' => true, 'class' => 'form-control w-100 mb-3 custom-form-select']);
                echo $this->Form->control('language', ['class' => 'form-control w-100 mb-3', 'type' => 'text', 'required' => true]);
                echo $this->Form->control('protocol_code', ['class' => 'form-control w-100 mb-3', 'type' => 'text']);
                echo $this->Form->control('inline_code', ['class' => 'form-control w-100 mb-3', 'type' => 'text']);
                echo $this->Form->control('notes', ['class' => 'form-control w-100 mb-3', 'type' => 'text']);
            ?>

        <div>
        <?= $this->Form->button(__('Save'),['class'=>'btn cdli-btn-blue']) ?>
        <?= $this->Form->end() ?>
        <?= $this->element('deleteModal', [ 'entity_name' => $language->language, 'id' => $language->id]); ?>
        </div>          

    </div>

</div>
