<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ExternalResource $externalResource
 */
?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($externalResource) ?>
            <legend class="form-heading mb-3"><?= __('Add External Resource') ?></legend>
            <hr class="form-hr mb-4"/>

            <?= $this->Form->control('external_resource', ['class' => 'form-control w-100 mb-3', 'type' => 'text', 'required' => true]) ?>
            <?= $this->Form->control('abbrev', ['class' => 'form-control w-100 mb-3', 'type' => 'text', 'required' => true]) ?>
            <?= $this->Form->control('project_url', ['class' => 'form-control w-100 mb-3', 'type' => 'text']) ?>

            <?= $this->Form->control('base_url', ['class' => 'form-control w-100 mb-3', 'type' => 'text', 'required' => true]) ?>
            <?= $this->Form->control('base_uri', ['class' => 'form-control w-100 mb-3', 'type' => 'text']) ?>

            <?= $this->Form->button(__('Submit'), ['class'=>'btn cdli-btn-blue']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
