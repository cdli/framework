<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Abbreviation $abbreviation
 */
?>
<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($abbreviation) ?>
            <legend class="form-heading mb-3"><?= __('Add Abbreviation') ?></legend>
            <hr class="form-hr mb-4"/>
                <?php
                echo $this->Form->control('abbreviation', ['class' => 'form-control w-100 mb-3', 'required' => true]);
                echo $this->Form->control('fullform', ['class' => 'form-control w-100 mb-3']);
                echo $this->Form->control('bibtexkey', ['type' => 'text', 'class' => 'form-control w-100', 'error' => false]); ?>
                <div class="mb-3">
                    <?php
                    if ($this->Form->isFieldError('bibtexkey')) {
                        echo $this->Form->error('bibtexkey', 'This Bibtexkey doesn\'t match with the saved entries'); }
                    ?>
                </div>
                <?php
                echo $this->Form->control('type', ['class' => 'form-control w-100 mb-4', 'type' => 'text']);
                ?>
        <div class="pt-2">
        <?= $this->Form->button(__('Submit'),['class' => 'btn btn-primary cdli-btn-blue mr-2', 'id' => 'submit']) ?>
        <?= $this->Form->end() ?>
        </div>
    </div>
</div>
