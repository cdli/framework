<?php
/**
 * @var \App\View\AppView $this
 * @var object $doc
 */

function display_json ($array, $level) {
    foreach ($array as $i => $value) {
        if (str_starts_with($i, '@')) {
            continue;
        }

        $key = is_integer($i) ? '[' . h($i) . ']' : h($i);

        if (is_array($value) && count($value) > 0) {
            $margin = $level === 0 ? 0 : 2;
            $open = $level === 0 ? ' open' : '';
            echo "<details$open style=\"margin-left:${margin}em;\"><summary>$key</summary>";
            display_json($value, $level + 1);
            echo '</details>';
        } else {
            $margin = $level === 0 ? 1 : 3;

            echo "<div style=\"margin-left:${margin}em;\"><span class=\"mr-3\">$key</span>";

            if (is_array($value)) {
                echo '[]';
            } elseif (is_numeric($value)) {
                echo '<span style="color:#B08000;">' . h($value) . '</span>';
            } elseif (is_string($value)) {
                echo '<span style="color:#BF0303;">' . h(json_encode($value)) . '</span>';
            } else {
                echo '<span style="font-weight:bold;">' . h(json_encode($value)) . '</span>';
            }

            echo '</div>';
        }
    }
}
?>

<div class="row justify-content-center text-left">
    <div class="col-lg-9">
        <h1>Search index document</h1>
        <table class="table-bootstrap">
            <?php foreach ($doc['_source'] as $key => $value): ?>
                <?php if (str_starts_with($key, '@')): ?>
                    <tr>
                        <th><?= h($key) ?></th>
                        <td><?= h($value) ?></td>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?>
        </table>

        <h2>Data</h2>
        <div style="font-family:monospace;">
            <?php display_json($doc, 0); ?>
        </div>
    </div>
</div>
