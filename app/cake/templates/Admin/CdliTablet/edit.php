<main class="text-left">
    <h2>CDLI tablet</h2>

    <?= $this->Form->create($entry, ['type' => 'file']) ?>
        <div class="form-group">
            <?= $this->Form->control('date_display', ['label' => 'Date', 'type' => 'date', 'class' => 'form-control']) ?>
        </div>

        <div class="form-group">
            <?= $this->Form->control('theme', ['label' => 'Theme', 'type' => 'text', 'class' => 'form-control']) ?>
        </div>

        <div class="form-group">
            <?= $this->Form->control('title_short', ['label' => 'Short Title', 'type' => 'text', 'class' => 'form-control']) ?>
        </div>

        <div class="form-group">
            <?= $this->Form->control('title_long', ['label' => 'Long Title', 'type' => 'text', 'class' => 'form-control']) ?>
        </div>

        <?php if ($entry->isNew()): ?>
            <div class="form-group">
            <?= $this->Form->control('file', ['label' => 'Image', 'type' => 'file', 'class' => 'form-control', 'required' => true]) ?>
                <small id="file" class="form-text text-muted">Select an image to upload. Choose a 2048 x 1536 JPEG file.</small>
            </div>
        <?php endif; ?>

        <div class="form-group">
            <?= $this->Form->control('text_short', ['label' => 'Short Description', 'class' => 'form-control w-50']) ?>
            <small class="form-text text-muted">Description should be from 10-50 words [UTF8 & HTML are supported].</small>
        </div>

        <div class="form-group">
            <?= $this->Form->control('text_long', ['label' => 'Long Description', 'class' => 'form-control w-100']) ?>
            <small id="longdesc" class="form-text text-muted">Description should be from 10-50 words [UTF8 & HTML are supported].</small>
        </div>

        <div class="form-group">
            <?= $this->Form->control('author_id', ['label' => 'Author', 'type' => 'text', 'class' => 'form-control']) ?>
        </div>

        <?= $this->Form->submit(__('Save changes'), ['class' => 'btn cdli-btn-blue']) ?>
    <?= $this->Form->end() ?>
</main>
