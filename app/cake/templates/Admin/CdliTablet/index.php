<main class="text-left">
    <h2><?= __('CDLI Tablet') ?></h2>

    <div class="boxed">
        <?= $this->Form->create(null, ['type' => 'get', 'class' => 'mb-3']) ?>
            <div class="d-inline-block align-middle">
                <?= $this->Form->input('key', [
                    'value' => $this->request->getQuery('key'),
                    'class' => 'form-control',
                    'placeholder' => 'e.g. "Cuneiform building plans"',
                    'required' => true
                ]) ?>
            </div>
            <div class="d-inline-block align-middle">
                <input type="submit" class="btn cdli-btn-blue" value="Search">
            </div>
        <?= $this->Form->end() ?>

        <?= $this->Form->create(null, ['type' => 'get', 'class' => 'mb-3']) ?>
            <div class="d-inline-block align-middle">
                <?= $this->Form->input('start_date', [
                    'type' => 'date',
                    'value' => $this->request->getQuery('start_date'),
                    'class' => 'form-control',
                    'required' => true
                ]) ?>
            </div>
            <div class="d-inline-block align-middle">
                <?= $this->Form->input('end_date', [
                    'type' => 'date',
                    'value' => $this->request->getQuery('end_date'),
                    'class' => 'form-control',
                    'required' => true
                ]) ?>
            </div>
            <div class="d-inline-block align-middle">
                <input type="submit" class="btn cdli-btn-blue" value="Search">
            </div>
        <?= $this->Form->end() ?>

        <hr>

        <?= $this->Html->link(__('Add a new entry'), ['controller' => 'CdliTablet', 'action' => 'add'], ['class' => 'btn cdli-btn-blue']) ?>
    </div>

    <div>
        <?= $this->Form->create(NULL, ['url' => ['controller' => 'CdliTablet', 'action' => 'deleteAll']]) ?>
            <?= $this->Form->submit(__('Delete selected'), [
                'class' => 'btn btn-danger mb-3'
            ]) ?>

            <table cellpadding="0" cellspacing="0" class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th scope="col">Title</th>
                        <th scope="col">Date</th>
                        <th scope="col">Summary</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($results as $result): ?>
                        <tr>
                            <td><?= $this->Form->checkbox('ids[]', ['value' => $result->id]) ?></td>
                            <td><?= $this->Html->link($result->theme . ': ' . $result->title_short, [
                                'prefix' => false,
                                'controller' => 'CdliTablet',
                                'action' => 'view',
                                $result->id
                            ]) ?></td>
                            <td class="text-nowrap"><?= h($result->date_display) ?></td>
                            <td><?= strip_tags($result->text_short, '<a><abbr><b><br><i><center><div><em><p><q>') ?></td>
                            <td class="text-nowrap">
                                <?= $this->Html->link(
                                    __('Edit'),
                                    ['controller' => 'CdliTablet', 'action' => 'edit', $result->id],
                                    ['class' => 'btn cdli-btn-light']
                                ) ?>
                                <?= $this->Form->postLink(
                                    __('Delete'),
                                    ['controller' => 'CdliTablet', 'action' => 'delete', $result->id],
                                    ['class' => 'btn btn-danger', 'confirm' => __('Are you sure you want to delete this?')]
                                ) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?= $this->Form->end() ?>
    </div>

    <?= $this->element('Paginator') ?>
    <?= $this->element('smoothscroll') ?>
</main>
