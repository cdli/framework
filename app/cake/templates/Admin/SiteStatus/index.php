<?php
/**
 * @var \App\View\AppView $this
 */

$this->assign('title', 'Site status');
?>

<div class="text-left">
    <h1>Site status</h1>

    <div class="row">
        <div class="col-12 col-lg-6">
            <div class="my-3">
                <b>Legend</b><br>
                <div class="d-inline-block" style="width: 1em; height: 1em; background-color: #cc0000"></div> Errors<br>
                <div class="d-inline-block" style="width: 1em; height: 1em; background-color: #0000cc"></div> Warnings
            </div>

            <?= $this->Form->control(__('Filter trivial errors'), ['type' => 'toggle']) ?>

            <div class="mb-3">
                Filters:
                <div id="error_filters"></div>
            </div>

            <h2>Error rate</h2>
            <div id="graph_error_rate"></div>

            <h2>Error types</h2>
            <div id="graph_error_types"></div>

            <h2>Error path groups</h2>
            <div id="graph_error_paths"></div>
        </div>
        <div class="col-12 col-lg-6">
            <h2>Errors</h2>

            <nav aria-label="Error navigation">
                <ul class="pagination">
                    <li class="page-item disabled"><a id="table_error_previous" class="page-link" href="#" >Previous</a></li>
                    <li class="page-item active"><a id="table_error_page" class="page-link" href="#">1</a></li>
                    <li class="page-item"><a id="table_error_next" class="page-link" href="#">Next</a></li>
                </ul>
            </nav>

            <table id="table_error" class="table">
                <thead>
                    <tr>
                        <th>Level</th>
                        <th>Message</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<?php $this->append('library'); ?>
<?= $this->Html->script('d3') ?>
<?= $this->Html->script('https://cdn.jsdelivr.net/npm/d3-array@3.0.2/dist/d3-array.js') ?>
<?php $this->end('library'); ?>

<?php $this->append('script'); ?>
<script>
    window.addEventListener('load', function () {
        function makeErrorRateGraph (data) {
            var margin = { top: 0, right: 0, bottom: 30, left: 60 }
            var width = 600 - margin.left - margin.right
            var height = 400 - margin.top - margin.bottom

            var graph = d3.select('#graph_error_rate')
                .html(null)
                .append('svg')
                    .attr('width', width + margin.left + margin.right)
                    .attr('height', height + margin.top + margin.bottom)
                .append('g')
                    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')

            var counts = d3.flatRollup(data, x => x.length, d => d.datetime, d => d.level)
                .map(([datetime, level, n]) => ({ datetime, level, n }))
            var groups = d3.nest().key(d => d.level).entries(counts)

            var x = d3.scaleTime()
                .domain(d3.extent(counts, d => d.datetime))
                .range([0, width])

            graph.append('g')
                .attr('transform', 'translate(0,' + height + ')')
                .call(d3.axisBottom(x))

            var y = d3.scaleLinear()
                .domain([0, d3.max(counts, d => d.n)])
                .range([height, 0])

            graph.append('g')
                .call(d3.axisLeft(y))

            var color = d3.scaleOrdinal()
                .domain(['Error', 'Warning'])
                .range(['#cc0000','#0000cc'])

            var lines = graph.selectAll('.line')
                .data(groups)
                .enter()
                .append('g')

            lines.append('path')
                .attr('fill', 'none')
                .attr('stroke', d => color(d.key))
                .attr('stroke-width', 2)
                .attr('d', d => d3.line().x(d => x(d.datetime)).y(d => y(d.n))(d.values))

            lines.append('g')
                .attr('fill', d => color(d.key))
                .selectAll('.points')
                .data(d => d.values)
                .enter()
                .append('circle')
                    .attr('cx', d => x(d.datetime))
                    .attr('cy', d => y(d.n))
                    .attr('r', 3)
                    .on('mouseover', d => tooltip
                        .style('visibility', 'visible')
                        .html(d.level + ': ' + d.n + '<br>' + d.datetime.toLocaleString()))
                    .on('mousemove', () => tooltip
                        .style('top', (d3.event.pageY + 10) + 'px')
                        .style('left', (d3.event.pageX + 10) + 'px'))
                    .on('mouseout', () => tooltip.style('visibility', 'hidden'))
                    .on('click', d => {
                        filterOptions.datetime = d.datetime
                        errorTablePage = 1
                        updateData()
                    })

            var tooltip = d3.select('#graph_error_rate')
                .append('div')
                    .style('position', 'absolute')
                    .style('visibility', 'hidden')
                    .style('background-color', 'white')
                    .style('border', 'solid')
                    .style('border-width', '1px')
                    .style('padding', '10px')
        }

        function makeErrorTypesGraph (data) {
            var margin = { top: 0, right: 0, bottom: 30, left: 0 }
            var width = 600 - margin.left - margin.right
            var height = 400 - margin.top - margin.bottom

            var graph = d3.select('#graph_error_types')
                .html(null)
                .append('svg')
                    .attr('width', width + margin.left + margin.right)
                    .attr('height', height + margin.top + margin.bottom)
                .append('g')
                    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')

            var counts = d3.flatRollup(data, x => x.length, d => d.message, d => d.level)
                .map(([message, level, n]) => ({ message, level, n }))
            counts = d3.sort(counts, d => -d.n)
            counts = counts.slice(0, 14).concat({
                level: 'Other',
                message: 'Other',
                n: counts.slice(14).reduce((sum, group) => sum + group.n, 0)
            })

            var x = d3.scaleLinear()
                .domain([0, d3.max(counts, d => d.n)])
                .range([0, width])

            graph.append('g')
                .attr('transform', 'translate(0,' + height + ')')
                .call(d3.axisBottom(x))

            var y = d3.scaleBand()
                .domain(d3.map(counts, d => d.message))
                .range([0, height])

            var color = d3.scaleOrdinal()
                .domain(['Error', 'Warning', 'Other'])
                .range(['#ffcccc','#ccccff', '#e6e6e6'])

            var bars = graph.selectAll('.rect')
                .data(counts)
                .enter()
                    .append('rect')
                    .attr('fill', d => color(d.level))
                    .attr('x', x(0))
                    .attr('y', d => y(d.message))
                    .attr('width', d => x(d.n))
                    .attr('height', y.bandwidth())

            graph.append('g')
                .call(d3.axisRight(y))

            graph.selectAll('g:last-child .tick, rect')
                .on('click', d => {
                    filterOptions.message = d.message || d
                    errorTablePage = 1
                    updateData()
                })
        }

        function makeErrorPathsGraph (data) {
            var margin = { top: 0, right: 0, bottom: 30, left: 0 }
            var width = 600 - margin.left - margin.right
            var height = 400 - margin.top - margin.bottom

            var graph = d3.select('#graph_error_paths')
                .html(null)
                .append('svg')
                    .attr('width', width + margin.left + margin.right)
                    .attr('height', height + margin.top + margin.bottom)
                .append('g')
                    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')

            data = d3.map(data, d => {
                if (d.url === null) {
                    return d
                }

                var parts = d.url.slice(1).split(/[/?]/)
                if (parts[0] === 'admin') {
                    parts = parts.slice(0, 2)
                } else {
                    parts = parts.slice(0, 1)
                }
                return {
                    ...d,
                    url: '/' + parts.join('/')
                }
            })

            var counts = d3.flatRollup(data, x => x.length, d => d.url)
                .map(([url, n]) => ({ url, n }))
            counts = d3.sort(counts, d => -d.n)
            counts = counts.slice(0, 14).concat({
                level: 'Other',
                url: 'Other',
                n: counts.slice(14).reduce((sum, group) => sum + group.n, 0)
            })

            var x = d3.scaleLinear()
                .domain([0, d3.max(counts, d => d.n)])
                .range([0, width])

            graph.append('g')
                .attr('transform', 'translate(0,' + height + ')')
                .call(d3.axisBottom(x))

            var y = d3.scaleBand()
                .domain(d3.map(counts, d => d.url))
                .range([0, height])

            var bars = graph.selectAll('.rect')
                .data(counts)
                .enter()
                    .append('rect')
                    .attr('fill', '#e6e6e6')
                    .attr('x', x(0))
                    .attr('y', d => y(d.url))
                    .attr('width', d => x(d.n))
                    .attr('height', y.bandwidth())

            graph.append('g')
                .call(d3.axisRight(y))

            graph.selectAll('g:last-child .tick, rect')
                .on('click', d => {
                    filterOptions.url = d.url || d
                    errorTablePage = 1
                    updateData()
                })
        }

        function makeErrorTable (data) {
            var view = data.slice((errorTablePage - 1) * 20, errorTablePage * 20)

            $('#table_error_previous').parent('.page-item').toggleClass('disabled', errorTablePage === 1)
            $('#table_error_next').parent('.page-item').toggleClass('disabled', data.length < errorTablePage * 20)
            $('#table_error_page').text(errorTablePage)

            var rows = d3.select('#table_error tbody')
                .html(null)
                .selectAll('tr')
                .data(view)
                .enter()
                    .append('tr')

            rows.append('td').text(d => d.level)
            var message = rows.append('td')
            message.append('div').text(d => d.message)
            message.append('div').attr('class', 'text-muted').text(d => d.url ? `URL: ${d.url}` : null)
            message.append('div').attr('class', 'text-muted').text(d => d.referer ? `Referer: ${d.referer}` : null)
        }

        var data = []
        var filterOptions = {}
        var errorTablePage = 1

        var parseDate = d3.timeParse('%Y-%m-%d %H:%M:%S')
        var trivialErrors = new RegExp([
            'RecordNotFoundException',
            'MissingControllerException',
            'MissingRouteException',
            'MissingTemplateException',
            'InvalidCsrfTokenException',
            'NotAcceptableException'
        ].join('|'))

        function updateData () {
            var filters = []
            for (var key in filterOptions) {
                if (key === 'filterTrivial') {
                    continue
                }

                filters.push({ key: key, value: filterOptions[key] })
            }

            d3.select('#error_filters')
                .html(null)
                .selectAll('button')
                .data(filters)
                .enter()
                    .append('button')
                    .attr('class', 'btn btn-link btn-sm align-top mr-3')
                    .text(d => `${d.key}: ${d.value.toString().replace(/^(.{20})(.+)$/, '$1...')}`)
                    .on('click', d => {
                        delete filterOptions[d.key]
                        updateData()
                    })
                    .append('i')
                    .attr('class', 'fa fa-times ml-3')

            var selection = data
                .filter(error => {
                    if (filterOptions.url && (!error.url || !error.url.startsWith(filterOptions.url))) {
                        return false
                    } else if (filterOptions.datetime && error.datetime.toISOString() !== filterOptions.datetime.toISOString()) {
                        return false
                    } else if (filterOptions.message && error.message !== filterOptions.message) {
                        return false
                    } else if (filterOptions.filterTrivial && trivialErrors.test(error.message)) {
                        return false
                    }
                    return true
                })

            makeErrorRateGraph(selection)
            makeErrorTypesGraph(selection)
            makeErrorPathsGraph(selection)
            makeErrorTable(selection)
        }

        updateData()

        d3.json('/admin/site-status/errors.json').then(function (response) {
            data = response

            data.forEach(function (d) {
                d.datetime = parseDate(d.datetime)
                d.datetime = d3.timeHour.every(2).floor(d.datetime)
            })
            data = d3.sort(data, d => d.datetime)

            updateData()
        })

        $('#filter-trivial-errors').on('change', function () {
            filterOptions.filterTrivial = $(this).prop('checked')
            updateData()
        })

        $('#table_error_previous').click(function (e) {
            e.preventDefault()
            errorTablePage--
            updateData()
        })
        $('#table_error_next').click(function (e) {
            e.preventDefault()
            errorTablePage++
            updateData()
        })
    })
</script>
<?php $this->end('script'); ?>
