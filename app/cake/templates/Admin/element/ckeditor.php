<?php $this->append('library'); ?>
    <script src="/assets/js/ckeditor/ckeditor.js"></script>
<?php $this->end(); ?>

<?php $this->append('script'); ?>
    <script>
        CKEDITOR.editorConfig = function( config ) {
            config.htmlEncodeOutput = true;
        };
        CKEDITOR.replace('<?= $id ?>', {
            filebrowserBrowseUrl: '/admin/ckeconnector/elfinder',
            extraPlugins: 'autogrow',
            autoGrow_maxHeight: 800,
            removePlugins: 'resize'
        });
    </script>
<?php $this->end(); ?>
