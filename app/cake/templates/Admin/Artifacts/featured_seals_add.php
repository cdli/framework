<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <div id="div1"> </div>
        <?= $this->Form->create($featuredSealsAddData) ?>
            <legend class="form-heading mb-3"><?= __('Add a Featured Seal') ?></legend>
            <hr class="form-hr mb-4"/>
            <?= $this->Form->control('title', ['type' => 'text', 'class' => 'form-control w-100 mb-3', 'required' => true]);?>
            <?= $this->Form->control('body', ['type' => 'textarea', 'class' => 'form-control w-100 mb-3', 'required' => true]);?>

            <!-- Artifact -->
            <?= $this->Form->control('Artifact id', ['type' => 'text', 'class' => 'form-control w-100 mb-3','id'=>'pnum', 'required' => true]); ?>

            <!-- Image -->
            <?= $this->Form->control('Image type', ['for'=> 'image_type','value'=>'photo detail','type' => 'text', 'class' => 'form-control w-100','readonly'=>true]); ?>

            <div class="pt-2">
            <?= $this->Form->button(__('Save Changes'),['class' => 'btn btn-primary cdli-btn-blue mr-2', 'id' => 'submit']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
