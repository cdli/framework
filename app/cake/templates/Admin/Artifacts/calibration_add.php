<?php
$this->assign('title', __('Calibration add form'));
?>
<div class="row justify-content-md-center">

    <div class="col-7 text-left form-wrapper">
        <?= $this->Form->create($calibrationAddData,['type' => 'file']) ?>
            <legend class="form-heading mb-3"><?= __('Add calibration data for seal') ?></legend>
            <hr class="form-hr mb-4"/>
            <div class="form-group">
                <label for="calibrationSheet"><?= __('Upload Calibration Data') ?></label>
                <input name="calibrationSheet" type="file" class="form-control w-100 mb-3" required>
            </div>
            <div class="form-group">
                <label for="humanId"><?= __('HumanId') ?></label>
                <input name="humanId" type="text" class="form-control w-100 mb-3" required>
            </div>

          <?= $this->Form->button(__('Submit'),['class' => 'btn btn-primary cdli-btn-blue mr-2', 'id' => 'submit']) ?>
        <?= $this->Form->end() ?>

    </div>

</div>
