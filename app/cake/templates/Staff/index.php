<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Staff[]|\Cake\Collection\CollectionInterface $staff
 */
use Cake\Routing\Router;
?>

<main class="container text-left">
    <div class="d-flex justify-content-between align-items-end mb-2">
        <h1 class="display-3 header-txt text-left"><?= __('Collaborators and Staff') ?></h1>     
        <?= $this->element('addButton'); ?>      
    </div>
    <p class="text-justify page-summary-text mt-4">Over the years, scholars, scientists,
    students, employees, and volunteers have helped shape what the CDLI is today. Below
    you can find a list of some of these people who have collaborated with us, contributing
    their expertise, time, data, and skills.</p>

    <h2>Categories</h2>
    <p>Click on a category to see the associated list of contributors.</p>
    <div class="row">
        <?php foreach ($staff_types as $staff_type) :?>
            <div class="col">
                <?= $this->Html->link(
                    $staff_type->staff_type,
                    ['controller' => 'Staff', 'action' => 'index', '#' => $staff_type->id]
                ) ?>
            </div>
        <?php endforeach; ?>
    </div>

    <div>
        <?php foreach ($staff_types as $staff_type) :?>
            <h3 id="<?= $staff_type->id ?>" class="mb-3 mt-3"><?= $staff_type->staff_type ?></h3>
            <div class="row row-cols-1 row-cols-md-4 staff-row">
                <?php foreach ($staff as $person): ?>
                    <?php if ($person->staff_type_id == $staff_type->id): ?>
                        <div class="col-sm-3 mb-4">
                            <div class="card mb-4 border border-secondary p-3 h-100">
                                <?php if ($access_granted): ?>
                                    <?= $this->Html->image('/images/edit.svg', [
                                        'alt' => 'Edit',
                                        'url' => ['prefix' => 'Admin', 'action' => 'edit', $person->id],
                                        'title' => 'Edit',
                                        'class' => 'pull-right'
                                    ]) ?>
                                <?php endif; ?>
                                <div class="card-header d-flex h-100">
                                    <p class="my-0 font-weight-normal flex-grow-1">
                                        <span class="font-weight-bold">
                                            <?= $this->Html->link(
                                                $person->author->author,
                                                ['controller' => 'Authors', 'action' => 'view', $person->author_id],
                                            ) ?>
                                            <br>
                                            <?= $person->cdli_title ?>
                                        </span>
                                        <br>
                                        <?= $person->author->institution ?>
                                    </p>
                                </div>

                                 <div class="card-body">
                                     <?php
                                        $files = glob(WWW_ROOT . 'files-up/images/staff-img/' . $person->id . '.*');
                                        $image;
                                        if (count($files) > 0) {
                                            $image = '/files-up/images/staff-img/' . basename($files[0]);
                                        } else {
                                            $image = '/files-up/images/staff-img/staff.png';
                                        }
                                     ?>
                                     <div class="profile-image-responsive" style="background-image: url(<?= $image ?>);"></div>
                                 </div>
                             </div>
                         </div>
                    <?php endif; ?>
                <?php endforeach; ?>
             </div>
         <?php endforeach; ?>
    </div>
</main>

<button class="p-0 mt-5 btn d-flex align-items-center justify-content-between mx-auto wide-btn">
    <a href="#" class="text-dark btn wide-btn">
        Back to top
        <span class="fa fa-long-arrow-up ml-2" aria-hidden="true"></span>
    </a>
</button>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>