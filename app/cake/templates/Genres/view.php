<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Genre $genre
 */
$this->assign('description', $genre->genre_description);
?>
<?php use Cake\Utility\Inflector; ?>

<div>
    <div class="text-heading text-left"><?= __(h(Inflector::humanize(Inflector::singularize(Inflector::tableize($this->request->getParam('controller')))))) ?>: <?= h($genre->genre) ?>
      <?php if ($access_granted): ?>
          <?= $this->Html->link(
              __('Edit'),
              ['prefix' => 'Admin', 'action' => 'edit', $genre->id],
              ['escape' => false , 'class' => "btn cdli-btn-blue pull-right", 'title' => 'Edit']
          ) ?>
      <?php endif; ?>
    </div>
</div>

<?php if ($genre->has('genre_description')): ?>
    <p class="text-left mt-4"><?= $genre->genre_description ?></p>
<?php endif; ?>

<?php if ($genre->has('parent_genre')): ?>
    <div class="single-entity-wrapper mx-0 text-left mt-4">
        <div class="capital-heading"><?= __('Parent Genres') ?></div>
        <?= $this->Html->link(
                            $genre->parent_genre->genre,
                            ['controller' => 'Genres', 'action' => 'view', $genre->parent_genre->id]
                        ) ?>
    </div>
<?php endif; ?>

<?php if (!empty($genre->child_genres)): ?>
    <div class="single-entity-wrapper mx-0 text-left mt-4">
        <div class="capital-heading"><?= __('Child Genres') ?></div>
        <ul>        
            <?php foreach ($genre->child_genres as $childGenres): ?>
                                <li><?= $this->Html->link(
                                    $childGenres->genre,
                                    ['controller' => 'Genres', 'action' => 'view', $childGenres->id]
                                ) ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<?php if ($count!=0): ?>
    <div class="single-entity-wrapper text-left mt-4">
        <div class="capital-heading"><?= __('Related Artifacts') ?></div>
        <p>
          <?php echo 'There are '.$count.' artifacts related to '.h($genre->genre) ?><br>
            <?= $this->Html->link(__('Click here to view the artifacts'), [
                'controller' => 'Search',
                'action' => 'index',
                '?' => ['genre' => h($genre->genre)]
            ]) ?>
        </p>
    </div>
<?php endif; ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>