<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Archive $archive
 */
?>

<div class="text-left">
    <h1>
        <?= __('Archive') ?>: <?= h($archive->archive) ?>
        <?php if ($canEdit): ?>
            <?= $this->Html->link(
                __('Edit'),
                ['prefix' => 'Admin', 'action' => 'edit', $archive->id],
                ['escape' => false , 'class' => "btn cdli-btn-blue float-right", 'title' => 'Edit']
            ) ?>
        <?php endif; ?>
    </h1>

    <ul>
        <li>
            <?= __('Provenience') ?>:
            <?php if ($archive->has('provenience')): ?>
                <?= $this->Html->link($archive->provenience->id, [
                    'controller' => 'Proveniences',
                    'action' => 'view',
                    $archive->provenience->id
                ]) ?>
            <?php else: ?>
                &mdash;
            <?php endif; ?>
        </li>
    </ul>

    <?php if (!empty($archive->artifacts)): ?>
        <h2><?= __('Related Artifacts') ?></h2>

        <p>
            <?= $this->Html->link(__('View in search results'), [
                'controller' => 'Search',
                'action' => 'index',
                '?' => ['archive' => "/$archive->archive/"]
            ]) ?>
        </p>

        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
                <th scope="col"><?= __('CDLI') ?></th>
                <th scope="col"><?= __('Designation') ?></th>
                <th scope="col"><?= __('Language(s)') ?></th>
                <th scope="col"><?= __('Period') ?></th>
            </thead>
            <tbody>
                <?php foreach ($archive->artifacts as $artifact): ?>
                <tr>
                    <td>
                        <?= $this->Html->link($artifact->getCdliNumber(), [
                            'controller' => 'Artifacts',
                            'action' => 'view',
                            $artifact->id
                        ]) ?>
                    </td>
                    <td><?= h($artifact->designation) ?></td>
                    <td><?= $artifact->serializeList($artifact->languages, 'language') ?></td>
                    <td><?= $artifact->has('period') ? h($artifact->period->period) : '' ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
