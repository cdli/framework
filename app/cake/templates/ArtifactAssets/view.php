<?php
/**
 * @var \App\View\AppView $this
 */

$title = $asset->getDescription();
$artifactInfo = "{$asset->artifact->designation} ({$asset->artifact->getCdliNumber()})";
$this->assign('title', "$title of $artifactInfo");

$attribution = $this->ArtifactAssets->getAttributionInfo($asset, $asset->artifact->collections[0] ?? null);
?>

<div class="row">
    <div class="col-12 boxed">
        <?php if ($isAdmin): ?>
            <?= $this->Html->link(
                __('Edit'),
                ['prefix' => 'Admin', 'controller' => 'ArtifactAssets', 'action' => 'edit', $asset->id],
                ['class' => 'btn cdli-btn-blue float-right']
            ) ?>
        <?php endif; ?>

        <h2 class="mb-3">
            <?= $title ?> of
            <?= $this->Html->link($artifactInfo, ['controller' => 'Artifacts', 'action' => 'view', $asset->artifact->id]) ?>

            <?php if (!$asset->is_public): ?>
                <span class="p-2 badge badge-secondary"><?= __('Private') ?></span>
            <?php endif; ?>
        </h2>

        <?= $this->cell('ArtifactAsset', [$asset->id]) ?>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <div class="boxed" id="copyright">
            <h3><?= __('Copyright info') ?></h3>

            <p><?= $attribution['comment'] ?? '' ?></p>

            <table class="table-bootstrap">
                <tbody>
                    <tr>
                        <th><?= __('Owner') ?></th>
                        <td><?= $attribution['attribution'] ?? '' ?></td>
                    </tr>
                    <tr>
                        <th><?= __('License') ?></th>
                        <td><?= $attribution['license'] ?? '' ?></td>
                    </tr>
                </tbody>
            </table>

            <?php if (!empty($asset->authors)): ?>
                <h3><?= __('Contributors') ?></h3>

                <div class="table-response">
                    <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
                        <thead>
                            <tr>
                                <th scope="col"><?= __('Contributor') ?></th>
                                <th scope="col"><?= __('Role') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($asset->authors as $author): ?>
                                <tr>
                                    <td>
                                        <?= $this->Html->link(
                                            $author->author,
                                            ['controller' => 'Authors', 'action' => 'view', $author->id]
                                        ) ?>
                                    </td>
                                    <td><?= h($author->_joinData->role) ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?php endif; ?>

            <?php if (!empty($asset->publications)): ?>
                <h3><?= __('Publications') ?></h3>

                <ul>
                    <?php foreach ($asset->publications as $publication): ?>
                        <li>
                            <p class="m-0">
                                <b><?= $publication->_joinData->publication_type ?></b>:
                                <?= h($publication->designation) ?>
                                <?= h($publication->_joinData->exact_reference) ?>
                            </p>

                            <?= $this->Citation->formatReferenceAsHtml($publication, 'bibliography', [
                                'template' => 'chicago-author-date',
                                'format' => 'html',
                                'hyperlinks' => true,
                                'prepend' => '[' . $this->Html->link(
                                    $publication->bibtexkey ?? 'Details',
                                    ['prefix' => false, 'controller' => 'Publications', 'action' => 'view', $publication->id]
                                ) . '] ',
                            ]) ?>

                            <?php if (!empty($publication->_joinData->publication_comments)): ?>
                                <p class="my-3">
                                    <i><?= h($publication->_joinData->publication_comments) ?></i>
                                </p>
                                <hr/>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>

        <div class="boxed">
            <h3><?= __('Visual assets of this artifact') ?></h3>

            <table class="table-bootstrap">
                <thead>
                    <tr>
                        <th><?= __('Type of visual asset') ?></th>
                        <th><?= __('Artifact aspect') ?></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($asset->artifact->artifact_assets as $relatedAsset): ?>
                        <tr <?= $relatedAsset->id === $asset->id ? 'class="font-weight-bold"' : '' ?>>
                            <td><?= h($relatedAsset->asset_type) ?></td>
                            <td><?= h($relatedAsset->artifact_aspect ?? '—') ?></td>
                            <td>
                                <?php if ($relatedAsset->id !== $asset->id): ?>
                                    <?= $this->Html->link(__('View'), [$relatedAsset->id]) ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-6">
        <div class="boxed">
            <h3><?= __('File information') ?></h3>

            <table class="table-bootstrap">
                <tbody>
                    <tr>
                        <th><?= __('File format') ?></th>
                        <td><?= h($asset->file_format) ?></td>
                    </tr>
                    <tr>
                        <th><?= __('File size') ?></th>
                        <td><?= $this->Number->format($asset->file_size / (1024 * 1024)) ?> MiB</td>
                    </tr>
                    <?php if ($asset->file_format === 'jpg'): ?>
                        <tr>
                            <th><?= __('Image size') ?></th>
                            <td><?= h($asset->image_width) ?>x<?= h($asset->image_height) ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Image color') ?></th>
                            <td>
                                <div class="d-inline-block align-bottom" style="width: 1.5em; height: 1.5em; background-color: <?= h($asset->image_color_average) ?>"></div>
                                <?= h($asset->image_color_average) ?>
                                (<?= h($asset->image_color_depth) ?> bit)
                            </td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>

            <?php if ($asset->file_format === 'jpg'): ?>
                <?php $assetData = exif_read_data(WWW_ROOT . $asset->getPath(), null, true); ?>
                <?php foreach (['IFD0', 'EXIF'] as $section): ?>
                    <h4><?= $section ?></h4>
                    <table class="table-bootstrap" style="font-family: monospace;">
                        <tbody>
                            <?php foreach ($assetData[$section] as $key => $value): ?>
                                <tr>
                                    <th><?= h($key) ?></th>
                                    <td><?= h(is_array($value) ? json_encode($value) : $value) ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
