<?php
$this->assign('title', __('Custom Search Result Export'));

$searchQuery = $this->getRequest()->getQuery();
$searchQuery['format'] = $searchQuery['export'] ?? 'csv';
unset($searchQuery['export']);

$fields = [
    'id' => __('Artifact ID'),
    'museum_no' => __('Museum Number'),
    'designation' => __('Designation'),
    'publication' => __('Bibliography fields'),
    'provenience' => __('Provenience'),
    'period' => __('Period'),
    'dates_referenced' => __('Referenced date(s)'),
    'collection' => __('Museum Collection(s)'),
    'genre' => __('Genre(s)/sub-genre(s)'),
    'material' => __('Material(s)'),
    'artifact_type' => __('Artifact type'),
    'language' => __('Language(s)'),
    'accession_no' => __('Accession number'),
    'excavation_no' => __('Excavation number'),
    'written_in' => __('Written in'),
    'archive' => __('Archive(s)'),
];
?>

<div class="row justify-content-center text-left">
    <div class="col-lg-6 boxed">
        <h1>Custom Search Result Export</h1>

        <?= $this->Form->create(null, [
            'type' => 'post',
            'url' => ['custom-export', '?' => $searchQuery]
        ]) ?>
            <p><?= __('Column order') ?></p>
            <div>
                <?php foreach ($fields as $key => $label): ?>
                    <div class="export-column">
                        <div class="btn-group float-left mr-3">
                            <?= $this->Form->button('<span class="fa fa-chevron-up" aria-hidden="true"></span>', [
                                'class' => 'btn cdli-btn-blue px-1 py-0 js-column-up',
                                'escapeTitle' => false,
                                'title' => __('Move up')
                            ]) ?>
                            <?= $this->Form->button('<span class="fa fa-chevron-down" aria-hidden="true"></span>', [
                                'class' => 'btn cdli-btn-blue px-1 py-0 js-column-down',
                                'escapeTitle' => false,
                                'title' => __('Move down')
                            ]) ?>
                        </div>

                        <?= $this->Form->control('columns[]', [
                            'id' => "export-column-$key-enabled",
                            'type' => 'toggle',
                            'label' => $label,
                            'value' => $key,
                            'hiddenField' => false,
                            'checked' => $settings["compact:field:$key"] ?? true
                        ]) ?>
                    </div>
                <?php endforeach; ?>
            </div>

            <?= $this->Form->submit(__('Download'), ['class' => 'btn cdli-btn-blue']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<?php $this->append('script'); ?>
<script>
    $(function () {
        function updateMoveButtons () {
            $('.js-column-up').prop('disabled', false)
            $('.js-column-down').prop('disabled', false)
            $('.export-column:first-child .js-column-up').prop('disabled', true)
            $('.export-column:last-child .js-column-down').prop('disabled', true)
        }

        $('.js-column-up').click(function (e) {
            e.preventDefault()
            var item = $(this).closest('.export-column')
            var previous = item.prev()
            item.insertBefore(previous)
            updateMoveButtons()
        })
        $('.js-column-down').click(function (e) {
            e.preventDefault()
            var item = $(this).closest('.export-column')
            var next = item.next()
            item.insertAfter(next)
            updateMoveButtons()
        })

        updateMoveButtons()
    })
</script>
<?php $this->end('script'); ?>
