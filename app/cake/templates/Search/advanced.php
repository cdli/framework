<?php
$this->assign('title', __('Advanced Search'));

$this->Form->setTemplates([
    'label' => '<label class="mb-0"{{attrs}}>{{text}}</label>',
    'inputContainer' => '<div class="input form-group mb-2 {{type}}{{required}}">{{content}}</div>'
]);
?>

<?= $this->Form->create(['data' => $query, 'schema' => []], ['type' => 'GET', 'url' => ['action' => 'index'], 'class' => 'text-left']) ?>
    <div>
        <?= $this->Html->link(__('Clear'), ['advanced', '?' => []], ['class' => 'btn my-4 ml-3 w-30 float-right']) ?>
        <?= $this->Form->button(__('Search') . ' <span class="fa fa-search" aria-hidden="true"></span>', [
            'type' => 'submit',
            'class' => 'btn cdli-btn-blue my-4 ml-3 w-30 float-right',
            'escapeTitle' => false
        ]) ?>

        <h1 class="display-3 text-left"><?= __('Advanced Search') ?></h1>
    </div>

    <div class="mt-4 mb-2">
        <?= $this->Html->link('Search Settings', ['action' => 'view', 'settings'], ['class' => 'mr-5'])?>
        <?= $this->Html->link(
            __('Search Guide') . ' <span class="fa fa-question-circle" aria-hidden="true"></span>',
            ['controller' => 'Docs', 'action' => 'view', 'search'],
            ['escapeTitle' => false]
        ) ?>
    </div>

    <div class="row mb-3">
        <div class="col-md-6 col-12">
            <div class="boxed m-0">
                <h3>Publication Data</h3>

                <?= $this->Form->control('publication_designation', ['label' => 'Publication Designation', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('publication_authors', ['label' => 'Author(s)', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('publication_editors', ['label' => 'Editor(s)', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('publication_year', ['label' => 'Publication Year', 'class' => 'form-control w-100', 'type' => 'number']) ?>
                <?= $this->Form->control('publication_title', ['label' => 'Title', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('publication_type', [
                    'options' => [
                        'primary' => 'Primary',
                        'history' => 'History',
                        'citation' => 'Citation',
                        'collation' => 'Collation',
                        'other' => 'Other'
                    ],
                    'label' => 'Publication Type',
                    'empty' => 'Select a publication type',
                    'class' => 'form-control w-100'
                ]) ?>
                <?= $this->Form->control('publication_publisher', ['label' => 'Publisher', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('publication_series', ['label' => 'Series', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('publication_exact_reference', ['label' => 'Exact Reference', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('publication_bibtexkey', ['label' => 'Bibtex Key', 'class' => 'form-control w-100']) ?>
            </div>

            <div class="boxed m-0">
                <h3>Artifact Data</h3>

                <?= $this->Form->control('artifact_type', ['label' => 'Artifact Type', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('material', ['label' => 'Material', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('collection', ['label' => 'Collection', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('provenience', ['label' => 'Provenience', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('written_in', ['label' => 'Written in', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('archive', ['label' => 'Archive', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('period', ['label' => 'Period', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('artifact_comments', ['label' => 'Artifact Comment', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('dates_referenced', ['label' => 'Dates', 'class' => 'form-control w-100']) ?>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="boxed m-0">
                <h3>Inscriptional data</h3>

                <?= $this->Form->control('atf_translation_text', ['label' => 'Translation', 'class' => 'form-control w-100']) ?>
                <fieldset class="form-group border py-2 px-3">
                    <?= $this->Form->control('atf_transliteration', ['label' => 'Transliteration', 'class' => 'form-control w-100']) ?>
                    <?= $this->Form->control('atf_transliteration_mode', [
                        'label' => 'Single line search (default: full text)',
                        'value' => 'line',
                        'hiddenField' => false,
                        'class' => 'form-control w-100',
                        'type' => 'toggle'
                    ]) ?>
                    <?= $this->Form->control('atf_transliteration_case_sensitive', [
                        'label' => 'Case sensitive',
                        'hiddenField' => false,
                        'class' => 'form-control w-100',
                        'type' => 'toggle'
                    ]) ?>
                    <?= $this->Form->control('atf_transliteration_sign_permutation', [
                        'label' => 'Sign Permutation (all sign readings per sign)',
                        'hiddenField' => false,
                        'class' => 'form-control w-100',
                        'type' => 'toggle'
                    ]) ?>
                </fieldset>
                <?= $this->Form->control('atf_comments', ['label' => 'Comment', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('atf_structure', ['label' => 'ATF structure (eg. "edge")', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('genre', ['label' => 'Genre', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('genre_comments', ['label' => 'Genre Comments', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('language', ['label' => 'Language', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('asset_annotations', ['label' => 'Image annotations', 'class' => 'form-control w-100']) ?>
            </div>

            <div class="boxed m-0">
                <h3>Artifact Identification</h3>

                <?= $this->Form->control('designation', ['label' => 'Designation', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('museum_no', ['label' => 'Museum Number (Collection no.)', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('accession_no', ['label' => 'Accession Number', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('excavation_no', ['label' => 'Excavation Number', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('id', ['label' => 'Artifact Number (P no.)', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('all_seal_no', ['label' => 'Seal Number (S. no.)', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('all_composite_no', ['label' => 'Composite Number (Q no.)', 'class' => 'form-control w-100']) ?>
            </div>

            <div class="boxed m-0">
                <h3>Credits</h3>

                <?= $this->Form->control('update_authors', ['label' => 'Author/Contributor', 'class' => 'form-control w-100']) ?>
                <?= $this->Form->control('update_external_resource', ['label' => 'Project', 'class' => 'form-control w-100']) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <?= $this->Form->button(__('Search') . ' <span class="fa fa-search" aria-hidden="true"></span>', [
                'type' => 'submit',
                'class' => 'btn cdli-btn-blue',
                'escapeTitle' => false
            ]) ?>
            <?= $this->Html->link(__('Clear'), ['advanced', '?' => []], ['class' => 'btn my-4 ml-3 w-30']) ?>
        </div>
    </div>
<?= $this->Form->end()?>

<?php $this->append('script'); ?>
<script type="text/javascript">
    $(function () {
        $('form').find(':input').prop('disabled', false)
        $('form').submit(function() {
            $(this).find(':input').filter(function () { return $(this).val() === '' }).attr('disabled', 'disabled')
            return true
        })
    })
</script>
<?php $this->end(); ?>
