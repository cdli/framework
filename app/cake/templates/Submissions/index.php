<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Submission[]|\Cake\Collection\CollectionInterface $submissions
 */
?>
<div class="submissions index content">
    <?= $this->Html->link(__('New Submission'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Submissions') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('submission_id') ?></th>
                    <th><?= $this->Paginator->sort('locale') ?></th>
                    <th><?= $this->Paginator->sort('context_id') ?></th>
                    <th><?= $this->Paginator->sort('section_id') ?></th>
                    <th><?= $this->Paginator->sort('current_publication_id') ?></th>
                    <th><?= $this->Paginator->sort('date_last_activity') ?></th>
                    <th><?= $this->Paginator->sort('date_submitted') ?></th>
                    <th><?= $this->Paginator->sort('last_modified') ?></th>
                    <th><?= $this->Paginator->sort('stage_id') ?></th>
                    <th><?= $this->Paginator->sort('status') ?></th>
                    <th><?= $this->Paginator->sort('submission_progress') ?></th>
                    <th><?= $this->Paginator->sort('work_type') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($submissions as $submission): ?>
                <tr>
                    <td><?= $this->Number->format($submission->submission_id) ?></td>
                    <td><?= h($submission->locale) ?></td>
                    <td><?= $this->Number->format($submission->context_id) ?></td>
                    <td><?= $submission->has('section') ? $this->Html->link($submission->section->section_id, ['controller' => 'Sections', 'action' => 'view', $submission->section->section_id]) : '' ?></td>
                    <td><?= $this->Number->format($submission->current_publication_id) ?></td>
                    <td><?= h($submission->date_last_activity) ?></td>
                    <td><?= h($submission->date_submitted) ?></td>
                    <td><?= h($submission->last_modified) ?></td>
                    <td><?= $this->Number->format($submission->stage_id) ?></td>
                    <td><?= $this->Number->format($submission->status) ?></td>
                    <td><?= $this->Number->format($submission->submission_progress) ?></td>
                    <td><?= $this->Number->format($submission->work_type) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $submission->submission_id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $submission->submission_id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $submission->submission_id], ['confirm' => __('Are you sure you want to delete # {0}?', $submission->submission_id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
