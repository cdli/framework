<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Language $language
 */
?>
<?php use Cake\Utility\Inflector; ?>

<div>
    <div class="text-heading text-left"><?= __(h(Inflector::humanize(Inflector::singularize(Inflector::tableize($this->request->getParam('controller')))))) ?>: <?= h($language->language) ?>
      <?php if ($access_granted): ?>
          <?= $this->Html->link(
              __('Edit'),
              ['prefix' => 'Admin', 'action' => 'edit', $language->id],
              ['escape' => false , 'class' => "btn cdli-btn-blue pull-right", 'title' => 'Edit']
          ) ?>
      <?php endif; ?>
    </div>
</div>
<p class="text-left"><?= $language->notes ?></p>

<div class="text-left my-4" style="font-size: large;">
    <ul>
        <li><?= __('Protocol Code') ?>: <?= h($language->protocol_code) ?></li>
        <li><?= __('Inline Code') ?>: <?= h($language->inline_code) ?></li>
    </ul>
</div>

<?php if ($language->has('parent_language')): ?>
    <div class="single-entity-wrapper text-left mt-4">
        <div class="capital-heading"><?= __('Parent Language') ?></div>
        <?= $this->Html->link(
              $language->parent_language->language,
              ['controller' => 'Languages', 'action' => 'view', $language->parent_language->id]
                            ) ?>
    </div>
<?php endif; ?>

<?php if (!empty($language->child_languages)): ?>
    <div class="single-entity-wrapper text-left mx-0">
        <div class="capital-heading"><?= __('Child Languages') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
                <th scope="col"><?= __('Language') ?></th>
                <th scope="col"><?= __('Protocol Code') ?></th>
                <th scope="col"><?= __('Inline Code') ?></th>
            </thead>
            <tbody>
                <?php foreach ($language->child_languages as $childLanguages): ?>
                    <tr>
                        <td><?= $this->Html->link(
                            __(h($childLanguages->language)),                
                            [
                              'controller' => 'Languages',
                              'action' => 'View',
                              $childLanguages->id]
                              ) ?></td>
                        <td><?= h($childLanguages->protocol_code) ?></td>
                        <td><?= h($childLanguages->inline_code) ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>

<?php if ($count != 0): ?>
    <div class="single-entity-wrapper text-left mx-0">
        <div class="capital-heading"><?= __('Related Artifacts') ?></div>
        <p>
          <?php echo 'There are '.$count.' artifacts related to '.h($language->language).' '.h(Inflector::humanize(Inflector::singularize(Inflector::tableize($this->request->getParam('controller')))))?><br>
            <?= $this->Html->link(__('Click here to view the artifacts'),[
                'controller' => 'Search',
                'action' => 'index',
                '?' => ['language' => h($language->language)]
            ]) ?>
        </p>
    </div>
<?php endif; ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>