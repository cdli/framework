<?php
/**
 * @var \App\View\AppView $this
 */

use Cake\Utility\Inflector;

$this->assign('title', __('Update event on {0}', h($updateEvent->created)));
?>

<div class="row justify-content-md-center">
    <div class="col-lg-12 text-left">
        <h1>Update event</h1>

        <p>
            <span class="badge badge-secondary">
                <?= h($updateEvent->status) ?>
            </span>
            <?php if ($updateEvent->has('reviewer')): ?>
                by <?= $this->Html->link(
                h($updateEvent->reviewer->author),
                ['controller' => 'Authors', 'action' => 'view', $updateEvent->reviewer->id]
                ) ?>
            <?php endif; ?>
        </p>
        <p>
            <?= h(implode(', ', array_map([Inflector::class, 'humanize'], $updateEvent->update_type))) ?>
            update <?= $updateEvent->status === 'created' ? __('created') : __('submitted') ?>
            by
            <?= $this->Html->link(
                h($updateEvent->creator->author),
                ['controller' => 'Authors', 'action' => 'view', $updateEvent->creator->id]
            ) ?>
            on
            <?= h($updateEvent->created) ?>
            with credits to
            <?php
                if (!empty($updateEvent->authors)) {
                    $total = count($updateEvent->authors);
                    foreach ($updateEvent->authors as $i => $author) {
                        if ($i == $total && $total > 1) echo ' and ';
                        echo $this->Html->link(
                            $author->author,
                            ['controller' => 'Authors', 'action' => 'view', $author->id]
                        );
                        if ($i != $total) echo '; ';
                    }
                } else {
                    echo $this->Html->link(
                        $updateEvent->creator->author,
                        ['prefix' => false, 'controller' => 'Authors', 'action' => 'view', $updateEvent->creator->id]
                    );
                }
            ?>
            <?php if ($updateEvent->has('external_resource')): ?>
                project:
                <?= $this->Html->link(
                    h($updateEvent->external_resource->external_resource),
                    ['controller' => 'ExternalResources', 'action' => 'view', $updateEvent->external_resource->id]
                ) ?>
            <?php endif; ?>

            <?= h($updateEvent->event_comments) ?>

            <?php if ($isAdmin || $isAuthor): ?>
                <?= $this->Html->link(
                    __('Edit'),
                    ['controller' => 'UpdateEvents', 'action' => 'edit', $updateEvent->id],
                    ['class' => 'btn cdli-btn-light']
                ) ?>
            <?php endif; ?>
        </p>
    </div>

    <?php
      // - admin and author can delete unless approved
      // - author can submit unless already submitted
      // - reviewer (includes admin) can approve when submitted
      // - admin and author can edit
    ?>
    <?php if (($isAdmin || $isReviewer || $isAuthor) && $updateEvent->status != 'approved'): ?>
        <div class="col-lg-7 boxed">
            <?php if ($isReviewer && $updateEvent->status == 'submitted'): ?>
                <?php if ($isApprovable): ?>
                    <?= $this->Form->postLink(
                        __('Approve & apply'),
                        ['controller' => 'UpdateEvents', 'action' => 'approve', $updateEvent->id],
                        ['class' => 'btn cdli-btn-blue']
                    ) ?>
                <?php else: ?>
                    <?= $this->Form->button(
                        __('Approve & apply'),
                        ['class' => 'btn cdli-btn-blue', 'disabled' => true]
                    ) ?>
                <?php endif; ?>
            <?php endif; ?>
            <?php if ($isAuthor && $updateEvent->status == 'created'): ?>
                <?= $this->Form->postLink(
                    __('Submit'),
                    ['controller' => 'UpdateEvents', 'action' => 'submit', $updateEvent->id],
                    [
                        'class' => 'btn cdli-btn-light',
                        'confirm' => __('Are you sure you want to submit your drafted update?')
                    ]
                ) ?>
            <?php endif; ?>
            <div class="float-right">
                <?php if ($isReviewer && $updateEvent->status == 'submitted'): ?>
                    <?= $this->Form->postLink(
                        __('Decline'),
                        ['controller' => 'UpdateEvents', 'action' => 'decline', $updateEvent->id],
                        [
                            'class' => 'btn btn-danger',
                            'confirm' => __('Are you sure you want to decline this update?')
                        ]
                    ) ?>
                <?php endif; ?>
                <?php if (($isAdmin || $isAuthor) && $updateEvent->status != 'approved'): ?>
                    <?= $this->Form->postLink(
                        __('Delete'),
                        ['controller' => 'UpdateEvents', 'action' => 'delete', $updateEvent->id],
                        [
                            'class' => 'btn btn-danger',
                            'confirm' => $updateEvent->status == 'created'
                                ? __('Are you sure you want to delete this drafted update?')
                                : __('Are you sure you want to delete this submitted update?')
                        ]
                    ) ?>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="col-12 text-left">
        <?php if ($updateEvent->missingPrivateChanges): ?>
            <div class="alert alert-warning">
                <?= __('This update event contains hidden changes') ?>
            </div>
        <?php endif; ?>

        <?php if ($updateEvent->has('artifacts_updates') && !empty($updateEvent->artifacts_updates)): ?>
            <h2><?= __('Changes to {0} in this update', 'artifacts') ?></h2>
            <?= $this->element('artifactUpdateDiffStat', ['updateEvent' => $updateEvent]) ?>
        <?php endif; ?>

        <?php if ($updateEvent->has('artifact_asset_annotations') && !empty($updateEvent->artifact_asset_annotations)): ?>
            <h2><?= __('Changes to {0} in this update', 'visual annotations') ?></h2>
            <?= $this->element('annotationDiffStat', ['updateEvent' => $updateEvent]) ?>
        <?php endif; ?>

        <?php if ($updateEvent->has('entities_updates') && !empty($updateEvent->entities_updates)): ?>
            <h2><?= __('Changes to {0} in this update', 'other entities') ?></h2>
            <?= $this->element('diff/otherEntity', ['updateEvent' => $updateEvent]) ?>
        <?php endif; ?>

        <?php if ($updateEvent->has('inscriptions') && !empty($updateEvent->inscriptions)): ?>
            <h2><?= __('Changes to {0} in this update', 'inscriptions') ?></h2>
            <?= $this->element('inscriptionDiffStat', ['updateEvent' => $updateEvent]) ?>
        <?php endif; ?>
    </div>
</div>
