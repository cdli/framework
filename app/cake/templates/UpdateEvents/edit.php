<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UpdateEvent $event
 */

$this->Form->setTemplates([
    'inputContainer' => '<div class="input form-group {{type}}{{required}}">{{content}}</div>'
]);
$this->assign('title', __('Edit update event'));
?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <p>Privacy warning: by submitting your suggestion(s), you acknowledge that your name will be associated publicly with it once it is accepted.</p>
        <div class="abbreviations form content">
            <?= $this->Form->create($event) ?>
                <legend class="capital-heading"><?= __('Edit information of changes') ?></legend>
                <p>This form can be used to provide credit for the submitted changes.</p>

                <div class="form-group">
                    <?= $this->Form->control('update_type', [
                        'class' => 'form-control',
                        'options' => [
                            ['text' => 'Transliteration', 'value' => 'atf'],
                            ['text' => 'Translation', 'value' => 'translation'],
                            ['text' => 'Annotation', 'value' => 'annotation'],
                            ['text' => 'Image annotation', 'value' => 'visual_annotation'],
                            ['text' => 'Artifact metadata', 'value' => 'artifact'],
                            ['text' => 'Other metadata', 'value' => 'other_entity']
                        ],
                        'multiple' => true,
                        'empty' => false,
                        'required' => true,
                    ]) ?>
                </div>
                <div class="form-group">
                    <?= $this->Form->control('event_comments', [
                        'class' => 'form-control'
                    ]); ?>
                    <small class="form-text text-muted">Add a comment to summarise the changes.</small>
                </div>
                <div class="form-group">
                    <?= $this->Form->control('external_resource_id', [
                        'class' => 'form-control',
                        'options' => $external_resources,
                        'empty' => true
                    ]) ?>
                    <small class="form-text text-muted">Credit a contributing project.</small>
                </div>
                <div class="form-group">
                    <?= $this->element('entityPicker', [
                        'model' => 'Authors',
                        'entities' => $event->authors
                    ]) ?>
                    <small class="form-text text-muted">Credit any additional authors.</small>
                </div>

                <hr>

                <div class="submit">
                    <?= $this->Form->button(__('Save'), ['class' => 'btn cdli-btn-blue']) ?>
                    <?= $this->Html->link(
                        __('Cancel'),
                        ['action' => 'view', $event->id],
                        ['class' => 'btn']
                    ) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $event->id], [
                        'class' => 'btn btn-danger float-right',
                        'confirm' => __('Are you sure you want to delete # {0}?', $event->id)
                    ]) ?>
                </div>
            <?= $this->Form->end() ?>
        </div>
    </div>

</div>
