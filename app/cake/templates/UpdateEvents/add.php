<?php
/**
 * @var \App\View\AppView $this
 */
$this->Form->setTemplates([
    'inputContainer' => '<div class="input form-group {{type}}{{required}}">{{content}}</div>'
]);
$this->assign('title', __('Add information to changes'));
$updateTypes = [
    ['text' => 'Transliteration', 'value' => 'atf'],
    ['text' => 'Translation', 'value' => 'translation'],
    ['text' => 'Annotation', 'value' => 'annotation'],
    ['text' => 'Image annotation', 'value' => 'visual_annotation'],
    ['text' => 'Artifact metadata', 'value' => 'artifact'],
    ['text' => 'Other metadata', 'value' => 'other_entity']
];
?>

<?= $this->element('changesetStatus', ['currentState' => 2]) ?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <?= $this->Form->create($event) ?>
            <legend class="capital-heading"><?= __('Add information to changes') ?></legend>
            <p>This form can be used to provide credit for the submitted changes.</p>

            <div class="form-group">
                <label>Update type</label>
                <?php foreach ($updateTypes as $updateType): ?>
                    <div class="form-check">
                        <?= $this->Form->checkbox('update_type[]', [
                            'class' => 'form-check-input',
                            'id' => 'update_type-' . $updateType['value'],
                            'value' => $updateType['value'],
                            'checked' => in_array($updateType['value'], $event->update_type),
                            'disabled' => $updateType['value'] !== 'translation' || !in_array('atf', $event->update_type),
                            'hiddenField' => false,
                        ]) ?>
                        <label for="update_type-<?= $updateType['value'] ?>" class="form-check-label">
                            <?= $updateType['text'] ?>
                        </label>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="form-group">
                <?= $this->Form->control('event_comments', [
                    'class' => 'form-control'
                ]) ?>
                <small class="form-text text-muted">Add a comment to summarise the changes.</small>
            </div>
            <div class="form-group">
                <?= $this->Form->control('external_resource_id', [
                    'class' => 'form-control',
                    'options' => $external_resources,
                    'empty' => true
                ]) ?>
                <small class="form-text text-muted">Credit a contributing project.</small>
            </div>
            <div class="form-group">
                <?= $this->element('entityPicker', [
                    'model' => 'Authors',
                    'entities' => $event->authors
                ]) ?>
                <small class="form-text text-muted">Credit any additional authors.</small>
            </div>

            <hr>

            <div class="alert alert-warning">
            Privacy warning: by submitting your suggestion(s), you acknowledge that your name will be associated publicly with it once it is accepted.
            </div>

            <div class="submit">
                <?= $this->Form->button(__('Submit'), [
                    'class' => 'btn cdli-btn-blue',
                    'name' => 'action',
                    'value' => 'submitted'
                ]) ?>
                <?= $this->Form->button(__('Save as draft'), [
                    'class' => 'btn cdli-btn-light',
                    'name' => 'action',
                    'value' => 'created'
                ]) ?>
                <?= $this->Html->link(__('Cancel'), ['controller' => 'Changesets', 'action' => 'index'], [
                    'class' => 'btn cdli-btn-light float-right'
                ]) ?>
            </div>
        <?= $this->Form->end() ?>
    </div>

    <div class="col-lg-12 boxed">
        <?php if ($event->has('artifacts_updates') && !empty($event->artifacts_updates)): ?>
            <h2><?= __('Changes to {0} in this update', 'artifacts') ?></h2>
            <?= $this->element('artifactUpdateDiffStat', ['updateEvent' => $event]) ?>
        <?php endif; ?>

        <?php if ($event->has('artifact_asset_annotations') && !empty($event->artifact_asset_annotations)): ?>
            <h2><?= __('Changes to {0} in this update', 'visual annotations') ?></h2>
            <?= $this->element('annotationDiffStat', ['updateEvent' => $event]) ?>
        <?php endif; ?>

        <?php if ($event->has('entities_updates') && !empty($event->entities_updates)): ?>
            <h2><?= __('Changes to {0} in this update', 'other entities') ?></h2>
            <?= $this->element('diff/otherEntity', ['updateEvent' => $event]) ?>
        <?php endif; ?>

        <?php if ($event->has('inscriptions') && !empty($event->inscriptions)): ?>
            <h2><?= __('Changes to {0} in this update', 'inscriptions') ?></h2>
            <?= $this->element('inscriptionDiffStat', ['updateEvent' => $event]) ?>
        <?php endif; ?>
    </div>
</div>
