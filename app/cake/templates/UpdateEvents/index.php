<?php
/**
 * @var \App\View\AppView $this
 */

use Cake\Utility\Inflector;

$this->assign('title', 'Update Events');
?>
<div class="row">
    <div class="col-9 text-left">
        <h3 class="display-4 pt-3 text-left pl-0">
            <?= __('Update Events') ?>
        </h3>
    </div>
    <div class="col-12 boxed">
        <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
            <thead>
                <tr align="left">
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Creator') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('update_type', __('Type of changes')) ?></th>
                    <th scope="col"><?= __('Authors') ?></th>
                    <th scope="col"><?= __('Description') ?></th>
                    <th scope="col"><?= __('Project') ?></th>
                    <th scope="col"><?= __('Approved by') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('status', __('Status')) ?></th>
                    <th scope="col"><?= __('Action') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($updateEvents as $updateEvent): ?>
                    <tr align="left">
                        <td><?= h($updateEvent->created) ?></td>
                        <td><?= $this->Html->link(
                            h($updateEvent->creator->author),
                            ['controller' => 'Authors', 'action' => 'view', $updateEvent->creator->id]
                        ) ?></td>
                        <td><?= h(implode(', ', array_map([Inflector::class, 'humanize'], $updateEvent->update_type))) ?></td>
                        <td>
                            <?php if ($updateEvent->has('authors') && !empty($updateEvent->authors)): ?>
                                <?= implode('; ', array_map(function ($author) {
                                    return $this->Html->link(
                                        $author->author,
                                        ['controller' => 'Authors', 'action' => 'view', $author->id]
                                    );
                                }, $updateEvent->authors)) ?>
                            <?php else: ?>
                                <?= $this->Html->link(
                                    $updateEvent->creator->author,
                                    ['controller' => 'Authors', 'action' => 'view', $updateEvent->creator->id]
                                ) ?>
                            <?php endif; ?>
                        </td>
                        <td><?= h($updateEvent->event_comments) ?></td>
                        <td>
                            <?php if ($updateEvent->has('external_resource')): ?>
                                <?= $this->Html->link(
                                    h($updateEvent->external_resource->external_resource),
                                    ['controller' => 'ExternalResources', 'action' => 'view', $updateEvent->external_resource->id]
                                ) ?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if ($updateEvent->has('reviewer')): ?>
                                <?= $this->Html->link(
                                    h($updateEvent->reviewer->author),
                                    ['controller' => 'Authors', 'action' => 'view', $updateEvent->reviewer->id]
                                ) ?>
                            <?php endif; ?>
                        </td>
                        <td><?= h($updateEvent->status) ?></td>
                        <td align="center"><?= $this->Html->link(
                            $this->Html->image('/images/view.svg', ['alt' => __('View')]),
                            ['controller' => 'UpdateEvents', 'action' => 'view', $updateEvent->id],
                            ['escapeTitle' => false, 'title' => __('View')]
                        ) ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php echo $this->element('Paginator'); ?>
    </div>

    <?= $this->element('citeBottom') ?>
    <?= $this->element('citeButton') ?>
</div>
