<?php
/**
 * @var \App\View\AppView $this
 */

$this->assign('title', __('Upload Publication Links'));
?>

<?= $this->element('changesetStatus', ['currentState' => 0]) ?>

<div class="row justify-content-md-center">
    <div class="col-lg-12 text-left">
        <h2><?= __('Upload Publication Links') ?></h2>

        <p>
            Upload links between publications and artifact or other entities, as a CSV file.
            <?= $this->Html->link('Documentation.', [
                'controller' => 'Docs',
                'action' => 'view',
                'adding-and-editing-entities-publications-editor'
            ]) ?>
            To delete links between publications and artifacts, please use the <?= $this->Html->link(
                'catalogue data upload',
                ['controller' => 'ArtifactsUpdates', 'action' => 'add']
            ) ?>.
        </p>
    </div>

    <div class="col-lg-12 text-left form-wrapper">
        <?= $this->Form->create(null, ['type' => 'file']) ?>

            <?= $this->Form->control('upload', ['type' => 'file', 'class' => 'form-control']) ?>

            <hr>

            <?= $this->Form->button(__('Continue'), ['class'=>'btn btn-primary cdli-btn-blue mr-2']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
