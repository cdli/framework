<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Author $author
 */

use Cake\Utility\Inflector;
?>

<main class="container">
    <h1 class="display-3 header-text text-left">
        <?= h($author->author) ?>
        <?php if ($access_granted) : ?>
            <?= $this->Html->link(
                'Edit Author',
                ['prefix' => 'Admin', 'action' => 'edit', $author->id],
                ['class' => 'btn cdli-btn-blue', 'style' => "float: right;"]
            ) ?>
        <?php endif; ?>
    </h1>
    <p>&nbsp;</p>

    <div class="text-justify">
        <ul>
            <li><?= __('Institution') ?>: <?= h($author->institution) ?></li>
            <li><?= __('Email') ?>: <?= h($author->email) ?></li>
            <li><?= __('ORCID ID') ?>: <?= $this->Orcid->format($author->orcid_id, 'full') ?></li>
            <?php if ($author->birth_year) : ?>
                <li><?= __('Birth Year') ?>: <?= h($author->birth_year) ?></li>
            <?php endif; ?>
            <?php if ($author->death_year) : ?>
                <li><?= __('Death Year') ?>: <?= h($author->death_year) ?></li>
            <?php endif; ?>
            <li>
                This name should be presented in
                <?php if ($author->east_asian_order == 1): ?>
                    <a href="https://en.wikipedia.org/wiki/Personal_name#Eastern_name_order">
                        "Eastern name order"
                    </a>
                <?php else: ?>
                    <a href="https://en.wikipedia.org/wiki/Personal_name#Western_name_order">
                        "Western name order"
                    </a>
                <?php endif; ?>
            </li>
        </ul>
        <em>This profile was last modified on <?= h($author->modified) ?></em>

        <?php if (!empty($author->biography)): ?>
            <h2 class="text-left display-4 section-title">Biography and Research</h2>
            <div class="text-justify">
                <?= ($author->biography) ?>
            </div>
        <?php endif; ?>

        <?php if (!empty($publications->first())): ?>
            <h2 class="text-left display-4 section-title"><?= __('Publications') ?></h2>
            <div class="text-justify">
                <?php foreach ($publications as $publication) : ?>
                    <?= $this->element('publicationReference', ['publication' => $publication]) ?>
                <?php endforeach; ?>
            </div>
            <br>
            <div class='text-center'>
                <?php $this->Paginator->options(['model' => 'Publications']) ?>
                <?php echo $this->element('Paginator'); ?>
            </div>
        <?php endif; ?>

        <!-- Staff -->
        <?php if (!empty($staff)) : ?>
            <h2 class="text-left display-4 section-title">
                <?= __('{0} at CDLI', $this->Html->link($staff->cdli_title, [
                    'controller' => 'Staff',
                    'action' => 'index',
                    '#' => $staff->staff_type_id
                ])) ?>
            </h2>
            <?php if (!empty($staff->contribution)) : ?>
                <h3><?= __('Contributions') ?></h3>
                <div class="text-justify">
                    <?= $staff->contribution; ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>

        <!-- Update events -->
        <?php if (!empty($updateEvents->first())) : ?>
            <div class="row justify-content-md-center">
                <div class="col-lg-12 boxed">
                    <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
                        <thead>
                            <tr align="left">
                                <th scope="col"><?= h('Created') ?></th>
                                <th scope="col"><?= h('Type') ?></th>
                                <th scope="col"><?= h('Description') ?></th>
                                <th scope="col"><?= h('Role') ?></th>
                                <th scope="col"><?= h('Status') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($updateEvents as $updateEvent): ?>
                                <?php
                                    // The roles will contain the following people, Reviewer, Editor, Creat
                                    $roles = [];
                                    // get the fields for reviewer and creator
                                    if ($updateEvent->created_by == $author->id) {
                                        $roles[] = __('Creator');
                                    }
                                    if ($updateEvent->approved_by == $author->id) {
                                        $roles[] = __('Reviewer');
                                    }
                                    // check if the current author is the author of the event
                                    if (in_array($author->id, array_column($updateEvent->authors, 'id'))) {
                                        $roles[] = __('Author');
                                    }
                                ?>
                                <tr align="left">
                                    <!-- Created -->
                                    <td>
                                        <?= $this->Html->link(
                                            h($updateEvent->created),
                                            ['prefix' => false, 'controller' => 'UpdateEvents', 'action' => 'view', $updateEvent->id]
                                        ) ?>
                                    </td>
                                    <!-- Update event type -->
                                    <td><?= h(implode(', ', array_map([Inflector::class, 'humanize'], $updateEvent->update_type))) ?></td>
                                    <!-- Update event comments -->
                                    <td><?= h($updateEvent->event_comments) ?> </td>
                                    <!-- Role -->
                                    <td><?= implode(', ', $roles) ?></td>
                                    <!-- Status -->
                                    <td><?= h($updateEvent->status) ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <?php $this->Paginator->options(['model' => 'UpdateEvents']) ?>
                    <?= $this->element('Paginator'); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</main>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
