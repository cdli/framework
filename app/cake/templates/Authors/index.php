<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Author[]|\Cake\Collection\CollectionInterface $authors
 */
?>

<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left">Authors Index</h1>
    <?= $this->element('addButton'); ?>
</div>

<?= $this->Html->link(__('ALL'), ['action' => 'view'], ['class' => 'btn btn-action']) ?>
<?php foreach (range('A', 'Z') as $char): ?>
    <?= $this->Html->link($char, ['action' => 'view', '?' => ['letter' => $char]], ['class' => 'btn btn-action']) ?>
<?php endforeach ?>

<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>

<?= $this->element('search_form') ?>

<table class="table-bootstrap my-3 mx-0">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('author', __('Author Name')) ?></th>
            <th scope="col"><?= $this->Paginator->sort('institution') ?></th>
            <th scope="col">Email</th>
            <th scope="col">ORCID ID</th>
            <th scope="col">Birth Year</th>
            <th scope="col">Death Year</th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($authors as $author): ?>
            <tr>
                <td align="left"><?= $this->Html->link($author->author, ['prefix' => false, 'action' => 'view', $author->id]) ?></td>
                <td align="left"><?= h($author->institution) ?></td>
                <td align="left"><?= h($author->email) ?></td>
                <td align="left"><?= $this->Orcid->format($author->orcid_id, 'compact') ?></td>
                <td><?= h($author->birth_year) ?></td>
                <td><?= h($author->death_year) ?></td>
                <td>
                    <?php if ($access_granted): ?>
                        <?= $this->Html->image('/images/edit.svg', [
                            'alt' => 'Edit',
                            'url' => ['prefix' => 'Admin', 'action' => 'edit', $author->id],
                            'title' => 'Edit'
                        ]) ?>
                    <?php endif ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
