<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsUpdate $artifactsUpdate
 */

$this->assign('title', 'Submit catalogue changes');

$this->Form->setTemplates([
    'inputContainer' => '<div class="input form-group {{type}}{{required}}">{{content}}</div>'
]);
?>

<?= $this->element('changesetStatus', ['currentState' => 0]) ?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <h2><?= __('Upload Catalogue data') ?></h2>
        <p>
            Use this form to <span class="font-weight-bold">validate</span> catalogue
            data or to <span class ="font-weight-bold">submit</span> catalogue addition
            or change suggestions to CDLI.<br> Your file must be formatted in Comma
            Separated Values (CSV) format. For more information, please consult
            the <?= $this->Html->link(
                'guide on preparing catalogue data',
                ['controller' => 'Docs', 'action' => 'view', 'add-and-edit-artifact-metadata']
            ); ?>. If you used FileMaker to create the CSV file, please be sure to
            include column names.<br>
        </p>

        <?php if (!$canSubmitEdits) : ?>
            <p>You need to register an account to submit addition or edition
            suggestions to a CDLI editor for review. Please email cdli-support
            (at) ames.ox.ac.uk to activate your crowdsourcing privilege.</p>
        <?php endif; ?>

        <p>
            To submit information about a single artifact not yet in the CDLI dataogue,
            you can use the
            <?= $this->Html->link(
                'Add Artifact',
                ['controller' => 'Artifacts', 'action' => 'add']
            ); ?>
            form. To suggest an edition to an existing single artifact, first search for the
            artifact using <a href="/">Search</a>, then press on the edit artifact button.
        </p>
        <p>
            If you activate the toggle for concatenation, you can submit additions to fields which contain multiple values and some free text fields. For example, you could provide new external resources links or publication links without having to provide, and without overwriting, the existing ones, or add a comment following existing comments.
        </p>

        <?= $this->Form->create(null, [
            'type' => 'file',
            'url' => ['controller' => 'ArtifactsUpdates', 'action' => 'add']
        ]) ?>
        <?= $this->Form->control('csv', [
            'class' => 'form-control',
            'label' => 'CSV file with new or modified entries',
            'type' => 'file'
        ]) ?>
        <?= $this->Form->control('concatenate_data', [
            'id' => 'concatenate_data',
            'class' => 'form-control',
            'type' => 'toggle',
            'label' => __('Concatenate multi-value fields with existing data?')
        ]) ?>
        <?= $this->Form->submit(__('Continue'), ['class' => 'btn cdli-btn-blue']) ?>
        <?= $this->Form->end(); ?>
    </div>
</div>
