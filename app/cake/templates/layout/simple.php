<?php
use Cake\Routing\Router;
?>

<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= h($this->fetch('title')) ?> - Cuneiform Digital Library Initiative
    </title>

    <?php if ($this->fetch('image')): ?>
        <meta name="twitter:card" content="summary_large_image">
        <meta property="og:image" content="<?= h(Router::url($this->fetch('image'), true)) ?>" />
    <?php else: ?>
        <meta name="twitter:card" content="summary">
    <?php endif; ?>

    <meta property="og:title" content="<?= h($this->fetch('title')) ?>" />
    <meta property="og:url" content="<?= Router::url($this->getRequest()->getRequestTarget(), true) ?>" />
    <meta property="og:site_name" content="CDLI">
    <meta name="twitter:site" content="@cdli_news">

    <?php if ($this->fetch('description')): ?>
        <meta name="description" content="<?= h($this->fetch('description')) ?>">
        <meta property="og:description" content="<?= h($this->fetch('description')) ?>" />
    <?php endif; ?>

    <?= $this->fetch('meta') ?>

    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('main.css')?>
    <?= $this->Html->css('font-awesome/css/font-awesome.min.css')?>
    <?= $this->fetch('css') ?>

    <?php
        // For using Bootstrap dropdowns, set variable $includePopper in your view
        if (isset($includePopper) && $includePopper) {
            echo $this->Html->script('popper.min.js');
        }
    ?>
    <?= $this->Html->script('popper.min.js');?>
    <?= $this->Html->script('jquery.min.js')?>
    <?= $this->Html->script('bootstrap.min.js')?>
    <?= $this->Html->script('js.js')?>
    <?= $this->Html->script('drawer.js', ['defer' => true]) ?>
    <?= $this->fetch('library') ?>
</head>
<body>
    <?= $this->fetch('content') ?>

    <?= $this->fetch('modal') ?>
    <script>
        $(document).ready(function() {
            $('header .no-js-dd').removeClass('no-js-dd');
        });
    </script>
    <?= $this->fetch('script') ?>
</body>
</html>
