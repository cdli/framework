<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Abbreviation[]|\Cake\Collection\CollectionInterface $abbreviations
 */
?>

<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('Abbreviations') ?></h1>
    <?= $this->element('addButton'); ?>
</div>

<?= $this->Html->link(__('ALL'), ['action' => 'view'], ['class' => 'btn btn-action']) ?>
<?php foreach (range('A', 'Z') as $char): ?>
    <?= $this->Html->link($char, ['action' => 'view', '?' => ['letter' => $char]], ['class' => 'btn btn-action']) ?>
<?php endforeach ?>
<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>

<?= $this->element('search_form') ?>

<div class="abbreviations index content">
     <div class="table-responsive table-hover">
        <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
            <thead align="left">
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('abbreviation') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('fullform') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                    <th scope="col"><?= __('Publication') ?></th>
                    <?php if ($access_granted): ?>
                        <th scope="col"><?= __('Action') ?></th>
                    <?php endif ?>
                </tr>
            </thead>
            <tbody>
               <?php foreach ($abbreviations as $abbreviation): ?>
                <tr>
                    <td align="left"><?= h($abbreviation->abbreviation) ?></td>
                    <td align="left"><?= h($abbreviation->fullform) ?></td>
                    <td align="left"><?= h($abbreviation->type) ?></td>
                    <td align="left">
                        <?php if (!empty($abbreviation->publication)): ?>
                            <?= $this->Html->link(
                                $abbreviation->publication->designation ?? $abbreviation->publication->bibtexkey,
                                ['controller' => 'Publications', 'action' => 'view', $abbreviation->publication_id]
                            ) ?>
                        <?php endif ?>
                    </td>
                    <?php if ($access_granted): ?>
                        <td class="btn">
                            <?= $this->Html->link(__('Edit'),['prefix' => 'Admin', 'action' => 'edit', $abbreviation->id],
                                ['escape' => false , 'class' => "btn btn-warning btn-sm", 'title' => 'Edit']) ?>
                        </td>
                    <?php endif ?>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('Paginator'); ?>

</div>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
