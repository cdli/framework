<main>
    <h2 class="text-left display-4 section-title">Highlights</h2>
    <?= $this->element('search_form', ['legend' => __('Highlights Search')]) ?>
    <div class="row mt-5">
        <?php foreach ($highlights as $highlight): ?>
            <div class="card bs4-cdli-card col-sm-12 col-md-6 col-lg-4 mb-5">
                <img src="<?= $this->ArtifactAssets->getThumbnail($highlight->artifact) ?>"
                     alt="Highlight <?= $highlight->title . ' ' . $highlight->image_type ?>"
                     class="card-img-top">
                <div class="card-body">
                    <p class="card-title">
                        <?= $this->Html->link(
                            strlen($highlight->title) > 38 ? substr($highlight->title, 0, 38) . '...' : $highlight->title,
                            ['controller' => 'Postings', 'action' => 'view', $highlight->id]
                        ) ?>
                    </p>
                    <p class="card-text">
                        <?=
                        strlen(strip_tags($highlight->body)) > 100 ? substr(strip_tags($highlight->body), 0, 100) . '...' : strip_tags($highlight->body)
                        // use above to trim extra card-length
                        ?>
                    </p>
                </div>
            </div>
        <?php endforeach;?>
    </div>
    <?= $this->element('Paginator'); ?>
</main>
<?php echo $this->element('smoothscroll'); ?>
