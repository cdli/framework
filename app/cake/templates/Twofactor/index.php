<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="row justify-content-md-center">

    <div class="capital-heading text-center">
        <h1 class="display-4 header-txt">Two-factor authentication setup</h1>
    </div>
    <?= $this->Flash->render() ?>

    <?php
    if (env('APP_ENV') !== 'development') {
        require_once '/srv/app/cake/plugins/GoogleAuthenticate/GoogleAuthenticator.php';
        $ga = new PHPGangsta_GoogleAuthenticator();
        $secret = $ga->createSecret();
        $oneCode = $ga->getCode($secret);
        //  echo "Checking Code '$oneCode' and Secret '$secret':\n";
        $qrCodeUrl = $ga->getQRCodeGoogleUrl('cdli framework', $secret);
    }
    ?>

    <?php if (env('APP_ENV') === 'development') { ?>
        <div class="boxed col-md-6">
            <div class="capital-heading">Two Factor Authentication</div>
            <?= $this->Form->create() ?>
            <div class="form-group text-left my-4">
                For Two-Factor Authentication (2FA) setup,
                <br>
                set <b>APP_ENV = "Production"</b> in <b>.env</b> and try again.
                <br>
                Currently you are in <b>development</b> mode.
            </div>
            <?= $this->Form->input('code', [
                'type' => 'hidden',
                'value' => 'random_code_value'
            ]); ?>
            <?= $this->Form->input('secretcode', [
                'type' => 'hidden',
                'value' => 'random_code_value'
            ]); ?>

            <?= $this->Form->submit('Skip', ['class' => 'form-control btn btn-primary col-md-2']); ?>
            <?= $this->Form->end() ?>
        </div>
    <?php } else { ?>
        <div class="boxed col-md-6">
            <div class="capital-heading">Two Factor Authentication</div>
            <p>Please download "Google Authenticator" app on your mobile device.</p>
            <p>In Google Authenticator, click on the plus sign at the bottom right, chose the option "Enter a setup key".</p>
            <p>In Google Authenticator, enter your setup key: <span class="font-weight-bold"><?= $secret ?></span></p>
            <p>Enter the code provided back by authenticator below.</p>
            <?= $this->Flash->render('auth') ?>
            <?= $this->Flash->render() ?>

            <?= $this->Form->create() ?>
            <div class="form-group text-left my-4">
                <?= $this->Form->control('code', [
                    'type' => 'text',
                    'class' => 'form-control w-100 mb-3',
                    'label' => __('Google Authenticator 2FA code'),
                    'required' => true
                ]); ?>
                <?= $this->Form->input('secretcode', [
                    'type' => 'hidden',
                    'value' => $secret
                ]); ?>
                <?= $this->Form->submit('Submit', ['class' => 'btn btn-primary cdli-btn-blue mr-2']); ?>
            </div>
            <?= $this->Form->end() ?>
            For detailed instructions, please visit the <a href="https://cdli-gh.github.io/guides/cdli_two_factor_guide.html"> "CDLI two factor guide"</a>
        </div>
    <?php } ?>
</div>
<script language="JavaScript" type="text/javascript">
    $(document).ready(function () {
        setTimeout(function(){
            location.reload(true);
        }, 150000);
    });
</script>
