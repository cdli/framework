<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MaterialColor $materialColor
 */
?>
<?php use Cake\Utility\Inflector; ?>

<div>
    <div class="text-heading text-left"><?= __(h(Inflector::humanize(Inflector::singularize(Inflector::tableize($this->request->getParam('controller')))))) ?>: <?= h($materialColor->material_color) ?>
      <?php if ($access_granted): ?>
          <?= $this->Html->link(
              __('Edit'),
              ['prefix' => 'Admin', 'action' => 'edit', $materialColor->id],
              ['escape' => false , 'class' => "btn cdli-btn-blue pull-right", 'title' => 'Edit']
          ) ?>
      <?php endif; ?>
    </div>
</div>

<?php if (!empty($materialColor->artifacts_materials)): ?>
    <div class="single-entity-wrapper mx-0">
        <div class="capital-heading"><?= __('Related Artifacts Materials') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Artifact Id') ?></th>
                <th scope="col"><?= __('Material Id') ?></th>
                <th scope="col"><?= __('Is Material Uncertain') ?></th>
                <th scope="col"><?= __('Material Color Id') ?></th>
                <th scope="col"><?= __('Material Aspect Id') ?></th>
            </thead>
            <tbody>
                <?php foreach ($materialColor->artifacts_materials as $artifactsMaterials): ?>
                <tr>
                    <td><?= h($artifactsMaterials->id) ?></td>
                    <td><?= h($artifactsMaterials->artifact_id) ?></td>
                    <td><?= h($artifactsMaterials->material_id) ?></td>
                    <td><?= h($artifactsMaterials->is_material_uncertain) ?></td>
                    <td><?= h($artifactsMaterials->material_color_id) ?></td>
                    <td><?= h($artifactsMaterials->material_aspect_id) ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>

<?php if ($count != 0): ?>
    <div class="single-entity-wrapper text-left mx-0">
        <div class="capital-heading"><?= __('Related Artifacts') ?></div>
        <p>
          <?php echo 'There are '.$count.' artifacts related to '.h(Inflector::humanize(Inflector::singularize(Inflector::tableize($this->request->getParam('controller')))))?><br>
            <?= $this->Html->link(__('Click here to view the artifacts'), [
              'controller' => 'Search',
              '?' => ['material_color' => h($materialColor->material_color)]
            ]) ?>
        </p>
    </div>
<?php endif; ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
