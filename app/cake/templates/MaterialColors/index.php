<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MaterialColor[]|\Cake\Collection\CollectionInterface $materialColors
 */
?>

<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('Material Colors') ?></h1>   
        <?= $this->element('addButton'); ?>    
</div>
<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('material_color') ?></th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($materialColors as $materialColor): ?>
            <tr align="left">
                <td>
                    <?= $this->Html->link(
                        $materialColor->material_color,
                        ['controller' => 'MaterialColors', 'action' => 'view', $materialColor->id]
                    ) ?>
                </td>
                <?php if ($access_granted): ?>
                    <td>
                        <?= $this->Html->link(
                            __('Edit'),
                            ['prefix' => 'Admin', 'action' => 'edit', $materialColor->id],
                            ['escape' => false , 'class' => 'btn btn-warning btn-sm', 'title' => 'Edit']
                        ) ?>
                    </td>
                <?php endif ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>