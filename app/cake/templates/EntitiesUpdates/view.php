<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\EntitiesUpdate $entitiesUpdate
 */

use Cake\Utility\Inflector;

$this->assign('title', 'Revision');

try {
    $entity = $entitiesUpdate->getEntity();
    $entityName = $entity->get($entitiesUpdate->getTable()->getDisplayField());
    $entityName = empty($entityName) ? '' : '"' . $entityName . '"';
} catch (\Exception $e) {
    $entity = null;
    $entityName = '(deleted)';
}
?>

<div class="row justify-content-md-center">
    <div class="col-lg-12 boxed">
        <h2>
            <?php if (!$entitiesUpdate->has('entity_id')): ?>
                <?= Inflector::singularize(Inflector::humanize($entitiesUpdate->entity_table)) ?>
                <div class="badge badge-secondary">
                    <?= __('new') ?>
                </div>
            <?php else: ?>
                <?= $this->Html->link(
                    Inflector::singularize(Inflector::humanize($entitiesUpdate->entity_table)) . ' ' . $entityName,
                    [
                        'controller' => Inflector::camelize($entitiesUpdate->entity_table),
                        'action' => 'view',
                        $entitiesUpdate->entity_id
                    ]
                ) ?>
            <?php endif; ?>
        </h2>

        <?php if (!empty($entitiesUpdate->update_event->event_comments)): ?>
            <p><?= h($entitiesUpdate->update_event->event_comments) ?></p>
        <?php endif; ?>
        <p><?= $this->element('updateEventHeader', ['update_event' => $entitiesUpdate->update_event]) ?></p>
        <?= $this->element('diff/otherEntityUpdate', ['entitiesUpdate' => $entitiesUpdate]) ?>
    </div>
</div>
