<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Date[]|\Cake\Collection\CollectionInterface $dates
 */
?>

<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('Dates') ?></h1>
    <?= $this->element('addButton'); ?>
</div>


<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('day_no') ?></th>
            <th scope="col"><?= $this->Paginator->sort('month_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_uncertain') ?></th>
            <th scope="col"><?= $this->Paginator->sort('month_no') ?></th>
            <th scope="col"><?= $this->Paginator->sort('year_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('dynasty_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('ruler_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('absolute_year') ?></th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($dates as $date): ?>
        <tr>
            <td><?= h($date->day_no) ?></td>
            <td><?= $date->has('month') ? $this->Html->link($date->month->month_no, ['controller' => 'Months', 'action' => 'view', $date->month->id]) : '' ?></td>
            <td><?= h($date->is_uncertain) ?></td>
            <td><?= h($date->month_no) ?></td>
            <td><?= $date->has('year') ? $this->Html->link($date->year->year_no, ['controller' => 'Years', 'action' => 'view', $date->year->id]) : '' ?></td>
            <td><?= $date->has('dynasty') ? $this->Html->link($date->dynasty->dynasty, ['controller' => 'Dynasties', 'action' => 'view', $date->dynasty->id]) : '' ?></td>
            <td><?= $date->has('ruler') ? $this->Html->link($date->ruler->ruler, ['controller' => 'Rulers', 'action' => 'view', $date->ruler->id]) : '' ?></td>
            <td><?= h($date->absolute_year) ?></td>
            <td>
            <?php if ($access_granted): ?>
                <?= $this->Html->link(__('Edit'),['prefix' => 'Admin', 'action' => 'edit', $date->id],
                        ['escape' => false , 'class' => "btn btn-warning btn-sm", 'title' => 'Edit']) ?>
            <?php endif ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>