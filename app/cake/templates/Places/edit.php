<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Place $place
 */
?>

<?= $this->element('changesetStatus', ['currentState' => 0]) ?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($place) ?>
            <legend class="form-heading mb-3">
                <?php if ($place->isNew()): ?>
                    <?= __('Add Place') ?>
                <?php else: ?>
                    <?= __('Edit Place {0}', h($place->place)) ?>
                <?php endif; ?>
            </legend>

            <?= $this->Form->control('place', ['class' => 'form-control w-100 mb-3', 'required' => true]) ?>

            <?= $this->Form->control('location_id', ['class' => 'form-control w-100 mb-3', 'options' => $locations, 'empty' => true]) ?>
            <!--?= $this->Form->control('period_id', ['class' => 'form-control w-100 mb-3', 'options' => $periods, 'empty' => true]) ?-->

            <h2 class="text-uppercase display-4 section-title mt-5 mb-4"><?= __('Linked information') ?></h2>
            <p><?= __('External Resources') ?></p>
            <div class="many-to-many">
                <div id="external-resources-wrapper">
                    <?php foreach (array_merge(array_keys($place->external_resources ?? []), ['template']) as $x): ?>
                        <div class="form-group border rounded pt-3 px-3<?= $x == 'template' ? ' d-none' : '' ?>">
                            <?= $this->Form->control("external_resources.$x.id", [
                                'class' => 'form-control w-100 mb-3',
                                'type' => 'select',
                                'options' => $externalResources,
                                'label' => __('External Resource'),
                            ]) ?>
                            <?= $this->Form->control("external_resources.$x._joinData.external_resource_key", [
                                'class' => 'form-control w-100 mb-3',
                                'type' => 'text',
                                'label' => __('External Resource Key'),
                                'empty' => true,
                            ]) ?>
                            <?= $this->Form->iconButton(__('Remove'), 'times-circle') ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?= $this->Form->iconButton(__('Add External Resource'), 'plus-circle', ['data-for' => 'external-resources-wrapper']) ?>
            </div>

            <?= $this->Form->button(__('Continue'), ['class'=>'btn btn-primary cdli-btn-blue mr-2']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<?php $this->append('script'); ?>
<?= $this->Html->script('form-control-many-to-many'); ?>
<?php $this->end() ?>
