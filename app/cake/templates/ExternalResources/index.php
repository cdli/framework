<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ExternalResource[]|\Cake\Collection\CollectionInterface $externalResources
 */
?>
<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('External Resources') ?></h1>
    <?= $this->element('addButton'); ?>
</div>
<?= $this->Html->link(__('ALL'), ['action' => 'view'], ['class' => 'btn btn-action']) ?>
<?php foreach (range('A', 'Z') as $char) : ?>
    <?= $this->Html->link($char, ['action' => 'view', '?' => ['letter' => $char]], ['class' => 'btn btn-action']) ?>
<?php endforeach ?>
<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>
<?= $this->element('search_form') ?>
<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('external_resource') ?></th>
            <th scope="col"><?= $this->Paginator->sort('abbrev') ?></th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($externalResources as $externalResource): ?>
            <tr align="left">
                <td>
                    <?= $this->Html->link(
                        $externalResource->external_resource,
                        ['action' => 'view', $externalResource->id]
                    ) ?>
                </td>
                <td><?= h($externalResource->abbrev) ?></td>
                <td>
                <?php if ($access_granted): ?>
                    <?= $this->Html->link(
                        __('Edit'),
                        ['prefix' => 'Admin', 'action' => 'edit', $externalResource->id],
                        ['escape' => false , 'class' => 'btn btn-warning btn-sm', 'title' => 'Edit']
                    ) ?>
                <?php endif ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?= $this->element('Paginator'); ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
