<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inscription $inscription
 */

$changesAtf = in_array('atf', $inscription->update_event->update_type);
$changesTranslation = in_array('translation', $inscription->update_event->update_type);
$changesAnnotation = in_array('annotation', $inscription->update_event->update_type);

$title = [];
if ($changesAtf) { $title[] = __('Transliteration'); }
if ($changesTranslation) { $title[] = __('Translation'); }
if ($changesAnnotation) { $title[] = __('Linguistic Annotation'); }
$title = $this->Text->toList($title);

$this->assign('title', "Text of {$inscription->artifact->designation}, revision {$inscription->id}");
?>


<div class="row justify-content-md-center">
    <div class="col-lg-12 text-left">
        <h1><?= $title ?></h1>

        <h2><?= $this->Html->link(
            $inscription->artifact->designation . ' (' . $inscription->artifact->getCdliNumber() . ')',
            ['controller' => 'Artifacts', 'action' => 'view', $inscription->artifact->id]
        ) ?></h2>
        <?= $this->element('updateEventHeader', ['update_event' => $inscription->update_event]);?>
        <pre><?= $this->Diff->diffInscription(
            $oldInscription,
            $inscription,
            $changesAnnotation ? 'annotation' : 'atf'
        ) ?></pre>

        <?php if ($changesAtf && !$changesAnnotation && $inscription->has('annotation')): ?>
            <h3><?= __('Automatic changes to CoNLL annotation') ?></h3>

            <pre><?= $this->Diff->diffInscription($oldInscription, $inscription, 'annotation') ?></pre>
        <?php endif; ?>
    </div>
</div>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
