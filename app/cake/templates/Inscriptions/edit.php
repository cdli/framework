<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inscription $inscription
 */

$this->assign('title', 'Edit text of ' . $artifact->getCdliNumber());
$this->Form->setTemplates([
    'inputContainer' => '<div class="input form-group {{type}}{{required}}">{{content}}</div>'
]);
$this->loadHelper('Tabs');

$asset = $this->ArtifactAssets->orderAssets($artifact)[0];
if (!is_null($asset)) {
    $image = DS . $asset->getPath();
}
?>

<?= $this->element('changesetStatus', ['currentState' => 0]) ?>

<div class="row justify-content-md-center">
    <?php if (!is_null($asset)): ?>
        <div class="col-lg-6">
            <div class="boxed">
                <h2><?= __('Artifact image') ?></h2>
                <figure>
                    <?= $this->Html->image($image, [
                        'alt' => $asset->getDescription(),
                        'class' => 'w-100'
                    ]) ?>
                </figure>

                <div class="text-left mt-3">
                    <?= $this->element('assetAttribution', [
                        'asset' => $asset,
                        'collection' => $artifact->collections[0] ?? null,
                    ]) ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="col-lg-6">
        <div class="boxed">
            <?= $this->Tabs->create(['atf' => __('Transliterations'), 'conll' => __('Annotations')], $tab) ?>
                <?= $this->Tabs->createTab('atf') ?>
                    <?= $this->Form->create($inscription, [
                        'type' => 'file',
                        'url' => ['controller' => 'Inscriptions', 'action' => 'add'],
                        'class' => 'pt-2'
                    ]) ?>
                        <p>
                            This form is for submitting changes to transliterations
                            of texts, in the C-ATF format. You can use the
                            <?= $this->Html->link('Uqnu editor', '/uqnu' , ['class' => 'link']) ?>
                            to check and edit your ATF before submitting it.
                        </p>

                        <?= $this->Form->control('atf', [
                            'name' => 'atf_paste',
                            'label' => __('ATF'),
                            'class' => 'form-control',
                            'style' => 'width: 100%;',
                            'type' => 'textarea',
                            'rows' => '20'
                        ]) ?>

                        <?= $this->Form->submit(__('Continue'), ['class' => 'btn cdli-btn-blue']) ?>
                    <?= $this->Form->end(); ?>
                <?= $this->Tabs->endTab() ?>
                <?= $this->Tabs->createTab('conll') ?>
                    <?= $this->Form->create($inscription, [
                        'type' => 'file',
                        'url' => ['controller' => 'Inscriptions', 'action' => 'add'],
                        'class' => 'pt-2'
                    ]) ?>
                        <p>
                            This form is for submitting changes to annotations
                            of texts, in the CDLI-CoNLL format. You can use the
                            <?= $this->Html->link('Morphology Pre-Annotation Tool', [
                                'controller' => 'Resources',
                                'action' => 'checkCdliConll'
                            ]) ?>
                            to check and edit your CDLI-CoNLL before submitting it.
                        </p>

                        <?php if (!$inscription->is_atf2conll_diff_resolved): ?>
                            <div class="alert alert-warning">
                                This annotation was changed automatically to match a new
                                version of the transliteration, but has not yet been
                                reviewed since.
                                <?php if ($isAdmin): ?>
                                    <?= $this->Html->link(
                                        __('Review this annotation.'),
                                        [
                                            'prefix' => 'Admin',
                                            'controller' => 'inscriptions',
                                            'action' => 'view',
                                            $inscription->id
                                        ]
                                    ) ?>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>

                        <?= $this->Form->control('annotation', [
                            'name' => 'conll_paste',
                            'class' => 'form-control',
                            'label' => __('CDLI-CoNLL'),
                            'style' => 'width: 100%;',
                            'type' => 'textarea',
                            'rows' => '20'
                        ]) ?>
                        <?= $this->Form->submit(__('Continue'), ['class' => 'btn cdli-btn-blue']) ?>
                    <?= $this->Form->end(); ?>
                <?= $this->Tabs->endTab() ?>
            <?= $this->Tabs->end() ?>
        </div>
    </div>

    <?php if (!is_null($artifact)): ?>
        <div class="col-lg-12">
            <?= $this->cell('ArtifactView', [$artifact->id])->render('display') ?>
        </div>
    <?php endif; ?>
</div>
