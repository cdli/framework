<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inscription $inscription
 * @var string $tab
 * @var ?string $input
 */

$this->assign('title', 'Add text or annotation');
$this->Form->setTemplates([
    'inputContainer' => '<div class="input form-group {{type}}{{required}}">{{content}}</div>'
]);
$this->loadHelper('Tabs');
?>

<?= $this->element('changesetStatus', ['currentState' => 0]) ?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <h2><?= __('Add or edit transliteration(s), transcription(s), and translation(s)') ?></h2>

        <p>
            <?php if (!$canSubmitEdits): ?>
                You need to register an account to submit addition or edition
                suggestions to a CDLI editor for review. Please email cdli-support
                (at) ames.ox.ac.uk to activate your crowdsourcing privilege.
            <?php endif; ?>
        </p>

        <?= $this->Tabs->create(['atf' => __('Transliterations'), 'conll' => __('Annotations')], $tab) ?>
            <?= $this->Tabs->createTab('atf') ?>
                <?= $this->Form->create(null, ['type' => 'file', 'class' => 'pt-2']) ?>
                    <p>
                        This form is for submitting additions and changes to
                        transliterations of texts, in the C-ATF format.
                    </p>

                    <p>
                        Uploaded files and pasted text can contain multiple ATF entries.
                        The associated artifact will be derived from the first line of
                        each ATF entry. You cannot submit ATF for artifacts that are not
                        in the catalogue.
                    </p>

                    <p>
                        You can use the
                        <?= $this->Html->link('Uqnu editor', '/uqnu' , ['class' => 'link']) ?>
                        to check and edit your ATF before submitting it.
                    </p>

                    <?= $this->Form->control('atf_paste', [
                        'value' => $tab === 'atf' && isset($input) ? $input : null,
                        'class' => 'form-control',
                        'label' => __('Paste text'),
                        'style' => 'width: 100%;',
                        'type' => 'textarea'
                    ]) ?>

                    <?= $this->Form->control('atf_file', [
                        'class' => 'form-control',
                        'label' => __('Upload file'),
                        'type' => 'file'
                    ]) ?>

                    <?= $this->Form->submit('Proceed', ['class' => 'btn cdli-btn-blue']) ?>
                <?= $this->Form->end(); ?>
            <?= $this->Tabs->endTab() ?>
            <?= $this->Tabs->createTab('conll') ?>
                <?= $this->Form->create(null, ['type' => 'file', 'class' => 'pt-2']) ?>
                    <p>
                        This form is for submitting additions and changes to
                        annotations of texts, in the CDLI-CoNLL format.
                    </p>

                    <p>
                        Uploaded files and pasted text can contain multiple CDLI-CoNLL entries.
                        The associated artifact will be derived from the first line of
                        each CDLI-CoNLL entry. You cannot submit CDLI-CoNLL for artifacts
                        that are not in the catalogue. You also cannot submit CDLI-CoNLL
                        for artifacts without transliterations.
                    </p>

                    <p>
                        You can use the
                        <?= $this->Html->link('Morphology Pre-Annotation Tool', [
                            'controller' => 'Resources',
                            'action' => 'checkCdliConll'
                        ]) ?>
                        to check and edit your CDLI-CoNLL before submitting it.
                    </p>

                    <?= $this->Form->control('conll_paste', [
                        'value' => $tab === 'conll' && isset($input) ? $input : null,
                        'class' => 'form-control',
                        'label' => __('Paste text'),
                        'style' => 'width: 100%;',
                        'type' => 'textarea'
                    ]) ?>

                    <?= $this->Form->control('conll_file', [
                        'class' => 'form-control',
                        'label' => __('Upload file'),
                        'type' => 'file'
                    ]) ?>
                    <?= $this->Form->submit('Proceed', ['class' => 'btn cdli-btn-blue']) ?>
                <?= $this->Form->end(); ?>
            <?= $this->Tabs->endTab() ?>
        <?= $this->Tabs->end() ?>
    </div>
</div>
