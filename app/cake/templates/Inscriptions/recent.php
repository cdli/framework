<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inscription[]|\Cake\Collection\CollectionInterface $inscriptions
 * @var array $periods
 * @var array $proveniences
 */
?>

<div class="row">
    <div class="col-12 text-left">
        <h1><?= __('Recent ATF changes') ?></h1>
    </div>
</div>

<div class="row text-left">
    <!-- Filters -->
    <div id="search-filters" class="col-lg-3 col-12 pt-3">
        <h3>
            <?= __('Search') ?>

            <button id="search-filter-collapse" class="btn bg-white float-right mx-1">
                <span class="h3 m-0">
                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                </span>
                <span class="sr-only"><?= __('Close') ?></span>
            </button>
        </h3>

        <?= $this->Form->create(['data' => $queryParams, 'schema' => []], ['url' => ['action' => 'view', 'recent'], 'type' => 'get']) ?>
            <?= $this->Form->button(__('Search'), ['class' => 'btn cdli-btn-blue mb-3', 'type' => 'submit']) ?>
            <?= $this->Html->link(__('Reset'), ['action' => 'index'], ['class' => 'btn cdli-btn-light mb-3']) ?>

            <?= $this->Form->control('period_id', ['label' => __('Period'), 'options' => $periods, 'empty' => true, 'class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('provenience_id', ['label' => __('Proveniences'), 'options' => $proveniences, 'empty' => true, 'class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('dates_referenced', ['label' => __('Dates'), 'class' => 'form-control w-100 mb-3']) ?>
        <?= $this->Form->end() ?>
    </div>

    <div id="search-results" class="col-lg-9 col-12">
        <div class="clearfix">
            <?= $this->Html->link(
                __('Search') . ' <span class="fa fa-chevron-right ml-1 align-middle" aria-hidden="true"></span>',
                '#',
                [
                    'id' => 'search-filter-open',
                    'class' => 'float-md-left my-3 btn cdli-btn-blue d-lg-none',
                    'escapeTitle' => false
                ]
            ) ?>

            <div class="my-3 float-md-right text-center">
                <div class="d-inline-block">
                    <?= $this->element('paginatorSimple') ?>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table-bootstrap mx-0">
                <thead>
                    <tr>
                        <th scope="col"><?= __('Artifact') ?></th>
                        <th scope="col"><?= __('Period') ?></th>
                        <th scope="col"><?= __('Provenience') ?></th>
                        <th scope="col"><?= __('Date') ?></th>
                        <th scope="col"><?= __('Modified') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($inscriptions as $inscription): ?>
                        <tr>
                            <td>
                                <?= $this->Html->link(
                                    $inscription->artifact->getCdliNumber(),
                                    ['controller' => 'Artifacts', 'action' => 'view', $inscription->artifact_id]
                                ) ?>
                                <br>
                                <?= $inscription->artifact->designation ?>
                            </td>
                            <td nowrap="nowrap">
                                <?php if ($inscription->artifact->has('period')): ?>
                                    <?= $this->Html->link(
                                        $inscription->artifact->period->period,
                                        ['controller' => 'Periods', 'action' => 'view', $inscription->artifact->period_id]
                                    ) ?>
                                <?php endif; ?>
                            </td>
                            <td nowrap="nowrap">
                                <?php if ($inscription->artifact->has('provenience')): ?>
                                    <?= $this->Html->link(
                                        $inscription->artifact->provenience->provenience,
                                        ['controller' => 'Proveniences', 'action' => 'view', $inscription->artifact->provenience_id]
                                    ) ?>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if ($inscription->artifact->has('dates_referenced')): ?>
                                    <?= $this->Text->truncate($inscription->artifact->dates_referenced, 25) ?>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?= h($inscription->update_event->created->toDateString()) ?>
                                <br>
                                <?= $this->Html->link(
                                    __('View changes'),
                                    ['action' => 'view', $inscription->id],
                                    ['class' => 'text-nowrap']
                                ) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>

        <?= $this->element('Paginator') ?>
    </div>
</div>

<?php $this->append('library'); ?>
<?= $this->Html->script('searchResult') ?>
<?php $this->end(); ?>
