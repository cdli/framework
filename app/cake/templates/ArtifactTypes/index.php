<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactType[]|\Cake\Collection\CollectionInterface $artifactTypes
 */
?>


<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('Artifact Types') ?></h1>
    <?= $this->element('addButton'); ?>
</div>
<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>
<?= $this->element('search_form') ?>
<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('artifact_type') ?></th>
            <th scope="col"><?= $this->Paginator->sort('parent_id') ?></th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactTypes as $artifactType): ?>
            <tr>
                <td>
                    <?= $this->Html->link(
                        $artifactType->artifact_type,
                        ['controller' => 'ArtifactTypes', 'action' => 'view', $artifactType->id]
                    ) ?>
                </td>
                <td>
                    <?php if ($artifactType->has('parent_artifact_type')): ?>
                        <?= $this->Html->link(
                            $artifactType->parent_artifact_type->artifact_type,
                            ['controller' => 'ArtifactTypes', 'action' => 'view', $artifactType->parent_artifact_type->id]
                        ) ?>
                    <?php endif; ?>
                </td>
                <td>
                <?php if ($access_granted): ?>
                    <?= $this->Html->link(
                        __('Edit'),
                        ['prefix' => 'Admin', 'action' => 'edit', $artifactType->id],
                        ['escape' => false , 'class' => 'btn btn-warning btn-sm', 'title' => 'Edit']
                    ) ?>
                <?php endif ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
