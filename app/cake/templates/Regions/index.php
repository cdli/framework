<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Region[]|\Cake\Collection\CollectionInterface $regions
 */
?>

<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('Regions') ?></h1>
    <?= $this->element('addButton'); ?>
</div>
<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>

<div class="row justify-content-md-center my-4">
    <div class="col-lg-10">
        <div id="map" style="height: 500px;z-index: 0;"></div>
    </div>
</div>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('region') ?></th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($regions as $region): ?>
            <tr align="left">
                <td>
                    <?= $this->Html->link(
                        $region->region,
                        ['controller' => 'Regions', 'action' => 'view', $region->id]
                    ) ?>
                </td>
                <?php if ($access_granted): ?>
                    <td>
                        <?= $this->Html->link(
                            __('Edit'),
                            ['prefix' => 'Admin', 'action' => 'edit', $region->id],
                            ['escape' => false , 'class' => 'btn btn-warning btn-sm', 'title' => 'Edit']
                        ) ?>
                    </td>
                <?php endif ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

<?= $this->element('leaflet') ?>
<?php $this->append('script'); ?>
<script>
(function () {
    var regions = <?= json_encode($regions) ?>

    var map = L.map('map')
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map)
    var featureGroup = []

    for (var i = 0; i < regions.length; i++) {
        var coordinates = JSON.parse('[' + regions[i].geo_coordinates + ']')
        for (var j = 0; j < coordinates.length; j++) {
            coordinates[j].reverse()
        }
        var polygon = L.polygon(coordinates).addTo(map).bindPopup(
            '<a href="/regions/' + regions[i].id + '">' + regions[i].region + '</a>'
        )
        featureGroup.push(polygon)
    }

    featureGroup = L.featureGroup(featureGroup)

    var bounds = featureGroup.getBounds()
    map.setView(bounds.getCenter(), map.getBoundsZoom(bounds))
})()
</script>
<?php $this->end(); ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>