<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Provenience $provenience
 */

$this->assign('title', h($provenience->provenience) . ' - ' . __('Proveniences'));
?>

<div class="row justify-content-md-center">
    <div class="col-lg-12 text-left">
        <h1><?= __('Metadata history') ?></h1>
        <h2><?= $this->Html->link($provenience->provenience, ['action' => 'view', $provenience->id]) ?></h2>
    </div>

    <?php foreach ($updateEvents as $updateEvent): ?>
        <?php $entitiesUpdate = $updateEvent->_matchingData['EntitiesUpdates'] ?>
        <div class="col-lg-12 boxed">
            <?php if ($entitiesUpdate->entity_table === 'proveniences'): ?>
                <h3><?= __('Provenience') ?> <?= h($provenience->provenience) ?></h2>
            <?php elseif ($entitiesUpdate->entity_table === 'places'): ?>
                <h3><?= __('Place') ?> <?= h($provenience->place->place) ?></h2>
            <?php elseif ($entitiesUpdate->entity_table === 'locations'): ?>
                <h3><?= __('Location') ?> <?= h($provenience->location->location) ?></h2>
            <?php endif; ?>

            <?= $this->element('updateEventHeader', ['update_event' => $updateEvent]) ?>

            <?php if (!empty($updateEvent->event_comments)): ?>
                <p><?= h($updateEvent->event_comments) ?></p>
            <?php endif; ?>

            <?= $this->element('diff/otherEntityUpdate', ['entitiesUpdate' => $entitiesUpdate]) ?>
        </div>
    <?php endforeach; ?>
</div>
