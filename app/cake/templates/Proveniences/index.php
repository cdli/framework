<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Provenience[]|\Cake\Collection\CollectionInterface $proveniences
 */
?>

<div class="d-flex justify-content-between align-items-end mb-2">
    <h1 class="display-3 header-txt text-left"><?= __('Proveniences') ?></h1>
    <?php if ($access_granted): ?>
        <?= $this->Html->link('+ ' . __('Add Provenience'), ['action' => 'add'], ['class' => 'btn cdli-btn-blue my-3']) ?>
    <?php endif; ?>
</div>
<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>
<?= $this->element('search_form') ?>
<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr align="left">
            <th scope="col"><?= $this->Paginator->sort('provenience') ?></th>
            <th scope="col"><?= $this->Paginator->sort('region_id') ?></th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($proveniences as $provenience): ?>
            <tr align="left">
                <td>
                    <?= $this->Html->link(
                        $provenience->provenience,
                        ['controller' => 'Proveniences', 'action' => 'view', $provenience->id]
                    ) ?>
                </td>
                <td>
                    <?php if ($provenience->has('region')): ?>
                        <?= $this->Html->link(
                            $provenience->region->region,
                            ['controller' => 'Regions', 'action' => 'view', $provenience->region->id]
                        ) ?>
                    <?php endif; ?>
                </td>
                <?php if ($access_granted): ?>
                    <td>
                        <?= $this->Html->link(
                            __('Edit'),
                            ['prefix' => false, 'action' => 'edit', $provenience->id],
                            ['escape' => false , 'class' => 'btn btn-warning btn-sm', 'title' => 'Edit']
                        ) ?>
                    </td>
                <?php endif ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?= $this->element('Paginator') ?>

<?= $this->element('citeButton') ?>
<?= $this->element('citeBottom') ?>
