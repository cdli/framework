<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Provenience $provenience
 */
?>

<div class="text-left">
    <h1>
        <?= __('Provenience') ?>: <?= h($provenience->provenience) ?>
        <?php if ($canEdit): ?>
            <?= $this->Html->link(
                __('Edit'),
                ['action' => 'edit', $provenience->id],
                ['class' => 'btn cdli-btn-blue float-right ml-3']
            ) ?>
        <?php endif; ?>
        <?= $this->Html->link(
            __('History'),
            ['history', $provenience->id],
            ['class' => 'btn btn-link float-right']
        ) ?>
    </h1>

    <div class="pt-3 text-left">
        <?= $provenience->description ?>
    </div>

    <div class="row">
        <div class="col-lg-6 col-12">
            <table class="table-bootstrap">
                <tr>
                    <th><?= __('Region') ?></th>
                    <td>
                        <?php if ($provenience->has('region')): ?>
                            <?= $this->Html->link($provenience->region->region, [
                                'controller' => 'Regions',
                                'action' => 'view',
                                $provenience->region->id
                            ]) ?>
                        <?php else: ?>
                            &mdash;
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <th><?= __('Related artifacts') ?></th>
                    <td>
                        <?= $this->Html->link(__('{0} artifacts', $count), [
                            'controller' => 'Search',
                            'action' => 'index',
                            '?' => ['provenience' => $provenience->provenience]
                        ]) ?>
                    </td>
                </tr>
            </table>

            <?php if ($provenience->has('place')): ?>
                <div class="single-entity-wrapper">
                    <h2 class="capital-heading">
                        <?= __('Ancient place') ?>
                        <?php if ($canEdit): ?>
                            <?= $this->Html->link(
                                '<span class="fa fa-pencil"><span>',
                                ['controller' => 'Places', 'action' => 'edit', $provenience->place->id],
                                ['class' => 'text-muted float-right', 'escape' => false, 'title' => __('Edit')]
                            ) ?>
                        <?php endif; ?>
                    </h2>
                    <p><?= h($provenience->place->place) ?></p>
                    <?= $this->element('entityExternalResources', ['entity' => $provenience->place]) ?>
                </div>
            <?php endif; ?>

            <?php if ($provenience->has('location')): ?>
                <div class="single-entity-wrapper">
                    <h2 class="capital-heading">
                        <?= __('Location') ?>
                        <?php if ($canEdit): ?>
                            <?= $this->Html->link(
                                '<span class="fa fa-pencil"><span>',
                                ['controller' => 'Locations', 'action' => 'edit', $provenience->location->id],
                                ['class' => 'text-muted float-right', 'escape' => false, 'title' => __('Edit')]
                            ) ?>
                        <?php endif; ?>
                    </h2>
                    <p><?= h($provenience->location->location) ?></p>
                    <?= $this->element('entityNames', ['entity' => $provenience->location]) ?>
                    <?= $this->element('entityExternalResources', ['entity' => $provenience->location]) ?>
                </div>
            <?php endif; ?>

            <?php if (!empty($provenience->archives)): ?>
                <div class="single-entity-wrapper">
                    <h2 class="capital-heading"><?= __('Related Archives') ?></h2>

                    <ul>
                        <?php foreach ($provenience->archives as $archives): ?>
                            <li><?= $this->Html->link($archives->archive, ['controller' => 'Archives', 'action' => 'view', $archives->id]) ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>

            <?php if (!empty($provenience->dynasties)): ?>
                <div class="single-entity-wrapper">
                    <h2 class="capital-heading"><?= __('Related Dynasties') ?></h2>

                    <ul>
                        <?php foreach ($provenience->dynasties as $dynasties): ?>
                            <li><?= $this->Html->link($dynasties->dynasty, ['controller' => 'Dynasties', 'action' => 'view', $dynasties->id]) ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>

            <?php if (!empty($provenience->publications)): ?>
                <div class="single-entity-wrapper">
                    <h2 class="capital-heading">
                        <?= __('Publications') ?>
                    </h2>

                    <ul>
                        <?php foreach ($provenience->publications as $publication): ?>
                            <li>
                                <p class="m-0">
                                    <b><?= $publication->_joinData->publication_type ?></b>:
                                    <?= h($publication->designation) ?>
                                    <?= h($publication->_joinData->exact_reference) ?>
                                </p>

                                <?= $this->Citation->formatReferenceAsHtml($publication, 'bibliography', [
                                    'template' => 'chicago-author-date',
                                    'format' => 'html',
                                    'hyperlinks' => true,
                                    'prepend' => '[' . $this->Html->link(
                                        $publication->bibtexkey ?? 'Details',
                                        ['prefix' => false, 'controller' => 'Publications', 'action' => 'view', $publication->id]
                                    ) . '] ',
                                ]) ?>

                                <?php if (!empty($publication->_joinData->publication_comments)): ?>
                                    <p class="my-3">
                                        <i><?= h($publication->_joinData->publication_comments) ?></i>
                                    </p>
                                    <hr/>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>

        <div class="col-lg-6 col-12">
            <?php if ($provenience->has('location') && $provenience->location->has('location_latitude_wgs1984')): ?>
                <div id="map" style="height: 500px;z-index: 0;"></div>

                <?= $this->element('leaflet') ?>
                <?php $this->append('script'); ?>
<script>
(function () {
    var provenience = <?= json_encode($provenience) ?>;

    var map = L.map('map')
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map)

    var coordinates = [
        provenience.location.location_latitude_wgs1984,
        provenience.location.location_longitude_wgs1984
    ]
    var link = '<a href="/proveniences/' + provenience.id + '">' + provenience.provenience + '</a>'
    L.marker(coordinates).addTo(map).bindPopup(link)

    var view = {
        region: [coordinates, 5],
        site: [coordinates, 10],
    }

    if (provenience.region) {
        var region = JSON.parse('[' + provenience.region.geo_coordinates + ']')
        for (var j = 0; j < region.length; j++) {
            region[j].reverse()
        }

        var polygon = L.polygon(region, { color: 'grey' }).addTo(map)
        var bounds = polygon.getBounds()
        view.region = [bounds.getCenter(), map.getBoundsZoom(bounds)]

        $('#btn-zoom-region').removeClass('d-none').click(function () {
            map.setView(view.region[0], view.region[1])
        })
    }
    if (provenience.location.location_geoshape_wgs1984) {
        var site = L.geoJSON(JSON.parse(provenience.location.location_geoshape_wgs1984)).addTo(map)
        var bounds = site.getBounds()
        view.site = [bounds.getCenter(), map.getBoundsZoom(bounds)]

        $('#btn-zoom-site').removeClass('d-none').click(function () {
            map.setView(view.site[0], view.site[1])
        })
    }

    map.setView(view.region[0], view.region[1])
})()
</script>
                <?php $this->end(); ?>

                <p>
                    <?= __('Coordinates') ?>:
                    <?= $provenience->location->location_longitude_wgs1984 ?>,
                    <?= $provenience->location->location_latitude_wgs1984 ?>
                </p>

                <p>
                    <button id="btn-zoom-region" class="btn cdli-btn-gray d-none"><?= __('Zoom to region') ?></button>
                    <button id="btn-zoom-site" class="btn cdli-btn-gray d-none"><?= __('Zoom to site') ?></button>
                </p>
            <?php endif; ?>
        </div>
    </div>
</div>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
