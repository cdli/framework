<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Provenience $provenience
 */

$this->assign('title', ($provenience->isNew() ? 'New' : h($provenience->provenience)) . ' - ' . __('Proveniences'));
?>

<?= $this->element('changesetStatus', ['currentState' => 0]) ?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($provenience) ?>
            <legend class="form-heading mb-3">
                <?php if ($provenience->isNew()): ?>
                    <?= __('Add Provenience') ?>
                <?php else: ?>
                    <?= __('Edit Provenience {0}', h($provenience->provenience)) ?>
                <?php endif; ?>
            </legend>

            <?= $this->Form->control('provenience', ['class' => 'form-control w-100 mb-3', 'type' => 'text', 'maxLength' => 300]) ?>
            <?= $this->Form->control('description', ['class' => 'form-control w-100 mb-3', 'type' => 'textarea']) ?>

            <hr>

            <?= $this->Form->control('region_id', ['class' => 'form-control w-100 mb-3', 'options' => $regions, 'empty' => true]) ?>
            <?= $this->Form->control('location_id', ['class' => 'form-control w-100 mb-3', 'options' => $locations, 'empty' => true]) ?>
            <?= $this->Form->control('place_id', ['class' => 'form-control w-100 mb-3', 'options' => $places, 'empty' => true]) ?>

            <hr>

            <?= $this->element('formManyToMany/publications', ['publications' => $provenience->publications]) ?>

            <?= $this->Form->button(__('Continue'), ['class'=>'btn btn-primary cdli-btn-blue mr-2']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<?php $this->append('library'); ?>
    <script src="/assets/js/ckeditor/ckeditor.js"></script>
<?php $this->end(); ?>

<?php $this->append('script'); ?>
    <?= $this->Html->script('form-control-entity-picker') ?>
    <?= $this->Html->script('form-control-many-to-many') ?>
    <script>
    CKEDITOR.editorConfig = function( config ) {
        config.htmlEncodeOutput = true;
    };
    CKEDITOR.replace('description', {
        extraPlugins: 'autogrow',
        autoGrow_maxHeight: 800,
        removePlugins: 'resize'
    });
    </script>
<?php $this->end() ?>
