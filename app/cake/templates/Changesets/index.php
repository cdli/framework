<?php
/**
 * @var \App\View\AppView $this
 * @var array $changesets
 */

use Cake\Utility\Inflector;

$this->assign('title', __('Unsaved changesets'));
?>

<?= $this->element('changesetStatus', ['currentState' => 1]) ?>

<div class="text-left">
    <?= $this->Form->create(null, ['url' => ['action' => 'add']]) ?>
        <h2><?= __('Unsaved changesets') ?></h2>

        <p>
            <?= $this->Form->button(__('Select all'), ['id' => 'changesets-select-all', 'type' => 'button', 'class' => 'btn']) ?>
            <?= $this->Form->button(__('Deselect all'), ['id' => 'changesets-select-none', 'type' => 'button', 'class' => 'btn']) ?>
        </p>

        <div class="alert alert-warning">
            These changes are unsaved and may be lost when the browser is closed. To save them, select
            the changes you want to upload, click "Upload" and add a summary describing the changes.
        </div>

        <table class="table-bootstrap">
            <thead>
                <th><?= __('Include') ?></th>
                <th><?= __('Type') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Updates') ?></th>
                <th></th>
            </thead>
            <tbody>
                <?php if (count($changesets) === 0): ?>
                    <tr>
                        <td colspan="5" class="text-center text-muted">
                            <?= __('No unsaved changes') ?>.
                            <?= $this->Html->link(__('Go back'), '/') ?>.
                        </td>
                    </tr>
                <?php endif; ?>

                <?php foreach ($changesets as $id => $changeset): ?>
                    <tr>
                        <td>
                            <?= $this->Form->control("include[$id]", [
                                'type' => 'checkbox',
                                'label' => false,
                                'checked' => !$changeset['hasErrors'],
                                'disabled' => $changeset['hasErrors'],
                            ]) ?>
                        </td>
                        <td><?= h(implode(', ', array_map([Inflector::class, 'humanize'], $changeset['update_type']))) ?></td>
                        <td><?= h($changeset['created']) ?></td>
                        <td>
                            <?= $this->element('updatesSummary', $changeset) ?>
                            <?php if ($changeset['hasErrors']): ?>
                                <span class="badge badge-danger"><?= __('Errors') ?></span>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?= $this->Html->link(__('View details'), ['action' => 'view', $id]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <p>
            Selected changesets:<br>
            <?= $this->Form->button(__('Upload'), ['name' => 'action', 'value' => 'submit', 'class' => 'btn cdli-btn-blue', 'disabled' => count($changesets) === 0]) ?>
            <?= $this->Form->button(__('Discard'), ['name' => 'action', 'value' => 'delete', 'class' => 'btn', 'confirm' => __('Are you sure?'), 'disabled' => count($changesets) === 0]) ?>
        </p>
    <?= $this->Form->end() ?>
</div>

<?php $this->append('script'); ?>
<script>
$(function () {
    $('#changesets-select-all').on('click', function (event) {
        event.preventDefault();
        $('table input[type="checkbox"]:not([disabled])').prop('checked', true);
    })
    $('#changesets-select-none').on('click', function (event) {
        event.preventDefault();
        $('table input[type="checkbox"]:not([disabled])').prop('checked', false);
    })
})
</script>
<?php $this->end(); ?>
