<?php
/**
 * @var array $changeset
 * @var \App\View\AppView $this
 */

use Cake\Utility\Inflector;

$this->assign('title', __('Unsaved changes from {0}', h($changeset['created'])));

$updateEvent = (object) [
    'update_type' => $changeset['update_type'],
    'artifacts_updates' => [],
    'artifact_asset_annotations' => [],
    'entities_updates' => [],
    'inscriptions' => [],
];

foreach ($changeset['updates'] as $update) {
    $updateEvent->{$update->getUpdateEventField()}[] = $update;
}
?>

<div class="text-left">
    <h1><?= __('Changeset') ?></h1>

    <p>
        Unsaved changes to
        <?= h(strtolower($this->Text->toList(array_map([Inflector::class, 'humanize'], $changeset['update_type'])))) ?>
        added on <?= h($changeset['created']) ?>
    </p>

    <div class="mb-3">
        <?php if (!is_null($redraftUrl)): ?>
            <?= $this->Html->link(
                __('Edit'),
                $redraftUrl,
                ['class' => 'btn cdli-btn-blue']
            ) ?>
        <?php endif; ?>
        <?= $this->Form->postLink(
            __('Discard'),
            ['controller' => 'Changesets', 'action' => 'add'],
            ['class' => 'btn cdli-btn-light', 'data' => ['include[' . $changeset['id'] . ']' => 1, 'action' => 'delete']]
        ) ?>
    </div>

    <h2><?= __('Encountered errors') ?></h2>

    <ul>
        <?php foreach ($changeset['errors'] as $error): ?>
            <li><?= h($error) ?></li>
        <?php endforeach; ?>
    </ul>

    <?php if (!empty($updateEvent->artifacts_updates)): ?>
        <h3><?= __('Artifacts') ?></h3>

        <?php $Artifacts = \Cake\ORM\TableRegistry::get('Artifacts'); ?>
        <ul>
            <?php foreach ($updateEvent->artifacts_updates as $update): ?>
                <?php if ($update->hasErrors()): ?>
                    <li>
                        <?= h($update->getDesignation()) ?>

                        <ul>
                            <?php foreach ($update->getErrors() as $field => $errors): ?>
                                <?php
                                    $association = Inflector::pluralize(Inflector::camelize($field));
                                    $displayName = Inflector::pluralize(Inflector::humanize($field));
                                    $route = null;
                                    if ($Artifacts->hasAssociation($association)) {
                                        $possibleRoute = [
                                            'controller' => $Artifacts->getAssociation($association)->getClassName(),
                                            'action' => 'index'
                                        ];
                                        if (\Cake\Routing\Router::routeExists($possibleRoute)) {
                                            $route = $possibleRoute;
                                        }
                                    }
                                ?>
                                <?php foreach ($errors as $error): ?>
                                    <li>
                                        <?= h($field) ?>: <?= h($error) ?>
                                        <?php if (!is_null($route)): ?>
                                            <br>
                                            <?= $this->Html->link(__('Browse {0}', $displayName), $route) ?>
                                        <?php endif; ?>
                                    </li>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>

    <?php if (!empty($updateEvent->artifact_asset_annotations)): ?>
        <ul>
            <?php foreach ($updateEvent->artifact_asset_annotations as $annotation): ?>
                <?php if ($annotation->hasErrors()): ?>
                    <li>
                        <?php if ($annotation->has('artifact_asset')): ?>
                            <?= $annotation->artifact_asset->artifact->getCdliNumber() ?>
                        <?php else: ?>
                            (unknown)
                        <?php endif; ?>

                        <ul>
                            <?php foreach ($annotation->getErrors() as $field => $errors): ?>
                                <?php foreach ($errors as $error): ?>
                                    <li>
                                        <?= h($field) ?>:
                                        <?php if (is_array($error)): ?>
                                            <?php foreach ($error as $subFields => $subErrors): ?>
                                                <?= h($subFields) ?>:
                                                <?php foreach ($subErrors as $subError): ?>
                                                    <?= h($subError) ?>
                                                <?php endforeach; ?>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <?= h($error) ?>
                                        <?php endif; ?>
                                    </li>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>

    <?php if (!empty($updateEvent->inscriptions)): ?>
        <ul>
            <?php foreach ($updateEvent->inscriptions as $inscription): ?>
                <?php if ($inscription->hasErrors()): ?>
                    <li>
                        <?= h(str_pad($inscription->artifact_id, 6, '0', STR_PAD_LEFT)) ?>

                        <ul>
                            <?php foreach ($inscription->getErrors() as $field => $errors): ?>
                                <?php foreach ($errors as $error): ?>
                                    <li><?= h($field) ?>:
                                        <?php if ($field === 'atf'): ?>
                                            <?php $parts = explode("\n\n", $error, 2); ?>
                                            <?= h($parts[0]) ?>
                                            <?php if (array_key_exists(1, $parts)): ?>
                                                <details class="d-inline-block align-text-top">
                                                    <summary><?= __('Click for technical info') ?></summary>
                                                    <pre><?= h($parts[1]) ?></pre>
                                                </details>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <?= h($error) ?>
                                        <?php endif; ?>
                                    </li>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>

    <?php if (!empty($updateEvent->entities_updates)): ?>
        <ul>
            <?php foreach ($updateEvent->entities_updates as $entitiesUpdate): ?>
                <?php if ($entitiesUpdate->hasErrors()): ?>
                    <li>
                        <?php if ($entitiesUpdate->has('entity_id')): ?>
                            <?= h(ucfirst($entitiesUpdate->entity_table)) ?>
                            #<?= h($entitiesUpdate->entity_id) ?>
                        <?php else: ?>
                            <?= __('New {0}', h($entitiesUpdate->entity_table)) ?>

                            <?php $entityUpdateContents = $entitiesUpdate->getUpdate(); ?>
                            <?php if ($entitiesUpdate->entity_table === 'publications' && array_key_exists('bibtexkey', $entityUpdateContents)): ?>
                                (<?= $entityUpdateContents['bibtexkey'] ?>)
                            <?php endif; ?>
                        <?php endif; ?>

                        <ul>
                            <?php foreach ($entitiesUpdate->getErrors() as $field => $errors): ?>
                                <?php foreach ($errors as $error): ?>
                                    <li><?= h($field) ?>:
                                        <?= h(is_array($error) ? json_encode($error) : $error) ?>
                                    </li>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>
