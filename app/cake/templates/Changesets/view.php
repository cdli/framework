<?php
/**
 * @var array $changeset
 * @var \App\View\AppView $this
 */

use Cake\Utility\Inflector;

$this->assign('title', __('Unsaved changes from {0}', h($changeset['created'])));

$updateEvent = (object) [
    'update_type' => $changeset['update_type'],
    'artifacts_updates' => [],
    'artifact_asset_annotations' => [],
    'entities_updates' => [],
    'inscriptions' => [],
];

foreach ($changeset['updates'] as $update) {
    $updateEvent->{$update->getUpdateEventField()}[] = $update;
}
?>

<?= $this->element('changesetStatus', ['currentState' => 1]) ?>

<div class="text-left">
    <h1><?= __('Changeset') ?></h1>

    <p>
        Unsaved changes to
        <?= h(strtolower($this->Text->toList(array_map([Inflector::class, 'humanize'], $changeset['update_type'])))) ?>
        added on <?= h($changeset['created']) ?>.
    </p>

    <p>
        <?= $this->Html->link(__('View all unsaved changes'), ['action' => 'index']) ?>
    </p>

    <div class="mb-3">
        <?php if (!$changeset['hasErrors']): ?>
            <?= $this->Html->link(
                __('Upload'),
                ['controller' => 'UpdateEvents', 'action' => 'add', '?' => ['changesets[]' => $changeset['id']]],
                ['class' => 'btn cdli-btn-blue']
            ) ?>
        <?php endif; ?>
        <?php if ($isBlocking): ?>
            <?= $this->Form->postLink(
                __('Upload later'),
                ['controller' => 'Changesets', 'action' => 'add', '?' => ['redirect' => $redirect]],
                ['class' => 'btn cdli-btn-light', 'data' => ['action' => 'unblock']]
            ) ?>
        <?php endif; ?>
        <?php if (!is_null($redraftUrl)): ?>
            <?= $this->Html->link(
                __('Edit'),
                $redraftUrl,
                ['class' => 'btn cdli-btn-light']
            ) ?>
        <?php endif; ?>
        <?= $this->Form->postLink(
            __('Discard'),
            ['controller' => 'Changesets', 'action' => 'add'],
            [
                'class' => 'btn cdli-btn-light',
                'data' => ['include[' . $changeset['id'] . ']' => 1, 'action' => 'delete'],
                'confirm' => __('Are you sure?')
            ]
        ) ?>
    </div>

    <div class="alert alert-warning">
        These changes are unsaved and may be lost when the browser is closed. To save them, click "Upload"
        and add a summary describing the changes.
    </div>

    <?php if (!empty($updateEvent->artifacts_updates)): ?>
        <h2><?= __('Changes to {0}', 'artifacts') ?></h2>
        <?= $this->element('diff/artifact', ['updateEvent' => $updateEvent]) ?>
    <?php endif; ?>

    <?php if (!empty($updateEvent->artifact_asset_annotations)): ?>
        <h2><?= __('Changes to {0}', 'visual annotations') ?></h2>
        <?= $this->element('annotationDiffStat', ['updateEvent' => $updateEvent]) ?>
    <?php endif; ?>

    <?php if (!empty($updateEvent->entities_updates)): ?>
        <h2><?= __('Changes to {0}', 'other entities') ?></h2>
        <?= $this->element('diff/otherEntity', ['updateEvent' => $updateEvent]) ?>
    <?php endif; ?>

    <?php if (!empty($updateEvent->inscriptions)): ?>
        <h2><?= __('Changes to {0}', 'inscriptions') ?></h2>
        <?= $this->element('inscriptionDiffStat', ['updateEvent' => $updateEvent]) ?>
    <?php endif; ?>
</div>
