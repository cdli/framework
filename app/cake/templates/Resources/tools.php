<?php
/**
 * @var \App\View\AppView $this
 */
?>

<?php
$myTemplates = [
    'inputContainer' => '<div class="w-100">{{content}}</div>',
];
$this->Form->setTemplates($myTemplates);

$this->assign('title', 'Convert, annotate or translate');
?>

<div class="row justify-content-md-center">
    <div class="col-lg-10 boxed">
        <h2><?= __('Convert, annotate or translate') ?></h2>
        <p>Convert your text or textual annotations to other formats or automatically annotate or translate your text(s).</p>
        <p>Use the drop-down to select the converter you would like to use. Upload your file or paste your text(s) or annotations in the text area below. If you both upload a file and use the textarea, the file will have priority. If your file is too large to process, please consider installing the converter or annotator you want to use locally.</p>
        <div class="ml-2">
            <?= $this->Form->create(null, ['type' => 'file']) ?>
            <div class="form-group row">
                <?= $this->Form->control('select_tool', ['label' => 'Select a tool', 'options' => $tools, 'empty' => false, 'class' => "form-control w-100"]);?>
            </div>
            <div class="form-group row">
                <?= $this->Form->control('file_to_convert', ['label' => 'Inscription(s) / Annotations file', 'type' => 'file', 'class' => "form-control w-100"]); ?>
            </div>
            <div class="form-group row">
                <?= $this->Form->control('text_to_convert', ['label' => 'Inscription(s) / Annotations', 'type'=> 'textarea', 'class' =>  "form-control w-100", 'rows' => '14']);?>
            </div>
            <div class="form-group row">
                <?= $this->Form->button(__('Submit'), ['type'=>'submit', 'class' => 'form-control btn cdli-btn-blue']); ?>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
    <div class="col-lg-10 boxed">
        <h2><?= __('Output') ?></h2>
        <p>
            <?php if (isset($result)): ?>
                <pre id="output"><?= $result; ?></pre>
            <?php else: ?>
                <p class="text-muted">
                    <?= __('(none)') ?>
                </p>
            <?php endif; ?>
        </p>
        <button id="download" class="btn cdli-btn-blue"><?= __('Download') ?></button>
    </div>

    <?= $this->element('citeBottom') ?>
    <?= $this->element('citeButton') ?>
</div>

<?php $this->append('script') ?>
<script>
$('#download').click(function() {
    var url = window.URL.createObjectURL(new Blob([$('#output').text()], { type: 'text/plain' }))
    var $a = $(document.createElement('a'))
    $a.attr({ href: url, download: 'formatted.conll' })
    $a.appendTo('body')
    $a[0].click()
    $a.remove()
    window.URL.revokeObjectURL(url)
})
</script>
<?php $this->end('script') ?>
