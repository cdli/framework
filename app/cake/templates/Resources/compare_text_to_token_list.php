<?php
$this->assign('title', 'Compare ATF against token lists');
?>

<div class="row justify-content-md-center">
    <div class="col-lg-10 boxed">
        <h2><?= __('Compare ATF against token lists') ?></h2>
        <p>Compare word and sign frequencies in an ATF file to the auto-generated
        word and sign lists from a period.</p>
        <?= $this->Form->create(null, ['type' => 'file']) ?>
            <div class="form-group">
                <?= $this->Form->label('period', __('Period')) ?>
                <?= $this->Form->select(
                    'period',
                    array_combine(array_column($periods, 'id'), array_column($periods, 'period')),
                    ['class' => 'form-control']
                ) ?>
            </div>
            <div class="form-group">
                <?= $this->Form->label('kind', __('Type of token')) ?>
                <?= $this->Form->select(
                    'kind',
                    ['words', 'signs'],
                    ['class' => 'form-control']
                ) ?>
            </div>

            <div class="form-group">
                <?= $this->Form->control('atf_text', [
                    'class' => 'form-control',
                    'label' => __('Paste text'),
                    'style' => 'width: 100%;',
                    'type' => 'textarea'
                ]) ?>
            </div>
            <div class="form-group">
                <?= $this->Form->control('atf_file', [
                    'class' => 'form-control',
                    'label' => __('Upload file'),
                    'type' => 'file'
                ]) ?>
            </div>

            <div class="form-group">
                <?= $this->Form->submit(__('Submit'), ['class' => 'form-control btn cdli-btn-blue']) ?>
            </div>
        <?= $this->Form->end() ?>
    </div>

    <div class="col-lg-10 boxed">
        <h2><?= __('Output') ?></h2>
        <?php if (isset($output)): ?>
            <?php foreach ($output as $warning): ?>
                <div class="alert alert-<?= h($warning[0]) ?>">
                    <?= h($warning[1]) ?>
                </div>
            <?php endforeach; ?>
            <div class="alert alert-primary">
                <?= __('ATF processed') ?>
            </div>
        <?php else: ?>
            <p class="text-muted">
                <?= __('(none)') ?>
            </p>
        <?php endif; ?>
    </div>

    <?= $this->element('citeBottom') ?>
    <?= $this->element('citeButton') ?>
</div>
