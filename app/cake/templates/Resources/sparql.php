<?php
/**
 * @var \App\View\AppView $this
 * @var array $data
 * @var array $prefixes
 */

use Cake\Routing\Router;

$this->assign('title', 'SPARQL - Resources');
?>

<div class="row justify-content-md-center">
    <div class="col-lg-10 boxed">
        <h2><?= __('SPARQL queries') ?></h2>
        <p>Query a recent export of the CDLI linked data using <a href="https://www.w3.org/TR/sparql11-query/">SPARQL</a>. See also <?= $this->Html->link('the documentation', ['controller' => 'Docs', 'action' => 'view', 'sparql-interface']) ?>.</p>

        <?= $this->Form->create(null, ['id' => 'input', 'type' => 'get', 'action' => Router::url(['controller' => 'Resources', 'action' => 'sparql'])]) ?>
            <?= $this->Form->control('endpoint', [
                'class' => 'form-control w-100 mb-3',
                'val' => $data['endpoint'] ?? 'https://cdli.utoronto.ca/sparql'
            ]) ?>

            <?= $this->Form->control('display', [
                'type' => 'select',
                'class' => 'form-control w-100 mb-3',
                'val' => $data['display'] ?? 'table',
                'options' => [
                    'table' => __('Table'),
                    'graph' => __('Graph'),
                    'map' => __('Map'),
                ],
            ]) ?>

            <details class="mb-3">
                <summary><?= __('Prefixes') ?></summary>
                <pre id="prefixes" class="p-3 bg-white border"><code><?= implode("\n", array_map(function ($prefix, $uri) {
                    return h("PREFIX $prefix: <$uri>");
                }, array_keys($prefixes), array_values($prefixes))) ?></code></pre>
            </details>

            <?= $this->Form->control('query', [
                'type' => 'textarea',
                'class' => 'form-control w-100 mb-3',
                'rows' => 16,
                'style' => 'font-family: monospace;',
                'val' => $data['query'] ?? "SELECT * WHERE {\n    ?s ?p ?o . \n} LIMIT 10"
            ]) ?>

            <?= $this->Form->button(__('Run'), ['type' => 'submit', 'class' => 'form-control btn cdli-btn-blue']); ?>
        <?= $this->Form->end(); ?>
    </div>
    <div class="col-lg-10 boxed">
        <h2><?= __('Output') ?></h2>

        <div id="output">
        </div>
    </div>
</div>

<?php $this->append('library'); ?>
<?= $this->Html->script('d3') ?>
<?php $this->end('library'); ?>

<?= $this->element('leaflet') ?>

<?php $this->append('script'); ?>
<?= $this->Html->scriptBlock('var prefixes=' . json_encode($prefixes)) ?>
<?= $this->Html->script('sparql') ?>
<?php $this->end('script'); ?>
