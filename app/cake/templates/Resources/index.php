<div class="text-left">
    <h1 class="display-4">CDLI Resources</h1>

    <div id="cdli-resources">
        <h2 class="p-2">Quick links</h2>
        <div class="row mt-3">
            <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3>
                        <?= $this->Html->link('CDLI Wiki', 'https://cdli.ox.ac.uk/wiki/') ?>
                        <span class="fa fa-external-link" aria-hidden="true"></span>
                    </h3>
                    <p class="resource-des">Encyclopedic articles related to Assyriological studies.</p>
                </div>
            </div>
            <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3><?= $this->Html->link('CDLI Documentation', ['controller' => 'Docs', 'action' => 'index']) ?></h3>
                    <p class="resource-des">List of main categories in documentation.</p>
                </div>
            </div>
            <div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3><?= $this->Html->link('CDLI Bibliography', ['controller' => 'Publications', 'action' => 'index']) ?></h3>
                    <p class="resource-des">Information about cuneiform artifacts have been published in numerous peer-reviewed books and articles. This CDLI feature provides search and browse capabilities into this precious resource that was compiled by a multitude of scholars over a three decades period.</p>
                </div>
            </div>
            <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
                <div class="d-flex flex-column card-body border">
                    <h3><?= $this->Html->link('Seals & seals impressions', ['controller' => 'Artifacts', 'action' => 'seals']) ?></h3>
                    <p class="resource-des">Seals, mostly cylindrical, engraved with scenes and text, were rolled onto fresh clay to leave their imprint, an ingenious administrative tool. </p>
                </div>
            </div>
            <!-- <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3><?= $this->Html->link('CDLI Tablet', ['controller' => 'CdliTablet', 'action' => 'index']) ?></h3>
                    <p class="resource-des">cdli tablet is an iPad and Android app that combines text and images of cuneiform inscriptions, of related archaeological artifacts, and of the equipment and techniques employed by specialists to capture, digitally preserve and disseminate the cultural heritage of the ancient Near East. </p>
                </div>
            </div>
            <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
                <div class="d-flex flex-column card-body border">
                    <h3><?= $this->Html->link('Cooked Agade', ['controller' => 'AgadeMails', 'action' => 'index']) ?></h3>
                    <p class="resource-des">Enjoy a filtered version of the Agade mailing list with manually curated entries centering around Mesopotamian studies only.</p>
                </div>
            </div> -->
        </div>
    </div>

    <div id="composite-texts">
        <h2>Composite Texts</h2>
        <div class="row mt-3">
            <div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3><?= $this->Html->link('Composite litrary texts & witnesses (scores)', ['controller' => 'Artifacts', 'action' => 'composites', 'Literary']) ?></h3>
                    <p class="resource-des">Numerous cuneiform artifacts were inscribed with witnesses of compositions. In order to study these compositions, scholars align witnesses so they can see the variations and similarities between exemplars. There are composites of various genres of texts but mostly Royal inscriptions, literary compositions and lexical texts.</p>
                </div>
            </div>
            <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3><?= $this->Html->link('Royal inscriptions and their witnesses & witnesses (scores)', ['controller' => 'Artifacts', 'action' => 'composites', 'Royal']) ?></h3>
                    <p class="resource-des">Powerful figures of Mesopotamia of all periods have disseminated their accomplishements using varied mediums. </p>
                </div>
            </div>

            <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3><?= $this->Html->link('Lexical texts and their witnesses (scores)', ['controller' => 'Artifacts', 'action' => 'composites', 'Lexical']) ?></h3>
                    <p class="resource-des"></p>
                </div>
            </div>
            <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3><?= $this->Html->link('Scientific texts and their witnesses (scores)', ['controller' => 'Artifacts', 'action' => 'composites', 'Scientific']) ?></h3>
                    <p class="resource-des"></p>
                </div>
            </div>
        </div>
    </div>

    <div id="references">
        <h2>References</h2>
        <div class="row mt-3">
            <div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3><?= $this->Html->link('Abbreviations for Assyriology', ['controller' => 'Abbreviations', 'action' => 'index']) ?></h3>
                    <p class="resource-des">List of abbreviations used in Mesopotamian Studies.</p>
                </div>
            </div>
            <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3><?= $this->Html->link('Preferred sign readings', ['controller' => 'SignReadings', 'action' => 'view', 'preferred']) ?></h3>
                    <p class="resource-des">Convention of sign values to use when submitting transliterations to the CDLI.</p>
                </div>
            </div>
            <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3>
                        <?= $this->Html->link('Sign lists', 'https://cdli.ox.ac.uk/wiki/sign_lists') ?>
                        <span class="fa fa-external-link" aria-hidden="true"></span>
                    </h3>
                    <p class="resource-des">Published cuneifom sign lists for all periods.</p>
                </div>
            </div>
            <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3><?= $this->Html->link(__('Generated token lists'), ['action' => 'tokenLists']) ?></h3>
                    <p class="resource-des">Sign and word lists for all periods, generated from CDLI data.</p>
                </div>
            </div>
            <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3>
                        <?= $this->Html->link('UR III months names', 'https://cdli-gh.github.io/ur3months/month.html') ?>
                        <span class="fa fa-external-link" aria-hidden="true"></span>
                    </h3>
                    <p class="resource-des">A correspondance table showing months vs month number, period, ruler and rulership year.</p>
                </div>
            </div>
            <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
                <div class="d-flex flex-column card-body border">
                    <h3>
                        <?= $this->Html->link('Years', 'https://cdli-gh.github.io/year-names/') ?>
                        <span class="fa fa-external-link" aria-hidden="true"></span>
                    </h3>
                    <p class="resource-des">A list of composite year names.</p>
                </div>
            </div>
        </div>
    </div>

    <div id="utilities-tools">
        <h2>Utilities and tools</h2>
        <div class="row mt-3">
            <div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3><?= $this->Html->link('Metadata checker', ['controller' => 'ArtifactsUpdates', 'action' => 'add']) ?></h3>
                    <p class="resource-des">Submit your catalogue data for validation.</p>
                </div>
            </div>
            <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3><?= $this->Html->link('Transliteration checker', ['controller' => 'Inscriptions', 'action' => 'add']) ?></h3>
                    <p class="resource-des">Submit your texts for validadion.</p>
                </div>
            </div>
            <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3><?= $this->Html->link('COCO converter', ['action' => 'cocoConverter']) ?></h3>
                    <p class="resource-des">Employ the COCO converter to convert annotations from COCO format to W3C.JSON format.</p>
                </div>
            </div>
            <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3><?= $this->Html->link('CDLI-CoNLL Checker', ['action' => 'checkCdliConll']) ?></h3>
                    <p class="resource-des">Validate your linguistic annotations.</p>
                </div>
            </div>
            <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3><?= $this->Html->link('Signfilter (Compare ATF against token lists)', ['action' => 'compareTextToTokenList']) ?></h3>
                    <p class="resource-des">Compare word and sign frequencies in an ATF file to the auto-generated
                    word and sign lists from a period.</p>
                </div>
            </div>
            <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3><?= $this->Html->link('Convert, annotate, or translate', ['action' => 'tools']) ?></h3>
                    <p class="resource-des">Convert your text or textual annotations to other formats and automatically annotate or translate your text(s). </p>
                </div>
            </div>
            <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3><?= $this->Html->link('conll-merge', ['action' => 'mergeConll']) ?></h3>
                    <p class="resource-des">Merge CoNLL files.</p>
                </div>
            </div>
            <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3><?= $this->Html->link('SPARQL client', ['action' => 'sparql']) ?></h3>
                    <p class="resource-des">Query the CDLI SPARQL endpoint and create visualizations for query results.</p>
                </div>
            </div>
            <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body border">
                    <h3>
                        <?= $this->Html->link('Standalone tools', 'https://github.com/cdli-gh') ?>
                        <span class="fa fa-external-link" aria-hidden="true"></span>
                    </h3>
                    <p class="resource-des">Navigate to our Gitlab repository where most of the tools offered as services on the cdli website can be installed on a local machine.</p>
                </div>
            </div>
        </div>
    </div>

    <div id="other-resources">
        <h2>Other Resources and Information</h2>
        <div class="row mt-3">
            <?php foreach ($postings as $posting): ?>
                <div class="border-0 card col-md-8  col-lg-4">
                    <div class="d-flex flex-column">
                        <p>
                            <?=$this->Html->link($posting->title, ['controller' => 'Postings', 'action' => 'view', $posting->id]) ?>
                        </p>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<?= $this->element('smoothscroll') ?>
