<?php
?>

<div class="row justify-content-md-center">
    <div class="col-lg-10 boxed">
        <h2><?= __('coco converter') ?></h2>

        <?= $this->Form->create(null, ['type' => 'file']) ?>
            <div class="form-group">
                <?= $this->Form->control('coco_file', [
                    'class' => 'form-control',
                    'label' => __('Upload file'),
                    'type' => 'file'
                ]) ?>
            </div>
            <div class="form-group">
                <?= $this->Form->submit(__('Submit'), ['class' => 'form-control btn cdli-btn-blue']) ?>
            </div>
        <?= $this->Form->end() ?>
        <?php /*
        <h2><?= __('VIA converter') ?></h2>

        <?= $this->Form->create(null, ['type' => 'file']) ?>
            <div class="form-group">
                <?= $this->Form->control('via_file', [
                    'class' => 'form-control',
                    'label' => __('Upload file'),
                    'type' => 'file'
                ]) ?>
            </div>
            <div class="form-group">
                <?= $this->Form->submit(__('Submit'), ['class' => 'form-control btn cdli-btn-blue']) ?>
            </div>
        <?= $this->Form->end() ?>
        */ ?>
    </div>

    <div class="col-lg-10 boxed">
        <div class="row justify-content-between">
            <h2 class="col-6 text-left"><?= __('Output') ?></h2>
            <?php if (isset($output)): ?>
                <button class=" maxw-5 pull-right copy-clipbaord-btn-bottom" onclick="CopyToClipboardBottom('output');return false;" id="copy-button-bottom">
                    <span class="copyBtn fa fa-thin fa-copy" aria-hidden="true"></span>
                </button>
            <?php endif; ?>
        </div>
        <?php if (isset($output)): ?>
            <pre id="output"><?= h($output) ?></pre>
        <?php else: ?>
            <p class="text-muted">
                <?= __('(none)') ?>
            </p>
        <?php endif; ?>
    </div>
</div>
<?= $this->Html->script('copyText') ?>
