<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ReviewAssignment[]|\Cake\Collection\CollectionInterface $reviewAssignments
 */
?>
<div class="reviewAssignments index content">
    <?= $this->Html->link(__('New Review Assignment'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Review Assignments') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('review_id') ?></th>
                    <th><?= $this->Paginator->sort('submission_id') ?></th>
                    <th><?= $this->Paginator->sort('reviewer_id') ?></th>
                    <th><?= $this->Paginator->sort('recommendation') ?></th>
                    <th><?= $this->Paginator->sort('date_assigned') ?></th>
                    <th><?= $this->Paginator->sort('date_notified') ?></th>
                    <th><?= $this->Paginator->sort('date_confirmed') ?></th>
                    <th><?= $this->Paginator->sort('date_completed') ?></th>
                    <th><?= $this->Paginator->sort('date_acknowledged') ?></th>
                    <th><?= $this->Paginator->sort('date_due') ?></th>
                    <th><?= $this->Paginator->sort('date_response_due') ?></th>
                    <th><?= $this->Paginator->sort('last_modified') ?></th>
                    <th><?= $this->Paginator->sort('reminder_was_automatic') ?></th>
                    <th><?= $this->Paginator->sort('declined') ?></th>
                    <th><?= $this->Paginator->sort('cancelled') ?></th>
                    <th><?= $this->Paginator->sort('reviewer_file_id') ?></th>
                    <th><?= $this->Paginator->sort('date_rated') ?></th>
                    <th><?= $this->Paginator->sort('date_reminded') ?></th>
                    <th><?= $this->Paginator->sort('quality') ?></th>
                    <th><?= $this->Paginator->sort('review_round_id') ?></th>
                    <th><?= $this->Paginator->sort('stage_id') ?></th>
                    <th><?= $this->Paginator->sort('review_method') ?></th>
                    <th><?= $this->Paginator->sort('round') ?></th>
                    <th><?= $this->Paginator->sort('step') ?></th>
                    <th><?= $this->Paginator->sort('review_form_id') ?></th>
                    <th><?= $this->Paginator->sort('unconsidered') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($reviewAssignments as $reviewAssignment): ?>
                <tr>
                    <td><?= $this->Number->format($reviewAssignment->review_id) ?></td>
                    <td><?= $reviewAssignment->has('submission') ? $this->Html->link($reviewAssignment->submission->submission_id, ['controller' => 'Submissions', 'action' => 'view', $reviewAssignment->submission->submission_id]) : '' ?></td>
                    <td><?= $this->Number->format($reviewAssignment->reviewer_id) ?></td>
                    <td><?= $this->Number->format($reviewAssignment->recommendation) ?></td>
                    <td><?= h($reviewAssignment->date_assigned) ?></td>
                    <td><?= h($reviewAssignment->date_notified) ?></td>
                    <td><?= h($reviewAssignment->date_confirmed) ?></td>
                    <td><?= h($reviewAssignment->date_completed) ?></td>
                    <td><?= h($reviewAssignment->date_acknowledged) ?></td>
                    <td><?= h($reviewAssignment->date_due) ?></td>
                    <td><?= h($reviewAssignment->date_response_due) ?></td>
                    <td><?= h($reviewAssignment->last_modified) ?></td>
                    <td><?= $this->Number->format($reviewAssignment->reminder_was_automatic) ?></td>
                    <td><?= $this->Number->format($reviewAssignment->declined) ?></td>
                    <td><?= $this->Number->format($reviewAssignment->cancelled) ?></td>
                    <td><?= $this->Number->format($reviewAssignment->reviewer_file_id) ?></td>
                    <td><?= h($reviewAssignment->date_rated) ?></td>
                    <td><?= h($reviewAssignment->date_reminded) ?></td>
                    <td><?= $this->Number->format($reviewAssignment->quality) ?></td>
                    <td><?= $reviewAssignment->has('review_round') ? $this->Html->link($reviewAssignment->review_round->review_round_id, ['controller' => 'ReviewRounds', 'action' => 'view', $reviewAssignment->review_round->review_round_id]) : '' ?></td>
                    <td><?= $this->Number->format($reviewAssignment->stage_id) ?></td>
                    <td><?= $this->Number->format($reviewAssignment->review_method) ?></td>
                    <td><?= $this->Number->format($reviewAssignment->round) ?></td>
                    <td><?= $this->Number->format($reviewAssignment->step) ?></td>
                    <td><?= $this->Number->format($reviewAssignment->review_form_id) ?></td>
                    <td><?= $this->Number->format($reviewAssignment->unconsidered) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $reviewAssignment->review_id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $reviewAssignment->review_id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $reviewAssignment->review_id], ['confirm' => __('Are you sure you want to delete # {0}?', $reviewAssignment->review_id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
