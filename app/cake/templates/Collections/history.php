<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Collection $collection
 */

$this->assign('title', h($collection->collection) . ' - ' . __('Collections'));
?>

<div class="row justify-content-md-center">
    <div class="col-lg-12 text-left">
        <h1><?= __('Metadata history') ?></h1>
        <h2><?= $this->Html->link($collection->collection, ['action' => 'view', $collection->id]) ?></h2>
    </div>

    <?php foreach ($updateEvents as $updateEvent): ?>
        <div class="col-lg-12 boxed">
            <?= $this->element('updateEventHeader', ['update_event' => $updateEvent]) ?>

            <?php if (!empty($updateEvent->event_comments)): ?>
                <p><?= h($updateEvent->event_comments) ?></p>
            <?php endif; ?>

            <?= $this->element('diff/otherEntityUpdate', ['entitiesUpdate' => $updateEvent->_matchingData['EntitiesUpdates']]) ?>
        </div>
    <?php endforeach; ?>
</div>
