<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Collection $collection
 */
?>

<div class="text-left">
    <h1>
        <?= __('Collection') ?>:
        <?= h($collection->collection) ?>
        <?php if ($collection->collection_is_private): ?>
            <span class="text-muted">(<?= __('private')?>)</span>
        <?php endif; ?>

        <?php if ($canEdit): ?>
            <?= $this->Html->link(
                __('Edit'),
                ['action' => 'edit', $collection->id],
                ['class' => 'btn cdli-btn-blue float-right ml-3']
            ) ?>
        <?php endif; ?>
        <?= $this->Html->link(
            __('History'),
            ['history', $collection->id],
            ['class' => 'btn btn-link float-right']
        ) ?>
    </h1>

    <hr>

    <div class="text-left">
        <?= $collection->description ?>
    </div>

    <div class="row">
        <div class="col-lg-6 col-12">
            <h2><?= __('Information') ?></h2>

            <table class="table-bootstrap">
                <tr>
                    <th><?= __('Link') ?></th>
                    <td>
                        <?php if ($collection->has('collection_url')): ?>
                            <?= $this->Html->link($collection->collection_url, $collection->collection_url, ['target' => '_blank']) ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php /*
                <?php if ($collection->has('collection_actor')): ?>
                    <tr>
                        <th><?= __('Actor') ?> (<?= __('Status') ?>)</th>
                        <td>
                            <?= h($collection->collection_actor) ?>
                            (<?= h($collection->collection_actor_status ?? '—') ?>)
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if ($collection->has('collection_holding')): ?>
                    <tr>
                        <th><?= __('Holding') ?> (<?= __('Status') ?>)</th>
                        <td>
                            <?= h($collection->collection_holding) ?>
                            (<?= h($collection->collection_holding_status ?? '—') ?>)
                        </td>
                    </tr>
                <?php endif; ?>
                */ ?>
                <tr>
                    <th><?= __('Related artifacts') ?></th>
                    <td>
                        <?= $this->Html->link(__('{0} artifacts', $count), [
                            'controller' => 'Search',
                            'action' => 'index',
                            '?' => ['collection' => $collection->collection]
                        ]) ?>
                    </td>
                </tr>
            </table>

            <h3 class="mt-3"><?= __('Names') ?></h3>
            <?= $this->element('entityNames', ['entity' => $collection]) ?>

            <h3 class="mt-3"><?= __('Linked data') ?></h3>
            <?= $this->element('entityExternalResources', ['entity' => $collection]) ?>

            <h3 class="mt-3" id="copyright"><?= __('Photo copyright') ?></h3>
            <p><?= $collection->license_comment ?></p>
            <p>Individual photos can be owned and licensed differently. Always verify the attributes of the digital asset you are interested in.</p>
            <table class="table-bootstrap">
                <tbody>
                    <tr>
                        <th><?= __('Owner') ?></th>
                        <td><?= $collection->license_attribution ?></td>
                    </tr>
                    <tr>
                        <th><?= __('License') ?></th>
                        <td>
                            <?php if ($collection->has('license_id')): ?>
                                <?= $this->ArtifactAssets->makeLicenseLink($collection->license_id) ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="col-lg-6 col-12">
            <h2><?= __('Location') ?></h2>

            <?php if ($collection->has('location_latitude_wgs1984')): ?>
                <div id="map" style="height: 500px; z-index: 0;"></div>

                <?= $this->element('leaflet') ?>
                <?php $this->append('script'); ?>
<script>
(function () {
    var collection = <?= json_encode($collection) ?>;

    var map = L.map('map')
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map)

    var coordinates = [
        collection.location_latitude_wgs1984,
        collection.location_longitude_wgs1984
    ]
    var link = '<a href="/collections/' + collection.id + '">' + collection.collection + '</a>'
    L.marker(coordinates).addTo(map).bindPopup(link)

    map.setView(coordinates, 5)
})()
</script>
                <?php $this->end(); ?>

                <p>
                    <?= __('Coordinates') ?>:
                    <?= h($collection->location_longitude_wgs1984) ?>,
                    <?= h($collection->location_latitude_wgs1984) ?>
                </p>
            <?php else: ?>
                <?= __('No precise location') ?>
            <?php endif; ?>

            <?php if ($collection->has('country_iso')): ?>
                <h3 class="mt-3"><?= __('Administrative regions') ?></h3>
                <table class="table-bootstrap">
                    <tr>
                        <th><?= __('Country') ?></th>
                        <td>
                            <?= h($collection->gadm_country->gadm_entity) ?>
                            <small class="text-muted"><?= h($collection->country_iso) ?></small>
                        </td>
                    </tr>
                    <tr>
                        <th><?= __('Region') ?></th>
                        <td>
                            <?php if ($collection->has('region_gadm')): ?>
                                <?= h($collection->gadm_region->gadm_entity) ?>
                                <small class="text-muted"><?= h($collection->region_gadm) ?></small>
                            <?php else: ?>
                                &mdash;
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <th><?= __('District') ?></th>
                        <td>
                            <?php if ($collection->has('district_gadm')): ?>
                                <?= h($collection->gadm_district->gadm_entity) ?>
                                <small class="text-muted"><?= h($collection->district_gadm) ?></small>
                            <?php else: ?>
                                &mdash;
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
