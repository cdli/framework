<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Collection $collection
 */
?>

<?= $this->element('changesetStatus', ['currentState' => 0]) ?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($collection) ?>
            <legend class="form-heading mb-3">
                <?php if ($collection->isNew()): ?>
                    <?= __('Add Collection') ?>
                <?php else: ?>
                    <?= __('Edit Collection {0}', h($collection->collection)) ?>
                <?php endif; ?>
            </legend>

            <?= $this->Form->control('collection', ['class' => 'form-control w-100 mb-3', 'required' => true]) ?>
            <?= $this->Form->control('slug', ['class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('collection_url', ['class' => 'form-control w-100 mb-3', 'type' => 'text']) ?>
            <?= $this->Form->control('is_pinned', ['class' => 'form-control w-100 mb-3', 'type' => 'toggle']) ?>

            <p><?= __('Names') ?></p>
            <div class="many-to-many">
                <div id="name-wrapper">
                    <?php foreach (array_merge(array_keys($collection->entities_names ?? []), ['template']) as $x): ?>
                        <div class="form-group border rounded pt-3 px-3<?= $x == 'template' ? ' d-none' : '' ?>">
                            <?= $this->Form->control("entities_names.$x.id") ?>
                            <div class="form-row">
                                <div class="col">
                                    <?= $this->Form->control("entities_names.$x.name", [
                                        'class' => 'form-control w-100 mb-3',
                                        'type' => 'text',
                                        'label' => __('Name'),
                                    ]) ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $this->Form->control("entities_names.$x.language_ietf", [
                                        'class' => 'form-control w-100 mb-3',
                                        'type' => 'text',
                                        'label' => __('Language (IETF tag)'),
                                    ]) ?>
                                </div>
                            </div>
                            <?= $this->Form->iconButton(__('Remove'), 'times-circle') ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?= $this->Form->iconButton(__('Add Name'), 'plus-circle', ['data-for' => 'name-wrapper']) ?>
            </div>

            <?= $this->Form->control('description', ['class' => 'form-control w-100 mb-3', 'type' => 'textarea']) ?>

            <h2 class="text-uppercase display-4 section-title mt-5 mb-4"><?= __('Classification') ?></h2>
            <?= $this->Form->control('collection_is_private', ['class' => 'form-control w-100 mb-3', 'type' => 'toggle']) ?>
            <div class="form-row">
                <div class="col form-group">
                    <?= $this->Form->control('collection_actor', [
                        'type' => 'hidden',
                        'class' => 'form-control w-100 mb-3',
                        'options' => [
                            'Agency' => 'Agency',
                            'Company' => 'Company',
                            'Educational institution' => 'Educational institution',
                            'Foundation' => 'Foundation',
                            'Person' => 'Person',
                            'Research project' => 'Research project',
                            'Uncertain' => 'Uncertain',
                            'Unknown' => 'Unknown',
                        ],
                    ]) ?>
                </div>
                <div class="col form-group">
                    <?= $this->Form->control('collection_actor_status', [
                        'type' => 'hidden',
                        'class' => 'form-control w-100 mb-3',
                        'options' => [
                            'Group' => 'Group',
                            'Individual' => 'Individual',
                            'Museum' => 'Museum',
                            'Public' => 'Public',
                            'Storage' => 'Storage',
                            'Uncertain' => 'Uncertain',
                            'Unknown' => 'Unknown',
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="form-row">
                <div class="col form-group">
                    <?= $this->Form->control('collection_holding', [
                        'type' => 'hidden',
                        'class' => 'form-control w-100 mb-3',
                        'options' => [
                            'Collection' => 'Collection',
                            'Library' => 'Library',
                            'Lot' => 'Lot',
                            'Museum' => 'Museum',
                            'Public' => 'Public',
                            'Storage' => 'Storage',
                            'Uncertain' => 'Uncertain',
                            'Unknown' => 'Unknown',
                        ],
                    ]) ?>
                </div>
                <div class="col form-group">
                    <?= $this->Form->control('collection_holding_status', [
                        'type' => 'hidden',
                        'class' => 'form-control w-100 mb-3',
                        'options' => [
                            'Extant' => 'Extant',
                            'Person' => 'Person',
                            'Terminated' => 'Terminated',
                            'Uncertain' => 'Uncertain',
                            'Unknown' => 'Unknown',
                        ],
                    ]) ?>
                </div>
            </div>

            <h2 class="text-uppercase display-4 section-title mt-5 mb-4"><?= __('Location') ?></h2>
            <div class="form-row">
                <div class="col form-group">
                    <?= $this->Form->control('location_longitude_wgs1984', [
                        'class' => 'form-control w-100 mb-3',
                        'label' => __('Longitude (WGS 1984)'),
                    ]) ?>
                </div>
                <div class="col form-group">
                    <?= $this->Form->control('location_latitude_wgs1984', [
                        'class' => 'form-control w-100 mb-3',
                        'label' => __('Latitude (WGS 1984)'),
                    ]) ?>
                </div>
            </div>
            <?= $this->Form->control('location_accuracy', [
                'class' => 'form-control w-100 mb-3',
                'label' => __('Coordinate accuracy (m)'),
            ]) ?>
            <?= $this->Form->control('country_iso', [
                'class' => 'form-control w-100 mb-3',
                'label' => __('Country code (ISO 3166-1 alpha-2)'),
            ]) ?>
            <?= $this->Form->control('region_gadm', [
                'class' => 'form-control w-100 mb-3',
                'label' => __('Region number (GID-1 GADM 3.6)'),
            ]) ?>
            <?= $this->Form->control('district_gadm', [
                'class' => 'form-control w-100 mb-3',
                'label' => __('District number (GID-2 GADM)'),
            ]) ?>

            <h2 class="text-uppercase display-4 section-title mt-5 mb-4"><?= __('License information') ?></h2>
            <?= $this->Form->control('license_id', [
                'class' => 'form-control w-100 mb-3',
                'label' => __('License identifier (SPDX)'),
                'type' => 'text',
            ]) ?>
            <?= $this->Form->control('license_attribution', [
                'class' => 'form-control w-100 mb-3',
                'label' => __('Copyright attribution'),
            ]) ?>
            <?= $this->Form->control('license_comment', [
                'class' => 'form-control w-100 mb-3',
                'label' => __('Licensing info'),
            ]) ?>

            <h2 class="text-uppercase display-4 section-title mt-5 mb-4"><?= __('Linked information') ?></h2>
            <p><?= __('External Resources') ?></p>
            <div class="many-to-many">
                <div id="external-resources-wrapper">
                    <?php foreach (array_merge(array_keys($collection->external_resources ?? []), ['template']) as $x): ?>
                        <div class="form-group border rounded pt-3 px-3<?= $x == 'template' ? ' d-none' : '' ?>">
                            <?= $this->Form->control("external_resources.$x.id", [
                                'class' => 'form-control w-100 mb-3',
                                'type' => 'select',
                                'options' => $externalResources,
                                'label' => __('External Resource'),
                            ]) ?>
                            <?= $this->Form->control("external_resources.$x._joinData.external_resource_key", [
                                'class' => 'form-control w-100 mb-3',
                                'type' => 'text',
                                'label' => __('External Resource Key'),
                                'empty' => true,
                            ]) ?>
                            <?= $this->Form->iconButton(__('Remove'), 'times-circle') ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?= $this->Form->iconButton(__('Add External Resource'), 'plus-circle', ['data-for' => 'external-resources-wrapper']) ?>
            </div>

            <?= $this->Form->button(__('Continue'), ['class'=>'btn btn-primary cdli-btn-blue mr-2']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<?php $this->append('library'); ?>
<script src="/assets/js/ckeditor/ckeditor.js"></script>
<?php $this->end(); ?>

<?php $this->append('script'); ?>
<?= $this->Html->script('form-control-many-to-many'); ?>
<script>
CKEDITOR.editorConfig = function( config ) {
    config.htmlEncodeOutput = true;
};
CKEDITOR.replace('description', {
    extraPlugins: 'autogrow',
    autoGrow_maxHeight: 800,
    removePlugins: 'resize'
});
</script>
<?php $this->end() ?>
