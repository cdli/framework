<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Collection[]|\Cake\Collection\CollectionInterface $collections
 * @var \App\Model\Entity\Collection[]|\Cake\Collection\CollectionInterface $pinned_collections
 * @var bool $canSubmitEdits
 */
?>

<div class="d-flex justify-content-between align-items-end mb-2">
    <h2 class="display-3 header-txt text-left"><?= __('Collections') ?></h2>
    <?php if ($canSubmitEdits): ?>
        <?= $this->Html->link('+ ' . __('Add Collection'), ['action' => 'add'], ['class' => 'btn cdli-btn-blue my-3']) ?>
    <?php endif; ?>
</div>

<div class="row mt-3">
    <?php foreach ($pinned_collections as $collection): ?>
        <div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
            <div class="d-flex flex-column card-body text-left border">
                <h5 class="mb-4">
                    <?= $this->Html->link($collection->collection, ['action' => 'view', $collection->id]) ?>
                </h5>
                <?= $this->Text->truncate(strip_tags($collection->description), 90) ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<?= $this->element('search_form') ?>

<?= $this->Html->link(__('ALL'), ['action' => 'view'], ['class' => 'btn btn-action']) ?>
<?php foreach (range('A', 'Z') as $char): ?>
    <?= $this->Html->link($char, ['action' => 'view', '?' => ['letter' => $char]], ['class' => 'btn btn-action']) ?>
<?php endforeach ?>

<div class="text-left my-2">
    <?= $this->element('entityExport'); ?>
</div>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead class="text-left">
        <tr>
            <th scope="col"><?= $this->Paginator->sort('collection') ?></th>
            <?php if ($canSubmitEdits): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($collections as $collection): ?>
            <tr class="text-left">
                <td>
                    <?= $this->Html->link(
                        $collection->collection,
                        ['controller' => 'Collections', 'action' => 'view', $collection->id]
                    ) ?>
                    <?php if ($collection->collection_is_private): ?>
                        (<?= __('private') ?>)
                    <?php endif;?>
                </td>

                <?php if ($canSubmitEdits): ?>
                    <td>
                        <?= $this->Html->link(
                            __('Edit'),
                            ['action' => 'edit', $collection->id],
                            ['class' => 'btn cdli-btn-light']
                        ) ?>
                    </td>
                <?php endif ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
