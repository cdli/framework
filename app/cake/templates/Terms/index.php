<div class="text-left">
    <section class="border-bottom mt-3">
        <h2 id="identifier">Identifiers</h2>
        <p>Various identifiers used throughout the CDLI database.</p>
        <h3>Artifacts</h3>
        <dl class="ml-3">
            <dt id="identifier_cdli">cdli</dt>
            <dd>
                CDLI numbers, formatted as <code>P######</code> (where <code>#</code>
                are numbers 0–9). Issued by the CDLI.
            </dd>

            <dt id="identifier_composite">composite</dt>
            <dd>
                Composite numbers, formatted as <code>Q######</code> (where
                <code>#</code> are numbers 0–9). Actual formatting might vary.
                Issued for composites by <a href="http://oracc.museum.upenn.edu/qcat/">QCat</a>.
            </dd>

            <dt id="identifier_seal">seal</dt>
            <dd>
                Seal numbers, formatted as <code>S######</code> (where <code>#</code>
                are numbers 0–9). Actual formatting might vary. Issued for seals.
            </dd>

            <dt id="identifier_designation">designation</dt>
            <dd>Designations, commonly a shorthand reference to a publication.</dd>

            <dt id="identifier_accession">accession</dt>
            <dd>Accession numbers.</dd>

            <dt id="identifier_museum">museum</dt>
            <dd>Museum numbers.</dd>
        </dl>
        <h3>Languages</h3>
        <dl class="ml-3">
            <dt id="identifier_iso-639">iso-639</dt>
            <dd>ISO 639-1:2002 language codes.</dd>

            <dt id="identifier_atf-protocol-code">atf-protocol-code</dt>
            <dd>
                Protocol codes for ancient languages as used in ATF.
                See <a href="https://oracc.museum.upenn.edu/doc/help/editinginatf/primer/inlinetutorial/index.html#h_languages">ORACC documentation</a>.
            </dd>

            <dt id="identifier_atf-inline-code">atf-inline-code</dt>
            <dd>
                Inline codes for ancient languages as used in ATF.
                See <a href="https://oracc.museum.upenn.edu/doc/help/editinginatf/primer/inlinetutorial/index.html#h_languages">ORACC documentation</a>.
            </dd>
        </dl>
        <h3>Other</h3>
        <dl class="ml-3">
            <dt id="identifier_findspot">findspot</dt>
            <dd>Findspot square.</dd>

            <dt id="identifier_excavation">excavation</dt>
            <dd>Excavation numbers.</dd>
        </dl>
    </section>

    <section class="border-bottom mt-3">
        <h2 id="type">Types</h2>
        <p>Various classifications used throughout the CDLI database.</p>
        <dl class="ml-3">
            <dt id="type_artifact_type">artifact_type</dt>
            <dd>Artifact Type.</dd>

            <dt id="type_genre">genre</dt>
            <dd>Genre.</dd>

            <dt id="type_aspect">aspect</dt>
            <dd>Material aspect.</dd>

            <dt id="type_color">color</dt>
            <dd>Material color.</dd>
        </dd>
    </section>
</div>
