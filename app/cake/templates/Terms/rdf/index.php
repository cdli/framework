<?xml version="1.0"?>
<rdf:RDF xmlns="http://www.w3.org/2002/07/owl#"
     xml:base="http://www.w3.org/2002/07/owl"
     xmlns:crm="http://www.cidoc-crm.org/cidoc-crm/"
     xmlns:owl="http://www.w3.org/2002/07/owl#"
     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
     xmlns:xml="http://www.w3.org/XML/1998/namespace"
     xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
     xmlns:cdli="https://cdli.network/entity/"
     xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">
    <Ontology/>



    <!--
    ///////////////////////////////////////////////////////////////////////////////////////
    //
    // Classes
    //
    ///////////////////////////////////////////////////////////////////////////////////////
     -->




    <!-- http://www.cidoc-crm.org/cidoc-crm/E42_Identifier -->

    <Class rdf:about="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>



    <!-- http://www.cidoc-crm.org/cidoc-crm/E55_Type -->

    <Class rdf:about="http://www.cidoc-crm.org/cidoc-crm/E55_Type"/>



    <!-- https://cdli.network/entity/identifier/accession -->

    <Class rdf:about="https://cdli.network/entity/identifier/accession">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:label xml:lang="en">Accession number</rdfs:label>
    </Class>



    <!-- https://cdli.network/entity/identifier/atf-protocol-code -->

    <Class rdf:about="https://cdli.network/entity/identifier/atf-protocol-code">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:comment xml:lang="en">Protocol codes for ancient languages as used in ATF.</rdfs:comment>
        <rdfs:label xml:lang="en">ATF protocol code</rdfs:label>
    </Class>



    <!-- https://cdli.network/entity/identifier/atf-inline-code -->

    <Class rdf:about="https://cdli.network/entity/identifier/atf-inline-code">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:comment xml:lang="en">Inline codes for ancient languages as used in ATF.</rdfs:comment>
        <rdfs:label xml:lang="en">ATF inline code</rdfs:label>
    </Class>



    <!-- https://cdli.network/entity/identifier/cdli -->

    <Class rdf:about="https://cdli.network/entity/identifier/cdli">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:comment xml:lang="en">CDLI numbers, formatted as P###### (where # are numbers 0–9). Issued for artifacts by the CDLI.</rdfs:comment>
        <rdfs:label xml:lang="en">CDLI number</rdfs:label>
    </Class>



    <!-- https://cdli.network/entity/identifier/composite -->

    <Class rdf:about="https://cdli.network/entity/identifier/composite">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:comment xml:lang="en">Composite numbers, formatted as Q###### (where # are numbers 0–9). Actual formatting might vary. Issued for composites by ORACC&apos;s QCat.</rdfs:comment>
        <rdfs:label xml:lang="en">Composite number</rdfs:label>
    </Class>



    <!-- https://cdli.network/entity/identifier/designation -->

    <Class rdf:about="https://cdli.network/entity/identifier/designation">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:comment xml:lang="en">Designations, commonly a shorthand reference to a publication.</rdfs:comment>
        <rdfs:label xml:lang="en">Designation</rdfs:label>
    </Class>



    <!-- https://cdli.network/entity/identifier/excavation -->

    <Class rdf:about="https://cdli.network/entity/identifier/excavation">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:label xml:lang="en">Excavation number</rdfs:label>
    </Class>



    <!-- https://cdli.network/entity/identifier/findspot -->

    <Class rdf:about="https://cdli.network/entity/identifier/findspot">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:label xml:lang="en">Findspot square</rdfs:label>
    </Class>



    <!-- https://cdli.network/entity/identifier/iso-639 -->

    <Class rdf:about="https://cdli.network/entity/identifier/iso-639">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:label xml:lang="en">ISO 639-1:2002 language code</rdfs:label>
    </Class>



    <!-- https://cdli.network/entity/identifier/museum -->

    <Class rdf:about="https://cdli.network/entity/identifier/museum">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:label xml:lang="en">Museum number</rdfs:label>
    </Class>



    <!-- https://cdli.network/entity/identifier/seal -->

    <Class rdf:about="https://cdli.network/entity/identifier/seal">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:comment xml:lang="en">Seal numbers, formatted as S###### (where # are numbers 0–9). Actual formatting might vary. Issued for seals.</rdfs:comment>
        <rdfs:label xml:lang="en">Seal number</rdfs:label>
    </Class>



    <!-- https://cdli.network/entity/type/artifact_type -->

    <Class rdf:about="https://cdli.network/entity/type/artifact_type">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E55_Type"/>
        <rdfs:label xml:lang="en">Artifact type</rdfs:label>
    </Class>



    <!-- https://cdli.network/entity/type/aspect -->

    <Class rdf:about="https://cdli.network/entity/type/aspect">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E55_Type"/>
        <rdfs:label xml:lang="en">Material aspect</rdfs:label>
    </Class>



    <!-- https://cdli.network/entity/type/color -->

    <Class rdf:about="https://cdli.network/entity/type/color">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E55_Type"/>
        <rdfs:label xml:lang="en">Material color</rdfs:label>
    </Class>



    <!-- https://cdli.network/entity/type/genre -->

    <Class rdf:about="https://cdli.network/entity/type/genre">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E55_Type"/>
        <rdfs:label xml:lang="en">Genre</rdfs:label>
    </Class>
</rdf:RDF>



<!-- Generated by the OWL API (version 4.5.13) https://github.com/owlcs/owlapi -->

