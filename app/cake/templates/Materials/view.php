<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Material $material
 */
?>
<?php use Cake\Utility\Inflector; ?>

<div>
    <div class="text-heading text-left"><?= __(h(Inflector::humanize(Inflector::singularize(Inflector::tableize($this->request->getParam('controller')))))) ?>: <?= h($material->material) ?>
      <?php if ($access_granted): ?>
          <?= $this->Html->link(
              __('Edit'),
              ['prefix' => 'Admin', 'action' => 'edit', $material->id],
              ['escape' => false , 'class' => "btn cdli-btn-blue pull-right", 'title' => 'Edit']
          ) ?>
      <?php endif; ?>
    </div>
</div>

<?php if (!empty($material->child_materials)): ?>
    <div class="single-entity-wrapper text-left mt-4">
        <div class="capital-heading"><?= __('Children Materials') ?></div>
            <ul>
                <?php foreach ($material->child_materials as $childMaterials): ?>
                    <li><?= $this->Html->link(
                            $childMaterials->material,
                            ['controller' => 'Materials', 'action' => 'view', $childMaterials->id]
                        ) ?></li>
                <?php endforeach; ?>
            </ul>
    </div>
<?php endif; ?>

<?php if ($material->has('parent_material')): ?>
    <div class="single-entity-wrapper text-left mt-4">
        <div class="capital-heading"><?= __('Parent Material') ?></div>
            <ul>
                  <li><?= $this->Html->link(
                        $material->parent_material->material,
                        ['controller' => 'Materials', 'action' => 'view', $material->parent_material->id]
                    ) ?></li>
            </ul>
    </div>
<?php endif; ?>

<div class="single-entity-wrapper text-left mt-2">
    <?php if ($count!=0): ?>
        <div class="capital-heading"><?= __('Related Artifacts') ?></div>
        <p>
          <?php echo 'There are '.$count.' artifacts related to '.h($material->material) ?><br>
            <?= $this->Html->link(__('Click here to view the artifacts'), [
                'controller' => 'Search',
                '?' => ['material' => $material->material]
            ]) ?>
        </p>
    <?php endif; ?>
</div>

<?= $this->element('citeBottom'); ?>
<?= $this->element('citeButton'); ?>