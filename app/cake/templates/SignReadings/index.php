<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SignReading[]|\Cake\Collection\CollectionInterface $signReadings
 */
?>
<div class="signReadings index content">
    <div class="d-flex justify-content-between align-items-end mb-2">
        <h1 class="display-3 header-txt text-left"><?= __('Sign Readings') ?></h1>
        <?= $this->element('addButton'); ?>
    </div>
    <div class="text-left my-2">
        <?= $this->element('entityExport'); ?>
    </div>
    <?= $this->element('search_form') ?>
    <div class="table-responsive table-hover">
        <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
            <thead align="left">
                <tr>
                    <th><?= $this->Paginator->sort('sign_reading') ?></th>
                    <th><?= $this->Paginator->sort('sign_name') ?></th>
                    <th><?= $this->Paginator->sort('meaning') ?></th>
                    <th><?= $this->Paginator->sort('preferred_reading') ?></th>
                    <th scope="col"><?= __('Action') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($signReadings as $signReading): ?>
                    <tr align="left">
                        <td>
                            <?= $this->Html->link(
                                $signReading->sign_reading,
                                ['controller' => 'SignReadings', 'action' => 'view', $signReading->id]
                            ) ?>
                        </td>
                        <td><?= h($signReading->sign_name) ?></td>
                        <td><?= h($signReading->meaning) ?></td>
                        <td><?=h($signReading->preferred_reading?'yes':'no')?></td>
                        <td>
                            <?= $this->Html->link(
                                __('View'),
                                ['action' => 'view', $signReading->id],
                                ['escape' => false , 'class' => 'btn btn-outline-primary btn-sm', 'title' => 'View']
                            ) ?>
                            <?php if ($access_granted): ?>
                                <?= $this->Html->link(
                                    __('Edit'),
                                    ['prefix' => 'Admin', 'action' => 'edit', $signReading->id],
                                    ['escape' => false , 'class' => 'btn btn-warning btn-sm', 'title' => 'Edit']
                                ) ?>
                            <?php endif ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php echo $this->element('Paginator'); ?>
</div>

<?= $this->element('citeButton'); ?>
<?= $this->element('citeBottom'); ?>
