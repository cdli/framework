<?php
/**
 * @var \App\View\AppView $this
 */
?>
<main class="row justify-content-md-center">

    <div class="col-lg-6 boxed bg-light rounded" >
        <h1 class="text-center text-danger text-uppercase"><?= __('Delete Account') ?></h1>

        <p class="fw-bold mt-5"><strong>Deleting your user account will permanently and irreversibly remove your account information and access.</p>
        <p class="fw-bold mb-4">Any contributions made by you are linked to the author entry and will be preserved.</p>

            <?= $this->Form->create() ?>
            <?= $this->Form->control(
                'confirm_delete',
                [
                    'type' => 'checkbox',
                    'required' => true,
                    'label' => __(' I understand, please delete my account'),
                ]
            ) ?>
            <div class="text-center">
                <?= $this->Form->button(
                    __('DELETE ACCOUNT'),
                    [
                        'class' => 'btn btn-danger',
                    ]
                ) ?>
            </div>
            <?php $this->Form->end() ?>
    </div>

</main>
