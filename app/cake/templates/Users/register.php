<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row justify-content-md-center">

    <div class="login-box">
        <div class="capital-heading text-center">
            <h1 class="display-4 header-txt">Register</h1>
        </div>
        <?= $this->Flash->render() ?>
<p>In order to complete this registration, you will need to use a 2FA application on your mobile device. We recommend Google Authenticator.</p>
<p>For detailed instructions, please visit the <a href="https://cdli-gh.github.io/guides/cdli_two_factor_guide.html"> "CDLI two factor guide"</a></p>
        <?= $this->Form->create($user, ['context' => ['validator' => 'register']]) ?>
            <?= $this->Form->control('first_name', [
                'class' => 'col-12 input-background-child-md',
                'title' => 'Only alphabets are allowed',
            ]) ?>
            <?php if (isset($errors['first_name'])):?>
                <?php foreach ($errors['first_name'] as $key => $value):?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <span class="fa fa-exclamation-circle mr-2"></span>
                    <?=$value?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <?php endforeach;?>
            <?php endif;?>

            <?= $this->Form->control('last_name', [
                'class' => 'col-12 input-background-child-md',
                'title' => 'Only alphabets are allowed',
            ]) ?>
            <?php if (isset($errors['last_name'])):?>
                <?php foreach ($errors['last_name'] as $key => $value):?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <span class="fa fa-exclamation-circle mr-2"></span>
                    <?=$value?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <?php endforeach;?>
            <?php endif;?>

            <?= $this->Form->control('email', ['class' => 'col-12 input-background-child-md',
            'required' => true]) ?>
            <?php if (isset($errors['email'])):?>
                <?php foreach ($errors['email'] as $key => $value):?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <span class="fa fa-exclamation-circle mr-2"></span>
                    <?=$value?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <?php endforeach;?>
            <?php endif;?>

            <?= $this->Form->control('username', ['class' => 'col-12 input-background-child-md',
            'pattern' => '^[^\t\n<>;"&\']+$',
            'title' => '<>;"&\' characters are not allowed',
            'required' => true] ) ?>
            <?php if (isset($errors['username'])):?>
                <?php foreach ($errors['username'] as $key => $value):?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <span class="fa fa-exclamation-circle mr-2"></span>
                    <?=$value?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <?php endforeach;?>
            <?php endif;?>

            <?= $this->Form->control('password', [
                'class' => 'col-12 input-background-child-md mb-0',
                'required' => true,
                'templates' => [
                    'inputContainer' => '<div class="input mb-1 {{type}}{{required}}">{{content}}{{help}}</div>',
                ],
                'templateVars' => [
                    'help' => '<small>' . __('Please use a passphrase of minimum 12 characters') . '</small>',
                ],
            ]) ?>
            <?php if (isset($errors['password'])):?>
                <?php foreach ($errors['password'] as $key => $value):?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <span class="fa fa-exclamation-circle mr-2"></span>
                    <?=$value?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <?php endforeach;?>
            <?php endif;?>

            <?= $this->Form->control('password_confirm', [
                'class' => 'col-12 input-background-child-md',
                'label'=>'Confirm Password ',
                'type'=>'password',
                'required' => true
            ]) ?>
            <?php if (isset($errors['password_confirm'])):?>
                <?php foreach ($errors['password_confirm'] as $key => $value):?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <span class="fa fa-exclamation-circle mr-2"></span>
                    <?=$value?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <?php endforeach;?>
            <?php endif;?>

            <div class="d-flex justify-content-evenly">
                <?= $this->Form->control('accept_privacy_policy', [
                    'type' => 'checkbox',
                    'label' => ' By creating an account, you accept that we store your email address and your name. You understand that we will link your account to a public author profile. This profile will display and credit you for any suggestions you provide CDLI regarding text, text annotations, images, images annotations, and metadata.',
                ]) ?>
            </div>

            <div class="userloginbuttoncenter">
                 <?= $this->Form->submit('Submit', ['class' => 'btn cdli-btn-blue']) ?>
            </div>

        <?= $this->Form->end() ?>
    </div>

</div>
