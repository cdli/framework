<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dynasty[]|\Cake\Collection\CollectionInterface $dynasties
 */

$this->assign('title', 'Documentation');
?>
<div class="container-fluid text-center contentWrapper">
    <main id="docs" class="container text-justify">
        <h1 class="display-3 header-txt">Documentation</h1>
        <span class="page-summary-text mt-4">This page indexes documentation regarding the usage of the site, it's features, data formats and preparation. Technical documentation for developers can be found in the <a href="https://gitlab.com/cdli/framework">framework repository</a>.</span>

        <div class="mt-5 mb-3">
            <h2 class="text-left display-4 section-title">Quick links</h2>
            <div class="text-left mt-3">
                <ul class="list-group">
                    <li class="list-group-item justify-content-between align-items-center">
                        <?php echo $this->Html->link(
                            'Search guide',
                            ['controller' => 'Docs', 'action' => 'view', 'search'],
                        ); ?>
                    </li>
                    <li class="list-group-item justify-content-between align-items-center"><?php echo $this->Html->link(
                        'Metadata / catalogue edition guide',
                        ['controller' => 'Docs', 'action' => 'view', 'add-and-edit-artifact-metadata'],
                    ); ?></li>
                    <li class="list-group-item justify-content-between align-items-center"><?php echo $this->Html->link(
                        'Text edition guide',
                        ['controller' => 'Docs', 'action' => 'view', 'editing-text'],
                    ); ?></li>
                    <li class="list-group-item justify-content-between align-items-center"><?php echo $this->Html->link(
                        'Images annotations guide',
                        ['controller' => 'Docs', 'action' => 'view', 'adding-and-editing-annotations'],
                    ); ?></li>
                    <li class="list-group-item justify-content-between align-items-center"><?php echo $this->Html->link(
                        'Linguistic annotations guide',
                        ['controller' => 'Docs', 'action' => 'view', 'linguistic-annotations'],
                    ); ?></li>

                </ul>
            </div>
        </div>

        <h2 class="text-left display-4 section-title">Documentation by audience</h2>
        <div class="row">
            <div class="card bs4-cdli-card col-sm-12 col-md-6 col-lg-6 mb-5">
                <div class="card-body">
                    <h5 class="card-title">Visitor</h5>
                    <p class="card-text">I am a student, a scholar, a curator, or an interested member of the public.</p>
                    <?= $this->Html->link(
                        'See the documentation for visitors',
                        ['controller' => 'Docs', 'action' => 'audience', 'visitor'],
                        ['class' => 'card-link']
                    ); ?>
                </div>
            </div>
            <div class="card bs4-cdli-card col-sm-12 col-md-6 col-lg-6 mb-5">
                <div class="card-body">
                    <h5 class="card-title">Content contributor</h5>
                    <p class="card-text">I am looking to contribute some metadata, text or textual annotations, or I would like to learn more on how these data are atructures and accessibl so I can reuse them.</p>
                    <?= $this->Html->link(
                        'See the documentation for content contributors',
                        ['controller' => 'Docs', 'action' => 'audience', 'content_contributor'],
                        ['class' => 'card-link']
                    ); ?>
                </div>
            </div>
            <div class="card bs4-cdli-card col-sm-12 col-md-6 col-lg-6 mb-5">
                <div class="card-body">
                    <h5 class="card-title">Organization</h5>
                    <p class="card-text">I work for a funding agency, I am part of the administration of an academic organization or I work for a media company.</p>
                    <?= $this->Html->link(
                        'See the documentation for organizations',
                        ['controller' => 'Docs', 'action' => 'audience', 'organization'],
                        ['class' => 'card-link']
                    ); ?>
                </div>
            </div>
            <div class="card bs4-cdli-card col-sm-12 col-md-6 col-lg-6 mb-5">
                <div class="card-body">
                    <h5 class="card-title">Developer</h5>
                    <p class="card-text">I am a researcher or a student in computer science or compotational linguistics, I am a web developer, or I work for a tech company.</p>
                    <?php echo $this->Html->link(
                        'See the documentation for developers',
                        ['controller' => 'Docs', 'action' => 'audience', 'developer'],
                        ['class' => 'card-link']
                    ); ?>
                </div>
            </div>
        </div>
        <h2 class="text-left display-4 section-title"> Documentation sections</h2>
        <?php
        foreach ($sections as $section) {
            echo '<h3>'.$section.'</h3>';
            echo '<ul class="list-unstyled">';
            foreach ($doc_heads as $doc_head) {
                if ($doc_head['section'] == $section) {
                    echo '<li>';
                    echo $this->Html->link(
                        $doc_head['title'],
                        ['controller' => 'Docs', 'action' => 'view', $doc_head['link_part']]
                    );
                    if (strlen($doc_head['category'])) {
                        echo ' ('.$doc_head['category'].')';
                    }
                    echo '</li>';
                }
            }
            echo '</ul>';
        }?>

        <?= $this->element('citeBottom'); ?>
        <?= $this->element('citeButton'); ?>
    </main>
</div>
