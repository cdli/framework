<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactAssetAnnotation $annotation
 */
?>

<?= $this->element('changesetStatus', ['currentState' => 0]) ?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($annotation) ?>
            <legend class="form-heading mb-3">
                <?= __('Edit Annotation') ?>
            </legend>

            <?= $this->Form->control('annotation_id', ['type' => 'hidden']) ?>

            <?= $this->Form->control('is_active', ['class' => 'form-control w-100 mb-3', 'type' => 'toggle']) ?>

            <p><?= __('Content') ?></p>
            <div class="many-to-many">
                <div id="bodies-wrapper">
                    <?php foreach (array_merge(array_keys($annotation->bodies ?? []), ['template']) as $x): ?>
                        <div class="form-group border rounded pt-3 px-3<?= $x == 'template' ? ' d-none' : '' ?>">
                            <div class="form-row">
                                <div class="col form-group">
                                    <?= $this->Form->control("bodies.$x.type", [
                                        'class' => 'form-control w-100 mb-3',
                                        'type' => 'select',
                                        'options' => ['SpecificResource' => 'vocabulary', 'TextualBody' => 'free text'],
                                        'label' => __('Type'),
                                        'required' => true,
                                    ]) ?>
                                </div>
                                <div class="col form-group">
                                    <?= $this->Form->control("bodies.$x.purpose", [
                                        'class' => 'form-control w-100 mb-3',
                                        'type' => 'select',
                                        'options' => [
                                            'assessing' => 'assessing',
                                            'classifying' => 'classifying',
                                            'commenting' => 'commenting',
                                            'describing' => 'describing',
                                            'identifying' => 'identifying',
                                            'tagging' => 'tagging',
                                        ],
                                        'label' => __('Purpose'),
                                        'required' => true,
                                    ]) ?>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col form-group">
                                    <?= $this->Form->control("bodies.$x.value", [
                                        'class' => 'form-control w-100 mb-3',
                                        'type' => 'text',
                                        'label' => __('Label'),
                                    ]) ?>
                                </div>
                                <div class="col form-group">
                                    <?= $this->Form->control("bodies.$x.source", [
                                        'class' => 'form-control w-100 mb-3',
                                        'type' => 'text',
                                        'label' => __('Vocabulary URL'),
                                    ]) ?>
                                </div>
                            </div>

                            <?= $this->Form->iconButton(__('Remove'), 'times-circle') ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?= $this->Form->iconButton(__('Add Content'), 'plus-circle', ['data-for' => 'bodies-wrapper']) ?>
            </div>

            <?= $this->Form->control('target_selector', ['class' => 'form-control w-100 mb-3', 'required' => true]) ?>
            <?= $this->Form->control('license', ['class' => 'form-control w-100 mb-3']) ?>

            <?= $this->Form->button(__('Save'), ['class'=>'btn btn-primary cdli-btn-blue mr-2']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<?php $this->append('script'); ?>
<?= $this->Html->script('form-control-many-to-many'); ?>
<?php $this->end() ?>
