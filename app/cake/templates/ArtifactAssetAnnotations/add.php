<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inscription $inscription
 */

$this->assign('title', __('Add Image Annotations'));

?>

<?= $this->element('changesetStatus', ['currentState' => 0]) ?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <h2><?= __('Add Image Annotations') ?></h2>
        <p>
            <?php if (!$canSubmitEdits): ?>
                You need to register an account to submit addition or edition
                suggestions to a CDLI editor for review. Please email cdli-support
                (at) ames.ox.ac.uk to activate your crowdsourcing privilege.
            <?php endif; ?>
        </p>

        <div class="cycle-tab">
            <div class="tab-content">
                <div class="tab-pane show active" id="anno" role="tabpanel" aria-labelledby="anno-tab">
                    <?= $this->Form->create(null, [
                        'type' => 'file',
                        'url' => ['controller' => 'ArtifactAssetAnnotations', 'action' => 'add'],
                        'class' => 'pt-2'
                    ]) ?>
                        <p>
                            This form is for submitting image annotations in JSON format.
                        </p>

                        <?= $this->Form->control('anno_file_type', [
                            'class' => 'form-control mb-3',
                            'label' => __('File type'),
                            'type' => 'select',
                            'options' => [
                                'coco' => 'Common Objects in Context (COCO)',
                                'via' => 'VGG Image Annotator (VIA)',
                                'wadm' => 'W3C/Web Annotation Data Model (WADM)',
                            ],
                            'value' => 'via',
                        ]) ?>

                        <?= $this->Form->control('anno_file', [
                            'class' => 'form-control mb-3',
                            'label' => __('Upload file'),
                            'type' => 'file'
                        ]) ?>

                        <span class="small text-muted">
                            This will deactive any currently visible annotations that are not included in the uploaded file.
                        </span>
                        <?= $this->Form->control('overwrite', [
                            'class' => 'form-control mb-3',
                            'label' => __('Overwrite existing annotation set'),
                            'type' => 'toggle'
                        ]) ?>

                        <?= $this->Form->submit('Proceed', ['class' => 'btn cdli-btn-blue']) ?>
                    <?= $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
