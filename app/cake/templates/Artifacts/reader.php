<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artifact $artifact
 * @var \App\Model\Entity\ArtifactAsset $asset
 */

$this->layout = 'simple';
$this->assign('title', __('{0} of {1}', $asset->getDescription(), $artifact->getCdliNumber()));
?>

<div class="d-md-flex text-left <?= $asset->asset_type === 'lineart' ? 'lineart' : '' ?>">
    <div id="reader-viewer-container" class="position-relative">
        <?= $this->cell('ArtifactAsset', [$asset->id]) ?>

        <button class="btn text-white bg-dark d-none d-md-block position-absolute" type="button" data-toggle="collapse" data-target="#reader-info" aria-expanded="false" aria-controls="reader-info">
            <span class="fa fa-chevron-right" aria-hidden="true"></span>
            <span class="sr-only"><?= __('Toggle sidebar') ?></span>
        </button>
    </div>

    <div class="collapse show text-light bg-dark p-3" id="reader-info">
        <h2 class="h4">
            <?= $this->Html->link(
                $artifact->designation . ' (' . $artifact->getCdliNumber() . ')',
                ['action' => 'view', $artifact->id],
                ['escapeTitle' => false, 'class' => 'text-white']
            ) ?>
        </h2>
        <p><?= $artifact->getDescription() ?></p>
        <p>
            <?= __('Image') ?>
            <?= $this->element('assetAttribution', [
                'asset' => $asset,
                'collection' => $artifact->collections[0] ?? null,
            ]) ?>
        </p>

        <?php if ($artifact->has('inscription')): ?>
            <h3>Transliteration</h3>
            <div class="border p-3" style="height: 100vh; overflow-y: auto;">
                <?= $artifact->inscription->getProcessedAtf() ?>
            </div>
        <?php endif; ?>
    </div>
</div>
