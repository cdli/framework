<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactAsset $asset
 * @var \App\Model\Entity\UpdateEvent[] $revisions
 * @var int $revision
 * @var bool $canSubmitEdits
 */

$this->layout = 'simple';
$this->assign('title', __('Annotations on {0} of {1}', $asset->getDescription(), $artifact->getCdliNumber()));
?>

<div class="d-md-flex text-left <?= $asset->asset_type === 'lineart' ? 'lineart' : '' ?>">
    <div id="reader-viewer-container" class="position-relative">
        <?= $this->cell('ArtifactAssetAnnotations', [$asset->id, $asset->annotations, $canSubmitEdits]) ?>

        <button class="btn text-white bg-dark d-none d-md-block position-absolute" type="button" data-toggle="collapse" data-target="#reader-info" aria-expanded="false" aria-controls="reader-info">
            <span class="fa fa-chevron-right" aria-hidden="true"></span>
            <span class="sr-only"><?= __('Toggle sidebar') ?></span>
        </button>
    </div>

    <div class="collapse show text-light bg-dark p-3" id="reader-info">
        <h2 class="h4">
            <?= $this->Html->link(
                $artifact->designation . ' (' . $artifact->getCdliNumber() . ')',
                ['action' => 'view', $artifact->id],
                ['escapeTitle' => false, 'class' => 'text-white']
            ) ?>
        </h2>
        <p><?= $artifact->getDescription() ?></p>
        <p>
            <?= __('Image') ?>
            <?= $this->element('assetAttribution', [
                'asset' => $asset,
                'collection' => $artifact->collections[0] ?? null,
            ]) ?>
        </p>

        <p><?= $this->Html->link(
            __('Hide annotations'),
            ['action' => 'reader', $artifact->id, $asset->id],
            ['class' => 'btn cdli-btn-light']
        ) ?></p>

        <div class="dropdown mb-3">
            <button class="btn cdli-btn-light dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                <?= __('Download') ?>
            </button>
            <div class="dropdown-menu">
                <?= $this->Html->link(
                    'W3C/Web Annotation Data Model (WADM)',
                    [$asset->artifact_id, $asset->id, '?' => $this->getRequest()->getQueryParams() + ['format' => 'wadm']],
                    ['class' => 'dropdown-item']
                ) ?>
                <?= $this->Html->link(
                    'VGG Image Annotator (VIA)',
                    [$asset->artifact_id, $asset->id, '?' => $this->getRequest()->getQueryParams() + ['format' => 'viajson']],
                    ['class' => 'dropdown-item']
                ) ?>
            </div>
        </div>

        <h3 class="h5"><?= __('Revisions') ?></h3>
        <form method="GET" style="overflow-y: auto;">
            <?= $this->Form->button(__('Show'), ['class' => 'btn cdli-btn-light mb-3']) ?>

            <div>
                <?php foreach ($revisions as $updateEvent): ?>
                    <div class="border bg-light text-dark p-2 mb-1 small d-flex">
                        <input type="radio" name="revision" class="mr-2" value="<?= $updateEvent->id ?>"
                               <?= $updateEvent->id === $revision ? 'checked' : '' ?> />
                        <div class="d-inline-block">
                            <?= $this->element('updateEventHeader', ['update_event' => $updateEvent]) ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </form>
    </div>
</div>
