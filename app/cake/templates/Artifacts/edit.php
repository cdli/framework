<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artifact $artifact
 */
$this->assign('title', $artifact->isNew() ? __('Add Artifact') : __('Edit Artifact {0}', $artifact->getCdliNumber()));
?>

<?= $this->element('changesetStatus', ['currentState' => 0]) ?>

<div class="row justify-content-md-center">
    <div class="col-lg-7 text-left form-wrapper">
        <?= $this->Form->create($artifact) ?>
            <legend class="form-heading mb-3"><?= $this->fetch('title') ?></legend>
            <hr class="form-hr mb-4"/>
            <?= $this->Form->button(__('Continue'), ['class' => 'btn cdli-btn-blue', 'id' => 'submit']) ?>

            <!-- CDLI DATA -->
            <h2 class="text-uppercase display-4 section-title mt-5 mb-4"><?= __('CDLI Data') ?></h2>
            <?= $this->Form->control('designation', ['class' => 'form-control w-100 mb-3', 'placeholder' => __('Hammurabi Code')]) ?>

            <?= $this->Form->control('composite_no', ['class' => 'form-control w-100 mb-3', 'placeholder' => 'Q000000', 'label' => __('Composite number of this artifact')]) ?>
            <?= $this->Form->control('seal_no', ['class' => 'form-control w-100 mb-3', 'placeholder' => 'S000000', 'label' => __('Seal number of this artifact')]) ?>

            <p><?= __('Composites') ?></p>
            <div class="many-to-many">
                <div id="composites-wrapper">
                    <?php foreach (array_merge(array_keys($artifact->composites ?? []), ['template']) as $x): ?>
                        <div class="form-group border rounded pt-3 px-3<?= $x == 'template' ? ' d-none' : '' ?>">
                            <?= $this->Form->control("composites.$x.composite_no", [
                                'class' => 'form-control w-100 mb-3',
                                'placeholder' => 'Q000000',
                                'label' => __('Witness to composite')
                            ]) ?>
                            <?= $this->Form->iconButton(__('Remove'), 'times-circle') ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?= $this->Form->iconButton(__('Add Associated Composite'), 'plus-circle', ['data-for' => 'composites-wrapper']) ?>
            </div>

            <p><?= __('Seals') ?></p>
            <div class="many-to-many">
                <div id="seals-wrapper">
                    <?php foreach (array_merge(array_keys($artifact->seals ?? []), ['template']) as $x): ?>
                        <div class="form-group border rounded pt-3 px-3<?= $x == 'template' ? ' d-none' : '' ?>">
                            <?= $this->Form->control("seals.$x.seal_no", [
                                'class' => 'form-control w-100 mb-3',
                                'placeholder' => 'S000000',
                                'label' => __('Impressed with seal')
                            ]) ?>
                            <?= $this->Form->iconButton(__('Remove'), 'times-circle') ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?= $this->Form->iconButton(__('Add Seal Impression'), 'plus-circle', ['data-for' => 'seals-wrapper']) ?>
            </div>
            <?= $this->Form->control('seal_information', ['class' => 'form-control w-100 mb-3', 'type' => 'textarea', 'label' => __('Information about seal impressions')]) ?>

            <?= $this->Form->control('cdli_comments', ['class' => 'form-control w-100 mb-3', 'placeholder' => __('CDLI Comments'), 'label' => __('Public Comments')]) ?>

            <?php if ($isAdmin): ?>
                <?= $this->Form->control('retired', ['type' => 'toggle', 'label' => __('Is this artifact retired?'), 'class' => 'float-right']) ?>
                <?= $this->Form->control('redirect_artifact_id', ['class' => 'form-control w-100 mb-3 retiredfields', 'type' => 'text', 'label' => __('Redirect Artifact ID')]) ?>
                <?= $this->Form->control('retired_comments', ['class' => 'form-control w-100 mb-3 retiredfields', 'type' => 'textarea', 'label' => __('Retired Comments')]) ?>
            <?php endif; ?>

            <!-- ADMINISTRATIVE DATA -->
            <?php if ($isAdmin): ?>
                <h2 class="text-uppercase display-4 section-title mt-5 mb-4"><?= __('Administrative Data') ?></h2>
                <?= $this->Form->control('is_public', ['type' => 'toggle', 'label' => __('Is this artifact public?'), 'class' => 'float-right']) ?>
                <?= $this->Form->control('is_atf_public', ['type' => 'toggle', 'label' => __('Is the inscription of this artifact public?'), 'class' => 'float-right']) ?>
                <?= $this->Form->control('are_images_public', ['type' => 'toggle', 'label' => __('Are images for this artifact public?'), 'class' => 'float-right']) ?>

                <?= $this->Form->control('artifacts_shadow.cdli_comments', ['class' => 'form-control w-100 mb-3', 'label' => __('Comments (Private)')]) ?>
                <?= $this->Form->control('artifacts_shadow.collection_location', ['class' => 'form-control w-100 mb-3', 'label' => __('Collection Location (Private)')]) ?>
                <?= $this->Form->control('artifacts_shadow.collection_comments', ['class' => 'form-control w-100 mb-3', 'label' => __('Collection Comments (Private)')]) ?>
                <?= $this->Form->control('artifacts_shadow.acquisition_history', ['class' => 'form-control w-100 mb-3', 'label' => __('Acquisition History (Private)')]) ?>
            <?php endif; ?>

            <!-- PHYSICAL INFORMATION -->
            <h2 class="text-uppercase display-4 section-title mt-5 mb-4"><?= __('Physical Information') ?></h2>
            <?= $this->Form->control('artifact_type_id', [
                'class' => 'form-control w-100 mb-3 custom-form-select',
                'options' => $artifactTypes,
                'empty' => true
            ]) ?>
            <?= $this->Form->control('is_artifact_type_uncertain', [
                'type' => 'toggle',
                'label' => __('Is this artifact type uncertain?')
            ]) ?>
            <?= $this->Form->control('artifact_type_comments', ['class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('artifact_comments', ['class' => 'form-control w-100 mb-3']) ?>
            <div class="form-group row">
                <div class="col">
                    <?= $this->Form->control('height', ['class' => 'form-control', 'placeholder' => '12.3', 'label' => __('Height (mm)')]) ?>
                </div>
                <div class="col">
                    <?= $this->Form->control('width', ['class' => 'form-control', 'placeholder' => '12.3', 'label' => __('Width (mm)')]) ?>
                </div>
                <div class="col">
                    <?= $this->Form->control('thickness', ['class' => 'form-control', 'placeholder' => '1.2', 'label' => __('Thickness (mm)')]) ?>
                </div>
            </div>
            <?= $this->Form->control('weight', ['class' => 'form-control w-25 mb-3', 'placeholder' => '12.3', 'label' => __('Weight (grams)')]) ?>

            <p><?= __('Materials') ?></p>
            <div class="many-to-many">
                <div id="material-wrapper">
                    <?php foreach (array_merge(array_keys($artifact->materials ?? []), ['template']) as $x): ?>
                        <div class="form-group border rounded pt-3 px-3<?= $x == 'template' ? ' d-none' : '' ?>">
                            <?= $this->Form->control("materials.$x.id", [
                                'class' => 'form-control w-100 mb-3 custom-form-select',
                                'type' => 'select',
                                'options' => $materials,
                                'label' => __('Material')
                            ]) ?>
                            <?= $this->Form->control("materials.$x._joinData.material_color_id", [
                                'class' => 'form-control w-100 mb-3 custom-form-select',
                                'type' => 'select',
                                'options' => $materialColors,
                                'empty' => true
                            ]) ?>
                            <?= $this->Form->control("materials.$x._joinData.material_aspect_id", [
                                'class' => 'form-control w-100 mb-3 custom-form-select',
                                'type' => 'select',
                                'options' => $materialAspects,
                                'empty' => true
                            ]) ?>
                            <?= $this->Form->control("materials.$x._joinData.is_material_uncertain", [
                                'type' => 'toggle',
                                'label' => __('Is this material uncertain?')
                            ]) ?>
                            <?= $this->Form->iconButton(__('Remove'), 'times-circle') ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?= $this->Form->iconButton(__('Add Material'), 'plus-circle', ['data-for' => 'material-wrapper']) ?>
            </div>

            <?= $this->Form->control('has_fragments', ['type' => 'toggle', 'label' => __('Does this artifact have fragments?')]) ?>
            <?php if ($artifact->isNew()): ?>
                <?= $this->Form->control('artifact_preservation', [
                    'class' => 'form-control w-100 mb-3',
                    'options' => ['fragment' => 'fragment', '25%' => '25%', '50%' => '50%', '75%' => '75%', 'complete' => 'complete']
                ]) ?>
                <?= $this->Form->control('surface_preservation', [
                    'class' => 'form-control w-100 mb-3',
                    'options' => ['Excellent' => 'Excellent', 'Good' => 'Good', 'Fair' => 'Fair', 'Poor' => 'Poor']
                ]) ?>
            <?php else: ?>
                <?= $this->Form->control('artifact_preservation', [
                    'class' => 'form-control w-100 mb-3',
                    'type' => 'text'
                ]) ?>
                <?= $this->Form->control('surface_preservation', [
                    'class' => 'form-control w-100 mb-3',
                    'type' => 'text'
                ]) ?>
            <?php endif; ?>
            <?= $this->Form->control('condition_description', ['class' => 'form-control w-100 mb-3', 'type' => 'textarea']) ?>

            <!-- PROVENIENCE -->
            <h2 class="text-uppercase display-4 section-title mt-5 mb-4"><?= __('Provenience') ?></h2>
            <?= $this->Form->control('provenience_id', ['options' => $proveniences, 'empty' => true, 'class' => 'form-control w-100 mb-3 custom-form-select']) ?>
            <?= $this->Form->control('is_provenience_uncertain', ['type' => 'toggle', 'label' => __('Is provenience uncertain?')]) ?>
            <?= $this->Form->control('provenience_comments', ['class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('excavation_no', ['class' => 'form-control w-100 mb-3', 'label' => __('Excavation number')]) ?>
            <?= $this->Form->control('findspot_square', ['class' => 'form-control w-100 mb-3', 'type' => 'text']) ?>
            <?= $this->Form->control('findspot_comments', ['class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('stratigraphic_level', ['class' => 'form-control w-100 mb-3', 'type' => 'text']) ?>
            <?= $this->Form->control('elevation', ['class' => 'form-control w-100 mb-3']) ?>

            <!-- ORIGIN -->
            <h2 class="text-uppercase display-4 section-title mt-5 mb-4"><?= __('Origin') ?></h2>
            <?= $this->Form->control('period_id', ['class' => 'form-control w-100 mb-3 custom-form-select', 'options' => $periods, 'empty' => true]) ?>
            <?= $this->Form->control('is_period_uncertain', ['type' => 'toggle', 'label' => __('Is this period uncertain?')]) ?>
            <?= $this->Form->control('period_comments', ['class' => 'form-control w-100 mb-3']) ?>
            <?= $this->Form->control('written_in', ['class' => 'form-control w-100 mb-3 custom-form-select', 'empty' => true, 'options' => $proveniences]) ?>
            <?= $this->Form->control('archive_id', ['class' => 'form-control w-100 mb-3 custom-form-select', 'empty' => true, 'options' => $archives]) ?>
            <?= $this->Form->control('is_artifact_fake', ['type' => 'toggle', 'label' => __('Is this artifact fake?')]) ?>

            <!-- COLLECTION INFORMATION -->
            <h2 class="text-uppercase display-4 section-title mt-5 mb-4"><?= __('Collection Information') ?></h2>
            <p><?= __('Collections') ?></p>
            <div class="many-to-many">
                <div id="collection-wrapper">
                    <?php foreach (array_merge(array_keys($artifact->collections ?? []), ['template']) as $x): ?>
                        <div class="form-group border rounded pt-3 px-3<?= $x == 'template' ? ' d-none' : '' ?>">
                            <?= $this->Form->control("collections.$x.id", [
                                'class' => 'form-control w-100 mb-3 custom-form-select',
                                'type' => 'select',
                                'options' => $collections,
                                'label' => __('Collection')
                            ]) ?>
                            <?= $this->Form->iconButton(__('Remove'), 'times-circle') ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?= $this->Form->iconButton('Add Collection', 'plus-circle', ['data-for' => 'collection-wrapper']) ?>
            </div>
            <?= $this->Form->control('museum_no', ['class' => 'form-control w-100 mb-3', 'type' => 'text', 'label' => __('Museum number')]) ?>
            <?= $this->Form->control('accession_no', ['class' => 'form-control w-100 mb-3', 'type' => 'text', 'label' => __('Accession number')]) ?>

            <!-- TEXT CONTENT -->
            <h2 class="text-uppercase display-4 section-title mt-5 mb-4"><?= __('Text Content') ?></h2>
            <p><?= __('Languages') ?></p>
            <div class="many-to-many">
                <div id="language-wrapper">
                    <?php foreach (array_merge(array_keys($artifact->languages ?? []), ['template']) as $x): ?>
                        <div class="form-group border rounded pt-3 px-3<?= $x == 'template' ? ' d-none' : '' ?>">
                            <?= $this->Form->control("languages.$x.id", [
                                'class' => 'form-control w-100 mb-3 custom-form-select',
                                'options' => $languages,
                                'type' => 'select',
                                'label' => __('Language')
                            ]) ?>
                            <?= $this->Form->control("languages.$x._joinData.is_language_uncertain", [
                                'type' => 'toggle',
                                'class' => 'float-right',
                                'label' => __('Is this language uncertain?')
                            ]) ?>
                            <?= $this->Form->iconButton(__('Remove'), 'times-circle') ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?= $this->Form->iconButton(__('Add Language'), 'plus-circle', ['data-for' => 'language-wrapper']) ?>
            </div>

            <p><?= __('Genre') ?></p>
            <div class="many-to-many">
                <div id="genre-wrapper">
                    <?php foreach (array_merge(array_keys($artifact->genres ?? []), ['template']) as $x): ?>
                        <div class="form-group border rounded pt-3 px-3<?= $x == 'template' ? ' d-none' : '' ?>">
                            <?= $this->Form->control("genres.$x.id", [
                                'options' => $genres,
                                'class' => 'form-control w-100 mb-3 custom-form-select',
                                'type' => 'select',
                                'label' => __('Genre')
                            ]) ?>
                            <?= $this->Form->control("genres.$x._joinData.is_genre_uncertain", [
                                'type' => 'toggle',
                                'label' => __('Is this genre uncertain?')
                            ]) ?>
                            <?= $this->Form->control("genres.$x._joinData.comments", ['class' => 'form-control w-100 mb-4']) ?>
                            <?= $this->Form->iconButton(__('Remove'), 'times-circle') ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?= $this->Form->iconButton(__('Add Genre'), 'plus-circle', ['data-for' => 'genre-wrapper']) ?>
            </div>

            <?= $this->Form->control('dates_referenced', ['class' => 'form-control w-100 mb-3', 'placeholder' => 'Ibbi-Suen.02.04.00; Šulgi.46.02.29 (us2 year)', 'label' => __('Date(s)')]) ?>
            <?= $this->Form->control('alternative_years', ['class' => 'form-control w-100 mb-3', 'type' => 'text']) ?>
            <?= $this->Form->control('is_school_text', ['type' => 'toggle', 'label' => __('Is this a school text?')]) ?>

            <!-- IDENTIFIERS -->
            <h2 class="text-uppercase display-4 section-title mt-5 mb-4" id="identifiers"><?= __('Identifiers') ?></h2>

            <p><?= __('External Resources') ?></p>
            <div class="many-to-many">
                <div id="external-resources-wrapper">
                    <?php foreach (array_merge(array_keys($artifact->artifacts_external_resources ?? []), ['template']) as $x): ?>
                        <div class="form-group border rounded pt-3 px-3<?= $x == 'template' ? ' d-none' : '' ?>">
                            <div class="row">
                                <div class="col">
                                    <?= $this->Form->control("artifacts_external_resources.$x.external_resource_id", [
                                        'class' => 'form-control w-100 mb-3',
                                        'type' => 'select',
                                        'options' => $externalResources,
                                        'label' => __('External Resource'),
                                        'empty' => true,
                                    ]) ?>
                                </div>
                                <div class="col">
                                    <?= $this->Form->control("artifacts_external_resources.$x.external_resource_key", [
                                        'class' => 'form-control w-100 mb-3',
                                        'type' => 'text',
                                        'label' => __('External Resource Key'),
                                        'empty' => true,
                                    ]) ?>
                                </div>
                            </div>
                            <?= $this->Form->iconButton(__('Remove'), 'times-circle') ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?= $this->Form->iconButton(__('Add External Resource'), 'plus-circle', ['data-for' => 'external-resources-wrapper']) ?>
            </div>

            <!-- PUBLICATIONS -->
            <h2 class="text-uppercase display-4 section-title mt-5 mb-4" id="publications"><?= __('Publications') ?></h2>
            <?= $this->element('formManyToMany/publications', ['publications' => $artifact->publications]) ?>

            <div class="pt-2">
                <?= $this->Form->button(__('Continue'), ['class' => 'btn btn-primary cdli-btn-blue mr-2', 'id' => 'submit']) ?>
            </div>
        <?= $this->Form->end() ?>
    </div>
</div>

<?php $this->append('script'); ?>
<?= $this->Html->script('form-control-entity-picker'); ?>
<?= $this->Html->script('form-control-many-to-many'); ?>
<script type="text/javascript">
    if (!$('#retired').is(':checked')) {
        $('.retiredfields').attr('disabled', 'disabled')
    }

    $('#retired').click(function () {
        if ($('#retired').is(':checked')) {
            $('.retiredfields').removeAttr('disabled')
        } else {
            $('.retiredfields').attr('disabled', 'disabled')
        }
    })

    $(function () {
        var submitted = false

        $('form').on('submit', function () {
            submitted = true
        })

        $(window).on('beforeunload', function () {
            if (!submitted) {
                return ''
            }
        })
    })
</script>
<?php $this->end() ?>
