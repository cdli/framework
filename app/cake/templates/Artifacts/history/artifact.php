<?php
/**
 * @var \App\View\AppView $this
 */

$artifactLabel = $artifact['designation'] . ' (' . $artifact->getCdliNumber() . ')';
$this->assign('title', __('Metadata history') . ' - ' . $artifactLabel);
?>

<div class="row justify-content-md-center">
    <div class="col-lg-12 text-left">
        <h1><?= __('Metadata history') ?></h1>
        <h2><?= $this->Html->link($artifactLabel, ['action' => 'view', $artifact->id] ) ?></h2>
    </div>

    <?php foreach ($updates as $update): ?>
        <div class="col-lg-12 boxed">
            <?= $this->element('updateEventHeader', ['update_event' => $update[1]->update_event]) ?>

            <?php if (!empty($update[1]->update_event->event_comments)): ?>
                <p><?= h($update[1]->update_event->event_comments) ?></p>
            <?php endif; ?>

            <?= $this->element('artifactUpdateDiff', ['new' => $update[1], 'old' => $update[0]]) ?>
        </div>
    <?php endforeach; ?>
</div>
