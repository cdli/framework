<main class="text-left">
    <h2><?= __('CDLI Tablet') ?></h2>
    <?= $this->element('search_form') ?>
    <div class="row">
        <?php foreach ($results as $result): ?>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="card mb-3">
                    <img src="/pubs/daily_tablets/<?= $result->image_filename ?>" class="card-img-top" style="height: auto;">
                    <div class="card-body">
                        <h3 class="card-title">
                            <?= $this->Html->link(
                                $result->theme . ': ' . $result->title_short,
                                ['action' => 'view', $result->id]
                            ) ?>
                        </h3>
                        <p class="card-subtitle text-muted font-italic mb-3">
                            Created by <?= $this->Html->link(h($result->author->author), [
                                'controller' => 'Authors',
                                'action' => 'view',
                                $result->author_id
                            ]) ?>
                            on
                            <span class="text-nowrap"><?= $result->date_display ?></span>
                        </p>

                        <p class="card-text">
                            <?= strip_tags($result->text_short, '<a><abbr><b><br><i><center><div><em><p><q>') ?>
                        </p>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <?= $this->element('Paginator') ?>
</main>

<?= $this->element('smoothscroll') ?>

<?= $this->element('citeButton') ?>
<?= $this->element('citeBottom') ?>
