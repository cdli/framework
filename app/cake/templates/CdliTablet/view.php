<main class="text-left">
    <h2>CDLI tablet</h2>

    <div class="card">
        <div class="row no-gutters">
            <div class="col-md-6 text-center" style="background-color: black;">
                <img src="/pubs/daily_tablets/<?= $entry->image_filename ?>" style="max-width: 100%; height: auto;">
            </div>

            <div class="col-md-6">
                <div class="card-body">
                    <h3 class="card-title">
                        <?= $entry->theme . ': ' . $entry->title_short . ' (' . $entry->date_display . ')' ?>
                    </h3>
                    <p class="card-subtitle text-muted font-italic mb-3">
                        Created by: <?= $this->Html->link(h($entry->author->author), [
                            'controller' => 'Authors',
                            'action' => 'view',
                            $entry->author_id
                        ]) ?>
                    </p>

                    <p class="card-text font-weight-bold">
                        <?= strip_tags($entry->text_short, '<a><abbr><b><br><i><center><div><em><p><q>') ?>
                    </p>

                    <p class="card-text">
                        <?= strip_tags($entry->text_long, '<a><abbr><b><br><i><center><div><em><p><q>') ?>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <?= $this->element('citeButton') ?>
    <?= $this->element('citeBottom') ?>
</main>
