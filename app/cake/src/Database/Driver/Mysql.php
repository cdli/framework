<?php
declare(strict_types=1);

namespace App\Database\Driver;

use App\Database\FixedQueryCompiler;
use Cake\Database\Driver\Mysql as BrokenMysql;
use Cake\Database\QueryCompiler;

class Mysql extends BrokenMysql
{
    public function newCompiler(): QueryCompiler
    {
        return new FixedQueryCompiler();
    }
}
