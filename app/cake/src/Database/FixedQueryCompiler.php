<?php
declare(strict_types=1);

namespace App\Database;

use Cake\Database\QueryCompiler;

class FixedQueryCompiler extends QueryCompiler
{
    protected $_orderedUnion = false;
}
