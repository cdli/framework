<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * Date Entity
 *
 * @property int $id
 * @property string|null $day_no
 * @property string|null $day_remarks
 * @property int|null $month_id
 * @property bool|null $is_uncertain
 * @property string|null $month_no
 * @property int|null $year_id
 * @property int|null $dynasty_id
 * @property int|null $ruler_id
 * @property string|null $absolute_year
 *
 * @property \App\Model\Entity\Month $month
 * @property \App\Model\Entity\Year $year
 * @property \App\Model\Entity\Dynasty $dynasty
 * @property \App\Model\Entity\Ruler $ruler
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class Date extends Entity implements LinkedDataInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'day_no' => true,
        'day_remarks' => true,
        'month_id' => true,
        'is_uncertain' => true,
        'month_no' => true,
        'year_id' => true,
        'dynasty_id' => true,
        'ruler_id' => true,
        'absolute_year' => true,
        'month' => true,
        'year' => true,
        'dynasty' => true,
        'ruler' => true,
        'artifacts' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'day_no',
        'day_remarks',
        'month_id',
        'is_uncertain',
        'month_no',
        'year_id',
        'dynasty_id',
        'ruler_id',
        'absolute_year',
        'month',
        'year',
        'dynasty',
        'ruler',
    ];

    private function getDate(): string
    {
        $parts = [
            empty($this->ruler) ? null : $this->ruler->ruler,
            empty($this->year) ? null : $this->year->year_no,
            $this->month_no,
            $this->day_no,
        ];

        $parts = array_map(function ($part) {
            return empty($part) || $part === '00' ? '--' : $part;
        }, $parts);

        return implode('.', $parts);
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "dates/$id";
    }

    public function getLinkedData(): array
    {
        $entity = [
            'rdf:type' => LinkedData::newResource('crm:E52_Time-Span'),
            'crm:P170i_time_is_defined_by' => $this->getDate(),
            // TODO:
            //   - is_uncertain
            //   - month_id, year_id
            //   - absolute_year
        ];

        if ($this->has('day_remarks') && !empty($this->day_remarks)) {
            $entity['crm:P3_has_note'] = $this->day_remarks;
        }

        if ($this->has('ruler_id')) {
            $entity['crm:P86_falls_within'] = LinkedData::newResource(Ruler::makeUri($this->ruler_id) . '#rule/time-span');
        } elseif ($this->has('dynasty_id')) {
            $entity['crm:P86_falls_within'] = LinkedData::newResource(Dynasty::makeUri($this->dynasty_id) . '#time-span');
        }

        return $entity;
    }
}
