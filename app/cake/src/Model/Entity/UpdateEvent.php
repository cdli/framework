<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;

/**
 * UpdateEvent Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property int|null $created_by
 * @property int|null $external_resource_id
 * @property array<string> $update_type
 * @property string|null $event_comments
 * @property \Cake\I18n\FrozenTime $approved
 * @property int|null $approved_by
 * @property string $status
 *
 * @property \App\Model\Entity\Author $creator
 * @property \App\Model\Entity\Author $reviewer
 * @property \App\Model\Entity\Author[] $authors
 * @property \App\Model\Entity\ArtifactsUpdate[] $artifacts_updates
 * @property \App\Model\Entity\ArtifactAssetAnnotation[] $artifact_asset_annotations
 * @property \App\Model\Entity\EntitiesUpdate[] $entities_updates
 * @property \App\Model\Entity\Inscription[] $inscriptions
 */
class UpdateEvent extends Entity
{
    use LocatorAwareTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'external_resource_id' => true,
        'update_type' => true,
        'event_comments' => true,
        'authors' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'external_resource_id',
        'update_type',
        'event_comments',
        'created_by',
        'approved_by',
        'status',
        'creator',
        'reviewer',
        'authors',
    ];

    /**
     * @var bool
     */
    public bool $missingPrivateChanges = false;

    public function apply()
    {
        if (count($this->artifacts_updates) > 0) {
            $this->getTableLocator()->get('UpdateEvents')->loadInto($this, ['ArtifactsUpdates.Artifacts']);
            $Artifacts = $this->getTableLocator()->get('Artifacts');
            $ArtifactsShadow = $this->getTableLocator()->get('ArtifactsShadow');
            $ArtifactsUpdates = $this->getTableLocator()->get('ArtifactsUpdates');

            foreach ($this->artifacts_updates as $artifacts_update) {
                $artifact = $artifacts_update->artifact;
                if (empty($artifact)) {
                    $artifacts_update->artifact = $artifact = $Artifacts->newEmptyEntity();
                }

                $success = $artifacts_update->apply($artifact);
                if ($artifact->has('artifacts_shadow')) {
                    $success = $success && $ArtifactsShadow->save($artifact->artifacts_shadow);
                }
                $success = $success && $Artifacts->save($artifact);
                $success = $success && $ArtifactsUpdates->save($artifacts_update);

                if (!$success) {
                    $errors = array_filter($artifacts_update->getErrors(), function ($errors) {
                        return is_array($errors) && count($errors) > 0;
                    });
                    $message = __(
                        'Failed to update artifact P{0}. Reason(s): {1}',
                        str_pad((string)$artifacts_update->artifact_id, 6, '0', STR_PAD_LEFT),
                        implode('; ', array_map(function ($field, $error) {
                            return $field . ': ' . implode(', ', $error);
                        }, array_keys($errors), array_values($errors)))
                    );
                    $this->setError('artifacts_updates', $message, false);
                }
            }
        }

        if (count($this->artifact_asset_annotations) > 0) {
            $this->getTableLocator()->get('UpdateEvents')->loadInto($this, ['ArtifactAssetAnnotations']);
            $Annotations = $this->getTableLocator()->get('ArtifactAssetAnnotations');

            foreach ($this->artifact_asset_annotations as $annotation) {
                if (!$Annotations->save($annotation)) {
                    $this->setError('artifact_asset_annotations', __('Failed to save annotation #{0}', $annotation->id), false);
                }
            }
        }

        if (count($this->entities_updates) > 0) {
            $this->getTableLocator()->get('UpdateEvents')->loadInto($this, ['EntitiesUpdates']);
            $EntitiesUpdates = $this->getTableLocator()->get('EntitiesUpdates');

            foreach ($this->entities_updates as $entitiesUpdate) {
                $table = $entitiesUpdate->getTable();
                if (is_null($entitiesUpdate->getUpdate())) {
                    $success = $table->delete($entitiesUpdate->getEntity());
                } else {
                    $entity = $entitiesUpdate->apply();
                    $success = $table->save($entity);
                    if (!$entitiesUpdate->has('entity_id')) {
                        $entitiesUpdate->entity_id = $entity->id;
                        $success = $success && $EntitiesUpdates->save($entitiesUpdate);
                    }
                }
                if (!$success) {
                    // Try to append some error messages
                    $reasons = [];
                    foreach ($entity->getErrors() as $field => $errors) {
                        if (!is_array($errors)) {
                            $errors = [$errors];
                        }

                        foreach ($errors as $error) {
                            if (is_string($error)) {
                                $reasons[] = $error;
                            }
                        }
                    }
                    $reasons = implode(', ', $reasons);

                    $this->setError('entities_updates', __('Failed to save update #{0}. Reason(s): {1}', $entitiesUpdate->id, $reasons), false);
                }
            }
        }

        if (count($this->inscriptions) > 0) {
            $this->getTableLocator()->get('UpdateEvents')->loadInto($this, ['Inscriptions.Artifacts']);
            $Inscriptions = $this->getTableLocator()->get('Inscriptions');
            $Artifacts = $this->getTableLocator()->get('Artifacts');

            foreach ($this->inscriptions as $inscription) {
                $Artifacts->loadInto($inscription->artifact, ['Inscriptions']);

                $success = true;
                if ($inscription->artifact->has('inscription')) {
                    $inscription->artifact->inscription->is_latest = false;
                    $success = $success && $Inscriptions->save($inscription->artifact->inscription);
                }

                $inscription->is_latest = true;
                $success = $success && $Inscriptions->save($inscription);

                if (!$success) {
                    $this->setError('inscriptions', __('Failed to update inscription #{0}', $inscription->id), false);
                }
            }
        }

        return !$this->hasErrors();
    }

    public function isCreatedBy(User $user)
    {
        if (!$user->has('author_id')) {
            return false;
        }

        return $this->created_by == $user->author_id;
    }

    public function isApprovableByEditor()
    {
        // Temporarily ignored; see https://gitlab.com/cdli/framework/-/issues/1620
        // if (in_array('atf', $this->update_type) && count($this->inscriptions) > 0) {
        //     foreach ($this->inscriptions as $inscription) {
        //         $inscription->setAtfWarnings();
        //         $warnings = $inscription->getAtfWarnings();
        //         if (in_array('danger', array_column($warnings, 0))) {
        //             return false;
        //         }
        //     }
        // }

        // Temporarily ignored; see https://gitlab.com/cdli/framework/-/issues/1375
        // if (in_array('atf', $this->update_type) && count($this->inscriptions) > 0) {
        //     foreach ($this->inscriptions as $inscription) {
        //         $inscription->setTokenWarnings($inscription->artifact->period_id);
        //         $warnings = $inscription->getTokenWarnings();
        //         if (in_array('danger', array_column($warnings, 0))) {
        //             return false;
        //         }
        //     }
        // }

        if (in_array('annotation', $this->update_type) && count($this->inscriptions) > 0) {
            foreach ($this->inscriptions as $inscription) {
                $inscription->setAnnotationWarnings($inscription->artifact->period_id);
                $warnings = $inscription->getAnnotationWarnings();
                if (in_array('danger', array_column($warnings, 0))) {
                    return false;
                }
            }
        }

        if ($this->missingPrivateChanges) {
            return false;
        }

        return true;
    }

    public function getUpdateField()
    {
        if (in_array('artifact', $this->update_type)) {
            return 'artifacts_updates';
        } elseif (in_array('visual_annotation', $this->update_type)) {
            return 'artifact_asset_annotations';
        } elseif (in_array('other_entity', $this->update_type)) {
            return 'entities_updates';
        } elseif (in_array('atf', $this->update_type) || in_array('translation', $this->update_type) || in_array('annotation', $this->update_type)) {
            return 'inscriptions';
        }
    }
}
