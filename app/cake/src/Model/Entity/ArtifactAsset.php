<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * ArtifactAsset Entity
 *
 * @property int $id
 * @property int $artifact_id
 * @property string|null $artifact_aspect
 * @property bool $is_public
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property string $asset_type
 * @property string $file_format
 * @property float $file_size
 * @property int|null $image_width
 * @property int|null $image_height
 * @property float|null $image_scale
 * @property string|null $image_color_average
 * @property int|null $image_color_depth
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\Author[] $authors
 * @property \App\Model\Entity\Publication[] $publications
 */
class ArtifactAsset extends Entity implements LinkedDataInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'artifact_aspect' => true,
        'is_public' => true,
        'created' => true,
        'modified' => true,
        'asset_type' => true,
        'file_format' => true,
        'file_size' => true,
        'image_width' => true,
        'image_height' => true,
        'image_scale' => true,
        'image_color_average' => true,
        'image_color_depth' => true,
        'license_id' => true,
        'license_attribution' => true,
        'license_comment' => true,
        'artifact' => true,
        'annotations' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'artifact_id',
        'artifact_aspect',
        'created',
        'modified',
        'asset_type',
        'file_format',
        'file_size',
        'image_width',
        'image_height',
        'image_scale',
        'image_color_average',
        'image_color_depth',
        'license_id',
        'license_attribution',
        'license_comment',
        'artifact',
        'annotations',
        'authors',
        'publications',
    ];

    /**
     * Return main path to asset.
     *
     * @return string
     */
    public function getPath()
    {
        // Base directory
        $directory = $this->asset_type;
        if ($this->asset_type === '3D model') {
            $directory = 'vcmodels';
        } elseif ($this->asset_type === 'rti') {
            $directory = 'ptm';
        } elseif (in_array($this->file_format, ['svg', 'eps', 'pdf'])) {
            $directory = $this->file_format;
        } elseif ($this->asset_type === 'info' && $this->file_format === 'html') {
            $directory = 'long_translit';
        }

        // File name
        $file = 'P' . str_pad(strval($this->artifact_id), 6, '0', STR_PAD_LEFT);

        // File name suffix(es)
        $suffix = $this->asset_type === 'lineart' ? 'l' : '';

        if ($this->has('artifact_aspect')) {
            foreach (explode(', ', $this->artifact_aspect) as $part) {
                if (array_key_exists($part, self::$aspectToSuffix)) {
                    $suffix .= self::$aspectToSuffix[$part];
                } elseif (str_starts_with($part, 'column ')) {
                    $suffix .= substr($part, strlen('column '));
                } elseif (str_starts_with($part, 'detail')) {
                    $suffix .= 'd' . substr($part, strlen('detail '));
                }
            }
        }

        if ($suffix !== '') {
            $file .= '_' . $suffix;
        }

        if ($this->asset_type === '3D model') {
            $file .= DS . $file;
        } elseif ($this->asset_type === 'rti') {
            $file .= DS . 'info';
        } elseif ($this->asset_type === 'info' && $this->file_format === 'html') {
            $file .= DS . 'index';
        }

        return 'dl' . DS . $directory . DS . $file . '.' . $this->file_format;
    }

    /**
     * Return path for asset thumbnail.
     *
     * @return string|null
     */
    public function getThumbnailPath()
    {
        if ($this->asset_type === '3D model') {
            return 'images/thumbnail-3d.png';
        }

        $path = null;
        if ($this->asset_type === 'rti') {
            $path = substr($this->getPath(), 0, -strlen('/info.xml')) . '/1_3.jpg';
        } elseif ($this->file_format === 'jpg') {
            $path = $this->getPath();
            $path = 'dl' . DS . 'tn_' . substr($path, strlen('dl' . DS));
        }

        if (!is_null($path) && file_exists(WWW_ROOT . $path)) {
            return $path;
        }

        return null;
    }

    /**
     * Return human-readable description of asset.
     *
     * @return string
     */
    public function getDescription()
    {
        $description = $this->asset_type === 'rti' ? 'RTI' : ucfirst($this->asset_type);
        if ($this->has('artifact_aspect')) {
            $description .= " ({$this->artifact_aspect})";
        }

        return $description;
    }

    /**
     * For photos, lineart, pdfs it returns the path to the file itself.
     * For RTI and info, it returns the path to the directory.
     *
     * @param string $path
     * @return string
     */
    public static function getEncompassingPath(string $path)
    {
        $prefix = WWW_ROOT . 'dl' . DS;
        $parts = explode(DS, substr($path, strlen($prefix)));

        return $prefix . $parts[0] . DS . $parts[1];
    }

    /**
     * @return ?string
     */
    public function getMimeType()
    {
        if ($this->file_format === 'jpg') {
            return 'image/jpeg';
        } elseif ($this->file_format === 'tif') {
            return 'image/tiff';
        } elseif ($this->file_format === 'svg') {
            return 'image/svg+xml';
        } elseif ($this->file_format === 'pdf') {
            return 'application/pdf';
        } elseif ($this->file_format === 'ply') {
            return 'text/plain';
        } elseif ($this->file_format === 'xml') {
            return 'application/xml';
        }
    }

    /**
     * Get description of asset based on the path.
     *
     * @param string $path
     * @param bool $strict allow only the main file (for import) or allow all files (for access check) (default: true)
     * @return array|null
     */
    public static function parsePath(string $path, bool $strict = true)
    {
        $data = [];

        $directory = explode(DS, $path)[1];
        $file = basename($path);

        // [$artifact_id, $is_lineart, $artifact_aspect, $file_format]
        //  P0*([1-9]\d*)     l?           [a-z1-9]*   (ply|xml|html|.+)
        $parts = [];
        $matches = false;
        if ($directory === 'vcmodels') {
            $relativePath = substr($path, strlen('dl' . DS . 'vcmodels' . DS));
            if ($strict) {
                $matches = preg_match('/^P0*([1-9]\d*)(?:_()([a-z1-9]*))?\/P0*\1(?:_\2\3)?\.(ply)$/', $relativePath, $parts);
            } else {
                $matches = preg_match('/^P0*([1-9]\d*)(?:_()([a-z1-9]*))?\/(.+)/', $relativePath, $parts);
                $parts[4] = 'ply';
            }
        } elseif ($directory === 'ptm') {
            $relativePath = substr($path, strlen('dl' . DS . 'ptm' . DS));
            if ($strict) {
                $matches = preg_match('/^P0*([1-9]\d*)(?:_()([a-z1-9]*))?\/info\.(xml)$/', $relativePath, $parts);
            } else {
                $matches = preg_match('/^P0*([1-9]\d*)(?:_()([a-z1-9]*))?\/(.+)/', $relativePath, $parts);
                $parts[4] = 'xml';
            }
        } elseif ($directory === 'long_translit') {
            $relativePath = substr($path, strlen('dl' . DS . 'long_translit' . DS));
            if ($strict) {
                $matches = preg_match('/^P0*([1-9]\d*)(?:_()([a-z1-9]*))?\/index\.(html)$/', $relativePath, $parts);
            } else {
                $matches = preg_match('/^P0*([1-9]\d*)(?:_()([a-z1-9]*))?\/(.+)/', $relativePath, $parts);
                $parts[4] = 'html';
            }
        } else {
            $matches = preg_match('/^P0*([1-9]\d*)(?:_(l?)([a-z1-9]*))?\.(.+)$/', $file, $parts);
        }

        if (!$matches) {
            return null;
        }

        $data['artifact_id'] = $parts[1];

        if ($directory === 'vcmodels') {
            $data['asset_type'] = '3D model';
        } elseif ($directory === 'ptm') {
            $data['asset_type'] = 'rti';
        } elseif ($directory === 'lineart' || (!$strict && $directory === 'tn_lineart')) {
            $data['asset_type'] = 'lineart';
        } elseif ($directory === 'photo' || (!$strict && $directory === 'tn_photo')) {
            $data['asset_type'] = 'photo';
        } else {
            $data['asset_type'] = 'info';
        }

        // Error if lineart does not have _l in name; error if non-lineart has _l in name
        if (($parts[2] === 'l') !== ($data['asset_type'] === 'lineart')) {
            return null;
        }

        // [$aspect, $column, $detail]
        $suffixParts = [];
        $suffixMatches = preg_match('/^(?:(s[a-d]|[trlb]e|[eors]|)([1-9][a-d]?|))(d[1-9]?|)$/', $parts[3], $suffixParts);
        if (!$suffixMatches) {
            return null;
        }
        if ($suffixParts[1] !== '') {
            $data['artifact_aspect'] = self::$suffixToAspect[$suffixParts[1]];
            if ($suffixParts[2] !== '') {
                $data['artifact_aspect'] .= ', column ' . $suffixParts[2];
            }
        }
        if ($suffixParts[3] !== '') {
            $detail = $suffixParts[3] === 'd' ? 'detail' : 'detail ' . substr($suffixParts[3], 1);
            $data['artifact_aspect'] = array_key_exists('artifact_aspect', $data) ? $data['artifact_aspect'] . ', ' . $detail : $detail;
        }

        $data['file_format'] = $parts[4];

        return $data;
    }

    private static $suffixToAspect = [
        'e' => 'envelope',
        'o' => 'obverse',
        'r' => 'reverse',
        's' => 'seal impression',

        're' => 'right edge',
        'be' => 'bottom edge',
        'te' => 'top edge',
        'le' => 'left edge',

        'sa' => 'surface a',
        'sb' => 'surface b',
        'sc' => 'surface c',
        'sd' => 'surface d',
    ];

    private static $aspectToSuffix = [
        'envelope' => 'e',
        'obverse' => 'o',
        'reverse' => 'r',
        'seal impression' => 's',

        'right edge' => 're',
        'bottom edge' => 'be',
        'top edge' => 'te',
        'left edge' => 'le',

        'surface a' => 'sa',
        'surface b' => 'sb',
        'surface c' => 'sc',
        'surface d' => 'sd',
    ];

    public function getTableRow()
    {
        return [
            'path' => $this->getPath(),
            'thumbnail' => $this->getThumbnailPath(),
            'artifact_id' => $this->artifact_id,
            'artifact_aspect' => $this->artifact_aspect,
            'asset_type' => $this->asset_type,
            'file_format' => $this->file_format,
        ];
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "artifact-assets/$id";
    }

    public function getLinkedData(): array
    {
        $uri = $this->getUri();
        $data = ['rdf:type' => [LinkedData::newResource('crm:E31_Document')]];

        if ($this->asset_type === 'lineart') {
            $data['rdf:type'][] = LinkedData::newResource('schema:Drawing');
        } elseif ($this->asset_type === 'photo') {
            $data['rdf:type'][] = LinkedData::newResource('schema:Photograph');
        } else {
            $data['rdf:type'][] = LinkedData::newResource('schema:CreativeWork');
        }

        $artifact = LinkedData::newResource(Artifact::makeUri($this->artifact_id));
        if ($this->has('artifact_aspect')) {
            $data['crm:P70_documents'] = LinkedData::newResource("$uri#aspect", [
                'rdf:type' => LinkedData::newResource('crm:E26_Physical_Feature'),
                'crm:P46i_forms_part_of' => $artifact,
            ]);
        } else {
            $data['crm:P70_documents'] = $artifact;
        }

        // Main file
        $data['crm:P138i_has_representation'][] = LinkedData::newResource("$uri#file", [
            'rdf:type' => [
                LinkedData::newResource('crm:E36_Visual_Item'),
                LinkedData::newResource($this->asset_type == '3D model' ? 'schema:3DModel' : 'schema:ImageObject'),
            ],
            'schema:encodesCreativeWork' => LinkedData::newResource($uri),
            'schema:embedUrl' => 'https://cdli.mpiwg-berlin.mpg.de/' . $uri,
            'schema:contentUrl' => 'https://cdli.mpiwg-berlin.mpg.de/' . $this->getPath(),
            'schema:encodingFormat' => $this->getMimeType(),
            'schema:contentSize' => $this->has('file_size') && $this->file_size > 0 ? ($this->file_size / (1024 * 1024)) . ' MiB' : null,
            'schema:height' => $this->image_height,
            'schema:width' => $this->image_width,
            'schema:created' => is_null($this->created) ? null : $this->created->toIso8601String(),
        ]);

        // Thumbnail
        $thumbnail = $this->getThumbnailPath();
        if (!is_null($thumbnail)) {
            $data['crm:P138i_has_representation'][] = LinkedData::newResource("$uri#thumbnail", [
                'rdf:type' => [
                    LinkedData::newResource('crm:E36_Visual_Item'),
                    LinkedData::newResource('schema:ImageObject'),
                ],
                'schema:encodesCreativeWork' => LinkedData::newResource($uri),
                'schema:contentUrl' => 'https://cdli.mpiwg-berlin.mpg.de/' . $thumbnail,
                'schema:encodingFormat' => $this->getMimeType(),
                'schema:created' => is_null($this->created) ? null : $this->created->toIso8601String(),
            ]);
        }

        if ($this->has('publications')) {
            foreach ($this->publications as $publication) {
                $data['crm:P148i_is_component_of'][] = LinkedData::newResource(Publication::makeUri($publication->id));
            }
        }

        if ($this->has('authors')) {
            $roles = [];
            foreach ($this->authors as $author) {
                $authorUri = LinkedData::newResource(Author::makeUri($author->id));
                $data['schema:contributor'][] = $authorUri;
                $roles[$author->_joinData->role][] = $authorUri;
            }
            foreach ($roles as $role => $authors) {
                $role = preg_replace('/[^a-zA-Z0-9-]+/', '_', $role);
                $data['crm:P12i_was_present_at'][] = LinkedData::newResource("$uri#contribution/$role", [
                    'rdf:type' => LinkedData::newResource('crm:E7_Activity'),
                    'crm:P11_had_participant' => $authors,
                ]);
            }
        }

        return $data;
    }
}
