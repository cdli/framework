<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * ArtifactsExternalResource Entity
 *
 * @property int $id
 * @property int|null $artifact_id
 * @property int|null $external_resource_id
 * @property string|null $external_resource_key
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\ExternalResource $external_resource
 */
class ArtifactsExternalResource extends Entity implements LinkedDataInterface
{
    use ExternalResourceLinkTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'external_resource_id' => true,
        'external_resource_key' => true,
        'artifact' => true,
        'external_resource' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'artifact_id',
        'external_resource_id',
        'external_resource_key',
        'artifact',
        'external_resource',
    ];

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "artifacts-external-resources/$id";
    }

    public function getLinkedData(): array
    {
        $externalUri = $this->getExternalUrl('base_uri');

        return [
            'rdf:type' => LinkedData::newResource('crm:E42_Identifier'),
            'crm:P37i_was_assigned_by' => LinkedData::newResource($this->getUri() . '#assignment', [
                'rdf:type' => LinkedData::newResource('crm:E15_Identifier_Assignment'),
                'crm:P14_carried_out_by' => LinkedData::newResource(ExternalResource::makeUri($this->external_resource_id)),
            ]),
            'crm:P1i_identifies' => LinkedData::newResource(Artifact::makeUri($this->artifact_id), [
                'owl:sameAs' => LinkedData::newResource($externalUri),
            ]),
            'crm:P190_has_symbolic_content' => $externalUri ?? $this->getExternalUrl('base_url') ?? $this->getIdentifier(),
        ];
    }
}
