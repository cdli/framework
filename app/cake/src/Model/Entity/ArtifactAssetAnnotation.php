<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Model\Entity;

use App\Utility\Svg\AnnotationSelector;
use Cake\ORM\Entity;
use Cake\Routing\Router;

/**
 * ArtifactAssetAnnotation Entity
 *
 * @property int $id
 * @property int $annotation_id
 * @property int $artifact_asset_id
 * @property int $update_event_id
 * @property string|null $license
 * @property string $target_selector
 * @property bool $is_active
 *
 * @property \App\Model\Entity\Annotation $annotation
 * @property \App\Model\Entity\ArtifactAsset $artifact_asset
 * @property \App\Model\Entity\UpdateEvent $update_events
 * @property \App\Model\Entity\ArtifactAssetAnnotationBody[] $bodies
 * @property \App\Model\Entity\UpdateEvent $update_event
 */
class ArtifactAssetAnnotation extends Entity implements UpdateInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'annotation_id' => true,
        'license' => true,
        'target_selector' => true,
        'is_active' => true,
        'bodies' => true,
    ];

    protected function getId()
    {
        return Router::url('artifact-asset-annotations/' . $this->id, true);
    }

    public function getW3cJson()
    {
        $creation = $this->update_event;
        if ($this->has('original_annotation')) {
            $creation = $this->get('original_annotation')->update_event;
        }
        $creators = [$creation->creator, ...($creation->authors ?? [])];

        return [
            '@context' => 'http://www.w3.org/ns/anno.jsonld',
            'id' => $this->getId(),
            'type' => 'Annotation',
            'body' => array_map(function ($body) {
                $body = $body->getW3cJson();
                $body['rights'] = $this->license;

                return $body;
            }, $this->bodies),
            'target' => [
                'source' => [
                    'id' => Router::url($this->artifact_asset->getPath(), true),
                ],
                'selector' => [
                    'type' => 'SvgSelector',
                    'value' => $this->target_selector,
                ],
            ],
            'created' => $creation->approved,
            'creator' => array_map(function ($author) {
                return [
                    'id' => $author->getUri(),
                    'type' => $author->id === 820 ? 'Organization' : 'Person',
                    'name' => $author->author,
                ];
            }, $creators),
            'modified' => $this->update_event->approved,
            'rights' => $this->license,
        ];
    }

    public function getViaJson()
    {
        $fields = [
            '@id' => $this->getId(),
        ];

        foreach ($this->bodies as $body) {
            if ($body->purpose === 'classifying') {
                $fields['classifying'] = $body->source ?? $body->value;
            } elseif (in_array($body->purpose, ['identifying', 'describing', 'assessing'])) {
                $fields[$body->purpose][$body->source ?? $body->value] = true;
            }
        }

        return [
            'shape_attributes' => AnnotationSelector::fromSvg($this->target_selector)->getViaJson(),
            'region_attributes' => $fields,
        ];
    }

    /**
     * @return string
     */
    public function getUpdateEventField(): string
    {
        return 'artifact_asset_annotations';
    }
}
