<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * Location Entity
 *
 * @property int $id
 * @property string|null $accuracy
 * @property float $location_longitude_wgs1984
 * @property float $location_latitude_wgs1984
 * @property string|null $location_geoshape_wgs1984
 * @property string|null $notes
 *
 * @property \App\Model\Entity\Provenience[] $proveniences
 * @property \App\Model\Entity\Places[] $places
 * @property \App\Model\Entity\EntitiesName[] $entities_names
 * @property \App\Model\Entity\ExternalResource[] $external_resources
 */
class Location extends Entity implements LinkedDataInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'location' => true,
        'accuracy' => true,
        'location_longitude_wgs1984' => true,
        'location_latitude_wgs1984' => true,
        'location_geoshape_wgs1984' => true,
        'notes' => true,
        'entities_names' => true,
        'external_resources' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'location',
        'accuracy',
        'location_longitude_wgs1984',
        'location_latitude_wgs1984',
        'location_geoshape_wgs1984',
        'notes',

        'entities_names',
        'external_resources',
        'proveniences',
        'places',
    ];

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "locations/$id";
    }

    public function getLinkedData(): array
    {
        $uri = $this->getUri();

        return [
            'rdf:type' => [
                LinkedData::newResource('crm:E53_Place'),
                LinkedData::newResource('pleiades:Location'),
            ],
            'geo:hasGeometry' => LinkedData::newGeometry("$uri#geometry", $this->location_geoshape_wgs1984),
            'geo:hasCentroid' => LinkedData::newGeometry("$uri#centroid", [
                'type' => 'Point',
                'coordinates' => [$this->location_longitude_wgs1984, $this->location_latitude_wgs1984],
            ]),
            'crm:P3_has_note' => $this->notes,
        ];
    }
}
