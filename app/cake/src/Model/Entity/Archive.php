<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * Archive Entity
 *
 * @property int $id
 * @property string|null $archive
 * @property int|null $provenience_id
 *
 * @property \App\Model\Entity\Provenience $provenience
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class Archive extends Entity implements LinkedDataInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'archive' => true,
        'provenience_id' => true,
        'provenience' => true,
        'artifacts' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'archive',
        'provenience_id',
        'provenience',
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'archive' => $this->archive,
            'provenience' => $this->has('provenience') ? $this->provenience->provenience : '',
        ];
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "archives/$id";
    }

    public function getLinkedData(): array
    {
        return [
            'rdf:type' => LinkedData::newResource('crm:E53_Place'),
            'rdfs:label' => $this->archive,
            'crm:P89_falls_within' => LinkedData::newResource(Provenience::makeUri($this->provenience_id)),
        ];
    }
}
