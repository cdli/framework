<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * Genre Entity
 *
 * @property int $id
 * @property string $genre
 * @property int|null $parent_id
 * @property string|null $genre_description
 *
 * @property \App\Model\Entity\Genre $parent_genre
 * @property \App\Model\Entity\Genre[] $child_genres
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class Genre extends Entity implements LinkedDataInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'genre' => true,
        'parent_id' => true,
        'genre_description' => true,
        'parent_genre' => true,
        'child_genres' => true,
        'artifacts' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'genre',
        'parent_id',
        'genre_description',
        'parent_genre',
        'child_genres',
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'genre' => $this->genre,
            'parent' => $this->has('parent_genre') ? $this->parent_genre->genre : '',
            'genre_description' => $this->genre_description,
        ];
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "genres/$id";
    }

    public function getLinkedData(): array
    {
        $entity = [
            'rdf:type' => [
                LinkedData::newResource('terms/type/genre'),
                LinkedData::newResource('crm:E55_Type'),
            ],
            'rdfs:label' => $this->genre,
            'crm:P127_has_broader_term' => LinkedData::newResource(self::makeUri($this->parent_id)),
        ];

        if (!empty($this->genre_description)) {
            $entity['crm:P3_has_note'] = $this->genre_description;
        }

        return $entity;
    }
}
