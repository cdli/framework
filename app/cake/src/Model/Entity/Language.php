<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * Language Entity
 *
 * @property int $id
 * @property int|null $sequence
 * @property int|null $parent_id
 * @property string $language
 * @property string|null $protocol_code
 * @property string|null $inline_code
 * @property string|null $notes
 *
 * @property \App\Model\Entity\Language $parent_language
 * @property \App\Model\Entity\Language[] $child_languages
 * @property \App\Model\Entity\SignReading[] $sign_readings
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class Language extends Entity implements LinkedDataInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sequence' => true,
        'parent_id' => true,
        'language' => true,
        'protocol_code' => true,
        'inline_code' => true,
        'notes' => true,
        'parent_language' => true,
        'child_languages' => true,
        'sign_readings' => true,
        'artifacts' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'sequence',
        'parent_id',
        'language',
        'protocol_code',
        'inline_code',
        'notes',
        'parent_language',
        'child_languages',
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'sequence' => $this->sequence,
            'parent' => $this->has('parent_language') ? $this->parent_language->language : '',
            'language' => $this->language,
            'protocol_code' => $this->protocol_code,
            'inline_code' => $this->inline_code,
            'notes' => $this->notes,
        ];
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "languages/$id";
    }

    public function getLinkedData(): array
    {
        $uri = $this->getUri();

        $entity = [
            'rdf:type' => LinkedData::newResource('crm:E56_Language'),
            'rdfs:label' => $this->language,
            'crm:P1_is_identified_by' => [
                LinkedData::newAppellation("$uri#identifier/iso-639", $this->inline_code, 'terms/identifier/iso-639'),
                LinkedData::newAppellation("$uri#identifier/atf-protocol-code", $this->protocol_code, 'terms/identifier/atf-protocol-code'),
                LinkedData::newAppellation("$uri#identifier/atf-inline-code", $this->inline_code, 'terms/identifier/atf-inline-code'),
            ],
            'crm:P127_has_broader_term' => LinkedData::newResource(self::makeUri($this->parent_id)),
        ];

        if (!empty($this->notes)) {
            $entity['crm:P3_has_note'] = $this->notes;
        }

        return $entity;
    }
}
