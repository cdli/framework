<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ImageAnnotation Entity
 *
 * @property int $id
 * @property int $annotation_id
 * @property string|null $creator
 * @property \Cake\I18n\FrozenTime|null $created_at
 * @property int $annotation_type_id
 * @property string $annotation_body_type
 * @property string $annotation_body_value
 * @property string $annotation_body_purpose
 * @property string|null $tag_label
 * @property string|null $tag_uri
 * @property string $annotation_target_source
 * @property string $annotation_target_selector
 * @property string $surface
 * @property int $image_id
 * @property int $artifact_id
 *
 * @property \App\Model\Entity\Annotation $annotation
 * @property \App\Model\Entity\AnnotationType $annotation_type
 * @property \App\Model\Entity\Image $image
 * @property \App\Model\Entity\Artifact $artifact
 */
class ImageAnnotation extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'annotation_id' => true,
        'creator' => true,
        'created_at' => true,
        'annotation_type_id' => true,
        'annotation_body_type' => true,
        'annotation_body_value' => true,
        'annotation_body_purpose' => true,
        'tag_label' => true,
        'tag_uri' => true,
        'annotation_target_source' => true,
        'annotation_target_selector' => true,
        'surface' => true,
        'image_id' => true,
        'artifact_id' => true,
        'annotation' => true,
        'annotation_type' => true,
        'image' => true,
        'artifact' => true,
    ];
}
