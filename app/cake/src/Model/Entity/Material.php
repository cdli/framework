<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * Material Entity
 *
 * @property int $id
 * @property string $material
 * @property int|null $parent_id
 *
 * @property \App\Model\Entity\Material $parent_material
 * @property \App\Model\Entity\Material[] $child_materials
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class Material extends Entity implements LinkedDataInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'material' => true,
        'parent_id' => true,
        'parent_material' => true,
        'child_materials' => true,
        'artifacts' => true,
        '_joinData' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'material',
        'parent_id',
        'parent_material',
        'child_materials',
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'material' => $this->material,
            'parent' => $this->has('parent_material') ? $this->parent_material->material : '',
        ];
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "materials/$id";
    }

    public function getLinkedData(): array
    {
        return [
            'rdf:type' => LinkedData::newResource('crm:E57_Material'),
            'rdfs:label' => $this->material,
            'crm:P127_has_broader_term' => LinkedData::newResource(self::makeUri($this->parent_id)),
        ];
    }
}
