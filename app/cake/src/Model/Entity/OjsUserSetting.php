<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OjsUserSetting Entity
 *
 * @property int $user_id
 * @property string $locale
 * @property string $setting_name
 * @property int|null $assoc_type
 * @property int|null $assoc_id
 * @property string|null $setting_value
 * @property string $setting_type
 *
 * @property \App\Model\Entity\OjsUser $users_oj
 * @property \App\Model\Entity\Assoc $assoc
 */
class OjsUserSetting extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'locale' => true,
        'setting_name' => true,
        'assoc_type' => true,
        'assoc_id' => true,
        'setting_value' => true,
        'setting_type' => true,
        'users_oj' => true,
        'assoc' => true,
    ];
}
