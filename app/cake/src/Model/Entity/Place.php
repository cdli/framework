<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Place Entity
 *
 * @property int $id
 * @property string $place
 * @property int $location_id
 * @property int $period_id
 *
 * @property \App\Model\Entity\Location $location
 * @property \App\Model\Entity\Period $period
 * @property \App\Model\Entity\Provenience[] $proveniences
 * @property \App\Model\Entity\ExternalResource[] $external_resources
 */
class Place extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'place' => true,
        'location_id' => true,
        'location' => true,
        'period_id' => true,
        'period' => true,
        'external_resources' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'place',
        'location',
        'period',
        'proveniences',
        'external_resources',
    ];
}
