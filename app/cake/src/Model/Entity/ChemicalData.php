<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ChemicalData Entity
 *
 * @property int $id
 * @property string $chemistry
 * @property string $area
 * @property string|null $area_description
 * @property string $reading
 * @property string $time
 * @property string $duration
 * @property string $date
 * @property int $artifact_id
 * @property int $calibration_id
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\Calibration $calibration
 */
class ChemicalData extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'chemistry' => true,
        'area' => true,
        'area_description' => true,
        'reading' => true,
        'time' => true,
        'duration' => true,
        'date' => true,
        'artifact_id' => true,
        'calibration_id' => true,
        'artifact' => true,
        'calibration' => true,
    ];
}
