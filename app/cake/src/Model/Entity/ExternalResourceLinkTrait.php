<?php
declare(strict_types=1);

namespace App\Model\Entity;

trait ExternalResourceLinkTrait
{
    /**
     * @return ?string
     */
    public function getIdentifier(): ?string
    {
        if (!$this->has('external_resource') || !$this->external_resource->has('abbrev') || !$this->has('external_resource_key')) {
            return null;
        }

        return $this->external_resource->abbrev . ' ' . $this->external_resource_key;
    }

    /**
     * @param ?string $field
     * @return ?string
     */
    public function getExternalUrl(?string $field = 'base_url'): ?string
    {
        if (!$this->has('external_resource') || !$this->external_resource->has($field) || !$this->has('external_resource_key')) {
            return null;
        }

        $url = (string)$this->external_resource->get($field);

        return $this->formatExternalUrl($url);
    }

    public function formatExternalUrl(?string $url): ?string
    {
        if (empty($url)) {
            return null;
        }

        $key = (string)$this->external_resource_key;

        if (preg_match('{TEXT_ID}', $url)) {
            return preg_replace('{TEXT_ID}', $key, $url);
        } else {
            return $url . $key;
        }
    }
}
