<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * Period Entity
 *
 * @property int $id
 * @property int|null $sequence
 * @property string $period
 * @property string $name
 * @property string $time_range
 *
 * @property \App\Model\Entity\Artifact[] $artifacts
 * @property \App\Model\Entity\Ruler[] $rulers
 * @property \App\Model\Entity\SignReading[] $sign_readings
 * @property \App\Model\Entity\Year[] $years
 */
class Period extends Entity implements LinkedDataInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sequence' => true,
        'period' => true,
        'name' => true,
        'time_range' => true,
        'artifacts' => true,
        'rulers' => true,
        'sign_readings' => true,
        'years' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'sequence',
        'period',
        'name',
        'time_range',
        'rulers',
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'sequence' => $this->sequence,
            'period' => $this->period,
            'name' => $this->name,
            'time_range' => $this->time_range,
        ];
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "periods/$id";
    }

    public function getLinkedData(): array
    {
        $entity = [
            'rdf:type' => LinkedData::newResource('crm:E4_Period'),
            'rdfs:label' => $this->period,
        ];

        preg_match('/(.+) \\(((?:ca?\\. )?)(.+?)\\)/', $this->period, $matches);
        if (count($matches) > 0) {
            $entity['rdfs:label'] = $matches[1];
            if ($matches[2] === 'ca. ' || $matches[2] === 'c. ') {
                $entity['crm:P79_end_is_qualified_by'] = 'circa';
                $entity['crm:P79_beginning_is_qualified_by'] = 'circa';
            }
            $entity['crm:P4_has_time-span'] = LinkedData::newResource($this->getUri() . '#time-span', [
                'rdf:type' => LinkedData::newResource('crm:E52_Time-Span'),
                'crm:P170i_time_is_defined_by' => $matches[3],
            ]);
        }

        return $entity;
    }
}
