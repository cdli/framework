<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * @property int $id
 * @property int $artifact_asset_id
 * @property int $author_id
 * @property string|null $role
 *
 * @property \App\Model\Entity\ArtifactAsset $artifact_asset
 * @property \App\Model\Entity\Author $author
 */
class ArtifactAssetsAuthor extends Entity
{
    /**
     * @var array
     */
    protected $_accessible = [
        'artifact_asset_id' => true,
        'author_id' => true,
        'role' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'artifact_asset_id',
        'author_id',
        'role',

        'artifact_asset',
        'author',
    ];
}
