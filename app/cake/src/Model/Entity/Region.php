<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * Region Entity
 *
 * @property int $id
 * @property string $region
 * @property string|null $geo_coordinates
 * @property int $pleiades_id
 *
 * @property \App\Model\Entity\Provenience[] $proveniences
 */
class Region extends Entity implements LinkedDataInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'region' => true,
        'geo_coordinates' => true,
        'pleiades_id' => true,
        'proveniences' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'region',
        'geo_coordinates',
        'pleiades_id',
        'proveniences',
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'region' => $this->region,
            'geo_coordinates' => $this->geo_coordinates,
            'pleiades_id' => $this->pleiades_id,
        ];
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "regions/$id";
    }

    public function getLinkedData(): array
    {
        if (!empty($this->geo_coordinates)) {
            $geoJson = '{"type": "Polygon", "coordinates": [[' . $this->geo_coordinates . ']]}';
            $geometry = LinkedData::newGeometry($this->getUri() . '#geometry', $geoJson);
        }

        return [
            'rdf:type' => LinkedData::newResource('crm:E53_Place'),
            'rdfs:label' => $this->region,
            'geo:hasGeometry' => $geometry ?? null,
        ];
    }
}
