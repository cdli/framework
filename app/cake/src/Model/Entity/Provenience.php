<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * Provenience Entity
 *
 * @property int $id
 * @property string $provenience
 * @property string|null $description
 * @property int|null $location_id
 * @property int|null $place_id
 * @property int|null $region_id
 *
 * @property \App\Model\Entity\Location $location
 * @property \App\Model\Entity\Place $place
 * @property \App\Model\Entity\Region $region
 * @property \App\Model\Entity\Archive[] $archives
 * @property \App\Model\Entity\Artifact[] $artifacts
 * @property \App\Model\Entity\Dynasty[] $dynasties
 * @property \App\Model\Entity\Publication[] $publications
 * @property \App\Model\Entity\SignReading[] $sign_readings
 */
class Provenience extends Entity implements LinkedDataInterface
{
    use TableExportTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'provenience' => true,
        'description' => true,
        'location_id' => true,
        'place_id' => true,
        'region_id' => true,
        'region' => true,
        'archives' => true,
        'artifacts' => true,
        'dynasties' => true,
        'publications' => true,
        'sign_readings' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'provenience',
        'description',
        'location_id',
        'location',
        'place_id',
        'place',
        'region_id',
        'region',
        'archives',
        'dynasties',
        'publications',
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'provenience' => $this->provenience,
            'description' => $this->description,
            'region' => $this->serializeDisplay($this->region, 'region'),

            'place' => $this->serializeDisplay($this->place, 'place'),
            'place_external_resources' => $this->serializeExternalResources($this->place ? $this->place->external_resources : []),

            'location' => $this->serializeDisplay($this->location, 'location'),
            'location_longitude_wgs1984' => $this->serializeDisplay($this->location, 'location_longitude_wgs1984'),
            'location_latitude_wgs1984' => $this->serializeDisplay($this->location, 'location_latitude_wgs1984'),
            'location_geoshape_wgs1984' => $this->serializeDisplay($this->location, 'location_geoshape_wgs1984'),
            'location_accuracy' => $this->serializeDisplay($this->location, 'accuracy'),
            'location_notes' => $this->serializeDisplay($this->location, 'notes'),
            'location_names' => $this->serializeEntitiesNames($this->location ? $this->location->entities_names : []),
            'location_external_resources' => $this->serializeExternalResources($this->location ? $this->location->external_resources : []),
        ];
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "proveniences/$id";
    }

    public function getLinkedData(): array
    {
        if ($this->has('place')) {
            $label = $this->place->place;
            $period = $this->place->period_id;
            $location = $this->place->location_id;
        } else {
            preg_match('/(.+) \\((?:mod. )?(.+?)\\)/', $this->provenience, $matches);
            $label = $matches[1] ?? $this->provenience;
            $period = null;
            $location = $this->location_id;
        }

        return [
            'rdf:type' => [
                LinkedData::newResource('crm:E93_Presence'),
                LinkedData::newResource('pleiades:Place'),
            ],
            'rdfs:label' => $label,
            'crm:P161_has_spatial_projection' => LinkedData::newResource($this->getUri() . '#location', [
                'rdf:type' => LinkedData::newResource('crm:E53_Place'),
                'crm:P89_falls_within' => LinkedData::newResource(Region::makeUri($this->region_id)),
                'crm:P189i_is_approximated_by' => LinkedData::newResource(Location::makeUri($location)),
            ]),
            'pleiades:hasLocation' => LinkedData::newResource(Location::makeUri($location)),
            'pleiades:hasName' => LinkedData::newResource($this->getUri() . '#name', [
                'rdf:type' => LinkedData::newResource('pleiades:Name'),
                'pleiades:nameRomanized' => $label,
                'pleiades:during' => LinkedData::newResource(Period::makeUri($period)),
            ]),
        ];
    }
}
