<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;
use EasyRdf\Literal;

/**
 * Author Entity
 *
 * @property int $id
 * @property string $author
 * @property string|null $last
 * @property string|null $first
 * @property bool $east_asian_order
 * @property string|null $email
 * @property string|null $institution
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property string|null $orcid_id
 * @property int $birth_year
 * @property int $death_year
 * @property string|null $biography
 * @property int|null $ojs_author_id
 * @property string $org_script_name
 *
 * @property \App\Model\Entity\SignReadingsComment[] $sign_readings_comments
 * @property \App\Model\Entity\Staff[] $staff
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Article[] $articles
 * @property \App\Model\Entity\Publication[] $publications
 * @property \App\Model\Entity\UpdateEvent[] $update_events
 */
class Author extends Entity implements LinkedDataInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'author' => true,
        'last' => true,
        'first' => true,
        'east_asian_order' => true,
        'email' => true,
        'institution' => true,
        'modified' => true,
        'orcid_id' => true,
        'birth_year' => true,
        'death_year' => true,
        'biography' => true,
        'sign_readings_comments' => true,
        'staff' => true,
        'user' => true,
        'articles' => true,
        'publications' => true,
        'update_events' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'author',
        'last',
        'first',
        'east_asian_order',
        'email',
        'institution',
        'orcid_id',
        'birth_year',
        'death_year',
        'sign_readings_comments',
        'staff',
        'articles',
        'publications',
        'update_events',
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'author' => $this->author,
            'last' => $this->last,
            'first' => $this->first,
            'east_asian_order' => $this->east_asian_order,
            'email' => $this->email,
            'institution' => $this->institution,
            'biography' => $this->biography,
            'modified' => $this->modified,
            'orcid_id' => $this->orcid_id,
            'birth_year' => $this->birth_year,
            'death_year' => $this->death_year,
            'ojs_author_id' => $this->ojs_author_id,
            'org_script_name' => $this->org_script_name,
        ];
    }

    protected function _setEmail($value)
    {
        if ($value == '') {
            $value = null;
        }

        return $value;
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "authors/$id";
    }

    public function getLinkedData(): array
    {
        $uri = $this->getUri();
        $entity = [
            'rdf:type' => LinkedData::newResource('crm:E21_Person'),
            'rdfs:label' => $this->author,
            'crm:P1_is_identified_by' => [
                LinkedData::newAppellation("$uri#name", $this->author, 'crm:E82_Actor_Appellation'),
            ],
        ];

        if (!empty($this->org_script_name)) {
            $entity['crm:P1_is_identified_by'][] = LinkedData::newAppellation("$uri#name/original", $this->org_script_name, 'crm:E82_Actor_Appellation');
        }

        if (!empty($this->orcid_id)) {
            $entity['crm:P1_is_identified_by'][] = LinkedData::newAppellation("$uri#orcid", $this->orcid_id, 'crm:E42_Identifier');
            $entity['owl:sameAs'] = LinkedData::newResource("https://orcid.org/$this->orcid_id");
        }

        if ($this->has('biography') && !empty($this->biography)) {
            $entity['crm:P3_has_note'] = Literal::create($this->biography, null, 'rdf:HTML');
        }

        if ($this->has('birth_year')) {
            $entity['crm:P98i_was_born'] = LinkedData::newResource("$uri#birth", [
                'rdf:type' => LinkedData::newResource('crm:E67_Birth'),
                'crm:P4_has_time-span' => LinkedData::newResource("$uri#birth/date", [
                    'rdf:type' => LinkedData::newResource('crm:E52_Time-Span'),
                    'crm:P86_falls_within' => LinkedData::newResource("$uri#birth/year", [
                        'rdf:type' => LinkedData::newResource('crm:E52_Time-Span'),
                        'crm:P170i_time_is_defined_by' => (string)$this->birth_year,
                    ]),
                ]),
            ]);
        }

        if ($this->has('death_year')) {
            $entity['crm:P100i_died_in'] = LinkedData::newResource("$uri#birth", [
                'rdf:type' => LinkedData::newResource('crm:E69_Death'),
                'crm:P4_has_time-span' => LinkedData::newResource("$uri#birth/date", [
                    'rdf:type' => LinkedData::newResource('crm:E52_Time-Span'),
                    'crm:P86_falls_within' => LinkedData::newResource("$uri#birth/year", [
                        'rdf:type' => LinkedData::newResource('crm:E52_Time-Span'),
                        'crm:P170i_time_is_defined_by' => (string)$this->death_year,
                    ]),
                ]),
            ]);
        }

        return $entity;
    }
}
