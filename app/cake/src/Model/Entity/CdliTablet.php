<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CdliTablet Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenDate|null $date
 * @property string|null $name
 * @property string|null $path
 * @property string|null $theme
 * @property string|null $title
 * @property string|null $blurb
 * @property string|null $full_info
 */
class CdliTablet extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'date_display' => true,
        'text_long' => true,
        'text_short' => true,
        'image_filename' => true,
        'title_long' => true,
        'title_short' => true,
        'theme' => true,
        'author_id' => true,
        'author' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'date_display',
        'text_long',
        'text_short',
        'image_filename',
        'title_long',
        'title_short',
        'theme',
        'author_id',
        'author',
    ];
}
