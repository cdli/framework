<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * Artifact Entity
 *
 * @property int $id
 * @property string|null $ark_no
 * @property string|null $cdli_comments
 * @property string|null $composite_no
 * @property string|null $condition_description
 * @property string|null $designation
 * @property string|null $elevation
 * @property string|null $excavation_no
 * @property string|null $findspot_comments
 * @property string|null $findspot_square
 * @property string|null $museum_no
 * @property string|null $artifact_preservation
 * @property bool|null $is_public
 * @property bool|null $is_atf_public
 * @property bool|null $are_images_public
 * @property string|null $seal_no
 * @property string|null $seal_information
 * @property string|null $stratigraphic_level
 * @property string|null $surface_preservation
 * @property string|null $thickness
 * @property string|null $height
 * @property string|null $width
 * @property string|null $weight
 * @property int|null $provenience_id
 * @property int|null $period_id
 * @property bool|null $is_provenience_uncertain
 * @property bool|null $is_period_uncertain
 * @property int|null $artifact_type_id
 * @property string|null $accession_no
 * @property string|null $alternative_years
 * @property string|null $period_comments
 * @property string|null $provenience_comments
 * @property string|null $artifact_type_comments
 * @property bool|null $is_school_text
 * @property int|null $written_in
 * @property bool|null $is_artifact_type_uncertain
 * @property int|null $archive_id
 * @property string|null $dates_referenced
 * @property string|null $accounting_period
 * @property string|null $artifact_comments
 * @property int|null $created_by
 * @property bool $retired
 * @property bool $has_fragments
 * @property int $is_artifact_fake
 *
 * @property \App\Model\Entity\Provenience|null $provenience
 * @property \App\Model\Entity\Provenience|null $origin
 * @property \App\Model\Entity\Period|null $period
 * @property \App\Model\Entity\ArtifactType|null $artifact_type
 * @property \App\Model\Entity\Archive|null $archive
 * @property \App\Model\Entity\ArtifactsShadow[]|null $artifacts_shadow
 * @property \App\Model\Entity\ArtifactsMaterial[]|null $artifacts_materials
 * @property \App\Model\Entity\ArtifactsExternalResource[]|null $artifacts_external_resources
 * @property \App\Model\Entity\ArtifactsUpdate[]|null $artifacts_updates
 * @property \App\Model\Entity\Inscription|null $inscription
 * @property \App\Model\Entity\Posting[]|null $postings
 * @property \App\Model\Entity\RetiredArtifact[]|null $retired_artifacts
 * @property \App\Model\Entity\Collection[]|null $collections
 * @property \App\Model\Entity\Date[]|null $dates
 * @property \App\Model\Entity\ExternalResource[]|null $external_resources
 * @property \App\Model\Entity\Genre[]|null $genres
 * @property \App\Model\Entity\Language[]|null $languages
 * @property \App\Model\Entity\Material[]|null $materials
 * @property \App\Model\Entity\MaterialAspect[]|null $material_aspects
 * @property \App\Model\Entity\MaterialColor[]|null $material_colors
 * @property \App\Model\Entity\Publication[]|null $publications
 * @property \App\Model\Entity\Artifact[]|null $witnesses
 * @property \App\Model\Entity\Artifact[]|null $composites
 * @property \App\Model\Entity\Artifact[]|null $impressions
 * @property \App\Model\Entity\Artifact[]|null $seals
 */
class Artifact extends Entity implements LinkedDataInterface
{
    use TableExportTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'ark_no' => true,
        'cdli_comments' => true,
        'composite_no' => true,
        'condition_description' => true,
        'designation' => true,
        'elevation' => true,
        'excavation_no' => true,
        'findspot_comments' => true,
        'findspot_square' => true,
        'museum_no' => true,
        'artifact_preservation' => true,
        'is_public' => true,
        'is_atf_public' => true,
        'are_images_public' => true,
        'seal_no' => true,
        'seal_information' => true,
        'stratigraphic_level' => true,
        'surface_preservation' => true,
        'thickness' => true,
        'height' => true,
        'width' => true,
        'weight' => true,
        'provenience_id' => true,
        'period_id' => true,
        'is_provenience_uncertain' => true,
        'is_period_uncertain' => true,
        'artifact_type_id' => true,
        'accession_no' => true,
        'alternative_years' => true,
        'period_comments' => true,
        'provenience_comments' => true,
        'artifact_type_comments' => true,
        'is_school_text' => true,
        'written_in' => true,
        'is_artifact_type_uncertain' => true,
        'archive_id' => true,
        'dates_referenced' => true,
        'accounting_period' => true,
        'artifact_comments' => true,
        'created_by' => true,
        'retired' => true,
        'has_fragments' => true,
        'is_artifact_fake' => true,
        'provenience' => true,
        'origin' => true,
        'period' => true,
        'artifact_type' => true,
        'archive' => true,
        'artifacts_shadow' => true,
        'artifacts_external_resources' => true,
        'artifacts_materials' => true,
        'artifacts_updates' => true,
        'inscription' => true,
        'postings' => true,
        'retired_artifacts' => true,
        'collections' => true,
        'dates' => true,
        'external_resources' => true,
        'genres' => true,
        'languages' => true,
        'materials' => true,
        'material_aspects' => true,
        'material_colors' => true,
        'publications' => true,
        'witnesses' => true,
        'composites' => true,
        'impressions' => true,
        'seals' => true,
        'redirect_artifact_id' => true,
        'retired_comments' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'ark_no',
        'cdli_comments',
        'composite_no',
        'condition_description',
        'designation',
        'elevation',
        'excavation_no',
        'findspot_comments',
        'findspot_square',
        'museum_no',
        'artifact_preservation',
        'seal_no',
        'seal_information',
        'stratigraphic_level',
        'surface_preservation',
        'thickness',
        'height',
        'width',
        'weight',
        'is_provenience_uncertain',
        'is_period_uncertain',
        'accession_no',
        'alternative_years',
        'period_comments',
        'provenience_comments',
        'artifact_type_comments',
        'is_school_text',
        'written_in',
        'is_artifact_type_uncertain',
        'dates_referenced',
        'accounting_period',
        'artifact_comments',
        'created_by',
        'retired',
        'has_fragments',
        'is_artifact_fake',
        'provenience',
        'origin',
        'period',
        'artifact_type',
        'archive',
        'artifacts_materials',
        'inscription',
        'retired_artifacts',
        'collections',
        'dates',
        'external_resources',
        'genres',
        'languages',
        'materials',
        'material_aspects',
        'material_colors',
        'publications',
        'witnesses',
        'composites',
        'impressions',
        'seals',
        'redirect_artifact_id',
        'retired_comments',
    ];

    public function getFlatValue(string $key)
    {
        switch ($key) {
            case 'artifact_id':
                return $this->serializeValue($this->id);
            case 'dates':
                return $this->serializeValue($this->dates_referenced);

            case 'artifact_type':
                return $this->serializeDisplay($this->artifact_type, 'artifact_type');
            case 'period':
                return $this->serializeDisplay($this->period, 'period');
            case 'provenience':
                return $this->serializeDisplay($this->provenience, 'provenience');
            case 'written_in':
                return $this->serializeDisplay($this->origin, 'provenience');
            case 'archive':
                return $this->serializeDisplay($this->archive, 'archive');
            case 'composites':
                return $this->serializeList($this->composites, 'composite_no');
            case 'seals':
                return $this->serializeList($this->seals, 'seal_no');
            case 'collections':
                return $this->serializeList($this->collections, 'collection');

            // External resources
            case 'external_resources':
                if ($this->has('artifacts_external_resources')) {
                    return $this->serializeListMap(
                        $this->artifacts_external_resources,
                        function ($external_resource) {
                            return $this->serializeValue($external_resource->external_resource->abbrev);
                        }
                    );
                } else {
                    return $this->serializeList($this->external_resources, 'abbrev');
                }
            case 'external_resources_key':
                if ($this->has('artifacts_external_resources')) {
                    return $this->serializeList($this->artifacts_external_resources, 'external_resource_key');
                } else {
                    return $this->serializeListMap(
                        $this->external_resources,
                        function ($external_resource) {
                            return $this->serializeValue($external_resource->_joinData->external_resource_key);
                        }
                    );
                }

            // Genres
            case 'genres':
                return $this->serializeList($this->genres, 'genre');
            case 'genres_comment':
                return $this->serializeListMap(
                    $this->genres,
                    function ($genre) {
                        return $this->serializeValue($genre->_joinData->comments);
                    }
                );
            case 'genres_uncertain':
                return $this->serializeListMap(
                    $this->genres,
                    function ($genre) {
                        return $this->serializeValue($genre->_joinData->is_genre_uncertain);
                    }
                );

            // Languages
            case 'languages':
                return $this->serializeList($this->languages, 'language');
            case 'languages_uncertain':
                return $this->serializeListMap(
                    $this->languages,
                    function ($language) {
                        return $this->serializeValue($language->_joinData->is_language_uncertain);
                    }
                );

            // Materials
            case 'materials':
                return $this->serializeList($this->materials, 'material');
            case 'materials_aspect':
                return $this->serializeListMap(
                    $this->materials,
                    function ($material) {
                        return $this->serializeDisplay($material->_joinData->material_aspect, 'material_aspect');
                    }
                );
            case 'materials_color':
                return $this->serializeListMap(
                    $this->materials,
                    function ($material) {
                        return $this->serializeDisplay($material->_joinData->material_color, 'material_color');
                    }
                );
            case 'materials_uncertain':
                return $this->serializeListMap(
                    $this->materials,
                    function ($material) {
                        return $this->serializeValue($material->_joinData->is_material_uncertain);
                    }
                );

            // Shadows
            case 'shadow_cdli_comments':
                return $this->serializeDisplay($this->artifacts_shadow, 'cdli_comments');
            case 'shadow_collection_location':
                return $this->serializeDisplay($this->artifacts_shadow, 'collection_location');
            case 'shadow_collection_comments':
                return $this->serializeDisplay($this->artifacts_shadow, 'collection_comments');
            case 'shadow_acquisition_history':
                return $this->serializeDisplay($this->artifacts_shadow, 'acquisition_history');

            // Publications
            case 'publications_key':
                return $this->serializeList($this->publications, 'bibtexkey');
            case 'publications_type':
                return $this->serializeListMap(
                    $this->publications,
                    function ($publication) {
                        return $this->serializeValue($publication->_joinData->publication_type);
                    }
                );
            case 'publications_exact_ref':
                return $this->serializeListMap(
                    $this->publications,
                    function ($publication) {
                        return $this->serializeValue($publication->_joinData->exact_reference);
                    }
                );
            case 'publications_comment':
                return $this->serializeListMap(
                    $this->publications,
                    function ($publication) {
                        return $this->serializeValue($publication->_joinData->publication_comments);
                    }
                );

            // Other flat fields have the same field name in the entity
            default:
                return $this->has($key) ? $this->serializeValue($this->get($key)) : null;
        }
    }

    public function getFlatValues(array $keys)
    {
        return array_combine($keys, array_map(function ($key) {
            return $this->getFlatValue($key);
        }, $keys));
    }

    public function getTableRow($admin)
    {
        if ($admin) {
            return $this->getFlatData();
        }

        return $this->getFlatValues(array_keys(array_diff_key(
            self::$flatFields,
            self::$privateFlatFields,
        )));
    }

    public function getFlatData()
    {
        return $this->getFlatValues(array_keys(self::$flatFields));
    }

    public function getCdliNumber()
    {
        return self::formatCdliNumber($this->id);
    }

    public static function formatCdliNumber($id)
    {
        return 'P' . str_pad((string)$id, 6, '0', STR_PAD_LEFT);
    }

    public function getDescription()
    {
        $description = [];

        if (!empty($this->genres)) {
            $description[] = ucfirst(h($this->genres[0]->genre));
        }

        $artifactType = empty($this->artifact_type) ? 'artifact' : h($this->artifact_type->artifact_type);
        $description[] = $description ? $artifactType : ucfirst($artifactType);

        if (!empty($this->written_in)) {
            $description[] = 'written in ' . ucfirst(h($this->origin->provenience));
        }

        if (!empty($this->written_in) && !empty($this->provenience)) {
            $description[] = 'and';
        }

        if (!empty($this->provenience)) {
            $description[] = 'excavated in ' . ucfirst(h($this->provenience->provenience)) . ',';
        }

        if (!empty($this->period)) {
            $description[] = 'dated to the ' . ucfirst(h($this->period->period)) . ' period';
        }

        if (!empty($this->collections)) {
            $description[] = 'and now kept in ' . ucfirst(h($this->collections[0]->collection));
        }

        return implode(' ', $description);
    }

    private static function getSeparatedIdentifiers($node, $ids, $type)
    {
        if (is_null($ids)) {
            return [];
        }

        $ids = explode(' + ', $ids);

        return array_map(function ($id, $i) use ($node, $type) {
            return LinkedData::newAppellation($node . '/' . ($i + 1), $id, $type);
        }, $ids, array_keys($ids));
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : self::formatCdliNumber($id);
    }

    public function getLinkedData(): array
    {
        // TODO
        //   - is_school_text
        //   - is_fake
        //   - destroyed
        //   - anepigraphic
        //   - chemical data

        $uri = $this->getUri();

        return [
            'rdf:type' => empty($this->composite_no)
                ? LinkedData::newResource('crm:E22_Human-Made_Object')
                : LinkedData::newResource('crm:E33_Linguistic_Object'),
            'rdfs:label' => $this->designation,
            'crm:P1_is_identified_by' => array_merge(
                [
                    LinkedData::newAppellation("$uri#identifier/cdli", $this->getCdliNumber(), 'terms/identifier/cdli'),
                    LinkedData::newAppellation("$uri#identifier/composite", $this->composite_no, 'terms/identifier/composite'),
                    LinkedData::newAppellation("$uri#identifier/seal", $this->seal_no, 'terms/identifier/seal'),
                    LinkedData::newAppellation("$uri#identifier/designation", $this->designation, 'terms/identifier/designation'),
                ],
                self::getSeparatedIdentifiers("$uri#identifier/accession", $this->accession_no, 'terms/identifier/accession'),
                self::getSeparatedIdentifiers("$uri#identifier/museum", $this->museum_no, 'terms/identifier/museum'),
                self::getSeparatedIdentifiers("$uri#identifier/excavation", $this->excavation_no, 'terms/identifier/excavation')
            ),
            'crm:P43_has_dimension' => [
                LinkedData::newDimension("$uri#dimension/width", $this->width, 'quantitykind:Width', 'unit:MilliM'),
                LinkedData::newDimension("$uri#dimension/height", $this->height, 'quantitykind:Height', 'unit:MilliM'),
                LinkedData::newDimension("$uri#dimension/thickness", $this->thickness, 'quantitykind:Thickness', 'unit:MilliM'),
                LinkedData::newDimension("$uri#dimension/weight", $this->weight, 'quantitykind:Mass', 'unit:GM'),
            ],
            'crm:P108i_was_produced_by' => !$this->has(['period_id', 'written_in']) ? null : LinkedData::newResource("$uri#production", [
                'rdf:type' => LinkedData::newResource('crm:E12_Production'),
                'crm:P10_falls_within' => LinkedData::newResource(Period::makeUri($this->period_id)),
                'crm:P7_took_place_at' => LinkedData::newResource(Provenience::makeUri($this->written_in)),
            ]),
            'crm:P46i_forms_part_of' => array_map(function ($collection) {
                return LinkedData::newResource($collection->getUri());
            }, $this->collections),
            'crm:P67i_is_referred_to_by' => array_map(function ($publication) {
                // TODO exact reference, comments
                return LinkedData::newResource($publication->getUri());
            }, $this->publications),
            'crm:P44_has_condition' => !$this->has(['artifact_preservation', 'condition_description', 'surface_preservation']) ? null : LinkedData::newResource("$uri#condition", [
                'rdf:type' => LinkedData::newResource('crm:E3_Condition_State'),
                'crm:P5_consists_of' => !$this->has('surface_preservation') ? null : LinkedData::newResource("$uri#condition/surface", [
                    'rdf:type' => LinkedData::newResource('crm:E3_Condition_State'),
                    'rdfs:value' => $this->surface_preservation,
                ]),
                'rdfs:value' => $this->artifact_preservation,
                'crm:P3_has_note' => $this->condition_description,
            ]),
            'crm:P53_has_former_or_current_location' => LinkedData::newResource(Archive::makeUri($this->archive_id)),
            'crm:P2_has_type' => LinkedData::newResource(ArtifactType::makeUri($this->artifact_type_id)),
            'crma:AP21i_is_contained_in' => !$this->has('stratigraphic_level') && !$this->has('findspot_square') && !$this->has('findspot_comments') && !$this->has('provenience_id') ? null : LinkedData::newResource("$uri#findspot/stratigraphy", [
                'rdf:type' => LinkedData::newResource('crma:A2_Stratigraphic_Volume_Unit'),
                'rdfs:label' => $this->stratigraphic_level,
                'crma:AP5i_was_partially_or_totally_removed_by' => !$this->has('findspot_square') && !$this->has('findspot_comments') && !$this->has('provenience_id') ? null : LinkedData::newResource("$uri#findspot/excavation", [
                    'rdf:type' => LinkedData::newResource('crma:A1_Excavation_Process_Unit'),
                    'crm:P7_took_place_at' => LinkedData::newResource("$uri#findspot", [
                        'rdf:type' => LinkedData::newResource('crm:E53_Place'),
                        'rdfs:label' => $this->findspot_square,
                        // TODO elevation
                        'crm:P3_has_note' => $this->findspot_comments,
                        'crm:P89_falls_within' => LinkedData::newResource(Provenience::makeUri($this->provenience_id) . '#location'),
                    ]),
                ]),
            ]),
            'crm:P31i_was_modified_by' => array_map(function ($seal) use ($uri) {
                return LinkedData::newResource("$uri#impression/{$seal->_joinData->id}", [
                    'rdf:type' => LinkedData::newResource('crm:E11_Modification'),
                    'crm:P16_used_specific_object' => LinkedData::newResource($seal->getUri()),
                ]);
            }, $this->seals),
        ];
    }

    public static $flatFields = [
        'artifact_id' => null,
        'museum_no' => null,
        'designation' => null,
        'publications_key' => ['Publications'],
        'publications_type' => ['Publications'],
        'publications_exact_ref' => ['Publications'],
        'publications_comment' => ['Publications'],
        'provenience' => ['Proveniences'],
        'period' => ['Periods'],
        'dates' => null,
        'collections' => ['Collections'],
        'genres' => ['Genres'],
        'materials' => ['Materials'],
        'artifact_type' => ['ArtifactTypes'],
        'height' => null,
        'thickness' => null,
        'width' => null,
        'weight' => null,
        'elevation' => null,
        'excavation_no' => null,
        'findspot_square' => null,
        'findspot_comments' => null,
        'stratigraphic_level' => null,
        'surface_preservation' => null,
        'condition_description' => null,
        'artifact_preservation' => null,
        'languages' => ['Languages'],
        'written_in' => ['Origins'],
        'archive' => ['Archives'],
        'composite_no' => null,
        'composites' => ['Composites'],
        'seal_no' => null,
        'seals' => ['Seals'],
        'accession_no' => null,
        'cdli_comments' => null,
        'artifact_comments' => null,
        'provenience_comments' => null,
        'is_provenience_uncertain' => null,
        'period_comments' => null,
        'is_period_uncertain' => null,
        'artifact_type_comments' => null,
        'is_artifact_type_uncertain' => null,
        'seal_information' => null,
        'is_school_text' => null,
        'alternative_years' => null,
        'is_public' => null,
        'is_atf_public' => null,
        'are_images_public' => null,
        'shadow_cdli_comments' => ['ArtifactsShadow'],
        'shadow_collection_location' => ['ArtifactsShadow'],
        'shadow_collection_comments' => ['ArtifactsShadow'],
        'shadow_acquisition_history' => ['ArtifactsShadow'],
        'external_resources' => ['ExternalResources'],
        'external_resources_key' => ['ExternalResources'],
        'genres_comment' => ['Genres'],
        'genres_uncertain' => ['Genres'],
        'languages_uncertain' => ['Languages'],
        'materials_aspect' => ['Materials'],
        'materials_color' => ['Materials'],
        'materials_uncertain' => ['Materials'],
        'retired' => null,
        'has_fragments' => null,
        'is_artifact_fake' => null,
        'redirect_artifact_id' => null,
        'retired_comments' => null,
    ];

    public static $privateFlatFields = [
        'is_public' => null,
        'is_atf_public' => null,
        'are_images_public' => null,
        'shadow_cdli_comments' => null,
        'shadow_collection_location' => null,
        'shadow_collection_comments' => null,
        'shadow_acquisition_history' => null,
    ];
}
