<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArtifactsDate Entity
 *
 * @property int $id
 * @property int|null $artifact_id
 * @property int|null $date_id
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\Date $date
 */
class ArtifactsDate extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'date_id' => true,
        'artifact' => true,
        'date' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'artifact_id',
        'date_id',
        'artifact',
        'date',
    ];
}
