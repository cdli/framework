<?php
declare(strict_types=1);

namespace App\Model\Entity;

interface UpdateInterface
{
    public function getUpdateEventField(): string;
}
