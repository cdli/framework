<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * EntitiesPublication Entity
 *
 * @property int $id
 * @property int $entity_id
 * @property int $publication_id
 * @property string|null $table_name
 * @property string|null $exact_reference
 * @property string|null $publication_type
 * @property string|null $publication_comments
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\Publication $publication
 */
class EntitiesPublication extends Entity
{
    use TableExportTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'entity_id' => true,
        'publication_id' => true,
        'table_name' => true,
        'exact_reference' => true,
        'publication_type' => true,
        'publication_comments' => true,
        'artifact' => true,
        'artifact_asset' => true,
        'publication' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'entity_id',
        'publication_id',
        'table_name',
        'exact_reference',
        'publication_type',
        'publication_comments',
        'artifact',
        'artifact_asset',
        'publication',
    ];

    protected function _setArtifactId($value)
    {
        // Format for P value input
        return ltrim((string)$value, 'P0');
    }

    public function getTableRow()
    {
        return [
            'entity_id' => $this->entity_id,
            'table_name' => $this->table_name,
            'bibtexkey' => $this->serializeDisplay($this->publication, 'bibtexkey'),
            'exact_reference' => $this->exact_reference,
            'publication_type' => $this->publication_type,
            'publication_comments' => $this->publication_comments,
        ];
    }
}
