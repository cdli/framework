<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * Abbreviation Entity
 *
 * @property int $id
 * @property string $abbreviation
 * @property string|null $fullform
 * @property string|null $type
 * @property int|null $publication_id
 *
 * @property \App\Model\Entity\Publication $publication
 */
class Abbreviation extends Entity implements LinkedDataInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'abbreviation' => true,
        'fullform' => true,
        'type' => true,
        'publication_id' => true,
        'publication' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'abbreviation',
        'fullform',
        'type',
        'publication_id',
        'publication',
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'abbreviation' => $this->abbreviation,
            'fullform' => $this->fullform,
            'type' => $this->type,
            'publication' => $this->has('publication') ? $this->publication->publication : '',
        ];
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "abbreviations/$id";
    }

    public function getLinkedData(): array
    {
        /** @var array $entity */
        $entity = LinkedData::newAppellation($this->getUri(), $this->abbreviation)->properties;

        if ($this->has('publication_id')) {
            $entity['crm:P1i_identifies'] = LinkedData::newResource(Publication::makeUri($this->publication_id) . '#manifestation');
        } elseif ($this->has('fullform')) {
            if ($this->type === 'Book') {
                $type = LinkedData::newResource('frbroo:F19');
            } elseif ($this->type === 'Journal' || $this->type === 'Serial') {
                $type = LinkedData::newResource('frbroo:F18');
            } else {
                // TODO Chronology, Collection siglum
                $type = null;
            }

            $entity['crm:P1i_identifies'] = LinkedData::newResource($this->getUri() . '#concept', [
                'rdf:type' => $type,
                'crm:P1_is_identified_by' => LinkedData::newAppellation($this->getUri() . '#fullform', trim((string)$this->fullform)),
            ]);
        }

        return $entity;
    }
}
