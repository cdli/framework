<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OjsSubmission Entity
 *
 * @property int $submission_id
 * @property string|null $locale
 * @property int $context_id
 * @property int|null $section_id
 * @property int|null $current_publication_id
 * @property \Cake\I18n\FrozenTime|null $date_last_activity
 * @property \Cake\I18n\FrozenTime|null $date_submitted
 * @property \Cake\I18n\FrozenTime|null $last_modified
 * @property int $stage_id
 * @property int $status
 * @property int $submission_progress
 * @property int|null $work_type
 *
 * @property \App\Model\Entity\OjsSection $section
 * @property \App\Model\Entity\OjsReviewAssignment[] $review_assignments
 * @property \App\Model\Entity\Query[] $queries
 */
class OjsSubmission extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'locale' => true,
        'context_id' => true,
        'section_id' => true,
        'current_publication_id' => true,
        'date_last_activity' => true,
        'date_submitted' => true,
        'last_modified' => true,
        'stage_id' => true,
        'status' => true,
        'submission_progress' => true,
        'work_type' => true,
        'section' => true,
        'review_assignments' => true,
        'queries' => true,
    ];
}
