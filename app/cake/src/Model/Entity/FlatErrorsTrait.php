<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Model\Entity;

use Cake\Utility\Inflector;

trait FlatErrorsTrait
{
    public function getFlatErrors()
    {
        $flatErrors = [];
        foreach (self::flattenErrors($this->getErrors()) as $error) {
            $flatErrors[implode(' ', $error[0])] = $error[1];
        }

        return $flatErrors;
    }

    private static function flattenErrors($errors)
    {
        $flatErrors = [];

        foreach ($errors as $key => $error) {
            if (is_array($error)) {
                foreach (self::flattenErrors($error) as $flatError) {
                    if ($key !== '_joinData' && $key !== $flatError[0][0]) {
                        $field = self::formatErrorKey(Inflector::singularize($key));
                        $flatError[0] = array_merge([$field], $flatError[0]);
                    }
                    $flatErrors[] = [$flatError[0], $flatError[1]];
                }
            } else {
                $flatErrors[] = [[self::formatErrorKey($key)], $error];
            }
        }

        return $flatErrors;
    }

    private static function formatErrorKey($key)
    {
        if (is_numeric($key)) {
            return '#' . ($key + 1);
        } else {
            return $key;
        }
    }
}
