<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * @property int $id
 * @property string|null $bibtexkey
 * @property string|null $year
 * @property int|null $entry_type_id
 * @property string|null $address
 * @property string|null $book_title
 * @property string|null $chapter
 * @property string|null $edition
 * @property string|null $how_published
 * @property string|null $institution
 * @property int|null $journal_id
 * @property string|null $month
 * @property string|null $note
 * @property string|null $number
 * @property string|null $organization
 * @property string|null $pages
 * @property string|null $publisher
 * @property string|null $school
 * @property string|null $title
 * @property string|null $volume
 * @property string|null $series
 * @property int|null $oclc
 * @property string|null $designation
 *
 * @property \App\Model\Entity\EntryType $entry_type
 * @property \App\Model\Entity\Journal $journal
 * @property \App\Model\Entity\Author[] $authors
 * @property \App\Model\Entity\Author[] $editors
 * @property \App\Model\Entity\Artifact[] $artifacts
 * @property \App\Model\Entity\Provenience[] $proveniences
 * @property \App\Model\Entity\Abbreviation[] $abbreviations
 * @property \App\Model\Entity\Article[] $articles
 * @property \App\Model\Entity\UpdateEvent[] $update_events
 */
class Publication extends Entity implements LinkedDataInterface
{
    use TableExportTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'bibtexkey' => true,
        'year' => true,
        'entry_type_id' => true,
        'address' => true,
        'book_title' => true,
        'chapter' => true,
        'edition' => true,
        'how_published' => true,
        'institution' => true,
        'journal_id' => true,
        'month' => true,
        'note' => true,
        'number' => true,
        'organization' => true,
        'pages' => true,
        'publisher' => true,
        'school' => true,
        'title' => true,
        'volume' => true,
        'series' => true,
        'oclc' => true,
        'designation' => true,

        'entry_type' => true,
        'journal' => true,

        'abbreviations' => false,
        'articles' => true,
        'authors' => true,
        'editors' => true,
        'external_resources' => true,

        'artifacts' => false,
        'artifact_assets' => false,
        'proveniences' => false,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'bibtexkey',
        'year',
        'entry_type_id',
        'address',
        'book_title',
        'chapter',
        'edition',
        'how_published',
        'institution',
        'journal_id',
        'month',
        'note',
        'number',
        'organization',
        'pages',
        'publisher',
        'school',
        'title',
        'volume',
        'series',
        'oclc',
        'designation',

        'entry_type',
        'journal',

        'abbreviations',
        'articles',
        'authors',
        'editors',
        'external_resources',

        'artifacts',
        'artifact_assets',
        'proveniences',
    ];

    /**
     * Mapping of CSV to data.
     *
     * @var array
     */
    public static $csvSchema = [
        'bibtexkey' => [],
        'authors' => ['table' => 'Authors', 'targetKey' => 'author', 'joinTable' => 'AuthorsPublications', 'joinData' => []],
        'year' => [],
        'entry_type' => ['table' => 'EntryTypes', 'targetKey' => 'label'],
        'address' => [],
        'book_title' => [],
        'chapter' => [],
        'edition' => [],
        'editors' => ['table' => 'Authors', 'targetKey' => 'author', 'joinTable' => 'EditorsPublications', 'joinData' => []],
        'how_published' => [],
        'institution' => [],
        'journal' => ['table' => 'Journals', 'targetKey' => 'journal'],
        'month' => [],
        'note' => [],
        'number' => [],
        'organization' => [],
        'pages' => [],
        'publisher' => [],
        'school' => [],
        'title' => [],
        'volume' => [],
        'series' => [],
        'designation' => [],
        'external_resources' => [
            'table' => 'ExternalResources',
            'targetKey' => 'abbrev',
            'key' => 'external_resources',
            'joinTable' => 'EntitiesExternalResources',
            'joinData' => [
                'external_resources_key' => ['key' => 'external_resource_key'],
            ],
        ],
    ];

    public static array $topicAssociations = [
        // Artifacts are treated differently
        'artifact_assets' => 'ArtifactAssets',
        'proveniences' => 'Proveniences',
    ];

    public function getTableRow()
    {
        return [
            'bibtexkey' => $this->bibtexkey,
            'authors' => $this->serializeList($this->authors, 'author'),
            'year' => $this->year,
            'entry_type' => $this->serializeDisplay($this->entry_type, 'label'),
            'address' => $this->address,
            'annote' => '',
            'book_title' => $this->book_title,
            'chapter' => $this->chapter,
            'crossref' => '',
            'edition' => $this->edition,
            'editors' => $this->serializeList($this->editors, 'author'),
            'how_published' => $this->how_published,
            'institution' => $this->institution,
            'journal' => $this->serializeDisplay($this->journal, 'journal'),
            'month' => $this->month,
            'note' => $this->note,
            'number' => $this->number,
            'organization' => $this->organization,
            'pages' => $this->pages,
            'publisher' => $this->publisher,
            'school' => $this->school,
            'title' => $this->title,
            'volume' => $this->volume,
            'publication_history' => '',
            'series' => $this->series,
            'designation' => $this->designation,
        ];
    }

    public function getTopics(): array
    {
        $topics = [];

        foreach (self::$topicAssociations as $topic => $association) {
            if ($this->has($topic)) {
                foreach ($this->get($topic) as $entity) {
                    $topics[$topic][] = $entity;
                }
            }
        }

        return $topics;
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): string
    {
        return "publications/$id";
    }

    public function getLinkedData(): array
    {
        if (!empty($this->publisher)) {
            $publisher = $this->publisher;
        } elseif (!empty($this->institution)) {
            $publisher = $this->institution;
        } elseif (!empty($this->organization)) {
            $publisher = $this->organization;
        } elseif (!empty($this->school)) {
            $publisher = $this->school;
        }

        if (!empty($this->year)) {
            $date = empty($this->month) ? $this->year : $this->month . ' ' . $this->year;
        }

        if (!empty($this->entry_type)) {
            $entryType = 'bibtex:' . ucfirst($this->entry_type->label);
        } else {
            $entryType = 'bibtex:Entry';
        }

        $uri = $this->getUri();

        if ($this->has('journal')) {
            $container = LinkedData::newResource("$uri/expression/container", [
                'rdf:type' => LinkedData::newResource('frbroo:F2_Expression'),
                'frbroo:R3i' => LinkedData::newResource(Journal::makeUri($this->journal_id)),
            ]);

            if (!empty($this->volume)) {
                $container = LinkedData::newResource("$uri/expression/container", [
                    'rdf:type' => LinkedData::newResource('frbroo:F2_Expression'),
                    'frbroo:R5i' => $container,
                    'rdfs:label' => "Volume $this->volume",
                ]);
            }
            if (!empty($this->number)) {
                $container = LinkedData::newResource("$uri/expression/container", [
                    'rdf:type' => LinkedData::newResource('frbroo:F2_Expression'),
                    'frbroo:R5i' => $container,
                    'rdfs:label' => "Issue $this->number",
                ]);
            }
        } else {
            if (!empty($this->series)) {
                $container = LinkedData::newResource("$uri/expression/container", [
                    'rdf:type' => LinkedData::newResource('frbroo:F2_Expression'),
                    'frbroo:R3i' => LinkedData::newResource("$uri/container", [
                        'rdf:type' => LinkedData::newResource('frbroo:F18_Serial_Work'),
                        'rdfs:label' => $this->series,
                    ]),
                ]);
            } else {
                $container = null;
            }

            if (!empty($this->booktitle)) {
                $container = LinkedData::newResource("$uri/expression/container", [
                    'rdf:type' => LinkedData::newResource('frbroo:F2_Expression'),
                    'rdfs:label' => $this->booktitle,
                    'frbroo:R5i' => $container,
                ]);
            }
        }

        $itemCreation = LinkedData::newResource("$uri#item/creation", [
            'rdf:type' => LinkedData::newResource('frbroo:F32_Carrier_Production_Event'),
            // publisher
            'frbroo:P14_carried_out_by' => (empty($publisher) ? null : LinkedData::newResource("$uri/item/publisher", [
                'rdf:type' => LinkedData::newResource('crm:E39_Actor'),
                'rdfs:label' => $publisher,
                // address
                'crm:P74_has_current_or_former_residence' => (empty($this->address) ? null : LinkedData::newResource("$uri/item/publisher/place", [
                    'rdf:type' => LinkedData::newResource('crm:E53_Place'),
                    'rdfs:label' => $this->address,
                ])),
            ])),
            // year & month
            'crm:P4_has_time-span' => (empty($date) ? null : LinkedData::newResource("$uri#item/creation/time-span", [
                'rdf:type' => LinkedData::newResource('crm:E52_Time-Span'),
                'crm:P170i_time_is_defined_by' => $date,
            ])),
        ]);

        $manifestation = LinkedData::newResource("$uri#manifestation", [
            'rdf:type' => LinkedData::newResource('frbroo:F3_Manifestation'),
            'rdfs:label' => [
                // designation
                empty($this->designation) ? null : LinkedData::newAppellation("$uri/manifestation/designation", $this->designation),
                // oclc
                empty($this->oclc) ? null : LinkedData::newAppellation("$uri/manifestation/oclc", (string)$this->oclc),
            ],
            // type
            'crm:P2_has_type' => LinkedData::newResource($entryType),
            // itemCreation
            'frbroo:R27i' => $itemCreation,
        ]);

        $entity = [
            'rdf:type' => LinkedData::newResource('frbroo:F1_Work'),
            'frbroo:R3' => LinkedData::newResource("$uri#expression"),
            // expressionCreation
            'frbroo:R19i' => LinkedData::newResource("$uri#expression/creation", [
                'rdf:type' => LinkedData::newResource('frbroo:F28_Expression_Creation'),
                // author
                'frbroo:P14_carried_out_by' => array_map(function ($author) {
                    return LinkedData::newResource(Author::makeUri($author->id));
                }, $this->authors),
                // expression
                'frbroo:R17' => LinkedData::newResource("$uri#expression", [
                    'rdf:type' => LinkedData::newResource('frbroo:F2_Expression'),
                    'frbroo:R4i' => LinkedData::newResource("$uri#manifestation"),
                    // title
                    'rdfs:label' => empty($this->title) ? null : $this->title,
                    // journal/volume/number or series/booktitle
                    'frbroo:R5i' => $container ?? null,
                    // manifestationCreation
                    'frbroo:R19i' => LinkedData::newResource("$uri#manifestation/creation", [
                        'rdf:type' => LinkedData::newResource('frbroo:F30_Manifestation_Creation'),
                        // editor
                        'frbroo:P15_was_influenced_by' => array_map(function ($author) {
                            return LinkedData::newResource(Author::makeUri($author->id));
                        }, $this->editors),
                        // manifestation
                        'frbroo:R24' => $manifestation,
                    ]),
                ]),
            ]),
        ];

        $record = [
            'rdf:type' => [
                LinkedData::newResource('crm:E89_Propositional_Object'),
                LinkedData::newResource($entryType),
            ],
            'crm:P3_has_note' => $this->note,
            'crm:P1_is_identified_by' => empty($this->title) ? null : $this->title,

            'bibtex:hasKey' => $this->bibtexkey,
            'bibtex:hasAddress' => $this->address,
            'bibtex:hasAuthor' => array_column($this->authors, 'author'),
            'bibtex:hasBooktitle' => $this->book_title,
            'bibtex:hasChapter' => $this->chapter,
            'bibtex:hasEdition' => $this->edition,
            'bibtex:hasEditor' => array_column($this->editors, 'editors'),
            'bibtex:howPublished' => $this->how_published,
            'bibtex:hasInstitution' => $this->institution,
            'bibtex:hasJournal' => $this->serializeDisplay($this->journal, 'journal'),
            'bibtex:hasMonth' => $this->month,
            'bibtex:hasNote' => $this->note,
            'bibtex:hasNumber' => $this->number,
            'bibtex:hasOrganization' => $this->organization,
            'bibtex:hasPages' => $this->pages,
            'bibtex:hasPublisher' => $this->publisher,
            'bibtex:hasSchool' => $this->school,
            'bibtex:hasSeries' => $this->series,
            'bibtex:hasTitle' => $this->title,
            'bibtex:hasType' => $this->type,
            'bibtex:hasVolume' => $this->volume,
            'bibtex:hasYear' => $this->year,
        ];

        foreach ($record as $key => $value) {
            if (empty($value)) {
                unset($record[$key]);
            }
        }

        $entity['crm:P129i_is_subject_of'] = LinkedData::newResource("$uri#record", $record);

        return $entity;
    }
}
