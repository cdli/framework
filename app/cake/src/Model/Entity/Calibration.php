<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Calibration Entity
 *
 * @property int $id
 * @property string $human_number
 * @property string $content
 *
 * @property \App\Model\Entity\ChemicalData[] $chemical_data
 */
class Calibration extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'human_number' => true,
        'content' => true,
        'chemical_data' => true,
    ];
}
