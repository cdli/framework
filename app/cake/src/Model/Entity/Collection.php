<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;
use EasyRdf\Literal;

/**
 * Collection Entity
 *
 * @property int $id
 * @property string $collection
 * @property string|null $collection_url
 * @property string|null $slug
 * @property string|null $description
 * @property bool $is_pinned
 * @property string|null $collection_actor
 * @property string|null $collection_holding
 * @property string|null $collection_actor_status
 * @property string|null $collection_holding_status
 * @property bool $collection_is_private
 * @property string|null $country_iso
 * @property string|null $region_gadm
 * @property string|null $district_gadm
 * @property float|null $location_longitude_wgs1984
 * @property float|null $location_latitude_wgs1984
 * @property int|null $location_accuracy
 * @property int $glow_id
 * @property string|null $license_id
 * @property string|null $license_attribution
 * @property \App\Model\Entity\Artifact[] $artifacts
 * @property \App\Model\Entity\EntitiesName[] $entities_names
 * @property \App\Model\Entity\ExternalResource[] $external_resources
 */
class Collection extends Entity implements LinkedDataInterface
{
    use TableExportTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'collection' => true,
        'collection_url' => true,
        'slug' => true,
        'description' => true,
        'is_pinned' => true,
        'collection_actor' => true,
        'collection_holding' => true,
        'collection_actor_status' => true,
        'collection_holding_status' => true,
        'collection_is_private' => true,
        'country_iso' => true,
        'region_gadm' => true,
        'district_gadm' => true,
        'location_longitude_wgs1984' => true,
        'location_latitude_wgs1984' => true,
        'location_accuracy' => true,
        'glow_id' => true,
        'license_id' => true,
        'license_attribution' => true,
        'license_comment' => true,
        'entities_names' => true,
        'external_resources' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'collection',
        'collection_url',
        'slug',
        'description',
        'is_pinned',
        'collection_actor',
        'collection_holding',
        'collection_actor_status',
        'collection_holding_status',
        'collection_is_private',
        'country_iso',
        'region_gadm',
        'district_gadm',
        'location_longitude_wgs1984',
        'location_latitude_wgs1984',
        'location_accuracy',
        'glow_id',
        'license_id',
        'license_attribution',
        'license_comment',
        'entities_names',
        'external_resources',
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'collection' => $this->collection,
            'collection_url' => $this->collection_url,
            'slug' => $this->slug,
            'description' => $this->description,
            'is_pinned' => $this->is_pinned,
            'collection_actor' => $this->collection_actor,
            'collection_holding' => $this->collection_holding,
            'collection_actor_status' => $this->collection_actor_status,
            'collection_holding_status' => $this->collection_holding_status,
            'collection_is_private' => $this->collection_is_private,
            'country_iso' => $this->country_iso,
            'region_gadm' => $this->region_gadm,
            'district_gadm' => $this->district_gadm,
            'location_longitude_wgs1984' => $this->location_longitude_wgs1984,
            'location_latitude_wgs1984' => $this->location_latitude_wgs1984,
            'location_accuracy' => $this->location_accuracy,
            'license_id' => $this->license_id,
            'license_attribution' => $this->license_attribution,

            'names' => $this->serializeEntitiesNames($this->entities_names),
            'external_resources' => $this->serializeExternalResources($this->external_resources),
        ];
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "collections/$id";
    }

    public function getLinkedData(): array
    {
        $uri = $this->getUri();
        $entity = [
            'rdf:type' => LinkedData::newResource('crm:E78_Curated_Holding'),
            'rdfs:label' => $this->collection,
        ];

        if ($this->has('collection_url')) {
            $entity['crm:P1_is_identified_by'][] = LinkedData::newAppellation("$uri#url", $this->collection_url);
        }

        if ($this->has('description') && !empty($this->description)) {
            $entity['crm:P3_has_note'] = Literal::create($this->description, null, 'rdf:HTML');
        }

        if ($this->has('location_longitude_wgs1984') || $this->has('location_latitude_wgs1984')) {
            $entity['crm:P53_has_former_or_current_location'] = LinkedData::newResource("$uri#location", [
                'rdf:type' => LinkedData::newResource('crm:E53_Place'),
                'geo:hasCentroid' => LinkedData::newGeometry("$uri#location/centroid", [
                    'type' => 'Point',
                    'coordinates' => [$this->location_longitude_wgs1984, $this->location_latitude_wgs1984],
                ]),
            ]);
        }

        // TODO GADM

        return $entity;
    }
}
