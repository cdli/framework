<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;
use Cake\Utility\Inflector;
use EasyRdf\Literal;

/**
 * Entities Name Entity
 *
 * @property int $id
 * @property string $entity_table
 * @property int $entity_id
 * @property string $language_ietf
 * @property string $name
 *
 * @property \App\Model\Entity\Location $location
 */
class EntitiesName extends Entity implements LinkedDataInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'entity_table' => true,
        'language_ietf' => true,
        'name' => true,
        'location' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'entity_table',
        'entity_id',
        'language_ietf',
        'name',
        'location',
    ];

    /**
     * @return ?string
     */
    public function getSourceUri(): ?string
    {
        $sourceClass = __NAMESPACE__ . '\\' . Inflector::singularize(Inflector::camelize($this->entity_table));

        if (class_exists($sourceClass) && method_exists($sourceClass, 'makeUri')) {
            return $sourceClass::makeUri($this->entity_id);
        }

        return null;
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "entities-names/$id";
    }

    public function getLinkedData(): array
    {
        return [
            'rdf:type' => LinkedData::newResource('crm:E41_Appellation'),
            'crm:P190_has_symbolic_content' => Literal::create($this->name, $this->language_ietf),
            'crm:P1i_identifies' => LinkedData::newResource($this->getSourceUri()),
        ];
    }
}
