<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\Utility\Inflector;

/**
 * @property int $id
 * @property string $entity_table
 * @property int $entity_id
 * @property string $entity_update
 *
 * @property \App\Model\Entity\UpdateEvent $update_event
 */
class EntitiesUpdate extends Entity implements UpdateInterface
{
    use LocatorAwareTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'entity_update' => true,
        'update_event' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'entity_table',
        'entity_id',
        'entity_update',
        'update_event',
    ];

    public static array $dependentAssociations = [
        'collections' => [
            'ExternalResources' => ['fields' => ['id']],
            'EntitiesNames' => [],
        ],
        'locations' => [
            'ExternalResources' => ['fields' => ['id']],
            'EntitiesNames' => [],
        ],
        'places' => [
            'ExternalResources' => ['fields' => ['id']],
        ],
        'proveniences' => [
            'Publications' => ['fields' => ['id']],
        ],
        'publications' => [
            'Authors' => ['fields' => ['id']],
            'Editors' => ['fields' => ['id']],
            'ExternalResources' => ['fields' => ['id']],
        ],
    ];

    /**
     * @todo TODO move to table
     * @return \Cake\ORM\Entity
     */
    public function apply()
    {
        $table = $this->getTable();
        $entity = $this->getEntity();

        $table->loadInto($entity, self::$dependentAssociations[$this->entity_table]);
        $update = self::_mergeArray($entity->toArray(), $this->getUpdate());
        $table->patchEntity($entity, $update);

        return $entity;
    }

    /**
     * @param array $a
     * @param array $b
     * @return array
     */
    private static function _mergeArray(array $a, array $b)
    {
        $c = [];
        foreach ($a as $key => $value) {
            $c[$key] = $value;
        }
        foreach ($b as $key => $value) {
            if (isset($c[$key]) && is_array($value)) {
                $c[$key] = self::_mergeArray($c[$key] ?? [], $value);
            } else {
                $c[$key] = $value;
            }
        }

        return array_filter($c, function ($value) {
            return !is_null($value);
        });
    }

    /**
     * @todo TODO move to table
     * @return \Cake\ORM\Entity
     */
    public function getEntity()
    {
        $table = $this->getTable();

        return $this->has('entity_id') ? $table->get($this->entity_id) : $table->newEmptyEntity();
    }

    /**
     * @return \Cake\ORM\Table
     */
    public function getTable()
    {
        $tableLocator = $this->getTableLocator();
        $this->_tableLocator = null;

        return $tableLocator->get(Inflector::camelize($this->entity_table));
    }

    /**
     * @return array
     */
    public function getUpdate()
    {
        return json_decode($this->entity_update, true);
    }

    /**
     * @return string
     */
    public function getUpdateEventField(): string
    {
        return 'entities_updates';
    }
}
