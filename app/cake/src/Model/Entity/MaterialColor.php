<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * MaterialColor Entity
 *
 * @property int $id
 * @property string|null $material_color
 *
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class MaterialColor extends Entity implements LinkedDataInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'material_color' => true,
        'artifacts' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'material_color',
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'material_color' => $this->material_color,
        ];
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "material-colors/$id";
    }

    public function getLinkedData(): array
    {
        return [
            'rdf:type' => LinkedData::newResource('terms/type/color'),
            'rdfs:label' => $this->material_color,
        ];
    }
}
