<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArtifactAssetAnnotationBody Entity
 *
 * @property int $id
 * @property int $annotation_id
 * @property string|null $type
 * @property string|null $value
 * @property string|null $source
 * @property string|null $purpose
 *
 * @property \App\Model\Entity\Annotation $annotation
 */
class ArtifactAssetAnnotationBody extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'type' => true,
        'value' => true,
        'source' => true,
        'purpose' => true,
    ];

    public function getW3cJson()
    {
        $body = [
            'type' => $this->type,
            'purpose' => $this->purpose,
        ];

        if ($this->type === 'SpecificResource') {
            $body['source'] = ['id' => $this->source, 'label' => $this->value];
        } else {
            $body['value'] = $this->value;
        }

        return $body;
    }
}
