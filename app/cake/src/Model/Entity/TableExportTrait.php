<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Model\Entity;

trait TableExportTrait
{
    public function serializeValue($variable)
    {
        return is_bool($variable) ? (string)(int)$variable : (string)$variable;
    }

    public function serializeDisplay($variable, $display)
    {
        return empty($variable) ? null : $this->serializeValue($variable[$display]);
    }

    public function serializeList($list, $key)
    {
        return $this->serializeListMap($list, function ($item) use ($key) {
            return $this->serializeValue($item[$key]);
        });
    }

    public function serializeExternalResources($list)
    {
        return $this->serializeListMap($list, function ($resource) {
            if ($resource->base_url) {
                return $resource->base_url . $resource->_joinData->external_resource_key;
            } elseif ($resource->_joinData->has('external_resource_key')) {
                return $resource->abbrev . ': ' . $resource->_joinData->external_resource_key;
            }
        });
    }

    public function serializeEntitiesNames($list)
    {
        return $this->serializeListMap($list, function ($name) {
            return $name->name . ' [' . $name->language_ietf . ']';
        });
    }

    public function serializeListMap($list, $map)
    {
        if (empty($list)) {
            return null;
        }

        $list = array_map($map, $list);

        foreach ($list as $key => $item) {
            if (str_contains($item, ';')) {
                $list[$key] = '"' . str_replace('"', '""', $item) . '"';
            }
        }

        return implode('; ', $list);
    }
}
