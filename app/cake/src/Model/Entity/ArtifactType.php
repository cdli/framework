<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * ArtifactType Entity
 *
 * @property int $id
 * @property string $artifact_type
 * @property int|null $parent_id
 * @property string $description
 *
 * @property \App\Model\Entity\ArtifactType $parent_artifact_type
 * @property \App\Model\Entity\ArtifactType[] $child_artifact_types
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class ArtifactType extends Entity implements LinkedDataInterface
{
    use TableExportTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_type' => true,
        'parent_id' => true,
        'parent_artifact_type' => true,
        'child_artifact_types' => true,
        'artifacts' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'artifact_type',
        'parent_id',
        'parent_artifact_type',
        'child_artifact_types',
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'artifact_type' => $this->artifact_type,
            'parent_artifact_type' => $this->serializeDisplay($this->parent_artifact_type, 'artifact_type'),
            'description' => $this->description,
        ];
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "artifact-types/$id";
    }

    public function getLinkedData(): array
    {
        return [
            'rdf:type' => [
                LinkedData::newResource('terms/type/artifact_type'),
                LinkedData::newResource('crm:E55_Type'),
            ],
            'rdfs:label' => $this->artifact_type,
            'crm:P127_has_broader_term' => LinkedData::newResource(self::makeUri($this->parent_id)),
        ];
    }
}
