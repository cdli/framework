<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArticlesAuthor Entity
 *
 * @property int $id
 * @property int|null $article_id
 * @property int|null $author_id
 * @property int $sequence
 *
 * @property \App\Model\Entity\Article $article
 * @property \App\Model\Entity\Author $author
 */
class ArticlesAuthor extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'article_id' => true,
        'author_id' => true,
        'sequence' => true,
        'email' => true,
        'institution' => true,
        'article' => true,
        'author' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'article_id',
        'author_id',
        'sequence',
        'email',
        'institution',
        'article',
        'author',
    ];
}
