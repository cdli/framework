<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * Ruler Entity
 *
 * @property int $id
 * @property int|null $sequence
 * @property string|null $ruler
 * @property int|null $period_id
 * @property int|null $dynasty_id
 *
 * @property \App\Model\Entity\Period $period
 * @property \App\Model\Entity\Dynasty $dynasty
 * @property \App\Model\Entity\Date[] $dates
 */
class Ruler extends Entity implements LinkedDataInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sequence' => true,
        'ruler' => true,
        'period_id' => true,
        'dynasty_id' => true,
        'period' => true,
        'dynasty' => true,
        'dates' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'sequence',
        'ruler',
        'period_id',
        'dynasty_id',
        'period',
        'dynasty',
        'dates',
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'sequence' => $this->sequence,
            'ruler' => $this->ruler,
            'period' => $this->has('period') ? $this->period->period : '',
            'dynasty' => $this->has('dynasty') ? $this->dynasty->dynasty : '',
        ];
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "rulers/$id";
    }

    public function getLinkedData(): array
    {
        return [
            'rdf:type' => LinkedData::newResource('crm:E21_Person'),
            'rdfs:label' => $this->ruler,
            'crm:P14i_performed' => LinkedData::newResource($this->getUri() . '#rule', [
                'rdf:type' => LinkedData::newResource('crm:E7_Activity'),
                'crm:P10_falls_within' => [
                    LinkedData::newResource(Dynasty::makeUri($this->dynasty_id)),
                    LinkedData::newResource(Period::makeUri($this->period_id)),
                ],
                'crm:P4_has_time-span' => LinkedData::newResource($this->getUri() . '#rule/time-span', [
                    'rdf:type' => LinkedData::newResource('crm:E52_Time-Span'),
                ]),
            ]),
        ];
    }
}
