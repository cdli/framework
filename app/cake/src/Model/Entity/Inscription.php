<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\CdliProcessor\Check;
use App\Utility\CdliProcessor\Convert;
use App\Utility\CdliProcessor\TokenList;
use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * Inscription Entity
 *
 * @property int $id
 * @property int|null $artifact_id
 * @property string|null $atf
 * @property string|null $jtf
 * @property string|null $transliteration
 * @property string|null $transliteration_clean
 * @property string|null $transliteration_sign_names
 * @property string|null $transliteration_for_search
 * @property string|null $annotation
 * @property bool|null $is_atf2conll_diff_resolved
 * @property string|null $comments
 * @property string|null $structure
 * @property string|null $translation
 * @property string|null $transcription
 * @property int|null $update_event_id
 * @property string|null $inscription_comments
 * @property bool $is_latest
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\UpdateEvent $update_event
 */
class Inscription extends Entity implements UpdateInterface, LinkedDataInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'atf' => true,
        'jtf' => true,
        'transliteration' => true,
        'transliteration_clean' => true,
        'transliteration_sign_names' => true,
        'transliteration_for_search' => true,
        'annotation' => true,
        'is_atf2conll_diff_resolved' => true,
        'comments' => true,
        'structure' => true,
        'translation' => true,
        'transcription' => true,
        'update_event_id' => true,
        'inscription_comments' => true,
        'is_latest' => true,
        'artifact' => true,
        'update_event' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'artifact_id',
        'atf',
        'jtf',
        'transliteration',
        'transliteration_clean',
        'transliteration_sign_names',
        'transliteration_for_search',
        'annotation',
        'is_atf2conll_diff_resolved',
        'comments',
        'structure',
        'translation',
        'transcription',
        'inscription_comments',
        'is_latest',
        'artifact',
    ];

    public function isFieldDifferent($other, $field)
    {
        return !$this->has($field) || !$other->has($field) || $this->get($field) !== $other->get($field);
    }

    /**
     * @var array|null
     */
    protected $atfWarnings = null;

    public function setAtfWarnings()
    {
        if (is_null($this->atfWarnings)) {
            $atfWarnings = [];

            $jtf = Convert::cAtfToJtf($this->atf);
            if (!empty($jtf->errors)) {
                foreach ($jtf->errors as $error) {
                    if (property_exists($error, 'text')) {
                        $atfWarnings[] = ['danger', implode('', array_slice(explode("\n", $error->text), 0, 3))];
                    } elseif (property_exists($error, 'column')) {
                        $atfWarnings[] = [
                            'danger',
                            sprintf('Parsing failed on line %d near character %d', $error->line, $error->column),
                        ];
                    } elseif (property_exists($error, 'line')) {
                        $atfWarnings[] = [
                            'danger',
                            sprintf('Parsing failed on line %d', $error->line),
                        ];
                    } else {
                        $atfWarnings[] = ['danger', 'Unknown error'];
                    }
                }
            }

            $this->atfWarnings = $atfWarnings;
        }
    }

    public function hasAtfWarnings()
    {
        if (is_null($this->atfWarnings)) {
            return null;
        }

        return count($this->atfWarnings) > 0;
    }

    public function getAtfWarnings()
    {
        return $this->atfWarnings;
    }

    /**
     * @var array|null
     */
    private $tokenWarnings = null;

    public function setTokenWarnings($period)
    {
        if (is_null($this->tokenWarnings)) {
            $atfWarnings = [];
            foreach (['words', 'signs'] as $kind) {
                $list = TokenList::getTokenList($period, $kind);
                if (is_null($list)) {
                    array_push($atfWarnings, ['danger', __('{0} list not available', ucfirst($kind))]);
                } else {
                    array_push($atfWarnings, ...TokenList::compareText($this->atf, $kind, $list));
                }
            }
            $this->tokenWarnings = $atfWarnings;
        }
    }

    public function hasTokenWarnings()
    {
        if (is_null($this->tokenWarnings)) {
            return null;
        }

        return count($this->tokenWarnings) > 0;
    }

    public function getTokenWarnings()
    {
        return $this->tokenWarnings;
    }

    /**
     * @var array|null
     */
    protected $annotationWarnings = null;

    public function setAnnotationWarnings()
    {
        if (is_null($this->annotationWarnings)) {
            $this->annotationWarnings = array_filter(Check::checkCdliConll($this->annotation), function ($error) {
                return $error[0] === 'danger';
            });
        }
    }

    public function hasAnnotationWarnings()
    {
        if (is_null($this->annotationWarnings)) {
            return null;
        }

        return count($this->annotationWarnings) > 0;
    }

    public function getAnnotationWarnings()
    {
        return $this->annotationWarnings;
    }

    public function getPreferredType($types)
    {
        foreach ($types as $type) {
            if ($type === 'atf' && !$this->has('atf')) {
                continue;
            }

            if ($type === 'cdli-conll' && !$this->has('annotation')) {
                continue;
            }

            if ($type === 'conll-u' && !$this->has('annotation')) {
                continue;
            }

            if ($type === 'jtf' && !$this->has('atf')) {
                continue;
            }

            return $type;
        }

        return null;
    }

    public function getProcessedAtf()
    {
        if (!$this->has('atf')) {
            return null;
        }

        // Process line by line
        $processed = '';
        foreach (preg_split('/\r\n|\r|\n/', $this->atf) as $line) {
            if (preg_match('/^[0-9]/', $line)) {
                // Transliterations
                $processed .= h($line) . '<br>';
            } elseif (preg_match('/^#tr\./', $line)) {
                // Translations
                $processed .= '<i>&emsp;' . h(substr($line, 4)) . '</i> <br>';
            } elseif (preg_match('/^[#$] /', $line)) {
                // Comments
                $processed .= '<i>&emsp;' . h(substr($line, 1)) . '</i><br>';
            } elseif (preg_match('/^@/', $line)) {
                // Structure
                $processed .= '<b>' . h(substr($line, 1)) . '</b><br>';
            }
        }

        return $processed;
    }

    /**
     * @return string
     */
    public function getUpdateEventField(): string
    {
        return 'inscriptions';
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): string
    {
        return "inscriptions/$id";
    }

    public function getLinkedData(): array
    {
        $entity = [
            'rdf:type' => [
                LinkedData::newResource('crm:E34_Inscription'),
                LinkedData::newResource('dcmitype:Text'),
            ],
            'crm:P128i_is_carried_by' => LinkedData::newResource(Artifact::makeUri($this->artifact_id)),
            'crm:P3_has_note' => $this->atf,
            // TODO annotation
        ];

        if (!empty($this->artifact)) {
            $entity['crm:P72_has_language'] = array_map(function ($language) {
                return LinkedData::newResource(Language::makeUri($language->id));
            }, $this->artifact->languages);

            $entity['crm:P148i_is_component_of'] = array_map(function ($composite) {
                return LinkedData::newResource(Artifact::makeUri($composite->id));
            }, $this->artifact->composites);

            // $entity['crm:P67_refers_to'] = array_map(function ($date) {
            //     return LinkedData::newResource(Date::makeUri($date->id));
            // }, $this->artifact->dates);

            $entity['crm:P2_has_type'] = array_map(function ($genre) {
                return LinkedData::newResource(Genre::makeUri($genre->id));
            }, $this->artifact->genres);

            // if ($this->artifact->is_school_text) {
            //     $entity['crm:P2_has_type'][] = TODO;
            // }
        }

        return $entity;
    }
}
