<?php
declare(strict_types=1);

namespace App\Model\Entity;

interface LinkedDataInterface
{
    public function getUri(): string;

    public static function makeUri($id): ?string;

    public function getLinkedData(): array;
}
