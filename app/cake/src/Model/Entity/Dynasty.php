<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * Dynasty Entity
 *
 * @property int $id
 * @property string|null $polity
 * @property string|null $dynasty
 * @property int|null $sequence
 * @property int|null $provenience_id
 *
 * @property \App\Model\Entity\Provenience $provenience
 * @property \App\Model\Entity\Date[] $dates
 * @property \App\Model\Entity\Ruler[] $rulers
 */
class Dynasty extends Entity implements LinkedDataInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'polity' => true,
        'dynasty' => true,
        'sequence' => true,
        'provenience_id' => true,
        'provenience' => true,
        'dates' => true,
        'rulers' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'polity',
        'dynasty',
        'sequence',
        'provenience_id',
        'provenience',
        'dates',
        'rulers',
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'polity' => $this->polity,
            'dynasty' => $this->dynasty,
            'sequence' => $this->sequence,
            'provenience' => $this->has('provenience') ? $this->provenience->provenience : '',
        ];
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "dynasties/$id";
    }

    public function getLinkedData(): array
    {
        return [
            'rdf:type' => LinkedData::newResource('crm:E5_Event'),
            'rdfs:label' => $this->dynasty,
            'crm:P7_took_place_at' => LinkedData::newResource(Provenience::makeUri($this->provenience_id)),
            'crm:P11_had_participant' => LinkedData::newResource($this->getUri() . '#polity', [
                'rdf:type' => 'crm:E74_Group',
                'rdfs:label' => $this->polity,
            ]),
            'crm:P4_has_time-span' => LinkedData::newResource($this->getUri() . '#time-span', [
                'rdf:type' => LinkedData::newResource('crm:E52_Time-Span'),
            ]),
        ];
    }
}
