<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * ExternalResource Entity
 *
 * @property int $id
 * @property string|null $external_resource
 * @property string|null $base_url
 * @property string|null $base_uri
 * @property string|null $project_url
 * @property string|null $abbrev
 *
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class ExternalResource extends Entity implements LinkedDataInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'external_resource' => true,
        'base_url' => true,
        'base_uri' => true,
        'project_url' => true,
        'abbrev' => true,
        'artifacts' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'external_resource',
        'base_url',
        'base_uri',
        'project_url',
        'abbrev',
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'external_resource' => $this->external_resource,
            'base_url' => $this->base_url,
            'base_uri' => $this->base_uri,
            'project_url' => $this->project_url,
            'abbrev' => $this->abbrev,
        ];
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "external-resources/$id";
    }

    public function getLinkedData(): array
    {
        $uri = $this->getUri();

        return [
            'rdf:type' => LinkedData::newResource('crm:E74_Group'),
            'rdfs:label' => $this->external_resource,
            'crm:P1_is_identified_by' => [
                LinkedData::newAppellation("$uri#name", $this->external_resource),
                LinkedData::newAppellation("$uri#name/abbreviation", $this->abbrev),
                LinkedData::newAppellation("$uri#url", $this->project_url),
            ],
        ];
    }
}
