<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * SignReading Entity
 *
 * @property int $id
 * @property string|null $sign_name
 * @property string|null $sign_reading
 * @property bool|null $preferred_reading
 * @property int|null $period_id
 * @property int|null $provenience_id
 * @property int|null $language_id
 * @property string|null $meaning
 *
 * @property \App\Model\Entity\Period $period
 * @property \App\Model\Entity\Provenience $provenience
 * @property \App\Model\Entity\Language $language
 * @property \App\Model\Entity\SignReadingsComment[] $sign_readings_comments
 */
class SignReading extends Entity implements LinkedDataInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sign_name' => true,
        'sign_reading' => true,
        'preferred_reading' => true,
        'period_id' => true,
        'provenience_id' => true,
        'language_id' => true,
        'meaning' => true,
        'period' => true,
        'provenience' => true,
        'language' => true,
        'sign_readings_comments' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'sign_reading',
        'preferred_reading',
        'period_id',
        'provenience_id',
        'language_id',
        'meaning',
        'period',
        'provenience',
        'language',
        'sign_readings_comments',
    ];

    public function getTableRow()
    {
        return [
            'id' => $this->id,
            'sign_name' => $this->sign_name,
            'sign_reading' => $this->sign_reading,
            'preferred_reading' => $this->preferred_reading,
            'period' => $this->has('period') ? $this->period->period : '',
            'provenience' => $this->has('provenience') ? $this->provenience->provenience : '',
            'language' => $this->has('language') ? $this->language->language : '',
            'meaning' => $this->meaning,
        ];
    }

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "periods/$id";
    }

    public function getLinkedData(): array
    {
        return [
            'rdf:type' => LinkedData::newResource('crm:E13_Attribute_Assignment'),
            'crm:P140_assigned_attribute_to' => LinkedData::newResource($this->getUri() . '#reading', [
                'rdf:type' => LinkedData::newResource('crmtex:TX9_Grapheme'),
                'rdfs:label' => $this->sign_reading,
            ]),
            'crm:P141_assigned' => LinkedData::newResource($this->getUri() . '#name', [
                'rdf:type' => LinkedData::newResource('crmtex:TX9_Glyph'),
                'rdfs:label' => $this->sign_name,
                'crm:P62_depicts' => LinkedData::newResource($this->getUri() . '#reading'),
            ]),
            'crm:P177_assigned_property_of_type' => LinkedData::newResource('crm:P62_depicts'),
        ];

        // TODO
        // 'crmtex:TXP7i_is_item_of' => [
        //     'rdf:type' => LinkedData::newResource('crmtex:TX3_Writing_System'),
        //     'crmtex:TXP12_has_style' => [
        //         'rdf:type' => LinkedData::newResource('crmtex:TX10_Style'),
        //         // provenience, period, language
        //     ]
        // ]
    }
}
