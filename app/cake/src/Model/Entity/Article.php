<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Article Entity
 *
 * @property int $id
 * @property string|null $serial
 * @property string $title
 * @property int $year
 * @property string $article_no
 * @property string $content_html
 * @property string $content_latex
 * @property bool $is_pdf_uploaded
 * @property string $article_type
 * @property int $is_published
 * @property int $article_status
 * @property string $pdf_link
 * @property int $created_by
 * @property \Cake\I18n\FrozenDate|null $created
 * @property \Cake\I18n\FrozenDate|null $modified
 *
 * @property \App\Model\Entity\Author[] $authors
 * @property \App\Model\Entity\Publication[] $publications
 * @property \App\Model\Entity\OjsReviewAssignment[] $review_assignments
 * @property \App\Model\Entity\OjsReviewRound[] $review_rounds
 * @property \App\Model\Entity\Query[] $queries
 * @property \App\Model\Entity\OjsSubmission $submission
 */
class Article extends Entity
{
    use FlatErrorsTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'year' => true,
        'article_no' => true,
        'content_html' => true,
        'content_latex' => true,
        'article_status' => true,
        'authors' => true,
        'publications' => true,
        'created' => true,
        'modified' => true,

        'article_type' => false,
        'is_published' => false,
        'is_pdf_uploaded' => false,
        'pdf_link' => false,
        'created_by' => false,
        'review_assignments' => false,
        'queries' => false,
        'submission' => false,
        'review_rounds' => false,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'title',
        'year',
        'article_no',
        'content_html',
        'authors',
        'publications',
        'created',
        'modified',
    ];

    public function getImagePath(string $image)
    {
        return sprintf(
            '/srv/app/cake/webroot/pubs/figs/%s/%s-%s-%s',
            $this->article_type,
            $this->article_type,
            $this->getArticleNumberPath(),
            $image
        );
    }

    public function getPdfPath()
    {
        return sprintf(
            '/srv/app/cake/webroot/pubs/pdf/%s/%s-%s.pdf',
            $this->article_type,
            $this->article_type,
            $this->getArticleNumberPath()
        );
    }

    public function getArticleNumber()
    {
        if ($this->article_type === 'cdlp') {
            return $this->article_no;
        } else {
            return $this->year . ':' . $this->article_no;
        }
    }

    public function getArticleNumberPath()
    {
        if ($this->article_type === 'cdlp') {
            return $this->article_no;
        } else {
            return $this->year . '-' . $this->article_no;
        }
    }
}
