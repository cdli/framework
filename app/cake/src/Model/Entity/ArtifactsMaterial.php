<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Utility\LinkedData\LinkedData;
use Cake\ORM\Entity;

/**
 * ArtifactsMaterial Entity
 *
 * @property int $id
 * @property int $artifact_id
 * @property int $material_id
 * @property bool|null $is_material_uncertain
 * @property int|null $material_color_id
 * @property int|null $material_aspect_id
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\Material $material
 * @property \App\Model\Entity\MaterialColor $material_color
 * @property \App\Model\Entity\MaterialAspect $material_aspect
 */
class ArtifactsMaterial extends Entity implements LinkedDataInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'material_id' => true,
        'is_material_uncertain' => true,
        'material_color_id' => true,
        'material_aspect_id' => true,
        'artifact' => true,
        'material' => true,
        'material_color' => true,
        'material_aspect' => true,
    ];

    /**
     * Fields that should be included in the JSON export.
     *
     * @var array
     */
    public array $jsonSchema = [
        'id',
        'artifact_id',
        'material_id',
        'is_material_uncertain',
        'material_color_id',
        'material_aspect_id',
        'artifact',
        'material',
        'material_color',
        'material_aspect',
    ];

    // LinkedDataInterface

    public function getUri(): string
    {
        /** @var string */
        return self::makeUri($this->id);
    }

    public static function makeUri($id): ?string
    {
        return is_null($id) ? null : "artifacts-materials/$id";
    }

    public function getLinkedData(): array
    {
        return [
            'rdf:type' => LinkedData::newResource('crm:E57_Material'),
            'crm:P46i_forms_part_of' => LinkedData::newResource(Artifact::makeUri($this->artifact_id)),
            'crm:P127_has_broader_term' => [
                LinkedData::newResource(MaterialColor::makeUri($this->material_color_id)),
                LinkedData::newResource(MaterialAspect::makeUri($this->material_aspect_id)),
                LinkedData::newResource(Material::makeUri($this->material_id)),
            ],
        ];
    }
}
