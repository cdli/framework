<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Entity;

trait ValidationTrait
{
    public function validateChanges(Entity $entity, array $changes): array
    {
        $errors = [];

        if (!$this->checkRules($entity, $entity->isNew() ? 'create' : 'update')) {
            foreach ($entity->getErrors() as $field => $error) {
                $errors[$field] = $error;
            }
        }
        foreach ($this->getValidator()->validate($changes, $entity->isNew()) as $field => $error) {
            $errors[$field] = $error;
        }

        return $errors;
    }
}
