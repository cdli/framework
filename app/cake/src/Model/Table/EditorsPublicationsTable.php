<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EditorsPublications Model
 *
 * @property \App\Model\Table\PublicationsTable&\Cake\ORM\Association\BelongsTo $Publications
 * @property \App\Model\Table\AuthorsTable&\Cake\ORM\Association\BelongsTo $Editors
 * @method \App\Model\Entity\EditorsPublication newEmptyEntity()
 * @method \App\Model\Entity\EditorsPublication newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\EditorsPublication[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\EditorsPublication get($primaryKey, $options = [])
 * @method \App\Model\Entity\EditorsPublication findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\EditorsPublication patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\EditorsPublication[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\EditorsPublication|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EditorsPublication saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EditorsPublication[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\EditorsPublication[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\EditorsPublication[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\EditorsPublication[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class EditorsPublicationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('editors_publications');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Publications', [
            'foreignKey' => 'publication_id',
        ]);
        $this->belongsTo('Editors', [
            'foreignKey' => 'editor_id',
            'className' => 'Authors',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('sequence')
            ->allowEmptyString('sequence');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['publication_id'], 'Publications'), ['errorField' => 'publication_id']);
        $rules->add($rules->existsIn(['editor_id'], 'Editors'), ['errorField' => 'editor_id']);
        $rules->add($rules->isUnique(['publication_id', 'editor_id'], 'This editor is listed twice'));
        $rules->add($rules->isUnique(['publication_id', 'sequence'], 'This editor is listed at the same position as another'));

        return $rules;
    }
}
