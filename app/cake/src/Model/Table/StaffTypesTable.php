<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * StaffTypes Model
 *
 * @property \App\Model\Table\StaffTable&\Cake\ORM\Association\HasMany $Staff
 * @method \App\Model\Entity\StaffType newEmptyEntity()
 * @method \App\Model\Entity\StaffType newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\StaffType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\StaffType get($primaryKey, $options = [])
 * @method \App\Model\Entity\StaffType findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\StaffType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\StaffType[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\StaffType|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StaffType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StaffType[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\StaffType[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\StaffType[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\StaffType[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class StaffTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('staff_types');
        $this->setDisplayField('staff_type');
        $this->setPrimaryKey('id');

        $this->hasMany('Staff', [
            'foreignKey' => 'staff_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('staff_type')
            ->maxLength('staff_type', 200)
            ->requirePresence('staff_type', 'create')
            ->notEmptyString('staff_type');

        return $validator;
    }
}
