<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Collections Model
 *
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsToMany $Artifacts
 * @property \App\Model\Table\EntitiesNamesTable&\Cake\ORM\Association\HasMany $EntitiesNames
 * @property \App\Model\Table\ExternalResourcesTable&\Cake\ORM\Association\BelongsToMany $ExternalResources
 * @method \App\Model\Entity\Collection newEmptyEntity()
 * @method \App\Model\Entity\Collection newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Collection[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Collection get($primaryKey, $options = [])
 * @method \App\Model\Entity\Collection findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Collection patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Collection[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Collection|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Collection saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Collection[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Collection[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Collection[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Collection[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class CollectionsTable extends Table
{
    /**
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('collections');
        $this->setDisplayField('collection');
        $this->setPrimaryKey('id');

        $this->addBehavior('Muffin/Orderly.Orderly');
        $this->addBehavior('Search.Search', [
            'collectionClass' => $this->getAlias(),
        ]);

        $this->hasMany('EntitiesNames', [
            'foreignKey' => 'entity_id',
            'conditions' => ['entity_table' => 'collections'],
            'saveStrategy' => 'replace',
        ]);
        $this->belongsTo('GadmCountries', [
            'className' => 'GadmEntities',
            'foreignKey' => 'country_iso',
        ]);
        $this->belongsTo('GadmRegions', [
            'className' => 'GadmEntities',
            'foreignKey' => 'region_gadm',
        ]);
        $this->belongsTo('GadmDistricts', [
            'className' => 'GadmEntities',
            'foreignKey' => 'district_gadm',
        ]);
        $this->belongsToMany('Artifacts', [
            'foreignKey' => 'collection_id',
            'targetForeignKey' => 'artifact_id',
            'joinTable' => 'artifacts_collections',
        ]);
        $this->belongsToMany('ExternalResources', [
            'foreignKey' => 'entity_id',
            'targetForeignKey' => 'external_resource_id',
            'joinTable' => 'entities_external_resources',
            'conditions' => ['EntitiesExternalResources.entity_table' => 'collections'],
        ]);
        $this->belongsToMany('UpdateEvents', [
            'foreignKey' => 'entity_id',
            'targetForeignKey' => 'update_event_id',
            'joinTable' => 'entities_updates',
            'conditions' => ['EntitiesUpdates.entity_table' => 'collections'],
            'dependent' => false,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('collection')
            ->maxLength('collection', 255)
            ->requirePresence('collection', 'create')
            ->notEmptyString('collection');

        $validator
            ->scalar('collection_url')
            ->allowEmptyString('collection_url')
            ->maxLength('collection_url', 65535);

        // CDLI metadata
        $validator
            ->scalar('slug')
            ->maxLength('slug', 50)
            ->allowEmptyString('slug');

        $validator
            ->scalar('description')
            ->allowEmptyString('description')
            ->maxLength('description', 65535);

        $validator
            ->boolean('is_pinned')
            ->allowEmptyString('is_pinned');

        // Collection classification
        $validator
            ->scalar('collection_actor')
            ->inList('collection_actor', [
                'Agency',
                'Company',
                'Educational institution',
                'Foundation',
                'Person',
                'Research project',
                'Uncertain',
                'Unknown',
            ])
            ->allowEmptyString('collection_actor');

        $validator
            ->scalar('collection_holding')
            ->inList('collection_holding', [
                'Collection',
                'Library',
                'Lot',
                'Museum',
                'Public',
                'Storage',
                'Uncertain',
                'Unknown',
            ])
            ->allowEmptyString('collection_holding');

        $validator
            ->scalar('collection_actor_status')
            ->inList('collection_actor_status', [
                'Group',
                'Individual',
                'Museum',
                'Public',
                'Storage',
                'Uncertain',
                'Unknown',
            ])
            ->allowEmptyString('collection_actor_status');

        $validator
            ->scalar('collection_holding_status')
            ->inList('collection_holding_status', [
                'Extant',
                'Person',
                'Terminated',
                'Uncertain',
                'Unknown',
            ])
            ->allowEmptyString('collection_holding_status');

        $validator
            ->boolean('collection_is_private')
            ->allowEmptyString('collection_is_private');

        // Location
        $validator
            ->scalar('country_iso')
            ->maxLength('country_iso', 50)
            ->allowEmptyString('country_iso');

        $validator
            ->scalar('region_gadm')
            ->maxLength('region_gadm', 50)
            ->allowEmptyString('region_gadm');

        $validator
            ->scalar('district_gadm')
            ->maxLength('district_gadm', 50)
            ->allowEmptyString('district_gadm');

        $validator
            ->decimal('location_longitude_wgs1984')
            ->allowEmptyString('location_longitude_wgs1984');

        $validator
            ->decimal('location_latitude_wgs1984')
            ->allowEmptyString('location_latitude_wgs1984');

        $validator
            ->nonNegativeInteger('location_accuracy')
            ->allowEmptyString('location_accuracy');

        // GLoW ID
        $validator
            ->nonNegativeInteger('glow_id')
            ->allowEmptyString('glow_id');

        // Licenses
        $validator
            ->scalar('license_id')
            ->maxLength('license_id', 50)
            ->allowEmptyString('license_id');

        $validator
            ->scalar('license_attribution')
            ->maxLength('license_attribution', 255)
            ->allowEmptyString('license_attribution');

        $validator
            ->scalar('license_comment')
            ->maxLength('license_comment', 65535)
            ->allowEmptyString('license_comment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);

        return $rules;
    }

    /**
     * @return \Cake\Datasource\EntityInterface
     */
    public function newEmptyEntity(): \Cake\Datasource\EntityInterface
    {
        $entity = parent::newEmptyEntity();
        $entity->entities_names = [];
        $entity->external_resources = [];

        return $entity;
    }
}
