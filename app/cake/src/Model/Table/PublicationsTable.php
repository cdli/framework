<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Utility\LongestCommonSubsequence;
use ArrayObject;
use Cake\Event\Event;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Text;
use Cake\Validation\Validator;

/**
 * @property \App\Model\Table\EntryTypesTable&\Cake\ORM\Association\BelongsTo $EntryTypes
 * @property \App\Model\Table\JournalsTable&\Cake\ORM\Association\BelongsTo $Journals
 * @property \App\Model\Table\AuthorsTable&\Cake\ORM\Association\BelongsToMany $Authors
 * @property \App\Model\Table\AuthorsTable&\Cake\ORM\Association\BelongsToMany $Editors
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsToMany $Artifacts
 * @property \App\Model\Table\ArtifactAssetsTable&\Cake\ORM\Association\BelongsToMany $ArtifactAssets
 * @property \App\Model\Table\ProveniencesTable&\Cake\ORM\Association\BelongsToMany $Proveniences
 * @property \App\Model\Table\AbbreviationsTable&\Cake\ORM\Association\HasMany $Abbreviations
 * @property \App\Model\Table\ArticlesTable&\Cake\ORM\Association\BelongsToMany $Articles
 * @property \App\Model\Table\UpdateEventsTable&\Cake\ORM\Association\BelongsToMany $UpdateEvents
 * @method \App\Model\Entity\Publication newEmptyEntity()
 * @method \App\Model\Entity\Publication newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Publication[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Publication get($primaryKey, $options = [])
 * @method \App\Model\Entity\Publication findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Publication patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Publication[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Publication|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Publication saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Publication[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Publication[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Publication[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Publication[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class PublicationsTable extends Table
{
    use ValidationTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('publications');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Muffin/Orderly.Orderly', [
            'order' => [
                'Publications.bibtexkey' => 'ASC',
            ],
        ]);

        // Metadata
        $this->belongsTo('EntryTypes', [
            'foreignKey' => 'entry_type_id',
        ]);
        $this->belongsTo('Journals', [
            'foreignKey' => 'journal_id',
        ]);
        $this->belongsToMany('Authors', [
            'foreignKey' => 'publication_id',
            'targetForeignKey' => 'author_id',
            'joinTable' => 'authors_publications',
            'propertyName' => 'authors',
            'sort' => ['AuthorsPublications.sequence' => 'ASC'],
        ]);
        $this->belongsToMany('Editors', [
            'foreignKey' => 'publication_id',
            'targetForeignKey' => 'editor_id',
            'joinTable' => 'editors_publications',
            'through' => 'EditorsPublications',
            'propertyName' => 'editors',
            'sort' => ['EditorsPublications.sequence' => 'ASC'],
            'className' => 'Authors',
        ]);
        $this->belongsToMany('ExternalResources', [
            'foreignKey' => 'entity_id',
            'targetForeignKey' => 'external_resource_id',
            'joinTable' => 'entities_external_resources',
            'conditions' => ['EntitiesExternalResources.entity_table' => 'publications'],
        ]);

        // Topics
        $this->belongsToMany('ArtifactAssets', [
            'foreignKey' => 'publication_id',
            'targetForeignKey' => 'entity_id',
            'joinTable' => 'entities_publications',
            'conditions' => 'table_name = "artifact_assets"',
        ]);
        $this->belongsToMany('Artifacts', [
            'foreignKey' => 'publication_id',
            'targetForeignKey' => 'entity_id',
            'joinTable' => 'entities_publications',
            'conditions' => 'table_name = "artifacts"',
        ]);
        $this->belongsToMany('Proveniences', [
            'foreignKey' => 'publication_id',
            'targetForeignKey' => 'entity_id',
            'joinTable' => 'entities_publications',
            'conditions' => 'table_name = "proveniences"',
        ]);

        // Other
        $this->hasMany('Abbreviations', [
            'foreignKey' => 'publication_id',
        ]);
        $this->belongsToMany('Articles', [
            'foreignKey' => 'publication_id',
            'targetForeignKey' => 'article_id',
            'joinTable' => 'articles_publications',
        ]);
        $this->belongsToMany('UpdateEvents', [
            'foreignKey' => 'entity_id',
            'targetForeignKey' => 'update_event_id',
            'joinTable' => 'entities_updates',
            'conditions' => ['EntitiesUpdates.table_name' => 'publications'],
            'dependent' => false,
        ]);
    }

    /**
     * @param array $ids
     * @param int $primaryId
     * @return array
     */
    public function getMergeData(array $ids, int $primaryId): array
    {
        $publications = $this->find('all')
            ->whereInList('id', $ids)
            ->contain(['Artifacts', 'Authors', 'Editors', 'ExternalResources'])
            ->toArray();
        $artifacts = [];
        $merge = [];

        $designation = $this->_getDesignation($publications);
        foreach ($publications as $publication) {
            foreach ($publication->artifacts as $artifact) {
                if ($artifact->_joinData->exact_reference == null || $artifact->_joinData->exact_reference == '') {
                    // To pre-fill the shortened publication, we need to find the real designation.
                    if (empty($designation)) {
                        $artifact->_joinData->exact_reference = $publication->designation;
                    } else {
                        $exact_reference = explode($designation, $publication->designation);
                        $exact_reference = array_pop($exact_reference);
                        $exact_reference = preg_replace('/^( \(\d{4}\))?([,:;]? )?/', '', $exact_reference);
                        $artifact->_joinData->exact_reference = $exact_reference;
                    }
                }
                $artifacts[] = $artifact;
            }

            if ($publication->id === $primaryId) {
                $primary = $publication;
            } else {
                $merge[] = $publication;
            }
        }

        $primary->designation = preg_replace('/([,:;]? )$/', '', $designation);

        return [
            'publications' => $merge,
            'artifacts' => $artifacts,
            'publication' => $primary,
        ];
    }

    private function _getDesignation(array $publications): string
    {
        $designations = array_filter(array_column($publications, 'designation'));

        // Break designations at word boundaries
        $tokenized = array_map(function ($designation) {
            return preg_split('/\b/', $designation);
        }, $designations);

        // Find the longest common subsequence
        $lcs = LongestCommonSubsequence::fromSegmentedStringArray($tokenized);

        return implode($lcs);
    }

    public function beforeMarshal(Event $event, ArrayObject $data)
    {
        // Generating bibtexkey if it is empty
        if (!isset($data['bibtexkey']) && (isset($data['authors']) && isset($data['year']))) {
            $bibtexKey = Text::slug($data['authors'][0]->last) . $data['year'];

            // Ensure the bibtexkey is unique
            $similarBibtexKeys = $this->find('all', ['fields' => ['bibtexkey'], 'order' => ['bibtexkey' => 'asc']])
                ->where(['bibtexkey LIKE' => $bibtexKey . '%'])
                ->toArray();
            $similarBibtexKeys = array_column($similarBibtexKeys, 'bibtexkey');
            if (in_array($bibtexKey, $similarBibtexKeys)) {
                $suffix = 'a';
                while (in_array($bibtexKey . $suffix, $similarBibtexKeys)) {
                    $suffix++;
                }
                $bibtexKey = $bibtexKey . $suffix;
            }

            $data['bibtexkey'] = $bibtexKey;
        }
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('bibtexkey')
            ->maxLength('bibtexkey', 80)
            ->notEmptyString('bibtexkey')
            ->requirePresence('bibtexkey', 'create');

        $validator
            ->scalar('year')
            ->maxLength('year', 20)
            ->allowEmptyString('year');

        $validator
            ->scalar('address')
            ->maxLength('address', 45)
            ->allowEmptyString('address');

        $validator
            ->scalar('book_title')
            ->maxLength('book_title', 255)
            ->allowEmptyString('book_title');

        $validator
            ->scalar('chapter')
            ->maxLength('chapter', 100)
            ->allowEmptyString('chapter');

        $validator
            ->scalar('edition')
            ->maxLength('edition', 45)
            ->allowEmptyString('edition');

        $validator
            ->scalar('how_published')
            ->maxLength('how_published', 255)
            ->allowEmptyString('how_published');

        $validator
            ->scalar('institution')
            ->maxLength('institution', 45)
            ->allowEmptyString('institution');

        $validator
            ->scalar('month')
            ->maxLength('month', 45)
            ->allowEmptyString('month');

        $validator
            ->scalar('note')
            ->allowEmptyString('note');

        $validator
            ->scalar('number')
            ->maxLength('number', 100)
            ->allowEmptyString('number');

        $validator
            ->scalar('organization')
            ->maxLength('organization', 45)
            ->allowEmptyString('organization');

        $validator
            ->scalar('pages')
            ->maxLength('pages', 45)
            ->allowEmptyString('pages');

        $validator
            ->scalar('publisher')
            ->maxLength('publisher', 100)
            ->allowEmptyString('publisher');

        $validator
            ->scalar('school')
            ->maxLength('school', 80)
            ->allowEmptyString('school');

        $validator
            ->scalar('title')
            ->allowEmptyString(
                'title',
                __('Title or designation must be provided.'),
                function ($context) {
                    return !empty($context['data']['title']) || !empty($context['data']['designation']);
                }
            )
            ->requirePresence('title', 'create');

        $validator
            ->scalar('volume')
            ->maxLength('volume', 50)
            ->allowEmptyString('volume');

        $validator
            ->scalar('series')
            ->maxLength('series', 255)
            ->allowEmptyString('series');

        $validator
            ->integer('oclc')
            ->allowEmptyString('oclc');

        $validator
            ->scalar('designation')
            ->maxLength('designation', 2048)
            ->allowEmptyString('designation');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        $rules->add($rules->isUnique(['bibtexkey'], ['errorField' => 'bibtexkey', 'message' => 'The BibTeX key already exists']));
        $rules->add($rules->isUnique(['designation'], ['errorField' => 'designation']));
        $rules->add($rules->existsIn(['entry_type_id'], 'EntryTypes'), ['errorField' => 'entry_type_id']);
        $rules->add($rules->existsIn(['journal_id'], 'Journals'), ['errorField' => 'journal_id']);

        return $rules;
    }

    /**
     * @return \Cake\Datasource\EntityInterface
     */
    public function newEmptyEntity(): \Cake\Datasource\EntityInterface
    {
        $entity = parent::newEmptyEntity();
        $entity->authors = [];
        $entity->editors = [];
        $entity->external_resources = [];

        return $entity;
    }
}
