<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Model\Table;

use App\Model\Entity\Artifact;
use App\Model\Entity\ArtifactsUpdate;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use UnexpectedValueException;

/**
 * ArtifactsUpdates Model
 *
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsTo $Artifacts
 * @property \App\Model\Table\UpdateEventsTable&\Cake\ORM\Association\BelongsTo $UpdateEvents
 * @method \App\Model\Entity\ArtifactsUpdate newEmptyEntity()
 * @method \App\Model\Entity\ArtifactsUpdate newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ArtifactsUpdatesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('artifacts_updates');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id',
        ]);
        $this->belongsTo('UpdateEvents', [
            'foreignKey' => 'update_events_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('cdli_comments')
            ->allowEmptyString('cdli_comments');

        $validator
            ->scalar('designation')
            ->maxLength('designation', 200)
            // For new records designation must be provided
            ->requirePresence('designation', function ($context) {
                return !isset($context['data']['artifact_id']);
            })
            // For new records designation must be non empty
            ->allowEmptyString('designation', null, function ($context) {
                return isset($context['data']['artifact_id']);
            });

        $validator
            ->scalar('artifact_type')
            ->allowEmptyString('artifact_type');

        $validator
            ->scalar('period')
            ->allowEmptyString('period');

        $validator
            ->scalar('provenience')
            ->allowEmptyString('provenience');

        $validator
            ->scalar('written_in')
            ->allowEmptyString('written_in');

        $validator
            ->scalar('archive')
            ->allowEmptyString('archive');

        $validator
            ->scalar('composite_no')
            ->maxLength('composite_no', 250)
            ->allowEmptyString('composite_no');

        $validator
            ->scalar('seal_no')
            ->maxLength('seal_no', 250)
            ->allowEmptyString('seal_no');

        $validator
            ->scalar('composites')
            ->allowEmptyString('composites');

        $validator
            ->scalar('seals')
            ->allowEmptyString('seals');

        $validator
            ->scalar('museum_no')
            ->maxLength('museum_no', 250)
            ->allowEmptyString('museum_no');

        $validator
            ->scalar('accession_no')
            ->maxLength('accession_no', 250)
            ->allowEmptyString('accession_no');

        $validator
            ->scalar('condition_description')
            ->allowEmptyString('condition_description');

        $validator
            ->scalar('artifact_preservation')
            ->allowEmptyString('artifact_preservation');

        $validator
            ->scalar('period_comments')
            ->allowEmptyString('period_comments');

        $validator
            ->scalar('provenience_comments')
            ->allowEmptyString('provenience_comments');

        $validator
            ->scalar('artifact_type_comments')
            ->allowEmptyString('artifact_type_comments');

        $validator
            ->scalar('is_provenience_uncertain')
            ->maxLength('is_provenience_uncertain', 1)
            ->allowEmptyString('is_provenience_uncertain');

        $validator
            ->scalar('is_period_uncertain')
            ->maxLength('is_period_uncertain', 1)
            ->allowEmptyString('is_period_uncertain');

        $validator
            ->scalar('is_artifact_type_uncertain')
            ->maxLength('is_artifact_type_uncertain', 1)
            ->allowEmptyString('is_artifact_type_uncertain');

        $validator
            ->scalar('is_school_text')
            ->maxLength('is_school_text', 1)
            ->allowEmptyString('is_school_text');

        $validator
            ->scalar('height')
            ->maxLength('height', 11)
            ->allowEmptyString('height');

        $validator
            ->scalar('thickness')
            ->maxLength('thickness', 11)
            ->allowEmptyString('thickness');

        $validator
            ->scalar('width')
            ->maxLength('width', 11)
            ->allowEmptyString('width');

        $validator
            ->scalar('weight')
            ->maxLength('weight', 11)
            ->allowEmptyString('weight');

        $validator
            ->scalar('elevation')
            ->allowEmptyString('elevation');

        $validator
            ->scalar('excavation_no')
            ->allowEmptyString('excavation_no');

        $validator
            ->scalar('findspot_square')
            ->allowEmptyString('findspot_square');

        $validator
            ->scalar('findspot_comments')
            ->allowEmptyString('findspot_comments');

        $validator
            ->scalar('stratigraphic_level')
            ->allowEmptyString('stratigraphic_level');

        $validator
            ->scalar('surface_preservation')
            ->allowEmptyString('surface_preservation');

        $validator
            ->scalar('artifact_comments')
            ->allowEmptyString('artifact_comments');

        $validator
            ->scalar('seal_information')
            ->allowEmptyString('seal_information');

        $validator
            ->scalar('accounting_period')
            ->allowEmptyString('accounting_period');

        $validator
            ->scalar('is_public')
            ->maxLength('is_public', 1)
            ->allowEmptyString('is_public');

        $validator
            ->scalar('is_atf_public')
            ->maxLength('is_atf_public', 1)
            ->allowEmptyString('is_atf_public');

        $validator
            ->scalar('are_images_public')
            ->maxLength('are_images_public', 1)
            ->allowEmptyFile('are_images_public');

        $validator
            ->scalar('collections')
            ->allowEmptyString('collections');

        $validator
            ->scalar('dates')
            ->allowEmptyString('dates');

        $validator
            ->scalar('alternative_years')
            ->allowEmptyString('alternative_years');

        $validator
            ->scalar('external_resources')
            ->allowEmptyString('external_resources');

        $validator
            ->scalar('external_resources_key')
            ->maxLength('external_resources_key', 250)
            ->allowEmptyString('external_resources_key');

        $validator
            ->scalar('genres')
            ->allowEmptyString('genres');

        $validator
            ->scalar('genres_comment')
            ->allowEmptyString('genres_comment');

        $validator
            ->scalar('genres_uncertain')
            ->maxLength('genres_uncertain', 250)
            ->allowEmptyString('genres_uncertain');

        $validator
            ->scalar('languages')
            ->allowEmptyString('languages');

        $validator
            ->scalar('languages_uncertain')
            ->maxLength('languages_uncertain', 250)
            ->allowEmptyString('languages_uncertain');

        $validator
            ->scalar('materials')
            ->allowEmptyString('materials');

        $validator
            ->scalar('materials_aspect')
            ->allowEmptyString('materials_aspect');

        $validator
            ->scalar('materials_color')
            ->allowEmptyString('materials_color');

        $validator
            ->scalar('materials_uncertain')
            ->maxLength('materials_uncertain', 250)
            ->allowEmptyString('materials_uncertain');

        $validator
            ->scalar('shadow_cdli_comments')
            ->allowEmptyString('shadow_cdli_comments');

        $validator
            ->scalar('shadow_collection_location')
            ->allowEmptyString('shadow_collection_location');

        $validator
            ->scalar('shadow_collection_comments')
            ->allowEmptyString('shadow_collection_comments');

        $validator
            ->scalar('shadow_acquisition_history')
            ->allowEmptyString('shadow_acquisition_history');

        $validator
            ->scalar('publications_key')
            ->allowEmptyString('publications_key');

        $validator
            ->scalar('publications_type')
            ->allowEmptyString('publications_type');

        $validator
            ->scalar('publications_exact_ref')
            ->allowEmptyString('publications_exact_ref');

        $validator
            ->scalar('publications_comment')
            ->allowEmptyString('publications_comment');

        $validator
            ->scalar('retired')
            ->maxLength('retired', 1)
            ->allowEmptyString('retired');

        $validator
            ->scalar('has_fragments')
            ->maxLength('has_fragments', 1)
            ->allowEmptyString('has_fragments');

        $validator
            ->scalar('is_artifact_fake')
            ->maxLength('is_artifact_fake', 1)
            ->allowEmptyString('is_artifact_fake');

        $validator
            ->scalar('publication_error')
            ->allowEmptyString('publication_error');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'), ['errorField' => 'artifact_id']);
        $rules->add($rules->existsIn(['update_events_id'], 'UpdateEvents'), ['errorField' => 'update_events_id']);

        return $rules;
    }

    /**
     * @param array $new_data table row to import
     * @param bool $concatenate append import to existing data
     * @return \App\Model\Entity\ArtifactsUpdate
     */
    public function newEntityFromFlatData(array $new_data, bool $concatenate = false)
    {
        $update = $this->newEmptyEntity();
        $update->artifact = $this->Artifacts->newEmptyEntity();

        $diffData = [];

        if (empty($new_data['artifact_id'])) {
            $update->artifact_id = null; // continue
        } elseif (!preg_match('/^\d+$/', $new_data['artifact_id'])) {
            $update->setError('artifact_id', __('Expected a CDLI number (without prefix), got "{0}"', $new_data['artifact_id']));
        } else {
            $update->artifact = $this->Artifacts->get($new_data['artifact_id']);
            $update->artifact_id = $update->artifact->id;
            // This is required by the validator for `designation`.
            $diffData['artifact_id'] = $update->artifact_id;
        }

        // Store only changed data.
        foreach ($new_data as $key => $new_value) {
            // Set separately, see above
            if ($key === 'artifact_id') {
                continue;
            }

            if (!array_key_exists($key, Artifact::$flatFields)) {
                $update->setError($key, __('Column name "{0}" cannot be imported', $key));
                continue;
            }

            // Lazy-load associations
            if (!is_null(Artifact::$flatFields[$key])) {
                $this->Artifacts->loadInto($update->artifact, Artifact::$flatFields[$key]);
            }

            $old_value = $update->artifact->getFlatValue($key);
            if ($concatenate && array_key_exists($key, self::$concatenatedFields)) {
                if (self::$concatenatedFields[$key] === 'string') {
                    $diffData[$key] = $old_value === null ? $new_value : $old_value . $new_value;
                } elseif (self::$concatenatedFields[$key] === 'array') {
                    $diffData[$key] = $old_value === null ? $new_value : $old_value . '; ' . $new_value;
                }
            } elseif ($new_value !== $old_value && !($new_value === '' && is_null($old_value))) {
                $diffData[$key] = $new_value;
            }
        }

        $this->patchEntity($update, $diffData);

        // Exception: make sure join data is always included if the main values change.
        foreach (ArtifactsUpdate::$mappings as $key => $mapping) {
            if ($update->has($key) && array_key_exists('joinData', $mapping)) {
                foreach (array_keys($mapping['joinData']) as $subKey) {
                    if (!$update->has($subKey)) {
                        $update->set($subKey, $update->artifact->getFlatValue($subKey));
                    }
                }
            }
        }

        return $update;
    }

    /**
     * @param array|\Cake\ORM\ResultSet $updates
     * @return \App\Model\Entity\ArtifactsUpdate
     */
    public function mergeUpdates($updates)
    {
        $update = $this->newEmptyEntity();

        foreach ($updates as $new) {
            if ($update->has('artifact_id') && $update->artifact_id !== $new->artifact_id) {
                throw new UnexpectedValueException('Not all updates pertain to the same artifact.');
            } else {
                $update->set('artifact_id', $new->artifact_id);
            }

            foreach ($new->getChanged() as $key) {
                $update->set($key, $new[$key]);
            }
        }

        return $update;
    }

    /**
     * @param \App\Model\Entity\ArtifactsUpdate $artifactsUpdate
     * @param bool $inclusive
     * @return \App\Model\Entity\ArtifactsUpdate
     */
    public function getPreviousUpdates(ArtifactsUpdate $artifactsUpdate, bool $inclusive)
    {
        if (!$artifactsUpdate->has('artifact_id')) {
            return $this->newEmptyEntity();
        }

        $conditions = [
            'ArtifactsUpdates.artifact_id' => $artifactsUpdate->artifact_id,
            'UpdateEvents.status' => 'approved',
        ];

        if ($artifactsUpdate->update_event->status === 'approved') {
            $conditions['UpdateEvents.approved <'] = $artifactsUpdate->update_event->approved;
        }

        $updates = $this->find()
            ->where($conditions)
            ->order(['UpdateEvents.approved' => 'ASC'])
            ->contain(['UpdateEvents'])
            ->all();

        return $this->mergeUpdates($updates);
    }

    protected static array $concatenatedFields = [
        'designation' => 'string',
        'cdli_comments' => 'string',
        'composites' => 'array',
        'seals' => 'array',
        'period_comments' => 'string',
        'provenience_comments' => 'string',
        'artifact_type_comments' => 'string',
        'findspot_comments' => 'string',
        'artifact_comments' => 'string',
        'seal_information' => 'string',
        'collections' => 'array',
        'external_resources' => 'array',
        'external_resources_key' => 'array',
        'genres' => 'array',
        'genres_comment' => 'array',
        'genres_uncertain' => 'array',
        'languages' => 'array',
        'languages_uncertain' => 'array',
        'materials' => 'array',
        'materials_aspect' => 'array',
        'materials_color' => 'array',
        'materials_uncertain' => 'array',
        'shadow_cdli_comments' => 'string',
        'shadow_collection_location' => 'string',
        'shadow_collection_comments' => 'string',
        'shadow_acquisition_history' => 'string',
        'publications_key' => 'array',
        'publications_type' => 'array',
        'publications_exact_ref' => 'array',
        'publications_comment' => 'array',
        'retired_comments' => 'string',
    ];
}
