<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Locations Model
 *
 * @property \App\Model\Table\ProveniencesTable&\Cake\ORM\Association\HasMany $Proveniences
 * @property \App\Model\Table\PlacesTable&\Cake\ORM\Association\HasMany $Places
 * @property \App\Model\Table\EntitiesNamesTable&\Cake\ORM\Association\HasMany $EntitiesNames
 * @property \App\Model\Table\ExternalResourcesTable&\Cake\ORM\Association\BelongsToMany $ExternalResources
 * @method \App\Model\Entity\Location newEmptyEntity()
 * @method \App\Model\Entity\Location newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Location[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Location get($primaryKey, $options = [])
 * @method \App\Model\Entity\Location findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Location patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Location[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Location|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Location saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Location[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Location[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Location[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Location[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class LocationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('locations');
        $this->setDisplayField('location');
        $this->setPrimaryKey('id');

        $this->addBehavior('Muffin/Orderly.Orderly');

        $this->hasMany('EntitiesNames', [
            'foreignKey' => 'entity_id',
            'conditions' => ['entity_table' => 'locations'],
            'saveStrategy' => 'replace',
        ]);
        $this->hasMany('Proveniences', [
            'foreignKey' => 'location_id',
        ]);
        $this->hasMany('Places', [
            'foreignKey' => 'location_id',
        ]);
        $this->belongsToMany('ExternalResources', [
            'foreignKey' => 'entity_id',
            'targetForeignKey' => 'external_resource_id',
            'joinTable' => 'entities_external_resources',
            'conditions' => ['EntitiesExternalResources.entity_table' => 'locations'],
        ]);
        $this->belongsToMany('UpdateEvents', [
            'foreignKey' => 'entity_id',
            'targetForeignKey' => 'update_event_id',
            'joinTable' => 'entities_updates',
            'conditions' => ['EntitiesUpdates.entity_table' => 'locations'],
            'dependent' => false,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('accuracy')
            ->allowEmptyString('accuracy');

        $validator
            ->decimal('location_longitude_wgs1984')
            ->allowEmptyString('location_longitude_wgs1984');

        $validator
            ->decimal('location_latitude_wgs1984')
            ->allowEmptyString('location_latitude_wgs1984');

        $validator
            ->scalar('location_geoshape_wgs1984')
            ->allowEmptyString('location_geoshape_wgs1984');

        $validator
            ->scalar('notes')
            ->allowEmptyString('notes');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);

        return $rules;
    }

    /**
     * @return \Cake\Datasource\EntityInterface
     */
    public function newEmptyEntity(): \Cake\Datasource\EntityInterface
    {
        $entity = parent::newEmptyEntity();
        $entity->entities_names = [];
        $entity->external_resources = [];

        return $entity;
    }
}
