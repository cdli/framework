<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\EntitiesUpdate;
use Cake\ORM\Association;
use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * @property \App\Model\Table\UpdateEventsTable&\Cake\ORM\Association\BelongsToMany $UpdateEventsTable
 * @method \App\Model\Entity\EntitiesUpdate newEmptyEntity()
 * @method \App\Model\Entity\EntitiesUpdate newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\EntitiesUpdate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\EntitiesUpdate get($primaryKey, $options = [])
 * @method \App\Model\Entity\EntitiesUpdate findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\EntitiesUpdate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\EntitiesUpdate[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\EntitiesUpdate|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EntitiesUpdate saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EntitiesUpdate[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\EntitiesUpdate[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\EntitiesUpdate[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\EntitiesUpdate[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class EntitiesUpdatesTable extends Table
{
    use LocatorAwareTrait;

    /**
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('entities_updates');
        $this->setPrimaryKey('id');

        $this->belongsTo('UpdateEvents', [
            'foreignKey' => 'update_event_id',
        ]);
    }

    /**
     * @param \Cake\ORM\Entity $entity
     * @return \App\Model\Entity\EntitiesUpdate
     */
    public function newEntityFromChangedEntity(Entity $entity)
    {
        $table = $this->getTableLocator()->get($entity->getSource());
        $update = $this->newEmptyEntity();
        $update->entity_table = $table->getTable();
        $update->entity_id = $entity->get($table->getPrimaryKey());

        $associations = $table->associations();

        $diff = [];

        foreach ($entity->getDirty() as $field) {
            $value = $entity->get($field);

            $association = $associations->getByProperty($field);
            if (!is_null($association)) {
                // Handle BelongsTo if ..._id columns are not updated
                if ($association->type() === Association::MANY_TO_ONE) {
                    $foreignKeyField = $association->getForeignKey();
                    $bindingKeyField = $association->getBindingKey();

                    if (!$entity->isDirty($foreignKeyField) && $entity->getOriginal($foreignKeyField) !== $value->get($bindingKeyField)) {
                        $diff[$foreignKeyField] = $value->get($bindingKeyField);
                    }
                }

                continue;
            }

            // Do not overwrite null or empty string with empty strings
            if ($value === '' && (is_null($entity->getOriginal($field)) || $entity->getOriginal($field) === '')) {
                continue;
            }

            $diff[$field] = $value;
        }

        $dependentAssociations = array_keys(EntitiesUpdate::$dependentAssociations[$update->entity_table]);
        foreach ($dependentAssociations as $dependentAssociation) {
            $association = $table->getAssociation($dependentAssociation);

            // Skip BelongsTo (handled by ..._id columns)
            if ($association->type() === Association::MANY_TO_ONE) {
                continue;
            }

            $field = $association->getProperty();

            // Skip if no changes.
            if (!$entity->isDirty($field)) {
                continue;
            }

            $value = $entity->get($field);
            $valueCount = count($value);
            $originalValue = $entity->getOriginal($field);
            $originalValueCount = count($originalValue);

            // Do not 'overwrite' empty arrays with empty arrays
            if (is_array($value) && $valueCount === 0 && $originalValueCount === 0) {
                continue;
            }

            // Set entity_table fields where necessary
            if (is_array($value)) {
                foreach ($value as $associatedEntity) {
                    if ($associatedEntity->has('_joinData')) {
                        $associatedEntity = $associatedEntity->_joinData;
                    }
                    $associatedTable = $this->getTableLocator()->get($associatedEntity->getSource());
                    if ($associatedTable->hasField('entity_table') && !$associatedEntity->has('entity_table')) {
                        $associatedEntity->set('entity_table', $update->entity_table);
                    } elseif ($associatedTable->hasField('table_name') && !$associatedEntity->has('table_name')) {
                        $associatedEntity->set('table_name', $update->entity_table);
                    }
                }
            }

            if ($association->type() === Association::MANY_TO_MANY) {
                $subValues = [];
                foreach ($value as $subValue) {
                    $subValues[$subValue['id']] = $subValue;
                }

                $originalSubValues = [];
                foreach ($originalValue as $index => $originalSubValue) {
                    if (array_key_exists($originalSubValue['id'], $subValues)) {
                        $subValue = $subValues[$originalSubValue['id']];
                        foreach ($subValue->_joinData->getDirty() as $subField) {
                            $diff[$field][$index]['_joinData'][$subField] = $subValue->_joinData->get($subField);
                        }
                        unset($subValues[$originalSubValue['id']]);
                    } else {
                        $diff[$field][$index] = null;
                    }
                }

                $offset = $originalValueCount;
                foreach ($subValues as $subValue) {
                    $index = $offset++;
                    $diff[$field][$index]['id'] = $subValue->get('id');
                    foreach ($subValue->_joinData->getDirty() as $subField) {
                        $diff[$field][$index]['_joinData'][$subField] = $subValue->_joinData->get($subField);
                    }
                }
            } elseif ($association->type() === Association::ONE_TO_MANY) {
                $subValues = [];
                foreach ($value as $subValue) {
                    $subValues[$subValue['id']] = $subValue;
                }

                $originalSubValues = [];
                foreach ($originalValue as $index => $originalSubValue) {
                    if (array_key_exists($originalSubValue['id'], $subValues)) {
                        $subValue = $subValues[$originalSubValue['id']];
                        foreach ($subValue->getDirty() as $subField) {
                            $diff[$field][$index][$subField] = $subValue->get($subField);
                        }
                        unset($subValues[$originalSubValue['id']]);
                    } else {
                        $diff[$field][$index] = null;
                    }
                }

                $offset = $originalValueCount;
                foreach ($subValues as $subValue) {
                    $index = $offset++;
                    foreach ($subValue->getDirty() as $subField) {
                        $diff[$field][$index][$subField] = $subValue->get($subField);
                    }
                }
            } elseif ($association->type() === Association::ONE_TO_ONE) {
                foreach ($value->getDirty() as $subField) {
                    $diff[$field][$subField] = $value->get($subfField);
                }
            }
        }

        $update->entity_update = json_encode($diff);

        foreach ($entity->getErrors() as $field => $errors) {
            $update->setError("entity_update.$field", $errors);
        }

        return $update;
    }

    /**
     * @param \Cake\ORM\Entity|string|int $entity
     * @param string|null $entityTable
     * @return \App\Model\Entity\EntitiesUpdate
     */
    public function newEntityFromDeletedEntity(Entity|string|int $entity, string|null $entityTable)
    {
        $update = $this->newEmptyEntity();
        $update->entity_update = 'null';

        if ($entity instanceof Entity) {
            $table = $this->getTableLocator()->get($entity->getSource());
            $update->entity_table = $table->getTable();
            $update->entity_id = $entity->get($table->getPrimaryKey());
        } else {
            $update->entity_table = $entityTable;
            $update->entity_id = (int)$entity;
        }

        return $update;
    }

    /**
     * @param string $field
     * @param string $message
     * @return \App\Model\Entity\EntitiesUpdate
     */
    public function newEntityFromError(string $field, string $message)
    {
        $update = $this->newEmptyEntity();
        $update->setError($field, $message);

        return $update;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('entity_table')
            ->maxLength('entity_table', 255);

        $validator
            ->nonNegativeInteger('entity_id');

        $validator
            ->scalar('entity_update');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        $rules->add($rules->existsIn(['update_event_id'], 'UpdateEvents'), ['errorField' => 'update_event_id']);

        return $rules;
    }
}
