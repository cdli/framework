<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Calibration Model
 *
 * @property \App\Model\Table\ChemicalDataTable&\Cake\ORM\Association\HasMany $ChemicalData
 * @method \App\Model\Entity\Calibration newEmptyEntity()
 * @method \App\Model\Entity\Calibration newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Calibration[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Calibration get($primaryKey, $options = [])
 * @method \App\Model\Entity\Calibration findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Calibration patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Calibration[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Calibration|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Calibration saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Calibration[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Calibration[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Calibration[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Calibration[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class CalibrationTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('calibration');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('ChemicalData', [
            'foreignKey' => 'calibration_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('human_number')
            ->maxLength('human_number', 100)
            ->requirePresence('human_number', 'create')
            ->notEmptyString('human_number');

        $validator
            ->scalar('content')
            ->maxLength('content', 4294967295)
            ->requirePresence('content', 'create')
            ->notEmptyString('content');

        return $validator;
    }
}
