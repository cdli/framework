<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Dates Model
 *
 * @property \App\Model\Table\MonthsTable&\Cake\ORM\Association\BelongsTo $Months
 * @property \App\Model\Table\YearsTable&\Cake\ORM\Association\BelongsTo $Years
 * @property \App\Model\Table\DynastiesTable&\Cake\ORM\Association\BelongsTo $Dynasties
 * @property \App\Model\Table\RulersTable&\Cake\ORM\Association\BelongsTo $Rulers
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsToMany $Artifacts
 * @method \App\Model\Entity\Date newEmptyEntity()
 * @method \App\Model\Entity\Date newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Date[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Date get($primaryKey, $options = [])
 * @method \App\Model\Entity\Date findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Date patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Date[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Date|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Date saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Date[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Date[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Date[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Date[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class DatesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('dates');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Months', [
            'foreignKey' => 'month_id',
        ]);
        $this->belongsTo('Years', [
            'foreignKey' => 'year_id',
        ]);
        $this->belongsTo('Dynasties', [
            'foreignKey' => 'dynasty_id',
        ]);
        $this->belongsTo('Rulers', [
            'foreignKey' => 'ruler_id',
        ]);
        $this->belongsToMany('Artifacts', [
            'foreignKey' => 'date_id',
            'targetForeignKey' => 'artifact_id',
            'joinTable' => 'artifacts_dates',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('day_no')
            ->maxLength('day_no', 50)
            ->allowEmptyString('day_no');

        $validator
            ->scalar('day_remarks')
            ->allowEmptyString('day_remarks');

        $validator
            ->boolean('is_uncertain')
            ->allowEmptyString('is_uncertain');

        $validator
            ->scalar('month_no')
            ->maxLength('month_no', 50)
            ->allowEmptyString('month_no');

        $validator
            ->scalar('absolute_year')
            ->maxLength('absolute_year', 15)
            ->allowEmptyString('absolute_year');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['month_id'], 'Months'), ['errorField' => 'month_id']);
        $rules->add($rules->existsIn(['year_id'], 'Years'), ['errorField' => 'year_id']);
        $rules->add($rules->existsIn(['dynasty_id'], 'Dynasties'), ['errorField' => 'dynasty_id']);
        $rules->add($rules->existsIn(['ruler_id'], 'Rulers'), ['errorField' => 'ruler_id']);

        return $rules;
    }
}
