<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Utility\Svg\AnnotationSelector;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArtifactAssetAnnotations Model
 *
 * @property \App\Model\Table\AnnotationsTable&\Cake\ORM\Association\BelongsTo $Annotations
 * @property \App\Model\Table\ArtifactAssetsTable&\Cake\ORM\Association\BelongsTo $ArtifactAssets
 * @property \App\Model\Table\UpdateEventsTable&\Cake\ORM\Association\BelongsTo $UpdateEvents
 * @property \App\Model\Table\ArtifactAssetAnnotationBodiesTable&\Cake\ORM\Association\BelongsTo $Bodies
 * @method \App\Model\Entity\ArtifactAssetAnnotation newEmptyEntity()
 * @method \App\Model\Entity\ArtifactAssetAnnotation newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactAssetAnnotation[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ArtifactAssetAnnotationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('artifact_asset_annotations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Bodies', [
            'className' => 'ArtifactAssetAnnotationBodies',
            'foreignKey' => 'annotation_id',
            'dependent' => true,
        ]);
        $this->belongsTo('OriginalAnnotations', [
            'className' => 'ArtifactAssetAnnotations',
            'foreignKey' => 'annotation_id',
        ]);
        $this->belongsTo('ArtifactAssets', [
            'foreignKey' => 'artifact_asset_id',
        ]);
        $this->belongsTo('UpdateEvents', [
            'foreignKey' => 'update_event_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('license')
            ->allowEmptyString('license');

        $validator
            ->scalar('target_selector')
            ->requirePresence('target_selector', 'create')
            ->notEmptyString('target_selector');

        $validator
            ->boolean('is_active')
            ->notEmptyString('is_active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['annotation_id'], 'OriginalAnnotations'), ['errorField' => 'annotation_id']);
        $rules->add($rules->existsIn(['artifact_asset_id'], 'ArtifactAssets'), ['errorField' => 'artifact_asset_id']);
        $rules->add($rules->existsIn(['update_event_id'], 'UpdateEvents'), ['errorField' => 'update_event_id']);

        return $rules;
    }

    /**
     * @param int $assetId
     * @param int|null $revision
     * @return array
     */
    public function getActiveAnnotations(int $assetId, int|null $revision = null): array
    {
        $annotations = $this->find()
            ->where(['ArtifactAssetAnnotations.artifact_asset_id' => $assetId])
            ->contain([
                'ArtifactAssets',
                'Bodies',
                'OriginalAnnotations.UpdateEvents' => ['Creators', 'Authors'],
                'UpdateEvents' => ['Creators', 'Authors'],
            ])
            ->matching('UpdateEvents', function ($q) {
                return $q->where(['UpdateEvents.status' => 'approved']);
            })
            ->order(['UpdateEvents.approved' => 'DESC'])
            ->all()
            ->toArray();

        // Only show the latest versions
        $activeAnnotations = [];
        $showAnnotations = false;
        $uniqueAnnotations = [];
        foreach ($annotations as $annotation) {
            if (is_null($revision) || $annotation->update_event_id === $revision) {
                $showAnnotations = true;
            }

            if (!$showAnnotations) {
                continue;
            }

            $uniqueId = $annotation->annotation_id ?? $annotation->id;
            if (array_key_exists($uniqueId, $uniqueAnnotations)) {
                continue;
            } elseif ($annotation->is_active) {
                $activeAnnotations[] = $annotation;
            }

            $uniqueAnnotations[$uniqueId] = true;
        }

        return $activeAnnotations;
    }

    public function newEntitiesFromJson(array $data)
    {
        if (array_key_exists('type', $data)) {
            if ($data['type'] === 'AnnotationCollection') {
                $data = array_merge($data['first']['items'] ?? [], $data['last']['items'] ?? []);
            } elseif ($data['type'] === 'AnnotationPage') {
                $data = $data['items'] ?? [];
            }
        }

        $annotations = [];
        foreach ($data as $annotation) {
            $annotations[] = $this->newEntityFromJson($annotation);
        }

        return $annotations;
    }

    public function newEntityFromJson(array $data)
    {
        $annotation = $this->newEmptyEntity();
        $annotation->is_active = true;

        if (isset($data['target'])) {
            $target = $data['target'];

            // Target selector
            if (!isset($target['selector'])) {
                $annotation->setError('target_selector', 'Annotation should have an embedded SVG selector');
            }
            $selector = isset($target['selector']['type']) ? $target['selector'] : ($target['selector'][0] ?? null);
            if (!is_null($selector) && $selector['type'] === 'SvgSelector' && isset($selector['value'])) {
                $annotation->target_selector = $selector['value'];
            } else {
                $annotation->setError('target_selector', 'Annotation should have an embedded SVG selector');

                return $annotation;
            }

            // ID of visual asset
            $asset = null;
            if (isset($target['source'])) {
                $source = substr(parse_url($target['source']['id'] ?? $target['source'], PHP_URL_PATH), 1);
                /** @var \App\Model\Entity\ArtifactAsset $asset */
                $asset = $this->ArtifactAssets->find('path', [$source])->contain(['Artifacts'])->first();
            }
            if (is_null($asset)) {
                $annotation->setError('artifact_asset_id', 'Image could not be identified from target source');

                return $annotation;
            } else {
                $annotation->artifact_asset_id = $asset->id;
                $annotation->artifact_asset = $asset;
            }
        } else {
            $annotation->setError('target_selector', 'Annotation should have a target');

            return $annotation;
        }

        if (isset($data['id'])) {
            $annotation->annotation_id = $this->_getOriginalAnnotationId($data['id']);
        }

        $bodies = null;
        if (isset($data['body'])) {
            if (isset($data['body']['type'])) {
                $bodies = [$data['body']];
            } elseif (isset($data['body'][0])) {
                $bodies = $data['body'];
            }
        }
        if (!is_null($bodies)) {
            $annotation->bodies = [];
            foreach ($bodies as $body) {
                $annotation->bodies[] = $this->Bodies->newEntityFromJson($body);
            }
        } else {
            $annotation->setError('bodies', 'Annotation should have at least one body');
        }

        if (isset($data['rights']) && is_string($data['rights'])) {
            $annotation->license = $data['rights'];
        }

        return $annotation;
    }

    public function newEntitiesFromVia(array $data, $labels = [])
    {
        if (array_key_exists('_via_img_metadata', $data)) {
            foreach ($data['_via_attributes']['region'] ?? [] as $attribute) {
                foreach ($attribute['options'] ?? [] as $id => $label) {
                    $labels[$id] = $label;
                }
            }

            return $this->newEntitiesFromVia($data['_via_img_metadata'], $labels);
        }

        $annotations = [];

        foreach ($data as $annotationSetId => $asset) {
            // Check if image has annotations
            if (!array_key_exists('regions', $asset)) {
                continue;
            }

            foreach ($asset['regions'] as $region) {
                $annotations[] = $this->newEntityFromVia($asset, $region, $labels);
            }
        }

        return $annotations;
    }

    public function newEntityFromVia(array $asset, array $region, $labels = [])
    {
        $annotation = $this->newEmptyEntity();
        $annotation->is_active = true;

        // ID of visual asset
        $assetEntity = null;
        if (array_key_exists('filename', $asset) && parse_url($asset['filename'])) {
            $source = substr(parse_url($asset['filename'], PHP_URL_PATH), 1);
            /** @var \App\Model\Entity\ArtifactAsset $assetEntity */
            $assetEntity = $this->ArtifactAssets->find('path', [$source])->contain(['Artifacts'])->first();
        }
        if (is_null($assetEntity)) {
            $annotation->setError('artifact_asset_id', 'Image could not be identified from target source');

            return $annotation;
        } else {
            $annotation->artifact_asset_id = $assetEntity->id;
            $annotation->artifact_asset = $assetEntity;
        }

        // Target selector
        if (!array_key_exists('shape_attributes', $region) || !array_key_exists('name', $region['shape_attributes'])) {
            $annotation->setError('target_selector', 'Annotation should have shape attributes');
        } elseif (!in_array($region['shape_attributes']['name'], ['rect', 'circle', 'ellipse', 'polygon', 'point', 'polyline'])) {
            $annotation->setError('target_selector', 'Annotation should have shape attributes for rect, circle, ellipse, polygon, point, or polyline');
        } else {
            $annotation->target_selector = AnnotationSelector::fromViaJson($region['shape_attributes'])->getSvg();
        }

        // Annotation content
        $annotation->bodies = [];

        if (array_key_exists('region_attributes', $region)) {
            $content = $region['region_attributes'];

            if (isset($content['@id'])) {
                $annotation->annotation_id = $this->_getOriginalAnnotationId($content['@id']);
            }

            foreach ($content as $attribute => $value) {
                if ($attribute === '@id') {
                    continue;
                }
                if (is_array($value)) {
                    $values = array_keys($value);
                } elseif (is_string($value)) {
                    $values = [$value];
                } else {
                    $values = [strval($value)];
                }
                foreach ($values as $value) {
                    $annotation->bodies[] = $this->Bodies->newEntityFromVia($attribute, $value, $labels[$value] ?? null);
                }
            }
        }

        if (count($annotation->bodies) === 0) {
            $annotation->setError('bodies', 'Annotation should have at least one body');
        }

        return $annotation;
    }

    protected function _getOriginalAnnotationId(string $url)
    {
        preg_match('/\/artifact-asset-annotations\/([1-9]\d*)/', $url, $match);

        if (is_null($match) || count($match) === 0) {
            return null;
        }

        /** @var \Cake\ORM\Entity $previousAnnotation */
        $previousAnnotation = $this->find()->where(['id' => (int)$match[1]])->first();

        return $previousAnnotation->annotation_id ?? $previousAnnotation->id ?? null;
    }
}
