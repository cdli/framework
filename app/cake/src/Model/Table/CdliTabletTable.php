<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CdliTablet Model
 *
 * @method \App\Model\Entity\CdliTablet newEmptyEntity()
 * @method \App\Model\Entity\CdliTablet newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\CdliTablet[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CdliTablet get($primaryKey, $options = [])
 * @method \App\Model\Entity\CdliTablet findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\CdliTablet patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CdliTablet[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\CdliTablet|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CdliTablet saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CdliTablet[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\CdliTablet[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\CdliTablet[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\CdliTablet[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class CdliTabletTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('cdli_tablet');
        $this->setDisplayField('title_short');
        $this->setPrimaryKey('id');

        $this->addBehavior('Muffin/Orderly.Orderly', [
            'order' => ['CdliTablet.date_display' => 'DESC'],
        ]);
        $this->addBehavior('Search.Search', [
            'collectionClass' => $this->getAlias(),
        ]);

        $this->belongsTo('Authors', [
            'foreignKey' => 'author_id',
            'className' => 'Authors',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->date('date_display');

        $validator
            ->scalar('text_long');

        $validator
            ->scalar('text_short');

        $validator
            ->scalar('image_filename')
            ->maxLength('path', 255);

        $validator
            ->scalar('title_long')
            ->allowEmptyString('title_long');

        $validator
            ->scalar('title_short');

        $validator
            ->scalar('theme');

        $validator
            ->integer('author_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        $rules->add($rules->existsIn(['author_id'], 'Authors'), ['errorField' => 'posting_type_id']);

        return $rules;
    }
}
