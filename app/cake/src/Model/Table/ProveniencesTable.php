<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Proveniences Model
 *
 * @property \App\Model\Table\LocationsTable&\Cake\ORM\Association\BelongsTo $Locations
 * @property \App\Model\Table\PlacesTable&\Cake\ORM\Association\BelongsTo $Places
 * @property \App\Model\Table\RegionsTable&\Cake\ORM\Association\BelongsTo $Regions
 * @property \App\Model\Table\ArchivesTable&\Cake\ORM\Association\HasMany $Archives
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\HasMany $Artifacts
 * @property \App\Model\Table\DynastiesTable&\Cake\ORM\Association\HasMany $Dynasties
 * @property \App\Model\Table\SignReadingsTable&\Cake\ORM\Association\HasMany $SignReadings
 * @method \App\Model\Entity\Provenience newEmptyEntity()
 * @method \App\Model\Entity\Provenience newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Provenience[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Provenience get($primaryKey, $options = [])
 * @method \App\Model\Entity\Provenience findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Provenience patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Provenience[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Provenience|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Provenience saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Provenience[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Provenience[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Provenience[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Provenience[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ProveniencesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('proveniences');
        $this->setDisplayField('provenience');
        $this->setPrimaryKey('id');

        $this->addBehavior('Muffin/Orderly.Orderly');
        $this->addBehavior('Search.Search', [
            'collectionClass' => $this->getAlias(),
        ]);

        $this->belongsTo('Locations', [
            'foreignKey' => 'location_id',
        ]);
        $this->belongsTo('Places', [
            'foreignKey' => 'place_id',
        ]);
        $this->belongsTo('Regions', [
            'foreignKey' => 'region_id',
        ]);

        $this->hasMany('Archives', [
            'foreignKey' => 'provenience_id',
        ]);
        $this->hasMany('Artifacts', [
            'foreignKey' => 'provenience_id',
        ]);
        $this->hasMany('Dynasties', [
            'foreignKey' => 'provenience_id',
        ]);
        $this->hasMany('SignReadings', [
            'foreignKey' => 'provenience_id',
        ]);
        $this->belongsToMany('ExternalResources', [
            'foreignKey' => 'entity_id',
            'targetForeignKey' => 'external_resource_id',
            'joinTable' => 'entities_external_resources',
            'conditions' => ['EntitiesExternalResources.entity_table' => 'proveniences'],
        ]);
        $this->belongsToMany('Publications', [
            'foreignKey' => 'entity_id',
            'targetForeignKey' => 'publication_id',
            'joinTable' => 'entities_publications',
            'conditions' => ['EntitiesPublications.table_name' => 'proveniences'],
        ]);
        $this->belongsToMany('UpdateEvents', [
            'foreignKey' => 'entity_id',
            'targetForeignKey' => 'update_event_id',
            'joinTable' => 'entities_updates',
            'conditions' => ['EntitiesUpdates.table_name' => 'proveniences'],
            'dependent' => false,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('provenience')
            ->maxLength('provenience', 300)
            ->allowEmptyString('provenience');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        $rules->add($rules->existsIn(['location_id'], 'Locations'), ['errorField' => 'location_id']);
        $rules->add($rules->existsIn(['place_id'], 'Places'), ['errorField' => 'place_id']);
        $rules->add($rules->existsIn(['region_id'], 'Regions'), ['errorField' => 'region_id']);

        return $rules;
    }

    /**
     * @return \Cake\Datasource\EntityInterface
     */
    public function newEmptyEntity(): \Cake\Datasource\EntityInterface
    {
        $entity = parent::newEmptyEntity();
        $entity->publications = [];

        return $entity;
    }
}
