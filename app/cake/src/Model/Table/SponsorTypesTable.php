<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SponsorTypes Model
 *
 * @property \App\Model\Table\SponsorTypesTable&\Cake\ORM\Association\HasMany $Sponsors
 * @method \App\Model\Entity\SponsorType newEmptyEntity()
 * @method \App\Model\Entity\SponsorType newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\SponsorType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SponsorType get($primaryKey, $options = [])
 * @method \App\Model\Entity\SponsorType findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\SponsorType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SponsorType[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\SponsorType|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SponsorType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SponsorType[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\SponsorType[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\SponsorType[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\SponsorType[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class SponsorTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('sponsor_types');
        $this->setDisplayField('sponsor_type');
        $this->setPrimaryKey('id');

        $this->hasMany('Sponsor', [
            'foreignKey' => 'sponsor_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('sponsor_type')
            ->maxLength('sponsor_type', 200)
            ->requirePresence('sponsor_type', 'create')
            ->notEmptyString('sponsor_type');

        return $validator;
    }
}
