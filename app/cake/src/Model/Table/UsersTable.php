<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\AuthorsTable|\Cake\ORM\Association\BelongsTo $Authors
 * @property \App\Model\Table\CollectionsTable|\Cake\ORM\Association\BelongsTo $Collections
 * @property \App\Model\Table\RolesTable|\Cake\ORM\Association\BelongsToMany $Roles
 * @method \App\Model\Entity\User newEmptyEntity()
 * @method \App\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('username');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Orderly.Orderly');
        $this->addBehavior('Search.Search', [
            'collectionClass' => $this->getAlias(),
        ]);

        $this->belongsTo('Authors', [
            'foreignKey' => 'author_id',
        ]);
        $this->belongsTo('Collections', [
            'propertyName' => 'hd_images_collection',
            'foreignKey' => 'hd_images_collection_id',
        ]);
        $this->belongsToMany('Roles', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'role_id',
            'joinTable' => 'roles_users',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 255)
            ->requirePresence('first_name', 'create')
            ->notEmptyString('first_name')
            ->notBlank('first_name', 'Please enter a valid first name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 255)
            ->requirePresence('last_name', 'create')
            ->notEmptyString('last_name')
            ->notBlank('last_name', 'Please enter a valid last name');

        $validator
            ->scalar('username')
            ->maxLength('username', 255)
            ->requirePresence('username', 'create')
            ->notEmptyString('username')
            ->add('username', 'validFormat', [
              'rule' => ['custom', '/^[^\t\n<>;"&\']+$/'],
              'message' => 'Please enter a valid username.',
        ]);

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->dateTime('last_login_at')
            ->requirePresence('last_login_at', 'create')
            ->notEmptyDateTime('last_login_at');

        $validator
            ->boolean('active')
            ->notEmptyString('active');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->minLength('password', 12, __('Passphrase must be minimum 12 characters'))
            ->add('password', 'secure', [
                'rule' => function ($value, $context) {
                    if (isset($context['data']['email'])) {
                        $email = explode('@', $context['data']['email'])[0];

                        if ($value === $email) {
                            return false;
                        }
                    }

                    if (isset($context['data']['username']) && $value === $context['data']['username']) {
                        return false;
                    }

                    return true;
                },
                'message' => __('Please use a more secure password'),
            ])
            ->requirePresence('password', 'create')
            ->notEmptyString('password');

        $validator
            ->scalar('password_confirm')
            ->maxLength('password_confirm', 255)
            ->equalToField(
                'password_confirm',
                'password',
                __('Passwords do not match')
            )
            ->notEmptyString('password_confirm');

        $validator
            ->scalar('2fa_key')
            ->maxLength('2fa_key', 255)
            ->allowEmptyString('2fa_key');

        $validator
            ->boolean('2fa_status')
            ->allowEmptyString('2fa_status');

        $validator
            ->dateTime('created_at')
            ->requirePresence('created_at', 'create')
            ->notEmptyDateTime('created_at');

        $validator
            ->dateTime('modified_at')
            ->requirePresence('modified_at', 'create')
            ->notEmptyDateTime('modified_at');

        $validator
            ->scalar('token_pass')
            ->maxLength('token_pass', 50)
            ->allowEmptyString('token_pass');

        $validator
            ->dateTime('generated_at')
            ->allowEmptyDateTime('generated_at');

        return $validator;
    }

    public function validationRegister(Validator $validator): Validator
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->requirePresence('password_confirm');

        $validator
            ->requirePresence('accept_privacy_policy')
            ->notEmptyString('accept_privacy_policy')
            ->boolean('accept_privacy_policy');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['username']), ['errorField' => 'username']);
        $rules->add($rules->isUnique(['email']), ['errorField' => 'email']);
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        $rules->add($rules->existsIn(['author_id'], 'Authors'), ['errorField' => 'author_id']);
        $rules->add($rules->existsIn(['hd_images_collection_id'], 'Collections'), ['errorField' => 'hd_images_collection_id']);

        return $rules;
    }
}
