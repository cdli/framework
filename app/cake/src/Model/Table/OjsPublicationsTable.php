<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OjsPublications Model
 *
 * @property \App\Model\Table\PrimaryContactsTable&\Cake\ORM\Association\BelongsTo $PrimaryContacts
 * @property \App\Model\Table\OjsSectionsTable&\Cake\ORM\Association\BelongsTo $OjsSections
 * @property \App\Model\Table\OjsSubmissionsTable&\Cake\ORM\Association\BelongsTo $OjsSubmissions
 * @method \App\Model\Entity\OjsPublication newEmptyEntity()
 * @method \App\Model\Entity\OjsPublication newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\OjsPublication[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OjsPublication get($primaryKey, $options = [])
 * @method \App\Model\Entity\OjsPublication findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\OjsPublication patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OjsPublication[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\OjsPublication|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OjsPublication saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OjsPublication[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsPublication[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsPublication[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsPublication[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class OjsPublicationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('publications');
        $this->setDisplayField('publication_id');
        $this->setPrimaryKey('publication_id');

        $this->belongsTo('PrimaryContacts', [
            'foreignKey' => 'primary_contact_id',
        ]);
        $this->belongsTo('OjsSections', [
            'foreignKey' => 'section_id',
        ]);
        $this->belongsTo('OjsSubmissions', [
            'foreignKey' => 'submission_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('publication_id', null, 'create');

        $validator
            ->allowEmptyString('access_status');

        $validator
            ->date('date_published')
            ->allowEmptyDate('date_published');

        $validator
            ->dateTime('last_modified')
            ->allowEmptyDateTime('last_modified');

        $validator
            ->scalar('locale')
            ->maxLength('locale', 14)
            ->allowEmptyString('locale');

        $validator
            ->numeric('seq')
            ->notEmptyString('seq');

        $validator
            ->notEmptyString('status');

        $validator
            ->scalar('url_path')
            ->maxLength('url_path', 64)
            ->allowEmptyString('url_path');

        $validator
            ->allowEmptyString('version');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['primary_contact_id'], 'PrimaryContacts'), ['errorField' => 'primary_contact_id']);
        $rules->add($rules->existsIn(['section_id'], 'OjsSections'), ['errorField' => 'section_id']);
        $rules->add($rules->existsIn(['submission_id'], 'OjsSubmission'), ['errorField' => 'submission_id']);

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'ojsdb';
    }
}
