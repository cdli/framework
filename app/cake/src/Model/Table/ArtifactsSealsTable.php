<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArtifactsSeals Model
 *
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\BelongsTo $Seals
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\BelongsTo $Impressions
 * @method \App\Model\Entity\ArtifactsSeal newEmptyEntity()
 * @method \App\Model\Entity\ArtifactsSeal newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsSeal[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsSeal get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArtifactsSeal findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ArtifactsSeal patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsSeal[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsSeal|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsSeal saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsSeal[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactsSeal[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactsSeal[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactsSeal[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ArtifactsSealsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('artifacts_seals');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Seals', [
            'bindingKey' => 'seal_no',
            'foreignKey' => 'seal_no',
            'className' => 'Artifacts',
        ]);
        $this->belongsTo('Impressions', [
            'bindingKey' => 'id',
            'foreignKey' => 'artifact_id',
            'className' => 'Artifacts',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('seal_no')
            ->maxLength('seal_no', 45)
            ->allowEmptyString('seal_no');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'), ['errorField' => 'artifact_id']);

        return $rules;
    }
}
