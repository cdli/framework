<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Entities Names Model
 *
 * @property \App\Model\Table\CollectionsTable&\Cake\ORM\Association\BelongsTo $Collections
 * @property \App\Model\Table\LocationsTable&\Cake\ORM\Association\BelongsTo $Locations
 * @method \App\Model\Entity\EntitiesName newEmptyEntity()
 * @method \App\Model\Entity\EntitiesName newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\EntitiesName[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\EntitiesName get($primaryKey, $options = [])
 * @method \App\Model\Entity\EntitiesName findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\EntitiesName patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\EntitiesName[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\EntitiesName|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EntitiesName saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EntitiesName[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\EntitiesName[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\EntitiesName[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\EntitiesName[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class EntitiesNamesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('entities_names');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Collections', [
            'foreignKey' => 'entity_id',
            'conditions' => ['entity_table' => 'collections'],
        ]);
        $this->belongsTo('Locations', [
            'foreignKey' => 'entity_id',
            'conditions' => ['entity_table' => 'locations'],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('entity_table')
            ->maxLength('entity_table', 255)
            ->allowEmptyString('entity_table');

        $validator
            ->nonNegativeInteger('entity_id')
            ->allowEmptyString('entity_id');

        $validator
            ->scalar('language_ietf')
            ->maxLength('language_ietf', 255)
            ->allowEmptyString('language_ietf');

        $validator
            ->scalar('name')
            ->allowEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        $rules->add($rules->isUnique(['entity_table', 'entity_id', 'language_ietf'], 'A name in this language already exists'));

        return $rules;
    }
}
