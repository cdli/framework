<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OjsUsers Model
 *
 * @property \App\Model\Table\AuthsTable&\Cake\ORM\Association\BelongsTo $Auths
 * @property \App\Model\Table\EmailLogTable&\Cake\ORM\Association\BelongsToMany $EmailLog
 * @property \App\Model\Table\OjsUserSettingsTable&\Cake\ORM\Association\HasMany $OjsUserSettings
 * @method \App\Model\Entity\OjsUsers newEmptyEntity()
 * @method \App\Model\Entity\OjsUsers newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\OjsUsers[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OjsUsers get($primaryKey, $options = [])
 * @method \App\Model\Entity\OjsUsers findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\OjsUsers patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OjsUsers[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\OjsUsers|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OjsUsers saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OjsUsers[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsUsers[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsUsers[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsUsers[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class OjsUsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('user_id');
        $this->setPrimaryKey('user_id');

        $this->belongsTo('Auths', [
            'foreignKey' => 'auth_id',
        ]);
        $this->belongsToMany('EmailLog', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'email_log_id',
            'joinTable' => 'email_log_users',
        ]);
        $this->hasMany('OjsUserSettings', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('user_id', null, 'create');

        $validator
            ->scalar('username')
            ->maxLength('username', 32)
            ->requirePresence('username', 'create')
            ->notEmptyString('username')
            ->add('username', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->notEmptyString('password');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email')
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('url')
            ->maxLength('url', 2047)
            ->allowEmptyString('url');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 32)
            ->allowEmptyString('phone');

        $validator
            ->scalar('mailing_address')
            ->maxLength('mailing_address', 255)
            ->allowEmptyString('mailing_address');

        $validator
            ->scalar('billing_address')
            ->maxLength('billing_address', 255)
            ->allowEmptyString('billing_address');

        $validator
            ->scalar('country')
            ->maxLength('country', 90)
            ->allowEmptyString('country');

        $validator
            ->scalar('locales')
            ->maxLength('locales', 255)
            ->allowEmptyString('locales');

        $validator
            ->scalar('gossip')
            ->allowEmptyString('gossip');

        $validator
            ->dateTime('date_last_email')
            ->allowEmptyDateTime('date_last_email');

        $validator
            ->dateTime('date_registered')
            ->requirePresence('date_registered', 'create')
            ->notEmptyDateTime('date_registered');

        $validator
            ->dateTime('date_validated')
            ->allowEmptyDateTime('date_validated');

        $validator
            ->dateTime('date_last_login')
            ->requirePresence('date_last_login', 'create')
            ->notEmptyDateTime('date_last_login');

        $validator
            ->allowEmptyString('must_change_password');

        $validator
            ->scalar('auth_str')
            ->maxLength('auth_str', 255)
            ->allowEmptyString('auth_str');

        $validator
            ->notEmptyString('disabled');

        $validator
            ->scalar('disabled_reason')
            ->allowEmptyString('disabled_reason');

        $validator
            ->allowEmptyString('inline_help');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['username']), ['errorField' => 'username']);
        $rules->add($rules->isUnique(['email']), ['errorField' => 'email']);
        $rules->add($rules->existsIn(['auth_id'], 'Auths'), ['errorField' => 'auth_id']);

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'ojsdb';
    }
}
