<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SignReadingsComments Model
 *
 * @property \App\Model\Table\SignReadingsTable&\Cake\ORM\Association\BelongsTo $SignReadings
 * @property \App\Model\Table\AuthorsTable&\Cake\ORM\Association\BelongsTo $Authors
 * @method \App\Model\Entity\SignReadingsComment newEmptyEntity()
 * @method \App\Model\Entity\SignReadingsComment newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\SignReadingsComment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SignReadingsComment get($primaryKey, $options = [])
 * @method \App\Model\Entity\SignReadingsComment findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\SignReadingsComment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SignReadingsComment[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\SignReadingsComment|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SignReadingsComment saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SignReadingsComment[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\SignReadingsComment[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\SignReadingsComment[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\SignReadingsComment[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class SignReadingsCommentsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('sign_readings_comments');
        $this->setDisplayField('comment');
        $this->setPrimaryKey('id');

        $this->belongsTo('SignReadings', [
            'foreignKey' => 'sign_reading_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Authors', [
            'foreignKey' => 'author_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('comment')
            ->requirePresence('comment', 'create')
            ->notEmptyString('comment');

        $validator
            ->date('date')
            ->requirePresence('date', 'create')
            ->notEmptyDate('date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['sign_reading_id'], 'SignReadings'), ['errorField' => 'sign_reading_id']);
        $rules->add($rules->existsIn(['author_id'], 'Authors'), ['errorField' => 'author_id']);

        return $rules;
    }
}
