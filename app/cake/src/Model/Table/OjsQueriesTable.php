<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OjsQueries Model
 *
 * @property \App\Model\Table\OjsNotesTable&\Cake\ORM\Association\HasMany $OjsNotes
 * @method \App\Model\Entity\OjsQuery newEmptyEntity()
 * @method \App\Model\Entity\OjsQuery newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\OjsQuery[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OjsQuery get($primaryKey, $options = [])
 * @method \App\Model\Entity\OjsQuery findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\OjsQuery patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OjsQuery[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\OjsQuery|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OjsQuery saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OjsQuery[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsQuery[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsQuery[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsQuery[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class OjsQueriesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('queries');
        $this->setDisplayField('query_id');
        $this->setPrimaryKey('query_id');

        $this->hasMany('OjsNotes', [
            'foreignKey' => 'assoc_id',
            'bindingKey' => 'query_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('query_id', null, 'create');

        $validator
            ->requirePresence('assoc_type', 'create')
            ->notEmptyString('assoc_type');

        $validator
            ->numeric('seq')
            ->notEmptyString('seq');

        $validator
            ->dateTime('date_posted')
            ->allowEmptyDateTime('date_posted');

        $validator
            ->dateTime('date_modified')
            ->allowEmptyDateTime('date_modified');

        $validator
            ->notEmptyString('closed');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['assoc_id'], 'Assocs'), ['errorField' => 'assoc_id']);
        $rules->add($rules->existsIn(['stage_id'], 'Stages'), ['errorField' => 'stage_id']);

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'ojsdb';
    }
}
