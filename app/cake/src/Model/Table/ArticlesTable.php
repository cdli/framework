<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Model\Table;

use App\Model\Entity\Article;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\I18n\FrozenDate;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Articles Model
 *
 * @property \App\Model\Table\AuthorsTable&\Cake\ORM\Association\BelongsToMany $Authors
 * @property \App\Model\Table\PublicationsTable&\Cake\ORM\Association\BelongsToMany $Publications
 * @property \App\Model\Table\OjsSubmissionsTable&\Cake\ORM\Association\BelongsTo $OjsSubmissions
 * @property \App\Model\Table\OjsReviewAssignmentsTable&\Cake\ORM\Association\HasMany $OjsReviewAssignments
 * @property \App\Model\Table\OjsReviewRoundsTable&\Cake\ORM\Association\HasMany $OjsReviewRounds
 * @property \App\Model\Table\QueriesTable&\Cake\ORM\Association\HasMany $Queries
 * @method \App\Model\Entity\Article newEmptyEntity()
 * @method \App\Model\Entity\Article newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Article[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Article get($primaryKey, $options = [])
 * @method \App\Model\Entity\Article findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Article patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Article[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Article|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Article saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Article[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Article[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Article[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Article[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ArticlesTable extends Table
{
    /**
     *       | html | latex | pdf |
     *       |------|-------|-----|
     * cdln: |    x |       |     |
     * cdlp: |      |       |   x |
     * cdlj: |    x |     x |   x |
     * cdlb: |    x |     x |   x |
     *
     * @var array
     */
    public $articleContentTypes = [
        'cdln' => ['html' => true, 'latex' => false, 'pdf' => false],
        'cdlp' => ['html' => false, 'latex' => false, 'pdf' => true],
        'cdlj' => ['html' => true, 'latex' => true, 'pdf' => true],
        'cdlb' => ['html' => true, 'latex' => true, 'pdf' => true],
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('articles');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Orderly.Orderly', [
            'order' => [
                'Articles.year' => 'DESC',
                'natural_sort_key(Articles.article_no)' => 'DESC',
            ],
        ]);
        $this->addBehavior('Search.Search', [
            'collectionClass' => $this->getAlias(),
        ]);

        $this->belongsToMany('Authors', [
            'foreignKey' => 'article_id',
            'targetForeignKey' => 'author_id',
            'joinTable' => 'articles_authors',
            'sort' => ['ArticlesAuthors.sequence' => 'ASC'],
        ]);
        $this->belongsTo('Creators', [
            'foreignKey' => 'created_by',
            'className' => 'Authors',
        ]);
        $this->belongsToMany('Publications', [
            'foreignKey' => 'article_id',
            'targetForeignKey' => 'publication_id',
            'joinTable' => 'articles_publications',
        ]);
        $this->belongsTo('OjsSubmissions');
        $this->hasMany('OjsReviewAssignments', [
            'foreignKey' => 'submission_id',
            'bindingKey' => 'submission_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('OjsQueries', [
            'foreignKey' => 'assoc_id',
            'bindingKey' => 'submission_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('OjsReviewRounds', [
            'foreignKey' => 'submission_id',
            'bindingKey' => 'submission_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('title')
            ->maxLength('title', 350)
            ->requirePresence('title', 'create')
            ->notEmptyString('title');

        $validator
            ->scalar('article_no')
            ->requirePresence('article_no', 'create')
            ->notEmpty('article_no')
            ->regex('article_no', '/^[1-9]/', 'Do not add preceding zeroes');

        $validator
            ->integer('year')
            ->requirePresence('year', 'create')
            ->notEmpty('year');

        $validator
            ->scalar('content_html')
            ->maxLength('content_html', 4294967295)
            ->requirePresence('content_html', 'create');

        $validator
            ->scalar('content_latex')
            ->maxLength('content_latex', 4294967295)
            ->requirePresence('content_latex', 'create');

        $validator
            ->boolean('is_pdf_uploaded')
            ->requirePresence('is_pdf_uploaded', 'create')
            ->notEmptyString('is_pdf_uploaded');

        $validator
            ->scalar('article_type')
            ->requirePresence('article_type', 'create')
            ->notEmptyString('article_type');

        $validator
            ->integer('is_published')
            ->requirePresence('is_published', 'create')
            ->notEmptyString('is_published');

        $validator
            ->integer('article_status')
            ->requirePresence('article_status', 'create')
            ->notEmptyString('article_status');

        $validator
            ->scalar('pdf_link')
            ->maxLength('pdf_link', 4294967295)
            ->requirePresence('pdf_link', 'create');

        $validator
            ->nonNegativeInteger('created_by')
            ->requirePresence('created_by', 'create')
            ->notEmptyString('created_by');

        return $validator;
    }

    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(
            ['article_type', 'year', 'article_no'],
            [
                'message' => 'Combination of journal, year and number',
                'allowMultipleNulls' => true,
            ]
        ));

        return $rules;
    }

    public function newArticle(string $type, array $data, int $creator)
    {
        $article = $this->newEmptyEntity();
        $article->article_type = $type;
        $article->created_by = $creator;

        // These are modified later if a PDF is available.
        $article->pdf_link = '';
        $article->is_pdf_uploaded = false;

        $article = $this->patchArticle($article, $data);

        if (empty($article->created)) {
            $article->created = date('y/m/d');
        }

        // patchEntity() incorrectly reports errors for article_type, created_by,
        // pdf_link, and is_pdf_uploaded even though they are defined *because*
        // they are defined outside of the function call. This resets the errors.
        $article->setErrors([
            'article_type' => [],
            'created_by' => [],
            'pdf_link' => [],
            'is_pdf_uploaded' => [],
        ], true);

        return $article;
    }

    public function patchArticle(Article $article, array $data)
    {
        if (empty($data['authors'])) {
            $data['authors'] = [];
        }

        if (!$article->isNew()) {
            unset($data['year']);
            unset($data['article_no']);
        }

        $article = $this->patchEntity($article, $data);
        $article->is_published = $article->article_status == 3 ? 1 : 0;

        if (empty($article->modified)) {
            $article->modified = new FrozenDate();
        }

        $articleType = $this->articleContentTypes[$article->article_type];
        if (!$articleType['html'] || !isset($article->content_html)) {
            $article->content_html = '';
        }
        if (!$articleType['latex'] || !isset($article->content_latex)) {
            $article->content_latex = '';
        }

        return $article;
    }

    public function findType(Query $query, array $options)
    {
        $type = $options['type'];

        return $query->where(['article_type' => $type]);
    }

    public function getByNumber($type, $id, $options = [])
    {
        if ($type === 'cdlp') {
            $conditions = [
                'article_type' => $type,
                'article_no' => $id,
            ];
        } else {
            $parts = explode('-', $id);
            if (count($parts) === 2) {
                $conditions = [
                    'article_type' => $type,
                    'year' => $parts[0],
                    'article_no' => $parts[1],
                ];
            } else {
                $conditions = [
                    'id' => $id,
                ];
            }
        }

        $article = $this->find('all', $options)->where($conditions)->first();

        if (is_null($article)) {
            throw new RecordNotFoundException();
        }

        return $article;
    }

    public function substituteImageUrls(Article $article)
    {
        $id = $article->getArticleNumberPath();
        $type = $article->article_type;
        $articleContent = $this->articleContentTypes[$type];

        if ($articleContent['latex'] && !empty($article->content_html)) {
            $article->content_html = preg_replace(
                '/%%ARTICLE_ID%%/',
                $type . '/' . $type . '-' . $id,
                $article->content_html
            );
        }
    }
}
