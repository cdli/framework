<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UpdateEvents Model
 *
 * @property \App\Model\Table\AuthorsTable&\Cake\ORM\Association\BelongsTo $Creators
 * @property \App\Model\Table\AuthorsTable&\Cake\ORM\Association\BelongsTo $Reviewers
 * @property \App\Model\Table\ExternalResourcesTable&\Cake\ORM\Association\BelongsTo $ExternalResources
 * @property \App\Model\Table\InscriptionsTable&\Cake\ORM\Association\HasMany $Inscriptions
 * @property \App\Model\Table\ArtifactsUpdatesTable&\Cake\ORM\Association\HasMany $ArtifactsUpdates
 * @property \App\Model\Table\ArtifactAssetAnnotationsTable&\Cake\ORM\Association\HasMany $ArtifactAssetAnnotations
 * @property \App\Model\Table\EntitiesUpdatesTable&\Cake\ORM\Association\HasMany $EntitiesUpdates
 * @property \App\Model\Table\AuthorsTable&\Cake\ORM\Association\BelongsToMany $Authors
 * @method \App\Model\Entity\UpdateEvent newEmptyEntity()
 * @method \App\Model\Entity\UpdateEvent newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\UpdateEvent[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UpdateEvent get($primaryKey, $options = [])
 * @method \App\Model\Entity\UpdateEvent findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\UpdateEvent patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UpdateEvent[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\UpdateEvent|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UpdateEvent saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UpdateEvent[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\UpdateEvent[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\UpdateEvent[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\UpdateEvent[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UpdateEventsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('update_events');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Orderly.Orderly', [
            'order' => ['UpdateEvents.created' => 'DESC'],
        ]);
        $this->addBehavior('Search.Search', [
            'collectionClass' => $this->getAlias(),
        ]);

        $this->belongsTo('ExternalResources', [
            'foreignKey' => 'external_resource_id',
        ]);
        $this->hasMany('Inscriptions', [
            'dependent' => true,
            'foreignKey' => 'update_event_id',
        ]);
        $this->hasMany('ArtifactsUpdates', [
            'dependent' => true,
            'foreignKey' => 'update_events_id',
        ]);
        $this->hasMany('ArtifactAssetAnnotations', [
            'dependent' => true,
            'foreignKey' => 'update_event_id',
        ]);
        $this->hasMany('EntitiesUpdates', [
            'dependent' => true,
            'foreignKey' => 'update_event_id',
        ]);
        $this->belongsTo('Creators', [
            'foreignKey' => 'created_by',
            'className' => 'Authors',
        ]);
        $this->belongsTo('Reviewers', [
            'foreignKey' => 'approved_by',
            'className' => 'Authors',
        ]);
        $this->belongsToMany('Authors', [
            'foreignKey' => 'update_event_id',
            'targetForeignKey' => 'author_id',
            'joinTable' => 'authors_update_events',
        ]);
    }

    public function getSchema(): \Cake\Database\Schema\TableSchemaInterface
    {
        return parent::getSchema()->setColumnType('update_type', 'set');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->nonNegativeInteger('created_by')
            ->allowEmptyString('created_by');

        $validator
            ->scalar('event_comments')
            ->allowEmptyString('event_comments');

        $validator
            ->nonNegativeInteger('approved_by')
            ->allowEmptyString('approved_by');

        $validator
            ->scalar('status')
            ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['external_resource_id'], 'ExternalResources'), ['errorField' => 'external_resource_id']);

        return $rules;
    }

    public function findArtifact(Query $query, array $options)
    {
        $artifact = $options['artifact'];

        $ids = $this->Inscriptions->find()
            ->select(['update_event_id' => 'update_event_id'])
            ->distinct()
            ->where(['artifact_id' => $artifact])
            ->union($this->ArtifactsUpdates->find()
                ->select(['update_event_id' => 'update_events_id'])
                ->distinct()
                ->where(['artifact_id' => $artifact]))
            ->union($this->ArtifactAssetAnnotations->find()
                ->select(['update_event_id' => 'update_event_id'])
                ->distinct()
                ->contain('ArtifactAssets')
                ->where(['ArtifactAssets.artifact_id' => $artifact]))
            ->all()
            ->toArray();
        $ids = array_column($ids, 'update_event_id');

        if (count($ids) === 0) {
            return $query->where(['1 = 0']);
        } else {
            return $query->where(['UpdateEvents.id IN' => $ids]);
        }
    }
}
