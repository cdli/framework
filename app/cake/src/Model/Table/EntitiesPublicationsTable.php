<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EntitiesPublications Model
 *
 * @property \App\Model\Table\ArtifactAssetsTable&\Cake\ORM\Association\BelongsTo $ArtifactAssets
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsTo $Artifacts
 * @property \App\Model\Table\ProveniencesTable&\Cake\ORM\Association\BelongsTo $Proveniences
 * @property \App\Model\Table\PublicationsTable&\Cake\ORM\Association\BelongsTo $Publications
 * @method \App\Model\Entity\EntitiesPublication newEmptyEntity()
 * @method \App\Model\Entity\EntitiesPublication newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\EntitiesPublication[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\EntitiesPublication get($primaryKey, $options = [])
 * @method \App\Model\Entity\EntitiesPublication findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\EntitiesPublication patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\EntitiesPublication[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\EntitiesPublication|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EntitiesPublication saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EntitiesPublication[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\EntitiesPublication[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\EntitiesPublication[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\EntitiesPublication[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class EntitiesPublicationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('entities_publications');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('ArtifactAssets', [
            'foreignKey' => 'entity_id',
            'conditions' => 'table_name = "artifact_assets"',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Artifacts', [
            'foreignKey' => 'entity_id',
            'conditions' => 'table_name = "artifacts"',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Proveniences', [
            'foreignKey' => 'entity_id',
            'conditions' => 'table_name = "proveniences"',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Publications', [
            'foreignKey' => 'publication_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('exact_reference')
            ->maxLength('exact_reference', 255)
            ->allowEmptyString('exact_reference');

        $validator
            ->scalar('publication_type')
            ->allowEmptyString('publication_type');

        $validator
            ->scalar('publication_comments')
            ->allowEmptyString('publication_comments');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        //$rules->add($rules->existsIn(['entity_id'], 'Artifacts'), ['errorField' => 'artifact_id']);
        $rules->add($rules->existsIn(['publication_id'], 'Publications'), ['errorField' => 'publication_id']);

        $rules->add($rules->isUnique(
            ['publication_id', 'entity_id', 'exact_reference', 'publication_type', 'table_name'],
            'This link already exists'
        ));

        return $rules;
    }
}
