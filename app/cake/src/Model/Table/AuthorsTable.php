<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Model\Table;

use ArrayObject;
use Cake\Event\Event;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Authors Model
 *
 * @property \App\Model\Table\StaffTable&\Cake\ORM\Association\hasOne $Staff
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\hasOne $Users
 * @property \App\Model\Table\SignReadingsCommentsTable&\Cake\ORM\Association\HasMany $SignReadingsComments
 * @property \App\Model\Table\CdliTabletTable&\Cake\ORM\Association\HasMany $CdliTablet
 * @property \App\Model\Table\ArticlesTable&\Cake\ORM\Association\BelongsToMany $Articles
 * @property \App\Model\Table\PublicationsTable&\Cake\ORM\Association\BelongsToMany $Publications
 * @property \App\Model\Table\UpdateEventsTable&\Cake\ORM\Association\BelongsToMany $UpdateEvents
 * @method \App\Model\Entity\Author newEmptyEntity()
 * @method \App\Model\Entity\Author newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Author[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Author get($primaryKey, $options = [])
 * @method \App\Model\Entity\Author findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Author patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Author[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Author|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Author saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Author[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Author[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Author[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Author[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AuthorsTable extends Table
{
    use LocatorAwareTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('authors');
        $this->setDisplayField('author');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Orderly.Orderly');
        $this->addBehavior('Search.Search', [
            'collectionClass' => $this->getAlias(),
        ]);

        $this->hasOne('Staff', [
            'foreignKey' => 'author_id',
        ]);
        $this->hasOne('Users', [
            'foreignKey' => 'author_id',
        ]);
        $this->hasMany('SignReadingsComments', [
            'foreignKey' => 'author_id',
        ]);
        $this->hasMany('CdliTablet', [
            'foreignKey' => 'author_id',
        ]);
        $this->belongsToMany('Articles', [
            'foreignKey' => 'author_id',
            'targetForeignKey' => 'article_id',
            'joinTable' => 'articles_authors',
        ]);
        $this->belongsToMany('Publications', [
            'foreignKey' => 'author_id',
            'targetForeignKey' => 'publication_id',
            'joinTable' => 'authors_publications',
        ]);

        $this->belongsToMany('UpdateEvents', [
            'foreignKey' => 'author_id',
            'targetForeignKey' => 'update_event_id',
            'joinTable' => 'authors_update_events',
        ]);
    }

    /**
     * Convert input data to required format.
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        foreach ($data as $key => $value) {
            if ($key != '_joinData') {
                $data[$key] = trim($value);
            }
        }

        if (isset($data['first']) && isset($data['last'])) {
            if ($data['first'] == '') {
                $data['author'] = $data['last'];
            } elseif ($data['last'] == '') {
                $data['author'] = $data['first'];
            } else {
                if ($data['east_asian_order']) {
                    $data['author'] = $data['last'] . ' ' . $data['first'];
                } else {
                    $data['author'] = $data['last'] . ', ' . $data['first'];
                }
            }
        }
    }

    public function findForDelete(Query $query, array $options): Query
    {
        return $query
            ->contain([
                'Staff',
                'Users',
            ])
            ->formatResults(function ($results) {
                return $results->each(function ($entity) {
                    $entity->sign_readings_comments_count = $this->SignReadingsComments->find()
                        ->where(['SignReadingsComments.author_id' => $entity->id])
                        ->count();
                    $entity->cdli_tablet_count = $this->CdliTablet->find()
                        ->where(['CdliTablet.author_id' => $entity->id])
                        ->count();

                    $entity->articles_count = $this->getTableLocator()->get('ArticlesAuthors')->find()
                        ->where(['ArticlesAuthors.author_id' => $entity->id])
                        ->count();
                    $entity->publications_count = $this->getTableLocator()->get('AuthorsPublications')->find()
                        ->where(['AuthorsPublications.author_id' => $entity->id])
                        ->count();
                    $entity->update_events_count = $this->getTableLocator()->get('AuthorsUpdateEvents')->find()
                        ->where(['AuthorsUpdateEvents.author_id' => $entity->id])
                        ->count();
                });
            });
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('author')
            ->maxLength('author', 300)
            ->requirePresence('author', 'create')
            ->notEmptyString('author');

        $validator
            ->scalar('last')
            ->maxLength('last', 150)
            ->allowEmptyString('last');

        $validator
            ->scalar('first')
            ->maxLength('first', 150)
            ->allowEmptyString('first');

        $validator
            ->boolean('east_asian_order')
            ->notEmptyString('east_asian_order');

        $validator
            ->email('email')
            ->allowEmptyString('email');

        $validator
            ->scalar('institution')
            ->maxLength('institution', 255)
            ->allowEmptyString('institution');

        $validator
            ->scalar('orcid_id')
            ->maxLength('orcid_id', 19)
            ->minLength('orcid_id', 19)
            ->allowEmpty('orcid_id');

        $validator
            ->integer('birth_year')
            ->lengthBetween('birth_year', [4, 4], __('Year must be 4 digits'))
            ->allowEmptyString('birth_year');

        $validator
            ->integer('death_year')
            ->lengthBetween('death_year', [4, 4], __('Year must be 4 digits'))
            ->allowEmptyString('death_year');

        $validator
            ->scalar('biography')
            ->maxLength('biography', 65000)
            ->allowEmptyString('biography');

        return $validator;
    }
}
