<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OjsNotes Model
 *
 * @property \App\Model\Table\AssocsTable&\Cake\ORM\Association\BelongsTo $Assocs
 * @property \App\Model\Table\OjsUserSettingsTable&\Cake\ORM\Association\HasMany $OjsUserSettings
 * @method \App\Model\Entity\OjsNote newEmptyEntity()
 * @method \App\Model\Entity\OjsNote newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\OjsNote[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OjsNote get($primaryKey, $options = [])
 * @method \App\Model\Entity\OjsNote findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\OjsNote patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OjsNote[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\OjsNote|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OjsNote saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OjsNote[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsNote[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsNote[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\OjsNote[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class OjsNotesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('notes');
        $this->setDisplayField('title');
        $this->setPrimaryKey('note_id');

        $this->belongsTo('Assocs', [
            'foreignKey' => 'assoc_id',
        ]);
        $this->hasMany('OjsUserSettings', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('note_id', null, 'create');

        $validator
            ->allowEmptyString('assoc_type');

        $validator
            ->dateTime('date_created')
            ->requirePresence('date_created', 'create')
            ->notEmptyDateTime('date_created');

        $validator
            ->dateTime('date_modified')
            ->allowEmptyDateTime('date_modified');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->allowEmptyString('title');

        $validator
            ->scalar('contents')
            ->allowEmptyString('contents');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['assoc_id'], 'Assocs'), ['errorField' => 'assoc_id']);
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'ojsdb';
    }
}
