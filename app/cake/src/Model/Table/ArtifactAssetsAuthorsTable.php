<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * @property \App\Model\Table\ArtifactAssetsTable&\Cake\ORM\Association\BelongsTo $ArtifactAssets
 * @property \App\Model\Table\AuthorsTable&\Cake\ORM\Association\BelongsTo $Authors
 * @method \App\Model\Entity\ArtifactAssetsAuthor newEmptyEntity()
 * @method \App\Model\Entity\ArtifactAssetsAuthor newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactAssetsAuthor[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactAssetsAuthor get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArtifactAssetsAuthor findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ArtifactAssetsAuthor patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactAssetsAuthor[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactAssetsAuthor|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactAssetsAuthor saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactAssetsAuthor[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactAssetsAuthor[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactAssetsAuthor[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactAssetsAuthor[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ArtifactAssetsAuthorsTable extends Table
{
    /**
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('artifact_assets_authors');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('ArtifactAssets', [
            'foreignKey' => 'artifact_asset_id',
        ]);
        $this->belongsTo('Authors', [
            'foreignKey' => 'author_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('role')
            ->maxLength('role', 255)
            ->allowEmptyString('role');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['artifact_asset_id'], 'ArtifactAssets'), ['errorField' => 'artifact_asset_id']);
        $rules->add($rules->existsIn(['author_id'], 'Authors'), ['errorField' => 'author_id']);
        $rules->add($rules->isUnique(['artifact_asset_id', 'author_id', 'role'], 'This contribution is listed twice'));

        return $rules;
    }
}
