<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Entities External Resources Model
 *
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsTo $Artifacts
 * @property \App\Model\Table\CollectionsTable&\Cake\ORM\Association\BelongsTo $Collections
 * @property \App\Model\Table\LocationsTable&\Cake\ORM\Association\BelongsTo $Locations
 * @property \App\Model\Table\PlacesTable&\Cake\ORM\Association\BelongsTo $Places
 * @property \App\Model\Table\ExternalResourcesTable&\Cake\ORM\Association\BelongsTo $ExternalResources
 * @method \App\Model\Entity\EntitiesExternalResource newEmptyEntity()
 * @method \App\Model\Entity\EntitiesExternalResource newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\EntitiesExternalResource[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\EntitiesExternalResource get($primaryKey, $options = [])
 * @method \App\Model\Entity\EntitiesExternalResource findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\EntitiesExternalResource patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\EntitiesExternalResource[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\EntitiesExternalResource|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EntitiesExternalResource saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EntitiesExternalResource[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\EntitiesExternalResource[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\EntitiesExternalResource[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\EntitiesExternalResource[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class EntitiesExternalResourcesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('entities_external_resources');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Artifacts', [
            'foreignKey' => 'entity_id',
            'conditions' => ['entity_table' => 'artifacts'],
        ]);
        $this->belongsTo('Collections', [
            'foreignKey' => 'entity_id',
            'conditions' => ['entity_table' => 'collections'],
        ]);
        $this->belongsTo('Locations', [
            'foreignKey' => 'entity_id',
            'conditions' => ['entity_table' => 'locations'],
        ]);
        $this->belongsTo('Places', [
            'foreignKey' => 'entity_id',
            'conditions' => ['entity_table' => 'places'],
        ]);

        $this->belongsTo('ExternalResources', [
            'foreignKey' => 'external_resource_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('entity_table')
            ->maxLength('entity_table', 255)
            ->allowEmptyString('entity_table');

        $validator
            ->nonNegativeInteger('entity_id')
            ->allowEmptyString('entity_id');

        $validator
            ->scalar('external_resource_key')
            ->maxLength('external_resource_key', 255)
            ->allowEmptyString('external_resource_key');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['external_resource_id'], 'ExternalResources'), ['errorField' => 'external_resource_id']);
        $rules->add($rules->isUnique(['entity_table', 'entity_id', 'external_resource_id', 'external_resource_key'], 'This link already exists'));

        return $rules;
    }
}
