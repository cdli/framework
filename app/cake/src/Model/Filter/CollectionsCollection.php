<?php
declare(strict_types=1);

namespace App\Model\Filter;

use Search\Model\Filter\FilterCollection;

class CollectionsCollection extends FilterCollection
{
    public function initialize(): void
    {
        $this
            ->like('collection', ['before' => true, 'after' => true]);
    }
}
