<?php
declare(strict_types=1);

namespace App\Model\Filter;

use Search\Model\Filter\FilterCollection;

class JournalsCollection extends FilterCollection
{
    public function initialize(): void
    {
        $this
            ->like('journal', ['before' => true, 'after' => true]);
    }
}
