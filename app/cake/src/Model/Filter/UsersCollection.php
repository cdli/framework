<?php
declare(strict_types=1);

namespace App\Model\Filter;

use Cake\ORM\TableRegistry;
use Search\Model\Filter\FilterCollection;

class UsersCollection extends FilterCollection
{
    public function initialize(): void
    {
        $this
            ->like('username', ['before' => true, 'after' => true])
            ->like('email', ['before' => true, 'after' => true])
            ->value('author_id', [
                'formControlOptions' => function () {
                    return [
                        'type' => 'select',
                        'empty' => true,
                        'options' => TableRegistry::getTableLocator()->get('Authors')
                            ->find('list')
                            ->where([
                                'Authors.id IN' => TableRegistry::getTableLocator()->get('Users')
                                    ->find()
                                    ->select(['Users.author_id'])
                                    ->where(['Users.author_id IS NOT' => null]),
                            ])
                            ->toArray(),
                    ];
                },
            ]);
    }
}
