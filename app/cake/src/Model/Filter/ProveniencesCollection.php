<?php
declare(strict_types=1);

namespace App\Model\Filter;

use Cake\ORM\TableRegistry;
use Search\Model\Filter\FilterCollection;

class ProveniencesCollection extends FilterCollection
{
    public function initialize(): void
    {
        $this
            ->like('provenience', ['before' => true, 'after' => true])
            ->value('region_id', [
                'formControlOptions' => function () {
                    return [
                        'type' => 'select',
                        'empty' => true,
                        'options' => TableRegistry::getTableLocator()->get('Regions')
                            ->find('list')
                            ->where([
                                'Regions.id IN' => TableRegistry::getTableLocator()->get('Proveniences')
                                    ->find()->select(['Proveniences.region_id']),
                            ])
                            ->orderAsc('Regions.region')
                            ->toArray(),
                    ];
                },
            ]);
    }
}
