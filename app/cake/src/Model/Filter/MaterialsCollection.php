<?php
declare(strict_types=1);

namespace App\Model\Filter;

use Cake\ORM\TableRegistry;
use Search\Model\Filter\FilterCollection;

class MaterialsCollection extends FilterCollection
{
    public function initialize(): void
    {
        $this
            ->like('material', ['before' => true, 'after' => true])
            ->value('parent_id', [
                'formControlOptions' => function () {
                    $materialsModel = TableRegistry::getTableLocator()->get('Materials');

                    return [
                        'type' => 'select',
                        'empty' => true,
                        'options' => $materialsModel->ParentMaterials->find('list')
                            ->where([
                                'ParentMaterials.id IN' => $materialsModel->find()
                                    ->select(['Materials.parent_id'])
                                    ->where(['Materials.parent_id IS NOT' => null]),
                            ])
                            ->toArray(),
                    ];
                },
            ]);
    }
}
