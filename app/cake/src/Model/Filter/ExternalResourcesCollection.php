<?php
declare(strict_types=1);

namespace App\Model\Filter;

use Search\Model\Filter\FilterCollection;

class ExternalResourcesCollection extends FilterCollection
{
    public function initialize(): void
    {
        $this
            ->like('external_resource', ['before' => true, 'after' => true])
            ->like('abbrev', ['before' => true, 'after' => true]);
    }
}
