<?php
declare(strict_types=1);

namespace App\Model\Filter;

use Cake\ORM\TableRegistry;
use Search\Model\Filter\FilterCollection;

class DynastiesCollection extends FilterCollection
{
    public function initialize(): void
    {
        $this
            ->like('polity', ['before' => true, 'after' => true])
            ->like('dynasty', ['before' => true, 'after' => true])
            ->value('provenience_id', [
                'formControlOptions' => function () {
                    return [
                        'type' => 'select',
                        'empty' => true,
                        'options' => TableRegistry::getTableLocator()->get(alias: 'Proveniences')
                        ->find('list')
                        ->where([
                            'Proveniences.id IN' => TableRegistry::getTableLocator()->get('Dynasties')
                                ->find()
                                ->select(['Dynasties.provenience_id'])
                                ->where(['Dynasties.provenience_id IS NOT' => null]),
                        ])
                        ->toArray(),
                    ];
                },
            ]);
    }
}
