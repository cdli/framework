<?php
declare(strict_types=1);

namespace App\Model\Filter;

use Cake\ORM\Query;
use Search\Model\Filter\Base;
use Search\Model\Filter\FilterCollection;

class UpdateEventsCollection extends FilterCollection
{
    public function initialize(): void
    {
        $this
            ->callback('update_type', [
                'callback' => function (Query $query, array $args, Base $filter) {
                    if (!array_key_exists('update_type', $args)) {
                        return false;
                    }

                    $query
                        ->where(['FIND_IN_SET(:update_type, UpdateEvents.update_type) > 0'])
                        ->bind(':update_type', $args['update_type'], 'string');

                    return true;
                },
            ])
            ->value('status')
            ->value('created_by')
            ->value('approved_by')
            ->compare('created_before', ['fields' => 'created', 'operator' => '<='])
            ->compare('created_after', ['fields' => 'created', 'operator' => '>='])
            ->compare('approved_before', ['fields' => 'approved', 'operator' => '<='])
            ->compare('approved_after', ['fields' => 'approved', 'operator' => '>='])
            ->like('event_comments', ['before' => true, 'after' => true]);
    }
}
