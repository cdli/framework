<?php
declare(strict_types=1);

namespace App\Model\Filter;

use Cake\ORM\TableRegistry;
use Search\Model\Filter\FilterCollection;

class ArchivesCollection extends FilterCollection
{
    public function initialize(): void
    {
        $this
            ->like('archive', ['before' => true, 'after' => true])
            ->value('provenience_id', [
                'formControlOptions' => function () {
                    return [
                        'type' => 'select',
                        'empty' => true,
                        'options' => TableRegistry::getTableLocator()->get(alias: 'Proveniences')
                            ->find('list')
                            ->where([
                                'Proveniences.id IN' => TableRegistry::getTableLocator()->get('Archives')
                                    ->find()
                                    ->select(['Archives.provenience_id'])
                                    ->where(['Archives.provenience_id IS NOT' => null]),
                            ])
                            ->toArray(),
                    ];
                },
            ]);
    }
}
