<?php
declare(strict_types=1);

namespace App\Model\Filter;

use Cake\ORM\TableRegistry;
use Search\Model\Filter\FilterCollection;

class RulersCollection extends FilterCollection
{
    public function initialize(): void
    {
        $this
            ->like('ruler', ['before' => true, 'after' => true])
            ->value('period_id', [
                'formControlOptions' => function () {
                    return [
                        'type' => 'select',
                        'empty' => true,
                        'options' => TableRegistry::getTableLocator()->get('Periods')
                            ->find('list')
                            ->where([
                                'Periods.id IN' => TableRegistry::getTableLocator()->get('Rulers')
                                    ->find()->select(['Rulers.period_id']),
                            ])
                            ->toArray(),
                    ];
                },
            ])
            ->value('dynasty_id', [
                'formControlOptions' => function () {
                    return [
                        'type' => 'select',
                        'empty' => true,
                        'options' => TableRegistry::getTableLocator()->get('Dynasties')
                            ->find('list')
                            ->where([
                                'Dynasties.id IN' => TableRegistry::getTableLocator()->get('Rulers')
                                    ->find()->select(['Rulers.dynasty_id']),
                            ])
                            ->toArray(),
                    ];
                },
            ]);
    }
}
