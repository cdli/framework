<?php
declare(strict_types=1);

namespace App\Model\Filter;

use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Search\Model\Filter\Base;
use Search\Model\Filter\FilterCollection;

class ArticlesCollection extends FilterCollection
{
    public function initialize(): void
    {
        $this
            ->like('title', ['before' => true, 'after' => true])
            ->callback('author_id', [
                'callback' => function (Query $query, array $args, Base $filter) {
                    $query
                        ->innerJoinWith('Authors', function (Query $query) use ($args) {
                            return $query->where(['Authors.id' => $args['author_id']]);
                        })
                        ->group('Articles.id');

                    return true;
                },
                'formControlOptions' => function () {
                    return [
                        'type' => 'select',
                        'empty' => true,
                        'options' => TableRegistry::getTableLocator()->get(alias: 'Authors')
                            ->find('list')
                            ->where([
                                'Authors.id IN' => TableRegistry::getTableLocator()->get('ArticlesAuthors')
                                    ->find()
                                    ->select(['ArticlesAuthors.author_id'])
                                    ->group('ArticlesAuthors.author_id'),
                            ])
                            ->toArray(),
                    ];
                },
            ]);
    }
}
