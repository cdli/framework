<?php
declare(strict_types=1);

namespace App\Model\Filter;

use Search\Model\Filter\FilterCollection;

class AuthorsCollection extends FilterCollection
{
    public function initialize(): void
    {
        $this
            ->like('author', ['before' => true, 'after' => true])
            ->like('institution', ['before' => true, 'after' => true])
            ->like('orcid_id', ['before' => true, 'after' => true]);
    }
}
