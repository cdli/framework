<?php
declare(strict_types=1);

namespace App\Model\Filter;

use Search\Model\Filter\FilterCollection;

class AbbreviationsCollection extends FilterCollection
{
    public function initialize(): void
    {
        $this
            ->like('abbreviation', ['before' => true, 'after' => true])
            ->like('fullform', ['before' => true, 'after' => true]);
    }
}
