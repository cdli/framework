<?php
declare(strict_types=1);

namespace App\Model\Filter;

use Cake\ORM\TableRegistry;
use Search\Model\Filter\FilterCollection;

class LanguagesCollection extends FilterCollection
{
    public function initialize(): void
    {
        $this
            ->like('language', ['before' => true, 'after' => true])
            ->value('parent_id', [
                'formControlOptions' => function () {
                    $languagesModel = TableRegistry::getTableLocator()->get('Languages');

                    return [
                        'type' => 'select',
                        'empty' => true,
                        'options' => $languagesModel->ParentLanguages->find('list')
                            ->where([
                                'ParentLanguages.id IN' => $languagesModel->find()
                                    ->select(['Languages.parent_id'])
                                    ->where(['Languages.parent_id IS NOT' => null]),
                            ])
                            ->toArray(),
                    ];
                },
            ]);
    }
}
