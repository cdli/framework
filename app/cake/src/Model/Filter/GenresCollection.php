<?php
declare(strict_types=1);

namespace App\Model\Filter;

use Cake\ORM\TableRegistry;
use Search\Model\Filter\FilterCollection;

class GenresCollection extends FilterCollection
{
    public function initialize(): void
    {
        $this
            ->like('genre', ['before' => true, 'after' => true])
            ->value('parent_id', [
                'formControlOptions' => function () {
                    $genresModel = TableRegistry::getTableLocator()->get('Genres');

                    return [
                        'type' => 'select',
                        'empty' => true,
                        'options' => $genresModel->ParentGenres->find('list')
                            ->where([
                                'ParentGenres.id IN' => $genresModel->find()
                                    ->select(['Genres.parent_id'])
                                    ->where(['Genres.parent_id IS NOT' => null]),
                            ])
                            ->toArray(),
                    ];
                },
            ]);
    }
}
