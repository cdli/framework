<?php
declare(strict_types=1);

namespace App\Model\Filter;

use Search\Model\Filter\FilterCollection;

class PostingsCollection extends FilterCollection
{
    public function initialize(): void
    {
        $this
            ->like('title', ['before' => true, 'after' => true])
            ->like('content', [
                'fields' => ['body'],
                'before' => true,
                'after' => true,
            ]);
    }
}
