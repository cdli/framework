<?php
declare(strict_types=1);

namespace App\Model\Filter;

use Search\Model\Filter\FilterCollection;

class SignReadingsCollection extends FilterCollection
{
    public function initialize(): void
    {
        $this
        ->like('sign_reading', ['before' => true, 'after' => true])
        ->like('sign_name', ['before' => true, 'after' => true])
        ->like('meaning', ['before' => true, 'after' => true])
        ->value('preferred_reading', [
            'formControlOptions' => [
                'type' => 'select',
                'empty' => true,
                'options' => [
                    '1' => __('Yes'),
                    '0' => __('No'),
                ],
            ],
        ]);
    }
}
