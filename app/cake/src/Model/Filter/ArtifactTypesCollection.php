<?php
declare(strict_types=1);

namespace App\Model\Filter;

use Cake\ORM\TableRegistry;
use Search\Model\Filter\FilterCollection;

class ArtifactTypesCollection extends FilterCollection
{
    public function initialize(): void
    {
        $this
            ->like('artifact_type', ['before' => true, 'after' => true])
            ->value('parent_id', [
                'formControlOptions' => function () {
                    $artifactTypesModel = TableRegistry::getTableLocator()->get('ArtifactTypes');

                    return [
                        'type' => 'select',
                        'empty' => true,
                        'options' => $artifactTypesModel->ParentArtifactTypes->find('list')
                            ->where([
                                'ParentArtifactTypes.id IN' => $artifactTypesModel->find()
                                    ->select(['ArtifactTypes.parent_id'])
                                    ->where(['ArtifactTypes.parent_id IS NOT' => null]),
                            ])
                            ->toArray(),
                    ];
                },
            ]);
    }
}
