<?php
declare(strict_types=1);

namespace App\Model\Filter;

use Cake\ORM\TableRegistry;
use Search\Model\Filter\FilterCollection;

class CdliTabletCollection extends FilterCollection
{
    public function initialize(): void
    {
        $this
            ->like('title', ['fields' => ['theme'], 'before' => true, 'after' => true])
            ->value('author_id', [
                'formControlOptions' => function () {
                    return [
                        'type' => 'select',
                        'empty' => true,
                        'options' => TableRegistry::getTableLocator()->get('Authors')
                            ->find('list')
                            ->where([
                                'Authors.id IN' => TableRegistry::getTableLocator()->get('CdliTablet')
                                    ->find()
                                    ->select(['CdliTablet.author_id'])
                                    ->where(['CdliTablet.author_id IS NOT' => null]),
                            ])
                            ->toArray(),
                    ];
                },
            ]);
    }
}
