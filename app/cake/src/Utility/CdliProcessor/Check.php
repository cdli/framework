<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Utility\CdliProcessor;

use Cake\Core\Configure;
use Cake\Http\Client;
use Cake\Http\Exception\HttpException;

class Check
{
    public static $mpatLevelMap = [
        'Error' => 'danger',
        'Warning' => 'warning',
        'Info' => 'info',
    ];

    public static function checkCdliConll($text)
    {
        $http = new Client();
        $response = $http->post(Configure::read('ServiceUrls.mpat') . '/check', json_encode(['annotation' => $text]), ['type' => 'json']);

        if ($response->getStatusCode() >= 400) {
            $htmlError = $response->getStringBody();
            $textError = html_entity_decode(strip_tags($htmlError), ENT_QUOTES | ENT_HTML5, 'UTF-8');
            throw new HttpException($textError);
        } else {
            $lines = preg_split('/\n+/', trim($response->getStringBody()));

            return array_map(function ($line) {
                [$level, $message] = explode(': ', trim($line), 2);
                $message = str_replace(' in file 0', '', $message);
                $message = str_replace('file 0', 'annotation', $message);
                if (!array_key_exists($level, self::$mpatLevelMap)) {
                    return ['danger', "$level: $message"];
                }

                return [self::$mpatLevelMap[$level], $message];
            }, $lines);
        }
    }

    public function checkConllU($conll_u)
    {
        return $conll_u;
    }

    public function checkBrat($brat)
    {
        return $brat;
    }
}
