<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Utility\CdliProcessor;

class TokenList
{
    public static function processText($text, $kind)
    {
        switch ($kind) {
            case 'signs':
                return Parse::getSignTokens($text);

            case 'words':
                return Parse::getWordTokens($text);
        }
    }

    public static function compareText($text, $kind, $list)
    {
        $warnings = [];
        $tokens = self::processText($text, $kind);
        foreach ($tokens as $token) {
            if (!array_key_exists($token, $list)) {
                $warnings[] = ['danger', "$token does not appear in $kind list"];
            } elseif ($list[$token] < 2) {
                $warnings[] = ['warning', "$token only appears a few times in $kind list"];
            }
        }

        return $warnings;
    }

    public static function getTokenList($id, $kind)
    {
        $file = self::getTokenListFile($id, $kind, 'json');
        if (!file_exists($file)) {
            return null;
        }

        return (array)json_decode(file_get_contents($file))->list;
    }

    public static function getTokenListFile($id, $kind, $type = 'json')
    {
        if (!preg_match('/^\d+$/', $id) || !preg_match('/^(words|signs)$/', $kind) || !preg_match('/^(json|txt|tsv)$/', $type)) {
            return null;
        }

        return '/srv/app/cake/webroot/pubs/token-lists/' . $id . '-' . $kind . '.' . $type;
    }
}
