<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Utility\CdliProcessor;

use Cake\Core\Configure;
use Cake\Http\Client;

class Annotate
{
    public function morphPre($c_conll)
    {
        $http = new Client();
        $response = $http->post(Configure::read('ServiceUrls.mpat') . '/pre-annotate', json_encode(['annotation' => $c_conll]), ['type' => 'json']);

        return $response->getStringBody();
    }

    public function formatConll($inscription)
    {
        $http = new Client();
        $response = $http->post(Configure::read('ServiceUrls.mpat') . '/format', json_encode($inscription), ['type' => 'json']);

        return $response->getStringBody();
    }

    public function cConllToSyntax($c_conll)
    {
        return $c_conll;
    }

    public function POSCConll($c_conll)
    {
        return $c_conll;
    }

    public function translateCConll($c_conll)
    {
        return $c_conll;
    }
}
