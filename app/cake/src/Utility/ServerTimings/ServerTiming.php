<?php
declare(strict_types=1);

namespace App\Utility\ServerTimings;

/**
 * Adapted from https://github.com/fetzi/server-timing/blob/master/src/ServerTiming.php
 */
class ServerTiming
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var float
     */
    private $start;

    /**
     * @var float
     */
    private $end;

    public function __construct(string $name, ?string $description = null)
    {
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @param float|null $fixedValue
     */
    public function start(?float $fixedValue = null): void
    {
        $this->start = $fixedValue ?? microtime(true);
    }

    public function stop(): void
    {
        $this->end = microtime(true);
    }

    public function __toString()
    {
        $timing = $this->name;

        if (!is_null($this->description)) {
            $timing .= sprintf(';desc="%s"', $this->description);
        }

        if (!is_null($this->start) && !is_null($this->end)) {
            $duration = round(($this->end - $this->start) * 1000, 1);
            $timing .= sprintf(';dur=%.1f', $duration);
        }

        return $timing;
    }
}
