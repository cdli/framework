<?php
declare(strict_types=1);

namespace App\Utility\ServerTimings;

/**
 * Adapted from https://github.com/fetzi/server-timing/blob/master/src/ServerTimings.php
 */
class ServerTimings
{
    /**
     * @var array
     */
    private $timings = [];

    /**
     * @const int MAX_TIMINGS
     */
    private const MAX_TIMINGS = 50;

    /**
     * @param string $name
     * @param string $description
     * @return \App\Utility\ServerTimings\ServerTiming
     */
    public function create(string $name, ?string $description = null)
    {
        $name = preg_replace('/\W+/', '_', $name);
        while (array_key_exists($name, $this->timings)) {
            if (preg_match('/-(\d+)$/', $name, $match)) {
                $name = preg_replace('/(\d+)$/', (string)((int)$match[1] + 1), $name);
            } else {
                $name .= '-1';
            }
        }

        $serverTiming = new ServerTiming($name, $description);
        $this->timings[$name] = $serverTiming;

        return $serverTiming;
    }

    /**
     * @return string
     */
    public function getTimings(): ?string
    {
        if (empty($this->timings)) {
            return null;
        }

        return implode(', ', array_slice($this->timings, 0, self::MAX_TIMINGS));
    }
}
