<?php
declare(strict_types=1);

namespace App\Utility;

class LongestCommonSubsequence
{
    public static function fromStringArray(array $array): string
    {
        if (count($array) == 0) {
            return '';
        }

        $string = $array[0];
        $length = count($array);
        for ($i = 1; $i < $length; $i++) {
            $string = self::fromStrings($string, $array[$i]);
        }

        return $string;
    }

    public static function fromStrings(string $S, string $T): string
    {
        return implode(self::fromSegmentedStrings(mb_str_split($S), mb_str_split($T)));
    }

    public static function fromSegmentedStringArray(array $array): array
    {
        if (count($array) == 0) {
            return [];
        }

        $string = $array[0];
        $length = count($array);
        for ($i = 1; $i < $length; $i++) {
            $string = self::fromSegmentedStrings($string, $array[$i]);
        }

        return $string;
    }

    // Implementation of https://en.wikipedia.org/wiki/Longest_common_substring#Dynamic_programming
    public static function fromSegmentedStrings(array $S, array $T): array
    {
        $r = count($S);
        $n = count($T);

        $L = [];
        $z = 0;
        $ret = [];

        for ($i = 0; $i < $r; $i++) {
            for ($j = 0; $j < $n; $j++) {
                if ($S[$i] == $T[$j]) {
                    if ($i == 0 || $j == 0) {
                        $L[$i][$j] = 1;
                    } else {
                        $L[$i][$j] = $L[$i - 1][$j - 1] + 1;
                    }

                    if ($L[$i][$j] > $z) {
                        $z = $L[$i][$j];
                        $ret = [array_slice($S, $i - $z + 1, $z)];
                    } elseif ($L[$i][$j] == $z) {
                        $ret[] = array_slice($S, $i - $z + 1, $z);
                    }
                } else {
                    $L[$i][$j] = 0;
                }
            }
        }

        return count($ret) > 0 ? $ret[0] : [];
    }
}
