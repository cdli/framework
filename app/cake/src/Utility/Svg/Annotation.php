<?php
declare(strict_types=1);

namespace App\Utility\Svg;

use App\Model\Entity\ArtifactAssetAnnotation;

class Annotation
{
    /**
     * @param \App\Model\Entity\ArtifactAssetAnnotation $annotation
     * @param int|null $size
     * @param string $id
     * @return string SVG
     */
    public static function getCutoutSvg(ArtifactAssetAnnotation $annotation, int|null $size = null, string $id = 'annotation'): string
    {
        $viewBox = self::getViewBox($annotation->target_selector);

        return implode('', [
            '<svg xmlns="http://www.w3.org/2000/svg" ',
            is_null($size) ? '' : 'width="' . $size . '" height="' . $size . '"',
            ' viewBox="',
            implode(' ', $viewBox),
            '"><mask id="',
            $id,
            '"><g fill="white">',
            preg_replace('/^<.+?>|<\/[^<]+>$/', '', $annotation->target_selector),
            '</g></mask><image href="/',
            h($annotation->artifact_asset->getPath()),
            '" height="',
            h($annotation->artifact_asset->image_height),
            '" width="',
            h($annotation->artifact_asset->image_width),
            '" mask="url(#',
            $id,
            ')" /></svg>',
        ]);
    }

    /**
     * @param string $svg only one element expected
     * @return array [x, y, width, height]
     */
    public static function getViewBox(string $svg): array
    {
        $boundingBox = AnnotationSelector::fromSvg($svg)->getBoundingBox();

        $viewBox = [
            $boundingBox[0][0],
            $boundingBox[0][1],
            $boundingBox[1][0] - $boundingBox[0][0],
            $boundingBox[1][1] - $boundingBox[0][1],
        ];

        // Make viewbox square
        if ($viewBox[3] > $viewBox[2]) {
            $viewBox[0] = $viewBox[0] - ($viewBox[3] - $viewBox[2]) / 2;
            $viewBox[2] = $viewBox[3];
        } elseif ($viewBox[2] > $viewBox[3]) {
            $viewBox[1] = $viewBox[1] - ($viewBox[2] - $viewBox[3]) / 2;
            $viewBox[3] = $viewBox[2];
        }

        return $viewBox;
    }
}
