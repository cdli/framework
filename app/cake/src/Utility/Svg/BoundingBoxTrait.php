<?php
declare(strict_types=1);

namespace App\Utility\Svg;

trait BoundingBoxTrait
{
    protected array $boundingBox = [[INF, INF], [-INF, -INF]];

    public function getBoundingBox(): array
    {
        return $this->boundingBox;
    }

    public function expandBoundingBox(float $x, float $y)
    {
        if ($x < $this->boundingBox[0][0]) {
            $this->boundingBox[0][0] = $x;
        }

        if ($y < $this->boundingBox[0][1]) {
            $this->boundingBox[0][1] = $y;
        }

        if ($x > $this->boundingBox[1][0]) {
            $this->boundingBox[1][0] = $x;
        }

        if ($y > $this->boundingBox[1][1]) {
            $this->boundingBox[1][1] = $y;
        }
    }
}
