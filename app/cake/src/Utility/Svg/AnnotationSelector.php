<?php
declare(strict_types=1);

namespace App\Utility\Svg;

class AnnotationSelector
{
    use BoundingBoxTrait;

    protected array $parts = [];

    public function __construct(array $parts = [])
    {
        foreach ($parts as $part) {
            $this->addPart($part);
        }
    }

    public function addPart(AnnotationSelectorPart $part)
    {
        foreach ($part->getBoundingBox() as [$x, $y]) {
            $this->expandBoundingBox($x, $y);
        }

        $this->parts[] = $part;
    }

    /**
     * @param string $svg
     */
    public static function fromSvg(string $svg): AnnotationSelector
    {
        $selector = new self();

        $parser = xml_parser_create();
        xml_set_element_handler(
            $parser,
            function ($parser, string $name, array $attr) use (&$selector) {
                $part = AnnotationSelectorPart::fromSvg($name, $attr);
                if (!is_null($part)) {
                    $selector->addPart($part);
                }
            },
            function () {
            },
        );

        if (!xml_parse($parser, $svg, true)) {
            $code = xml_get_error_code($parser);
            $message = xml_error_string($code);
            xml_parser_free($parser);
            throw new \ErrorException("Error parsing target selector: [$code] $message\n$svg");
        }

        xml_parser_free($parser);

        return $selector;
    }

    public function getSvg(): string
    {
        return implode('', [
            '<svg>',
            ...array_map(function ($part) {
                return $part->getSvg();
            }, array_values($this->parts)),
            '</svg>',
        ]);
    }

    /**
     * @param array $shapeAttributes
     */
    public static function fromViaJson(array $shapeAttributes)
    {
        return new self([AnnotationSelectorPart::fromViaJson($shapeAttributes)]);
    }

    public function getViaJson()
    {
        return $this->parts[0]->getViaJson();
    }
}
