<?php
declare(strict_types=1);

namespace App\Utility\Svg;

class AnnotationSelectorPart
{
    use BoundingBoxTrait;

    public string $name;
    public array $attr = [];

    public function __construct(string $name, array $attr)
    {
        $this->name = strtolower($name);
        foreach ($attr as $key => $value) {
            $this->attr[strtolower($key)] = $value;
        }

        if ($this->name === 'rect') {
            $this->expandBoundingBox($this->attr['x'], $this->attr['y']);
            $this->expandBoundingBox($this->attr['x'] + $this->attr['width'], $this->attr['y'] + $this->attr['height']);
        } elseif ($this->name === 'circle') {
            $this->expandBoundingBox($this->attr['cx'] - $this->attr['r'], $this->attr['cy'] - $this->attr['r']);
            $this->expandBoundingBox($this->attr['cx'] + $this->attr['r'], $this->attr['cy'] + $this->attr['r']);
        } elseif ($this->name === 'ellipse') {
            $this->expandBoundingBox($this->attr['cx'] - $this->attr['rx'], $this->attr['cy'] - $this->attr['ry']);
            $this->expandBoundingBox($this->attr['cx'] + $this->attr['rx'], $this->attr['cy'] + $this->attr['ry']);
        } elseif ($this->name === 'point') {
            $this->expandBoundingBox($this->attr['cx'] - 5, $this->attr['cy'] - 5);
            $this->expandBoundingBox($this->attr['cx'] + 5, $this->attr['cy'] + 5);
        } elseif ($this->name === 'polygon' || $this->name === 'polyline') {
            foreach ($this->attr['points'] as [$x, $y]) {
                $this->expandBoundingBox($x, $y);
            }
        } else {
            throw new \ErrorException("Unexpected annotation selector part '$name'");
        }
    }

    public static function fromSvg(string $name, array $attr)
    {
        if ($name === 'RECT') {
            $attr = [
                'x' => intval($attr['X'] ?? 0),
                'y' => intval($attr['Y'] ?? 0),
                'width' => intval($attr['WIDTH'] ?? 0),
                'height' => intval($attr['HEIGHT'] ?? 0),
            ];
        } elseif ($name === 'CIRCLE') {
            $attr = [
                'cx' => intval($attr['CX'] ?? 0),
                'cy' => intval($attr['CY'] ?? 0),
                'r' => intval($attr['R'] ?? 0),
            ];
        } elseif ($name === 'ELLIPSE') {
            $attr = [
                'cx' => intval($attr['CX'] ?? 0),
                'cy' => intval($attr['CY'] ?? 0),
                'rx' => intval($attr['RX'] ?? 0),
                'ry' => intval($attr['RY'] ?? 0),
            ];
        } elseif ($name === 'POLYGON' || $name === 'POLYLINE') {
            $points = $attr['POINTS'] ?? '0,0';
            $points = preg_replace('/\s+/', ' ', trim($points));
            if ($points === '') {
                $points = '0,0';
            }
            $points = array_map(function ($point) {
                $coords = explode(',', $point);

                return [intval($coords[0]), intval($coords[1])];
            }, explode(' ', $points));
            $attr = ['points' => $points];
        } else {
            return null;
        }

        if (isset($attr['r']) && $attr['r'] <= 5) {
            $name === 'POINT';
            unset($attr['r']);
        }

        return new self(strtolower($name), $attr);
    }

    public function getSvg(): string
    {
        // Annotorious does not support <rect>
        if ($this->name === 'rect') {
            $part = new self('polygon', ['points' => [
                [$this->attr['x'], $this->attr['y']],
                [$this->attr['x'], $this->attr['y'] + $this->attr['height']],
                [$this->attr['x'] + $this->attr['width'], $this->attr['y'] + $this->attr['height']],
                [$this->attr['x'] + $this->attr['width'], $this->attr['y']],
                [$this->attr['x'], $this->attr['y']],
            ]]);

            return $part->getSvg();
        }

        $name = $this->name;

        $attr = '';
        foreach (['cx', 'cy', 'rx', 'ry', 'r'] as $key) {
            if (array_key_exists($key, $this->attr)) {
                $attr .= $key . '="' . $this->attr[$key] . '" ';
            }
        }
        if ($this->name === 'point') {
            $name = 'circle';
            $attr .= 'r="5" ';
        }
        if (array_key_exists('points', $this->attr)) {
            $attr .= 'points="' . implode(' ', array_map(function ($point) {
                return "$point[0],$point[1]";
            }, $this->attr['points'])) . '" ';
        }

        return "<$name $attr/>";
    }

    /**
     * @param array $shapeAttributes
     */
    public static function fromViaJson(array $shapeAttributes)
    {
        if ($shapeAttributes['name'] === 'polygon' || $shapeAttributes['name'] === 'polyline') {
            $shapeAttributes['points'] = array_map(function ($x, $y) {
                return [intval($x), intval($y)];
            }, $shapeAttributes['all_points_x'], $shapeAttributes['all_points_y']);
            unset($shapeAttributes['all_points_x']);
            unset($shapeAttributes['all_points_y']);
        }

        $name = $shapeAttributes['name'];
        unset($shapeAttributes['name']);

        return new self($name, $shapeAttributes);
    }

    public function getViaJson(): array
    {
        $shapeAttributes = array_slice($this->attr, 0);
        $shapeAttributes['name'] = $this->name;
        if ($this->name === 'polygon' || $this->name === 'polyline') {
            $shapeAttributes['all_points_x'] = array_column($shapeAttributes['points'], 0);
            $shapeAttributes['all_points_y'] = array_column($shapeAttributes['points'], 1);
            unset($shapeAttributes['points']);
        }

        return $shapeAttributes;
    }
}
