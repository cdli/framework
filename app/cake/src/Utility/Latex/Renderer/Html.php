<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Utility\Latex\Renderer;

use App\Utility\Latex\Parser;
use App\Utility\Latex\Parser\Node;
use PhpLatex_Node;
use PhpLatex_Renderer_Html;

class Html extends PhpLatex_Renderer_Html
{
    protected $_data;

    protected $_switchCommands = [
        // Style
        '\\it' => '\\textit',
        '\\itshape' => '\\textit',
        '\\sl' => '\\textsl',
        '\\slshape' => '\\textsl',

        // Flags
        '\\bf' => '\\textbf',
        '\\bfseries' => '\\textbf',
        '\\em' => '\\emph',
        '\\sc' => '\\textsc',
        '\\scshape' => '\\textsc',

        // Font selection
        '\\rm' => '\\textrm',
        '\\sf' => '\\textsf',
        '\\tt' => '\\texttt',
    ];

    protected $_environRefLabels = [
        'figure' => 'Figure',
        'lstlisting' => 'Listing',
        'table' => 'Table',
    ];

    protected $_footnotes = [];

    public function __construct(array $data)
    {
        $this->_data = $data;
        $citations = $data['citations'];
        /** @var array<array> $footnotes */
        $footnotes = array_values($data['footnotes']);
        $labels = $data['labels'];
        $images = array_map(function ($index, $image) {
            return [$index, $image];
        }, array_keys($data['images']), $data['images']);

        // Text styling
        $this->addCommandRenderer('\\subscript', function ($node) {
            return '<sub>' . $this->_renderNodeChildren($node) . '</sub>';
        });
        $this->addCommandRenderer('\\superscript', function ($node) {
            return '<sup>' . $this->_renderNodeChildren($node) . '</sup>';
        });

        // Special characters
        $this->addCommandRenderer('\\everymodeprime', function () {
            return '&prime;';
        });
        $this->addCommandRenderer('\\hith', function () {
            return 'h&#x32E;';
        });
        $this->addCommandRenderer('\\Hith', function () {
            return 'H&#x32E;';
        });
        $this->addCommandRenderer('\\textcorner', function () {
            return '&#x231D;';
        });
        $this->addCommandRenderer('\\textopencorner', function () {
            return '&#x231C;';
        });

        // Bibliography
        $this->addCommandRenderer('\\printbibliography', function () use ($citations) {
            if ($citations) {
                $entries = array_map(function ($entry) {
                    return '<li>' . $entry[1] . '</li>';
                }, $citations['bibliography']);

                return '<div class="csl-bibliography"><ol style="list-style-type: none; padding: 0; margin: 0;">' . implode("\n", $entries) . '</ol></div>';
            } else {
                return '';
            }
        });
        $this->addCommandRenderer('\\nocite', function () {
            return '';
        });
        $this->addCommandRenderer('\\cite', function () use (&$citations) {
            if ($citations) {
                return substr(array_shift($citations['citation']), 1, -1);
            } else {
                return '';
            }
        });
        $this->addCommandRenderer('\\citep', function () use (&$citations) {
            if ($citations) {
                return array_shift($citations['citation']);
            } else {
                return '';
            }
        });
        $this->addCommandRenderer('\\parencite', function () use (&$citations) {
            if ($citations) {
                return array_shift($citations['citation']);
            } else {
                return '';
            }
        });

        // Footnotes
        $this->addCommandRenderer('\\footnote', function () use (&$footnotes) {
            $footnote = array_shift($footnotes);
            $index = $footnote['index'];
            $this->_footnotes[$footnote['index']] = $this->renderPart($footnote['content']);

            return '<sup><a href="#f' . $index . '" id="r' . $index . '">[' . $index . ']</a></sup>';
        });

        // Labels
        $this->addCommandRenderer('\\label', function ($node) {
            $label = $this->toLatex($node->getChildren()[0]->getChildren());

            return '<a name="' . $label . '"></a>';
        });
        $this->addCommandRenderer('\\Cref', function ($node) use ($labels) {
            $keys = $this->toLatex($node->getChildren()[0]->getChildren());
            $keys = explode(',', $keys);
            $refs = [];

            foreach ($keys as $key) {
                $text = $labels[$key]['index'];
                if (array_key_exists($labels[$key]['type'], $this->_environRefLabels)) {
                    $text = $this->_environRefLabels[$labels[$key]['type']] . ' ' . $text;
                }
                $refs[] = '<a href="' . $key . '">' . $text . '</a>';
            }

            return implode(', ', $refs);
        });

        // Figures
        $this->addCommandRenderer('\\caption', function ($node) {
            if ($node->getProp('environ') === 'longtable' || $node->getProp('environ') === 'table') {
                return '<caption align="bottom">' . $this->_renderNodeChildren($node) . '</caption>';
            } else {
                return '<figcaption>' . $this->_renderNodeChildren($node) . '</figcaption>';
            }
        });
        $this->addCommandRenderer('\\includegraphics', function ($node) use (&$images) {
            $image = array_shift($images); // [$index, ['url' => $url, 'caption' => $caption]]
            $caption = $image[1]['caption'];

            $fileExtension = pathinfo($image[1]['url'], PATHINFO_EXTENSION);
            $fileExtension = empty($fileExtension) ? '' : '.' . $fileExtension;
            $url = '/pubs/figs/%%ARTICLE_ID%%-' . str_pad($image[0] + 1, 3, '0', STR_PAD_LEFT) . $fileExtension;

            return '<img src="' . $url . '" alt="' . strip_tags($caption) . '"></img>';
        });
        $this->addCommandRenderer('\\captionsetup', function ($node) {
            return '';
        });

        // Tables
        $this->addCommandRenderer('\\hline', function () {
            return '';
        });
        $this->addCommandRenderer('\\cline', function ($node) {
            return '';
        });
        $this->addCommandRenderer('\\multicolumn', function ($node) {
            return $this->_renderNodeChildren($node->getChildren()[2]);
        });

        // Other
        $this->addCommandRenderer('\\newcolumntype', function () {
            return '';
        });
        $this->addCommandRenderer('\\newcommand', function () {
            return '';
        });
        $this->addCommandRenderer('\\renewcommand', function () {
            return '';
        });
        $this->addCommandRenderer('\\setlength', function () {
            return '';
        });
        $this->addCommandRenderer('\\today', function () {
            return date('d.m.Y');
        });
        $this->addCommandRenderer('\\thispagestyle', function () {
            return '';
        });
        $this->addCommandRenderer('\\vspace', function () {
            return '';
        });
        $this->addCommandRenderer('\\hspace', function () {
            return '';
        });
    }

    protected function _renderEnvironComment($node)
    {
        return '';
    }

    protected function _renderEnvironDescription($node)
    {
        return parent::_renderEnvironList($node);
    }

    protected function _renderEnvironFigure($node)
    {
        $node = Node::fromPhpLatexNode($node);

        // Remove 'placement' argument
        $node->removeOptionalArguments();

        return $this->_renderFigure($node);
    }

    protected function _renderEnvironLstlisting($node)
    {
        [$params, $code] = Parser::parseListingEnviron($node->getChildren()[0]->value);
        $language = h($params['language']);
        $code = "<pre style=\"text-align: left;\"><code class=\"language-$language\">$code</code></pre>";
        $caption = h(substr($params['caption'], 1, -1));

        return "<figure>$code<figcaption>$caption</figcaption></figure>";
    }

    protected function _renderEnvironSubfigure($node)
    {
        $node = Node::fromPhpLatexNode($node);

        // Remove 'outer-pos', 'height', 'inner-pos' arguments
        $node->removeOptionalArguments();

        // Remove 'width' argument
        $node->removeArguments(1);

        return $this->_renderFigure($node);
    }

    protected function _renderEnvironFlushedleft($node)
    {
        return $this->_renderNodeChildren($node);
    }

    protected function _renderEnvironLongtable($node)
    {
        $node = Node::fromPhpLatexNode($node);

        $caption = null;
        $endOfRow = true;
        foreach ($node->getChildren() as $child) {
            if ($child->getType() == Parser::TYPE_COMMAND && $child->getProp('value') == '\\caption') {
                $caption = $this->_renderNode($child);
                $node->removeChild($child);
                $endOfRow = false;
            } elseif (!$endOfRow) {
                $node->removeChild($child);
                $endOfRow = $child->getType() == Parser::TYPE_COMMAND && ($child->getProp('value') == '\\\\' || $child->getProp('value') == '\\endhead');
            }
        }

        $table = $this->_renderEnvironTabular($node);

        if (isset($caption)) {
            $table = preg_replace('/<\/table>$/', $caption . '</table>', $table);
        }

        return $table;
    }

    protected function _renderEnvironMulticols($node)
    {
        $node = Node::fromPhpLatexNode($node);
        $node->removeArguments(1);

        return $this->_renderNodeChildren($node);
    }

    protected function _renderEnvironTable($node)
    {
        foreach ($node->getChildren() as $child) {
            if ($child->getType() == Parser::TYPE_COMMAND && $child->getProp('value') == '\\caption') {
                $caption = $this->_renderNode($child);
                $node->removeChild($child);
            } elseif ($child->getType() === Parser::TYPE_ENVIRON && $child->getProp('value') == 'tabular') {
                $table = $child;
            }
        }

        if (isset($table)) {
            $table = $this->_renderEnvironTabular($table);
        } else {
            return '';
        }

        if (isset($caption)) {
            $table = preg_replace('/<\/table>$/', $caption . '</table>', $table);
        }

        return $table;
    }

    protected function _renderEnvironTabular($node)
    {
        // Remove 'pos' argument
        $node->removeOptionalArguments();

        $children = $node->getChildren();

        // Extract alignment parameters
        if ($children[0]->getProp('arg')) {
            $alignment = array_shift($children)->getChildren();
            $alignment = array_filter($alignment, function ($part) {
                return $part->getType() === Parser::TYPE_TEXT;
            });
            $alignment = strtolower($this->toLatex($alignment));

            // Parse alignment & vertical border info
            $alignment = preg_replace('/[pmb]/', 'l', $alignment);
            $alignment = preg_replace('/[^crl|]/', '', $alignment);
            $alignment = array_slice(preg_split('/(?<!\|)(\|?)/', $alignment, -1, PREG_SPLIT_DELIM_CAPTURE), 1, -1);
        } else {
            // Edge case; fallback
            $alignment = ['', 'c', ''];
        }

        $row = 0;
        $col = 0;
        $table = [];
        $borders = [];

        foreach ($children as $i => $child) {
            if (!isset($table[$row][$col])) {
                $table[$row][$col] = '';
            }

            if ($child->getType() === Parser::TYPE_COMMAND && $child->value === '\\\\') {
                ++$row;
                $col = 0;
                continue;
            } elseif ($child->getType() === Parser::TYPE_SPECIAL && $child->value === '&') {
                ++$col;
                continue;
            } elseif ($child->getType() === Parser::TYPE_COMMAND && $child->value === '\\hline') {
                $borders[$row]['top']['row'] = true;
                continue;
            } elseif ($child->getType() === Parser::TYPE_COMMAND && in_array($child->value, ['\\bottomrule', '\\midrule', '\\toprule'])) {
                $borders[$row]['top']['row'] = true;
                continue;
            } elseif ($child->getType() === Parser::TYPE_COMMAND && $child->value === '\\cline') {
                $range = $this->toLatex($child->getChildren()[0]->getChildren());
                $range = array_map('intval', explode('-', $range));
                for ($i = $range[0]; $i <= $range[1]; $i++) {
                    $borders[$row]['top']['cells'][$i - 1] = true;
                }
                continue;
            } elseif ($child->getType() === Parser::TYPE_COMMAND && $child->value === '\\multicolumn') {
                $cell = $child->getChildren();
                $table[$row][$col] = [
                    'span' => intval($cell[0]->getChildren()[0]->getProp('value')),
                    'alignment' => count($cell[1]->getChildren()) ? $cell[1]->getChildren()[0]->getProp('value') : '',
                    'content' => $this->_renderNode($cell[2]),
                ];
                continue;
            }

            $cell = $this->_renderNode($child);

            if (!is_array($table[$row][$col])) {
                $table[$row][$col] .= $cell;
            }
        }

        // If the last row is empty (apart from borders), remove it
        $nrow = count($table);
        if (trim(implode('', $table[$nrow - 1])) === '') {
            array_pop($table);

            if (isset($borders[$nrow - 1]) && $nrow > 1) {
                $borders[$nrow - 2]['bottom'] = $borders[$nrow - 1]['top'];
            }
        }

        $html = '<table class="table" style="border-collapse: collapse;">';
        foreach ($table as $i => $row) {
            $html .= '<tr>';
            foreach ($row as $j => $cell) {
                $cellAlignment = $alignment[2 * $j + 1] ?? $alignment[1];
                $span = '';

                // Col span
                if (is_array($cell)) {
                    $j += $cell['span'] - 1;
                    $span = "colspan=\"{$cell['span']}\"";
                    $cellAlignment = $cell['alignment'];
                    $cell = $cell['content'];
                }

                $style = [];

                // Alignment
                if ($cellAlignment === 'c') {
                    $style[] = 'text-align: center;';
                } elseif ($cellAlignment === 'l') {
                    $style[] = 'text-align: left;';
                } elseif ($cellAlignment === 'r') {
                    $style[] = 'text-align: right;';
                }

                // Borders
                $cellBorders = ['0', '0', '0', '0'];
                $cellBordered = false;
                if (isset($borders[$i]['top']['cells'][$j]) || isset($borders[$i]['top']['row'])) {
                    $cellBorders[0] = '1px';
                    $cellBordered = true;
                }
                if (isset($borders[$i]['bottom']['cells'][$j]) || isset($borders[$i]['bottom']['row'])) {
                    $cellBorders[2] = '1px';
                    $cellBordered = true;
                }
                if ($alignment[0] === '|' && $j === 0) {
                    $cellBorders[3] = '1px';
                    $cellBordered = true;
                }
                if ($alignment[2 * $j + 2] === '|') {
                    $cellBorders[1] = '1px';
                    $cellBordered = true;
                }

                if ($cellBordered) {
                    $style[] = 'border-width: ' . implode(' ', $cellBorders) . ';';
                    $style[] = 'border-color: black;';
                    $style[] = 'border-style: solid;';
                } else {
                    $style[] = 'border: 0;';
                }

                $style = implode(' ', $style);
                $cell = trim($cell);
                $html .= "<td style=\"$style\"$span>$cell</td>\n";
            }
            $html .= "</tr>\n";
        }
        $html .= '</table>';

        return $html;
    }

    protected function _renderFigure($node)
    {
        return '<figure>' . $this->_renderNodeChildren($node) . '</figure>';
    }

    protected function _renderNodeChildren($node)
    {
        $html = '';
        foreach ($node->getChildren() as $child) {
            $html .= $this->_renderNode($child, 0);
        }

        return $html;
    }

    protected function _collectFragments($node)
    {
        $node = Node::fromPhpLatexNode($node);
        $children = $node->getChildren();
        $fragments = [];
        $fragment = new PhpLatex_Node(Parser::TYPE_DOCUMENT);

        foreach ($children as $index => $child) {
            if ($child->getType() == Parser::TYPE_COMMAND) {
                $command = $child->getProp('value');

                if (array_key_exists($command, $this->_switchCommands)) {
                    $fragments[] = $fragment;
                    $fragment = new PhpLatex_Node(Parser::TYPE_COMMAND, [
                        'value' => $this->_switchCommands[$command],
                        'switchCommand' => true,
                        'fragmented' => true,
                    ]);
                    continue;
                }
            }

            $fragment->appendChild($child);
        }

        $fragments[] = $fragment;
        array_splice($fragments, 0, 1, $fragments[0]->getChildren());
        $node->setChildren($fragments);

        return $node;
    }

    /**
     * @param \PhpLatex_Node|string $document
     * @return mixed|string
     * @psalm-suppress ParamNameMismatch,LessSpecificImplementedReturnType
     */
    public function render($document)
    {
        if (!$document instanceof PhpLatex_Node) {
            $document = $this->getParser()->parse($document);
        }

        $result = parent::render($this->_collectFragments($document));

        // Fix whitespace issues
        $result = preg_replace('/<(h1|h2|h3|h4|h5|h6|pre|ul|ol|table|figure)/i', '</p><\1', $result);
        $result = preg_replace('/<\/(h1|h2|h3|h4|h5|h6|pre|ul|ol|table|figure)>/i', '</\1><p>', $result);
        $result = preg_replace('/<br\/>(<br\/>)+/', '</p><p>', $result);
        $result = preg_replace('/(^|<p>)\s*($|<\/p>)/', '', $result);

        return $result;
    }

    /**
     * @param mixed $document
     * @return mixed|string
     */
    public function renderPart($document)
    {
        if (is_array($document)) {
            $children = $document;
            $document = new Node(Parser::TYPE_GROUP);
            foreach ($children as $child) {
                $document->appendChild($child);
            }
        }

        if (!$document instanceof PhpLatex_Node) {
            $document = $this->getParser()->parse($document);
        }

        return $this->_renderNode($document);
    }

    /**
     * @param array $footnote
     * @return mixed|string
     */
    public function renderFootnote($footnote)
    {
        $index = $footnote['index'];
        if (array_key_exists($index, $this->_footnotes)) {
            return $this->_footnotes[$index];
        }

        return null;
    }

    protected function _renderNode($node, $flags = 0)
    {
        if (!$node->getProp('fragmented')) {
            $node = $this->_collectFragments($node);
        }

        return parent::_renderNode($node, $flags);
    }
}
