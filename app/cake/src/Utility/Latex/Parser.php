<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Utility\Latex;

use PhpLatex_Parser;

class Parser extends PhpLatex_Parser
{
    public function __construct()
    {
        parent::__construct();

        $this->_skipUndefinedCommands = false;
        $this->_skipUndefinedEnvirons = false;

        $this->addEnvirons([
            'description' => [],
            'document' => [],
            'figure' => ['numArgs' => 1, 'numOptArgs' => 1],
            'flushleft' => [],
            'table' => ['numArgs' => 1],
            'tabular' => ['numArgs' => 1, 'numOptArgs' => 1],

            // from CTAN package 'comment',
            'comment' => [],

            // from the CTAN package 'listings',
            // 'numOptArgs' => 1
            'lstlisting' => ['verbatim' => true],

            // from CTAN package 'longtable'
            'longtable' => ['numArgs' => 1, 'numOptArgs' => 1],

            // from CTAN pacakge 'multicol'
            'multicols' => ['numArgs' => 1],

            // from CTAN package 'subcaption'
            'subfigure' => ['numArgs' => 1, 'numOptArgs' => 3],
        ]);

        $this->addCommands([
            // Text styling
            '\\subscript' => ['numArgs' => 1],
            '\\superscript' => ['numArgs' => 1],
            '\\it' => null,
            '\\itshape' => null,
            '\\sl' => null,
            '\\slshape' => null,
            '\\bf' => null,
            '\\bfseries' => null,
            '\\em' => null,
            '\\sc' => null,
            '\\scshape' => null,
            '\\rm' => null,
            '\\sf' => null,
            '\\tt' => null,
            '\\item' => null,
            '\\subitem' => null,

            // Special characters
            '\\everymodeprime' => null,
            '\\hith' => null,
            '\\Hith' => null,
            '\\textcorner' => null,
            '\\textopencorner' => null,

            // Bibliography
            '\\printbibliography' => [
                'numOptArgs' => 1,
            ],
            '\\nocite' => [
                'numArgs' => 1,
                'parseArgs' => false,
            ],
            '\\parencite' => [
                'numArgs' => 1,
                'numOptArgs' => 1,
                'parseArgs' => false,
            ],
            '\\cite' => [
                'numArgs' => 1,
                'numOptArgs' => 1,
                'parseArgs' => false,
            ],
            '\\citep' => [
                'numArgs' => 1,
                'numOptArgs' => 1,
                'parseArgs' => false,
            ],

            // Footnotes
            '\\footnote' => [
                'numArgs' => 1,
            ],

            // Figures
            '\\includegraphics' => [
                'numArgs' => 1,
                'numOptArgs' => 1,
                'parseArgs' => false,
            ],
            '\\caption' => [
                'numArgs' => 1,
                'starred' => true,
            ],
            '\\label' => [
                'numArgs' => 1,
                'parseArgs' => false,
            ],
            '\\captionsetup' => [
                'numArgs' => 1,
            ],

            // Tables
            '\\cline' => ['numArgs' => 1],
            '\\multicolumn' => ['numArgs' => 3],

            // from CTAN package 'cleveref',
            '\\Cref' => ['numArgs' => 1],

            // Other
            '\\begingroup' => null,
            '\\centering' => null,
            '\\clearpage' => null,
            '\\endgroup' => null,
            '\\hspace' => [
                'numArgs' => 1,
                'starred' => true,
            ],
            '\\let' => null,
            '\\maketitle' => null,
            '\\newcolumntype' => ['numArgs' => 2],
            '\\newcommand' => ['numArgs' => 2],
            '\\newpage' => null,
            '\\relax' => null,
            '\\renewcommand' => [
                'numArgs' => 2,
                'starred' => true,
            ],
            '\\setlength' => ['numArgs' => 2],
            '\\thispagestyle' => ['numArgs' => 1],
            '\\today' => null,
            '\\vspace' => [
                'numArgs' => 1,
                'starred' => true,
            ],
        ]);
    }

    /**
     * @param string $name
     * @param array $options
     * @return $this
     */
    public function addEnviron($name, array $options)
    {
        if (!preg_match('/^[a-zA-Z]+$/', $name)) {
            throw new \InvalidArgumentException(sprintf('Invalid command name: "%s"', $name));
        }

        if (isset($options['mode'])) {
            $mode = $options['mode'];
            switch ($mode) {
                case 'both':
                    $mode = self::MODE_BOTH;
                    break;

                case 'math':
                    $mode = self::MODE_MATH;
                    break;

                case 'text':
                    $mode = self::MODE_TEXT;
                    break;

                default:
                    $mode = intval($mode);
                    break;
            }
        } else {
            $mode = self::MODE_TEXT;
        }

        $this->_environs[$name] = [
            'environs' => $options['environs'] ?? null,
            'math' => $options['math'] ?? false,
            'mode' => $mode,
            'numArgs' => isset($options['numArgs']) ? intval($options['numArgs']) : 0,
            'numOptArgs' => isset($options['numOptArgs']) ? intval($options['numOptArgs']) : 0,
            'parseArgs' => $options['parseArgs'] ?? true,
            'starred' => $options['starred'] ?? false,
            'verbatim' => $options['verbatim'] ?? false,
        ];

        return $this;
    }

    public function addEnvirons(array $environs)
    {
        foreach ($environs as $name => $spec) {
            $this->addEnviron($name, (array)$spec);
        }

        return $this;
    }

    public function isDefinedCommand(string $value)
    {
        return isset($this->_commands[$value]);
    }

    protected function _createEnviron($name, $mode, $environ = null)
    {
        // Do not enforce environ scope rules
        return parent::_createEnviron($name, $mode, null);
    }

    public static function parseListingEnviron($value)
    {
        if ($value[0] !== '[') {
            return [[], $value];
        }

        $previousIndex = 1;
        $depth = 0;
        $params = [];
        $paramKey = null;

        $length = strlen($value);
        for ($index = $previousIndex; $index < $length; $index++) {
            if ($value[$index] === '{') {
                $depth++;
            } elseif ($value[$index] === '}') {
                $depth--;
            }

            if ($depth > 0) {
                continue;
            }

            if ($value[$index] === '=') {
                $paramKey = substr($value, $previousIndex, $index - $previousIndex);
                $previousIndex = $index + 1;
            } elseif ($value[$index] === ',' || $value[$index] === ']') {
                $paramValue = substr($value, $previousIndex, $index - $previousIndex);
                $params[$paramKey] = $paramValue;
                $previousIndex = $index + 1;
            }

            if ($value[$index] === ']') {
                break;
            }
        }

        return [$params, substr($value, $previousIndex)];
    }
}
