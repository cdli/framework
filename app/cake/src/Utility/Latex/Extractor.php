<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Utility\Latex;

use App\Utility\Latex\Renderer\Html;
use PhpLatex_Node;

class Extractor
{
    public $citations = [];
    public $footnotes = [];
    public $images = [];
    public $labels = [];

    protected $_citationCommands = ['\\parencite', '\\cite', '\\citep', '\\nocite'];
    protected $_figureEnvirons = ['figure', 'subfigure'];
    protected $_renderer;
    protected $_labelTypeCounts = [];

    public function __construct(Html $renderer)
    {
        $this->_renderer = $renderer;
    }

    public function extract(PhpLatex_Node $node)
    {
        $command = $node->getProp('value');
        if ($node->getType() == Parser::TYPE_COMMAND) {
            $inFigure = in_array($node->getProp('environ'), $this->_figureEnvirons);
            if (in_array($command, $this->_citationCommands)) {
                if (!$inFigure) {
                    $this->_renderer->renderPart($node);
                }
                $this->citations[] = $this->_renderer->toLatex($node);
            } elseif ($command == '\\footnote') {
                $this->footnotes[] = [
                    'index' => count($this->footnotes) + 1,
                    'content' => $node->getChildren(),
                ];
            } elseif ($command == '\\label') {
                $key = $this->_renderer->toLatex($node->getChildren()[0]->getChildren());
                $type = $node->getProp('environ');
                $this->addLabel($key, $type);
            } elseif ($command == '\\includegraphics' && !$inFigure) {
                $this->images[] = [
                    'url' => $this->parseCommandIncludeGraphics($node),
                    'caption' => '',
                    'content' => $node,
                ];
            }
        } elseif ($node->getType() == Parser::TYPE_ENVIRON) {
            if (in_array($command, $this->_figureEnvirons)) {
                $url = $this->getFigureUrl($node);
                if ($url) {
                    $this->images[] = [
                        'url' => $url,
                        'caption' => $this->getFigureCaption($node),
                        'content' => $node,
                    ];
                }
            } elseif ($command === 'lstlisting') {
                [$params, $code] = Parser::parseListingEnviron($node->getChildren()[0]->value);
                if (array_key_exists('label', $params)) {
                    $this->addLabel($params['label'], 'lstlisting');
                }
            } elseif ($command === 'comment') {
                return;
            }
        }

        foreach ($node->getChildren() as $child) {
            $this->extract($child);
        }
    }

    public function getFigureUrl(PhpLatex_Node $node)
    {
        foreach ($node->getChildren() as $child) {
            if ($child->getType() == Parser::TYPE_COMMAND && $child->getProp('value') == '\\includegraphics') {
                return $this->parseCommandIncludeGraphics($child);
            }
        }
    }

    public function parseCommandIncludeGraphics(PhpLatex_Node $node)
    {
        $arguments = $node->getChildren();

        return $this->_renderer->toLatex($arguments[count($arguments) - 1]->getChildren());
    }

    public function getFigureCaption(PhpLatex_Node $node)
    {
        foreach ($node->getChildren() as $child) {
            if ($child->getType() == Parser::TYPE_COMMAND && $child->getProp('value') == '\\caption') {
                $arguments = $child->getChildren();

                return $this->_renderer->renderPart($child->getChildren());
            }
        }
    }

    public function addLabel(string $key, string $type)
    {
        if (array_key_exists($type, $this->_labelTypeCounts)) {
            $count = $this->_labelTypeCounts[$type] + 1;
        } else {
            $count = 1;
        }

        $this->_labelTypeCounts[$type] = $count;

        return $this->labels[$key] = ['index' => $count, 'type' => $type, 'key' => $key];
    }
}
