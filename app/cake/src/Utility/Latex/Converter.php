<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Utility\Latex;

use App\Utility\Latex\Extractor as LatexExtractor;
use App\Utility\Latex\Parser as LatexParser;
use App\Utility\Latex\Renderer\Html as LatexToHtmlRenderer;

class Converter
{
    public const FIX_TEMPLATE = 1;

    public static function convert(string $latex, $citations, int $flags = 0)
    {
        if ($flags & self::FIX_TEMPLATE) {
            $latex = self::_applyTemplateFixes($latex);
        }

        // Parse LaTeX and extract document
        $parser = new LatexParser();
        $document = null;
        foreach ($parser->parse($latex)->getChildren() as $child) {
            if ($child->getType() == LatexParser::TYPE_ENVIRON && $child->getProp('value') == 'document') {
                $document = $child;
            }
        }

        // Extract undefined commands
        $undefinedCommands = [];
        self::findCommands($document, $parser, $undefinedCommands);

        // Extract footnotes, images and citations
        $renderContext = [
            'citations' => $citations,
            'images' => [],
            'footnotes' => [],
            'labels' => [],
        ];
        $extractor = new LatexExtractor(new LatexToHtmlRenderer($renderContext));
        $extractor->extract($document);

        $renderContext['images'] = $extractor->images;
        $renderContext['footnotes'] = $extractor->footnotes;
        $renderContext['labels'] = $extractor->labels;
        $renderer = new LatexToHtmlRenderer($renderContext);

        return (object)[
            'renderer' => $renderer,
            'extractor' => $extractor,
            'document' => $document,
            'log' => [
                'undefinedCommands' => array_keys($undefinedCommands),
            ],
        ];
    }

    protected static function _applyTemplateFixes(string $latex)
    {
        // Put header text back *inside* the \section{} etc. commands
        $latex = preg_replace('/(\\\\(?:sub)*(?:section|paragraph)\*\{)}\s+\{\\\\bf (.*?})/', '$1$2', $latex);

        // Remove explicit line breaks after headers
        $latex = preg_replace('/(\\\\(?:sub)*(?:section|paragraph)\*\{.*?}) \\\\\\\\/', '$1', $latex);

        // Remove metadata table from start of article
        $latex = preg_replace('/\\\\begin{document}\n\\\\begin{flushleft}[\s\S]+?\\\\end{flushleft}/', '\\\\begin{document}', $latex);

        return $latex;
    }

    protected static function findCommands($node, $parser, &$undefinedCommands)
    {
        foreach ($node->getChildren() as $child) {
            if ($child->getType() == LatexParser::TYPE_COMMAND) {
                $command = $child->getProp('value');
                if (!$parser->isDefinedCommand($command)) {
                    $undefinedCommands[$command] = true;
                }
            }

            if ($child->hasChildren()) {
                self::findCommands($child, $parser, $undefinedCommands);
            }
        }
    }
}
