<?php
declare(strict_types=1);

namespace App\Utility\BulkUpload;

use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;

class BulkUploadData
{
    use LocatorAwareTrait;

    public string $source;
    public array $data;
    public array $mappings;

    /**
     * @param string $source
     * @param array $data
     * @param array $mappings
     */
    public function __construct(string $source, array $data, array $mappings)
    {
        $this->source = $source;
        $this->data = $data;
        $this->mappings = $mappings;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool
    {
        return array_key_exists($key, $this->data);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key)
    {
        return $this->has($key) ? $this->data[$key] : null;
    }

    /**
     * @param \Cake\ORM\Entity $entity
     */
    public function apply(Entity $entity)
    {
        $table = $this->getTableLocator()->get($this->source);
        $fields = [];

        foreach ($this->mappings as $key => $mapping) {
            if ($this->has($key)) {
                $values = $this->get($key);
            } elseif ($this->doesChangeJoinData($mapping)) {
                $table->loadInto($entity, [$mapping['table']]);
                $values = $entity->get($mapping['key']) ?? [];
            } else {
                // For validating required fields that have not changed
                $fields[$key] = $entity->get($key);
                continue;
            }

            $key = array_key_exists('key', $mapping) ? $mapping['key'] : $key;
            try {
                $values = $this->mapValues($values, $mapping);
            } catch (\Exception $error) {
                $entity->setError($key, $error->getMessage());
                continue;
            }

            // Validate values
            if (is_scalar($values) || is_null($values)) {
                $fields[$key] = $values;
            }

            // Do not mark unchanged values as dirty
            $original = $entity->get($key);
            if ((is_scalar($values) || is_null($values) || is_object($values)) && $values === $original) {
                continue;
            }

            $entity->set($key, $values);
        }

        $entity->setErrors($table->getValidator()->validate($fields));
    }

    /**
     * @param array|string $values
     * @param array $mapping
     * @return mixed
     */
    public function mapValues($values, array $mapping)
    {
        if (array_key_exists('joinData', $mapping)) {
            if ($values === '') {
                $values = [];
            } elseif (is_string($values)) {
                $values = BulkUploadFile::parseCsvField($values);
            }

            // Get joinData (such as publication comments, material aspects, etc.)
            $joinData = [];
            foreach ($mapping['joinData'] as $key => $subMapping) {
                if ($this->has($key)) {
                    $newKey = array_key_exists('key', $subMapping) ? $subMapping['key'] : $key;
                    $joinData[$newKey] = array_map(function ($value) use ($subMapping) {
                        return $this->mapValue($value, $subMapping);
                    }, BulkUploadFile::parseCsvField($this->get($key)));
                }
            }

            foreach ($values as $index => $entity) {
                if (is_string($entity)) {
                    $values[$index] = $entity = $this->mapValue($entity, $mapping);
                }

                if (is_null($entity)) {
                    continue;
                }

                if (!$entity->has('_joinData')) {
                    $entity->_joinData = new Entity([], ['source' => $mapping['joinTable']]);
                }

                foreach ($joinData as $key => $subValues) {
                    $entity->_joinData->set($key, $subValues[$index]);
                }
            }

            return $values;
        } else {
            return $this->mapValue($values, $mapping);
        }
    }

    /**
     * @param string $value
     * @param array $mapping
     * @return mixed
     */
    public function mapValue(string $value, array $mapping)
    {
        if ($value == '') {
            return null;
        } elseif (array_key_exists('table', $mapping)) {
            // Search for matching items
            $table = $this->getTableLocator()->get($mapping['table']);
            $query = $table->find()->where([$mapping['targetKey'] => $value]);
            if ($query->count() > 1) {
                throw new \ErrorException('More than one entity matches ' . h($value));
            }

            return $query->firstOrFail();
        } elseif (array_key_exists('type', $mapping)) {
            if ($mapping['type'] == 'number') {
                return floatval($value);
            } elseif ($mapping['type'] == 'bool') {
                return boolval($value);
            }
        } else {
            if (!mb_check_encoding($value, 'UTF-8')) {
                throw new \ErrorException('Invalid characters, please make sure the file is saved as UTF-8');
            }
        }

        return $value;
    }

    /**
     * @param array $mapping
     * @return bool
     */
    public function doesChangeJoinData($mapping): bool
    {
        if (!array_key_exists('joinData', $mapping)) {
            return false;
        }

        foreach (array_keys($mapping['joinData']) as $key) {
            if ($this->has($key)) {
                return true;
            }
        }

        return false;
    }
}
