<?php
declare(strict_types=1);

namespace App\Utility\BulkUpload;

use Cake\Core\Configure;
use Cake\Http\Client;
use Cake\Http\Exception\HttpException;
use Laminas\Diactoros\UploadedFile;

class BulkUploadFile
{
    /**
     * Parse an uploaded CSV file.
     *
     * @param \Laminas\Diactoros\UploadedFile $file
     * @return array
     */
    public static function parseCsv(UploadedFile $file): array
    {
        $handle = $file->getStream()->detach();
        if (fgets($handle, 4) !== "\u{FEFF}") {
            rewind($handle);
        }

        // Extract and normalize header
        $header = fgetcsv($handle);
        foreach ($header as $i => $columnHeader) {
            $header[$i] = strtolower(trim($columnHeader));
        }

        $rows = [];
        $line = 1;
        while (($row = fgetcsv($handle)) !== false) {
            $line++;

            // Skip empty lines
            if (trim(implode('', $row)) === '') {
                continue;
            }

            // Trim whitespace around values
            $row = array_map('trim', $row);

            // Try to create an associative array by combining the header from
            // the start of the file with the current line/row. If array_combine()
            // files it usually means the header and the current row are of
            // different lengths.
            try {
                $new_data = array_combine($header, $row);
            } catch (\Error $error) {
                $errors[] = __(
                    'Line {0} does not contain the same number of values ({1}) as expected from the header ({2})',
                    $line,
                    count($row),
                    count($header)
                );
                continue;
            }

            $rows[] = $new_data;
        }

        return $rows;
    }

    /**
     * @param string $field
     * @return array
     */
    public static function parseCsvField(string $field)
    {
        $pattern = '/(?:(; *)|"((?:""|[^"])+)"|([^;]+))/A';
        preg_match_all($pattern, $field, $matches, PREG_SET_ORDER | PREG_UNMATCHED_AS_NULL, 0);
        $matches[] = [';', ';', null, null];

        $list = [];
        $newValue = true;
        foreach ($matches as $match) {
            if (!is_null($match[1])) {
                if ($newValue) {
                    $list[] = '';
                }
                $newValue = true;
            } elseif (!is_null($match[2])) {
                $list[] = str_replace('""', '"', $match[2]);
                $newValue = false;
            } elseif (!is_null($match[3])) {
                $list[] = $match[3];
                $newValue = false;
            }
        }

        return $list;
    }

    /**
     * Parse an uploaded BibTeX file.
     *
     * @param \Laminas\Diactoros\UploadedFile $file
     * @return array
     * @throw \Cake\Http\Exception\HttpException
     */
    public static function parseBibtex(UploadedFile $file): array
    {
        $handle = $file->getStream()->detach();
        $content = stream_get_contents($handle);

        $http = new Client();
        $url = Configure::read('ServiceUrls.citation') . '/parse/bibtex';
        $response = $http->post($url, $content, ['headers' => ['Content-Type' => 'text/plain']]);

        $status = $response->getStatusCode();
        $body = $response->getStringBody();
        if ($status >= 400) {
            throw new HttpException($body, $status);
        }

        $body = json_decode($body, true);

        // Modify BibTeX entries to match the CSV input
        foreach ($body as $i => $entry) {
            // Unparse authors
            foreach (['author', 'editor'] as $property) {
                if (array_key_exists($property, $entry['properties'])) {
                    foreach ($entry['properties'][$property] as $j => $value) {
                        $entry['properties'][$property][$j] = $value['family'] . ', ' . $value['given'];
                    }

                    $entry['properties'][$property . 's'] = $entry['properties'][$property];
                    unset($entry['properties'][$property]);
                }
            }

            // Unparse other fields
            foreach ($entry['properties'] as $property => $value) {
                if ($property !== 'authors' && $property !== 'editors' && is_array($value)) {
                    $entry['properties'][$property] = implode(' and ', $entry['properties'][$property]);
                }
            }

            // Convert identifiers to external resources
            foreach (['doi' => 'DOI', 'isbn' => 'ISBN'] as $property => $externalResourceId) {
                if (array_key_exists($property, $entry['properties'])) {
                    // TODO overwrites existing external resources
                    // $entry['properties']['external_resources'][] = $externalResourceId;
                    // $entry['properties']['external_resources_key'][] = $entry['properties'][$property];
                    unset($entry['properties'][$property]);
                }
            }

            // Rename properties with non-standard underscores
            foreach (['booktitle' => 'book_title', 'howpublished' => 'how_published'] as $a => $b) {
                if (array_key_exists($a, $entry['properties'])) {
                    $entry['properties'][$b] = $entry['properties'][$a];
                    unset($entry['properties'][$a]);
                }
            }

            // Flatten entry
            $entry['properties']['entry_type'] = $entry['type'];
            $entry['properties']['bibtexkey'] = $entry['label'];
            $body[$i] = $entry['properties'];
        }

        return $body;
    }
}
