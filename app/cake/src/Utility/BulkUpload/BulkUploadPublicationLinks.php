<?php
declare(strict_types=1);

namespace App\Utility\BulkUpload;

use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Table;
use Cake\Utility\Inflector;

class BulkUploadPublicationLinks
{
    use LocatorAwareTrait;

    public Table $Publications;
    public Table $EntitiesPublications;
    public Table $table;
    public Entity $entity;
    public array $data;

    /**
     * @param string $table
     * @param string|int $id
     * @param array $data
     */
    public function __construct(string $table, string|int $id, array $data)
    {
        $this->Publications = $this->getTableLocator()->get('Publications');
        $this->EntitiesPublications = $this->getTableLocator()->get('EntitiesPublications');
        $this->table = $this->getTableLocator()->get(Inflector::camelize($table));
        $this->entity = $this->table->get($id, ['contain' => ['Publications']]);
        $this->data = $data;
    }

    /**
     * @return \Cake\ORM\Entity
     */
    public function apply(): Entity
    {
        $links = [];
        foreach ($this->data as $link) {
            $links[$link['bibtexkey']] = array_intersect_key($link, array_flip(['exact_reference', 'publication_type', 'publication_comments']));
        }

        $publications = $this->entity->publications;
        foreach ($publications as $publication) {
            if (!array_key_exists($publication->bibtexkey, $links)) {
                continue;
            }

            unset($links[$publication->bibtexkey]);
            foreach ($link as $property => $value) {
                if ($value !== $publication->_joinData->get($property)) {
                    $publication->_joinData->set($property, $value);
                }
            }
        }

        foreach ($links as $bibtexkey => $link) {
            $publication = $this->Publications->findAllByBibtexkey($bibtexkey)->firstOrFail();
            $publication->set('_joinData', $this->EntitiesPublications->newEntity($link + ['publication_id' => $publication->id]));
            $publication->_joinData->setDirty('publication_id', false);
            $publications[] = $publication;
        }

        $this->entity->set('publications', $publications);

        return $this->entity;
    }
}
