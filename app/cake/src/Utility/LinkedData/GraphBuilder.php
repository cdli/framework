<?php
declare(strict_types=1);

namespace App\Utility\LinkedData;

use EasyRdf\Graph;

/**
 * @property \EasyRdf\Graph $graph
 */
class GraphBuilder
{
    protected Graph $_graph;
    protected InverseMappings $_inverseMappings;

    public function __construct(Graph $graph)
    {
        $this->_graph = $graph;
        $this->_inverseMappings = new InverseMappings();
    }

    /**
     * @param \App\Utility\LinkedData\Node $node
     */
    public function add(Node $node)
    {
        foreach ($node->properties as $property => $values) {
            if (!is_array($values) || !array_is_list($values)) {
                $values = [$values];
            }

            foreach ($values as $value) {
                // Blank node
                if (is_array($value)) {
                    $value = new Node($this->_graph->newBNodeId(), $value, true);
                    $isBlankNode = true;
                } else {
                    $isBlankNode = false;
                }

                if ($value instanceof Node) {
                    $this->addResource($node->id, $property, $value->id, $isBlankNode);
                    $this->add($value);
                } elseif (!is_null($value)) {
                    $this->_graph->add($node->id, $property, $value);
                }
            }
        }
    }

    /**
     * @param string $subject
     * @param string $property
     * @param string $object
     * @param bool $isBlankNode
     */
    public function addResource(string $subject, string $property, string $object, bool $isBlankNode)
    {
        $this->_graph->add($subject, $property, [
            'type' => $isBlankNode ? 'bnode' : 'uri',
            'value' => $object,
        ]);

        if ($this->_inverseMappings->hasInverse($property)) {
            $inverse = $this->_inverseMappings->map($property);
            $this->_graph->add($object, $inverse, [
                'type' => 'uri',
                'value' => $subject,
            ]);
        }
    }
}
