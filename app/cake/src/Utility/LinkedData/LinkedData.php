<?php
declare(strict_types=1);

namespace App\Utility\LinkedData;

use EasyRdf\Literal;
use EasyRdf\RdfNamespace;
use geoPHP;

class LinkedData
{
    /**
     * Base IRI for linked data.
     *
     * @var string $_defaultNamespace
     */
    protected static string $_defaultNamespace = 'https://cdli.network/entity/';

    /**
     * Mapping of prefixes to IRIs.
     *
     * @var array $_prefixes
     */
    protected static array $_prefixes = [
        'cdli' => 'https://cdli.network/entity/',
        'crm' => 'http://www.cidoc-crm.org/cidoc-crm/',
        'crmtex' => 'http://www.cidoc-crm.org/crmtex/',
        'crma' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/',
        'frbroo' => 'http://iflastandards.info/ns/fr/frbr/frbroo/',
        'bibtex' => 'http://purl.org/net/nknouf/ns/bibtex#',
        'rdfs' => 'http://www.w3.org/2000/01/rdf-schema#',
        'geo' => 'http://www.opengis.net/ont/geosparql#',
        'geof' => 'http://www.opengis.net/def/function/geosparql/',
        'ogc' => 'http://www.opengis.net/',
        'dcmitype' => 'http://purl.org/dc/dcmitype/',
        'dcterms' => 'http://purl.org/dc/terms/',
        'nif' => 'http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#',
        'conll' => 'http://ufal.mff.cuni.cz/conll2009-st/task-description.html#',
        'owl' => 'http://www.w3.org/2002/07/owl#',
        'pleiades' => 'https://pleiades.stoa.org/places/vocab#',
        'unit' => 'http://qudt.org/vocab/unit/',
        'quantitykind' => 'http://qudt.org/vocab/quantitykind/',
        'schema' => 'https://schema.org/',
    ];

    /**
     * @param ?string $subject
     * @param ?array $properties
     * @return ?\App\Utility\LinkedData\Node entity
     */
    public static function newResource(?string $subject, ?array $properties = []): ?Node
    {
        if (is_null($subject)) {
            return null;
        }

        if (is_null(RdfNamespace::getDefault())) {
            self::setupEasyRdf();
        }

        $subject = RdfNamespace::expand($subject);
        if (!preg_match('/^https?:\/\//', $subject)) {
            $subject = RdfNamespace::getDefault() . $subject;
        }

        $subject = self::encodeUri($subject);

        return new Node($subject, $properties);
    }

    /**
     * @param array $subjects
     * @return array entities
     */
    public static function newResources(array $subjects): array
    {
        if (!is_array($subjects)) {
            return [];
        }

        return array_map(function ($subject) {
            return self::newResource($subject);
        }, $subjects);
    }

    /**
     * @param string $id
     * @param ?string $label
     * @param ?string $type
     */
    public static function newAppellation(string $id, ?string $label, ?string $type = 'crm:E41_Appellation'): ?Node
    {
        if (empty($label)) {
            return null;
        }

        return self::newResource($id, [
            'rdf:type' => self::newResource($type),
            'crm:P190_has_symbolic_content' => $label,
        ]);
    }

    /**
     * @param string $id
     * @param ?mixed $value
     * @param string $dimension
     * @param string $unit
     * @return ?\App\Utility\LinkedData\Node
     */
    public static function newDimension(string $id, $value, string $dimension, string $unit): ?Node
    {
        if (empty($value)) {
            return null;
        }

        return self::newResource($id, [
            'rdf:type' => self::newResource($dimension),
            'crm:P90_has_value' => $value,
            'crm:P91_has_unit' => self::newResource($unit),
        ]);
    }

    /**
     * @param string $id
     * @param string|array|null $geoJson
     * @return ?\App\Utility\LinkedData\Node
     */
    public static function newGeometry(string $id, string|array|null $geoJson): ?Node
    {
        if (empty($geoJson)) {
            return null;
        }

        if (is_array($geoJson)) {
            $geoJson = json_encode($geoJson);
        }

        $wkt = geoPHP::load($geoJson, 'json')->out('wkt');

        return self::newResource($id, [
            'rdf:type' => self::newResource('geo:Geometry'),
            'geo:asGeoJSON' => Literal::create($geoJson, null, 'geo:geoJSONLiteral'),
            'geo:asWKT' => Literal::create($wkt, null, 'geo:wktLiteral'),
        ]);
    }

    /**
     * @param string $uri
     * @return string
     */
    public static function encodeUri(string $uri): string
    {
        return str_replace([' ', '"'], ['%20', '%22'], $uri);
    }

    /**
     * @return string
     */
    public static function getDefaultNamespace(): string
    {
        return self::$_defaultNamespace;
    }

    /**
     * @return array
     */
    public static function getPrefixes(): array
    {
        return self::$_prefixes;
    }

    private static function setupEasyRdf()
    {
        RdfNamespace::setDefault(self::getDefaultNamespace());
        foreach (self::getPrefixes() as $prefix => $uri) {
            RdfNamespace::set($prefix, $uri);
        }
    }
}
