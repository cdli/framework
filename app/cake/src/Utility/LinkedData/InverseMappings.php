<?php
declare(strict_types=1);

namespace App\Utility\LinkedData;

use EasyRdf\RdfNamespace;

class InverseMappings
{
    public function __construct()
    {
        foreach ($this->_mappings as $key => $value) {
            $this->_inverseMappings[$value] = $key;
        }
    }

    /**
     * @param string $property
     * @return ?string
     */
    public function map(string $property): ?string
    {
        $property = RdfNamespace::expand($property);
        if (array_key_exists($property, $this->_mappings)) {
            return $this->_mappings[$property];
        } elseif (array_key_exists($property, $this->_inverseMappings)) {
            return $this->_inverseMappings[$property];
        } else {
            return null;
        }
    }

    /**
     * @param string $property
     * @return bool
     */
    public function hasInverse(string $property): bool
    {
        $property = RdfNamespace::expand($property);

        return array_key_exists($property, $this->_mappings) || array_key_exists($property, $this->_inverseMappings);
    }

    /**
     * @var array $_inverseMappings
     */
    protected array $_inverseMappings = [];

    /**
     * @var array $_mappings
     */
    protected array $_mappings = [
        // General
        'http://www.w3.org/2002/07/owl#sameAs' => 'http://www.w3.org/2002/07/owl#sameAs',

        // CIDOC-CRM
        'http://www.cidoc-crm.org/cidoc-crm/P1_is_identified_by' => 'http://www.cidoc-crm.org/cidoc-crm/P1i_identifies',
        'http://www.cidoc-crm.org/cidoc-crm/P2_has_type' => 'http://www.cidoc-crm.org/cidoc-crm/P2i_is_type_of',
        'http://www.cidoc-crm.org/cidoc-crm/P4_has_time_span' => 'http://www.cidoc-crm.org/cidoc-crm/P4i_is_time_span_of',
        'http://www.cidoc-crm.org/cidoc-crm/P5_consists_of' => 'http://www.cidoc-crm.org/cidoc-crm/P5i_forms_part_of',
        'http://www.cidoc-crm.org/cidoc-crm/P7_took_place_at' => 'http://www.cidoc-crm.org/cidoc-crm/P7i_witnessed',
        'http://www.cidoc-crm.org/cidoc-crm/P8_took_place_on_or_within' => 'http://www.cidoc-crm.org/cidoc-crm/P8i_witnessed',
        'http://www.cidoc-crm.org/cidoc-crm/P9_consists_of' => 'http://www.cidoc-crm.org/cidoc-crm/P9i_forms_part_of',
        'http://www.cidoc-crm.org/cidoc-crm/P10_falls_within' => 'http://www.cidoc-crm.org/cidoc-crm/P10i_contains',
        'http://www.cidoc-crm.org/cidoc-crm/P11_had_participant' => 'http://www.cidoc-crm.org/cidoc-crm/P11i_participated_in',
        'http://www.cidoc-crm.org/cidoc-crm/P12_occurred_in_the_presence_of' => 'http://www.cidoc-crm.org/cidoc-crm/P12i_was_present_at',
        'http://www.cidoc-crm.org/cidoc-crm/P13_destroyed' => 'http://www.cidoc-crm.org/cidoc-crm/P13i_was_destroyed_by',
        'http://www.cidoc-crm.org/cidoc-crm/P14_carried_out_by' => 'http://www.cidoc-crm.org/cidoc-crm/P14i_performed',
        'http://www.cidoc-crm.org/cidoc-crm/P15_was_influenced_by' => 'http://www.cidoc-crm.org/cidoc-crm/P15i_influenced',
        'http://www.cidoc-crm.org/cidoc-crm/P16_used_specific_object' => 'http://www.cidoc-crm.org/cidoc-crm/P16i_was_used_for',
        'http://www.cidoc-crm.org/cidoc-crm/P17_was_motivated_by' => 'http://www.cidoc-crm.org/cidoc-crm/P17i_motivated',
        'http://www.cidoc-crm.org/cidoc-crm/P19_was_intended_use_of' => 'http://www.cidoc-crm.org/cidoc-crm/P19i_was_made_for',
        'http://www.cidoc-crm.org/cidoc-crm/P20_had_specific_purpose' => 'http://www.cidoc-crm.org/cidoc-crm/P20i_was_purpose_of',
        'http://www.cidoc-crm.org/cidoc-crm/P21_had_general_purpose' => 'http://www.cidoc-crm.org/cidoc-crm/P21i_was_purpose_of',
        'http://www.cidoc-crm.org/cidoc-crm/P22_transferred_title_to' => 'http://www.cidoc-crm.org/cidoc-crm/P22i_acquired_title_through',
        'http://www.cidoc-crm.org/cidoc-crm/P23_transferred_title_from' => 'http://www.cidoc-crm.org/cidoc-crm/P23i_surrendered_title_through',
        'http://www.cidoc-crm.org/cidoc-crm/P24_transferred_title_of' => 'http://www.cidoc-crm.org/cidoc-crm/P24i_changed_ownership_through',
        'http://www.cidoc-crm.org/cidoc-crm/P25_moved' => 'http://www.cidoc-crm.org/cidoc-crm/P25i_moved_by',
        'http://www.cidoc-crm.org/cidoc-crm/P26_moved_to' => 'http://www.cidoc-crm.org/cidoc-crm/P26i_was_destination_of',
        'http://www.cidoc-crm.org/cidoc-crm/P27_moved_from' => 'http://www.cidoc-crm.org/cidoc-crm/P27i_was_origin_of',
        'http://www.cidoc-crm.org/cidoc-crm/P28_custody_surrendered_by' => 'http://www.cidoc-crm.org/cidoc-crm/P28i_surrendered_custody_through',
        'http://www.cidoc-crm.org/cidoc-crm/P29_custody_received_by' => 'http://www.cidoc-crm.org/cidoc-crm/P29i_received_custody_through',
        'http://www.cidoc-crm.org/cidoc-crm/P30_transferred_custody_of' => 'http://www.cidoc-crm.org/cidoc-crm/P30i_custody_transferred_through',
        'http://www.cidoc-crm.org/cidoc-crm/P31_has_modified' => 'http://www.cidoc-crm.org/cidoc-crm/P31i_was_modified_by',
        'http://www.cidoc-crm.org/cidoc-crm/P32_used_general_technique' => 'http://www.cidoc-crm.org/cidoc-crm/P32i_was_technique_of',
        'http://www.cidoc-crm.org/cidoc-crm/P33_used_specific_technique' => 'http://www.cidoc-crm.org/cidoc-crm/P33i_was_used_by',
        'http://www.cidoc-crm.org/cidoc-crm/P34_concerned' => 'http://www.cidoc-crm.org/cidoc-crm/P34i_was_assessed_by',
        'http://www.cidoc-crm.org/cidoc-crm/P35_has_identified' => 'http://www.cidoc-crm.org/cidoc-crm/P35i_was_identified_by',
        'http://www.cidoc-crm.org/cidoc-crm/P37_assigned' => 'http://www.cidoc-crm.org/cidoc-crm/P37i_was_assigned_by',
        'http://www.cidoc-crm.org/cidoc-crm/P38_deassigned' => 'http://www.cidoc-crm.org/cidoc-crm/P38i_was_deassigned_by',
        'http://www.cidoc-crm.org/cidoc-crm/P39_measured' => 'http://www.cidoc-crm.org/cidoc-crm/P39i_was_measured_by',
        'http://www.cidoc-crm.org/cidoc-crm/P40_observed_dimension' => 'http://www.cidoc-crm.org/cidoc-crm/P40i_was_observed_in',
        'http://www.cidoc-crm.org/cidoc-crm/P41_classified' => 'http://www.cidoc-crm.org/cidoc-crm/P41i_was_classified_by',
        'http://www.cidoc-crm.org/cidoc-crm/P42_assigned' => 'http://www.cidoc-crm.org/cidoc-crm/P42i_was_assigned_by',
        'http://www.cidoc-crm.org/cidoc-crm/P43_has_dimension' => 'http://www.cidoc-crm.org/cidoc-crm/P43i_is_dimension_of',
        'http://www.cidoc-crm.org/cidoc-crm/P44_has_condition' => 'http://www.cidoc-crm.org/cidoc-crm/P44i_is_condition_of',
        'http://www.cidoc-crm.org/cidoc-crm/P45_consists_of' => 'http://www.cidoc-crm.org/cidoc-crm/P45i_is_incorporated_in',
        'http://www.cidoc-crm.org/cidoc-crm/P46_is_composed_of' => 'http://www.cidoc-crm.org/cidoc-crm/P46i_forms_part_of',
        'http://www.cidoc-crm.org/cidoc-crm/P48_has_preferred_identifier' => 'http://www.cidoc-crm.org/cidoc-crm/P48i_is_preferred_identifier_of',
        'http://www.cidoc-crm.org/cidoc-crm/P49_has_former_or_current_keeper' => 'http://www.cidoc-crm.org/cidoc-crm/P49i_is_former_or_current_keeper_of',
        'http://www.cidoc-crm.org/cidoc-crm/P50_has_current_keeper' => 'http://www.cidoc-crm.org/cidoc-crm/P50i_is_current_keeper_of',
        'http://www.cidoc-crm.org/cidoc-crm/P51_has_former_or_current_owner' => 'http://www.cidoc-crm.org/cidoc-crm/P51i_is_former_or_current_owner_of',
        'http://www.cidoc-crm.org/cidoc-crm/P52_has_current_owner' => 'http://www.cidoc-crm.org/cidoc-crm/P52i_is_current_owner_of',
        'http://www.cidoc-crm.org/cidoc-crm/P53_has_former_or_current_location' => 'http://www.cidoc-crm.org/cidoc-crm/P53i_is_former_or_current_location_of',
        'http://www.cidoc-crm.org/cidoc-crm/P54_has_current_permanent_location' => 'http://www.cidoc-crm.org/cidoc-crm/P54i_is_current_permanent_location_of',
        'http://www.cidoc-crm.org/cidoc-crm/P55_has_current_location' => 'http://www.cidoc-crm.org/cidoc-crm/P55i_currently_holds',
        'http://www.cidoc-crm.org/cidoc-crm/P56_bears_feature' => 'http://www.cidoc-crm.org/cidoc-crm/P56i_is_found_on',
        'http://www.cidoc-crm.org/cidoc-crm/P59_has_section' => 'http://www.cidoc-crm.org/cidoc-crm/P59i_is_located_on_or_within',
        'http://www.cidoc-crm.org/cidoc-crm/P62_depicts' => 'http://www.cidoc-crm.org/cidoc-crm/P62i_is_depicted_by',
        'http://www.cidoc-crm.org/cidoc-crm/P65_shows_visual_item' => 'http://www.cidoc-crm.org/cidoc-crm/P65i_is_shown_by',
        'http://www.cidoc-crm.org/cidoc-crm/P67_refers_to' => 'http://www.cidoc-crm.org/cidoc-crm/P67i_is_referred_to_by',
        'http://www.cidoc-crm.org/cidoc-crm/P68_foresees_use_of' => 'http://www.cidoc-crm.org/cidoc-crm/P68i_use_foreseen_by',
        'http://www.cidoc-crm.org/cidoc-crm/P69_has_association_with' => 'http://www.cidoc-crm.org/cidoc-crm/P69i_is_associated_with',
        'http://www.cidoc-crm.org/cidoc-crm/P70_documents' => 'http://www.cidoc-crm.org/cidoc-crm/P70i_is_documented_in',
        'http://www.cidoc-crm.org/cidoc-crm/P71_lists' => 'http://www.cidoc-crm.org/cidoc-crm/P71i_is_listed_in',
        'http://www.cidoc-crm.org/cidoc-crm/P72_has_language' => 'http://www.cidoc-crm.org/cidoc-crm/P72i_is_language_of',
        'http://www.cidoc-crm.org/cidoc-crm/P73_has_translation' => 'http://www.cidoc-crm.org/cidoc-crm/P73i_is_translation_of',
        'http://www.cidoc-crm.org/cidoc-crm/P74_has_current_or_former_residence' => 'http://www.cidoc-crm.org/cidoc-crm/P74i_is_current_or_former_residence_of',
        'http://www.cidoc-crm.org/cidoc-crm/P75_possesses' => 'http://www.cidoc-crm.org/cidoc-crm/P75i_is_possessed_by',
        'http://www.cidoc-crm.org/cidoc-crm/P76_has_contact_point' => 'http://www.cidoc-crm.org/cidoc-crm/P76i_provides_access_to',
        'http://www.cidoc-crm.org/cidoc-crm/P86_falls_within' => 'http://www.cidoc-crm.org/cidoc-crm/P86i_contains',
        'http://www.cidoc-crm.org/cidoc-crm/P89_falls_within' => 'http://www.cidoc-crm.org/cidoc-crm/P89i_contains',
        'http://www.cidoc-crm.org/cidoc-crm/P91_has_unit' => 'http://www.cidoc-crm.org/cidoc-crm/P91i_is_unit_of',
        'http://www.cidoc-crm.org/cidoc-crm/P92_brought_into_existence' => 'http://www.cidoc-crm.org/cidoc-crm/P92i_was_brought_into_existence_by',
        'http://www.cidoc-crm.org/cidoc-crm/P93_took_out_of_existence' => 'http://www.cidoc-crm.org/cidoc-crm/P93i_was_taken_out_of_existence_by',
        'http://www.cidoc-crm.org/cidoc-crm/P94_has_created' => 'http://www.cidoc-crm.org/cidoc-crm/P94i_was_created_by',
        'http://www.cidoc-crm.org/cidoc-crm/P95_has_formed' => 'http://www.cidoc-crm.org/cidoc-crm/P95i_was_formed_by',
        'http://www.cidoc-crm.org/cidoc-crm/P96_by_mother' => 'http://www.cidoc-crm.org/cidoc-crm/P96i_gave_birth',
        'http://www.cidoc-crm.org/cidoc-crm/P97_from_father' => 'http://www.cidoc-crm.org/cidoc-crm/P97i_was_father_for',
        'http://www.cidoc-crm.org/cidoc-crm/P98_brought_into_life' => 'http://www.cidoc-crm.org/cidoc-crm/P98i_was_born',
        'http://www.cidoc-crm.org/cidoc-crm/P99_dissolved' => 'http://www.cidoc-crm.org/cidoc-crm/P99i_was_dissolved_by',
        'http://www.cidoc-crm.org/cidoc-crm/P100_was_death_of' => 'http://www.cidoc-crm.org/cidoc-crm/P100i_died_in',
        'http://www.cidoc-crm.org/cidoc-crm/P101_had_as_general_use' => 'http://www.cidoc-crm.org/cidoc-crm/P101i_was_use_of',
        'http://www.cidoc-crm.org/cidoc-crm/P102_has_title' => 'http://www.cidoc-crm.org/cidoc-crm/P102i_is_title_of',
        'http://www.cidoc-crm.org/cidoc-crm/P103_was_intended_for' => 'http://www.cidoc-crm.org/cidoc-crm/P103i_was_intention_of',
        'http://www.cidoc-crm.org/cidoc-crm/P104_is_subject_to' => 'http://www.cidoc-crm.org/cidoc-crm/P104i_applies_to',
        'http://www.cidoc-crm.org/cidoc-crm/P105_right_held_by' => 'http://www.cidoc-crm.org/cidoc-crm/P105i_has_right_on',
        'http://www.cidoc-crm.org/cidoc-crm/P106_is_composed_of' => 'http://www.cidoc-crm.org/cidoc-crm/P106i_forms_part_of',
        'http://www.cidoc-crm.org/cidoc-crm/P107_has_current_or_former_member' => 'http://www.cidoc-crm.org/cidoc-crm/P107i_is_current_or_former_member_of',
        'http://www.cidoc-crm.org/cidoc-crm/P108_has_produced' => 'http://www.cidoc-crm.org/cidoc-crm/P108i_was_produced_by',
        'http://www.cidoc-crm.org/cidoc-crm/P109_has_current_or_former_curator' => 'http://www.cidoc-crm.org/cidoc-crm/P109i_is_current_or_former_curator_of',
        'http://www.cidoc-crm.org/cidoc-crm/P110_augmented' => 'http://www.cidoc-crm.org/cidoc-crm/P110i_was_augmented_by',
        'http://www.cidoc-crm.org/cidoc-crm/P111_added' => 'http://www.cidoc-crm.org/cidoc-crm/P111i_was_added_by',
        'http://www.cidoc-crm.org/cidoc-crm/P112_diminished' => 'http://www.cidoc-crm.org/cidoc-crm/P112i_was_diminished_by',
        'http://www.cidoc-crm.org/cidoc-crm/P113_removed' => 'http://www.cidoc-crm.org/cidoc-crm/P113i_was_removed_by',
        'http://www.cidoc-crm.org/cidoc-crm/P123_resulted_in' => 'http://www.cidoc-crm.org/cidoc-crm/P123i_resulted_from',
        'http://www.cidoc-crm.org/cidoc-crm/P124_transformed' => 'http://www.cidoc-crm.org/cidoc-crm/P124i_was_transformed_by',
        'http://www.cidoc-crm.org/cidoc-crm/P125_used_object_of_type' => 'http://www.cidoc-crm.org/cidoc-crm/P125i_was_type_of_object_used_in',
        'http://www.cidoc-crm.org/cidoc-crm/P126_employed' => 'http://www.cidoc-crm.org/cidoc-crm/P126i_was_employed_in',
        'http://www.cidoc-crm.org/cidoc-crm/P127_has_broader_term' => 'http://www.cidoc-crm.org/cidoc-crm/P127i_has_narrower_term',
        'http://www.cidoc-crm.org/cidoc-crm/P128_carries' => 'http://www.cidoc-crm.org/cidoc-crm/P128i_is_carried_by',
        'http://www.cidoc-crm.org/cidoc-crm/P129_is_about' => 'http://www.cidoc-crm.org/cidoc-crm/P129i_is_subject_of',
        'http://www.cidoc-crm.org/cidoc-crm/P130_shows_features_of' => 'http://www.cidoc-crm.org/cidoc-crm/P130i_features_are_also_found_on',
        'http://www.cidoc-crm.org/cidoc-crm/P134_continued' => 'http://www.cidoc-crm.org/cidoc-crm/P134i_was_continued_by',
        'http://www.cidoc-crm.org/cidoc-crm/P135_created_type' => 'http://www.cidoc-crm.org/cidoc-crm/P135i_was_created_by',
        'http://www.cidoc-crm.org/cidoc-crm/P136_was_based_on' => 'http://www.cidoc-crm.org/cidoc-crm/P136i_supported_type_creation',
        'http://www.cidoc-crm.org/cidoc-crm/P137_exemplifies' => 'http://www.cidoc-crm.org/cidoc-crm/P137i_is_exemplified_by',
        'http://www.cidoc-crm.org/cidoc-crm/P138_represents' => 'http://www.cidoc-crm.org/cidoc-crm/P138i_has_representation',
        'http://www.cidoc-crm.org/cidoc-crm/P140_assigned_attribute_to' => 'http://www.cidoc-crm.org/cidoc-crm/P140i_was_attributed_by',
        'http://www.cidoc-crm.org/cidoc-crm/P141_assigned' => 'http://www.cidoc-crm.org/cidoc-crm/P141i_was_assigned_by',
        'http://www.cidoc-crm.org/cidoc-crm/P142_used_constituent' => 'http://www.cidoc-crm.org/cidoc-crm/P142i_was_used_in',
        'http://www.cidoc-crm.org/cidoc-crm/P143_joined' => 'http://www.cidoc-crm.org/cidoc-crm/P143i_was_joined_by',
        'http://www.cidoc-crm.org/cidoc-crm/P144_joined_with' => 'http://www.cidoc-crm.org/cidoc-crm/P144i_gained_member_by',
        'http://www.cidoc-crm.org/cidoc-crm/P145_separated' => 'http://www.cidoc-crm.org/cidoc-crm/P145i_left_by',
        'http://www.cidoc-crm.org/cidoc-crm/P146_separated_from' => 'http://www.cidoc-crm.org/cidoc-crm/P146i_lost_member_by',
        'http://www.cidoc-crm.org/cidoc-crm/P147_curated' => 'http://www.cidoc-crm.org/cidoc-crm/P147i_was_curated_by',
        'http://www.cidoc-crm.org/cidoc-crm/P148_has_component' => 'http://www.cidoc-crm.org/cidoc-crm/P148i_is_component_of',
        'http://www.cidoc-crm.org/cidoc-crm/P150_defines_typical_parts_of' => 'http://www.cidoc-crm.org/cidoc-crm/P150i_defines_typical_wholes_for',
        'http://www.cidoc-crm.org/cidoc-crm/P151_was_formed_from' => 'http://www.cidoc-crm.org/cidoc-crm/P151i_participated_in',
        'http://www.cidoc-crm.org/cidoc-crm/P152_has_parent' => 'http://www.cidoc-crm.org/cidoc-crm/P152i_is_parent_of',
        'http://www.cidoc-crm.org/cidoc-crm/P156_occupies' => 'http://www.cidoc-crm.org/cidoc-crm/P156i_is_occupied_by',
        'http://www.cidoc-crm.org/cidoc-crm/P157_is_at_rest_relative_to' => 'http://www.cidoc-crm.org/cidoc-crm/P157i_provides_reference_space_for',
        'http://www.cidoc-crm.org/cidoc-crm/P160_has_temporal_projection' => 'http://www.cidoc-crm.org/cidoc-crm/P160i_is_temporal_projection_of',
        'http://www.cidoc-crm.org/cidoc-crm/P161_has_spatial_projection' => 'http://www.cidoc-crm.org/cidoc-crm/P161i_is_spatial_projection_of',
        'http://www.cidoc-crm.org/cidoc-crm/P164_is_temporally_specified_by' => 'http://www.cidoc-crm.org/cidoc-crm/P164i_temporally_specifies',
        'http://www.cidoc-crm.org/cidoc-crm/P165_incorporates' => 'http://www.cidoc-crm.org/cidoc-crm/P165i_is_incorporated_in',
        'http://www.cidoc-crm.org/cidoc-crm/P166_was_a_presence_of' => 'http://www.cidoc-crm.org/cidoc-crm/P166i_had_presence',
        'http://www.cidoc-crm.org/cidoc-crm/P167_was_within' => 'http://www.cidoc-crm.org/cidoc-crm/P167i_includes',
        'http://www.cidoc-crm.org/cidoc-crm/P173_starts_before_or_with_the_end_of' => 'http://www.cidoc-crm.org/cidoc-crm/P173i_ends_after_or_with_the_start_of',
        'http://www.cidoc-crm.org/cidoc-crm/P174_starts_before_the_end_of' => 'http://www.cidoc-crm.org/cidoc-crm/P174i_ends_after_the_start_of',
        'http://www.cidoc-crm.org/cidoc-crm/P175_starts_before_or_with_the_start_of' => 'http://www.cidoc-crm.org/cidoc-crm/P175i_starts_after_or_with_the_start_of',
        'http://www.cidoc-crm.org/cidoc-crm/P176_starts_before_the_start_of' => 'http://www.cidoc-crm.org/cidoc-crm/P176i_starts_after_the_start_of',
        'http://www.cidoc-crm.org/cidoc-crm/P177_assigned_property_of_type' => 'http://www.cidoc-crm.org/cidoc-crm/P177i_is_type_of_property_assigned',
        'http://www.cidoc-crm.org/cidoc-crm/P179_had_sales_price' => 'http://www.cidoc-crm.org/cidoc-crm/P179i_was_sales_price_of',
        'http://www.cidoc-crm.org/cidoc-crm/P180_has_currency' => 'http://www.cidoc-crm.org/cidoc-crm/P180i_was_currency_of',
        'http://www.cidoc-crm.org/cidoc-crm/P182_ends_before_or_with_the_start_of' => 'http://www.cidoc-crm.org/cidoc-crm/P182i_starts_after_or_with_the_end_of',
        'http://www.cidoc-crm.org/cidoc-crm/P183_ends_before_the_start_of' => 'http://www.cidoc-crm.org/cidoc-crm/P183i_starts_after_the_end_of',
        'http://www.cidoc-crm.org/cidoc-crm/P184_ends_before_or_with_the_end_of' => 'http://www.cidoc-crm.org/cidoc-crm/P184i_ends_with_or_after_the_end_of',
        'http://www.cidoc-crm.org/cidoc-crm/P185_ends_before_the_end_of' => 'http://www.cidoc-crm.org/cidoc-crm/P185i_ends_after_the_end_of',
        'http://www.cidoc-crm.org/cidoc-crm/P186_produced_thing_of_product_type' => 'http://www.cidoc-crm.org/cidoc-crm/P186i_is_produced_by',
        'http://www.cidoc-crm.org/cidoc-crm/P187_has_production_plan' => 'http://www.cidoc-crm.org/cidoc-crm/P187i_is_production_plan_for',
        'http://www.cidoc-crm.org/cidoc-crm/P188_requires_production_tool' => 'http://www.cidoc-crm.org/cidoc-crm/P188i_is_production_tool_for',
        'http://www.cidoc-crm.org/cidoc-crm/P189_approximates' => 'http://www.cidoc-crm.org/cidoc-crm/P189i_is_approximated_by',
        'http://www.cidoc-crm.org/cidoc-crm/P191_had_duration' => 'http://www.cidoc-crm.org/cidoc-crm/P191i_was_duration_of',
        'http://www.cidoc-crm.org/cidoc-crm/P195_was_a_presence_of' => 'http://www.cidoc-crm.org/cidoc-crm/P195i_had_presence',
        'http://www.cidoc-crm.org/cidoc-crm/P196_defines' => 'http://www.cidoc-crm.org/cidoc-crm/P196i_is_defined_by',
        'http://www.cidoc-crm.org/cidoc-crm/P197_covered_parts_of' => 'http://www.cidoc-crm.org/cidoc-crm/P197i_was_partially_covered_by',
        'http://www.cidoc-crm.org/cidoc-crm/P198_holds_or_supports' => 'http://www.cidoc-crm.org/cidoc-crm/P198i_is_held_or_supported_by',

        // CRMtex
        'http://www.cidoc-crm.org/crmtex/TXP1_used_writing_system' => 'http://www.cidoc-crm.org/crmtex/TXP1i_writing_system_used_for',
        'http://www.cidoc-crm.org/crmtex/TXP2_includes' => 'http://www.cidoc-crm.org/crmtex/TXP2i_is_included_within',
        'http://www.cidoc-crm.org/crmtex/TXP4_has_segment' => 'http://www.cidoc-crm.org/crmtex/TXP4i_is_segment_of',
        'http://www.cidoc-crm.org/crmtex/TXP5_wrote' => 'http://www.cidoc-crm.org/crmtex/TXP5i_was_written_by',
        'http://www.cidoc-crm.org/crmtex/TXP6_encodes' => 'http://www.cidoc-crm.org/crmtex/TXP6i_is_encoding_of',
        'http://www.cidoc-crm.org/crmtex/TXP7_has_item' => 'http://www.cidoc-crm.org/crmtex/TXP7i_is_item_of',
        'http://www.cidoc-crm.org/crmtex/TXP8_has_component' => 'http://www.cidoc-crm.org/crmtex/TXP8i_is_component_of',
        'http://www.cidoc-crm.org/crmtex/TXP9_is_encoded_using' => 'http://www.cidoc-crm.org/crmtex/TXP9i_was_used_to_encode',
        'http://www.cidoc-crm.org/crmtex/TXP10_deciphered_text' => 'http://www.cidoc-crm.org/crmtex/TXP10i_was_deciphered_by',
        'http://www.cidoc-crm.org/crmtex/TXP11_transcribed' => 'http://www.cidoc-crm.org/crmtex/TXP11i_was_transcribed_by',
        'http://www.cidoc-crm.org/crmtex/TXP12_has_style' => 'http://www.cidoc-crm.org/crmtex/TXP12i_is_style_of',
        'http://www.cidoc-crm.org/crmtex/TXP13_deciphered_via_the_representation' => 'http://www.cidoc-crm.org/crmtex/TXP13i_was_representation_used_for_deciphering',
        'http://www.cidoc-crm.org/crmtex/TXP14_used_copy_or_representation_of' => 'http://www.cidoc-crm.org/crmtex/TXP14i_was_deciphered_via_copy_or_representation',
        'http://www.cidoc-crm.org/crmtex/TXP15_recorded_correspondence' => 'http://www.cidoc-crm.org/crmtex/TXP15i_was_recorded_by',
        'http://www.cidoc-crm.org/crmtex/TXP16_employs_script' => 'http://www.cidoc-crm.org/crmtex/TXP16i_is_employed_by',
        'http://www.cidoc-crm.org/crmtex/TXP17_has_part' => 'http://www.cidoc-crm.org/crmtex/TXP17i_forms_part_of',
        'http://www.cidoc-crm.org/crmtex/TXP18_read' => 'http://www.cidoc-crm.org/crmtex/TXP18i_was_read_by',

        // CRMaechaeo
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP1_produced' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP1i_was_produced_by',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP2_discarded' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP2i_was_discarded_by',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP3_investigated' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP3i_was_investigated_by',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP4_produced_surface' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP4i_was_surface_produced_by',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP5_removed_part_or_all_of' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP5i_was_partially_or_totally_removed_by',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP6_intended_to_approximate' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP6i_was_approximated_by',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP7_produced' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP7i_was_produced_by',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP8_disturbed' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP8i_was_disturbed_by',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP9_took_matter_from' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP9i_provided_matter_to',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP10_destroyed' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP10i_was_destroyed_by',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP11_has_physical_relation_to' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP11i_is_physical_relation_from',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP12_confines' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP12i_is_confined_by',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP13_has_stratigraphic_relation_to' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP13i_is_stratigraphic_relation_of',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP15_is_or_contains_remains_of' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP15i_is_or_has_remains_contained_in',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP16_assigned_attribute_to' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP16i_was_attributed_by',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP17_is_found_by' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP17i_found',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP18_is_embedding_of' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP18i_is_embedded',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP19_is_embedding_in' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP19i_contains_embedding',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP21_contains' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP21i_is_contained_in',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP23_finishes' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP23i_is_finished_by',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP24_starts' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP24i_is_started_by',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP25_occurs_during' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP25i_includes',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP26_overlaps_in_time_with' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP26i_is_overlapped_in_time_by',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP27_meets_in_time_with' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP27i_is_met_in_time_by',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP28_occurs_before' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP28i_occurs_after',
        'http://www.cidoc-crm.org/extensions/crmarchaeo/AP32_discarded_into' => 'http://www.cidoc-crm.org/extensions/crmarchaeo/AP32i_was_discarded_by',

        // FRBRoo
        'http://iflastandards.info/ns/fr/frbr/frbroo/CLP104' => 'http://iflastandards.info/ns/fr/frbr/frbroo/CLP104i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/CLP105' => 'http://iflastandards.info/ns/fr/frbr/frbroo/CLP105i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/CLP2' => 'http://iflastandards.info/ns/fr/frbr/frbroo/CLP2i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/CLP43' => 'http://iflastandards.info/ns/fr/frbr/frbroo/CLP43i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/CLP45' => 'http://iflastandards.info/ns/fr/frbr/frbroo/CLP45i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/CLP46' => 'http://iflastandards.info/ns/fr/frbr/frbroo/CLP46i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/CLR6' => 'http://iflastandards.info/ns/fr/frbr/frbroo/CLR6i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R1' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R1i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R10' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R10i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R11' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R11i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R12' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R12i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R13' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R13i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R14' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R14i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R15' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R15i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R16' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R16i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R17' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R17i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R18' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R18i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R19' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R19i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R2' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R2i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R20' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R20i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R21' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R21i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R22' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R22i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R23' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R23i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R24' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R24i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R25' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R25i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R26' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R26i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R27' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R27i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R28' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R28i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R29' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R29i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R3' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R3i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R30' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R30i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R31' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R31i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R32' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R32i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R34' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R34i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R35' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R35i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R36' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R36i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R37' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R37i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R38' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R38i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R39' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R39i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R4' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R4i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R40' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R40i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R41' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R41i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R42' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R42i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R43' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R43i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R44' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R44i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R45' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R45i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R46' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R46i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R47' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R47i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R48' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R48i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R49' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R49i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R5' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R5i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R50' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R50i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R51' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R51i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R52' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R52i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R53' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R53i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R54' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R54i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R55' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R55i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R56' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R56i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R57' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R57i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R58' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R58i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R59' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R59i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R6' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R6i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R60' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R60i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R61' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R61i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R62' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R62i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R63' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R63i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R64' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R64i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R7' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R7i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R8' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R8i',
        'http://iflastandards.info/ns/fr/frbr/frbroo/R9' => 'http://iflastandards.info/ns/fr/frbr/frbroo/R9i',
    ];
}
