<?php
declare(strict_types=1);

namespace App\Utility\LinkedData;

class Node
{
    public string $id;
    public array $properties;

    public function __construct(string $id, array $properties)
    {
        $this->id = $id;
        $this->properties = $properties;
    }
}
