<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\View;

use Cake\Event\EventManager;
use Cake\Http\Response;
use Cake\Http\ServerRequest as Request;

trait TableTrait
{
    use SerializeTrait;

    public function __construct(
        ?Request $request = null,
        ?Response $response = null,
        ?EventManager $eventManager = null,
        array $viewOptions = []
    ) {
        parent::__construct($request, $response, $eventManager, $viewOptions);
        $this->response = $this->response->withType($this->_responseType);
    }

    protected function prepareTableData($entities)
    {
        $table = [];

        foreach ($entities as $entity) {
            $row = $this->serializeEntity($entity);
            $table[] = array_values($row);
        }

        return $table;
    }

    protected function prepareTableHeader($entities)
    {
        if (array_key_exists('_header', $this->viewVars)) {
            return $this->viewVars['_header'];
        }

        if (empty($entities)) {
            return null;
        }

        $row = $this->serializeEntity(array_values($entities)[0]);

        return array_keys($row);
    }

    protected function serializeEntity($entity)
    {
        if (method_exists($entity, 'getTableRow')) {
            if (array_key_exists('_admin', $this->viewVars)) {
                $row = $entity->getTableRow($this->viewVars['_admin']);
            } else {
                $row = $entity->getTableRow();
            }

            if (array_key_exists('_header', $this->viewVars) && is_array($this->viewVars['_header'])) {
                $header = array_fill_keys($this->viewVars['_header'], '');
                $row = array_intersect_key($row, $header);
                $row = array_merge($header, $row);
            }

            return $row;
        } else {
            return array_map(function ($value) {
                if (empty($value)) {
                    return null;
                } elseif (is_array($value)) {
                    return $value[array_keys($value)[0]];
                } else {
                    return $value;
                }
            }, $entity);
        }
    }

    protected function _rowsToSerialize($serialize)
    {
        $data = $this->_dataToSerialize($serialize);
        if (is_array($data)) {
            return $data;
        } else {
            return [$data];
        }
    }
}
