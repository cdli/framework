<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\View;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Cake\View\View;

/**
 * Application View
 *
 * Your application’s default view class
 *
 * @link https://book.cakephp.org/3.0/en/views.html#the-app-view
 */
class AppView extends View
{
    /**
     * Initialization hook method.
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadHelper('Form', [
            'widgets' => [
                'toggle' => ['Toggle'],
                'addButton' => ['AddButton'],
            ],
            'autoSetCustomValidity' => false,
        ]);
        $this->Form->setTemplates([
            'toggleContainer' => '<div class="input {{type}}{{required}}">' .
                    '<div class="d-flex justify-content-between w-70-mq-100 mb-3">{{content}}</div>' .
                '</div>',
            'toggleFormGroup' => '{{label}}{{input}}',
            'toggle' => '<span class="css-toggle mb-0">' .
                    '<input type="checkbox" name="{{name}}" value="{{value}}"{{attrs}}>' .
                    '<span class="toggle-slider round"></span>' .
                '</span>',
        ]);

        $this->loadHelper('Authentication.Identity');
        $this->loadHelper('ServerTiming');
    }

    /**
     * @param string|null $template
     * @param string|false|null $layout
     * @return
     */
    public function render(?string $template = null, $layout = null): string
    {
        $action = $this->request->getParam('action');
        if ($action === 'view') {
            $vars = $this->viewVars;

            if (
                isset($vars['_serialize']) &&
                is_string($vars['_serialize']) &&
                isset($vars[$vars['_serialize']])
            ) {
                $entity = $vars[$vars['_serialize']];

                if ($entity instanceof Entity) {
                    $repository = $entity->getSource();
                    $table = TableRegistry::get($repository);
                    $displayField = $table->getDisplayField();

                    if (!empty($displayField)) {
                        $title = $entity[$displayField] . ' - ' . Inflector::humanize(Inflector::tableize($repository));
                        $this->assign('title', $title);
                    }
                }
            }
        } elseif ($action === 'index') {
            $title = Inflector::humanize(str_replace(DIRECTORY_SEPARATOR, '_', $this->templatePath));
            $this->assign('title', $title);
        }

        return parent::render($template, $layout);
    }
}
