<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\View;

use App\Utility\LinkedData\LinkedData;
use Cake\View\SerializedView;

class JsonLdView extends SerializedView
{
    use LinkedDataTrait;

    protected $_responseType = 'jsonld';

    protected function _serialize($serialize): string
    {
        $data = $this->_dataToSerialize($serialize);
        $graph = $this->prepareDataExport($data);

        return $graph->serialise('jsonld', [
            'compact' => true,
            'context' => (object)LinkedData::getPrefixes(),
        ]);
    }
}
