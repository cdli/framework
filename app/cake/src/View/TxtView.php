<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\View;

use Cake\ORM\TableRegistry;
use Cake\View\SerializedView;

class TxtView extends SerializedView
{
    use SerializeTrait;

    /**
     * List of special view vars.
     *
     * @var array
     */
    protected $_specialVars = ['_serialize', '_displayField'];

    /**
     * @var string
     */
    protected $_responseType = 'txt';

    protected function _serialize($serialize): string
    {
        $entities = $this->_dataToSerialize($serialize);

        return implode("\n", array_map(function ($entity) {
            $table = TableRegistry::get($entity->getSource());
            if (array_key_exists('_displayField', $this->viewVars)) {
                $displayField = $this->viewVars['_displayField'];
            } else {
                $displayField = $table->getDisplayField();
            }

            return $entity->get($displayField);
        }, $entities));
    }
}
