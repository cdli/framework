<?php
declare(strict_types=1);

namespace App\View\Helper;

use Cake\View\Helper;

class TabsHelper extends Helper
{
    protected ?array $config = null;

    public function create(array $tabs, ?string $active): string
    {
        $active ??= array_keys($tabs)[0];
        $this->config = [
            'active' => $active,
            'tabs' => $tabs,
        ];

        $result = [
            '<ul class="nav nav-tabs" role="tablist">',
        ];

        foreach ($tabs as $id => $label) {
            $class = $active === $id ? 'active' : '';
            $selected = $active === $id ? 'true' : 'false';

            $result[] = '    <li class="nav-item" role="presentation">';
            $result[] = "        <a class=\"nav-link $class\" id=\"$id-tab\" data-toggle=\"tab\" href=\"#$id\" role=\"tab\" aria-controls=\"$id\" aria-selected=\"$selected\">";
            $result[] = "            $label";
            $result[] = '        </a>';
            $result[] = '    </li>';
        }

        $result[] = '</ul>';
        $result[] = '<div class="tab-content">';

        return implode("\n", $result);
    }

    public function createTab(string $id): string
    {
        if (is_null($this->config)) {
            return '';
        }

        $class = $this->config['active'] === $id ? 'show active' : '';

        return "    <div class=\"tab-pane $class\" id=\"$id\" role=\"tabpanel\" aria-labelledby=\"$id-tab\">";
    }

    public function endTab(): string
    {
        if (is_null($this->config)) {
            return '';
        }

        return '    </div>';
    }

    public function end(): string
    {
        if (is_null($this->config)) {
            return '';
        }

        return '</div>';
    }
}
