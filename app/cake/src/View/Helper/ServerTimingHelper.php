<?php
declare(strict_types=1);

namespace App\View\Helper;

use App\Utility\ServerTimings\ServerTimings;
use Cake\Event\EventInterface;
use Cake\View\Helper;

class ServerTimingHelper extends Helper
{
    protected array $timers = [];
    protected ?ServerTimings $timings;

    public function initialize(array $config): void
    {
        $this->timings = $this->getView()->getRequest()->getAttribute('serverTimings');
    }

    public function start(string $name, $description = null): void
    {
        if (!is_null($this->timings)) {
            $this->timers[$name] = $this->timings->create($name, $description);
            $this->timers[$name]->start();
        }
    }

    public function stop(string $name)
    {
        if (array_key_exists($name, $this->timers)) {
            $this->timers[$name]->stop();
        }
    }

    public function beforeRenderFile(EventInterface $event, $file)
    {
        $this->start('render_' . $this->_cleanFile($file));
    }

    public function afterRenderFile(EventInterface $event, $file)
    {
        $this->stop('render_' . $this->_cleanFile($file));
    }

    private function _cleanFile(string $file): string
    {
        $file = preg_replace('#^/srv/app/cake/templates/|\.php$#', '', $file);

        if (!preg_match('/^(cell|element|layout)/', $file)) {
            $file = 'view_' . $file;
        }

        return $file;
    }
}
