<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\View\Helper;

use Cake\View\Helper;

class DropdownHelper extends Helper
{
    public $helpers = ['Html'];

    public function open($name)
    {
        return implode("\n", [
            "<div class='no-js-dd'>",
            '<button>' . $name . '</button>',
            "<div class='no-js-dd-content'>",
        ]);
    }

    public function close()
    {
        return '</div></div>';
    }

    public function notificationItem($label, $count, $url = null, $badgeClass = null)
    {
        if ($count === 0) {
            $url = null;
        }

        if (is_null($badgeClass)) {
            $badgeClass = !is_null($url) && $count > 0 ? 'badge-danger' : 'badge-light';
        }
        $badge = $this->Html->tag('span', $count, ['class' => "badge $badgeClass"]);

        if (is_null($url)) {
            return $this->Html->tag('span', $label . ' ' . $badge, ['class' => 'dropdown-item disabled']);
        } else {
            return $this->Html->link($label . ' ' . $badge, $url, ['class' => 'dropdown-item', 'escapeTitle' => false]);
        }
    }
}
