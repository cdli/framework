<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\View\Helper;

use App\Model\Entity\Article;
use App\Model\Entity\Artifact;
use App\Model\Entity\Publication;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\ResultSet;
use Cake\View\Helper;

/**
 * @property \App\View\Helper\ScriptsHelper $Scripts
 */
class CitationHelper extends Helper
{
    public $helpers = ['Scripts'];

    /**
     * Format any data into a reference.
     *
     * Option and format documentation: see /app/tools/citation.js/README.md.
     *
     * @param array $data
     * @param string $format
     * @param array $options
     * @return string
     * @throws \Cake\Http\Exception\HttpException
     */
    public function formatReference($data, string $format, array $options): string
    {
        return $this->Scripts->formatReference($this->removeFields($data), $format, $options);
    }

    /**
     * Same as CitationHelper::formatReference(), but returns a HTML error message instead
     * of throwing an exception, and puts BibTeX/RIS/etc. output in a `<pre>` element.
     *
     * @param array $data
     * @param string $format
     * @param array $options
     * @return string
     */
    public function formatReferenceAsHtml($data, string $format, array $options): string
    {
        try {
            $output = $this->formatReference($data, $format, $options);
        } catch (\Exception $e) {
            return '<p class="text-danger">' . __('Error') . ': ' . __('Cannot generate reference') . '</p>';
        }

        if ($format === 'bibliography') {
            return $output;
        } else {
            return '<pre class="pl-3">' . $output . '</pre>';
        }
    }

    /**
     * Get the default data for citing a webpage of the framework.
     *
     * @return array
     */
    public function getDefaultData()
    {
        $view = $this->getView();

        return [
            'type' => 'webpage',
            'title' => $view->fetch('title'),
            'author' => [['literal' => 'CDLI contributors']],
            'container-title' => 'Cuneiform Digital Library Initiative',
            'issued' => [['date-parts' => explode('-', date('Y-m-d'))]],
            'accessed' => [['date-parts' => explode('-', date('Y-m-d'))]],
            'URL' => 'https://cdli.mpiwg-berlin.mpg.de' . $view->getRequest()->getRequestTarget(),
        ];
    }

    /**
     * Remove certain fields from associative arrays.
     */
    private function removeFields($data): array
    {
        return array_map(function ($entry) {
            if ($entry instanceof Entity) {
                if ($entry instanceof Article) {
                    $entry = $this->extractData($entry, [
                        'title',
                        'authors' => [
                            'first',
                            'last',
                            'author',
                            '_joinData' => ['sequence'],
                        ],
                        'article_type',
                        'created',
                        'year',
                        'article_no',
                    ]);
                } elseif ($entry instanceof Artifact) {
                    $entry = $this->extractData($entry, [
                        'designation',
                        'artifacts_updates' => [
                            'update_event' => ['created', 'approved'],
                        ],
                        'inscription' => ['transliteration_clean'],
                        'id',
                        'has_fragments', // for recognition
                    ]);
                } elseif ($entry instanceof Publication) {
                    $entry = $this->extractData($entry, [
                        'address',
                        'annote',
                        'bibtexkey',
                        'book_title',
                        'chapter',
                        'crossref',
                        'edition',
                        'editor',
                        'how_published',
                        'institution',
                        'journal_id',
                        'month',
                        'note',
                        'number',
                        'organization',
                        'pages',
                        'publisher',
                        'school',
                        'series',
                        'title',
                        'volume',
                        'year',
                        'entry_type' => ['label'],
                        'authors' => ['first', 'last'],
                        'editors' => ['first', 'last'],
                        'journal' => ['journal'],
                    ]);
                }
            }

            return $entry;
        }, $this->getDataArray($data));
    }

    private function extractData(Entity $entity, $shape)
    {
        $data = [];
        foreach ($shape as $key => $value) {
            if (is_int($key)) {
                if ($entity->has($value)) {
                    $data[$value] = $entity->get($value);
                }
            } elseif ($entity->has($key)) {
                $value = $entity->get($key);
                if ($value instanceof Query) {
                    $value = $value->all();
                }
                if ($value instanceof ResultSet) {
                    $value = $value->toArray();
                }

                if (is_array($value)) {
                    $data[$key] = array_map(function ($entity) use ($shape, $key) {
                        return $this->extractData($entity, $shape[$key]);
                    }, $value);
                } else {
                    $data[$key] = $this->extractData($value, $shape[$key]);
                }
            }
        }

        return $data;
    }

    private function getDataArray($value)
    {
        if ($value instanceof Query) {
            $value = $value->all();
        }
        if ($value instanceof ResultSet) {
            $value = $value->toArray();
        }
        if ($value instanceof Entity) {
            $value = [$value];
        }

        return $value;
    }
}
