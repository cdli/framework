<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\View\Helper;

use Cake\View\Helper;

/**
 * @property \Cake\View\Helper\HtmlHelper $Html
 * @property \Cake\View\Helper\FormHelper $Form
 */
class ActionsHelper extends Helper
{
    public $helpers = ['Html', 'Form'];

    public function manageRelation($controller, $id)
    {
        return implode("\n", [
            $this->Html->link('View', ['controller' => $controller, 'action' => 'view', $id]),
            $this->Html->link('Edit', ['controller' => $controller, 'action' => 'edit', $id]),
            $this->Form->postLink('Delete', ['controller' => $controller, 'action' => 'delete', $id], ['confirm' => __('Are you sure you want to delete # {0}?', $id)]),
        ]);
    }
}
