<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\View\Helper;

use Cake\View\Helper;

class AccordionHelper extends Helper
{
    public function partOpen($id, $title, string $tag = 'p', bool $open = false)
    {
        return implode("\n", [
            '<section id="' . h($id) . '">',
            '<input type="checkbox" id="' . h($id) . '-toggle" class="accordion-textbox"' . ($open ? ' checked' : '') . '>',
            '<' . $tag . ' class="accordion-title"><label class="w-100" for="' . h($id) . '-toggle">' . h($title) . '</label></' . $tag . '>',
            '<div class="detail-leaf">',
        ]);
    }

    public function partClose()
    {
        return '</div></section>';
    }
}
