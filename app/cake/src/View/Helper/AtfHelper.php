<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\View\Helper;

use Cake\View\Helper;

class AtfHelper extends Helper
{
    public function format(string $atf, $highlight = [])
    {
        $processed = '';

        foreach (preg_split('/[\r\n]+/', $atf) as $line) {
            foreach (self::$lineTypes as $pattern => $type) {
                if (preg_match($pattern, $line)) {
                    $line = h(substr($line, $type['prefixLength']));
                    $line = $this->highlight($line, $type['type'], $highlight);
                    $processed .= implode($line, $type['formatting']);
                    break;
                }
            }
        }

        return $processed;
    }

    public function formatSnippet($atf, array $highlight)
    {
        $processed = '';

        if (is_null($atf)) {
            return $processed;
        }

        // Line marker based on structure. First element is generally obv./rev.,
        // the second element an optional specifier derived from column name
        // or surface letter.
        $marker = ['', ''];

        foreach (preg_split('/[\r\n]+/', $atf) as $line) {
            foreach (self::$lineTypes as $pattern => $type) {
                if (preg_match($pattern, $line)) {
                    $line = h(substr($line, $type['prefixLength']));

                    // Markers (based on structure). See comment above.
                    if ($type['type'] === 'atf_structure') {
                        $parts = explode(' ', $line);
                        if ($parts[0] === 'obverse') {
                            $marker[0] = 'obv.';
                        } elseif ($parts[0] === 'reverse') {
                            $marker[0] = 'rev.';
                        } elseif ($parts[0] === 'column') {
                            $marker[1] = self::formatRomanNumeral($parts[1]);
                        } elseif ($parts[0] === 'surface') {
                            $marker[0] = $parts[1] === 'a' ? 'obv.' : 'rev.';
                        } else {
                            $marker[0] = $marker[1] = '';
                        }
                    }

                    // Transliteration. Line with prefixed marker, only lines
                    // matching the query are shown.
                    $highlightedLine = $this->highlight($line, $type['type'], $highlight);
                    if ($line !== $highlightedLine) {
                        if ($type['type'] === 'atf_transliteration') {
                            $processed .= "<b>$marker[0] $marker[1]</b> ";
                        }
                        $processed .= implode($highlightedLine, $type['formatting']);
                    }

                    break;
                }
            }
        }

        return $processed;
    }

    protected function highlight(string $line, string $type, array $highlight)
    {
        foreach ($highlight as [$highlightType, $pattern]) {
            if ($highlightType === $type) {
                $line = preg_replace($pattern, '<span class="bg-warning">$0</span>', $line);
            }
        }

        return $line;
    }

    private static function formatRomanNumeral($integer)
    {
        $lookup = [
            'M' => 1000,
            'CM' => 900,
            'D' => 500,
            'CD' => 400,
            'C' => 100,
            'XC' => 90,
            'L' => 50,
            'XL' => 40,
            'X' => 10,
            'IX' => 9,
            'V' => 5,
            'IV' => 4,
            'I' => 1,
        ];

        $integer = intval($integer);
        $result = '';

        foreach ($lookup as $roman => $value) {
            $matches = intval($integer / $value);
            $result .= str_repeat($roman, $matches);
            $integer = $integer % $value;
        }

        return strtolower($result);
    }

    protected static array $lineTypes = [
        '/^[0-9]/' => [
            'type' => 'atf_transliteration',
            'formatting' => ['', '<br>'],
            'prefixLength' => 0,
        ],
        '/^#tr\.ts:/' => [
            'type' => 'atf_transcription',
            'formatting' => ['<i>&emsp;', '</i><br>'],
            'prefixLength' => 4,
        ],
        '/^#tr\./' => [
            'type' => 'atf_translation.text',
            'formatting' => ['<i>&emsp;', '</i><br>'],
            'prefixLength' => 4,
        ],
        '/^[$#]/' => [
            'type' => 'atf_comments',
            'formatting' => ['<i>&emsp;', '</i><br>'],
            'prefixLength' => 1,
        ],
        '/^@/' => [
            'type' => 'atf_structure',
            'formatting' => ['<b>', '</b><br>'],
            'prefixLength' => 1,
        ],
    ];
}
