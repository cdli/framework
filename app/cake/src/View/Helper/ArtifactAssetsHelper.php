<?php
declare(strict_types=1);

namespace App\View\Helper;

use App\Model\Entity\Artifact;
use App\Model\Entity\ArtifactAsset;
use App\Model\Entity\Collection;
use Cake\View\Helper;

/**
 * @property \Cake\View\Helper\HtmlHelper $Html
 */
class ArtifactAssetsHelper extends Helper
{
    public $helpers = ['Html'];

    public function getImage($assets): string|null
    {
        $main = $this->_selectMain($assets);

        if (!is_null($main)) {
            $main = DS . $main->getPath();
        }

        return $main;
    }

    public function getThumbnail($assets): string
    {
        $main = $this->_selectMain($assets);

        if (!is_null($main)) {
            $main = $main->getThumbnailPath();
        }

        return is_null($main) ? '/images/no-image.jpg' : DS . $main;
    }

    public function orderAssets($assets): array
    {
        if ($assets instanceof Artifact) {
            $assets = $assets->artifact_assets;
        }
        $assets = is_null($assets) ? [] : array_slice($assets, 0);

        usort($assets, function ($a, $b) {
            if ($a->file_format === 'jpg' && $b->file_format !== 'jpg') {
                return -1;
            } elseif ($a->file_format !== 'jpg' && $b->file_format === 'jpg') {
                return 1;
            }

            if (is_null($a->artifact_aspect) && !is_null($b->artifact_aspect)) {
                return -1;
            } elseif (!is_null($a->artifact_aspect) && is_null($b->artifact_aspect)) {
                return 1;
            }

            if ($a->asset_type === 'photo' && $b->asset_type === 'lineart') {
                return -1;
            } elseif ($a->asset_type === 'lineart' && $b->asset_type === 'photo') {
                return 1;
            } elseif ($a->asset_type === 'lineart' && $b->asset_type !== 'lineart') {
                return -1;
            } elseif ($a->asset_type !== 'lineart' && $b->asset_type === 'lineart') {
                return 1;
            }
        });

        return $assets;
    }

    public function newEntityFromData(array $data): ArtifactAsset
    {
        $entity = new ArtifactAsset();
        foreach ($data as $key => $value) {
            if ($value !== '') {
                $entity->set($key, $value);
            }
        }

        return $entity;
    }

    public function getAttributionInfo(ArtifactAsset $asset, Collection|null $collection): array
    {
        $info = [
            'attribution' => $asset->has('license_attribution') ? h($asset->license_attribution) : null,
            'license' => $asset->has('license_id') ? $this->makeLicenseLink($asset->license_id) : null,
            'comment' => $asset->has('license_comment') ? h($asset->license_comment) : null,
        ];

        if ($asset->asset_type === 'lineart') {
            if (is_null($info['attribution'])) {
                $info['attribution'] = $this->Html->link(__('[see publications]'), '/terms-of-use', ['target' => '_blank']);
            }
        } elseif ($asset->asset_type === 'photo') {
            $attribution = $collection->license_attribution ?? $collection->collection ?? null;
            if (is_null($info['attribution']) && !is_null($attribution)) {
                $info['attribution'] = $this->Html->link($attribution, [
                    'prefix' => false,
                    'controller' => 'Collections',
                    'action' => 'view',
                    $collection->id,
                ]);
            }

            $license = $collection->license_id ?? null;
            if (is_null($info['license']) && !is_null($license)) {
                $info['license'] = $this->makeLicenseLink($license);
            }

            $comment = $collection->license_comment ?? null;
            if (is_null($info['comment']) && !is_null($comment)) {
                $info['comment'] = $comment;
            }
        }

        if (is_null($info['attribution'])) {
            $info['attribution'] = $this->Html->link(__('[see terms of use]'), '/terms-of-use', ['target' => '_blank']);
        }

        return $info;
    }

    public function makeLicenseLink(string $license): string
    {
        return $this->Html->link($license, 'https://spdx.org/licenses/' . $license . '.html');
    }

    private function _selectMain($assets): ArtifactAsset|null
    {
        $assets = $this->orderAssets($assets);
        if (count($assets) === 0) {
            return null;
        }

        $main = $assets[0];
        if ($main->file_format !== 'jpg' && $main->asset_type !== 'photo' && $main->asset_type !== 'lineart') {
            return null;
        }

        return $main;
    }
}
