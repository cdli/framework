<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\View;

use App\Model\Entity\Inscription;
use App\Model\Entity\LinkedDataInterface;
use App\Utility\LinkedData\GraphBuilder;
use App\Utility\LinkedData\LinkedData;
use App\Utility\LinkedData\Node;
use EasyRdf\Graph;

/**
 * @property \App\View\Helper\ScriptsHelper $Scripts
 */
trait LinkedDataTrait
{
    use SerializeTrait;

    protected function prepareDataExport(array $data)
    {
        $graph = new Graph();
        $graph->setBNodePrefix('x' . hash('sha256', $this->getRequest()->getRequestTarget()) . 'x');

        $graphBuilder = new GraphBuilder($graph);
        foreach ($this->getLinkedData($data) as $node) {
            $graphBuilder->add($node);
        }

        foreach ($data as $entity) {
            if ($entity instanceof Inscription && !is_null($entity->annotation)) {
                $this->addConllRdf($graph, $entity);
            }
        }

        return $graph;
    }

    /**
     * @param mixed $data
     * @return \App\Utility\LinkedData\Node[]|null
     */
    protected function getLinkedData($data): ?array
    {
        if ($data instanceof LinkedDataInterface) {
            $id = $data->getUri();
            $data = $data->getLinkedData();
            $data = LinkedData::newResource($id, $data);

            return [$data];
        } elseif (is_array($data)) {
            $items = [];
            foreach ($data as $item) {
                if (is_array($item) || is_object($item)) {
                    $item = $this->getLinkedData($item);

                    if ($item instanceof Node) {
                        $items[] = $item;
                    } elseif (is_array($item)) {
                        foreach ($item as $node) {
                            $items[] = $node;
                        }
                    }
                }
            }

            return $items;
        }

        return null;
    }

    protected function addConllRdf(Graph $graph, Inscription $inscription)
    {
        $this->loadHelper('Scripts');

        $base = LinkedData::getDefaultNamespace() . $inscription->getUri();
        $graph->parse($this->Scripts->formatConllRdf($inscription, ['base' => $base . '#']), 'turtle');

        foreach ($graph->allOfType('nif:Sentence') as $sentence) {
            // Only attach a sentence to the inscription it belongs to (which
            // happens to be the first time this property gets added). Not the
            // prettiest solution, but there are uglier onces like checking
            // whether the URI starts with the same path (/inscription/:id).
            if (!$sentence->hasProperty('dcterms:isPartOf')) {
                $sentence->addResource('dcterms:isPartOf', $base);
            }
        }
    }
}
