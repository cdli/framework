<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\View;

use Cake\Routing\Router;
use Cake\View\SerializedView;

class ViaJsonView extends SerializedView
{
    use SerializeTrait;

    /**
     * @var string
     */
    protected $_responseType = 'viajson';

    protected function _serialize($serialize): string
    {
        $annotations = $this->_dataToSerialize($serialize);
        $collectionUrl = $this->viewVars['_collectionUrl'] ?? '';

        $data = [];

        foreach ($annotations as $annotation) {
            $image = Router::url($annotation->artifact_asset->getPath(), true);

            if (!array_key_exists("$image-1", $data)) {
                $data["$image-1"] = [
                    'filename' => $image,
                    'size' => -1,
                    'regions' => [],
                ];
            }

            $data["$image-1"]['regions'][] = $annotation->getViaJson();
        }

        return json_encode($data);
    }
}
