<?php
declare(strict_types=1);

namespace App\View;

class NdJsonView extends JsonView
{
    protected $_responseType = 'ndjson';

    protected function _serialize($serialize): string
    {
        $data = $this->_dataToSerialize($serialize);
        if (!(is_array($data) && array_is_list($data))) {
            $data = [$data];
        }
        $data = $this->prepareJson($data);

        return implode("\n", array_map('json_encode', $data)) . "\n";
    }
}
