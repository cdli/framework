<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\View;

use App\Model\Entity\Inscription;
use App\Utility\CdliProcessor\Convert;
use Cake\View\SerializedView;

class JtfView extends SerializedView
{
    use SerializeTrait;

    /**
     * List of special view vars.
     *
     * @var array
     */
    protected $_specialVars = ['_serialize'];

    /**
     * @var string
     */
    protected $_responseType = 'jtf';

    protected function _serialize($serialize): string
    {
        $inscriptions = $this->_dataToSerialize($serialize);

        if (!is_array($inscriptions)) {
            $inscriptions = [$inscriptions];
        }

        if (count($inscriptions) === 1 && $inscriptions[0] instanceof Inscription) {
            $this->response = $this->response->withDownload(
                $inscriptions[0]->artifact->getCdliNumber() . '-jtf.json'
            );
        } else {
            $this->response = $this->response->withDownload('jtf.json');
        }

        $objects = [];

        foreach ($inscriptions as $inscription) {
            if ($inscription instanceof Inscription) {
                if ($inscription->has('jtf')) {
                    array_push($objects, ...json_decode($inscription->jtf));
                }

                $jtf = Convert::cAtfToJtf($inscription->atf);
                if ($jtf->success) {
                    array_push($objects, ...$jtf->objects);
                }
            } else {
                continue;
            }
        }

        return json_encode($objects);
    }
}
