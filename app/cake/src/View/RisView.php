<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\View;

use Cake\View\SerializedView;

/**
 * @property \App\View\Helper\CitationHelper $Citation
 */
class RisView extends SerializedView
{
    use SerializeTrait;

    /**
     * List of special view vars.
     *
     * @var array
     */
    protected $_specialVars = ['_serialize'];

    /**
     * @var string
     */
    protected $_responseType = 'ris';

    public function initialize(): void
    {
        parent::initialize();
        $this->loadHelper('Scripts');
    }

    protected function _serialize($serialize): string
    {
        return $this->Citation->formatReference(
            $this->_dataToSerialize($serialize),
            'ris',
            []
        );
    }
}
