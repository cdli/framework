<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\View;

use Cake\ORM\Entity;
use Cake\Utility\Inflector;
use Cake\View\JsonView as CakeJsonView;

class JsonView extends CakeJsonView
{
    use SerializeTrait;

    protected $_responseType = 'json';

    protected function _serialize($serialize): string
    {
        $data = $this->_dataToSerialize($serialize);
        $this->set('data', $this->prepareJson($data));

        return parent::_serialize('data');
    }

    protected function prepareJson($data)
    {
        if ($data instanceof Entity) {
            return $this->prepareEntity($data);
        }

        if (is_array($data)) {
            foreach ($data as $key => $item) {
                if (is_array($item) || is_object($item)) {
                    $data[$key] = $this->prepareJson($item);
                } elseif (empty($item)) {
                    unset($data[$key]);
                }
            }
        }

        return $data;
    }

    protected function prepareEntity($entity)
    {
        $data = $entity->jsonSerialize();

        $repository = $entity->getSource();
        $key = Inflector::underscore(Inflector::singularize($repository));

        if (isset($entity->jsonSchema)) {
            $data = array_intersect_key($data, array_flip($entity->jsonSchema) + ['_joinData' => 0]);
        } else {
            throw new \ErrorException(__('Could not serialize data: {0} did not provide a schema', $key));
        }

        if (isset($data['_joinData'])) {
            $joinData = $this->prepareEntity($data['_joinData']);
            unset($data['_joinData']);
            $joinData[$key] = $this->prepareJson($data);

            return $joinData;
        } else {
            return $this->prepareJson($data);
        }
    }
}
