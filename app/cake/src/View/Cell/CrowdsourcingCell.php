<?php
declare(strict_types=1);

namespace App\View\Cell;

use App\Controller\AppController;
use Cake\Controller\ComponentRegistry;
use Cake\View\Cell;

/**
 * @property \App\Model\Table\UpdateEventsTable $UpdateEvents
 * @property \App\Controller\Component\GranularAccessComponent $GranularAccess
 * @property \App\Controller\Component\ChangesetsComponent $Changesets
 */
class CrowdsourcingCell extends Cell
{
    public \App\Controller\Component\GranularAccessComponent $GranularAccess;
    public \App\Controller\Component\ChangesetsComponent $Changesets;

    public function initialize(): void
    {
        $components = new ComponentRegistry(new AppController($this->request, $this->response));
        $this->GranularAccess = $components->load('GranularAccess');
        $this->Changesets = $components->load('Changesets');
    }

    public function render(string|null $template = null): string
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            return '';
        }

        return parent::render($template);
    }

    public function display($user = null)
    {
        if (is_null($user)) {
            $this->set(['total' => 0, 'created' => 0, 'submitted' => 0, 'approved' => 0]);

            return;
        }

        $this->loadModel('UpdateEvents');

        $changesets = $this->Changesets->getAll();
        $this->set('total', count($changesets));

        $authoredUpdateEvents = $this->UpdateEvents->find()
            ->select(['count' => 'COUNT(*)', 'status'])
            ->matching('Creators.Users', function ($q) use ($user) {
                return $q->where(['Users.id' => $user]);
            })
            // Remove the default order of the table; this messes with the index
            ->order('created_by', true)
            ->group(['status']);

        foreach ($authoredUpdateEvents->all() as $updateEvent) {
            $this->set($updateEvent->status, $updateEvent->count);
        }
    }
}
