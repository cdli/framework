<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\View\Cell;

/**
 * @property \App\Model\Table\ArtifactAssetsTable $ArtifactAssets
 */
class ArtifactAssetAnnotationsCell extends ArtifactAssetCell
{
    public function display($id, $annotations = [], $canSubmitEdits = false)
    {
        parent::display($id);
        $asset = $this->viewBuilder()->getVar('asset');
        $asset->annotations = $annotations;
        $this->set('canSubmitEdits', $canSubmitEdits);
    }
}
