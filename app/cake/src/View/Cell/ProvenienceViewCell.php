<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\View\Cell;

use Cake\View\Cell;

/**
 * @property \App\Model\Table\ProveniencesTable $Proveniences
 */
class ProvenienceViewCell extends Cell
{
    public function display($id)
    {
        $this->loadModel('Proveniences');
        $provenience = $this->Proveniences->get($id, [
            'contain' => ['Regions'],
        ]);
        $this->set(compact('provenience'));
    }
}
