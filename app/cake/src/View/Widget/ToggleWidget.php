<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\View\Widget;

use Cake\View\Form\ContextInterface;
use Cake\View\Widget\CheckboxWidget;

class ToggleWidget extends CheckboxWidget
{
    /**
     * Data defaults.
     *
     * @var array<string, mixed>
     */
    protected $defaults = [
        'name' => '',
        'value' => 1,
        'val' => null,
        'disabled' => false,
        'templateVars' => [],
    ];

    /**
     * Methods that render the widget.
     *
     * @param array $data The data to build an input with.
     * @param \Cake\View\Form\ContextInterface $context The current form context.
     * @return string
     */
    public function render(array $data, ContextInterface $context): string
    {
        $data += $this->mergeDefaults($data, $context);

        if ($this->_isChecked($data)) {
            $data['checked'] = true;
        }
        unset($data['val']);

        $attrs = $this->_templates->formatAttributes(
            $data,
            ['name', 'value']
        );

        return $this->_templates->format('toggle', [
            'name' => $data['name'],
            'value' => $data['value'],
            'templateVars' => $data['templateVars'],
            'attrs' => $attrs,
        ]);
    }
}
