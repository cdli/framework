<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\View;

use Cake\View\SerializedView;

class NTriplesView extends SerializedView
{
    use LinkedDataTrait;

    protected $_responseType = 'nt';

    protected function _serialize($serialize): string
    {
        $data = $this->_dataToSerialize($serialize);
        $graph = $this->prepareDataExport($data);

        return $graph->serialise('ntriples');
    }
}
