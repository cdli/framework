<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\View;

use Cake\View\SerializedView;

class WadmView extends SerializedView
{
    use SerializeTrait;

    /**
     * @var string
     */
    protected $_responseType = 'wadm';

    protected function _serialize($serialize): string
    {
        $annotations = $this->_dataToSerialize($serialize);
        $collectionUrl = $this->viewVars['_collectionUrl'] ?? '';

        return json_encode([
            '@context' => 'http://www.w3.org/ns/anno.jsonld',
            'id' => $collectionUrl,
            'type' => 'AnnotationCollection',
            'total' => count($annotations),
            'first' => [
                'id' => $collectionUrl . '#page1',
                'type' => 'AnnotationPage',
                'startIndex' => 0,
                'items' => array_map(function ($annotation) {
                    $data = $annotation->getW3cJson();
                    unset($data['@context']);

                    return $data;
                }, $annotations),
            ],
        ]);
    }
}
