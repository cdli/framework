<?php
declare(strict_types=1);

namespace App\Error;

use Cake\Core\Configure;
use Cake\Error\ExceptionRenderer;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\InternalErrorException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Routing\Exception\MissingRouteException;

class AppExceptionRenderer extends ExceptionRenderer
{
    /**
     * @return \Cake\Http\Response
     */
    public function render(): \Cake\Http\Response
    {
        if (
            $this->error instanceof MissingRouteException &&
            array_key_exists('context', $this->error->getAttributes())
        ) {
            $message = "Missing route\n" . $this->error->getMessage();
            $this->error = new InternalErrorException($message, null, $this->error);
        }

        if (
            $this->error instanceof UnauthorizedException ||
            $this->error instanceof ForbiddenException
        ) {
            $this->error = new NotFoundException();
        }

        $this->controller->loadComponent('RequestHandler');
        $type = $this->controller->RequestHandler->prefers();
        if ($type !== 'html' && !Configure::read('debug')) {
            $code = $this->getHttpCode($this->error);

            return $this->controller->getResponse()->withStatus($code);
        }

        return parent::render();
    }
}
