<?php
declare(strict_types=1);

namespace App\Middleware;

use App\Utility\ServerTimings\ServerTimings;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ServerTimingMiddleware implements MiddlewareInterface
{
    private const REQUEST_TIME = 'REQUEST_TIME_FLOAT';

    /**
     * @var \App\Utility\ServerTimings\ServerTimings
     */
    private $serverTimings;

    public function __construct()
    {
        $this->serverTimings = new ServerTimings();
    }

    /**
     * Adapted from https://github.com/fetzi/server-timing/blob/master/src/ServerTimingMiddleware.php
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        // Calculate bootstrap time
        $serverParams = $request->getServerParams();
        if (isset($serverParams[static::REQUEST_TIME])) {
            $bootstrap = $this->serverTimings->create('bootstrap');
            $bootstrap->start($serverParams[static::REQUEST_TIME]);
            $bootstrap->stop();
        }

        // Start request timer
        $requestServerTiming = $this->serverTimings->create('request');
        $requestServerTiming->start();

        // Make timings accessible
        $request = $request->withAttribute('serverTimings', $this->serverTimings);

        // Handle request
        $response = $handler->handle($request);

        // Stop request timer
        $requestServerTiming->stop();

        // Send timings
        $timings = $this->serverTimings->getTimings();
        if (!is_null($timings)) {
            $response = $response->withAddedHeader('Server-Timing', $timings);
        }

        return $response;
    }
}
