<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

/**
 * ExternalResources Controller
 *
 * @property \App\Model\Table\ExternalResourcesTable $ExternalResources
 * @method \App\Model\Entity\ExternalResource[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Model\Table\ArtifactsExternalResourcesTable $ArtifactsExternalResources
 * @property \App\Controller\Component\ApiComponent $Api
 */
class ExternalResourcesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);
        $this->loadComponent('Search');

        // Set access for public.
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'limit' => 1000,
            'maxLimit' => 1000,
        ] + $this->paginate;

        $query = $this->ExternalResources->find('search', [
            'search' => $this->request->getQueryParams(),
        ]);
        if ($this->request->getQuery('letter')) {
            $query->where([
                'ExternalResources.external_resource LIKE' => $this->request->getQuery('letter') . '%',
            ]);
        }

        $externalResources = $this->paginate($query);

        $access_granted = $this->RequestAccess->checkUserRoles([1]);
        $this->set(compact('externalResources', 'access_granted'));
        $this->set('_serialize', 'externalResources');
    }

    /**
     * View method
     *
     * @param string|null $id External Resource id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $externalResource = $this->ExternalResources->get($id);

        $artifacts = $this->loadModel('ArtifactsExternalResources');
        $count = $artifacts->find('list', ['conditions' => ['external_resource_id' => $id]])->count();
        $access_granted = $this->RequestAccess->checkUserRoles([1]);

        $this->set(compact('externalResource', 'count', 'access_granted'));
        $this->set('_serialize', 'externalResource');
    }
}
