<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * @property \App\Model\Table\EntitiesNamesTable $EntitiesNames
 * @method \App\Model\Entity\EntitiesName[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ApiComponent $Api
 */
class EntitiesNamesController extends AppController
{
    /**
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData']]);
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if (!$this->Api->requestsFormat(['LinkedData'])) {
            $this->redirect('/');
        }

        $this->paginate = [
            'limit' => 1000,
            'maxLimit' => 1000,
        ] + $this->paginate;

        $entitiesNames = $this->EntitiesNames->find();
        $entitiesNames = $this->paginate($entitiesNames);

        $this->set('entitiesNames', $entitiesNames);
        $this->set('_serialize', 'entitiesNames');
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException
     */
    public function view($id = null)
    {
        $entitiesName = $this->EntitiesNames->get($id);

        if (!$this->Api->requestsFormat(['LinkedData'])) {
            $this->redirect('/' . $entitiesName->getSourceUri());
        }

        $this->set('entitiesName', $entitiesName);
        $this->set('_serialize', 'entitiesName');
    }
}
