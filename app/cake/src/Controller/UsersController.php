<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

use Cake\Auth\DefaultPasswordHasher;
use Cake\I18n\Time;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Model\Table\StaffTable $Staff
 * @property \App\Model\Table\AuthorsTable $Authors
 */
class UsersController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadModel('Staff');
        $this->loadModel('Authors');
    }

    /**
     * beforeFilter method
     *
     * To set up access before this contoller is executed.
     *
     * @return \Cake\Http\Response|void
     */
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);

        $requestedAction = $this->getRequest()->getParam('action');

        $actionsPresent = ['login', 'register', 'profile', 'profileEdit', 'delete'];

        // Check if any undefined actions are requested
        if (!in_array($requestedAction, $actionsPresent)) {
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }

        // Action accessible only when not logged in.
        $actionForNotLoggedIn = ['login', 'register'];

        // If Not logged in redirect to profile page
        if (is_null($this->Authentication->getIdentity())) {
            $this->Authentication->allowUnauthenticated($actionForNotLoggedIn);
        } else {
            if ($requestedAction !== 'login' && in_array($requestedAction, $actionForNotLoggedIn)) {
                return $this->redirect([
                    'action' => 'profile',
                ]);
            }
        }
    }

    /**
     * login method
     *
     * @return \Cake\Http\Response|void
     **/
    public function login()
    {
        $data = $this->request->getQueryParams();
        if ($this->getRequest()->is('post')) {
            $user = $this->Authentication->getIdentity();

            // Check if login available
            if ($user) {
                // Check if user deactivated or banned
                if (!$user['active']) {
                    // If last login more than 6 months else banned
                    if (Time::now()->toUnixString() - $user['last_login_at']->toUnixString() > 6 * 30 * 24 * 60 * 60) {
                        return $this->Flash->error(__('Your account is inactive. Contact an administrator to reactivate it.'));
                    } else {
                        return $this->Flash->error(sprintf('This account has been deactivated. If you think this account should be active, please contact us at <a href="%s">cdli-support@ames.ox.ac.uk</a>.', h('mailto:cdli-support@ames.ox.ac.uk')), ['escape' => false]);
                    }
                }

                $session = $this->getRequest()->getSession();
                $modified_user_data['username'] = $user['username'];
                $modified_user_data['2fa_status'] = $user['2fa_status'];

                // Storing in session variable 'user' : [username, 2fa_key, 2fa_status, created]
                $session->write([
                    'user' => [
                        'type' => 'login',
                        'user' => $modified_user_data,
                        'session_verified' => 0,
                        '2fa_session_created' => Time::now(),
                    ],
                ]);

                if (isset($data['redirect'])) {
                    return $this->redirect([
                        'controller' => 'Twofactor',
                        'action' => 'index',
                        '?' => [
                            'redirect' => $data['redirect'],
                        ],
                    ]);
                } else {
                    return $this->redirect([
                        'controller' => 'Twofactor',
                        'action' => 'index',
                    ]);
                }
            } else {
                $this->Flash->error(__('Username or password is incorrect'));
            }
        } elseif ($this->Authentication->getIdentity()) {
            return $this->redirect([
                'action' => 'profile',
            ]);
        }
    }

    /**
     * Register method
     *
     * @return \Cake\Http\Response|void
     */
    public function register()
    {
        $user = $this->Users->newEmptyEntity();

        if ($this->getRequest()->is('post')) {
            $user = $this->Users->patchEntity(
                $user,
                $this->request->getData(),
                [
                    'fields' => ['first_name', 'last_name', 'username', 'email', 'password', 'password_confirm'],
                ]
            );

            $currentTime = Time::now();
            $user->last_login_at = $currentTime;
            $user->created_at = $currentTime;
            $user->modified_at = $currentTime;
            $user->active = 0;

            $errors = $user->getErrors();

            // Display errors generated after validation of user's data.
            if (!empty($errors)) {
                $this->set('errors', $errors);
            } else {
                $session = $this->getRequest()->getSession();

                // Storing in session variable 'user'
                $session->write([
                    'user' => [
                        'type' => 'register',
                        'user' => $user,
                        '2fa_session_created' => Time::now(),
                    ],
                ]);

                // This session_verified = 'false' will be set for every new session.
                // Will be set 'true' once user submit 2FA secret code or setup 2FA for first time.
                $this->getRequest()->getSession()->write('session_verified', 0);

                return $this->redirect([
                    'controller' => 'Twofactor',
                    'action' => 'index',
                ]);
            }
        }

        $this->set(compact('user'));
    }

    /**
     * Profile method
     *
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function profile($param = null)
    {
        //varaible to store if user is staff or not
        $access = null;
        $author_name = null;

        if ($this->Authentication->getIdentityData('author_id')) {
            $auth_id = $this->Authentication->getIdentityData('author_id');

            /** @var ?\Cake\ORM\Entity $staff */
            $staff = $this->Staff
                ->find()
                ->where(['author_id' => $auth_id])
                ->first();

            //to check if the staff id exist and of which author
            if ($staff != null && $staff->author_id == $auth_id) {
                $access = true;
            }

            /** @var \Cake\ORM\Entity $author_name */
            $author_name = $this->Authors
                ->find()
                ->where(['id' => $auth_id])
                ->first();
            $author_name = $author_name->author;
        }

        if ($param === 'edit') {
            $this->setAction('profileEdit');
        } else {
            $username = $this->Authentication->getIdentityData('username');
            $user = $this->Users->findByUsername($username)->first();

            if ($user) {
                $modifiedUser = $this->Authentication->getIdentity();
                $modifiedUser['username'] = $user['username'];
                $modifiedUser['email'] = $user['email'];
                $modifiedUser['last_login_at'] = $user['last_login_at'];
                $modifiedUser['active'] = $user['active'];
                $modifiedUser['modified_at'] = $user['modified_at'];
                $modifiedUser['created_at'] = $user['created_at'];
                $modifiedUser['roles'] = $this->getTableLocator()->get('RolesUsers')->getUserRoles($user['id']);

                $this->set('user', $modifiedUser);
            } else {
                $this->Flash->error('No such user exists');

                return $this->redirect('/');
            }
            $this->set(compact('access', 'author_name'));
        }
    }

    /**
     * Profile Edit method
     *
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function profileEdit()
    {
        if ($this->getRequest()->is(['post'])) {
            $username = $this->Authentication->getIdentityData('username');
            $user = $this->Users->findByUsername($username)->first();

            $formData = $this->getRequest()->getData();

            $checkOldPassword = (new DefaultPasswordHasher())->check($formData['old_password'], $user['password']);
            if (!$checkOldPassword) {
                $this->Flash->error(__('The old password is incorrect. Please, enter correct old password.'));
            } else {
                $sameAsPreviousPassword = (new DefaultPasswordHasher())->check($formData['password'], $user['password']);

                // Password check is kept with only previous password. (Not the list of previously used passwords by that user)
                if ($sameAsPreviousPassword) {
                    $this->Flash->error(__('The new password cannot be the same as your previously used passwords.'));
                } else {
                    $updateduser = $this->Users->patchEntity($user, $formData, ['fields' => 'password']);

                    $errors = $updateduser->getErrors();

                    // Display errors generated after validation of user's data.
                    if (!empty($errors)) {
                        foreach ($errors as $error) {
                            foreach ($error as $key => $value) {
                                $this->Flash->error($value);
                            }
                        }
                    } else {
                        if ($this->Users->save($updateduser)) {
                            $this->Flash->success(__('Your password has been updated.'));

                            return $this->redirect([
                            'action' => 'profile',
                            ]);
                        } else {
                            $this->Flash->error(__('The password could not be updated. Please, try again.'));
                        }
                    }
                }
            }
        }

        $this->set('users', $this->Authentication->getIdentity());
    }

    public function delete()
    {
        if ($this->request->is('post')) {
            /** @var \App\Model\Entity\User $user */
            $user = $this->Authentication->getIdentity();
            if ($this->Users->delete($user)) {
                $redirect = $this->Authentication->logout();

                $this->request->getSession()->destroy();

                $this->Flash->success('Account deleted successfully.');

                return $this->redirect($redirect);
            }

            $this->Flash->error('Error deleting account. Please contact the site administrator.');

            return $this->redirect([]);
        }
    }
}
