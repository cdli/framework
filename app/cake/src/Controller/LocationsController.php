<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * @property \App\Controller\Component\ApiComponent $Api
 * @property \App\Controller\Component\ChangesetsComponent $Changesets
 * @property \App\Model\Table\EntitiesUpdatesTable $EntitiesUpdates
 * @property \App\Model\Table\ExternalResourcesTable $ExternalResources
 * @property \App\Model\Table\LocationsTable $Locations
 * @method \App\Model\Entity\Location[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LocationsController extends AppController
{
    /**
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData']]);
        $this->loadComponent('Changesets');
        $this->loadComponent('GranularAccess');
        $this->loadModel('EntitiesUpdates');
        $this->loadModel('ExternalResources');
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'sortableFields' => ['id'],
            'limit' => 1000,
            'maxLength' => 1000,
            'contain' => ['EntitiesNames', 'ExternalResources'],
        ] + $this->paginate;
        $locations = $this->paginate($this->Locations);

        $this->set('locations', $locations);
        $this->set('_serialize', 'locations');
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $location = $this->Locations->get($id, [
            'contain' => [
                'EntitiesNames',
                'ExternalResources',
            ],
        ]);

        $this->set('location', $location);
        $this->set('_serialize', 'location');
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function add()
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error('Only logged in users who have an author associated to their account can edit. Create an account and / or  contact cdli@ames.ox.ac.uk to associate an author with your account.');

            return $this->redirect(['action' => 'index']);
        }

        $location = $this->Locations->newEmptyEntity();
        $this->_handleAddEdit($location);
        $this->render('edit');
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id)
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error('Only logged in users who have an author associated to their account can edit. Create an account and / or  contact cdli@ames.ox.ac.uk to associate an author with your account.');

            return $this->redirect(['action' => 'view', $id]);
        }

        $location = $this->Locations->get($id, ['contain' => ['EntitiesNames', 'ExternalResources']]);
        $this->_handleAddEdit($location);
    }

    /**
     * @param \App\Model\Entity\Location $location
     * @return \Cake\Http\Response|void
     */
    private function _handleAddEdit($location)
    {
        $this->Changesets->handleBlockingChangeset();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $data = $this->getRequest()->getData();
            foreach ($data as &$value) {
                if (is_array($value) && array_key_exists('template', $value)) {
                    unset($value['template']);
                }
            }
            $this->Locations->patchEntity($location, $data);
            $update = $this->EntitiesUpdates->newEntityFromChangedEntity($location);

            $changeset = $this->Changesets->create(['other_entity'], [$update]);
            $this->Changesets->set($changeset);
            $this->Changesets->setBlockingChangeset($changeset);
            $this->redirect(['controller' => 'Changesets', 'action' => 'view', $changeset['id']]);
        }

        $externalResources = $this->ExternalResources->find('list')->order('external_resource')->toArray();

        $this->set('location', $location);
        $this->set('externalResources', $externalResources);
    }
}
