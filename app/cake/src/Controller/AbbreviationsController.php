<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Abbreviations Controller
 *
 * @property \App\Model\Table\AbbreviationsTable $Abbreviations
 * @method \App\Model\Entity\Abbreviation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ApiComponent $Api
 */
class AbbreviationsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);
        $this->loadComponent('Search');

        // Set access for public.
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate['sortableFields'] = ['abbreviation', 'fullform', 'type'];
        $data = $this->request->getQueryParams();

        $query = $this->Abbreviations->find('search', [
            'search' => $this->request->getQueryParams(),
            'contain' => 'Publications',
        ]);
        if (isset($data['letter'])) {
            $query->where([
                'Abbreviations.abbreviation LIKE' => $data['letter'] . '%',
            ]);

            $this->paginate = [
                'limit' => 1000,
                'maxLimit' => 1000,
            ] + $this->paginate;
        }

        $abbreviations = $this->paginate($query);
        $access_granted = $this->RequestAccess->checkUserRoles([1]);
        $this->set(compact('abbreviations', 'access_granted'));
        $this->set('_serialize', 'abbreviations');
    }

    /**
     * View method
     *
     * @param string|null $id Abbreviation id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $abbreviation = $this->Abbreviations->get($id, [
            'contain' => 'Publications',
        ]);

        $this->set(compact('abbreviation'));
        $this->set('_serialize', 'abbreviation');
    }
}
