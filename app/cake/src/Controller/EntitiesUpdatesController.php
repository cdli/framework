<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * @property \App\Model\Table\EntitiesUpdatesTable $EntitiesUpdates
 * @method \App\Model\Entity\EntitiesUpdate[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EntitiesUpdatesController extends AppController
{
    /**
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('GranularAccess');
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null, $relatedId = null)
    {
        $entitiesUpdate = $this->EntitiesUpdates->get($id, [
            'contain' => [
                'UpdateEvents' => ['Creators', 'Reviewers', 'Authors'],
            ],
        ]);

        $this->set('entitiesUpdate', $entitiesUpdate);
        $this->set('_serialize', 'entitiesUpdate');
    }
}
