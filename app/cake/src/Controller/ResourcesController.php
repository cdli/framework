<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

use App\Utility\CdliProcessor\Annotate;
use App\Utility\CdliProcessor\Check;
use App\Utility\CdliProcessor\Convert;
use App\Utility\CdliProcessor\TokenList;
use App\Utility\LinkedData\LinkedData;

/**
 * @property \App\Model\Table\PostingsTable $Postings
 * @property \App\Model\Table\PeriodsTable $Periods
 * @property \App\Controller\Component\ApiComponent $Api
 */
class ResourcesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Postings');
        $this->loadComponent('Api');

        // Set access for public.
        $this->Authentication->allowUnauthenticated([$this->request->getParam('action')]);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function tools()
    {
        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();
            $files = $this->getRequest()->getUploadedFiles();
            if (($files !== null) && ($files['file_to_convert']->getError() !== UPLOAD_ERR_NO_FILE)) {
                $userInput = $files['file_to_convert']->getStream()->getContents();
            } else {
                $userInput = $this->request->getData('text_to_convert');
            }
            $result = null;
            switch ($data['select_tool']) {
                case 0:
                    $Convert = new Convert();
                    $result = $Convert->cAtfToCConllMulti($userInput);
                    break;
                case 4:
                    $Convert = new Convert();
                    $result = $Convert->cConllToConllUMulti(['annotation' => $userInput]);
                    break;
                case 8:
                    $Annotate = new Annotate();
                    $result = $Annotate->morphPre($userInput);
                    break;
                case 12:
                    $Annotate = new Annotate();
                    $result = $Annotate->formatConll(['annotation' => $userInput]);
                    break;
            }
            $this->set('result', $result);
        }
        $tools = [
            0 => 'Convert C-ATF to CDLI-CoNLL',
            //1 => 'Convert C-ATF to Oracc-ATF',
            //2 => 'Convert Oracc-ATF to C-ATF',
            //3 => 'Convert CDLI-Conll to Brat',
            4 => 'Convert CDLI-Conll to CoNLL-U',
            //5 => 'Convert CDLI-Conll to XML-RDF',
            //6 => 'Convert CDLI-Conll to JSON-RDF',
            //7 => 'Convert CDLI-Conll to Turtle (ttl)',
            8 => 'Morphological Pre-annotator',
            //9 => 'Syntactic Annotator',
            //10 => 'Part of Speech Annotator',
            //11 => 'Automated translation'
            12 => 'Format CDLI-CoNLL',
        ];
        $this->set('tools', $tools);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        //getting data of Table posings
        $access_granted = $this->RequestAccess->checkUserRoles([1, 2]);
        // if user is not of role type 1 or 2
        if ($access_granted != 1) {
            $postings = $this->Postings->find('all', [
                'order' => ['title' => 'ASC'],
                'conditions' => ['posting_type_id' => 2 , 'published' => 1],
                'fields' => ['id', 'title'],
            ]);
        // if user is of role of type 1 or 2
        } else {
            $postings = $this->Postings->find('all', [
                'order' => ['title' => 'ASC'],
                'conditions' => ['posting_type_id' => 2],
                'fields' => ['id', 'title'],
            ]);
        }
        $this->set('postings', $postings);
    }

    public function tokenLists($id = null, $kind = null)
    {
        if (!is_null($id) && !is_null($kind)) {
            $requestedType = $this->Api->prefers();
            $type = in_array($requestedType, ['json', 'tsv', 'txt']) ? $requestedType : 'txt';
            $kind = preg_replace('/\.' . $type . '$/', '', $kind);
            $file = TokenList::getTokenListFile($id, $kind, $type);
            if (!is_null($file)) {
                return $this->getResponse()->withFile($file, [
                    'name' => "period_$id-$kind.$type",
                    'download' => true,
                ]);
            }
        }

        $this->loadModel('Periods');
        $periods = $this->Periods->find()->all();
        $tokenLists = [];
        foreach ($periods as $period) {
            $tokenLists[$period->id] = [];

            $wordFile = TokenList::getTokenListFile($period->id, 'words');
            if (file_exists($wordFile)) {
                $tokenLists[$period->id]['words'] = filemtime($wordFile);
            }
            $signFile = TokenList::getTokenListFile($period->id, 'signs');
            if (file_exists($signFile)) {
                $tokenLists[$period->id]['signs'] = filemtime($signFile);
            }
        }
        $this->set('periods', $periods);
        $this->set('tokenLists', $tokenLists);
    }

    public function compareTextToTokenList($kind = null)
    {
        $this->loadModel('Periods');
        $periods = $this->Periods->find()->all()->toArray();
        $this->set('periods', $periods);

        $request = $this->getRequest();
        if ($request->is('post')) {
            $period = $request->getData('period');
            $kind = $request->getData('kind');
            $atf = $request->getData('atf_text');
            $file = $request->getData('atf_file');

            if (!is_null($file) && $file->getError() != UPLOAD_ERR_NO_FILE) {
                $handle = $file->getStream()->detach();
                $atf = stream_get_contents($handle);
            }

            $tokenFile = TokenList::getTokenListFile($period, $kind);

            if (is_null($atf) || is_null($tokenFile)) {
                return;
            }

            $tokens = (array)json_decode(file_get_contents($tokenFile))->list;
            $this->set('output', TokenList::compareText($atf, $kind, $tokens));
        }
    }

    public function checkCdliConll()
    {
        $request = $this->getRequest();
        if ($request->is('post')) {
            $conll = $request->getData('conll_text');
            $file = $request->getData('conll_file');

            if (!is_null($file) && $file->getError() != UPLOAD_ERR_NO_FILE) {
                $handle = $file->getStream()->detach();
                $conll = stream_get_contents($handle);
            }

            if (is_null($conll)) {
                return;
            }

            $this->set('output', Check::checkCdliConll($conll));
        }
    }

    public function mergeConll()
    {
        $request = $this->getRequest();
        if ($request->is('post')) {
            $a = $request->getData('a');
            $b = $request->getData('b');

            $this->set('output', Convert::mergeCConll($a, $b));
        }
    }

    public function cocoConverter()
    {
        $request = $this->getRequest();
        if ($request->is('post')) {
            if (!is_null($request->getData('coco_file'))) {
                $coco_file = $request->getData('coco_file');
                $coco_data = '';
                if ($coco_file->getError() != UPLOAD_ERR_NO_FILE) {
                    $handle = $coco_file->getStream()->detach();
                    $coco_data = stream_get_contents($handle);
                }
                $this->set('output', Convert::cocoToW3c($coco_data));
            } elseif (!is_null($request->getData('via_file'))) {
                $via_file = $request->getData('via_file');
                $via_data = '';
                if ($via_file->getError() != UPLOAD_ERR_NO_FILE) {
                    $handle = $via_file->getStream()->detach();
                    $via_data = stream_get_contents($handle);
                }
                $this->set('output', Convert::viaToW3c($via_data));
            }
        }
    }

    public function sparql()
    {
        $prefixes = LinkedData::getPrefixes();
        $prefixes['rdf'] = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#';
        $prefixes['xsd'] = 'http://www.w3.org/2001/XMLSchema#';

        $this->set('data', $this->getRequest()->getQueryParams());
        $this->set('prefixes', $prefixes);
    }
}
