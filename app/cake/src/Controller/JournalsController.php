<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Journals Controller
 *
 * @property \App\Model\Table\AbbreviationsTable $Abbreviations
 * @property \App\Model\Table\JournalsTable $Journals
 * @method \App\Model\Entity\Journal[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ApiComponent $Api
 */
class JournalsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);
        $this->loadComponent('Search');
        $this->loadModel('Abbreviations');

        // Set access for public.
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'limit' => 1000,
            'maxLimit' => 1000,
            'sortableFields' => ['journal', 'publications_count'],
        ] + $this->paginate;

        $query = $this->Journals->find('search', ['search' => $this->getRequest()->getQueryParams()]);
        $query->select(['publications_count' => $query->func()->count('Publications.id')])
            ->enableAutoFields(true)
            ->contain('Publications')
            ->leftJoinWith('Publications')
            ->group(['Journals.id']);

        $journals = $this->paginate($query)->toArray();

        $this->set('journals', $journals);
        $this->set('access_granted', $this->RequestAccess->checkUserRoles([1]));
        $this->set('_serialize', 'journals');
    }

    /**
     * View method
     *
     * @param string|null $id Journal id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $journal = $this->Journals->get($id, [
            'contain' => ['Publications' => ['Authors', 'Editors', 'EntryTypes', 'Journals', 'ExternalResources']],
        ]);

        $this->set('journal', $journal);
        $this->set('_serialize', 'journal');
    }
}
