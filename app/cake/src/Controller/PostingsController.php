<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

/**
 * Postings Controller
 *
 * @property \App\Model\Table\PostingsTable $Postings
 * @method \App\Model\Entity\Posting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\GranularAccessComponent $GranularAccess
 */
class PostingsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('GranularAccess');
        $this->loadComponent('Search', [
            'actions' => ['news', 'highlights'],
        ]);
        $this->Authentication->allowUnauthenticated(['news', 'highlights', 'view', 'index']);
    }

    /**
     * News method
     *
     * @return \Cake\Http\Response|void
     */
    public function news()
    {
        $query = $this->Postings->find('search', [
            'search' => $this->request->getQueryParams(),
            'contain' => ['Artifacts'],
            'conditions' => ['Postings.published' => 1, 'posting_type_id' => 1],
        ]);

        $newsarticles = $this->paginate($query, ['limit' => 50]);

        foreach ($newsarticles as $posting) {
            if (!is_null($posting->artifact)) {
                $this->GranularAccess->amendArtifactAssets($posting->artifact);
            }
        }

        $this->set('newsarticles', $newsarticles);
    }

    /**
     * Highlights method
     *
     * @return \Cake\Http\Response|void
     */
    public function highlights()
    {
        $query = $this->Postings->find('search', [
            'search' => $this->request->getQueryParams(),
            'contain' => ['Artifacts'],
            'conditions' => ['Postings.published' => 1, 'posting_type_id' => 3],
        ]);

        $highlights = $this->paginate($query, ['limit' => 50]);

        foreach ($highlights as $posting) {
            if (!is_null($posting->artifact)) {
                $this->GranularAccess->amendArtifactAssets($posting->artifact);
            }
        }

        $this->set('highlights', $highlights);
    }

    /**
     * View method
     *
     * @param string|null $id Posting id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $posting = $this->Postings->get($id, [
            'contain' => ['PostingTypes', 'Artifacts.Collections', 'Creators', 'Modifiers'],
        ]);

        if (!is_null($posting->artifact)) {
            $this->GranularAccess->amendArtifactAssets($posting->artifact);
        }

        $this->set('canEdit', $this->GranularAccess->isAdmin());
        $this->set('posting', $posting);
    }
}
