<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

/**
 * Home Controller
 *
 * @method \App\Model\Entity\Artifact[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Model\Table\PostingsTable $Postings
 * @property \App\Model\Table\ArtifactsTable $Artifacts
 * @property \App\Model\Table\UpdateEventsTable $UpdateEvents
 * @property \App\Controller\Component\GranularAccessComponent $GranularAccess
 */
class HomeController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('GranularAccess');
        $this->Authentication->allowUnauthenticated(['index', 'browse', 'hideCookiesNotification']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadModel('Postings');

        $newsarticles = $this->Postings->find('all', [
            'contain' => ['Artifacts'],
            'conditions' => ['Postings.published' => 1, 'posting_type_id' => 1],
            'limit' => 6,
        ]);
        $highlights = $this->Postings->find('all', [
            'contain' => ['Artifacts'],
            'conditions' => ['Postings.published' => 1, 'posting_type_id' => 3],
            'limit' => 6,
        ]);

        $this->loadModel('UpdateEvents');
        $updateEvents = $this->UpdateEvents->find('all', [
            'contain' => [
                'ExternalResources',
                'Reviewers',
                'Creators',
                'Authors',
                'Inscriptions',
                'ArtifactsUpdates',
                'ArtifactAssetAnnotations',
                'EntitiesUpdates',
            ],
            'conditions' => ['UpdateEvents.status' => 'approved'],
            'order' => ['UpdateEvents.approved' => 'DESC'],
            'limit' => 6,
        ]);

        foreach ($newsarticles as $posting) {
            if (!is_null($posting->artifact)) {
                $this->GranularAccess->amendArtifactAssets($posting->artifact);
            }
        }
        foreach ($highlights as $posting) {
            if (!is_null($posting->artifact)) {
                $this->GranularAccess->amendArtifactAssets($posting->artifact);
            }
        }

        $this->set('newsarticles', $newsarticles);
        $this->set('highlights', $highlights);
        $this->set('updateEvents', $updateEvents);
    }

    /**
     * Browse method
     *
     * @return \Cake\Http\Response|void
     */
    public function browse()
    {
        $this->loadModel('Artifacts');
        $this->paginate = [
            'contain' => ['Proveniences', 'Periods', 'ArtifactTypes', 'Archives'],
        ];
        $artifacts = $this->paginate($this->Artifacts);

        $this->set(compact('artifacts'));
        $this->set('_serialize', ['artifacts']);
    }

    public function hideCookiesNotification()
    {
        $this->request->allowMethod('post');

        $this->request->getSession()->write('hideCookiesNotification', true);

        return $this->redirect($this->referer('/'));
    }
}
