<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

use Cake\ORM\TableRegistry;

/**
 * Genres Controller
 *
 * @property \App\Model\Table\GenresTable $Genres
 * @method \App\Model\Entity\Genre[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ApiComponent $Api
 */
class GenresController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);
        $this->loadComponent('Search');

        // Set access for public.
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'limit' => 1000,
            'maxLimit' => 1000,
            'contain' => ['ParentGenres', 'ChildGenres'],
        ] + $this->paginate;

        $query = $this->Genres->find('search', [
            'search' => $this->request->getQueryParams(),
        ]);

        $genres = $this->paginate($query);
        $access_granted = $this->RequestAccess->checkUserRoles([1]);

        $this->set(compact('genres', 'access_granted'));
        $this->set('_serialize', 'genres');
    }

    /**
     * View method
     *
     * @param string|null $id Genre id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $genre = $this->Genres->get($id, [
            'contain' => ['ParentGenres', 'ChildGenres'],
        ]);

        $artifacts = TableRegistry::get('ArtifactsGenres');
        $count = $artifacts->find('list', ['conditions' => ['genre_id' => $id]])->count();
        $access_granted = $this->RequestAccess->checkUserRoles([1]);

        $this->set(compact('genre', 'count', 'access_granted'));
        $this->set('_serialize', 'genre');
    }
}
