<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

use App\Utility\CdliProcessor\Convert;

/**
 * ArtifactAssetAnnotations Controller
 *
 * @property \App\Model\Table\ArtifactAssetAnnotationsTable $ArtifactAssetAnnotations
 * @method \App\Model\Entity\ArtifactAssetAnnotation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ChangesetsComponent $Changesets
 * @property \App\Controller\Component\GranularAccessComponent $GranularAccess
 * @property \Cake\Controller\Component\RequestHandlerComponent $RequestHandler
 */
class ArtifactAssetAnnotationsController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('ArtifactAssetAnnotations');
        $this->loadComponent('Changesets');
        $this->loadComponent('GranularAccess');
        $this->loadComponent('RequestHandler');
        $this->Authentication->allowUnauthenticated(['add', 'view']);
    }

    public function view($id)
    {
        $this->getResponse()->setTypeMap('svg', 'image/svg+xml');

        $annotation = $this->ArtifactAssetAnnotations->get($id, ['contain' => ['ArtifactAssets']]);

        if ($this->RequestHandler->prefers() === 'svg') {
            $svg = \App\Utility\Svg\Annotation::getCutoutSvg($annotation);

            return $this->getResponse()->withType('svg')->withStringBody($svg);
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->Changesets->handleBlockingChangeset();

        if (!$this->request->is(['post', 'put', 'patch'])) {
            $this->set('canSubmitEdits', $this->GranularAccess->canSubmitEdits());

            return;
        }

        $input = null;

        // Handle uploaded files
        $file = $this->getRequest()->getData('anno_file');
        if (!is_null($file) && $file->getError() != UPLOAD_ERR_NO_FILE) {
            $file = $file->getStream()->detach();
            $file = stream_get_contents($file);
            $input = json_decode($file, true);
        }

        if (is_null($input)) {
            $this->set('canSubmitEdits', $this->GranularAccess->canSubmitEdits());

            return;
        }

        $fileType = $this->getRequest()->getData('anno_file_type');
        $annotations = [];
        $assets = [];
        $errors = [];

        try {
            if ($fileType === 'via') {
                $annotations = $this->ArtifactAssetAnnotations->newEntitiesFromVia($input);
            } elseif ($fileType === 'coco') {
                $input = json_decode(Convert::cocoToW3c(json_encode($input)), true);
                $annotations = $this->ArtifactAssetAnnotations->newEntitiesFromJson($input);
            } elseif ($fileType === 'wadm') {
                $annotations = $this->ArtifactAssetAnnotations->newEntitiesFromJson($input);
            } else {
                $errors[] = 'Unsupported file type: "' . h($fileType) . '"';
            }

            foreach ($annotations as $annotation) {
                if ($annotation->has('artifact_asset_id')) {
                    $assets[$annotation->artifact_asset_id][$annotation->annotation_id ?? $annotation->id] = true;
                }
            }
        } catch (\Throwable $error) {
            $errors[] = $error->getMessage();
        }

        if ($this->getRequest()->getData('overwrite')) {
            foreach ($assets as $assetId => $newAnnotations) {
                $activeAnnotations = $this->ArtifactAssetAnnotations->getActiveAnnotations($assetId);
                foreach ($activeAnnotations as $activeAnnotation) {
                    $originalAnnotationId = $activeAnnotation->annotation_id ?? $activeAnnotation->id;
                    if (!array_key_exists($originalAnnotationId, $newAnnotations)) {
                        $annotation = $this->ArtifactAssetAnnotations->newEntity($activeAnnotation->toArray(), [
                            'fields' => ['annotation_id', 'artifact_asset_id', 'license', 'target_selector', 'bodies'],
                            'associated' => ['Bodies' => ['fields' => ['type', 'value', 'source', 'purpose']]],
                        ]);
                        $annotation->is_active = false;
                        $annotation->annotation_id = $originalAnnotationId;
                        $annotation->artifact_asset = $this->ArtifactAssetAnnotations->ArtifactAssets->get(
                            $annotation->artifact_asset_id,
                            ['contain' => 'Artifacts']
                        );
                        $annotations[] = $annotation;
                    }
                }
            }
        }

        $changeset = $this->Changesets->create(['visual_annotation'], $annotations, $errors);
        $this->Changesets->set($changeset);
        $this->Changesets->setBlockingChangeset($changeset);
        $this->redirect(['controller' => 'Changesets', 'action' => 'view', $changeset['id']]);
    }

    /**
     * @param string|null $id Artifact Asset Annotation id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error('Only logged in users who have an author associated to their account can edit text. Create an account and / or  contact cdli@ames.ox.ac.uk to associate an author with your account.');

            return $this->redirect($this->referer());
        } elseif ($this->request->is(['patch', 'post', 'put'])) {
            $annotation = $this->ArtifactAssetAnnotations->get($id, ['contain' => ['ArtifactAssets.Artifacts', 'Bodies']]);
            $annotation = $this->ArtifactAssetAnnotations->patchEntity($annotation, $this->request->getData());
            $annotation->id = null;
            $annotation->setNew(true);

            $changeset = $this->Changesets->create(['visual_annotation'], [$annotation]);
            $this->Changesets->set($changeset);
            $this->Changesets->setBlockingChangeset($changeset);
            $this->redirect(['controller' => 'Changesets', 'action' => 'view', $changeset['id']]);
        } else {
            $this->Changesets->handleBlockingChangeset();
            $annotation = $this->ArtifactAssetAnnotations->get($id, ['contain' => ['Bodies']]);
            if (!$annotation->has('annotation_id')) {
                $annotation->annotation_id = $annotation->id;
            }
            $this->set('annotation', $annotation);
        }
    }
}
