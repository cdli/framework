<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

use App\Datasource\DiffPaginator;
use App\Datasource\NaturalSortPaginator;
use App\Model\Entity\Artifact;
use App\Utility\CdliProcessor\Convert;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\UnauthorizedException;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Ghunti\HighchartsPHP\Highchart;
use Ghunti\HighchartsPHP\HighchartJsExpr;

/**
 * Artifacts Controller
 *
 * @property \App\Model\Table\ArtifactsTable $Artifacts
 * @property \App\Controller\Component\ApiComponent $Api
 * @property \App\Controller\Component\ChangesetsComponent $Changesets
 * @property \App\Controller\Component\GranularAccessComponent $GranularAccess
 * @property \Cake\Controller\Component\PaginatorComponent $Paginator
 * @property \Cake\Controller\Component\RequestHandlerComponent $RequestHandler
 * @method \App\Model\Entity\Artifact[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Model\Table\ArtifactAssetAnnotationsTable $ArtifactAssetAnnotations
 * @property \App\Model\Table\ChemicalDataTable $ChemicalData
 * @property \App\Model\Table\FeaturedSealsTable $FeaturedSeals
 * @property \App\Model\Table\PostingsTable $Postings
 * @property \App\Model\Table\UpdateEventsTable $UpdateEvents
 */
class ArtifactsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Changesets');
        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('GranularAccess');
        $this->loadComponent('Api', ['features' => ['Bibliography', 'Inscription', 'LinkedData', 'TableExport', 'VisualAnnotations']]);

        // Set access for public.
        $this->Authentication->allowUnauthenticated([
            'index',
            'view',
            'resolve',
            'resolveInscription',
            'history',
            'composites',
            'compositesScore',
            'reader',
            'annotations',
            'add',
            'edit',
            'seals',
            'downloadChemistryCSV',
        ]);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if ($this->RequestHandler->prefers() == 'html') {
            return $this->redirect([
                'controller' => 'Search',
                'action' => 'index',
            ]);
        }

        if ($this->Api->requestsFormat(['TableExport'])) {
            $this->paginate = [
                'limit' => 1000,
                'maxLimit' => 1000,
            ] + $this->paginate;
            $options = [
                'contain' => [
                    'ArtifactsUpdates' => [
                        'UpdateEvents',
                        'conditions' => ['UpdateEvents.status' => 'approved'],
                        'sort' => ['UpdateEvents.approved' => 'ASC'],
                    ],
                ],
                'conditions' => ['Artifacts.is_public' => true],
                'order' => ['Artifacts.id' => 'ASC'],
            ];

            if ($this->GranularAccess->canViewPrivateArtifact()) {
                // Remove conditions for privileged users.
                unset($options['conditions']);
            }

            $query = $this->Artifacts->find('all', $options);

            $artifacts = $this->paginate($query)->toArray();
            $artifacts = array_map(function ($artifact) {
                return $this->Artifacts->ArtifactsUpdates->mergeUpdates($artifact->artifacts_updates);
            }, $artifacts);

            $this->set(compact('artifacts'));
            $this->set('_serialize', 'artifacts');
            $this->set('_admin', $this->GranularAccess->isAdmin());

            return;
        } elseif ($this->Api->requestsFormat(['LinkedData'])) {
            $this->paginate = [
                'limit' => 1000,
                'maxLimit' => 1000,
            ] + $this->paginate;

            $query = $this->Artifacts->find('linkedData', [
                'granularAccess' => $this->GranularAccess,
            ])
                ->order(['Artifacts.id']);

            $artifacts = $this->paginate($query)->toArray();

            $this->set('artifacts', $artifacts);
            $this->set('_serialize', 'artifacts');

            return;
        }

        $query = $this->Artifacts->find('full', [
            'granularAccess' => $this->GranularAccess,
        ])
            ->order(['Artifacts.id']);

        $artifacts = $this->paginate($query);

        $this->set(compact('artifacts'));
        $this->set('_serialize', 'artifacts');
        $this->set('_admin', $this->GranularAccess->isAdmin());
    }

    //composites controller

    /**
     *  Composites method
     *
     * @return \Cake\Http\Response|void
     */
    public function composites($compositetype = null)
    {
        $this->paginate = [
            'limit' => 20,
            'contain' => ['Composites', 'Witnesses', 'Witnesses.Inscriptions', 'Genres'],
            'sortableFields' => [
                'composite_no',
                'designation',
            ],
        ];

        $genreIdMap = [
            'Lexical' => 4,
            'Literary' => 5,
            'Royal' => 6,
            'Scientific' => 7,
        ];

        if (!is_null($compositetype)) {
            $artifacts = $this->Artifacts->find('all')
                ->contain(['Composites', 'Witnesses', 'Witnesses.Inscriptions'])
                ->innerJoinWith('Genres', function (\Cake\ORM\Query $q) use ($genreIdMap, $compositetype) {
                    return $q->where(['Genres.id' => $genreIdMap[$compositetype]]);
                })
                ->where([
                    'Artifacts.composite_no !=' => '',
                    'Artifacts.composite_no NOT LIKE' => 'needed',
                ]);

            $this->loadComponent('Paginator');
            $this->Paginator->setPaginator(new NaturalSortPaginator());

            $artifacts = $this->paginate($artifacts);
            $this->set(compact('artifacts', 'compositetype'));
        }
    }

    public function compositesScore($compositenumber = null)
    {
        $this->set('score', $compositenumber);
        $artifactscore = $this->Artifacts->find('all')
            ->contain(['Composites', 'Witnesses', 'Witnesses.Inscriptions'])
            ->where([
                'Artifacts.composite_no =' => $compositenumber,
            ]);
        $show404 = false;
        $text = '';
        $designation = '';
        $artifactId = '';
        $incArr = [];

        foreach ($artifactscore as $artifactscore) {
            $artifactId = $artifactscore->id;
            $designation = $artifactscore->designation;
            $count = count($artifactscore->witnesses);
            for ($i = 1; $i < $count; $i++) {
                if ($artifactscore->witnesses[$i]->composite_no == '' && (!is_null($artifactscore->witnesses[$i]->inscription))) {
                    array_push($incArr, $artifactscore->witnesses[$i]->inscription);
                    $text = $text . $artifactscore->witnesses[$i]->inscription->atf;
                }
            }
        }

        if (count($artifactscore->witnesses) == 0) {
            throw new UnauthorizedException();
        }

        $this->set(compact('text', 'artifactId', 'designation', 'incArr', 'show404'));
    }

    /**
     * Resolve identifiers
     *
     * @param string|null $id Artifact, composite or seal id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function resolve($id)
    {
        if (is_null($id)) {
            $this->setAction('index');
        }

        /** @var ?\Cake\ORM\Entity $artifact */
        $artifact = $this->Artifacts
            ->find('number', ['id' => $id])
            ->select(['id', 'is_public'])
            ->first();

        if (is_null($artifact)) {
            throw new RecordNotFoundException($id . ' not found');
        }

        // Do not redirect private artifacts
        if (
            !$artifact->is_public &&
            !$this->GranularAccess->canViewPrivateArtifact()
        ) {
            throw new UnauthorizedException();
        }

        $this->redirect([
            'action' => 'view',
            '_ext' => $this->getRequest()->getParam('_ext'),
            $artifact->id,
        ]);
    }

    /**
     * Resolve inscription
     *
     * @param string|null $id Artifact, composite or seal id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function resolveInscription($id)
    {
        $inscription = $this->Artifacts->getLatestInscription($id);
        $this->redirect([
            'controller' => 'Inscriptions',
            'action' => 'view',
            '_ext' => $this->getRequest()->getParam('_ext'),
            $inscription,
        ]);
    }

    public function setGraphData($type, $sealChemistry, $elementCount)
    {
        $sealArea = [];
        $dataXAxis = [];
        $headers = [];
        foreach ($sealChemistry as $sealdata) {
            $finalJson = json_decode(json_decode(json_encode($sealdata->chemistry, JSON_HEX_TAG)), true);
            $headers = array_merge($headers, array_keys($finalJson));
        }

        $headers = array_values(array_unique($headers));
        foreach ($sealChemistry as $sealdata) {
            $finalJson = json_decode(json_decode(json_encode($sealdata->chemistry, JSON_HEX_TAG)), true);
            $sealArea[$sealdata->reading]['XAxis'] = [];
            $sealArea[$sealdata->reading]['YAxis'] = [];

            foreach ($headers as $col) {
                array_push($sealArea[$sealdata->reading]['XAxis'], $col);
                if (isset($finalJson[$col])) {
                    array_push($sealArea[$sealdata->reading]['YAxis'], doubleval($finalJson[$col]['value']));
                } else {
                    array_push($sealArea[$sealdata->reading]['YAxis'], 0.0);
                }
            }
        }

        $chart = new Highchart();
        $chart->chart->renderTo = 'graphcontainer';
        $chart->title->text = '';

        $chart->yAxis->min = 0;
        $chart->yAxis->title->text = 'Percent';

        $chart->plotOptions->column->pointPadding = 0.2;
        $chart->plotOptions->column->borderWidth = 0;
        $chart->credits->enabled = false;

        $chart->tooltip->formatter = new HighchartJsExpr("function() {
            return '' + this.x +': '+ this.y +' %';}");

        foreach ($sealArea as $key => $val) {
            if ($elementCount != 'All') {
                $dataXAxis = count($dataXAxis) > count(array_slice($val['XAxis'], 0, $elementCount)) ? $dataXAxis : array_slice($val['XAxis'], 0, $elementCount);
                $chart->series[] = [
                    'type' => $type,
                    'name' => $key,
                    'data' => array_slice($val['YAxis'], 0, $elementCount),
                ];
            } else {
                $dataXAxis = count($dataXAxis) > count($val['XAxis']) ? $dataXAxis : $val['XAxis'];
                $chart->series[] = [
                    'type' => $type,
                    'name' => $key,
                    'data' => $val['YAxis'],
                ];
            }
        }
        $chart->xAxis->categories = $dataXAxis;

        return [$sealArea, $chart, $headers];
    }

    /**
     * View method
     *
     * @param string|null $id Artifact id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $associations = [
            'contain' => [
                'Proveniences',
                'Origins',
                'Periods',
                'ArtifactTypes',
                'Archives',
                'Collections',
                'Dates',
                'Dates.Years',
                'ExternalResources',
                'Genres',
                'Languages',
                'Materials',
                'MaterialAspects',
                'MaterialColors',
                'Publications',
                'Publications.Authors' => [
                    'sort' => ['AuthorsPublications.sequence' => 'ASC'],
                ],
                'Publications.Editors' => [
                    'sort' => ['EditorsPublications.sequence' => 'ASC'],
                ],
                'Publications.EntryTypes',
                'Publications.Journals',
            ],
        ];

        $artifact = $this->Artifacts->get($id, $associations);

        if (!is_null($this->_handleArtifactRetirement($artifact))) {
            return null;
        }

        if (!is_null($this->_handleArtifactAccess($artifact))) {
            return null;
        }

        // Fetching additional info.
        // =======================================================

        // Additional authorization-dependent data is fetched
        $this->GranularAccess->amendArtifact($artifact);
        $this->GranularAccess->amendArtifactAssets($artifact);

        // Artifact updates (for citations)
        $artifact->artifacts_updates = $this->Artifacts->ArtifactsUpdates->find()
            ->contain(['UpdateEvents' => ['Creators', 'Authors']])
            ->where(['UpdateEvents.status' => 'approved', 'artifact_id' => $artifact->id]);

        // Update events (for display)
        $updateEvents = $this->Artifacts->Inscriptions->UpdateEvents
            ->find('artifact', ['artifact' => $artifact->id])
            ->contain([
                'ExternalResources',
                'Creators',
                'Reviewers',
                'Authors',
                'Inscriptions' => function ($q) use ($artifact) {
                    return $q->where(['Inscriptions.artifact_id' => $artifact->id]);
                },
                'ArtifactsUpdates' => function ($q) use ($artifact) {
                    return $q->where(['ArtifactsUpdates.artifact_id' => $artifact->id]);
                },
            ])
            ->all();
        $this->set('updateEvents', $updateEvents);

        // Additional information on witnesses, seals, & impressions
        foreach ($artifact->witnesses as $witness) {
            $this->Artifacts->loadInto($witness, ['Periods', 'Proveniences']);
        }
        foreach ($artifact->seals as $seal) {
            $this->Artifacts->loadInto($seal, ['Periods', 'Proveniences']);
        }
        foreach ($artifact->impressions as $impression) {
            $this->Artifacts->loadInto($impression, ['Periods', 'Proveniences']);
        }

        // Edit access
        $this->set('canSubmitEdits', $this->GranularAccess->canSubmitEdits());

        // Additional annotation information
        if (!empty($artifact->inscription->annotation)) {
            $this->set('conll_u', (new Convert())->cConllToConllU($artifact->inscription));
        }

        // Additional information for retrieving parent materials, genres
        $this->set('artifactAllMaterials', $this->Artifacts->Materials->find()->all()->toArray());
        $this->set('artifactAllGenres', $this->Artifacts->Genres->find()->all()->toArray());

        // Image annotations
        $hasImageAnnotations = false;
        foreach ($artifact->artifact_assets as $asset) {
            $this->Artifacts->ArtifactAssets->loadInto($asset, ['Annotations.UpdateEvents' => function ($q) {
                return $q->where(['UpdateEvents.status' => 'approved']);
            }]);
            if (count($asset->annotations) > 0) {
                $hasImageAnnotations = true;
            }
        }
        $this->set('hasImageAnnotations', $hasImageAnnotations);

        // Chemistry data
        $data = $this->request->getQueryParams();
        $sealChemistryForTable = $this->loadModel('ChemicalData')->find()
            ->where([
                'artifact_id' => $id,
            ]);
        $this->paginate = [
            'limit' => 5,
        ];
        $tableData = $this->paginate($sealChemistryForTable);
        $sealChemistryForGraph = $this->loadModel('ChemicalData')->find()
            ->where([
                'artifact_id' => $id,
            ]);
        $type = 'column';
        $hasgraph = false;
        $elementCount = 'All';
        $sealArea = [];
        $chart = new Highchart();
        $headers = [];

        if (isset($data['type'])) {
            $type = $data['type'];
        }

        if (isset($data['elementCount'])) {
            $elementCount = (int)filter_var($data['elementCount'], FILTER_SANITIZE_NUMBER_INT) != 0 ? (int)filter_var($data['elementCount'], FILTER_SANITIZE_NUMBER_INT) : 'All';
        }

        [$sealArea, $chart, $headers] = $this->setGraphData($type, $sealChemistryForGraph, $elementCount);

        if (count($sealArea) > 0) {
            $hasgraph = true;
        }

        $this->set(compact('chart', 'type', 'elementCount', 'tableData', 'hasgraph', 'headers'));

        $this->set('artifact', $artifact);
        $this->set('_serialize', 'artifact');
        $this->set('_admin', $this->GranularAccess->isAdmin());
    }

    public function reader($artifactId, $assetId)
    {
        $artifact = $this->Artifacts->get($artifactId, ['contain' => [
            'Inscriptions',

            // For description
            'Genres',
            'ArtifactTypes',
            'Origins',
            'Proveniences',
            'Periods',
            'Collections',
        ]]);

        if (!is_null($this->_handleArtifactRetirement($artifact))) {
            return null;
        }

        if (!is_null($this->_handleArtifactAccess($artifact))) {
            return null;
        }

        if (!$artifact->is_atf_public && !$this->GranularAccess->canViewPrivateInscriptions()) {
            $artifact->inscription = null;
        }

        $asset = $this->Artifacts->ArtifactAssets->get($assetId);
        if ($asset->artifact_id != $artifactId) {
            throw new RecordNotFoundException();
        }

        if (!$this->GranularAccess->canViewAsset($asset)) {
            throw new UnauthorizedException();
        }

        $this->set('artifact', $artifact);
        $this->set('asset', $asset);
    }

    public function annotations($artifactId, $assetId)
    {
        $artifact = $this->Artifacts->get($artifactId, ['contain' => [
            // For description
            'Genres',
            'ArtifactTypes',
            'Origins',
            'Proveniences',
            'Periods',
            'Collections',
        ]]);

        if (!is_null($this->_handleArtifactRetirement($artifact))) {
            return null;
        }

        if (!is_null($this->_handleArtifactAccess($artifact))) {
            return null;
        }

        $asset = $this->Artifacts->ArtifactAssets->get($assetId);
        if ($asset->artifact_id != $artifactId) {
            throw new RecordNotFoundException();
        }

        if (!$this->GranularAccess->canViewAsset($asset)) {
            throw new UnauthorizedException();
        }

        $this->set('artifact', $artifact);
        $this->set('asset', $asset);

        // List all revisions
        $this->loadModel('UpdateEvents');
        $revisions = $this->UpdateEvents->find()
            ->distinct('UpdateEvents.id')
            ->where(['UpdateEvents.status' => 'approved'])
            ->contain(['Creators', 'Authors'])
            ->matching('ArtifactAssetAnnotations', function ($q) use ($assetId) {
                return $q->where(['ArtifactAssetAnnotations.artifact_asset_id' => $assetId]);
            })
            ->order(['UpdateEvents.approved' => 'DESC'])
            ->all()
            ->toArray();

        $this->set('revisions', $revisions);

        // Get current revision
        $revision = $this->getRequest()->getQuery('revision');
        if (is_null($revision) && count($revisions)) {
            $revision = $revisions[0]->id;
        } else {
            $revision = (int)$revision;
        }
        $this->set('revision', $revision);

        // Load annotations
        $this->loadModel('ArtifactAssetAnnotations');
        $asset->annotations = $this->ArtifactAssetAnnotations->getActiveAnnotations($assetId, $revision);

        $this->set('annotations', $asset->annotations);
        $this->set('_serialize', 'annotations');
        $this->set('_collectionUrl', Router::url([$artifactId, $assetId, '?' => ['revision' => $revision]], true));

        // Get other info
        $this->set('canSubmitEdits', $this->GranularAccess->canSubmitEdits());
    }

    public function downloadChemistryCSV($id = null)
    {
        /** @psalm-suppress UndefinedInterfaceMethod */
        $sealChemistry = $this->loadModel('ChemicalData')->find()
            ->where([
                'artifact_id' => $id,
            ])->contain(['Calibration']);
        $headers = [];
        foreach ($sealChemistry as $sealdata) {
            $finalJson = json_decode(json_decode(json_encode($sealdata->chemistry, JSON_HEX_TAG)), true);
            $headers = array_merge($headers, array_keys($finalJson));
        }

        $headers = array_values(array_unique($headers));

        $filename = 'artifact_' . $id . '_chemicalData.csv';
        $data = [];
        $tableHeaders = [];
        $tableRows = [];
        $count = 0;
        foreach ($sealChemistry as $sealdata) {
            $finalJson = json_decode(json_decode(json_encode($sealdata->chemistry, JSON_HEX_TAG)), true);
            $row = [];
            foreach ($headers as $key => $col) {
                if ($count == 1) {
                    array_push($tableHeaders, $col);
                    array_push($tableHeaders, '+/-');
                }
                if (isset($finalJson[$col])) {
                    array_push($row, $finalJson[$col]['value']);
                    array_push($row, $finalJson[$col]['+/-']);
                } else {
                    array_push($row, 0.0);
                    array_push($row, 0.0);
                }
            }
            $count = $count + 1;
            array_push($row, $sealdata->reading);
            array_push($row, $sealdata->area);
            array_push($row, $sealdata->area_description);
            array_push($row, $sealdata->artifact_id);
            array_push($row, $sealdata->calibration->human_number);
            array_push($row, $sealdata->date);
            array_push($row, $sealdata->time);
            array_push($row, $sealdata->duration);
            array_push($tableRows, $row);
        }
        array_push($tableHeaders, 'Reading');
        array_push($tableHeaders, 'Area');
        array_push($tableHeaders, 'Area description');
        array_push($tableHeaders, 'Artifact_id');
        array_push($tableHeaders, 'Calibration file name');
        array_push($tableHeaders, 'Date');
        array_push($tableHeaders, 'Time');
        array_push($tableHeaders, 'Duration');
        array_push($data, $tableHeaders);
        $data = array_merge($data, $tableRows);

        $this->setResponse($this->getResponse()->withDownload($filename));
        $this->set(compact('data'));
        $this->viewBuilder()
            ->setClassName('CsvView.Csv')
            ->setOption('serialize', 'data');
    }

    public function deleteChemistry($id = null, $artifactId = null)
    {
        $chemicalData = $this->loadModel('ChemicalData');
        $sealChemistry = $chemicalData->get($id);
        if ($chemicalData->delete($sealChemistry)) {
            $this->Flash->success('Entry has been deleted successfully.');
            $this->redirect(['controller' => 'Artifacts', 'action' => 'view', $artifactId]);
        } else {
            $this->Flash->error('Entry could not be deleted. Please, try again.');
        }
    }

    /**
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function history($id, $type)
    {
        $artifact = $this->Artifacts->get($id);

        if (!is_null($this->_handleArtifactRetirement($artifact))) {
            return null;
        }

        if (!is_null($this->_handleArtifactAccess($artifact))) {
            return null;
        }

        $this->set('artifact', $artifact);
        if ($type === 'atf' || $type === 'annotation') {
            if (!$artifact->is_atf_public && !$this->GranularAccess->canViewPrivateInscriptions()) {
                throw new UnauthorizedException();
            }

            $query = TableRegistry::get('Inscriptions')
                ->find()
                ->where([
                    'Inscriptions.artifact_id' => $id,
                    'UpdateEvents.status' => 'approved',
                    'FIND_IN_SET(:type, UpdateEvents.update_type) > 0',
                ])
                ->bind(':type', $type, 'string')
                ->contain(['UpdateEvents.Creators', 'UpdateEvents.Authors'])
                ->order(['UpdateEvents.approved' => 'DESC']);

            $this->Paginator->setPaginator(new DiffPaginator());
            $inscriptions = $this->paginate($query);

            $this->set('inscriptions', $inscriptions);
            $this->render("history/$type");
        } elseif ($type === 'artifact') {
            $query = $this->Artifacts->ArtifactsUpdates
                ->find()
                ->where([
                    'ArtifactsUpdates.artifact_id' => $id,
                    'UpdateEvents.status' => 'approved',
                ])
                ->contain(['UpdateEvents.Creators', 'UpdateEvents.Authors'])
                ->order(['UpdateEvents.approved' => 'DESC']);

            $updates = $query->all()->toArray();
            $this->GranularAccess->amendArtifactsUpdate($updates);

            $updates = array_map(function ($index, $update) use ($updates) {
                $older = array_slice($updates, $index + 1);
                $older = $this->Artifacts->ArtifactsUpdates->mergeUpdates($older);

                return [$older, $update];
            }, array_keys($updates), array_values($updates));

            $this->set('updates', $updates);
            $this->render('history/artifact');
        } else {
            $this->redirect($this->referer());
        }
    }

    /**
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error('Only logged in users who have an author associated to their account can edit. Create an account and / or  contact cdli@ames.ox.ac.uk to associate an author with your account.');

            return $this->redirect('/');
        }

        $artifact = $this->Artifacts->newEmptyEntity();
        $this->_handleAddEdit($artifact);
        $this->render('edit');
    }

    /**
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function edit($id)
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error('Only logged in users who have an author associated to their account can edit. Create an account and / or  contact cdli@ames.ox.ac.uk to associate an author with your account.');

            return $this->redirect(['action' => 'view', $id]);
        }

        $artifact = $this->Artifacts->get($id, [
            'contain' => [
                'ArtifactTypes',
                'Archives',
                'Collections',
                'Dates',
                'Dates.Years',
                'ArtifactsExternalResources.ExternalResources',
                'Genres',
                'Languages',
                'Materials',
                'MaterialAspects',
                'MaterialColors',
                'Origins',
                'Proveniences',
                'Periods',
                'Publications',
            ],
        ]);

        if (!is_null($this->_handleArtifactAccess($artifact))) {
            return null;
        }

        $this->GranularAccess->amendArtifact($artifact);

        $this->_handleAddEdit($artifact);
    }

    /**
     * @param \App\Model\Entity\Artifact $artifact
     * @return \Cake\Http\Response|null
     */
    private function _handleAddEdit($artifact)
    {
        $originalChangesetId = $this->getRequest()->getQuery('changeset');
        $originalChangeset = $this->Changesets->getRedraftable($originalChangesetId);

        if (!is_null($originalChangeset)) {
            $blockingChangeset = $this->Changesets->getBlockingChangeset();
            if (!is_null($blockingChangeset) && $originalChangeset['id'] !== $blockingChangeset) {
                $this->Changesets->handleBlockingChangeset();
            }

            $originalChangeset['updates'][0]->apply($artifact);
        } else {
            $this->Changesets->handleBlockingChangeset();
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            try {
                $flatData = $this->_getFlatData($artifact);
                $update = $this->Artifacts->ArtifactsUpdates->newEntityFromFlatData($flatData);
                $changeset = $this->Changesets->create(['artifact'], [$update]);
            } catch (\Error $error) {
                $changeset = $this->Changesets->create(['artifact'], [], [$error->getMessage()]);
            }

            if (!is_null($originalChangeset)) {
                $originalChangeset['updates'] = $changeset['updates'];
                $originalChangeset['errors'] = $changeset['errors'];
                $changeset = $originalChangeset;
            }

            $this->Changesets->set($changeset);
            $this->Changesets->setBlockingChangeset($changeset);

            $this->redirect(['controller' => 'Changesets', 'action' => 'view', $changeset['id']]);
        }

        $this->set('artifact', $artifact);
        $this->set('isAdmin', $this->GranularAccess->isAdmin());

        // List related entities
        $this->set([
            'proveniences' => $this->Artifacts->Proveniences->find('list'),
            'periods' => $this->Artifacts->Periods->find('list', ['order' => ['period' => 'ASC']]),
            'artifactTypes' => $this->Artifacts->ArtifactTypes->find('list'),
            'archives' => $this->Artifacts->Archives->find('list'),
            'collections' => $this->Artifacts->Collections->find('list'),
            // 'dates' => $this->Artifacts->Dates->find('list', ['order' => ['period' => 'ASC']]),
            'genres' => $this->Artifacts->Genres->find('list'),
            'languages' => $this->Artifacts->Languages->find('list'),
            'materials' => $this->Artifacts->Materials->find('list'),
            'materialColors' => $this->Artifacts->MaterialColors->find('list'),
            'materialAspects' => $this->Artifacts->MaterialAspects->find('list'),
            'externalResources' => $this->Artifacts->ExternalResources->find('list'),
        ]);
    }

    /**
     * @param \App\Model\Entity\Artifact $artifact
     * @return array
     */
    private function _getFlatData(Artifact $artifact): array
    {
        // Get data from the add or edit form
        $data = $this->getRequest()->getData();

        // Fix form data: remove values from template input elements
        foreach ($data as &$value) {
            if (is_array($value) && array_key_exists('template', $value)) {
                unset($value['template']);
            }
        }

        // Apply changes in form data
        $artifact = $this->Artifacts->patchEntity($artifact, $data);

        // Reload associations to allow for extracting full flat data
        $this->Artifacts->refreshAssociations($artifact, [
            'ArtifactTypes',
            'Archives',
            'Collections',
            'Dates',
            'Dates.Years',
            'ArtifactsExternalResources',
            'Genres',
            'Languages',
            'Materials',
            'Origins',
            'Proveniences',
            'Periods',
            'Publications',
        ]);

        // TODO fix trait, remove special cases
        // Special case for updating material aspects and colors
        if ($artifact->has('materials')) {
            foreach ($artifact->materials as $material) {
                $this->getTableLocator()->get('ArtifactsMaterials')->refreshAssociations($material->_joinData, [
                    'MaterialAspects',
                    'MaterialColors',
                ]);
            }
        }
        // Special case for external resources
        if ($artifact->has('artifacts_external_resources')) {
            foreach ($artifact->artifacts_external_resources as $link) {
                $this->getTableLocator()->get('ArtifactsExternalResources')->refreshAssociations($link, [
                    'ExternalResources',
                ]);
            }
        }

        // Get the flat data
        $flatData = $artifact->getFlatData();

        // Do not allow changes to private fields unless authorized
        if (!$this->GranularAccess->isAdmin()) {
            $flatData = array_diff_key($flatData, Artifact::$privateFlatFields);
        }

        // Replace null with ''
        foreach ($flatData as $key => $value) {
            if (is_null($value)) {
                $flatData[$key] = '';
            }
        }

        return $flatData;
    }

    /**
     * @param \App\Model\Entity\Artifact $artifact
     * @return \Cake\Http\Response|null
     */
    private function _handleArtifactRetirement(Artifact $artifact): ?\Cake\Http\Response
    {
        // Check for artifact retirement
        if ($artifact->retired == 1) {
            $retiredArtifact = $artifact->getCDLINumber();
            if ($this->GranularAccess->isAdmin()) {
                $retiredArtifact .= ' (<a href="' . Router::url(['action' => 'edit', $artifact->id]) . '">edit</a>)';
            }
            $retiredPhrase = __('The artifact {0} has been retired.', $retiredArtifact);
            if ($artifact->has('retired_comments') && $artifact->retired_comments !== '') {
                $retiredPhrase .= ' Reason: "' . $artifact->retired_comments . '"';
            }
            if (empty($artifact->redirect_artifact_id)) {
                $this->Flash->default(__('You have been redirected to the home page. {0}', $retiredPhrase), ['escape' => false]);

                return $this->redirect(['controller' => 'Home', 'action' => 'index']);
            } else {
                $redirectNumber = 'P' . str_pad($artifact->redirect_artifact_id, 6, '0', STR_PAD_LEFT);
                $this->Flash->default(__('You have been redirected to {0}. {1}', $redirectNumber, $retiredPhrase), ['escape' => false]);

                return $this->redirect(['controller' => 'Artifacts', 'action' => 'view', $artifact->redirect_artifact_id]);
            }
        }

        return null;
    }

    /**
     * @param \App\Model\Entity\Artifact $artifact
     * @return \Cake\Http\Response|null
     */
    private function _handleArtifactAccess(Artifact $artifact): ?\Cake\Http\Response
    {
        // Check if artifact is private or public
        if (!$artifact->is_public && !$this->GranularAccess->canViewPrivateArtifact()) {
            throw new UnauthorizedException();
        }

        return null;
    }

    /**
     * Admin-only.
     *
     * @param mixed $id
     */
    public function images($id)
    {
        $artifact = $this->Artifacts->get($id, ['contain' => 'Collections']);
        $this->GranularAccess->amendArtifactAssets($artifact);
        $this->set('artifact', $artifact);
        $this->set('isAdmin', $this->GranularAccess->isAdmin());
    }

    //seal portal
    public function seals()
    {
        $this->paginate = [
            'sortableFields' => [
                'Seals',
                'Periods.sequence',
            ],
            'contain' => 'Periods',
            'limit' => 10,
        ];

        /** @psalm-suppress UndefinedInterfaceMethod */
        $featuredSeals = $this->loadModel('FeaturedSeals')->find('all')->contain(['Artifacts']);
        foreach ($featuredSeals as $seal) {
            $this->GranularAccess->amendArtifactAssets($seal->artifact);
        }

        $isadmin = $this->GranularAccess->isAdmin();

        $posting = $this->loadModel('Postings')->find()
            ->where([
                'Postings.id' => 8,
            ])->all();

        $query = $this->Artifacts->find();
        $sealPeriods = $query
            ->contain([
                'ArtifactTypes',
                'Periods' => [
                    'fields' => [
                        'Periods.period',
                        'Periods.sequence',
                    ],
                ],
            ])
            ->select([
                'Seals' => $query->func()->count('Artifacts.period_id'),
                'Artifacts.seal_no',
                'Artifacts.period_id',
                'Artifacts.artifact_type_id',
                'ArtifactTypes.artifact_type',
            ])
            ->where([
                'Artifacts.period_id IS NOT' => null,
                'Artifacts.is_public' => 1,
                'OR' => [['Artifacts.artifact_type_id' => 12], ['ArtifactTypes.parent_id' => 12]],
                'period IS NOT' => null,
            ])
            ->group('Artifacts.period_id');
        $sealPeriods = $this->paginate($sealPeriods);

        $this->set(compact('featuredSeals', 'sealPeriods', 'posting', 'isadmin'));
    }
}
