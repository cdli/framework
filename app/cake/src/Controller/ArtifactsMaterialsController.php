<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\UnauthorizedException;

/**
 * @property \App\Model\Table\ArtifactsMaterialsTable $ArtifactsMaterials
 * @method \App\Model\Entity\ArtifactsExternalResource[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ApiComponent $Api
 * @property \App\Controller\Component\GranularAccessComponent $GranularAccess
 */
class ArtifactsMaterialsController extends AppController
{
    /**
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData']]);
        $this->loadComponent('GranularAccess');
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if (!$this->Api->requestsFormat(['LinkedData'])) {
            $this->redirect(['controller' => 'Search', 'action' => 'index']);
        }

        $this->paginate = [
            'limit' => 1000,
            'maxLimit' => 1000,
        ] + $this->paginate;

        $artifactsMaterials = $this->ArtifactsMaterials->find();

        if (!$this->GranularAccess->canViewPrivateArtifact()) {
            $artifactsMaterials = $artifactsMaterials
                ->matching('Artifacts', function ($q) {
                    return $q->where(['is_public' => true]);
                });
        }

        $artifactsMaterials = $this->paginate($artifactsMaterials);

        $this->set('artifactsMaterials', $artifactsMaterials);
        $this->set('_serialize', 'artifactsMaterials');
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException
     */
    public function view($id = null)
    {
        $artifactsMaterial = $this->ArtifactsMaterials->get($id);

        if (!$this->GranularAccess->canViewPrivateArtifact() && !$artifactsMaterial->artifact->is_public) {
            throw new UnauthorizedException();
        }

        if (!$this->Api->requestsFormat(['LinkedData'])) {
            $this->redirect(['controller' => 'Artifacts', 'action' => 'view', $artifactsMaterial->artifact_id]);
        }

        $this->set('artifactsMaterial', $artifactsMaterial);
        $this->set('_serialize', 'artifactsMaterial');
    }
}
