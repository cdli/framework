<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

use App\Datasource\ElasticSearchPaginator;
use App\Datasource\ElasticSearchQuery;
use App\Model\Entity\Artifact;
use App\Utility\CdliProcessor\TokenList;

/**
 * Search Controller
 *
 * @property \App\Model\Table\ArtifactsTable $Artifacts
 * @property \App\Controller\Component\ApiComponent $Api
 * @property \App\Controller\Component\GranularAccessComponent $GranularAccess
 * @property \Cake\Controller\Component\PaginatorComponent $Paginator
 */
class SearchController extends AppController
{
    /**
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['Bibliography', 'Inscription', 'LinkedData', 'TableExport']]);
        $this->loadComponent('GranularAccess');
        $this->loadComponent('Paginator');
        $this->loadModel('Artifacts');

        $this->Authentication->allowUnauthenticated(['index', 'view', 'resolve', 'tokenList']);

        if (is_null($this->readSearchSettings())) {
            $this->writeSearchSettings(self::$_defaultSearchSettings);
        }
    }

    /**
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        if ($this->Api->requestsApi()) {
            $this->Paginator->setPaginator(new ElasticSearchPaginator());
        }

        // Load search settings
        $searchSettings = $this->readSearchSettings();
        $this->set('searchSettings', $searchSettings);

        // Determine default layout
        $layout = $this->_getSearchLayout();
        $this->set('layout', $layout);

        // Prepare query data
        $queryData = array_filter(array_intersect_key($this->getRequest()->getQueryParams(), ElasticSearchQuery::$searchFields));
        $queryType = array_key_exists('simple-value', $queryData) || count($queryData) === 0 ? 'simple' : 'advanced';
        $orderData = $layout === 'full' ? [$searchSettings['full:sort']] : [
            $searchSettings['compact:sort_1'],
            $searchSettings['compact:sort_2'],
            $searchSettings['compact:sort_3'],
        ];
        $orderData = array_intersect($this->getRequest()->getQuery('order') ?? $orderData, array_keys(ElasticSearchQuery::$sortFields));
        $orderData[] = 'id';
        $filterData = array_intersect_key($this->getRequest()->getQuery('f') ?? [], ElasticSearchQuery::$filters);
        $access = [
            'artifact' => $this->GranularAccess->canViewPrivateArtifact(),
            'atf' => $this->GranularAccess->canViewPrivateInscriptions(),
            'asset' => $this->GranularAccess->canViewPrivateImage(),
        ];

        // Perform query
        $query = (new ElasticSearchQuery('artifacts', $access))
            ->find($queryType, ['query' => $queryData])
            ->where($filterData)
            ->order($orderData, true)
            ->aggregate(ElasticSearchQuery::$filters);

        // Paginate results
        $this->paginate = ['limit' => $searchSettings[$layout . ':page_size'], 'maxLimit' => 10000];
        $results = $this->paginate($query);
        if ($this->Api->requestsApi()) {
            return $this->_setApiVariables($results);
        }
        $this->set('results', $results->toArray());
        $this->set('filterBuckets', $results->_buckets);

        // Add collection info for copyright statements
        // TODO put info in search index or something
        $artifactsById = [];
        if ($results->count() > 0) {
            $artifacts = $this->Artifacts->find()
                ->where(['id IN' => array_column($results->toArray(), '_id')])
                ->contain('Collections')
                ->all();
            foreach ($artifacts as $artifact) {
                $artifactsById[$artifact->id] = $artifact;
            }
        }
        $this->set('artifactInfo', $artifactsById);

        // Pass additional info to the view
        $this->set('query', [
            'data' => $queryData,
            'type' => $queryType,
            'order' => $orderData,
            'filters' => $filterData,
            'highlight' => $query->atfQueries,
        ]);
        $this->set('sortFields', ElasticSearchQuery::$sortFields);
        $this->set('filters', ElasticSearchQuery::$filters);
        $this->set('metrics', $results->_metrics);
        $this->set('access', $access);
        $this->set('canSubmitEdits', $this->GranularAccess->canSubmitEdits());
        $this->set('canSeeIndexTime', $this->RequestAccess->checkUserRoles([1, 2]));
    }

    /**
     * @param string $path
     * @return \Cake\Http\Response|null
     */
    public function view($path)
    {
        if ($path === 'settings') {
            return $this->searchSettings();
        } elseif ($path === 'advanced') {
            $this->set('query', $this->getRequest()->getQueryParams());
            $this->render('advanced');
        } elseif ($path === 'commodity-viz') {
            return $this->redirectCommodityViz();
        } elseif ($path === 'custom-export') {
            return $this->customExport();
        } elseif ($path === 'redirect-ucla' || $path === 'search_results.php') {
            // redirect-ucla: internal URL generated by nginx from requests to UCLA
            // search_results.php: UCLA URL mistakenly used on the new domain
            return $this->redirectUcla();
        } elseif ($path === 'archival_view.php') {
            // UCLA URL mistakenly used on the new domain
            $id = $this->getRequest()->getQuery('ObjectID');
            $this->redirect(['controller' => 'Artifacts', 'action' => 'view', ltrim(substr($id, 1), '0')]);
        } elseif (is_numeric($path)) {
            // Old search URL, occasionally still found
            $this->redirect(['action' => 'index', '?' => $this->getRequest()->getQueryParams()]);
        }
    }

    /**
     * @return \Cake\Http\Response|null
     */
    protected function searchSettings()
    {
        $request = $this->getRequest();
        if ($request->is(['post', 'patch', 'put'])) {
            $data = $request->getData();
            if (array_key_exists('reset', $data)) {
                $this->writeSearchSettings(self::$_defaultSearchSettings);
                $this->Flash->success('Search settings reset successfully!');
            } else {
                $this->writeSearchSettings($data);
                $this->Flash->success('Search settings updated successfully!');
            }
        }
        $settings = $this->readSearchSettings();
        $this->set('settings', $settings);
        $this->set('sortFields', ElasticSearchQuery::$sortFields);
        $this->render('settings');
    }

    /**
     * @return \Cake\Http\Response|null
     */
    protected function customExport()
    {
        $request = $this->getRequest();
        $searchSettings = $this->readSearchSettings();

        if ($request->is(['post', 'patch', 'put'])) {
            $data = $request->getData();

            // Determine and set output format (CSV or TSV)
            $format = $data['format'] ?? 'csv';
            if (!in_array($format, ['csv', 'tsv'])) {
                $format = 'csv';
            }

            // Determine output fields and their order
            if (array_key_exists('columns', $data) && is_array($data['columns'])) {
                $fields = array_filter($data['columns'], function ($field) {
                    return in_array($field, self::$_customExportColumns);
                });
            } else {
                $fields = array_values(self::$_customExportColumns);
            }

            // Get CSV header
            $header = [];
            foreach (self::$_customExportColumns as $flatField => $field) {
                if (in_array($field, $fields)) {
                    $header[] = $flatField;
                }
            }
            if (!$this->GranularAccess->isAdmin()) {
                $header = array_diff($header, array_keys(Artifact::$privateFlatFields));
            }
            $this->set('_header', $header);

            /**
             * TODO psalm needs this here for some reason (but not in SearchController::index())
             * @var \Cake\ORM\Query $query
             */
            $query = $this->_getSearchQuery()->select('id');

            // TODO paginate through all results?
            $layout = $this->_getSearchLayout();
            $this->paginate = ['limit' => $searchSettings[$layout . ':page_size'], 'maxLimit' => 10000];
            $results = $this->paginate($query);

            // TODO optimize: depending on which headers are requested
            $this->_setApiVariables($results);

            return;
        }

        $this->set('settings', $searchSettings);
        $this->render('custom_export');
    }

    /**
     * @return \Cake\Http\Response|null
     */
    protected function redirectUcla()
    {
        $old = $this->getRequest()->getQueryParams();
        $new = [];
        foreach ($old as $key => $value) {
            if ($value === '') {
                unset($old[$key]);
            } elseif (array_key_exists($key, self::$_uclaQueryMapping)) {
                [$newKey, $newValue] = self::$_uclaQueryMapping[$key];
                if (is_array($newValue)) {
                    $newValue = array_key_exists($value, $newValue) ? $newValue[$value] : null;
                } elseif (is_null($newValue)) {
                    $newValue = $value;
                }
                $new[$newKey] = $newValue;
            }
        }

        if (array_key_exists('PrimaryPublication', $old) && array_key_exists('SecondaryPublication', $old)) {
            $new['publication_designation'] = $old['PrimaryPublication'] . ' %AND% ' . $old['SecondaryPublication'];
        } elseif (array_key_exists('PrimaryPublication', $old)) {
            $new['publication_designation'] = $old['PrimaryPublication'];
            $new['publication_type'] = 'primary';
        } elseif (array_key_exists('SecondaryPublication', $old)) {
            $new['publication_designation'] = $old['SecondaryPublication'];
            $new['publication_type'] = 'history';
        }

        $genres = [];
        if (array_key_exists('Genre', $old)) {
            $genres[] = $old['Genre'];
        }
        if (array_key_exists('SubGenre', $old)) {
            $genres[] = $old['SubGenre'];
        }
        $genres = implode(' %AND% ', $genres);
        if ($genres !== '') {
            $new['genre'] = $genres;
        }

        $contributors = [];
        if (array_key_exists('ATFSource', $old)) {
            $contributors[] = $old['ATFSource'];
        }
        if (array_key_exists('CatalogueSource', $old)) {
            $contributors[] = $old['CatalogueSource'];
        }
        if (array_key_exists('TranslationSource', $old)) {
            $contributors[] = $old['TranslationSource'];
        }
        $contributors = implode(' %AND% ', $contributors);
        if ($contributors !== '') {
            $new['update_authors'] = $contributors;
        }

        $this->redirect(['action' => 'index', '?' => $new]);
    }

    /**
     * @return \Cake\Http\Response|null
     */
    protected function redirectCommodityViz()
    {
        // Perform query
        $query = $this->_getSearchQuery(false);
        $query->limit(1000);
        $results = $query->all();

        if ($results->_metrics['count']['relation'] !== 'gte') {
            $url = '/cdli-accounting-viz/?' . $this->_encodeIdSequence($results->toArray());
            if (strlen($url) <= 2048) {
                return $this->redirect($url);
            }
        }

        $this->Flash->error(__('Too many results to show.'));
        $this->render('empty');
    }

    /**
     * @param string $id
     * @return \Cake\Http\Response|null
     */
    public function resolve($id)
    {
        $this->redirect(['action' => 'index', '?' => ['cdli_id' => $id] + $this->getRequest()->getQueryParams()]);
    }

    /**
     * @param string $kind
     * @return \Cake\Http\Response|null
     */
    public function tokenList($kind)
    {
        if ($kind !== 'signs' && $kind !== 'words') {
            throw new \Cake\Http\Exception\NotFoundException();
        }

        // Perform query
        $query = $this->_getSearchQuery(false);
        $query->limit(1000);
        $results = $query->all();
        $artifactIds = array_column(array_column($results->toArray(), '_source'), 'id');

        // Compose token list
        $type = $this->Api->prefers() === 'json' ? 'json' : 'txt';

        $inscription_count = 0;
        $token_count = 0;
        $token_instance_count = 0;

        $list = [];
        foreach ($artifactIds as $artifactId) {
            $inscription_count += 1;

            $inscription = $this->Artifacts->Inscriptions->find()
                ->where(['Inscriptions.artifact_id' => $artifactId, 'Inscriptions.is_latest' => true])
                ->select(['atf']);
            if (!$this->GranularAccess->canViewPrivateInscriptions()) {
                $inscription = $inscription->matching('Artifacts', function ($query) {
                    return $query->where(['Artifacts.is_atf_public' => true]);
                });
            }
            /** @var ?\Cake\ORM\Entity $inscription */
            $inscription = $inscription->first();

            if (empty($inscription) || empty($inscription->atf)) {
                continue;
            }

            $tokens = TokenList::processText($inscription->atf, $kind);

            foreach ($tokens as $token) {
                if (array_key_exists($token, $list)) {
                    $list[$token] += 1;
                } else {
                    $token_count += 1;
                    $list[$token] = 1;
                }
                $token_instance_count += 1;
            }
        }
        ksort($list);

        $data = null;
        if ($type === 'json') {
            $data = json_encode([
                'metadata' => [
                    'token_kind' => $kind,
                    'inscription_count' => $inscription_count,
                    'token_count' => $token_count,
                    'token_instance_count' => $token_instance_count,
                ],
                'list' => $list,
            ]);
        } else {
            $data = implode("\n", array_keys($list));
        }

        $this->Api->respondAs($type);

        return $this->getResponse()->withStringBody($data);
    }

    protected static array $_uclaQueryMapping = [
        'SearchMode' => ['layout', ['Line' => 'compact', 'Text' => 'full']],
        'Author' => ['publication_authors', null],
        'PublicationDate' => ['publication_year', null],
        'Collection' => ['collection', null],
        'AccessionNumber' => ['accession_no', null],
        'MuseumNumber' => ['museum_no', null],
        'Provenience' => ['provenience', null],
        'ExcavationNumber' => ['excavation_no', null],
        'Period' => ['period', null],
        'DatesReferenced' => ['dates_referenced', null],
        'ObjectType' => ['artifact_type', null],
        'ObjectRemarks' => ['artifact_comments', null],
        'Material' => ['material', null],
        'TextSearch' => ['atf_transliteration', null],
        'singleLine' => ['atf_transliteration_mode', ['true' => 'line', '' => '']],
        'caseSensitive' => ['atf_transliteration_case_sensitive', ['true' => 1, '' => '']],
        'TranslationSearch' => ['atf_translation', null],
        'CommentSearch' => ['atf_comments', null],
        'StructureSearch' => ['atf_structure', null],
        'Language' => ['language', null],
        'CompositeNumber' => ['all_composite_no', null],
        'SealID' => ['all_seal_no', null],
        'ObjectID' => ['id', null],
        'order' => ['order[0]', [
            'PrimaryPublication' => 'designation',
            'Author' => '',
            'PublicationDate' => '',
            'SecondaryPublication' => '',
            'Collection' => 'collection',
            'AccessionNumber' => 'accession_no',
            'MuseumNumber' => 'museum_no',
            'Provenience' => 'provenience',
            'ExcavationNumber' => 'excavation_no',
            'Period' => 'period_sequence',
            'DatesReferenced' => 'dates_referenced',
            'ObjectType' => 'artifact_type',
            'Genre' => 'genre',
            'SubGenre' => 'genre',
            'CompositeNumber' => '',
            'SealID' => '',
            'ObjectID' => 'id',
        ]],
    ];

    /**
     * @return array|null
     */
    protected function readSearchSettings()
    {
        $settings = $this->getRequest()->getSession()->read('searchSettings2');

        if (is_null($settings)) {
            return null;
        }

        if (!array_key_exists('version', $settings) || $settings['version'] !== self::$_defaultSearchSettings['version']) {
            $settings = array_merge(self::$_defaultSearchSettings, $settings);
            $settings['version'] = self::$_defaultSearchSettings['version'];
            $this->writeSearchSettings($settings);
        }

        return $settings;
    }

    /**
     * @param array $settings
     */
    protected function writeSearchSettings(array $settings)
    {
        $this->getRequest()->getSession()->write('searchSettings2', $settings);
    }

    /**
     * @return string
     */
    protected function _getSearchLayout(): string
    {
        $searchSettings = $this->readSearchSettings();
        $layout = $this->getRequest()->getQuery('layout');
        if (!is_string($layout) || !in_array($layout, ['full', 'compact'])) {
            /** @var string $layout */
            $layout = $searchSettings['layout_type'];
        }

        return $layout;
    }

    /**
     * @param bool $order
     * @return \App\Datasource\ElasticSearchQuery
     */
    protected function _getSearchQuery(bool $order = true)
    {
        if ($order) {
            $searchSettings = $this->readSearchSettings();
            $layout = $this->_getSearchLayout();

            $orderData = $layout === 'full' ? [$searchSettings['full:sort']] : [
                $searchSettings['compact:sort_1'],
                $searchSettings['compact:sort_2'],
                $searchSettings['compact:sort_3'],
            ];
            $orderData = array_intersect($this->getRequest()->getQuery('order') ?? $orderData, array_keys(ElasticSearchQuery::$sortFields));
        }

        // Prepare query data
        $queryData = array_filter(array_intersect_key($this->getRequest()->getQueryParams(), ElasticSearchQuery::$searchFields));
        $queryType = array_key_exists('simple-value', $queryData) || count($queryData) === 0 ? 'simple' : 'advanced';
        $filterData = array_intersect_key($this->getRequest()->getQuery('f') ?? [], ElasticSearchQuery::$filters);
        $orderData[] = 'id';
        $access = [
            'artifact' => $this->GranularAccess->canViewPrivateArtifact(),
            'atf' => $this->GranularAccess->canViewPrivateInscriptions(),
            'asset' => $this->GranularAccess->canViewPrivateImage(),
        ];

        // Create query
        $query = (new ElasticSearchQuery('artifacts', $access))
            ->find($queryType, ['query' => $queryData])
            ->where($filterData)
            ->order($orderData, true)
            ->aggregate(ElasticSearchQuery::$filters);

        return $query;
    }

    /**
     * Update the `version` field when adding keys to this array to let
     * the controller automatically update user settings.
     */
    protected static array $_defaultSearchSettings = [
        'version' => 2,

        'layout_type' => 'full',

        'full:page_size' => 25,
        'full:sidebar' => true,
        'full:sort' => '_score',
        'full:field:publication' => true,
        'full:field:collection' => true,
        'full:field:museum_no' => true,
        'full:field:provenience' => true,
        'full:field:period' => true,
        'full:field:artifact_type' => true,
        'full:field:material' => true,
        'full:field:dates_referenced' => true,
        'full:field:atf' => true,

        'compact:page_size' => 25,
        'compact:sort_1' => 'id',
        'compact:sort_2' => '',
        'compact:sort_3' => '',
        'compact:sidebar' => false,
        'compact:field:designation' => true,
        'compact:field:publication' => false,
        'compact:field:collection' => false,
        'compact:field:period' => true,
        'compact:field:provenience' => true,
        'compact:field:artifact_type' => false,
        'compact:field:material' => false,
        'compact:field:accession_no' => false,
        'compact:field:museum_no' => true,
        'compact:field:written_in' => false,
        'compact:field:excavation_no' => false,
        'compact:field:archive' => false,
        'compact:field:dates_referenced' => true,
        'compact:field:genre' => true,
        'compact:field:language' => false,

        'filter:period' => true,
        'filter:provenience' => true,
        'filter:artifact_type' => true,
        'filter:material' => true,
        'filter:genre' => true,
        'filter:language' => true,
        'filter:dates_referenced' => true,
        'filter:collection' => true,
        'filter:publication_authors' => true,
        'filter:publication_year' => true,
        'filter:publication_journal' => false,
        'filter:publication_editors' => false,
        'filter:publication_series' => false,
        'filter:publication_publisher' => false,
        'filter:update_external_resource' => true,
        'filter:asset_type' => true,
        'filter:chemical_data' => true,
        'filter:asset_annotations' => true,
        'filter:external_resource_external_resource' => true,
        'filter:publication' => true,
        'filter:atf_transliteration' => true,
        'filter:annotation' => true,
        'filter:atf_transcription' => true,
        'filter:atf_translation' => true,
        'filter:atf_translation_language' => true,
    ];

    /**
     * Maps columns from Artifact flat to Custom Export fields.
     */
    protected static array $_customExportColumns = [
        'artifact_id' => 'id',
        'museum_no' => 'museum_no',
        'designation' => 'designation',
        'publications_key' => 'publication',
        'publications_type' => 'publication',
        'publications_exact_ref' => 'publication',
        'publications_comment' => 'publication',
        'provenience' => 'provenience',
        'period' => 'period',
        'dates' => 'dates_referenced',
        'collections' => 'collection',
        'genres' => 'genre',
        'materials' => 'material',
        'provenience_comments' => 'provenience',
        'is_provenience_uncertain' => 'provenience',
        'period_comments' => 'period',
        'is_period_uncertain' => 'period',
        'genres_comments' => 'genre',
        'is_genres_uncertain' => 'genre',
        'materials_aspect' => 'material',
        'materials_color' => 'material',
        'materials_uncertain' => 'material',
        'artifact_type' => 'artifact_type',
        'artifact_type_comments' => 'artifact_type',
        'is_artifact_type_uncertain' => 'artifact_type',
        'languages' => 'language',
        'is_languages_uncertain' => 'language',
        'accession_no' => 'accession_no',
        'excavation_no' => 'excavation_no',
        'written_in' => 'written_in',
        'archive' => 'archive',
    ];

    /**
     * @param \App\Datasource\ElasticSearchResultSet $results
     * @return \Cake\Http\Response|null
     */
    protected function _setApiVariables($results)
    {
        $ids = array_column(array_column($results->toArray(), '_source'), 'id');
        $aspect = $this->getRequest()->getQuery('aspect');

        if (count($ids) === 0) {
            $entities = [];
        } elseif ($aspect == 'inscriptions') {
            $entities = [];

            if ($this->Api->requestsFormat(['LinkedData'])) {
                $contain = [
                    'Artifacts' => [
                        'Composites' => $this->GranularAccess->canViewPrivateInscriptions() ? [] : [
                            'conditions' => ['Composites.is_public' => true],
                        ],
                        // 'Dates',
                        'Genres',
                        'Languages',
                    ],
                ];
            } else {
                $contain = ['Artifacts'];
            }

            foreach ($ids as $id) {
                $inscription = $this->Artifacts->Inscriptions
                    ->findByArtifactId($id)
                    ->contain($contain)
                    ->where(['is_latest' => 1])
                    ->first();
                if ($inscription) {
                    $entities[] = $inscription;
                }
            }
        } elseif ($aspect == 'publications') {
            $entities = $this->Artifacts->Publications
                ->find()
                ->contain(['EntryTypes', 'Authors', 'Editors', 'Journals'])
                ->matching('Artifacts', function ($q) use ($ids) {
                    return $q->whereInList('Artifacts.id', $ids);
                })
                ->distinct('Publications.id')
                ->all()
                ->toArray();
        } else {
            $entities = $this->Artifacts->find('full', [
                    'granularAccess' => $this->GranularAccess,
                ])
                ->where(['Artifacts.id IN' => $ids])
                ->order(function ($exp, $q) use ($ids) {
                    return $q->func()->field(['Artifacts.id' => 'identifier', ...$ids]);
                })
                ->toArray();
        }

        $this->set([
            'entities' => $entities,
            '_admin' => $this->GranularAccess->isAdmin(),
            '_serialize' => 'entities',
        ]);
    }

    /**
     * @param array $results
     * @return string
     */
    protected function _encodeIdSequence($results)
    {
        if (count($results) === 0) {
            return '';
        }

        $map = [
            ',' => 'A', '0' => 'B', '1' => 'C', '2' => 'D', '3' => 'E',
            '4' => 'F', '5' => 'G', '6' => 'H', '7' => 'I', '8' => 'J',
            '9' => 'K', '1,1,' => 'L', '0,' => 'M', '1,' => 'N', '2,' => 'O',
            '3,' => 'P', '4,' => 'Q', '5,' => 'R', '6,' => 'S', '7,' => 'T',
            '8,' => 'U', '9,' => 'V', ',0' => 'W', ',1' => 'X', ',2' => 'Y',
            ',3' => 'Z', ',4' => 'a', ',5' => 'b', ',6' => 'c', ',7' => 'd',
            ',8' => 'e', ',9' => 'f', '00' => 'g', '11' => 'h', '22' => 'i',
            '33' => 'j', '44' => 'k', '55' => 'l', '66' => 'm', '77' => 'n',
            '88' => 'o', '99' => 'p', '0,1' => 'q', '1,1' => 'r', '2,1' => 's',
            '3,1' => 't', '4,1' => 'u', '5,1' => 'v', '6,1' => 'w', '7,1' => 'x',
            '8,1' => 'y', '9,1' => 'z', ',0,' => '0', ',1,' => '1', ',2,' => '2',
            ',3,' => '3', ',4,' => '4', ',5,' => '5', ',6,' => '6', ',7,' => '7',
            ',8,' => '8', ',9,' => '9',
        ];

        // Compute the difference between successive
        // ids, because these differences will be much
        // smaller than the ids themselves:
        $deltas = [$results[0]['_source']['id']];
        $count = count($results);
        for ($i = 1; $i < $count; $i++) {
            $deltas[] = $results[$i]['_source']['id'] - $results[$i - 1]['_source']['id'];
        }
        $delta_string = implode(',', $deltas);

        // Length of the longest string which
        // can be encoded by a single character:
        // For our encoding this is '1,1,'
        $MAX_ENCODING_LENGTH = 4;

        // Iterate over the string. Every iteration,
        // greedily convert the string's prefix into
        // an encoded letter.
        $encoding = '';
        $i = 0;
        $keys = array_map('strval', array_keys($map));
        $strlen = strlen($delta_string);
        while ($i < $strlen) {
            for ($length = $MAX_ENCODING_LENGTH; $length > 0; $length--) {
                $key = substr($delta_string, $i, $length);
                if (in_array($key, $keys, true)) {
                    $encoding .= $map[$key];
                    $i += $length;
                    break;
                }
            }
        }

        return $encoding;
    }
}
