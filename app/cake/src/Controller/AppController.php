<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\EventInterface;
use Cake\Http\Exception\RedirectException;
use Cake\I18n\Time;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 * @property \App\Controller\Component\RestPaginatorComponent $RestPaginator
 * @property \Authentication\Controller\Component\AuthenticationComponent $Authentication
 * @property \App\Controller\Component\GranularAccessComponent $GranularAccess
 * @property \App\Controller\Component\RequestAccessComponent $RequestAccess
 */
class AppController extends Controller
{
    public $paginate = [
        'limit' => 100,
    ];

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('RestPaginator');
        $this->loadComponent('Flash');
        $this->loadComponent('Authentication.Authentication', [
            'logoutRedirect' => [
                'prefix' => false,
                'plugin' => null,
                'controller' => 'Users',
                'action' => 'login',
            ],
        ]);
        // Must be loaded after the Authentication component
        $this->loadComponent('RequestAccess');
        $this->loadComponent('ServerTiming');

        $identity = $this->Authentication->getIdentity();

        if ($identity && !$identity->active) {
            $this->Flash->error(sprintf('This account has been deactivated. If you think this account should be active, please contact us at <a href="%s">cdli-support@ames.ox.ac.uk</a>.', h('mailto:cdli-support@ames.ox.ac.uk')), ['escape' => false]);
            $this->request->getSession()->delete('session_verified');

            throw new RedirectException($this->Authentication->logout());
        }

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
    }

    /**
     * beforeRender callback.
     *
     * @param \Cake\Event\EventInterface $event Event.
     * @return \Cake\Http\Response|null|void
     */
    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);

        $session = $this->getRequest()->getSession();
        $session_user = $session->read('user');

        // Check if user session details exists.
        if (!is_null($session_user)) {
            // Check if present Controller is TwoFactor
            if ($this instanceof TwofactorController) {
                // Check if session 'user' created is not more than 2 minutes.
                $session_user_created = strtotime($session_user['2fa_session_created']);
                $time_now = strtotime((string)Time::now());
                $time_diff = $time_now - $session_user_created;

                // If time differece is more then 2 minutes, delete the session 'user'
                if ($time_diff > 120) {
                    $session->delete('user');
                    $this->Flash->error(__('Please login or register again, two-step set-up and verification should be resolved in under 120 seconds.'));
                    throw new RedirectException($this->Authentication->logout());
                }
            } else {
                if ($session_user['type'] === 'login') {
                    $this->Flash->error(__('Login failed. Please complete the two-factor authentication to log in.'));
                } else {
                    $this->Flash->error(__('Registration incomplete. To register, please fill the register form again and complete the two-factor authentication setup.'));
                }
                $session->delete('user');
            }
        }

        $session_forgot = $session->read('forgotPassword');

        // Check if forgot_password session exists
        if (!is_null($session_forgot)) {
            if (Time::now()->toUnixString() > $session_forgot['expired_at']) {
                $session->delete('forgotPassword');
            } else {
                if ($this->request->getParam('controller') !== 'Forgot' && $session_forgot['2fa'] == 1) {
                    $session->write('forgotPassword.2fa', false);
                    $this->Flash->error('To set up new password, you will have to again verify 2FA.');
                }
            }
        }
    }

    public function beforeRender(EventInterface $event)
    {
        parent::beforeRender($event);

        $hideCookiesNotification = $this->request->getCookie('hideCookiesNotification')
            || $this->request->getSession()->read('hideCookiesNotification');

        $this->set(compact('hideCookiesNotification'));
    }
}
