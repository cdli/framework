<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

/**
 * Regions Controller
 *
 * @property \App\Model\Table\RegionsTable $Regions
 * @method \App\Model\Entity\Region[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ApiComponent $Api
 */
class RegionsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);

        // Set access for public.
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $regions = $this->paginate($this->Regions, [
            'sortableFields' => ['region'],
            'contain' => ['Proveniences'],
        ]);
        $access_granted = $this->RequestAccess->checkUserRoles([1]);

        $this->set(compact('regions', 'access_granted'));
        $this->set('_serialize', 'regions');
    }

    /**
     * View method
     *
     * @param string|null $id Region id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $region = $this->Regions->get($id, [
            'contain' => ['Proveniences'],
        ]);
        $access_granted = $this->RequestAccess->checkUserRoles([1]);

        $proveniencesList = [];
        $proveniencesId = [];
        foreach ($region->proveniences as $provenience) {
            $proveniencesList[] = $provenience->provenience;
            $proveniencesId[] = $provenience->id;
        }
        $proveniences_id = array_combine($proveniencesList, $proveniencesId);
        ksort($proveniences_id);
        $this->set(compact('access_granted', 'proveniences_id'));
        $this->set('region', $region);
        $this->set('_serialize', 'region');
    }
}
