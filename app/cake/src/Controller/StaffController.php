<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

/**
 * Staff Controller
 *
 * @property \App\Model\Table\StaffTable $Staff
 * @method \App\Model\Entity\Staff[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Model\Table\StaffTypesTable $StaffTypes
 * @property \App\Model\Table\AuthorsTable $Authors
 */
class StaffController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('StaffTypes');
        // Set access for public.
        $this->Authentication->allowUnauthenticated(['index', 'view', 'image']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $staff = $this->Staff
            ->find('all', ['contain' => ['Authors']])
            ->order([
                'CASE WHEN sequence = \'\' THEN 2 ELSE 1 END',
                'sequence' => 'ASC',
                'Authors.author' => 'ASC',
            ]);
        $staff_types = $this->StaffTypes->find('all');
        $access_granted = $this->RequestAccess->checkUserRoles([1]);
        $this->set(compact('staff_types', 'staff', 'access_granted'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Staff id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit()
    {
        $identity = $this->Authentication->getIdentity();
        $auth_id = $identity['author_id'];

        //to check if the author id exist if not show an error to the user
        if (!$auth_id) {
            $this->Flash->error(__('You are not authorized to access that page'));

            return $this->redirect($this->referer());
        }

        $this->loadModel('Authors');
        $author = $this->Authors
            ->find()
            ->where(['id' => $auth_id])
            ->first();

        $this->loadModel('Staff');
        $staff = $this->Staff
            ->find()
            ->where(['author_id' => $auth_id])
            ->first();

        if ($staff == null) {
            $this->Flash->error(__('You are not authorized to access that page'));

            return $this->redirect($this->referer());
        }

        $id = $staff->id;

        $staff = $this->Staff->get($id, [
            'contain' => [],
        ]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $staff = $this->Staff->patchEntity($staff, $this->getRequest()->getData());
            $image = $this->request->getUploadedFile('image_file');
            if ($image !== null && $image->getError() !== \UPLOAD_ERR_NO_FILE) {
                $name = $image->getClientFilename();
                $extension = substr(strrchr($name, '.'), 1);
                $id = h($staff->id);
                $newname = $id . '.' . $extension;

                $ImagePath = WWW_ROOT . 'files-up/images/staff-img/' . $newname;
                $files = glob(WWW_ROOT . 'files-up/images/staff-img/' . $id . '.*');
                if (count($files) > 0) {
                    unlink($files[0]);
                }
                $image->moveTo($ImagePath);
            }
            if ($this->Staff->save($staff)) {
                $this->Flash->success(__('The staff details has been saved.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'profile']);
            }
            $this->Flash->error(__('The staff could not be saved. Please, try again.'));
        }
        $authors = $this->Staff->Authors->find('list');
        if (glob(WWW_ROOT . 'files-up/images/staff-img/' . $id . '.*')) {
            $file = glob(WWW_ROOT . 'files-up/images/staff-img/' . $id . '.*');
            $extension = substr(strrchr($file[0], '.'), 1);
            $id = h($staff->id);
        } else {
            $id = 'default';
            $extension = 'png';
        }
        $staffTypes = $this->Staff->StaffTypes->find('list');
        $this->set(compact('staff', 'author', 'staffTypes', 'id', 'extension'));
    }
}
