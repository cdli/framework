<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\NotAcceptableException;
use Cake\Http\Exception\UnauthorizedException;

/**
 * Inscriptions Controller
 *
 * @property \App\Model\Table\InscriptionsTable $Inscriptions
 * @method \App\Model\Entity\Inscription[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Model\Table\ArtifactsTable $Artifacts
 * @property \App\Model\Table\PeriodsTable $Periods
 * @property \App\Model\Table\ProveniencesTable $Proveniences
 * @property \App\Controller\Component\ApiComponent $Api
 * @property \App\Controller\Component\ChangesetsComponent $Changesets
 * @property \App\Controller\Component\GranularAccessComponent $GranularAccess
 */
class InscriptionsController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Artifacts');
        $this->loadModel('Periods');
        $this->loadModel('Proveniences');
        $this->loadComponent('Api', ['features' => ['Inscription', 'LinkedData']]);
        $this->loadComponent('Changesets');
        $this->loadComponent('GranularAccess');
        $this->Authentication->allowUnauthenticated(['index', 'view', 'add', 'edit']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [
                'Artifacts' => [
                    'Composites' => [
                        'conditions' => ['Composites.is_public' => true],
                    ],
                    // 'Dates',
                    'Genres',
                    'Languages',
                ],
            ],
            'conditions' => [
                'Artifacts.is_public' => true,
                'Artifacts.is_atf_public' => true,
                'Inscriptions.is_latest' => true,
            ],
        ];

        if ($this->GranularAccess->canViewPrivateInscriptions()) {
            unset($this->paginate['conditions']['Artifacts.is_public']);
            unset($this->paginate['conditions']['Artifacts.is_atf_public']);
            unset($this->paginate['contain']['Artifacts']['Composites']['conditions']);
        }

        if ($this->Api->requestsFormat(['LinkedData'])) {
            $this->paginate['limit'] = 500;
            $this->paginate['maxLimit'] = 500;
        }

        $inscriptions = $this->paginate($this->Inscriptions);
        $access_granted = $this->RequestAccess->checkUserRoles([1]);
        $this->set(compact('inscriptions', 'access_granted'));
        $this->set('_serialize', 'inscriptions');
    }

    /**
     * View method
     *
     * @param string|null $id Inscription id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if ($id === 'recent') {
            return $this->recent();
        }

        $compositeConditions = [];
        if ($this->GranularAccess->canViewPrivateInscriptions()) {
            $compositeConditions = [
                'conditions' => ['Composites.is_public' => true],
            ];
        }

        $inscription = $this->Inscriptions->get($id, [
            'contain' => [
                'Artifacts' => [
                    'Composites' => $compositeConditions,
                    // 'Dates',
                    'Genres',
                    'Languages',
                ],
                'UpdateEvents',
                'UpdateEvents.Creators',
                'UpdateEvents.Authors',
            ],
        ]);

        $artifact = $inscription->artifact;

        if (
            !$this->GranularAccess->canViewPrivateInscriptions() &&
            (!$artifact->is_public || !$artifact->is_atf_public)
        ) {
            throw new UnauthorizedException();
        }

        if (!$this->GranularAccess->canAccessEdits($inscription->update_event)) {
            throw new UnauthorizedException();
        }

        $type = $inscription->getPreferredType($this->Api->accepts());
        if (empty($type)) {
            throw new NotAcceptableException();
        }

        if ($type == 'html') {
            $oldInscription = null;
            if ($inscription->update_event->status == 'approved') {
                $oldInscription = $this->Inscriptions->find()
                    ->contain('UpdateEvents')
                    ->where([
                        'Inscriptions.artifact_id' => $inscription->artifact_id,
                        'UpdateEvents.approved <' => $inscription->update_event->approved,
                    ])
                    ->order(['UpdateEvents.approved' => 'DESC'])
                    ->first();
            } else {
                $oldInscription = $this->Inscriptions->find()
                    ->where(['artifact_id' => $inscription->artifact_id, 'is_latest' => 1])
                    ->first();
            }
            $this->set('oldInscription', $oldInscription);
        } else {
            $this->Api->renderAs($this, $type);
        }

        $this->set('inscription', $inscription);
        $this->set('_serialize', 'inscription');
    }

    /**
     * @return \Cake\Http\Response|null
     */
    public function recent()
    {
        $query = $this->Inscriptions->find()
            ->contain(['UpdateEvents' => ['Creators', 'Authors'], 'Artifacts' => ['Periods', 'Proveniences']])
            ->where(['UpdateEvents.status' => 'approved', 'FIND_IN_SET(\'atf\', UpdateEvents.update_type) > 0'])
            ->order(['UpdateEvents.approved' => 'DESC']);

        if (!$this->GranularAccess->canViewPrivateArtifact()) {
            $query = $query->where(['Artifacts.is_public' => true]);
        }

        if (!$this->GranularAccess->canViewPrivateInscriptions()) {
            $query = $query->where(['Artifacts.is_atf_public' => true]);
        }

        $queryParams = $this->getRequest()->getQueryParams();
        if (array_key_exists('period_id', $queryParams) && $queryParams['period_id'] !== '') {
            $query = $query->where(['Artifacts.period_id' => $queryParams['period_id']]);
        }
        if (array_key_exists('provenience_id', $queryParams) && $queryParams['provenience_id'] !== '') {
            $query = $query->where(['Artifacts.provenience_id' => $queryParams['provenience_id']]);
        }
        if (array_key_exists('dates_referenced', $queryParams) && $queryParams['dates_referenced'] !== '') {
            $query = $query->where(['Artifacts.dates_referenced LIKE' => '%' . $queryParams['dates_referenced'] . '%']);
        }

        $inscriptions = $this->paginate($query);
        $periods = $this->Periods->find('list')->order('sequence');
        $proveniences = $this->Proveniences->find('list')->order('provenience');

        $this->set('inscriptions', $inscriptions);
        $this->set('queryParams', $queryParams);
        $this->set('periods', $periods);
        $this->set('proveniences', $proveniences);

        $this->render('recent');
    }

    private function _getInput(string $prefix): null|string
    {
        $file = $this->request->getData("${prefix}_file");
        $paste = $this->request->getData("${prefix}_paste");
        $input = null;

        // Choose input method
        if (!is_null($file) && $file->getError() != UPLOAD_ERR_NO_FILE) {
            $handle = $file->getStream()->detach();
            $input = stream_get_contents($handle);
        } else {
            $input = $paste;
        }

        return is_null($input) ? $input : preg_replace('/\r\n?/', "\n", $input);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null
     */
    public function add()
    {
        $originalChangesetId = $this->getRequest()->getQuery('changeset');
        $originalChangeset = $this->Changesets->getRedraftable($originalChangesetId);

        if (!$this->request->is(['post', 'put', 'patch'])) {
            if (!is_null($originalChangeset)) {
                $blockingChangeset = $this->Changesets->getBlockingChangeset();
                if (!is_null($blockingChangeset) && $originalChangeset['id'] !== $blockingChangeset) {
                    $this->Changesets->handleBlockingChangeset();
                }

                if ($originalChangeset['update_type'] === ['atf']) {
                    $input = implode("\n\n", array_column($originalChangeset['updates'], 'atf'));
                } else {
                    $input = implode("\n", array_column($originalChangeset['updates'], 'annotation'));
                }

                $this->set('input', $input);
            } else {
                $this->Changesets->handleBlockingChangeset();

                if (!is_null($this->request->getQuery('artifact'))) {
                    $this->setAction('edit');
                }
            }

            $this->set('tab', $this->getRequest()->getQuery('tab') ?? 'atf');
            $this->set('canSubmitEdits', $this->GranularAccess->canSubmitEdits());

            return;
        }

        $input = $this->_getInput('atf');
        $inputField = 'atf';

        $conll = $this->_getInput('conll');
        if (!empty($conll)) {
            $input = $conll;
            $inputField = 'annotation';
        }

        if (empty($input)) {
            // Empty input (distinct from missing input) does nothing
            $this->set('canSubmitEdits', $this->GranularAccess->canSubmitEdits());

            return;
        }

        if ($inputField === 'atf') {
            $records = preg_split('/(?<=\n)(?=&)/', $input);
            $records = array_map(function ($record) {
                return preg_replace('/\n+$/', "\n\n", $record);
            }, $records);
        } elseif ($inputField === 'annotation') {
            $records = preg_split('/(?<=\n)(?=#new_text=)/', $input);
        }

        $updates = [];
        $errors = [];

        foreach ($records as $record) {
            try {
                $inscription = $this->Inscriptions->newEntityFromInput($record, $inputField);

                // Check whether user has access to inscription
                if ($inscription->has('artifact_id')) {
                    $artifact = $this->Artifacts->get($inscription->artifact_id, ['contain' => ['Inscriptions']]);

                    if (!$artifact->is_public && !$this->GranularAccess->canViewPrivateArtifact()) {
                        throw new RecordNotFoundException((string)$inscription->artifact_id);
                    }

                    if (!$artifact->is_atf_public && !$this->GranularAccess->canViewPrivateInscriptions()) {
                        $inscription->setError('atf', __('Cannot add or change the transliteration/annotation. Please contact an administrator.'));
                    }

                    if (!empty($artifact->inscription) && !$inscription->isFieldDifferent($artifact->inscription, $inputField)) {
                        continue;
                    }

                    $artifact = null;
                }

                // Cannot add/change annotations if there is no atf
                if ($inputField === 'annotation' && !$inscription->has('atf')) {
                    $inscription->setError('atf', __('Not transliteration, cannot add or change the annotation.'));
                }
                $updates[] = $inscription;
            } catch (RecordNotFoundException $error) {
                $inscription = $this->Inscriptions->newEmptyEntity();
                $inscription->setError('artifact_id', __('Record {0} not found in table "artifacts"', $error->getMessage()));
                $updates[] = $inscription;
            } catch (\Throwable $error) {
                $errors[] = $error->getMessage();
            }
        }

        $changeset = $this->Changesets->create([$inputField], $updates, $errors);

        if (!is_null($originalChangeset)) {
            $originalChangeset['updates'] = $changeset['updates'];
            $originalChangeset['errors'] = $changeset['errors'];
            $changeset = $originalChangeset;
        }

        $this->Changesets->set($changeset);
        $this->Changesets->setBlockingChangeset($changeset);
        $this->redirect(['controller' => 'Changesets', 'action' => 'view', $changeset['id']]);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null
     */
    public function edit($id = null)
    {
        // Do not display the form if the user cannot submit edits anyway (there
        // are other ways to check validity of ATF so no need to make this public).
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error('Only logged in users who have an author associated to their account can edit text. Create an account and / or  contact cdli@ames.ox.ac.uk to associate an author with your account.');

            return $this->redirect($this->referer());
        }

        $this->Changesets->handleBlockingChangeset();

        $inscription = is_null($id) ? $this->Inscriptions->newEmptyEntity() : $this->Inscriptions->get($id);
        $this->set('inscription', $inscription);

        // Get artifact ID from
        //   1. Existing inscription, or, if that does not exist
        //   2. Query parameter (i.e. ?artifact=123)
        $artifactId = $inscription->artifact_id ?? $this->getRequest()->getQuery('artifact');

        // If still no artifact is given, return
        if (is_null($artifactId)) {
            return;
        }

        // Else, get artifact info to check for access
        $artifact = $this->Inscriptions->Artifacts->get($artifactId, ['contain' => 'Collections']);

        // If the visitor is not authorized to view artifact metadata, deny access
        if (!$artifact->is_public && !$this->GranularAccess->canViewPrivateArtifact()) {
            throw new UnauthorizedException();
        }

        // If the artifact has private atf, redirect
        if (!$artifact->is_atf_public && !$this->GranularAccess->canViewPrivateInscriptions()) {
            $this->Flash->error(__('Cannot add text for this artifact.'));
            $this->redirect(['controller' => 'Artifacts', 'action' => 'view', $artifactId]);
        }

        // Else, provide artifact metadata and if authorized, images
        $this->set('artifact', $artifact);
        $this->GranularAccess->amendArtifactAssets($artifact);

        // Check which tab to display
        $this->set('tab', $this->getRequest()->getQuery('tab') ?? 'atf');

        // More info for the template
        $this->set('isAdmin', $this->GranularAccess->isAdmin());
    }
}
