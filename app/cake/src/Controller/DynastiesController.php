<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

/**
 * Dynasties Controller
 *
 * @property \App\Model\Table\DynastiesTable $Dynasties
 * @method \App\Model\Entity\Dynasty[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ApiComponent $Api
 */
class DynastiesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);
        $this->loadComponent('Search');

        // Set access for public.
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'sortableFields' => ['polity', 'dynasty', 'Proveniences.provenience'],
            'contain' => ['Proveniences', 'Dates', 'Rulers'],
        ] + $this->paginate;

        $query = $this->Dynasties->find('search', [
            'search' => $this->request->getQueryParams(),
        ]);

        $dynasties = $this->paginate($query);
        $access_granted = $this->RequestAccess->checkUserRoles([1]);

        $this->set(compact('dynasties', 'access_granted'));
        $this->set('_serialize', 'dynasties');
    }

    /**
     * View method
     *
     * @param string|null $id Dynasty id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dynasty = $this->Dynasties->get($id, [
            'contain' => ['Proveniences', 'Dates', 'Rulers'],
        ]);

        $access_granted = $this->RequestAccess->checkUserRoles([1]);

        $this->set(compact('access_granted'));
        $this->set('dynasty', $dynasty);
        $this->set('_serialize', 'dynasty');
    }
}
