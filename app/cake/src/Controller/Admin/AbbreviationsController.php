<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Abbreviations Controller
 *
 * @property \App\Model\Table\AbbreviationsTable $Abbreviations
 * @method \App\Model\Entity\Abbreviation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AbbreviationsController extends AppController
{
    /**
     * View method
     *
     * @param string|null $id Abbreviation id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $abbreviation = $this->Abbreviations->get($id, [
            'contain' => ['Publications'],
        ]);

        $this->set('abbreviation', $abbreviation);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $abbreviation = $this->Abbreviations->newEmptyEntity();
        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();

            if ($data['bibtexkey']) {
                $publication = $this->Abbreviations->Publications
                    ->find()
                    ->where(['bibtexkey' => trim($data['bibtexkey'])])
                    ->first();
                if ($publication) {
                    $data['publication_id'] = $publication->id;
                }
                unset($data['bibtexkey']);
            } else {
                $data['publication_id'] = null;
            }

            $abbreviation = $this->Abbreviations->patchEntity($abbreviation, $data);

            if ($this->Abbreviations->save($abbreviation)) {
                $this->Flash->success(__('The abbreviation has been saved.'));

                return $this->redirect(['prefix' => false, 'action' => 'index']);
            }
            $this->Flash->error(__('The abbreviation could not be saved. Please, try again.'));
        }
        $this->set(compact('abbreviation'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Abbreviation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $abbreviation = $this->Abbreviations->get($id, [
            'contain' => ['Publications'],
        ]);
        $bibtexkey = $abbreviation->publication->bibtexkey ?? null;

        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $data = $this->getRequest()->getData();

            if ($data['bibtexkey']) {
                $bibtexkey = $data['bibtexkey'];
                $publication = $this->Abbreviations->Publications
                    ->find()
                    ->where(['bibtexkey' => trim($bibtexkey)])
                    ->first();
                if ($publication) {
                    $data['publication_id'] = $publication->id;
                }
                unset($data['bibtexkey']);
            } else {
                $data['publication_id'] = null;
            }

            $abbreviation = $this->Abbreviations->patchEntity($abbreviation, $data);

            if ($this->Abbreviations->save($abbreviation)) {
                $this->Flash->success(__('The abbreviation has been saved.'));

                return $this->redirect(['prefix' => false, 'action' => 'index']);
            }
            $this->Flash->error(__('The abbreviation could not be saved. Please, try again.'));
        }

        $this->set(compact('abbreviation', 'bibtexkey'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Abbreviation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->getRequest()->allowMethod(['post', 'delete']);
        $abbreviation = $this->Abbreviations->get($id);
        if ($this->Abbreviations->delete($abbreviation)) {
            $this->Flash->success(__('The abbreviation has been deleted.'));
        } else {
            $this->Flash->error(__('The abbreviation could not be deleted. Please, try again.'));
        }

        return $this->redirect(['prefix' => false, 'action' => 'index']);
    }
}
