<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Http\Exception\ForbiddenException;

/**
 * Staff Controller
 *
 * @property \App\Model\Table\StaffTable $Staff
 * @method \App\Model\Entity\Staff[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StaffController extends AppController
{
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $staff = $this->Staff->newEmptyEntity();
        if ($this->getRequest()->is('post')) {
            $staff = $this->Staff->patchEntity($staff, $this->getRequest()->getData());
            if ($this->Staff->save($staff)) {
                $id = $staff->id;
                $image = $this->request->getUploadedFile('image_file');
                if ($image !== null && $image->getError() !== \UPLOAD_ERR_NO_FILE) {
                    $name = $image->getClientFilename();
                    $extension = substr(strrchr($name, '.'), 1);
                    $newname = $id . '.' . $extension;

                    $ImagePath = WWW_ROOT . 'files-up/images/staff-img/' . $newname;

                    try {
                        $image->moveTo($ImagePath);
                    } catch (\Exception $e) {
                        echo 'Image is not uploaded.';
                    }
                }
                $this->Flash->success(__('The staff has been saved.'));

                return $this->redirect(['prefix' => false, 'action' => 'index']);
            }
            $this->Flash->error(__('The staff could not be saved. Please, try again.'));
        }
        $authors = $this->Staff->Authors->find('list');
        $staffTypes = $this->Staff->StaffTypes->find('list', ['limit' => 200]);
        $this->set(compact('staff', 'authors', 'staffTypes'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Staff id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $staff = $this->Staff->get($id, [
            'contain' => [],
        ]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $staff = $this->Staff->patchEntity($staff, $this->getRequest()->getData());
            $image = $this->request->getUploadedFile('image_file');
            if ($image !== null && $image->getError() !== \UPLOAD_ERR_NO_FILE) {
                $name = $image->getClientFilename();
                $extension = substr(strrchr($name, '.'), 1);
                $id = h($staff->id);
                $newname = $id . '.' . $extension;
                if (1) {
                    $ImagePath = WWW_ROOT . 'files-up/images/staff-img/' . $newname;
                    $files = glob(WWW_ROOT . 'files-up/images/staff-img/' . $id . '.*');
                    if (count($files) > 0) {
                        unlink($files[0]);
                    }
                    $image->moveTo($ImagePath);
                } else {
                    throw new ForbiddenException(__('Only images files are allowed.'));
                }
            }
            if ($this->Staff->save($staff)) {
                $this->Flash->success(__('The staff has been saved.'));

                return $this->redirect(['prefix' => false, 'action' => 'index']);
            }
            $this->Flash->error(__('The staff could not be saved. Please, try again.'));
        }
        $authors = $this->Staff->Authors->find('list');
        if (glob(WWW_ROOT . 'files-up/images/staff-img/' . $id . '.*')) {
            $file = glob(WWW_ROOT . 'files-up/images/staff-img/' . $id . '.*');
            $extension = substr(strrchr($file[0], '.'), 1);
            $id = h($staff->id);
        } else {
            $id = 'default';
            $extension = 'png';
        }
        $staffTypes = $this->Staff->StaffTypes->find('list');
        $this->set(compact('staff', 'authors', 'staffTypes', 'id', 'extension'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Staff id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->getRequest()->allowMethod(['post', 'delete']);
        $staff = $this->Staff->get($id);
        if (glob(WWW_ROOT . 'files-up/images/staff-img/' . $id . '.*')) {
            $file = glob(WWW_ROOT . 'files-up/images/staff-img/' . $id . '.*');
            if ($file[0]) {
                unlink($file[0]);
            }
        }
        if ($this->Staff->delete($staff)) {
            $this->Flash->success(__('The staff has been deleted.'));
        } else {
            $this->Flash->error(__('The staff could not be deleted. Please, try again.'));
        }

        return $this->redirect(['prefix' => false, 'action' => 'index']);
    }
}
