<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ExternalResources Controller
 *
 * @property \App\Model\Table\ExternalResourcesTable $ExternalResources
 * @method \App\Model\Entity\ExternalResource[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ExternalResourcesController extends AppController
{
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $externalResource = $this->ExternalResources->newEmptyEntity();
        if ($this->getRequest()->is('post')) {
            $externalResource = $this->ExternalResources->patchEntity($externalResource, $this->getRequest()->getData());
            if ($this->ExternalResources->save($externalResource)) {
                $this->Flash->success(__('The external resource has been saved.'));

                return $this->redirect(['prefix' => false, 'action' => 'index']);
            }
            $this->Flash->error(__('The external resource could not be saved. Please, try again.'));
        }
        $this->set(compact('externalResource'));
    }

    /**
     * Edit method
     *
     * @param string|null $id External Resource id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $externalResource = $this->ExternalResources->get($id, [
            'contain' => [],
        ]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $externalResource = $this->ExternalResources->patchEntity($externalResource, $this->getRequest()->getData());
            if ($this->ExternalResources->save($externalResource)) {
                $this->Flash->success(__('The external resource has been saved.'));

                return $this->redirect(['prefix' => false, 'action' => 'index']);
            }
            $this->Flash->error(__('The external resource could not be saved. Please, try again.'));
        }
        $artifacts = $this->ExternalResources->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('externalResource', 'artifacts'));
    }

    /**
     * Delete method
     *
     * @param string|null $id External Resource id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->getRequest()->allowMethod(['post', 'delete']);
        $externalResource = $this->ExternalResources->get($id);
        if ($this->ExternalResources->delete($externalResource)) {
            $this->Flash->success(__('The external resource has been deleted.'));
        } else {
            $this->Flash->error(__('The external resource could not be deleted. Please, try again.'));
        }

        return $this->redirect(['prefix' => false, 'action' => 'index']);
    }
}
