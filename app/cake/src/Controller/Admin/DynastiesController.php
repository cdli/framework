<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Dynasties Controller
 *
 * @property \App\Model\Table\DynastiesTable $Dynasties
 * @method \App\Model\Entity\Dynasty[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DynastiesController extends AppController
{
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dynasty = $this->Dynasties->newEmptyEntity();
        if ($this->getRequest()->is('post')) {
            $dynasty = $this->Dynasties->patchEntity($dynasty, $this->getRequest()->getData());
            if ($this->Dynasties->save($dynasty)) {
                $this->Flash->success(__('The dynasty has been saved.'));

                return $this->redirect(['prefix' => false, 'action' => 'index']);
            }
            $this->Flash->error(__('The dynasty could not be saved. Please, try again.'));
        }
        $proveniences = $this->Dynasties->Proveniences->find('list', ['limit' => 200]);
        $this->set(compact('dynasty', 'proveniences'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Dynasty id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dynasty = $this->Dynasties->get($id, [
            'contain' => [],
        ]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $dynasty = $this->Dynasties->patchEntity($dynasty, $this->getRequest()->getData());
            if ($this->Dynasties->save($dynasty)) {
                $this->Flash->success(__('The dynasty has been saved.'));

                return $this->redirect(['prefix' => false, 'action' => 'index']);
            }
            $this->Flash->error(__('The dynasty could not be saved. Please, try again.'));
        }
        $proveniences = $this->Dynasties->Proveniences->find('list', ['limit' => 200]);
        $this->set(compact('dynasty', 'proveniences'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Dynasty id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->getRequest()->allowMethod(['post', 'delete']);
        $dynasty = $this->Dynasties->get($id);
        if ($this->Dynasties->delete($dynasty)) {
            $this->Flash->success(__('The dynasty has been deleted.'));
        } else {
            $this->Flash->error(__('The dynasty could not be deleted. Please, try again.'));
        }

        return $this->redirect(['prefix' => false, 'action' => 'index']);
    }
}
