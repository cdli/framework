<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Inscriptions Controller
 *
 * @property \App\Model\Table\InscriptionsTable $Inscriptions
 * @method \App\Model\Entity\Inscription[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class InscriptionsController extends AppController
{
    public function index()
    {
        $inscriptions = $this->paginate(
            $this->Inscriptions->find('all')
                ->contain(['Artifacts', 'UpdateEvents'])
                ->where([
                    'is_atf2conll_diff_resolved' => 0,
                    'is_latest' => true,
                ])
                ->order(['UpdateEvents.approved' => 'DESC'])
        );

        $this->set('inscriptions', $inscriptions);
    }

    /**
     * @param string|null $id Inscription id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id, $resolve = null)
    {
        if ($id === 'resolve' && !is_null($resolve)) {
            $this->setAction('resolveConllDiff', $resolve);

            return;
        }

        $inscription = $this->Inscriptions->get($id, [
            'contain' => ['Artifacts', 'UpdateEvents'],
        ]);
        $this->set('inscription', $inscription);

        if ($inscription->is_atf2conll_diff_resolved) {
            return $this->redirect([
                'prefix' => false,
                'controller' => 'Inscriptions',
                'action' => 'view',
                $id,
            ]);
        }

        $oldInscription = $this->Inscriptions->getPreviousInscriptions($inscription);
        $this->set('oldInscription', $oldInscription);
    }

    /**
     * @param string|null $id Inscription id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function resolveConllDiff($id)
    {
        // Route: /admin/inscriptions/resolve/<id>
        // not in config/routes.php, handled by view above

        if ($this->getRequest()->is('post')) {
            $inscription = $this->Inscriptions->get($id);
            $inscription->is_atf2conll_diff_resolved = true;
            if ($this->Inscriptions->save($inscription)) {
                return $this->redirect([
                    'prefix' => false,
                    'controller' => 'Inscriptions',
                    'action' => 'view',
                    $id,
                ]);
            } else {
                $this->Flash->error(__('Resolving atf2conll diff failed.'));

                return $this->redirect($this->referer());
            }
        }
    }
}
