<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller\Admin;

use App\Controller\AppController;
use App\Model\Entity\Article;
use App\Utility\Latex\Converter as LatexConverter;
use Cake\Datasource\ConnectionManager;
use Cake\Http\Exception\ForbiddenException;
use Psr\Http\Message\UploadedFileInterface;

/**
 * Articles Controller
 *
 * @property \App\Model\Table\ArticlesTable $Articles
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\Article[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Model\Table\PublicationsTable $Publications
 * @property \App\Model\Table\ArticlesAuthorsTable $ArticlesAuthors
 * @property \App\Model\Table\AuthorsTable $Authors
 * @property \App\Model\Table\ArticlesPublicationsTable $ArticlesPublications
 */
class ArticlesController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Articles');
        $this->loadModel('Users');
        $this->loadModel('Publications');
        $this->loadModel('ArticlesAuthors');
        $this->loadModel('Authors');
        $this->loadModel('ArticlesPublications');
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $type = $this->getRequest()->getParam('type');
        $type = strtolower($type);
        if (!array_key_exists($type, $this->Articles->articleContentTypes)) {
            return;
        }

        $article = $this->Articles->newEmptyEntity();

        if ($this->getRequest()->is('post')) {
            $requestData = $this->getRequest()->getData();
            $creator = $this->Users->get($this->Authentication->getIdentityData('id'))->author_id;
            $article = $this->Articles->newArticle($type, $requestData, $creator);

            if ($this->Articles->save($article)) {
                $article = $this->Articles->get($article->id);
                $this->_saveMedia($article, $requestData);
                $this->Articles->substituteImageUrls($article);

                if (!$this->Articles->save($article)) {
                    $this->Flash->error(__('Some files could not be saved'));
                    foreach ($article->getErrors() as $field => $errors) {
                        foreach ($errors as $error) {
                            $this->Flash->error($field . ': ' . $error);
                        }
                    }
                }

                return $this->redirect(['prefix' => false, 'action' => 'view', $article->article_type, $article->getArticleNumberPath()]);
            } else {
                $this->Flash->error(__('The article could not be created.'));
                /** @psalm-suppress UndefinedInterfaceMethod */
                foreach ($article->getFlatErrors() as $field => $error) {
                    $this->Flash->error($field . ': ' . $error);
                }
            }
        }

        $this->set('articleContent', $this->Articles->articleContentTypes[$type]);
        $this->set('article', $article);
        $this->render('edit');
    }

    /**
     * Delete article
     *
     * @param string $type
     * @param int $id
     * @return \Cake\Http\Response|null
     */
    public function delete($type, $id)
    {
        $article = $this->Articles->get($id);
        if ($this->Articles->delete($article)) {
            return $this->redirect(['prefix' => false, 'action' => 'index', $article->article_type]);
        } else {
            $this->Flash->error(__('The article could not be deleted.'));

            return $this->redirect(['prefix' => false, 'action' => 'view', $article->article_type, $article->getArticleNumberPath()]);
        }
    }

    /**
     * Edit article
     *
     * @param string $type
     * @param int $id
     * @return \Cake\Http\Response|null
     */
    public function edit($type, $id)
    {
        $article = $this->Articles->get($id, [
            'contain' => [
                'Authors',
            ],
        ]);

        if ($this->getRequest()->is(['post', 'put'])) {
            $requestData = $this->getRequest()->getData();
            $article = $this->Articles->patchArticle($article, $requestData);
            $this->_saveMedia($article, $requestData);
            $this->Articles->substituteImageUrls($article);

            if ($this->Articles->save($article)) {
                return $this->redirect(['prefix' => false, 'action' => 'view', $article->article_type, $article->getArticleNumberPath()]);
            } else {
                $this->Flash->error(__('The article could not be edited.'));
                foreach ($article->getFlatErrors() as $field => $error) {
                    $this->Flash->error($field . ': ' . $error);
                }
            }
        }

        $type = strtolower($type);
        $this->set('articleContent', $this->Articles->articleContentTypes[$type]);
        $this->set('article', $article);
    }

    /**
     * Preview article.
     *
     * @return \Cake\Http\Response|null
     */
    public function preview()
    {
        $requestData = $this->getRequest()->getData();
        $article = $this->Articles->newEntity($requestData);
        $this->set('article', $article);
    }

    /**
     * This function converts LaTeX to HTML.
     */
    public function convertLatex()
    {
        // Get data
        $requestData = $this->getRequest()->getData();
        $articleLatex = $requestData['latex'];

        if (empty($articleLatex)) {
            $response = json_encode([
                'html' => '',
                'citations' => [],
                'images' => [],
            ]);

            return $this->getResponse()->withStringBody($response)->withType('json');
        }

        $citations = [];
        if (isset($requestData['citations']) && !empty($requestData['citations'])) {
            $citations = $requestData['citations'];
        }

        $latex = LatexConverter::convert($articleLatex, $citations, LatexConverter::FIX_TEMPLATE);

        // Render HTML
        $this->set([
            'renderer' => $latex->renderer,
            'extractor' => $latex->extractor,
            'document' => $latex->document,
        ]);
        $html = $this->render('/Admin/Articles/convert_latex')->getBody();
        $html->rewind();
        $html = $html->getContents();

        $response = json_encode([
            'html' => $html,
            'citations' => $latex->extractor->citations,
            'images' => $latex->extractor->images,
            'log' => $latex->log,
        ]);

        return $this->getResponse()->withStringBody($response)->withType('json');
    }

    /**
     * Format specified citations based on a bibtex file.
     */
    public function convertBibtex()
    {
        $file = $this->getRequest()->getData('file');
        $citations = $this->getRequest()->getData('citations');

        $entries = $file->getStream()->getContents();

        $this->set('entries', $entries);
        $this->set('_citations', $citations);
        $this->set('_serialize', 'entries');

        $this->viewBuilder()->setClassName('JournalBibliography');
    }

    /**
     * Save uploaded PDFs and images associated with an article.
     */
    protected function _saveMedia(Article $article, $data)
    {
        $articleType = $this->Articles->articleContentTypes[$article->article_type];

        // Save images
        if ($articleType['latex'] && !empty($data['images'])) {
            try {
                foreach ($data['images'] as $index => $file) {
                    if (!($file instanceof UploadedFileInterface) || $file->getError() == UPLOAD_ERR_NO_FILE) {
                        continue;
                    }
                    $fileExtension = pathinfo($file->getClientFilename(), PATHINFO_EXTENSION);
                    $fileExtension = empty($fileExtension) ? '' : '.' . $fileExtension;
                    $fileName = str_pad($index + 1, 3, '0', STR_PAD_LEFT) . $fileExtension;
                    $file->moveTo($article->getImagePath($fileName));
                }
            } catch (\Exception $error) {
                $article->setError('content_html', __('Failed to upload all images'));

                return $article;
            }
        }

        // Save PDF
        if ($articleType['pdf'] && !empty($data['pdf'])) {
            $file = $data['pdf'];
            if ($file instanceof UploadedFileInterface && $file->getError() != UPLOAD_ERR_NO_FILE) {
                if (!$file->getClientMediaType() == 'application/pdf') {
                    $article->setError('pdf_link', __('Only pdf files are allowed'));

                    return $article;
                }

                try {
                    $pdfLink = $article->getPdfPath();
                    $file->moveTo($pdfLink);
                    $article->pdf_link = $pdfLink;
                    $article->is_pdf_uploaded = true;
                } catch (\Exception $error) {
                    $article->setError('pdf_link', __('Failed to upload PDF'));
                    $article->pdf_link = '';
                    $article->is_pdf_uploaded = false;

                    return $article;
                }
            }
        }
    }

    public function linkPublications()
    {
    }

    public function linkSuggest()
    {
        $this->autoRender = false;
        $search_key = $this->getRequest()->getParam('key');
        $type = $this->getRequest()->getParam('type');
        if ($type == 'articles') {
            $articles = $this->Articles->find('all', [
            'fields' => ['title', 'id'],
            'order' => ['title' => 'asc'],
            ])->where(['title LIKE' => '%' . $search_key . '%'])->all();

            $resultJ = json_encode($articles);
        } else {
            $pubs = $this->Publications->find('all', [
            'fields' => ['bibtexkey', 'id'],
            'order' => ['bibtexkey' => 'asc'],
            ])->where(['bibtexkey LIKE' => '%' . $search_key . '%'])->all();

            $resultJ = json_encode($pubs);
        }

        return $this->getResponse()->withStringBody($resultJ)->withType('json');
    }

    public function getArticleByID()
    {
        $this->autoRender = false;
        $id = $this->getRequest()->getParam('id');
        $type = $this->getRequest()->getParam('type');

        if ($type == 'article') {
            $article = $this->Articles->find()->where(['id' => $id])->first();
            $resultJ = json_encode($article);
        } else {
            $pubs = $this->Publications->find()->where(['id' => $id])->first();
            $resultJ = json_encode($pubs);
        }

        return $this->getResponse()->withStringBody($resultJ)->withType('json');
    }

    public function completeLink()
    {
        $this->autoRender = false;
        $article = $this->getRequest()->getParam('article');
        $publication = $this->getRequest()->getParam('publication');

        if (is_numeric($article) == '' || is_numeric($publication) == '') {
            throw new ForbiddenException(__('Request not allowed.'));
        }
        $link = $this->ArticlesPublications->find()->where(['article_id' => $article, 'publication_id' => $publication])->first();
        if (!$link) {
            $link = $this->ArticlesPublications->newEmptyEntity();
            $link->article_id = $article;
            $link->publication_id = $publication;
        }

        $link->status = 1;
        $this->ArticlesPublications->save($link);
        echo 'success';
    }

    public function viewLink()
    {
        $this->autoRender = false;
        $article = $this->getRequest()->getParam('article');
        $publication = $this->getRequest()->getParam('publication');

        if (is_numeric($article) == '' || is_numeric($publication) == '') {
            throw new ForbiddenException(__('Request not allowed.'));
        }

        $connection = ConnectionManager::get('default');

        $resultJ = $connection->execute("select a.title, p.bibtexkey, articles_publications.id,articles_publications.status, a.id as aid, p.id as pid from articles_publications join articles as a,
        publications p where article_id = '" . $article . "' and publication_id = '" . $publication . "'
         and p.id = publication_id and a.id = article_id;'")->fetchAll('assoc');

        if (count($resultJ) == 0) {
            $resultJ = json_encode(['status' => 'norecord', 'data' => '0']);
        } else {
            $resultJ = json_encode($resultJ);
        }

        return $this->getResponse()->withStringBody($resultJ)->withType('json');
    }

    public function completeUnlink()
    {
        $this->autoRender = false;
        $article = $this->getRequest()->getParam('article');
        $publication = $this->getRequest()->getParam('publication');
        $link = $this->ArticlesPublications->find()->where(['article_id' => $article, 'publication_id' => $publication])->first();
        if ($link) {
            $link ->status = 0;
            $this->ArticlesPublications->save($link);
            echo 'success';
        } else {
            throw new ForbiddenException(__('Unable to find a link.'));
        }
    }

    public function viewAllArticleLinks()
    {
        $this->autoRender = false;
        $id = $this->getRequest()->getParam('id');
        $type = $this->getRequest()->getParam('type');
        $connection = ConnectionManager::get('default');

        if ($type == 'article') {
            $resultJ = $connection->execute("select a.title, p.bibtexkey, articles_publications.id,articles_publications.status, a.id as aid, p.id as pid from articles_publications join articles as a,
        publications p where articles_publications.status = 1 and article_id = '" . $id . "'
         and p.id = publication_id and a.id = article_id;'")->fetchAll('assoc');
        } else {
            $resultJ = $connection->execute("select a.title, p.bibtexkey, articles_publications.id,articles_publications.status, a.id as aid, p.id as pid from articles_publications join articles as a,
        publications p where articles_publications.status = 1 and publication_id = '" . $id . "'
         and p.id = publication_id and a.id = article_id;'")->fetchAll('assoc');
        }

        if (count($resultJ) == 0) {
            $resultJ = json_encode(['status' => 'norecord', 'data' => '0']);
        } else {
            $resultJ = json_encode($resultJ);
        }

        return $this->getResponse()->withStringBody($resultJ)->withType('json');
    }
}
