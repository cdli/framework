<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
use App\Model\Entity\Sponsor;

/**
 * @property \App\Model\Table\SponsorsTable $Sponsors
 * @method \App\Model\Entity\Sponsor[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SponsorsController extends AppController
{
    /**
     * @return \Cake\Http\Response|null
     */
    public function add()
    {
        $sponsor = $this->Sponsors->newEmptyEntity();

        return $this->_handleAddEdit($sponsor);
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException
     */
    public function edit($id = null)
    {
        $sponsor = $this->Sponsors->get($id);

        return $this->_handleAddEdit($sponsor);
    }

    /**
     * @param \App\Model\Entity\Sponsor $sponsor
     * @return \Cake\Http\Response|null
     */
    private function _handleAddEdit(Sponsor $sponsor)
    {
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $sponsor = $this->Sponsors->patchEntity($sponsor, $this->getRequest()->getData());

            if ($this->Sponsors->save($sponsor)) {
                $image = $this->getRequest()->getData('image_upload');
                $newImage = $image !== null && $image->getError() !== \UPLOAD_ERR_NO_FILE;
                $deleteImage = $this->getRequest()->getData('image_delete', '0') === '1';

                if ($newImage || $deleteImage) {
                    foreach ($sponsor->getImagePaths() as $file) {
                        unlink(realpath($file));
                    }
                }

                if ($newImage) {
                    $filename = $image->getClientFilename();
                    $extension = pathinfo($filename, PATHINFO_EXTENSION);
                    $filename = Sponsor::IMAGE_FILE_PREFIX . "$sponsor->id.$extension";
                    $image->moveTo($filename);
                }

                $this->Flash->success(__('The sponsor has been saved.'));
            } else {
                $this->Flash->error(__('The sponsor could not be saved. Please, try again.'));
            }

            if ($sponsor->has('id')) {
                return $this->redirect(['prefix' => false, 'action' => 'view', $sponsor->id]);
            } else {
                return $this->redirect(['prefix' => false, 'action' => 'index']);
            }
        }

        $sponsorTypes = $this->Sponsors->SponsorTypes->find('list');
        $this->set('sponsor', $sponsor);
        $this->set('sponsorTypes', $sponsorTypes);
        $this->render('edit');
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException
     */
    public function delete($id = null)
    {
        $this->getRequest()->allowMethod(['post', 'delete']);

        $sponsor = $this->Sponsors->get($id);
        if ($this->Sponsors->delete($sponsor)) {
            foreach ($sponsor->getImagePaths() as $file) {
                unlink(realpath($file));
            }

            $this->Flash->success(__('The sponsor has been deleted.'));
        } else {
            $this->Flash->error(__('The sponsor could not be deleted. Please, try again.'));
        }

        return $this->redirect(['prefix' => false, 'action' => 'index']);
    }
}
