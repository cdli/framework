<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;

class SiteStatusController extends AppController
{
    /**
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function view($page)
    {
        if ($page === 'errors.json') {
            $errors = [];

            if ($this->getRequest()->getQuery('exclude')) {
                $exclude = implode('|', array_map('preg_quote', $this->getRequest()->getQuery('exclude')));
            }

            $dir = '/srv/app/cake/logs';
            $files = array_values(preg_grep('/^error\.log.*$/', scandir($dir)));
            foreach ($files as $file) {
                $contents = file_get_contents($dir . DS . $file);
                $contents = mb_convert_encoding($contents, 'UTF-8', 'UTF-8');

                $logPattern = '/(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}) (Warning|Error): (.+)\n([\s\S]+?)(?=\n+\d{4}|$)/';
                preg_match_all($logPattern, $contents, $matches, PREG_SET_ORDER);

                foreach ($matches as $match) {
                    if (isset($exclude) && preg_match("/$exclude/", $match[3])) {
                        continue;
                    }

                    $errors[] = [
                        'datetime' => $match[1],
                        'url' => preg_match('/Request URL: (.+)/', $match[4], $url) ? $url[1] : null,
                        'referer' => preg_match('/Referer URL: (.+)/', $match[4], $url) ? $url[1] : null,
                        'level' => $match[2],
                        'message' => $match[3],
                    ];
                }
            }

            return $this->getResponse()->withStringBody(json_encode($errors, JSON_THROW_ON_ERROR))->withType('json');
        }
    }
}
