<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * CdliTablet Controller
 *
 * @property \App\Model\Table\CdliTabletTable $CdliTablet
 */
class CdliTabletController extends AppController
{
    /**
     * Intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('CdliTablet');
        $this->loadComponent('RequestHandler');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $start_date = $this->getRequest()->getQuery('start_date');
        $end_date = $this->getRequest()->getQuery('end_date');
        $key = $this->getRequest()->getQuery('key');

        if ($start_date && $end_date) {
            $conditions = [
                'DATE(date_display) >=' => $start_date,
                'DATE(date_display) <=' => $end_date,
            ];
        } elseif ($key) {
            $conditions = [
                'theme LIKE' => '%' . $key . '%',
            ];
        } else {
            $conditions = [];
        }

        $this->paginate = [
            'limit' => 30,
            'order' => [
                'CdliTablet.date_display' => 'DESC',
            ],
        ];

        $results = $this->paginate($this->CdliTablet->find('all')->where($conditions));
        $this->set('results', $results);
    }

    /**
     * Add method
     */
    public function add()
    {
        if ($this->getRequest()->is(['post', 'put'])) {
            $requestData = $this->getRequest()->getData();
            $entry = $this->CdliTablet->newEntity($requestData);

            $file = $this->getRequest()->getData('file');
            if (!empty($file) && !empty($file->getClientFilename())) {
                $filename = $file->getClientFilename();
                $imagePath = WWW_ROOT . 'pubs/daily_tablets/' . $filename;

                if (!file_exists($imagePath)) {
                    try {
                        $file->moveTo($imagePath);
                        $entry->image_filename = $filename;

                        if ($this->CdliTablet->save($entry)) {
                            $this->Flash->success('New entry has been saved successfully.');

                            return $this->redirect(['prefix' => false, 'action' => 'view', $entry->id]);
                        }
                    } catch (\Exception $error) {
                        $entry->setError('image_filename', $error->getMessage());
                    }
                } else {
                    $entry->setError('image_filename', "File $filename already exists");
                }
            }

            $this->Flash->error('New entry could not be saved. Please, try again.');
        } else {
            $entry = $this->CdliTablet->newEmptyEntity();
        }

        $this->set('entry', $entry);
        $this->render('edit');
    }

    /*
     * Edit method
     */

    public function edit($id)
    {
        $entry = $this->CdliTablet->get($id);

        if ($this->getRequest()->is(['post', 'put'])) {
            $requestData = $this->getRequest()->getData();
            $entry = $this->CdliTablet->patchEntity($entry, $requestData);

            if ($this->CdliTablet->save($entry)) {
                $this->Flash->success('Entry has been updated successfully.');

                return $this->redirect(['prefix' => false, 'action' => 'view', $id]);
            }

            $this->Flash->success('Entry could not be updated. Please, try again.');
        }

        $this->set('entry', $entry);
    }

    /**
     * Delete method
     */
    public function delete($id)
    {
        $entry = $this->CdliTablet->get($id);

        if ($this->CdliTablet->delete($entry)) {
            $this->Flash->success('Entry has been deleted successfully.');
            $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error('Entry could not be deleted. Please, try again.');
            $this->redirect(['action' => 'view', $id]);
        }
    }

    /**
     * Delete all method
     */
    public function deleteAll()
    {
        $this->getRequest()->allowMethod(['post', 'delete']);
        $ids = $this->getRequest()->getData('ids');

        if ($this->CdliTablet->deleteAll(['CdliTablet.id IN' => $ids])) {
            $this->Flash->success('All selected entries have been deleted successfully.');
        } else {
            $this->Flash->error('Selected entries could not be deleted. Please, try again.');
        }

        $this->redirect(['action' => 'index']);
    }
}
