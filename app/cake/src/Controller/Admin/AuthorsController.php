<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Authors Controller
 *
 * @property \App\Model\Table\AuthorsTable $Authors
 * @method \App\Model\Entity\Author[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AuthorsController extends AppController
{
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $author = $this->Authors->newEmptyEntity();
        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();
            $author = $this->Authors->patchEntity($author, $data);
            if ($data['last'] . $data['first'] == '') {
                $author->setErrors([
                    'first' => ['Both first and last name cannot be empty'],
                    'last' => ['Both first and last name cannot be empty'],
                    ]);
            } elseif ($this->Authors->save($author)) {
                $this->Flash->success(__('The author has been saved.'));

                return $this->redirect(['prefix' => false, 'action' => 'index']);
            }
            $this->Flash->error(__('The author could not be saved. Please, try again.'));
        }
        $this->set(compact('author'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Author id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // Check if id is provided
        if (!isset($id)) {
            $this->Flash->error('No Author selected');

            return $this->redirect(['action' => 'index']);
        }

        $author = $this->Authors->get($id);

        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $data = $this->getRequest()->getData();
            $author = $this->Authors->patchEntity($author, $data);
            if ($data['last'] . $data['first'] == '') {
                $author->setErrors([
                    'first' => ['Both first and last name cannot be empty'],
                    'last' => ['Both first and last name cannot be empty'],
                    ]);
            } elseif ($this->Authors->save($author)) {
                $this->Flash->success(__('The author details have been updated.'));

                return $this->redirect(['prefix' => false, 'action' => 'index']);
            }
            $this->Flash->error(__('The author details could not be updated. Please, try again.'));
        }
        $this->set(compact('author'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Author id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->getRequest()->allowMethod(['post', 'delete']);
        $author = $this->Authors->get($id, ['finder' => 'forDelete']);

        $error = false;
        if ($author->staff) {
            $this->Flash->error('The author has a related Staff record.');
            $error = true;
        }
        if ($author->user) {
            $this->Flash->error('The author has a related user record.');
            $error = true;
        }
        if ($author->sign_readings_comments_count) {
            $this->Flash->error('The author has related sign reading comments.');
            $error = true;
        }
        if ($author->cdli_tablet_count) {
            $this->Flash->error('The author has related CDLI tablet records.');
            $error = true;
        }
        if ($author->articles_count) {
            $this->Flash->error('The author has related articles records.');
            $error = true;
        }
        if ($author->publications_count) {
            $this->Flash->error('The author has related publications records.');
            $error = true;
        }
        if ($author->update_events_count) {
            $this->Flash->error('The author has related update events records.');
            $error = true;
        }

        if (!$error && $this->Authors->delete($author)) {
            $this->Flash->success(__('The author has been deleted.'));
        } else {
            $this->Flash->error(__('The author could not be deleted.'));
        }

        return $this->redirect(['prefix' => false, 'action' => 'index']);
    }

    /**
     * authorSearchAjax method
     */
    public function authorSearchAjax()
    {
        $this->autoRender = false;
        $search_key = $this->getRequest()->getParam('author');

        $authors = $this->Authors->find('all', [
            'fields' => ['author'],
            ])->where(['author LIKE' => '%' . $search_key . '%'])->all();
        $authors = json_encode($authors);

        echo $authors;
    }

    /**
     * addAuthorAjax method
     */
    public function addAuthorAjax()
    {
        $this->autoRender = false;
        $author_name = $this->getRequest()->getParam('author');

        $author = $this->Authors->newEmptyEntity();
        $author->author = $author_name;

        $this->Authors->save($author);

        echo $author;
    }
}
