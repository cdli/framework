<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Http\Exception\ForbiddenException;

/**
 * Archives Controller
 *
 * @property \App\Model\Table\UpdateEventsTable $UpdateEvents
 * @method \App\Model\Entity\UpdateEvent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\GranularAccessComponent $GranularAccess
 */
class UpdateEventsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('GranularAccess');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if (!$this->GranularAccess->canReviewEdits()) {
            throw new ForbiddenException();
        }

        $this->paginate = [
            'conditions' => ['OR' => [['status' => 'created'], ['status' => 'submitted']]],
            'contain' => ['Creators', 'ExternalResources'],
        ] + $this->paginate;

        $updateEvents = $this->paginate($this->UpdateEvents);
        $this->set('updateEvents', $updateEvents);
    }
}
