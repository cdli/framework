<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * StaffTypes Controller
 *
 * @property \App\Model\Table\StaffTypesTable $StaffTypes
 * @method \App\Model\Entity\StaffType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StaffTypesController extends AppController
{
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $staffType = $this->StaffTypes->newEmptyEntity();
        if ($this->getRequest()->is('post')) {
            $staffType = $this->StaffTypes->patchEntity($staffType, $this->getRequest()->getData());
            if ($this->StaffTypes->save($staffType)) {
                $this->Flash->success(__('The staff type has been saved.'));

                return $this->redirect(['prefix' => false, 'action' => 'index']);
            }
            $this->Flash->error(__('The staff type could not be saved. Please, try again.'));
        }
        $this->set(compact('staffType'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Staff Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $staffType = $this->StaffTypes->get($id, [
            'contain' => [],
        ]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $staffType = $this->StaffTypes->patchEntity($staffType, $this->getRequest()->getData());
            if ($this->StaffTypes->save($staffType)) {
                $this->Flash->success(__('The staff type has been saved.'));

                return $this->redirect(['prefix' => false, 'action' => 'index']);
            }
            $this->Flash->error(__('The staff type could not be saved. Please, try again.'));
        }
        $this->set(compact('staffType'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Staff Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->getRequest()->allowMethod(['post', 'delete']);
        $staffType = $this->StaffTypes->get($id);
        if ($this->StaffTypes->delete($staffType)) {
            $this->Flash->success(__('The staff type has been deleted.'));
        } else {
            $this->Flash->error(__('The staff type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['prefix' => false, 'action' => 'index']);
    }
}
