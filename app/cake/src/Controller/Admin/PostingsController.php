<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\I18n\FrozenTime;

/**
 * Postings Controller
 *
 * @property \App\Model\Table\PostingsTable $Postings
 * @method \App\Model\Entity\Posting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostingsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'limit' => 20,
            'contain' => ['Creators', 'Modifiers', 'PostingTypes'],
        ];

        $postings = $this->paginate($this->Postings);
        $this->set(compact('postings'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        //getting author_id
        $username = $this->Authentication->getIdentityData('username');
        $userTable = $this->getTableLocator()->get('Users');
        $user = $userTable->findByUsername($username)->first();

        $posting = $this->Postings->newEmptyEntity();
        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();
            $data['created_by'] = $user['author_id'];
            $data['modified_by'] = $user['author_id'];
            $data['artifact_id'] = strtolower($data['artifact_id']);
            $data['artifact_id'] = ltrim($data['artifact_id'], 'p');
            $posting = $this->Postings->patchEntity($posting, $data);

            $now = FrozenTime::now();
            $posting->created = $now;
            $posting->modified = $now;
            if ($posting->published && !$posting->has('publish_start')) {
                $posting->publish_start = $now;
            }

            if ($this->Postings->save($posting)) {
                $this->Flash->success(__('The posting has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The posting could not be saved. Please, try again.'));
        }
        $lang = ['eng' => 'eng', 'fr' => 'fr', 'de' => 'de', 'es' => 'es', 'ar' => 'ar'];
        $postingTypes = $this->Postings->PostingTypes->find('list', ['limit' => 200]);
        $artifacts = $this->Postings->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('posting', 'postingTypes', 'artifacts', 'lang'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Posting id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $posting = $this->Postings->get($id, [
            'contain' => [],
        ]);

        //getting author_id
        $username = $this->Authentication->getIdentityData('username');
        $userTable = $this->getTableLocator()->get('Users');
        $user = $userTable->findByUsername($username)->first();

        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $data = $this->getRequest()->getData();
            $data['artifact_id'] = strtolower($data['artifact_id']);
            $data['artifact_id'] = ltrim($data['artifact_id'], 'p');
            $data['modified_by'] = $user['author_id'];
            $posting = $this->Postings->patchEntity($posting, $data);

            $now = FrozenTime::now();
            $posting->modified = $now;
            if ($posting->published && !$posting->has('publish_start')) {
                $posting->publish_start = $now;
            }

            if ($this->Postings->save($posting)) {
                $this->Flash->success(__('The posting has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The posting could not be saved. Please, try again.'));
        }
        $lang = ['eng' => 'eng', 'fr' => 'fr', 'de' => 'de', 'es' => 'es', 'ar' => 'ar'];
        $postingTypes = $this->Postings->PostingTypes->find('list', ['limit' => 200]);
        $artifacts = $this->Postings->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('posting', 'postingTypes', 'artifacts', 'lang'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Posting id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->getRequest()->allowMethod(['post', 'delete']);
        $posting = $this->Postings->get($id);
        if ($this->Postings->delete($posting)) {
            $this->Flash->success(__('The posting has been deleted.'));
        } else {
            $this->Flash->error(__('The posting could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
