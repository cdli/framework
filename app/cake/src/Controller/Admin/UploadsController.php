<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Uploads Controller
 *
 * @property \App\Model\Table\UploadsTable $Uploads
 * @property \App\Controller\Component\TaskComponent $Task
 * @property \App\Controller\Component\FileserviceComponent $Fileservice
 * @method \App\Model\Entity\Upload[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UploadsController extends AppController
{
    protected $userid;

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        $this->loadComponent('Fileservice');
        // $this->loadComponent('Csrf');
        $this->loadComponent('Task');
        // $this->Authentication->allowUnauthenticated(['geturl']);
        $this->userid = $this->Authentication->getIdentityData('id');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    }

    public function dashboard()
    {
        if ($this->getRequest()->is('post')) {
            $payload = $this->getRequest()->getData();
            if ($payload['isLattice']) {
                unset($payload['isLattice']);
                $minioObjects = [];
                $taskArti = $this->Uploads->find('list', [
                    'keyField' => function ($file) {
                        $matches = [];
                        preg_match('/le|re|o|r|be|te/i', $file->realname, $matches);
                        if (count($matches) == 1) {
                            return $matches[0];
                        } else {
                            return 'Unknown';
                        }
                    },
                    'valueField' => function ($file) use (&$minioObjects) {
                        $result = $file->filename . '.processed.jpeg';
                        array_push($minioObjects, ['bucket' => 'image-scratch', 'key' => $file->filename . '.processed.jpeg']);
                        $file->status = 3;
                        $this->Uploads->save($file);

                        return $result;
                    }])
                    ->where(['user_id' => $this->userid, 'status' => 2, 'pno' => $payload['pno']])
                    ->toArray();
                $pno = $payload['pno'];
                unset($payload['pno']);
                if ($taskArti) {
                    $this->Task->create('image', 'archival', [$payload, $pno], true, $minioObjects);
                }
            } else {
                if (array_key_exists('Invalid', $payload)) {
                    $payload[''] = $payload['Invalid'];
                    unset($payload['Invalid']);
                }
                $res = $this->Uploads->find('list', [
                'keyField' => 'filename',
                'valueField' => function ($file) {
                    $file->status = 2;
                    if ($this->Uploads->save($file)) {
                        return true;
                    } else {
                        return false;
                    }
                },
                'groupField' => 'pno',
                ])
                ->where(['user_id' => $this->userid, 'status' => 1, 'pno IN' => array_keys($payload)])
                ->toArray();
                $imageObject = [];
                $imageTemp = [];
                foreach ($res as $pnoGroup => $pno) {
                    if (!array_key_exists($pnoGroup, $imageObject)) {
                        $imageObject[$pnoGroup] = [];
                    }
                    foreach ($pno as $image => $saveRes) {
                        if ($saveRes) {
                            array_push($imageObject[$pnoGroup], ['bucket' => 'image-raw', 'key' => $image]);
                        }
                    }
                }

                foreach ($imageObject as $pno => $imageObjs) {
                    $this->Task->create('image', 'jpegimage', ['pno' => $pno], true, $imageObjs);
                }
            }
            // debug($res);
        }
        $unconfirmed_up = $this->Uploads->find('list', [
            'keyField' => 'filename',
            'valueField' => function ($file) {
                return $file->realname ? $file->realname : $file->filename;
            },
            'groupField' => function ($file) {
                $return = $this->Fileservice->checkFile($file->filename, 'image-raw');
                if ($return) {
                    $file->status = 1;
                    $this->Uploads->save($file); //EXPERIMNETAL : saving data inside callback
                }

                return $return;
            },
            ])->where(['user_id' => $this->userid, 'status' => 0])->toArray();
        $this->set('uploads', $unconfirmed_up);

        $rawFiles = $this->Uploads->find('list', [
            'keyField' => 'pno',
            'valueField' => 'count',
        ]);
        $rawFiles = $rawFiles->select(['pno' => 'pno', 'count' => $rawFiles->func()->count('filename')])
                        ->where(['user_id' => $this->userid, 'status' => 1])
                        ->group('pno')
                        ->order('pno DESC')
                        ->toArray();
        $this->set('unprocessed', $rawFiles);

        $processedFiles = $this->Uploads->find('list', ['keyField' => 'pno'])
                            ->select(['pno' => 'pno'])
                            ->where(['user_id' => $this->userid, 'status' => 2])
                            ->group('pno')
                            ->order('pno DESC')
                            ->toArray();
        $this->set('processed', array_keys($processedFiles));
        $completed = $this->Uploads->find('list', ['keyField' => 'pno'])
                            ->select(['pno' => 'pno'])
                            ->where(['user_id' => $this->userid, 'status' => 3])
                            ->group('pno')
                            ->order('pno DESC')
                            ->toArray();
        $this->set('completed', array_keys($completed));
    }

    public function geturl()
    {
        $this->autoRender = false;
        if ($this->getRequest()->is('ajax')) {
            if ($this->getRequest()->is('post')) {
                $filename = $this->getRequest()->getData('name');
                $uploadInst = $this->Fileservice->presignedsave('image-raw', $filename);
                $this->log("newkeybelike {$uploadInst['key']}", 'debug');
                $uploadDB = $this->Uploads->newEmptyEntity();
                $uploadDB->filename = $uploadInst['key'];
                $uploadDB->bucket = 'image-raw';
                $uploadDB->user_id = $this->userid;
                $uploadDB->status = 0;
                $this->Uploads->save($uploadDB);
                $payload = ['presignedUrl' => $uploadInst['url'], 'key' => $uploadInst['key']];

                return $this->getResponse()
                    ->withStringBody(json_encode($payload))
                    ->withType('json');
            }
        }
    }

    public function edit($id = null)
    {
        $result = $this->Uploads->find('list', [
            'keyField' => function ($file) {
                $matches = [];
                preg_match('/le|re|o|r|be|te/i', $file->realname, $matches);
                if (count($matches) == 1) {
                    return $matches[0];
                } else {
                    return 'Unknown';
                }
            },
            'valueField' => function ($file) {
                $result = [];
                $result['Filename'] = $file->realname;
                $result['Clipart'] = $this->Fileservice->getPresignedFile($file->filename . '.lossy.jpeg', 'image-scratch');
                $result['Crop'] = $this->Fileservice->getPresignedFile($file->filename . '.half.processed.png', 'image-scratch');
                $result['Boundaries'] = $this->Fileservice->getPresignedFile($file->filename . '.contours.jpeg', 'image-scratch');
                $result['Remains'] = $this->Fileservice->getPresignedFile($file->filename . '.mask.jpeg', 'image-scratch');
                $result['Processed'] = $this->Fileservice->getPresignedFile($file->filename . '.processed.jpeg', 'image-scratch');

                return $result;
            }])
            ->where(['user_id' => $this->userid, 'status' => 2, 'pno' => $id])
            ->toArray();
        $this->set('filenames', array_keys($result));
        $this->set('artifacts', $result);
        $this->set('pno', $id);
    }

    public function view($id = null)
    {
        if ($this->Fileservice->checkFile((string)$id . '.jpg', 'archivals')) {
            $image = $this->Fileservice->getPresignedFile($id . '.jpg', 'archivals');
            $this->set('image', $image);
            $this->set('pno', $id);
            $this->set('result', true);
        } else {
            $this->set('pno', $id);
            $this->set('result', false);
        }
    }
}
