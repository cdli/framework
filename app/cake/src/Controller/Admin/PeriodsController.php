<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller\Admin;

use App\Controller\AppController;
use App\Utility\CdliProcessor\TokenList;

/**
 * Periods Controller
 *
 * @property \App\Model\Table\PeriodsTable $Periods
 * @method \App\Model\Entity\Period[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Model\Table\InscriptionsTable $Inscriptions
 * @property \App\Controller\Component\ApiComponent $Api
 */
class PeriodsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Api');
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $period = $this->Periods->newEmptyEntity();
        if ($this->getRequest()->is('post')) {
            $period = $this->Periods->patchEntity($period, $this->getRequest()->getData());
            if ($this->Periods->save($period)) {
                $this->Flash->success(__('The period has been saved.'));

                return $this->redirect(['prefix' => false, 'action' => 'index']);
            }
            $this->Flash->error(__('The period could not be saved. Please, try again.'));
        }
        $this->set(compact('period'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Period id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $period = $this->Periods->get($id, [
            'contain' => [],
        ]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $period = $this->Periods->patchEntity($period, $this->getRequest()->getData());
            if ($this->Periods->save($period)) {
                $this->Flash->success(__('The period has been saved.'));

                return $this->redirect(['prefix' => false, 'action' => 'index']);
            }
            $this->Flash->error(__('The period could not be saved. Please, try again.'));
        }
        $this->set(compact('period'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Period id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->getRequest()->allowMethod(['post', 'delete']);
        $period = $this->Periods->get($id);
        if ($this->Periods->delete($period)) {
            $this->Flash->success(__('The period has been deleted.'));
        } else {
            $this->Flash->error(__('The period could not be deleted. Please, try again.'));
        }

        return $this->redirect(['prefix' => false, 'action' => 'index']);
    }

    public function tokenLists()
    {
        if ($this->getRequest()->is('post')) {
            $ids = $this->getRequest()->getData();
            foreach ($ids as $id => $kinds) {
                foreach ($kinds as $kind) {
                    $data = $this->getTokenList($id, $kind);

                    // JSON output
                    $file = TokenList::getTokenListFile($id, $kind, 'json');
                    file_put_contents($file, json_encode($data));

                    // TXT output (tokens only)
                    $file = TokenList::getTokenListFile($id, $kind, 'txt');
                    file_put_contents($file, implode("\n", array_keys($data['list'])));

                    // TSV output (tokens and frequencies)
                    $file = TokenList::getTokenListFile($id, $kind, 'tsv');
                    $list = $data['list'];
                    asort($list, SORT_NUMERIC);
                    $list = array_map(function ($token, $frequency) {
                        return $token . "\t" . $frequency;
                    }, array_keys($list), $list);
                    file_put_contents($file, implode("\n", $list));
                }
            }
        }

        $periods = $this->Periods->find()->all();
        $tokenLists = [];
        foreach ($periods as $period) {
            $tokenLists[$period->id] = [];

            $wordFile = TokenList::getTokenListFile($period->id, 'words');
            if (file_exists($wordFile)) {
                $tokenLists[$period->id]['words'] = filemtime($wordFile);
            }
            $signFile = TokenList::getTokenListFile($period->id, 'signs');
            if (file_exists($signFile)) {
                $tokenLists[$period->id]['signs'] = filemtime($signFile);
            }
        }
        $this->set('periods', $periods);
        $this->set('tokenLists', $tokenLists);
    }

    /**
     * @param string|null $id Period id.
     * @return \Cake\Http\Response|null
     */
    public function tokenList($id, $kind)
    {
        $this->set([
            'list' => $this->getTokenList($id, $kind),
            '_serialize' => 'list',
        ]);
        $this->Api->renderAs($this, 'json');
    }

    private function getTokenList($id, $kind)
    {
        $this->loadModel('Inscriptions');
        $inscriptions = $this->Inscriptions->find()
            ->where(['Inscriptions.is_latest' => 1])
            ->matching('Artifacts.Periods', function ($q) use ($id) {
                return $q->where(['Periods.id' => $id]);
            })
            ->select('atf')
            ->disableBufferedResults()
            ->all();

        $inscription_count = 0;
        $token_count = 0;
        $token_instance_count = 0;

        $list = [];
        foreach ($inscriptions as $inscription) {
            $inscription_count += 1;

            $tokens = TokenList::processText($inscription->atf, $kind);

            foreach ($tokens as $token) {
                if (array_key_exists($token, $list)) {
                    $list[$token] += 1;
                } else {
                    $token_count += 1;
                    $list[$token] = 1;
                }
                $token_instance_count += 1;
            }
        }
        ksort($list);

        return [
            'metadata' => [
                'period_id' => $id,
                'token_kind' => $kind,
                'inscription_count' => $inscription_count,
                'token_count' => $token_count,
                'token_instance_count' => $token_instance_count,
            ],
            'list' => $list,
        ];
    }
}
