<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * AgadeMails Controller
 *
 * @property \App\Model\Table\AgadeMailsTable $AgadeMails
 * @method \App\Model\Entity\AgadeMail[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AgadeMailsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null Redirects on successful edit.
     */
    public function index()
    {
        $data = $this->getRequest()->getData();
        if ($data != []) {
            foreach ($data['changes'] as $key => $x) {
                $data['changes'][$key]['is_public'] = $x['is_public'] == '1' ? true : false;

                if (array_key_exists('cdli_tags', $x)) {
                    $cdli_tags_new = [];
                    foreach ($x['cdli_tags'] as $tag_id) {
                        if ($tag_id != '0') {
                            $tag_id = (int)$tag_id;
                            array_push($cdli_tags_new, ['id' => $tag_id]);
                        }
                    }
                    $data['changes'][$key]['cdli_tags'] = $cdli_tags_new;
                }
            }

            foreach ($data['changes'] as $entity) {
                $agademail = $this->AgadeMails->find()->where(['id' => $entity['id']])->first();
                $this->AgadeMails->patchEntity($agademail, $entity);
                $this->AgadeMails->save($agademail);
            }

            $this->Flash->success(__('Permissions updated.'));
        }

        $this->paginate = [
            'limit' => 10,
            'sort' => 'category',
            'contain' => ['CdliTags'],
        ];
        $agademails = $this->paginate($this->AgadeMails);

        // Get a list of tags.
        $cdli_tags = $this->AgadeMails->CdliTags->find('all', ['order' => 'cdli_tag'])->all();

        $this->set(compact('agademails', 'cdli_tags'));
    }

    /**
     * View method
     *
     * @param string|null $id AgadeMail id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $agademails = $this->AgadeMails->find('all', ['fields' => ['id', 'title', 'category'], 'order' => 'category'])->all();

        $agademail_ids = [];
        foreach ($agademails as $entry) {
            array_push($agademail_ids, $entry['id']);
        }

        $current_pos = array_search($id, $agademail_ids);
        $prev_pos = $current_pos - 1;
        $next_pos = $current_pos + 1;

        $prev_id = null;
        if ($prev_pos >= 0) {
            $prev_id = $agademail_ids[$prev_pos];
        }

        $next_id = null;
        if ($next_pos < count($agademail_ids)) {
            $next_id = $agademail_ids[$next_pos];
        }

        $agademail = $this->AgadeMails->find()->where(['id' => $id])->first();
        $this->set(compact('agademail', 'prev_id', 'next_id'));
    }
}
