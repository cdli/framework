<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * SponsorTypes Controller
 *
 * @property \App\Model\Table\SponsorTypesTable $SponsorTypes
 * @method \App\Model\Entity\SponsorType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SponsorTypesController extends AppController
{
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sponsorType = $this->SponsorTypes->newEmptyEntity();
        if ($this->getRequest()->is('post')) {
            $sponsorType = $this->SponsorTypes->patchEntity($sponsorType, $this->getRequest()->getData());
            if ($this->SponsorTypes->save($sponsorType)) {
                $this->Flash->success(__('The sponsor type has been saved.'));

                return $this->redirect(['prefix' => false, 'action' => 'index']);
            }
            $this->Flash->error(__('The sponsor type could not be saved. Please, try again.'));
        }
        $this->set(compact('sponsorType'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Sponsor Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sponsorType = $this->SponsorTypes->get($id, [
            'contain' => [],
        ]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $sponsorType = $this->SponsorTypes->patchEntity($sponsorType, $this->getRequest()->getData());
            if ($this->SponsorTypes->save($sponsorType)) {
                $this->Flash->success(__('The sponsor type has been saved.'));

                return $this->redirect(['prefix' => false, 'action' => 'index']);
            }
            $this->Flash->error(__('The sponsor type could not be saved. Please, try again.'));
        }
        $this->set(compact('sponsorType'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Sponsor Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->getRequest()->allowMethod(['post', 'delete']);
        $sponsorType = $this->SponsorTypes->get($id);
        if ($this->SponsorTypes->delete($sponsorType)) {
            if (glob(WWW_ROOT . 'files-up/images/sponsor-img/' . $id . '.*')) {
                $file = glob(WWW_ROOT . 'files-up/images/sponsor-img/' . $id . '.*');
                if ($file[0]) {
                    unlink($file[0]);
                }
            }
            $this->Flash->success(__('The sponsor type has been deleted.'));
        } else {
            $this->Flash->error(__('The sponsor type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['prefix' => false, 'action' => 'index']);
    }
}
