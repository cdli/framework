<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Http\Client;

class SearchController extends AppController
{
    /**
     * @return \Cake\Http\Response|void
     */
    public function view($page, $id = null)
    {
        if ($page === '_doc') {
            $query = ['query' => ['term' => ['id' => $id]]];

            $request = new Client();
            $request = $request->post(Configure::read('ServiceUrls.elasticsearch') . '/artifacts/_search', json_encode($query), ['type' => 'json']);
            $results = $request->getJson();
            $results = $results['hits']['hits'];

            $this->set('doc', $results[0]);
        }
    }
}
