<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

/**
 * Rulers Controller
 *
 * @property \App\Model\Table\RulersTable $Rulers
 * @method \App\Model\Entity\Ruler[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ApiComponent $Api
 */
class RulersController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);
        $this->loadComponent('Search');

        // Set access for public.
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'sortableFields' => ['ruler'],
            'contain' => ['Periods', 'Dynasties', 'Dates'],
        ] + $this->paginate;

        $query = $this->Rulers->find('search', [
            'search' => $this->request->getQueryParams(),
        ]);

        $rulers = $this->paginate($query);
        $access_granted = $this->RequestAccess->checkUserRoles([1]);

        $this->set(compact('rulers', 'access_granted'));
        $this->set('_serialize', 'rulers');
    }

    /**
     * View method
     *
     * @param string|null $id Ruler id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ruler = $this->Rulers->get($id, [
            'contain' => ['Periods', 'Dynasties', 'Dates'],
        ]);
        $access_granted = $this->RequestAccess->checkUserRoles([1]);

        $this->set(compact('access_granted'));
        $this->set('ruler', $ruler);
        $this->set('_serialize', 'ruler');
    }
}
