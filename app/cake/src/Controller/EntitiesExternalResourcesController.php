<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * @property \App\Model\Table\EntitiesExternalResourcesTable $EntitiesExternalResources
 * @method \App\Model\Entity\EntitiesExternalResource[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ApiComponent $Api
 */
class EntitiesExternalResourcesController extends AppController
{
    /**
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData']]);
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if (!$this->Api->requestsFormat(['LinkedData'])) {
            $this->redirect('/');
        }

        $this->paginate = [
            'limit' => 1000,
            'maxLimit' => 1000,
        ] + $this->paginate;

        $entitiesExternalResources = $this->EntitiesExternalResources->find()
            ->contain(['ExternalResources'])
            ->where(['entity_table !=' => 'artifacts']);
        $entitiesExternalResources = $this->paginate($entitiesExternalResources);

        $this->set('entitiesExternalResources', $entitiesExternalResources);
        $this->set('_serialize', 'entitiesExternalResources');
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException
     */
    public function view($id = null)
    {
        $entitiesExternalResource = $this->EntitiesExternalResources->get($id, [
            'contain' => ['ExternalResources'],
        ]);

        if (!$this->Api->requestsFormat(['LinkedData'])) {
            $this->redirect('/' . $entitiesExternalResource->getSourceUri());
        }

        $this->set('entitiesExternalResource', $entitiesExternalResource);
        $this->set('_serialize', 'entitiesExternalResource');
    }
}
