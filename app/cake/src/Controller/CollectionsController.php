<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * @property \App\Model\Table\ArtifactsCollectionsTable $ArtifactsCollections
 * @property \App\Model\Table\CollectionsTable $Collections
 * @property \App\Model\Table\EntitiesUpdatesTable $EntitiesUpdates
 * @property \App\Model\Table\ExternalResourcesTable $ExternalResources
 * @property \App\Model\Table\UpdateEventsTable $UpdateEvents
 * @method \App\Model\Entity\Collection[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ApiComponent $Api
 * @property \App\Controller\Component\ChangesetsComponent $Changesets
 * @property \App\Controller\Component\GranularAccessComponent $GranularAccess
 */
class CollectionsController extends AppController
{
    /**
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);
        $this->loadComponent('Changesets');
        $this->loadComponent('GranularAccess');
        $this->loadComponent('Search');
        $this->loadModel('ArtifactsCollections');
        $this->loadModel('EntitiesUpdates');
        $this->loadModel('ExternalResources');
        $this->loadModel('UpdateEvents');

        // Set access for public.
        $this->Authentication->allowUnauthenticated(['index', 'view', 'history', 'add', 'edit']);
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'sortableFields' => [
                'collection',
            ],
            'contain' => ['EntitiesNames', 'ExternalResources'],
        ] + $this->paginate;

        $data = $this->request->getQueryParams();

        $query = $this->Collections->find('search', [
            'search' => $data,
        ]);
        if (isset($data['letter'])) {
            $query->where([
                'Collections.collection LIKE' => $data['letter'] . '%',
            ]);
        }

        $collections = $this->paginate($query);
        $pinned_collections = $this->Collections->find()->where(['Collections.is_pinned']);
        $canSubmitEdits = $this->GranularAccess->canSubmitEdits();

        $this->set('collections', $collections);
        $this->set('pinned_collections', $pinned_collections);
        $this->set('canSubmitEdits', $canSubmitEdits);
        $this->set('_serialize', 'collections');
    }

    /**
     * @param string|null $id Collection id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $collection = $this->Collections->get($id, [
            'contain' => [
                'EntitiesNames',
                'ExternalResources',
                'GadmCountries',
                'GadmRegions',
                'GadmDistricts',
            ],
        ]);

        $this->set('collection', $collection);
        $this->set('count', $this->ArtifactsCollections->find()->where(['collection_id' => $id])->count());
        $this->set('canEdit', $this->GranularAccess->canSubmitEdits());
        $this->set('_serialize', 'collection');
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function history($id = null)
    {
        $collection = $this->Collections->get($id);
        $updateEvents = $this->UpdateEvents->find()
            ->where(['UpdateEvents.status' => 'approved'])
            ->matching('EntitiesUpdates', function ($q) use ($collection) {
                return $q->where(['entity_table' => 'collections', 'entity_id' => $collection->id]);
            })
            ->contain([
                'Creators',
                'Authors',
            ])
            ->order(['UpdateEvents.approved' => 'DESC'])
            ->all()
            ->toArray();

        $this->set('collection', $collection);
        $this->set('updateEvents', $updateEvents);
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function add()
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error('Only logged in users who have an author associated to their account can edit. Create an account and / or  contact cdli@ames.ox.ac.uk to associate an author with your account.');

            return $this->redirect(['action' => 'index']);
        }

        $collection = $this->Collections->newEmptyEntity();
        $this->_handleAddEdit($collection);
        $this->render('edit');
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id)
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error('Only logged in users who have an author associated to their account can edit. Create an account and / or  contact cdli@ames.ox.ac.uk to associate an author with your account.');

            return $this->redirect(['action' => 'view', $id]);
        }

        $collection = $this->Collections->get($id, ['contain' => ['EntitiesNames', 'ExternalResources']]);
        $this->_handleAddEdit($collection);
    }

    /**
     * @param \App\Model\Entity\Collection $collection
     * @return \Cake\Http\Response|void
     */
    private function _handleAddEdit($collection)
    {
        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $data = $this->getRequest()->getData();
            foreach ($data as &$value) {
                if (is_array($value) && array_key_exists('template', $value)) {
                    unset($value['template']);
                }
            }
            $this->Collections->patchEntity($collection, $data);
            $update = $this->EntitiesUpdates->newEntityFromChangedEntity($collection);

            $changeset = $this->Changesets->create(['other_entity'], [$update]);
            $this->Changesets->set($changeset);
            $this->Changesets->setBlockingChangeset($changeset);
            $this->redirect(['controller' => 'Changesets', 'action' => 'view', $changeset['id']]);
        }

        $externalResources = $this->ExternalResources->find('list')->order('external_resource')->toArray();

        $this->set('collection', $collection);
        $this->set('externalResources', $externalResources);
    }
}
