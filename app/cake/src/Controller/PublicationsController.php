<?php
declare(strict_types=1);

namespace App\Controller;

use App\Datasource\NaturalSortPaginator;
use App\Model\Entity\Publication;
use App\Utility\BulkUpload\BulkUploadData;
use App\Utility\BulkUpload\BulkUploadFile;

/**
 * @property \App\Model\Table\PublicationsTable $Publications
 * @property \App\Model\Table\ArtifactsTable $Artifacts
 * @property \App\Model\Table\ArtifactsUpdatesTable $ArtifactsUpdates
 * @property \App\Model\Table\EntitiesPublicationsTable $EntitiesPublications
 * @property \App\Model\Table\EntitiesUpdatesTable $EntitiesUpdates
 * @property \App\Model\Table\ExternalResourcesTable $ExternalResources
 * @property \App\Model\Table\UpdateEventsTable $UpdateEvents
 * @method \App\Model\Entity\Publication[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ApiComponent $Api
 * @property \App\Controller\Component\ChangesetsComponent $Changesets
 * @property \Cake\Controller\Component\PaginatorComponent $Paginator
 */
class PublicationsController extends AppController
{
    /**
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['Bibliography', 'LinkedData', 'TableExport']]);
        $this->loadComponent('Changesets');
        $this->loadComponent('GranularAccess');
        $this->loadModel('Artifacts');
        $this->loadModel('ArtifactsUpdates');
        $this->loadModel('EntitiesPublications');
        $this->loadModel('EntitiesUpdates');
        $this->loadModel('ExternalResources');
        $this->loadModel('UpdateEvents');

        $this->Authentication->allowUnauthenticated(['index', 'view', 'history', 'search']);
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = ['sortableFields' => ['bibtexkey', 'designation']] + $this->paginate;
        $this->loadComponent('Paginator');
        $this->Paginator->setPaginator(new NaturalSortPaginator());

        $queryParams = $this->getRequest()->getQueryParams();
        $query = $this->_getPublicationQuery($queryParams);
        $publications = $this->paginate($query)->toArray();

        $this->_prepareRelatedEntities();
        $this->set('publications', $publications);
        $this->set('queryParams', $queryParams);
        $this->set('canSubmitEdits', $this->GranularAccess->canSubmitEdits());
        $this->set('_serialize', 'publications');
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException
     */
    public function view($id = null)
    {
        $publication = $this->Publications->get($id, [
            'contain' => [
                'EntryTypes',
                'Journals',
                'Authors' => [
                    'sort' => ['AuthorsPublications.sequence' => 'ASC'],
                ],
                'Editors' => [
                    'sort' => ['EditorsPublications.sequence' => 'ASC'],
                ],
                'ExternalResources',
                'Proveniences.Regions',
            ],
        ]);

        $artifactsModel = $this->getTableLocator()->get('Artifacts');
        $query = $artifactsModel->find('forPublication', [
            'publicationId' => $id,
        ]);
        $query->where(['Artifacts.retired' => false]);
        if (!$this->RequestAccess->checkUserRoles([1, 4])) {
            $query->where(['Artifacts.is_public' => true]);
        }

        $this->loadComponent('Paginator');
        $this->Paginator->setPaginator(new NaturalSortPaginator());

        $publication->artifacts = $this->paginate($query, [
                'scope' => 'artifacts',
                'limit' => 10000,
                'maxLimit' => 10000,
                'sortableFields' => [
                    'id',
                    'designation',
                    'Periods.period',
                    'Proveniences.provenience',
                    'ArtifactTypes.artifact_type',
                    'EntitiesPublications.publication_type',
                    'EntitiesPublications.exact_reference',
                ],
            ])
            ->toArray();

        $this->set('publication', $publication);
        $this->set('canSubmitEdits', $this->GranularAccess->canSubmitEdits());
        $this->set('_serialize', 'publication');
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException
     */
    public function history($id = null)
    {
        $publication = $this->Publications->get($id);
        $updateEvents = $this->UpdateEvents->find()
            ->where(['UpdateEvents.status' => 'approved'])
            ->matching('EntitiesUpdates', function ($q) use ($publication) {
                return $q->where(['entity_table' => 'publications', 'entity_id' => $publication->id]);
            })
            ->contain(['Creators', 'Authors'])
            ->order(['UpdateEvents.approved' => 'DESC'])
            ->all()
            ->toArray();

        $this->set('publication', $publication);
        $this->set('updateEvents', $updateEvents);
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function search()
    {
        $searchKey = $this->getRequest()->getBody()->getContents();

        $publications = $this->Publications->find('all')
            ->select(['id', 'title', 'designation', 'bibtexkey'])
            ->limit(10)
            ->where([
                'OR' => [
                    'title LIKE' => '%' . $searchKey . '%',
                    'bibtexkey LIKE' => '%' . $searchKey . '%',
                    'designation LIKE' => '%' . $searchKey . '%',
                ],
            ])
            ->all()
            ->toArray();

        $publications = json_encode(array_map(function ($publication) {
            $label = '[' . $publication['designation'] . '] ' . ($publication['title'] ?? $publication['designation']);

            return [
                'id' => $publication['id'],
                'label' => h($label),
            ];
        }, $publications));

        return $this->getResponse()->withStringBody($publications)->withType('json');
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function add()
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error('Only logged in users who have an author associated to their account can edit. Create an account and / or  contact cdli@ames.ox.ac.uk to associate an author with your account.');

            return $this->redirect(['action' => 'index']);
        }

        $publication = $this->Publications->newEmptyEntity();
        $this->_handleAddEdit($publication);
        $this->render('edit');
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException
     */
    public function edit($id)
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error('Only logged in users who have an author associated to their account can edit. Create an account and / or  contact cdli@ames.ox.ac.uk to associate an author with your account.');

            return $this->redirect(['action' => 'view', $id]);
        }

        $publication = $this->Publications->get($id, ['contain' => ['Authors', 'Editors', 'ExternalResources']]);
        $this->_handleAddEdit($publication);
    }

    /**
     * @param \App\Model\Entity\Publication $publication
     * @return \Cake\Http\Response|void
     */
    private function _handleAddEdit($publication)
    {
        $this->Changesets->handleBlockingChangeset();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $data = $this->getRequest()->getData();
            foreach ($data as &$value) {
                if (is_array($value) && array_key_exists('template', $value)) {
                    unset($value['template']);
                }
            }
            $this->Publications->patchEntity($publication, $data);
            $update = $this->EntitiesUpdates->newEntityFromChangedEntity($publication);
            foreach ($this->Publications->validateChanges($publication, $update->apply()->toArray()) as $field => $errors) {
                $update->setError("entity_update.$field", $errors);
            }

            $changeset = $this->Changesets->create(['other_entity'], [$update]);
            $this->Changesets->set($changeset);
            $this->Changesets->setBlockingChangeset($changeset);
            $this->redirect(['controller' => 'Changesets', 'action' => 'view', $changeset['id']]);
        }

        $this->set('publication', $publication);
        $this->_prepareRelatedEntities();
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function merge()
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error('Only logged in users who have an author associated to their account can edit text. Create an account and / or  contact cdli@ames.ox.ac.uk to associate an author with your account.');

            return $this->redirect($this->referer());
        }

        $step = $this->getRequest()->getQuery('step', 'select');
        $post = $this->getRequest()->is(['post', 'put', 'patch']);

        if ($step === 'select' && !$post) {
            $this->Changesets->handleBlockingChangeset();

            $queryParams = $this->getRequest()->getQueryParams();
            $query = $this->_getPublicationQuery($queryParams);
            $publicationsTotal = $query->count();
            $queryLimit = 500;
            $publications = $query->limit($queryLimit)->all()->toArray();

            $this->_prepareRelatedEntities();
            $this->set('publications', $publications);
            $this->set('publicationsTotal', $publicationsTotal);
            $this->set('hasQuery', count($query->getValueBinder()->bindings()) > 0);
            $this->set('queryParams', $queryParams);
            $this->set('queryLimit', $queryLimit);
            $this->render('merge_select');
        } elseif ($step === 'select' && $post) {
            $merge = $this->getRequest()->getData('merge', []);
            $primary = $this->getRequest()->getData('primary', $merge[0]);

            if (count($merge) < 2) {
                $this->Flash->error(__('Select at least two publications to merge'));

                return $this->redirect($this->referer());
            }

            if (!in_array($primary, $merge)) {
                $primary = $merge[0];
            }

            $this->_prepareRelatedEntities();
            $this->set($this->Publications->getMergeData($merge, (int)$primary));
            $this->render('merge');
        } elseif ($step === 'merge' && $post) {
            $data = $this->getRequest()->getData();
            foreach ($data as &$value) {
                if (is_array($value) && array_key_exists('template', $value)) {
                    unset($value['template']);
                }
            }

            $publication = $this->Publications->get($data['id'], ['contain' => ['Authors', 'Editors', 'ExternalResources']]);
            $updates = [];

            if (array_key_exists('artifacts', $data)) {
                $artifacts = $this->Artifacts->find('all')
                    ->whereInList('id', array_keys($data['artifacts']))
                    ->contain(['Publications']);

                foreach ($artifacts as $artifact) {
                    $joinData = $data['artifacts'][$artifact->id]['_joinData'];
                    $index = array_search($joinData['id'], array_column(array_column($artifact->publications, '_joinData'), 'id'));
                    $artifact->publications[$index] = $this->Publications->newEmptyEntity();
                    $artifact->publications[$index]->id = $publication->id;
                    $artifact->publications[$index]->bibtexkey = $publication->bibtexkey;
                    $artifact->publications[$index]->_joinData = (object)$joinData;

                    $artifactUpdate = $this->ArtifactsUpdates->newEmptyEntity();
                    $artifactUpdate->artifact_id = $artifact->id;
                    foreach (['publications_key', 'publications_type', 'publications_exact_ref', 'publications_comment'] as $artifactField) {
                        $artifactUpdate->set($artifactField, $artifact->getFlatValue($artifactField));
                    }
                    $updates[] = $artifactUpdate;
                }
            }

            $this->Publications->patchEntity($publication, array_diff_key($data, array_flip(['artifacts', 'merge'])));
            $updates[] = $this->EntitiesUpdates->newEntityFromChangedEntity($publication);

            if (array_key_exists('merge', $data)) {
                foreach ($data['merge'] as $mergedId) {
                    /** @var string $mergedId */
                    $updates[] = $this->EntitiesUpdates->newEntityFromDeletedEntity($mergedId, 'publications');

                    // Update other publication associations
                    $topicAssociations = array_map(function ($topic) {
                        return $topic . '.Publications';
                    }, array_values(Publication::$topicAssociations));
                    $mergedPublication = $this->Publications->get($mergedId, ['contain' => $topicAssociations]);
                    foreach ($mergedPublication->getTopics() as $topicTable => $topics) {
                        foreach ($topics as $topic) {
                            $topicPublications = $topic->publications;
                            foreach ($topicPublications as $i => $topicPublication) {
                                if ($topicPublication->id === (int)$mergedId) {
                                    $topicPublications[$i] = $this->Publications->newEmptyEntity();
                                    $topicPublications[$i]->id = $publication->id;
                                    $topicPublications[$i]->_joinData = $this->EntitiesPublications->get($topicPublication->_joinData->id);
                                    $topicPublications[$i]->_joinData->publication_id = $publication->id;
                                }
                            }
                            $topic->set('publications', $topicPublications);
                            $updates[] = $this->EntitiesUpdates->newEntityFromChangedEntity($topic);
                        }
                    }
                }
            }

            $changeset = $this->Changesets->create(['artifact', 'other_entity'], $updates);
            $this->Changesets->set($changeset);
            $this->Changesets->setBlockingChangeset($changeset);
            $this->redirect(['controller' => 'Changesets', 'action' => 'view', $changeset['id']]);
        } else {
            return $this->redirect(['action' => 'view', '?' => $this->getRequest()->getQueryParams()]);
        }
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function upload()
    {
        $this->Changesets->handleBlockingChangeset();

        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error('Only logged in users who have an author associated to their account can edit text. Create an account and / or  contact cdli@ames.ox.ac.uk to associate an author with your account.');

            return $this->redirect($this->referer());
        }

        if (!$this->getRequest()->is(['post', 'put', 'patch'])) {
            return;
        }

        $file = $this->getRequest()->getData('upload');
        $fileType = $file->getClientMediaType();

        if ($fileType === 'text/csv') {
            $data = BulkUploadFile::parseCsv($file);
        } elseif ($fileType === 'text/x-bibtex') {
            $data = BulkUploadFile::parseBibtex($file);
        } else {
            // Might still be BibTeX
            try {
                $data = BulkUploadFile::parseBibtex($file);
            } catch (\Exception $e) {
            }
        }

        if (!isset($data) || !is_array($data) || count($data) === 0) {
            $this->Flash->error('Expected a non-empty BibTeX or CSV file');

            return;
        }

        $updates = [];

        foreach ($data as $row) {
            if (!array_key_exists('bibtexkey', $row)) {
                $udpates[] = $this->EntitiesUpdates->newEntityFromError('entity_id', 'No "bibtexkey" column');
                continue;
            }

            $publication = $this->Publications
                ->findAllByBibtexkey($row['bibtexkey'])
                ->contain(['Authors', 'Editors', 'ExternalResources'])
                ->first();

            if (is_null($publication)) {
                $publication = $this->Publications->newEmptyEntity();
            }

            $changes = new BulkUploadData('Publications', $row, Publication::$csvSchema);
            $changes->apply($publication);

            // Add some implicit join data
            foreach (['authors', 'editors'] as $field) {
                if ($publication->has($field)) {
                    foreach ($publication->get($field) as $i => $value) {
                        if (!$value->_joinData->has('sequence')) {
                            $value->_joinData->set('sequence', $i);
                        }
                    }
                }
            }

            $update = $this->EntitiesUpdates->newEntityFromChangedEntity($publication);
            foreach ($this->Publications->validateChanges($publication, $update->apply()->toArray()) as $field => $errors) {
                $update->setError("entity_update.$field", $errors);
            }

            $updates[] = $update;
        }

        $changeset = $this->Changesets->create(['other_entity'], $updates);
        $this->Changesets->set($changeset);
        $this->Changesets->setBlockingChangeset($changeset);
        $this->redirect(['controller' => 'Changesets', 'action' => 'view', $changeset['id']]);
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException
     */
    public function delete($id)
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error('Only logged in users who have an author associated to their account can edit. Create an account and / or  contact cdli@ames.ox.ac.uk to associate an author with your account.');

            return $this->redirect(['action' => 'view', $id]);
        }

        if (!$this->getRequest()->is(['post', 'put', 'patch'])) {
            return $this->redirect(['action' => 'edit', $id]);
        }

        $topics = ['Artifacts', ...array_values(Publication::$topicAssociations)];
        $publication = $this->Publications->get($id, ['contain' => $topics]);

        foreach ($topics as $topic) {
            $association = $this->Publications->getAssociation($topic);
            $property = $association->getProperty();
            if ($publication->has($property) && count($publication->get($property)) > 0) {
                $this->Flash->error(__('Cannot delete publication while still linked to {0}.', $topic));

                return $this->redirect(['action' => 'edit', $id]);
            }
        }

        $update = $this->EntitiesUpdates->newEntityFromDeletedEntity($id, 'publications');
        $changeset = $this->Changesets->create(['other_entity'], [$update]);
        $this->Changesets->set($changeset);
        $this->Changesets->setBlockingChangeset($changeset);
        $this->redirect(['controller' => 'Changesets', 'action' => 'view', $changeset['id']]);
    }

    /**
     * @param array $queryParams
     * @return \Cake\ORM\Query
     */
    private function _getPublicationQuery(array $queryParams)
    {
        $query = $this->Publications
            ->find('all')
            ->contain([
                'EntryTypes',
                'Journals',
                'Editors' => [
                    'sort' => ['EditorsPublications.sequence' => 'ASC'],
                ],
                'Authors' => [
                    'sort' => ['AuthorsPublications.sequence' => 'ASC'],
                ],
                'ExternalResources',
            ]);

        foreach ($queryParams as $field => $value) {
            $value = trim($value);

            if (empty($value)) {
                continue;
            }

            if ($field == 'entry_type_id' || $field == 'journal_id') {
                $query = $query->where(["Publications.{$field}" => $value]);
            } elseif ($field == 'from') {
                $query = $query->where(['Publications.year >=' => $value]);
            } elseif ($field == 'to') {
                $query = $query->where(['Publications.year <=' => $value]);
            } elseif ($field == 'author') {
                $query = $query->innerJoinWith(
                    'Authors',
                    function ($q) use ($value) {
                        return $q->where(['Authors.author LIKE' => "%{$value}%"]);
                    }
                );
            } elseif ($field == 'artifact') {
                $query = $query->innerJoinWith(
                    'Artifacts',
                    function ($q) use ($value) {
                        return $q->where([
                            'Artifacts.id' => ltrim($value, 'P0'),
                            'Artifacts.is_public' => 1,
                        ]);
                    }
                );
            } elseif ($this->Publications->hasField($field)) {
                $query = $query->where(["Publications.{$field} LIKE" => "%{$value}%"]);
            }
        }

        return $query;
    }

    private function _prepareRelatedEntities()
    {
        $entryTypes = $this->Publications->EntryTypes->find('list')->orderAsc('label')->toArray();
        $externalResources = $this->ExternalResources->find('list')->orderAsc('external_resource')->toArray();
        $journals = $this->Publications->Journals->find('list')->orderAsc('journal')->toArray();

        $this->set('entryTypes', $entryTypes);
        $this->set('externalResources', $externalResources);
        $this->set('journals', $journals);
    }
}
