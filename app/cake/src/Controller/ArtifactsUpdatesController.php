<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

use App\Model\Entity\Artifact;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\UnauthorizedException;

/**
 * ArtifactsUpdates Controller
 *
 * @property \App\Model\Table\ArtifactsUpdatesTable $ArtifactsUpdates
 * @method \App\Model\Entity\artifactsUpdate[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Model\Table\ArtifactsTable $Artifacts
 * @property \App\Controller\Component\ChangesetsComponent $Changesets
 * @property \App\Controller\Component\GranularAccessComponent $GranularAccess
 */
class ArtifactsUpdatesController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Artifacts');
        $this->loadModel('ArtifactsUpdates');
        $this->loadComponent('Changesets');
        $this->loadComponent('GranularAccess');

        $this->Authentication->allowUnauthenticated(['view', 'add', 'upload']);
    }

    /**
     * View method
     *
     * @param string|null $id Artifacts Date id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactsUpdate = $this->ArtifactsUpdates->get($id, [
            'contain' => [
                'Artifacts',
                'UpdateEvents' => ['Creators', 'Authors', 'Reviewers'],
            ],
        ]);

        if (!$this->GranularAccess->canAccessEdits($artifactsUpdate->update_event)) {
            throw new UnauthorizedException();
        }

        if (
            ($artifactsUpdate->has('artifact_id') && !$artifactsUpdate->artifact->is_public) &&
            !$this->GranularAccess->canViewPrivateArtifact()
        ) {
            throw new UnauthorizedException();
        }

        $old = $this->ArtifactsUpdates->getPreviousUpdates($artifactsUpdate, false);
        $this->GranularAccess->amendArtifactsUpdate([$artifactsUpdate, $old]);
        $this->set(['artifactsUpdate' => $artifactsUpdate, 'old' => $old]);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null
     */
    public function add()
    {
        $this->Changesets->handleBlockingChangeset();

        if (!$this->request->is(['post', 'put', 'patch'])) {
            $this->set('canSubmitEdits', $this->GranularAccess->canSubmitEdits());

            return;
        }

        /** @psalm-suppress InvalidArgument */
        ini_set('max_execution_time', 300);

        // Get options
        $concatenate = $this->request->getData('concatenate_data');

        // Validate file
        $file = $this->request->getData('csv');
        if ($file->getClientMediaType() !== 'text/csv') {
            $this->Flash->error('Expected a CSV file');
            $file = null;
        }

        if (is_null($file)) {
            $this->set('canSubmitEdits', $this->GranularAccess->canSubmitEdits());

            return;
        }

        // Parse CSV file
        $handle = $file->getStream()->detach();
        if (fgets($handle, 4) !== "\u{FEFF}") {
            rewind($handle);
        }
        $header = fgetcsv($handle);
        $errors = [];
        $updates = [];

        // Temporarily disable query logging as an optimization
        $connection = ConnectionManager::get('default');
        $connection->enableQueryLogging(false);

        $line = 1;
        while (($row = fgetcsv($handle)) !== false) {
            $line++;

            // Skip empty lines
            if (trim(implode('', $row)) === '') {
                continue;
            }

            // Trim whitespace around values
            $row = array_map('trim', $row);

            // Try to create an associative array by combining the header from
            // the start of the file with the current line/row. If array_combine()
            // files it usually means the header and the current row are of
            // different lengths.
            try {
                $new_data = array_combine($header, $row);
            } catch (\Error $error) {
                $errors[] = __(
                    'Line {0} does not contain the same number of values ({1}) as expected from the header ({2})',
                    $line,
                    count($row),
                    count($header)
                );
                continue;
            }

            // Parse row
            try {
                // Remove changes to admin data from unauthorized users
                if (!$this->GranularAccess->isAdmin()) {
                    $new_data = array_diff_key($new_data, Artifact::$privateFlatFields);
                }

                // Interpret & validate flat data
                $update = $this->ArtifactsUpdates->newEntityFromFlatData($new_data, $concatenate);

                // Check whether user has access to artifact
                if ($update->has('artifact_id')) {
                    if (!$update->artifact->is_public && !$this->GranularAccess->canViewPrivateArtifact()) {
                        throw new RecordNotFoundException();
                    }
                }

                // If a row does not change anything (perhaps common if uploading
                // a large CSV with sporadic changes), and the row does not contain
                // errors that obscure intended changes; skip the row entirely.
                if (empty($update->getChanged()) && empty($update->getErrors())) {
                    continue;
                }

                // Validate fields within update
                $update->validate();

                unset($update->artifact);
                $updates[] = $update;
            } catch (RecordNotFoundException $error) {
                // If the artifact id is given but does not exist (anymore),
                // display the error in the interface with additional info given
                // in the input.
                $update = $this->ArtifactsUpdates->newEntityFromFlatData(
                    array_diff_key($new_data, ['artifact_id' => null])
                );
                $update->setError('artifact_id', __('Record {0} not found in table "artifacts"', $new_data['artifact_id']));
                $updates[] = $update;
            } catch (\Error $error) {
                // If other errors are thrown, add a generic error.
                $errors[] = $error->getMessage();
            }
        }
        $connection->enableQueryLogging(true);

        $changeset = $this->Changesets->create(['artifact'], $updates, $errors);
        $this->Changesets->set($changeset);
        $this->Changesets->setBlockingChangeset($changeset);
        $this->redirect(['controller' => 'Changesets', 'action' => 'view', $changeset['id']]);
    }
}
