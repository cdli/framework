<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

use Cake\ORM\TableRegistry;

/**
 * Languages Controller
 *
 * @property \App\Model\Table\LanguagesTable $Languages
 * @method \App\Model\Entity\Language[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ApiComponent $Api
 */
class LanguagesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);
        $this->loadComponent('Search');

        // Set access for public.
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'limit' => 1000,
            'maxLimit' => 1000,
            'sortableFields' => ['parent_id', 'language', 'protocol_code', 'inline_code'],
            'contain' => ['ParentLanguages', 'ChildLanguages'],
        ] + $this->paginate;

        $query = $this->Languages->find('search', [
            'search' => $this->request->getQueryParams(),
        ]);

        $languages = $this->paginate($query);
        $access_granted = $this->RequestAccess->checkUserRoles([1]);

        $this->set(compact('languages', 'access_granted'));
        $this->set('_serialize', 'languages');
    }

    /**
     * View method
     *
     * @param string|null $id Language id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $language = $this->Languages->get($id, [
            'contain' => ['ParentLanguages', 'ChildLanguages'],
        ]);

        //$this->set('language', $language);
        $artifacts = TableRegistry::get('ArtifactsLanguages');
        $count = $artifacts->find('list', ['conditions' => ['language_id' => $id]])->count();
        $access_granted = $this->RequestAccess->checkUserRoles([1]);

        $this->set(compact('language', 'count', 'access_granted'));
        $this->set('_serialize', 'language');
    }
}
