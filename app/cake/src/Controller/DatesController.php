<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

use Cake\ORM\TableRegistry;

/**
 * Dates Controller
 *
 * @property \App\Model\Table\DatesTable $Dates
 * @method \App\Model\Entity\Date[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ApiComponent $Api
 */
class DatesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData']]);

        // Set access for public.
        // $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Months', 'Years', 'Dynasties', 'Rulers'],
        ];
        $dates = $this->paginate($this->Dates);
        $access_granted = $this->RequestAccess->checkUserRoles([1]);

        $this->set(compact('dates', 'access_granted'));
        $this->set('_serialize', 'dates');
    }

    /**
     * View method
     *
     * @param string|null $id Date id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $date = $this->Dates->get($id, [
            'contain' => ['Months', 'Years', 'Dynasties', 'Rulers'],
        ]);

        $artifacts = TableRegistry::get('ArtifactsDates');
        $count = $artifacts->find('list', ['conditions' => ['date_id' => $id]])->count();

        $this->set(compact('date', 'count'));
        $this->set('_serialize', 'date');
    }
}
