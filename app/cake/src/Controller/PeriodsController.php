<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

/**
 * Periods Controller
 *
 * @property \App\Model\Table\PeriodsTable $Periods
 * @method \App\Model\Entity\Period[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ApiComponent $Api
 */
class PeriodsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);

        // Set access for public.
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate['limit'] = 500;
        $this->paginate['maxLimit'] = 500;

        $periods = $this->paginate($this->Periods->find('all'));

        $access_granted = $this->RequestAccess->checkUserRoles([1]);

        $this->set(compact('periods', 'access_granted'));
        $this->set('_serialize', 'periods');
    }

    /**
     * View method
     *
     * @param string|null $id Period id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifacts = $this->loadModel('Artifacts');
        $count = $artifacts->find('list', ['conditions' => ['period_id' => $id]])->count();
        $period = $this->Periods->get($id, [
            'contain' => ['Rulers', 'Years'],
        ]);
        $access_granted = $this->RequestAccess->checkUserRoles([1]);

        $this->set(compact('access_granted', 'count'));
        $this->set('period', $period);
        $this->set('_serialize', 'period');
    }
}
