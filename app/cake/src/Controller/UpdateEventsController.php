<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

use Cake\Datasource\ConnectionManager;
use Cake\Http\Exception\BadRequestException;
use Cake\I18n\FrozenTime;

/**
 * UpdateEvents Controller
 *
 * @property \App\Model\Table\UpdateEventsTable $UpdateEvents
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\UpdateEvent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Model\Table\ArtifactsTable $Artifacts
 * @property \App\Controller\Component\ChangesetsComponent $Changesets
 * @property \App\Controller\Component\GranularAccessComponent $GranularAccess
 */
class UpdateEventsController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Artifacts');
        $this->loadModel('UpdateEvents');
        $this->loadModel('Users');
        $this->loadComponent('Api');
        $this->loadComponent('Changesets');
        $this->loadComponent('GranularAccess');
        $this->loadComponent('Search');

        $this->Authentication->allowUnauthenticated(['index', 'view', 'add', 'edit', 'delete', 'submit', 'decline']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = ['sortableFields' => ['update_type', 'status']] + $this->paginate;

        if (!$this->GranularAccess->canReviewEdits()) {
            $this->paginate['conditions'] = [
                'UpdateEvents.status' => 'approved',
            ];
        }

        $updateEvents = $this->paginate($this->UpdateEvents->find('search', [
            'search' => $this->request->getQueryParams(),
            'contain' => [
                'ExternalResources',
                'Reviewers',
                'Creators',
                'Authors',
            ],
        ]));

        $this->set('updateEvents', $updateEvents);
        $this->set('_serialize', 'updateEvents');
    }

    /**
     * View method
     *
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $event = $this->UpdateEvents->get($id, [
            'contain' => [
                'Reviewers.Users',
                'Creators.Users',
                'Authors',
                'ExternalResources',
            ],
        ]);

        $isAdmin = $this->GranularAccess->isAdmin();
        $isReviewer = $this->GranularAccess->canReviewEdits();
        $isAuthor = !empty($event->creator->user) && $this->Authentication->getIdentity() &&
            $event->creator->user->id === $this->Authentication->getIdentityData('id');

        if (!$this->GranularAccess->canAccessEdits($event)) {
            $this->Flash->error($this->RequestAccess->getConfig('authError'));

            return $this->redirect(['action' => 'index']);
        }

        // Add artifactsUpdates and inscriptions depending on access
        $this->GranularAccess->amendUpdateEvent($event);

        // Set warnings
        if ($event->has('inscriptions')) {
            foreach ($event->inscriptions as $inscription) {
                if (!is_null($inscription->annotation)) {
                    $inscription->setAnnotationWarnings();
                }

                $inscription->setAtfWarnings();
                if (!is_null($inscription->artifact->period_id)) {
                    $inscription->setTokenWarnings($inscription->artifact->period_id);
                }
            }
        }

        $this->set('updateEvent', $event);
        $this->set('isAdmin', $isAdmin);
        $this->set('isReviewer', $isReviewer);
        $this->set('isAuthor', $isAuthor);
        $this->set('isApprovable', $event->isApprovableByEditor() || $isAdmin);
        $this->set('_serialize', 'updateEvent');
    }

    /**
     * @return \Cake\Http\Response|null
     */
    public function add()
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error($this->RequestAccess->getConfig('authError'));

            return $this->redirect(['action' => 'index']);
        }

        $event = $this->UpdateEvents->newEmptyEntity();
        $creator = $this->Users->get($this->Authentication->getIdentityData('id'), ['contain' => ['Authors']]);
        $event->creator = $creator->author;

        // Get updates from sessions
        $allChangesets = $this->Changesets->getAll();
        $changesets = $this->getRequest()->getData('changesets') ?? $this->getRequest()->getQuery('changesets');

        if (is_null($changesets)) {
            $this->Flash->error(__('No changes to submit'));

            return $this->redirect($this->referer());
        }

        $changesets = array_intersect_key($allChangesets, array_flip($changesets));
        $changes = $this->Changesets->merge(array_values($changesets));

        if ($changes['hasErrors']) {
            $this->Flash->error(__('Cannot merge changesets with erros'));

            return $this->redirect($this->referer());
        }

        $event->update_type = $changes['update_type'];
        foreach ($changes['updates'] as $update) {
            $field = $update->getUpdateEventField();
            if (!$event->has($field)) {
                $event->set($field, []);
            }
            $event->{$field}[] = $update;
        }

        if ($event->has('inscriptions')) {
            foreach ($event->inscriptions as $inscription) {
                if ($inscription->has('artifact_id')) {
                    $inscription->artifact = $this->Artifacts->get($inscription->artifact_id, ['contain' => ['Inscriptions']]);
                }
            }
        }

        if ($this->request->is('post')) {
            $action = $this->request->getData('action');

            if ($action !== 'created' && $action !== 'submitted') {
                throw new BadRequestException();
            }

            // Add metadata to event
            $event = $this->UpdateEvents->patchEntity($event, $this->request->getData());
            $event->created = FrozenTime::now();
            $event->status = $action;

            // Remove placeholder artifacts
            if ($event->has('artifacts_updates')) {
                foreach ($event->artifacts_updates as $update) {
                    if (!empty($update->artifact) && $update->artifact->isNew()) {
                        unset($update->artifact);
                    }
                }
            }

            if ($this->UpdateEvents->save($event)) {
                $this->Changesets->setAll(array_diff_key($allChangesets, $changesets));
                $this->Flash->success(__('The update has been {0}.', $event->status));

                return $this->redirect(['action' => 'view', $event->id]);
            }

            $this->Flash->error(__('The update could not be {0}. Please, try again.', $event->status));
        }

        $external_resources = $this->UpdateEvents->ExternalResources->find('list', [
            'valueField' => ['abbrev'],
            'order' => ['abbrev' => 'ASC'],
            'limit' => 200,
        ]);
        $this->set('event', $event);
        $this->set('external_resources', $external_resources);
        $this->set('canReviewEdits', $this->GranularAccess->canReviewEdits());
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id)
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error($this->RequestAccess->getConfig('authError'));

            return $this->redirect(['action' => 'view', $id]);
        }

        $event = $this->UpdateEvents->get($id, [
            'contain' => [
                'Reviewers',
                'Creators',
                'Authors',
                'ExternalResources',
            ],
        ]);
        $user = $this->Users->get($this->Authentication->getIdentityData('id'));

        if (!$event->isCreatedBy($user) && !$this->GranularAccess->isAdmin()) {
            $this->Flash->error($this->RequestAccess->getConfig('authError'));

            return $this->redirect(['action' => 'view', $event->id]);
        }

        if ($this->request->is('post')) {
            $event = $this->UpdateEvents->patchEntity($event, $this->request->getData());

            if ($this->UpdateEvents->save($event)) {
                $this->Flash->success(__('The update has been updated.'));

                return $this->redirect(['action' => 'view', $event->id]);
            }

            $this->Flash->error(__('The update could not be updated.'));
        }

        $external_resources = $this->UpdateEvents->ExternalResources->find('list', [
            'valueField' => ['abbrev'],
            'order' => ['abbrev' => 'ASC'],
            'limit' => 200,
        ]);
        $this->set('event', $event);
        $this->set('external_resources', $external_resources);
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function submit($id)
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error($this->RequestAccess->getConfig('authError'));

            return $this->redirect(['action' => 'view', $id]);
        }

        if ($this->request->is('post')) {
            $event = $this->UpdateEvents->get($id, [
                'contain' => [
                    'Reviewers',
                    'Creators',
                    'ExternalResources',
                ],
            ]);

            if ($event->status != 'created') {
                $this->Flash->error('Cannot submit already submitted updates.');

                return $this->redirect(['action' => 'view', $event->id]);
            }

            $user = $this->Users->get($this->Authentication->getIdentityData('id'));

            if ($event->isCreatedBy($user) || $this->GranularAccess->isAdmin()) {
                $event->status = 'submitted';

                if ($this->UpdateEvents->save($event)) {
                    $this->Flash->success(__('The update has been submitted.'));

                    return $this->redirect(['action' => 'view', $event->id]);
                }
            }

            $this->Flash->error(__('The update could not be submitted.'));
        }

        return $this->redirect(['action' => 'view', $id]);
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function approve($id)
    {
        if (!$this->GranularAccess->canReviewEdits()) {
            $this->Flash->error($this->RequestAccess->getConfig('authError'));

            return $this->redirect(['action' => 'view', $id]);
        }

        if ($this->request->is('post')) {
            $event = $this->UpdateEvents->get($id, [
                'contain' => [
                    'Reviewers',
                    'Creators',
                    'ExternalResources',
                ],
            ]);

            if ($event->status != 'submitted') {
                $this->Flash->error('Can only approve submitted updates.');

                return $this->redirect(['action' => 'view', $event->id]);
            }

            $this->GranularAccess->amendUpdateEvent($event);
            if (!$event->isApprovableByEditor() && !$this->GranularAccess->isAdmin()) {
                $this->Flash->error('Cannot approve update.');

                return $this->redirect(['action' => 'view', $event->id]);
            }

            $user = $this->Users->get($this->Authentication->getIdentityData('id'), ['contain' => ['Authors']]);
            $event->status = 'approved';
            $event->approved = FrozenTime::now();
            $event->approved_by = $user->author->id;

            /** @var \Cake\Database\Connection $connection */
            $connection = ConnectionManager::get('default');
            $connection->begin();

            try {
                $success = $event->apply() && $this->UpdateEvents->save($event, ['associated' => false]);
            } catch (\Throwable $e) {
                $this->Flash->error(__('Technical info: {0}', $e->getMessage()));
                $success = false;
            }

            if ($success) {
                $connection->commit();
                $this->Flash->success(__('The update has been approved.'));
            } else {
                $connection->rollback();
                $this->Flash->error(__('The update could not be approved.'));
                foreach ($event->getErrors() as $errors) {
                    foreach ($errors as $error) {
                        if (!is_array($error)) {
                            $this->Flash->error($error);
                        }
                    }
                }
            }
        }

        return $this->redirect(['action' => 'view', $id]);
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function decline($id)
    {
        if (!$this->GranularAccess->canReviewEdits()) {
            $this->Flash->error($this->RequestAccess->getConfig('authError'));

            return $this->redirect(['action' => 'view', $id]);
        }

        if ($this->request->is('post')) {
            $event = $this->UpdateEvents->get($id, [
                'contain' => [
                    'Reviewers',
                    'Creators',
                    'ExternalResources',
                ],
            ]);

            if ($event->status != 'submitted') {
                $this->Flash->error('Can only decline submitted updates.');

                return $this->redirect(['action' => 'view', $event->id]);
            }

            $user = $this->Users->get($this->Authentication->getIdentityData('id'), ['contain' => ['Authors']]);
            $event->status = 'declined';
            $event->approved = FrozenTime::now();
            $event->approved_by = $user->author->id;

            if ($this->UpdateEvents->save($event, ['associated' => false])) {
                $this->Flash->success(__('The update has been declined.'));
            } else {
                $this->Flash->error(__('The update could not be declined.'));
            }
        }

        return $this->redirect(['action' => 'view', $id]);
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException
     */
    public function delete($id = null)
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error($this->RequestAccess->getConfig('authError'));

            return $this->redirect(['action' => 'view', $id]);
        }

        $this->getRequest()->allowMethod(['post', 'delete']);

        $event = $this->UpdateEvents->get($id, ['contain' => ['Creators']]);
        $user = $this->Users->get($this->Authentication->getIdentityData('id'));

        if ($event->status == 'approved') {
            $this->Flash->error('Cannot delete approved update.');

            return $this->redirect(['action' => 'view', $event->id]);
        }

        if ($event->isCreatedBy($user) || $this->GranularAccess->isAdmin()) {
            if ($this->UpdateEvents->delete($event)) {
                $this->Flash->success(__('The update has been deleted.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The update could not be deleted. Please, try again.'));

                return $this->redirect(['action' => 'view', $event->id]);
            }
        }

        $this->Flash->error($this->RequestAccess->getConfig('authError'));

        return $this->redirect(['action' => 'view', $event->id]);
    }
}
