<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

/**
 * Authors Controller
 *
 * @property \App\Model\Table\AuthorsTable $Authors
 * @method \App\Model\Entity\Author[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Model\Table\StaffTable $Staff
 * @property \App\Model\Table\UpdateEventsTable $UpdateEvents
 * @property \App\Model\Table\PublicationsTable $Publications
 * @property \App\Controller\Component\ApiComponent $Api
 */
class AuthorsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);
        $this->loadComponent('Search');

        // Set access for public.
        $this->Authentication->allowUnauthenticated(['index', 'view', 'edit']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $data = $this->request->getQueryParams();

        $query = $this->Authors->find('search', [
            'search' => $this->request->getQueryParams(),
        ]);

        if (isset($data['letter'])) {
            $query->where([
                'Authors.author LIKE' => $data['letter'] . '%',
            ]);

            $this->paginate = [
                'limit' => 1000,
                'maxLimit' => 1000,
            ] + $this->paginate;
        }

        $authors = $this->paginate($query);
        $access_granted = $this->RequestAccess->checkUserRoles([1, 2, 12]);
        $this->set(compact('authors', 'access_granted'));
        $this->set('_serialize', 'authors');
    }

    /**
     * View method
     *
     * @param string|null $id Author id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->paginate = [
            'limit' => 10,
            'Pub' => ['scope' => 'pub'],
        ];

        $author = $this->Authors->get($id, ['contain' => 'Publications']);

        $this->loadModel('Staff');
        $staff = $this->Staff
            ->find()
            ->where(['author_id' => $id])
            ->first();

        $this->loadModel('UpdateEvents');
        $updateEvents_q = $this->UpdateEvents
            ->find()
            ->contain([
                'Reviewers',
                'Creators',
                'Authors',
                'ExternalResources',
            ])
            ->leftJoinWith('Authors')
            ->where(['OR' => [
                'UpdateEvents.created_by' => $id, //creator
                'UpdateEvents.approved_by' => $id, // approver
                'Authors.id' => $id, // author -> editor
            ]])
            ->group('UpdateEvents.id');

        $updateEvents = $this->paginate($updateEvents_q, ['scope' => 'update_events']);

        $this->loadModel('Publications');
        $query = $this->Publications
            ->find('all')
            ->contain([
                'EntryTypes',
                'Journals',
                'Authors' => [
                    'sort' => ['AuthorsPublications.sequence' => 'ASC'],
                ],
                'Editors' => [
                    'sort' => ['EditorsPublications.sequence' => 'ASC'],
                ],
            ])
            ->innerJoinWith('Authors', function ($q) use ($author) {
                return $q->where(['Authors.author' => $author->author]);
            });
        $publications = $this->paginate($query, ['scope' => 'pub']);

        $access_granted = $this->RequestAccess->checkUserRoles([1]);
        $this->set(compact('staff', 'author', 'access_granted', 'publications', 'updateEvents'));
        $this->set('_serialize', 'author');
    }

    public function search()
    {
        $search_key = $this->request->getBody()->getContents();

        $authors = $this->Authors->find('all', [
            'fields' => ['id', 'author'],
            'limit' => 10,
        ])->where([
            'author LIKE' => '%' . $search_key . '%',
        ])->all();
        $authors = json_encode(array_map(function ($author) {
            return [
                'id' => $author['id'],
                'label' => h($author['author']),
                'author' => h($author['author']),
            ];
        }, $authors->toArray()));

        return $this->getResponse()->withStringBody($authors)->withType('json');
    }

    /**
     * Edit method
     *
     * @param string|null $id Author id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit()
    {
        // variable to store the author_id of the current logged in user
        $id = $this->Authentication->getIdentityData('author_id');

        if (!isset($id)) {
            $this->Flash->error('You are not authorized to access that location');

            return $this->redirect(['action' => 'index']);
        }

        $author = $this->Authors->get($id);

        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $data = $this->getRequest()->getData();
            $author = $this->Authors->patchEntity($author, $data);
            if ($this->Authors->save($author)) {
                $this->Flash->success(__('The author details have been updated.'));

                return $this->redirect(['controller' => 'Authors', 'action' => 'view', $id]);
            }
            $this->Flash->error(__('The author details could not be updated. Please, try again.'));
        }
        $this->set(compact('author'));
    }
}
