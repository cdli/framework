<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * @property \App\Model\Table\SponsorsTable $Sponsors
 * @property \App\Model\Table\SponsorTypesTable $SponsorTypes
 * @property \App\Controller\Component\GranularAccessComponent $GranularAccess
 * @method \App\Model\Entity\Sponsor[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SponsorsController extends AppController
{
    /**
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('SponsorTypes');
        $this->loadComponent('GranularAccess');

        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $sponsors = $this->Sponsors->find('all')->order(['ISNULL(sequence)', 'sequence']);

        $sponsor_types = $this->SponsorTypes->find('all');
        $isAdmin = $this->GranularAccess->isAdmin();

        $this->set('sponsors', $sponsors);
        $this->set('sponsor_types', $sponsor_types);
        $this->set('isAdmin', $isAdmin);
        $this->set('_serialize', 'sponsors');
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     */
    public function view($id = null)
    {
        $sponsor = $this->Sponsors->get($id, [
            'contain' => ['SponsorTypes'],
        ]);
        $isAdmin = $this->GranularAccess->isAdmin();
        $this->set('sponsor', $sponsor);
        $this->set('isAdmin', $isAdmin);
        $this->set('_serialize', 'sponsor');
    }
}
