<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;

/**
 * Docs Controller
 *
 * @property \App\Model\Table\DocsTable $Docs
 * @method \App\Model\Entity\Dynasty[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ApiComponent $Api
 */
class DocsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData']]);

        // Set access for public.
        $this->Authentication->allowUnauthenticated(['index', 'view', 'audience', 'category', 'otherDocuments']);
    }

    /**
     * Category method
     *
     * @return \Cake\Http\Response|void
     */
    public function category($category = null)
    {
    }

    /**
     * Audience method
     *
     * @return \Cake\Http\Response|void
     */
    public function audience($audience = null)
    {
        if ($audience == ('visitor' || 'content_contributor' || 'organization' || 'developer')) {
            $docs_dir = new Folder(WWW_ROOT . 'cdli-docs');
            $doc_files = $docs_dir->find('.*\.md');
            $doc_heads = [];
            foreach ($doc_files as $index => $value) {
                $call_file = new File(WWW_ROOT . 'cdli-docs' . DS . $doc_files[$index]);
                $file_content = $call_file->read();
                $doc_heads[$index] = yaml_parse($file_content);
                $doc_heads[$index]['file_name'] = $doc_files[$index];
                $doc_heads[$index]['link_part'] = substr($doc_files[$index], 0, -3);
                unset($doc_files[$index]);
            }
            $this->set(compact('audience', 'doc_heads'));
        } else {
            $this->redirect([
                'controller' => 'Docs',
                'action' => 'index']);
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */

    public function index()
    {
        $docs_dir = new Folder(WWW_ROOT . 'cdli-docs');
        $doc_files = $docs_dir->find('.*\.md');
        $doc_heads = [];
        $sections = [];
        $categories = [];
        foreach ($doc_files as $index => $value) {
            $call_file = new File(WWW_ROOT . 'cdli-docs' . DS . $doc_files[$index]);
            $file_content = $call_file->read();
            $doc_heads[$index] = yaml_parse($file_content);
            $doc_heads[$index]['file_name'] = $doc_files[$index];
            $doc_heads[$index]['link_part'] = substr($doc_files[$index], 0, -3);
            $sections[] = $doc_heads[$index]['section'];
            unset($doc_files[$index]);
        }
        $sections = array_unique($sections);
        $this->set(compact('doc_heads', 'sections'));
    }

    /**
     * View method
     *
     * @param string|null $doc_name document filename
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($doc_name = null)
    {
        $callFile = new File(WWW_ROOT . 'cdli-docs' . DS . $doc_name . '.md');
        if ($callFile->exists()) {
            $doc = $callFile->read();
            $doc_body = preg_replace('/---\n(.*?\n)*?---/', '', $doc, 1);
            $doc_body = preg_replace('/~~~conllu /', '<div class="conllu-parse" tabs="no">', $doc_body);
            $doc_body = preg_replace('/~~~ conllu/', '<div class="conllu-parse" tabs="no">', $doc_body);
            $doc_body = preg_replace('/~~~/', '</div>', $doc_body);
            $head = yaml_parse($doc);
        } else {
            throw new RecordNotFoundException("$callFile->path not found");
        }
        $this->set(compact('head', 'doc_body'));
    }

    public function otherDocuments($document, $version)
    {
        $layout = null;
        if ($this->Api->requestsApi()) {
            $layout = '';
        }

        $this->render($document . DS . $version, $layout);
    }
}
