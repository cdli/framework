<?php
declare(strict_types=1);

namespace App\Controller;

class TermsController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();

        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function view()
    {
        $parts = array_slice(explode('/', $this->getRequest()->getPath()), 2, 4);
        $hash = count($parts) === 2 ? implode('_', $parts) : '';

        return $this->redirect(['action' => 'index', '#' => $hash]);
    }
}
