<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

/**
 * Archives Controller
 *
 * @property \App\Model\Table\ArchivesTable $Archives
 * @method \App\Model\Entity\Archive[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ApiComponent $Api
 */
class ArchivesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);
        $this->loadComponent('Search');

        // Set access for public.
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'sortableFields' => ['archive'],
            'limit' => 1000,
            'maxLength' => 1000,
            'contain' => ['Proveniences'],
        ] + $this->paginate;

        $query = $this->Archives->find('search', [
            'search' => $this->request->getQueryParams(),
        ]);

        $archives = $this->paginate($query);

        $access_granted = $this->RequestAccess->checkUserRoles([1, 2]);

        $this->set(compact('archives', 'access_granted'));
        $this->set('_serialize', 'archives');
    }

    /**
     * View method
     *
     * @param string|null $id Archive id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $archive = $this->Archives->get($id, [
            'contain' => [
                'Proveniences',
                'Artifacts' => ['Languages', 'Periods'],
            ],
        ]);

        $this->set('canEdit', $this->RequestAccess->checkUserRoles([1, 2]));
        $this->set('archive', $archive);
        $this->set('_serialize', 'archive');
    }
}
