<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

use Cake\I18n\Time;
use Cake\Routing\Router;

/**
 * @property \App\Model\Table\CdliTabletTable $CdliTablet
 * @method \App\Model\Entity\CdliTablet[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CdliTabletController extends AppController
{
    /**
     * Initialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadModel('CdliTablet');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Search');

        $this->Authentication->allowUnauthenticated(['index', 'view', 'feed']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'limit' => 12,
        ];

        $query = $this->CdliTablet->find('search', [
                'search' => $this->request->getQueryParams(),
            ])
            ->contain(['Authors']);
        $results = $this->paginate($query);
        $this->set('results', $results);

        // JSON response
        $dailytablets = $this->CdliTablet->find('all', [
            'conditions ' => ['date_display <=' => Time::now()],
        ])->contain(['Authors']);

        // return JSON
        $this->set('dailytablets', $dailytablets);
        $this->set('_serialize', 'dailytablets');
    }

    public function feed()
    {
        $thumbnailOnly = false;

        $results = $this->CdliTablet->find()->orderDesc('date_display');
        if ($this->getRequest()->getQuery('all') === 'true') {
        } elseif (!is_null($this->getRequest()->getQuery('todaysdate'))) {
            $results = $results->limit(10)->where([
                'date_display <=' => $this->getRequest()->getQuery('todaysdate'),
            ]);
        } elseif (!is_null($this->getRequest()->getQuery('specificdate'))) {
            $results = $results->limit(10)->where([
                'date_display' => $this->getRequest()->getQuery('specificdate'),
            ]);
        } elseif (!is_null($this->getRequest()->getQuery('thumbnaildate'))) {
            $thumbnailOnly = true;
            $results = $results->where(['date_display <=' => Time::now()]);
        } else {
            $results = $results->limit(10)->where(['date_display <=' => Time::now()]);
        }

        $data = [];
        foreach ($results->all() as $result) {
            $imagePathParts = pathinfo($result->image_filename);
            $imageThumbnail = $imagePathParts['filename'] . '_thumbnail' . '.' . $imagePathParts['extension'];
            if (!file_exists(WWW_ROOT . 'pubs/daily_tablets/' . $imageThumbnail)) {
                $imageThumbnail = $result->image_filename;
            }

            if ($thumbnailOnly) {
                $entry = [
                    'date' => $result->date_display,
                    'thumbnail-url' => Router::url('pubs/daily_tablets/' . $imageThumbnail, true),
                ];
            } else {
                $entry = [
                    'date' => $result->date_display,
                    'thumbnail-url' => Router::url('pubs/daily_tablets/' . $imageThumbnail, true),
                    'url' => Router::url('pubs/daily_tablets/' . $result->image_filename, true),
                    'blurb-title' => $result->theme . ': ' . $result->title_short,
                    'theme' => $result->theme,
                    'blurb' => $result->text_short,
                    'full-title' => $result->theme . ': ' . $result->title_short,
                    'full-info' => $result->text_long,
                ];
            }

            $data[] = $entry;
        }

        return $this->getResponse()
            ->withType('json')
            ->withStringBody(json_encode($data));
    }

    /**
     * View method
     */
    public function view($id)
    {
        if ($id === 'feed') {
            return $this->setAction('feed');
        }

        $entry = $this->CdliTablet->get($id, [
            'contain' => 'Authors',
        ]);
        $this->set('entry', $entry);
    }
}
