<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * @property \App\Controller\Component\ApiComponent $Api
 * @property \App\Controller\Component\ChangesetsComponent $Changesets
 * @property \App\Controller\Component\GranularAccessComponent $GranularAccess
 * @property \App\Controller\Component\PaginatorComponent $Paginator
 * @property \App\Controller\Component\SearchAccessComponent $Search
 * @property \App\Model\Table\EntitiesUpdatesTable $EntitiesUpdates
 * @property \App\Model\Table\ProveniencesTable $Proveniences
 * @property \App\Model\Table\UpdateEventsTable $UpdateEvents
 * @method \App\Model\Entity\Provenience[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProveniencesController extends AppController
{
    /**
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);
        $this->loadComponent('Changesets');
        $this->loadComponent('GranularAccess');
        $this->loadComponent('Paginator');
        $this->loadComponent('Search');
        $this->loadModel('EntitiesUpdates');
        $this->loadModel('UpdateEvents');

        // Set access for public.
        $this->Authentication->allowUnauthenticated(['index', 'view', 'history', 'add', 'edit']);
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'sortableFields' => ['provenience', 'region_id'],
            'contain' => ['Regions', 'Archives', 'Dynasties'],
        ] + $this->paginate;

        $query = $this->Proveniences->find('search', [
            'search' => $this->request->getQueryParams(),
        ]);

        $this->set('proveniences', $this->paginate($query));
        $this->set('access_granted', $this->GranularAccess->canSubmitEdits());
        $this->set('_serialize', 'proveniences');
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $provenience = $this->Proveniences->get($id, [
            'contain' => [
                'Locations' => ['EntitiesNames', 'ExternalResources'],
                'Places' => ['ExternalResources'],

                'Regions',
                'Archives',
                'Dynasties',

                'Publications',
                'Publications.Authors' => [
                    'sort' => ['AuthorsPublications.sequence' => 'ASC'],
                ],
                'Publications.Editors' => [
                    'sort' => ['EditorsPublications.sequence' => 'ASC'],
                ],
                'Publications.EntryTypes',
                'Publications.Journals',
            ],
        ]);

        $this->set('provenience', $provenience);
        $this->set('count', $this->Proveniences->Artifacts->find()->where(['provenience_id' => $id])->count());
        $this->set('canEdit', $this->GranularAccess->canSubmitEdits());
        $this->set('_serialize', 'provenience');
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function history($id = null)
    {
        $provenience = $this->Proveniences->get($id, ['contain' => ['Locations', 'Places']]);
        $updateEvents = $this->UpdateEvents->find()
            ->where(['UpdateEvents.status' => 'approved'])
            ->matching('EntitiesUpdates', function ($q) use ($provenience) {
                $conditions = [
                    ['entity_table' => 'proveniences', 'entity_id' => $provenience->id],
                ];
                if ($provenience->has('place_id')) {
                    $conditions[] = ['entity_table' => 'places', 'entity_id' => $provenience->place_id];
                }
                if ($provenience->has('location_id')) {
                    $conditions[] = ['entity_table' => 'locations', 'entity_id' => $provenience->location_id];
                }

                return $q->where(['OR' => $conditions]);
            })
            ->contain([
                'Creators',
                'Authors',
            ])
            ->order(['UpdateEvents.approved' => 'DESC'])
            ->all()
            ->toArray();

        $this->set('provenience', $provenience);
        $this->set('updateEvents', $updateEvents);
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function add()
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error('Only logged in users who have an author associated to their account can edit. Create an account and / or  contact cdli@ames.ox.ac.uk to associate an author with your account.');

            return $this->redirect(['action' => 'index']);
        }

        $provenience = $this->Proveniences->newEmptyEntity();
        $this->_handleAddEdit($provenience);
        $this->render('edit');
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id)
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error('Only logged in users who have an author associated to their account can edit. Create an account and / or  contact cdli@ames.ox.ac.uk to associate an author with your account.');

            return $this->redirect(['action' => 'view', $id]);
        }

        $provenience = $this->Proveniences->get($id, ['contain' => ['Locations', 'Places', 'Regions', 'Publications']]);
        $this->_handleAddEdit($provenience);
    }

    /**
     * @param \App\Model\Entity\Provenience $provenience
     * @return \Cake\Http\Response|void
     */
    private function _handleAddEdit($provenience)
    {
        $this->Changesets->handleBlockingChangeset();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $data = $this->getRequest()->getData();
            foreach ($data as &$value) {
                if (is_array($value) && array_key_exists('template', $value)) {
                    unset($value['template']);
                }
            }
            $this->Proveniences->patchEntity($provenience, $data);
            $update = $this->EntitiesUpdates->newEntityFromChangedEntity($provenience);

            $changeset = $this->Changesets->create(['other_entity'], [$update]);
            $this->Changesets->set($changeset);
            $this->Changesets->setBlockingChangeset($changeset);
            $this->redirect(['controller' => 'Changesets', 'action' => 'view', $changeset['id']]);
        }

        $regions = $this->Proveniences->Regions->find('list')->toArray();
        $places = $this->Proveniences->Places->find('list')->toArray();
        $locations = $this->Proveniences->Locations->find('list')->toArray();

        $this->set('provenience', $provenience);
        $this->set('regions', $regions);
        $this->set('places', $places);
        $this->set('locations', $locations);
    }
}
