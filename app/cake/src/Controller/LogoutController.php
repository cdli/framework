<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LogoutController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if ($this->getRequest()->is(['post'])) {
            $this->getRequest()->getSession()->delete('session_verified');
            $this->Flash->success('You are now logged out.');

            return $this->redirect($this->Authentication->logout());
        }
    }

    public function view()
    {
        if ($this->getRequest()->is(['post'])) {
            return $this->redirect('/');
        }
    }
}
