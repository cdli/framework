<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\EntitiesUpdate;
use App\Model\Entity\Publication;
use App\Utility\BulkUpload\BulkUploadFile;
use App\Utility\BulkUpload\BulkUploadPublicationLinks;

/**
 * @property \App\Controller\Component\ChangesetsComponent $Changesets
 * @property \App\Controller\Component\GranularAccessComponent $GranularAccess
 * @property \App\Model\Table\ArtifactsUpdatesTable $ArtifactsUpdates
 * @property \App\Model\Table\EntitiesPublicationsTable $EntitiesPublications
 * @property \App\Model\Table\EntitiesUpdatesTable $EntitiesUpdates
 * @method \App\Model\Entity\EntitiesUpdate[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EntitiesPublicationsController extends AppController
{
    /**
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Changesets');
        $this->loadComponent('GranularAccess');
        $this->loadModel('ArtifactsUpdates');
        $this->loadModel('EntitiesUpdates');
        $this->Authentication->allowUnauthenticated(['add']);
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function add()
    {
        $this->Changesets->handleBlockingChangeset();

        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error('Only logged in users who have an author associated to their account can edit text. Create an account and / or  contact cdli@ames.ox.ac.uk to associate an author with your account.');

            return $this->redirect($this->referer());
        }

        if (!$this->getRequest()->is(['post', 'put', 'patch'])) {
            return;
        }

        $file = $this->getRequest()->getData('upload');
        $fileType = $file->getClientMediaType();

        if ($fileType === 'text/csv') {
            $data = BulkUploadFile::parseCsv($file);
        }

        if (!isset($data) || !is_array($data) || count($data) === 0) {
            $this->Flash->error('Expected a non-empty CSV file');

            return;
        }

        $validTables = array_flip([
            'artifacts',
            ...array_keys(Publication::$topicAssociations),
            ...array_keys(EntitiesUpdate::$dependentAssociations),
        ]);

        $updates = [];
        $updateTypes = [];

        $links = [];
        foreach ($data as $link) {
            if (!array_key_exists('entity_table', $link)) {
                $updates[] = $this->EntitiesUpdates->newEntityFromError('entity_table', 'No "entity_table" column');
                continue;
            }

            if (!array_key_exists($link['entity_table'], $validTables)) {
                $updates[] = $this->EntitiesUpdates->newEntityFromError('entity_table', 'Invalid "entity_table" value');
                continue;
            }

            if (!array_key_exists('bibtexkey', $link)) {
                $updates[] = $this->EntitiesUpdates->newEntityFromError('publication_id', 'No "bibtexkey" column');
                continue;
            }

            if (!array_key_exists('entity_id', $link)) {
                $updates[] = $this->EntitiesUpdates->newEntityFromError('entity_id', 'No "entity_id" column');
                continue;
            }

            $links[$link['entity_table']][$link['entity_id']][] = $link;
        }

        foreach ($links as $entityTable => $entities) {
            foreach ($entities as $entityId => $entityLinks) {
                $entityLinks = new BulkUploadPublicationLinks($entityTable, $entityId, $entityLinks);

                if ($entityTable === 'artifacts') {
                    $updateTypes['artifact'] = true;

                    /** @var \App\Model\Entity\Artifact $entity */
                    $entity = $entityLinks->entity;

                    $originalValues = [];
                    foreach (['publications_key', 'publications_type', 'publications_exact_ref', 'publications_comment'] as $field) {
                        $originalValues[$field] = $entity->getFlatValue($field);
                    }

                    $entityLinks->apply();
                    $update = $this->ArtifactsUpdates->newEmptyEntity();
                    $update->set('artifact_id', $entity->id);
                    foreach ($originalValues as $field => $originalValue) {
                        $value = $entity->getFlatValue($field);
                        if ($value !== $originalValue) {
                            $update->set($field, $value);
                        }
                    }
                    $updates[] = $update;
                } else {
                    $updateTypes['other_entity'] = true;

                    $entity = $entityLinks->apply();
                    $updates[] = $this->EntitiesUpdates->newEntityFromChangedEntity($entity);
                }
            }
        }

        $changeset = $this->Changesets->create(array_keys($updateTypes), $updates);
        $this->Changesets->set($changeset);
        $this->Changesets->setBlockingChangeset($changeset);
        $this->redirect(['controller' => 'Changesets', 'action' => 'view', $changeset['id']]);
    }
}
