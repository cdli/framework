<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

use Cake\ORM\TableRegistry;

/**
 * ArtifactTypes Controller
 *
 * @property \App\Model\Table\ArtifactTypesTable $ArtifactTypes
 * @method \App\Model\Entity\ArtifactType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ApiComponent $Api
 */
class ArtifactTypesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);
        $this->loadComponent('Search');

        // Set access for public.
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'limit' => 1000,
            'maxLimit' => 1000,
            'sortableFields' => ['artifact_type', 'parent_id'],
            'contain' => ['ParentArtifactTypes'],
        ] + $this->paginate;

        $query = $this->ArtifactTypes->find('search', [
            'search' => $this->request->getQueryParams(),
        ]);

        $artifactTypes = $this->paginate($query);
        $access_granted = $this->RequestAccess->checkUserRoles([1]);
        $this->set(compact('artifactTypes', 'access_granted'));
        $this->set('_serialize', 'artifactTypes');
    }

    /**
     * View method
     *
     * @param string|null $id Artifact Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactType = $this->ArtifactTypes->get($id, [
            'contain' => ['ParentArtifactTypes', 'ChildArtifactTypes'],
        ]);

        $artifacts = TableRegistry::get('Artifacts');
        $count = $artifacts->find('list', ['conditions' => ['artifact_type_id' => $id]])->count();
        $access_granted = $this->RequestAccess->checkUserRoles([1]);

        $this->set(compact('artifactType', 'count', 'access_granted'));
        $this->set('_serialize', 'artifactType');
    }
}
