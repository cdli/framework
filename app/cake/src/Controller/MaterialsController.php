<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

/**
 * Materials Controller
 *
 * @property \App\Model\Table\MaterialsTable $Materials
 * @method \App\Model\Entity\Material[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Model\Table\ArtifactsMaterialsTable $ArtifactsMaterials
 * @property \App\Controller\Component\ApiComponent $Api
 */
class MaterialsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);
        $this->loadComponent('Search');

        // Set access for public.
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'limit' => 1000,
            'maxLimit' => 1000,
            'contain' => ['ParentMaterials', 'ChildMaterials'],
        ] + $this->paginate;

        $query = $this->Materials->find('search', [
            'search' => $this->request->getQueryParams(),
        ]);

        $materials = $this->paginate($query);
        $access_granted = $this->RequestAccess->checkUserRoles([1]);

        $this->set(compact('materials', 'access_granted'));
        $this->set('_serialize', 'materials');
    }

    /**
     * View method
     *
     * @param string|null $id Material id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $material = $this->Materials->get($id, [
            'contain' => ['ParentMaterials', 'ChildMaterials'],
        ]);

        $artifacts = $this->loadModel('ArtifactsMaterials');
        $count = $artifacts->find('list', ['conditions' => ['material_id' => $id]])->count();
        $access_granted = $this->RequestAccess->checkUserRoles([1]);

        $this->set(compact('material', 'count', 'access_granted'));
        $this->set('_serialize', 'material');
    }
}
