<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\UnauthorizedException;

/**
 * ArtifactsExternalResources Controller
 *
 * @property \App\Model\Table\ArtifactsExternalResourcesTable $ArtifactsExternalResources
 * @method \App\Model\Entity\ArtifactsExternalResource[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ApiComponent $Api
 * @property \App\Controller\Component\GranularAccessComponent $GranularAccess
 */
class ArtifactsExternalResourcesController extends AppController
{
    /**
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData']]);
        $this->loadComponent('GranularAccess');
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if (!$this->Api->requestsFormat(['LinkedData'])) {
            $this->redirect(['controller' => 'Search', 'action' => 'index']);
        }

        $this->paginate = [
            'limit' => 1000,
            'maxLimit' => 1000,
            'contain' => ['Artifacts', 'ExternalResources'],
        ] + $this->paginate;

        $artifactsExternalResources = $this->ArtifactsExternalResources->find();

        if (!$this->GranularAccess->canViewPrivateArtifact()) {
            $artifactsExternalResources = $artifactsExternalResources
                ->matching('Artifacts', function ($q) {
                    return $q->where(['is_public' => true]);
                });
        }

        $artifactsExternalResources = $this->paginate($artifactsExternalResources);

        $this->set(compact('artifactsExternalResources'));
        $this->set('_serialize', 'artifactsExternalResources');
    }

    /**
     * View method
     *
     * @param string|null $id Artifacts External Resource id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactsExternalResource = $this->ArtifactsExternalResources->get($id, [
            'contain' => ['Artifacts', 'ExternalResources'],
        ]);

        if (!$this->GranularAccess->canViewPrivateArtifact() && !$artifactsExternalResource->artifact->is_public) {
            throw new UnauthorizedException();
        }

        if (!$this->Api->requestsFormat(['LinkedData'])) {
            $this->redirect(['controller' => 'Artifacts', 'action' => 'view', $artifactsExternalResource->artifact_id]);
        }

        $this->set('artifactsExternalResource', $artifactsExternalResource);
        $this->set('_serialize', 'artifactsExternalResource');
    }
}
