<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * @property \App\Controller\Component\ApiComponent $Api
 * @property \App\Controller\Component\ChangesetsComponent $Changesets
 * @property \App\Model\Table\EntitiesUpdatesTable $EntitiesUpdates
 * @property \App\Model\Table\ExternalResourcesTable $ExternalResources
 * @property \App\Model\Table\LocationsTable $Locations
 * @property \App\Model\Table\PlacesTable $Places
 * @property \App\Model\Table\PeriodsTable $Periods
 * @method \App\Model\Entity\Place[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PlacesController extends AppController
{
    /**
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData']]);
        $this->loadComponent('Changesets');
        $this->loadComponent('GranularAccess');
        $this->loadModel('EntitiesUpdates');
        $this->loadModel('ExternalResources');
        $this->loadModel('Locations');
        $this->loadModel('Periods');
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'sortableFields' => ['place'],
            'limit' => 1000,
            'maxLength' => 1000,
            'contain' => ['ExternalResources'],
        ] + $this->paginate;
        $places = $this->paginate($this->Places);

        $this->set('places', $places);
        $this->set('_serialize', 'places');
    }

    /**
     * @param string|null $id Archive id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $place = $this->Places->get($id, ['contain' => ['ExternalResources']]);

        $this->set('place', $place);
        $this->set('_serialize', 'place');
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function add()
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error('Only logged in users who have an author associated to their account can edit. Create an account and / or  contact cdli@ames.ox.ac.uk to associate an author with your account.');

            return $this->redirect(['action' => 'index']);
        }

        $place = $this->Places->newEmptyEntity();
        $this->_handleAddEdit($place);
        $this->render('edit');
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id)
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error('Only logged in users who have an author associated to their account can edit. Create an account and / or  contact cdli@ames.ox.ac.uk to associate an author with your account.');

            return $this->redirect(['action' => 'view', $id]);
        }

        $place = $this->Places->get($id, ['contain' => ['ExternalResources']]);
        $this->_handleAddEdit($place);
    }

    /**
     * @param \App\Model\Entity\Place $place
     * @return \Cake\Http\Response|void
     */
    private function _handleAddEdit($place)
    {
        $this->Changesets->handleBlockingChangeset();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $data = $this->getRequest()->getData();
            foreach ($data as &$value) {
                if (is_array($value) && array_key_exists('template', $value)) {
                    unset($value['template']);
                }
            }
            $this->Places->patchEntity($place, $data);
            $update = $this->EntitiesUpdates->newEntityFromChangedEntity($place);

            $changeset = $this->Changesets->create(['other_entity'], [$update]);
            $this->Changesets->set($changeset);
            $this->Changesets->setBlockingChangeset($changeset);
            $this->redirect(['controller' => 'Changesets', 'action' => 'view', $changeset['id']]);
        }

        $externalResources = $this->ExternalResources->find('list')->order('external_resource')->toArray();
        $locations = $this->Locations->find('list')->order('location')->toArray();
        // $periods = $this->Periods->find('list')->order('sequence')->toArray();

        $this->set('place', $place);
        $this->set('externalResources', $externalResources);
        $this->set('locations', $locations);
        // $this->set('periods', $periods);
    }
}
