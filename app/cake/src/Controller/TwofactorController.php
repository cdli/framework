<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

use Cake\Core\Configure;
use Cake\I18n\Time;

/**
 * TwofactorController Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @property \App\Model\Table\RolesUsersTable $RolesUsers
 * @property \App\Controller\Component\GoogleAuthenticatorComponent $GoogleAuthenticator
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TwofactorController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Set access for public view.
        $this->Authentication->allowUnauthenticated(['index']);
    }

    /**
     * Index method (2FA Setup Method)
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // Get Session
        $session = $this->getRequest()->getSession();

        // Check if session 'user' is present then execute next logic. If not then redirect to '/'
        if ($session->read('user.user') != null) {
            $session_user = $session->read('user');
            // If present session is verified then redirect to '/'
            if ($session->read('session_verified') != null) {
                return $this->redirect([
                    'controller' => 'Home',
                    'action' => 'index',
                ]);
            } else {
                // Check if 2FA Enabled or not. If enabled redirect to verify page or redirect to 2FA setup page.
                $redirectToVerify = $session_user['type'] === 'login' && $session_user['user']['2fa_status'];

                if ($redirectToVerify) {
                    $this->setAction('twofaverify');
                } else {
                    $this->loadModel('Users');
                    $this->loadComponent('GoogleAuthenticator');
                    $ga = $this->GoogleAuthenticator;

                    if ($this->getRequest()->is('post')) {
                        $secret = $this->getRequest()->getData('secretcode');
                        $oneCode = $this->getRequest()->getData('code');
                        $checkResult = $ga->verifyCode($secret, $oneCode, 2); // 2 = 2*30sec clock tolerance

                        // For Development mode
                        $ifDevelopment = (bool)(env('APP_ENV') === 'development');

                        if ($checkResult || $ifDevelopment) {
                            $time_now = Time::now();

                            // If requested from Login
                            if ($session_user['type'] === 'login') {
                                $updateUser['2fa_key'] = $this->getRequest()->getData('secretcode');
                                $updateUser['2fa_status'] = 1;
                                $updateUser['last_login_at'] = $time_now;
                                $updateUser['modified_at'] = $time_now;

                                $retrivedUser = $this->Users->find('all', [
                                    'conditions' => [
                                        'Users.username' => $session_user['user']['username'],
                                    ],
                                ])->first();

                                $saveUser = $this->Users->patchEntity($retrivedUser, $updateUser);
                            } else {
                                $modifiedData['2fa_key'] = $this->getRequest()->getData('secretcode');
                                $modifiedData['2fa_status'] = 1;
                                $modifiedData['last_login_at'] = $time_now;
                                $modifiedData['modified_at'] = $time_now;
                                $modifiedData['active'] = 1;
                                $modifiedData['created_at'] = $session_user['user']['created_at'];

                                $saveUser = $this->Users->patchEntity($session_user['user'], $modifiedData, ['validate' => false]);

                                if (!empty($saveUser->getErrors())) {
                                    foreach ($saveUser->getErrors() as $error) {
                                        foreach ($error as $key => $value) {
                                            $this->Flash->error($value);
                                        }
                                    }

                                    return $this->redirect([
                                        'controller' => 'Users',
                                        'action' => 'register',
                                    ]);
                                }
                            }

                            if ($this->Users->save($saveUser)) {
                                if ($session->read('user.email') == null) {
                                    // When User Logs in
                                    $this->AuthenticateUsersRole($session_user['user']['username']);
                                } else {
                                    // When User Registers
                                    $this->AuthenticateUsersRole($session_user['user']['username'], 1);
                                }

                                $session->delete('user');

                                $this->Flash->success(__('Two-Factor Authentication (2FA) Is Enabled.'), ['class' => 'alert alert-danger']);

                                return $this->redirect([
                                    'controller' => 'Home',
                                    'action' => 'index',
                                ]);
                            } else {
                                return $this->Flash->error(__('Please try again.'), ['class' => 'alert alert-danger']);
                            }
                        } else {
                            return $this->Flash->error(__('Wrong code entered.Please try again.'), ['class' => 'alert alert-danger']);
                        }
                    }
                }
            }
        } else {
            return $this->redirect([
                'controller' => 'Home',
                'action' => 'index',
            ]);
        }
    }

    /**
     * 2FA Verify method
     *
     * @return \Cake\Http\Response|void
     */
    public function twofaverify()
    {
        $data = $this->request->getQueryParams();

        $this->loadComponent('GoogleAuthenticator');
        $this->loadModel('Users');
        $ga = $this->GoogleAuthenticator;
        $user = $this->getRequest()->getSession()->read('user.user');

        // Remove the following line and $ifDevelopment variable while checking result (Line no. 188) in production.
        $ifDevelopment = (bool)(Configure::read('app_env') === 'development');

        if ($this->getRequest()->is('post')) {
            $secret = $this->get2faKey($user['username']);
            $oneCode = $this->getRequest()->getData('code');
            $checkResult = $ga->verifyCode($secret, $oneCode, 2);

            if ($checkResult || $ifDevelopment) {
                $updateUser['last_login_at'] = Time::now();

                $retrivedUser = $this->getUser($user['username']);

                $saveUser = $this->Users->patchEntity($retrivedUser, $updateUser);

                if (!$saveUser->hasErrors() && $this->Users->save($saveUser)) {
                    $this->AuthenticateUsersRole($user['username']);
                    $this->getRequest()->getSession()->delete('user');
                    if (isset($data['redirect'])) {
                        return $this->redirect('./' . $data['redirect']);
                    } else {
                        return $this->redirect([
                            'controller' => 'Home',
                            'action' => 'index',
                        ]);
                    }
                } else {
                    $this->getRequest()->getSession()->delete('user');
                    $this->Flash->error(__('Error on Server Side !!'));

                    return $this->redirect(['controller' => 'Home', 'action' => 'index']);
                }
            } else {
                $this->Flash->error(__('Wrong code entered.Please try again.'), [
                    'class' => 'alert alert-danger',
                ]);
            }
        }
    }

    // Retrive user from DB

    public function getUser($username)
    {
        $this->loadModel('Users');
        $user = $this->Users->find(
            'all',
            [
            'conditions' => [
                'Users.username' => $username],
            ]
        )->first();

        return $user;
    }

    // Authenticates User's Login or Registers.

    public function AuthenticateUser($user, $roles)
    {
        $this->getRequest()->getSession()->write('session_verified', 1);

        $user['roles'] = $roles;

        $this->Authentication->setIdentity($user);
    }

    // Returns 2FA key for specific username

    public function get2faKey($username)
    {
        $user = $this->getUser($username);

        return $user['2fa_key'];
    }

    /**
     * Set/Get User's Role method on successful GET, Authenticate User
     *
     * SET Role (Registration) and GET Role (Registration , Login)
     *
     * @param : $username $type
     * username -> Username
     * type -> 1 for SET
     * @return : void
     */
    public function AuthenticateUsersRole($username, $type = null)
    {
        $user = $this->getUser($username);

        // SET User Role
        if ($type == 1) {
            // By default setting it to empty array
            $roles = [];
        } else {
            // GET User Role
            $roles = $this->getTableLocator()->get('RolesUsers')->getUserRoles($user['id']);
        }
        $this->AuthenticateUser($user, $roles);
    }

    /**
     * Set Roles
     *
     * @param : $userId $toBeSet, $toBeRemoved
     * userId -> User ID
     * toBeSet -> Roles to be added (array)
     * toBeRemoved -> Roles to be removed (array)
     * @return : array of roles
     */
    public function setUsersRole($userId, $toBeSet = null, $toBeRemoved = null)
    {
        $this->loadModel('RolesUsers');

        if (!is_null($toBeRemoved)) {
            if (!$this->RolesUsers->deleteAll(['user_id' => $userId, 'role_id IN' => $toBeRemoved])) {
                $this->Flash->error(__('Error in setting Roles!'));
            }
        }

        if (!is_null($toBeSet)) {
            $userRole = [];

            foreach ($toBeSet as $role_id) {
                array_push($userRole, ['user_id' => $userId, 'role_id' => $role_id]);
            }

            if (!$this->RolesUsers->saveMany($this->RolesUsers->newEntities($userRole))) {
                $this->Flash->error(__('Error in setting Roles!'));
            }
        }

        return $this->RolesUsers->getUserRoles($userId);
    }
}
