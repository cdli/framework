<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\ArtifactsUpdate;
use App\Model\Entity\Inscription;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * @method \App\Model\Entity\UpdateEvent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Model\Table\ArtifactsTable $Artifacts
 * @property \App\Controller\Component\ChangesetsComponent $Changesets
 * @property \App\Controller\Component\GranularAccessComponent $GranularAccess
 */
class ChangesetsController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Artifacts');
        $this->loadComponent('Changesets');
        $this->loadComponent('GranularAccess');
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error($this->RequestAccess->getConfig('authError'));

            return $this->redirect('/');
        }

        $this->set('changesets', $this->Changesets->getAll());
        $this->set('blockingChangeset', $this->Changesets->getBlockingChangeset());
    }

    /**
     * @param string|null $id
     * @return \Cake\Http\Response|void
     */
    public function view($id = null)
    {
        if (!$this->GranularAccess->canSubmitEdits()) {
            $this->Flash->error($this->RequestAccess->getConfig('authError'));

            return $this->redirect('/');
        }

        try {
            $changeset = $this->Changesets->get($id);
        } catch (RecordNotFoundException $error) {
            $this->Flash->error(__('This changeset no longer exists'));

            return $this->redirect(['action' => 'index']);
        }

        foreach ($changeset['updates'] as $update) {
            if ($update instanceof ArtifactsUpdate && $update->has('artifact_id')) {
                $update->artifact = $this->Artifacts->get($update->artifact_id, [
                    'contain' => [
                        'Archives',
                        'ArtifactsShadow',
                        'ArtifactTypes',
                        'Collections',
                        'Composites',
                        'ExternalResources',
                        'Genres',
                        'Languages',
                        'Materials',
                        'Origins',
                        'Periods',
                        'Proveniences',
                        'Publications',
                        'Seals',
                    ],
                ]);
            }

            if ($update instanceof Inscription && $update->has('artifact_id')) {
                $update->artifact = $this->Artifacts->get($update->artifact_id, ['contain' => ['Inscriptions']]);
            }
        }

        $this->set('changeset', $changeset);
        $this->set('redraftUrl', $this->Changesets->getRedraftUrl($changeset));

        if ($changeset['hasErrors']) {
            $this->render('errors');
        } else {
            $this->set('isBlocking', $changeset['id'] === $this->Changesets->getBlockingChangeset());
            $this->set('redirect', $this->getRequest()->getQuery('redirect'));
        }
    }

    /**
     * @return \Cake\Http\Response|void
     */
    public function add()
    {
        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $action = $this->getRequest()->getData('action', 'none');
            $included = array_filter($this->getRequest()->getData('include', []), function (string $value) {
                return $value === '1';
            });

            if ($action === 'unblock') {
                $this->Changesets->setBlockingChangeset(null);
                $this->Flash->success(__('To finish uploading your changes, click the red notification in the top right.'));

                return $this->redirect($this->getRequest()->getQuery('redirect') ?? '/');
            }

            if ($action === 'submit') {
                return $this->redirect([
                    'controller' => 'UpdateEvents',
                    'action' => 'add',
                    '?' => ['changesets' => array_keys($included)],
                ]);
            }

            $changesets = $this->Changesets->getAll();
            $excludedChangesets = array_diff_key($changesets, $included);

            if ($action === 'delete') {
                $this->Flash->success(__('Change(s) discarded.'));
                $this->Changesets->setAll($excludedChangesets);

                if (count($excludedChangesets) === 0) {
                    return $this->redirect('/');
                }
            }
        }

        return $this->redirect(['action' => 'index']);
    }
}
