<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;

/**
 * Articles Controller
 *
 * @method \App\Model\Entity\Article[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Model\Table\ArticlesTable $Articles
 * @property \App\Model\Table\OjsUserSettingsTable $OjsUserSettings
 * @property \App\Model\Table\OjsSubmissionCommentsTable $OjsSubmissionComments
 * @property \App\Model\Table\OjsReviewAssignmentsTable $OjsReviewAssignments
 * @property \App\Model\Table\OjsReviewRoundFilesTable $OjsReviewRoundFiles
 * @property \App\Controller\Component\ApiComponent $Api
 * @property \App\Controller\Component\GranularAccessComponent $GranularAccess
 */
class ArticlesController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Api', ['features' => ['Bibliography']]);
        $this->loadComponent('GranularAccess');
        $this->loadModel('OjsUserSettings');
        $this->loadModel('OjsSubmissionComments');
        $this->loadModel('OjsReviewAssignments');
        $this->loadModel('OjsReviewRoundFiles');
        $this->loadComponent('Search');

        $this->Authentication->allowUnauthenticated(['index', 'view', 'image']);
    }

    public function index($type)
    {
        $this->paginate = ['sortableFields' => ['title', 'date']] + $this->paginate;
        $access_granted = $this->RequestAccess->checkUserRoles([1, 12]);

        $query = $this->Articles->find('search', [
                'search' => $this->request->getQueryParams(),
            ])
            ->where(['article_type' => $type])
            ->contain(['Authors']);

        if ($type === 'cdlp') {
            $query->order([
                'natural_sort_key(Articles.article_no)' => 'DESC',
            ]);
        }

        $allArticles = $this->paginate($query);

        $articles = [];
        foreach ($allArticles as $article) {
            if ($this->GranularAccess->canViewArticle($article)) {
                $articles[] = $article;
            }
        }

        $this->set(compact('articles', 'type', 'access_granted'));
        $this->set('_serialize', 'articles');
    }

    public function view($type, $id)
    {
        $article = $this->Articles->getByNumber($type, $id, [
            'contain' => [
                'Authors',
                'OjsReviewRounds',
                'OjsReviewRounds.OjsReviewAssignments',
                'OjsQueries',
                'OjsQueries.OjsNotes',
            ],
        ]);
        $ojs_file = null;
        $reviewer = null;
        $submission_comment = null;
        if (!empty($article->submission_id)) {
            $ojs_files = $this->OjsReviewRoundFiles
                ->find('all', ['contain' => 'OjsSubmissionFiles'])
                ->where([
                    'OjsReviewRoundFiles.submission_id' => $article->submission_id,
                    'OjsSubmissionFiles.file_type' => 'application/pdf',
                ]);
            $ojs_file = json_decode(json_encode($ojs_files), true);
            $submissions_comments = $this->OjsSubmissionComments
                ->find('all')
                ->where(['submission_id' => $article->submission_id, 'viewable' => 1]);
            $submission_comment = json_decode(json_encode($submissions_comments), true);
            $reviewers = $this->OjsReviewAssignments
                ->find()
                ->where(['submission_id' => $article->submission_id])
                ->extract('reviewer_id');
            $reviewer = json_decode(json_encode($reviewers), true);
        }
        $users_ojs_first = $this->OjsUserSettings->find('all')->where(['setting_name' => 'givenName']);
        $users_ojs_last = $this->OjsUserSettings->find('all')->where(['setting_name' => 'familyName']);
        $user_ojs_first = json_decode(json_encode($users_ojs_first), true);
        $user_ojs_last = json_decode(json_encode($users_ojs_last), true);
        $prefers = $this->RequestHandler->prefers();

        if ($prefers == 'pdf') {
            return $this->getResponse()->withFile($article->pdf_link);
        }

        $access_granted = $this->RequestAccess->checkUserRoles([1, 12]);
        if (!$this->GranularAccess->canViewArticle($article)) {
            throw new ForbiddenException();
        }

        $this->set('article', $article);
        if (!empty($article->submission_id)) {
            $this->set(compact('user_ojs_first', 'user_ojs_last', 'submission_comment', 'reviewer', 'ojs_file'));
        } else {
            $this->set(compact('user_ojs_first', 'user_ojs_last'));
        }

        $this->set('_serialize', 'article');
        $this->set(compact('access_granted', 'article'));
    }

    public function image($type, $id, $name)
    {
        $path = pathinfo($name);
        if (!preg_match('/^\d+$/', $path['filename'])) {
            throw new NotFoundException('Image not found');
        }
        $article = $this->Articles->getByNumber($type, $id);
        $path = $article->getFileDirectory() . 'images-' . $path['basename'];
        if (!file_exists($path)) {
            throw new NotFoundException('Image not found');
        }

        return $this->getResponse()->withFile($path);
    }
}
