<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

/**
 * MaterialColors Controller
 *
 * @property \App\Model\Table\MaterialColorsTable $MaterialColors
 * @method \App\Model\Entity\MaterialColor[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Model\Table\ArtifactsMaterialsTable $ArtifactsMaterials
 * @property \App\Controller\Component\ApiComponent $Api
 */
class MaterialColorsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);

        // Set access for public.
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'maxLimit' => $this->MaterialColors->find()->count(),
        ];
        $materialColors = $this->paginate($this->MaterialColors);
        $access_granted = $this->RequestAccess->checkUserRoles([1]);

        $this->set(compact('materialColors', 'access_granted'));
        $this->set('_serialize', 'materialColors');
    }

    /**
     * View method
     *
     * @param string|null $id Material Color id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $materialColor = $this->MaterialColors->get($id);
        $artifacts = $this->loadModel('ArtifactsMaterials');
        $access_granted = $this->RequestAccess->checkUserRoles([1]);
        $count = $artifacts->find('list', ['conditions' => ['material_color_id' => $id]])->count();

        $this->set(compact('access_granted', 'count'));
        $this->set('materialColor', $materialColor);
        $this->set('_serialize', 'materialColor');
    }
}
