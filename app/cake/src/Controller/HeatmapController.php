<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller;

use Cake\ORM\TableRegistry;
use Exception;

/**
 * Visualizations Controller
 */
class HeatmapController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        // This function must set the access to public users.
        $this->Authentication->allowUnauthenticated(['index', 'post']);
    }

    /**
     * Sets variables required by the HTML or index.ctp.
     *
     * @return void
     */
    public function index()
    {
        $mapTiles = [
            'Carto DB Light Map (Default)' => [
                'url' => 'https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png',
                'attribution' => htmlspecialchars('&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>'),
                'subdomains' => 'abcd',
            ],
            'Carto DB Dark Map' => [
                'url' => 'https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}{r}.png',
                'attribution' => htmlspecialchars('&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>'),
                'subdomains' => 'abcd',
            ],
            'Carto DB Color Map' => [
                'url' => 'https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png',
                'attribution' => htmlspecialchars('&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>'),
                'subdomains' => 'abcd',
            ],
            'Open Street Map (Local Language Labels)' => [
                'url' => 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                'attribution' => htmlspecialchars('&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'),
                'subdomains' => false,
            ],
            'Wikimedia' => [
                'url' => 'https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}{r}.png',
                'attribution' => htmlspecialchars('<a href="https://wikimediafoundation.org/wiki/Maps_Terms_of_Use">Wikimedia</a>'),
                'subdomains' => false,
            ],
        ];

        // phpcs:disable CakePHP.WhiteSpace.TabAndSpace.DoubleSpace
        $filters = [
            'Object Data' => [
                'material' =>    ['displayName' => 'Material', 'isConcise' => true ],
                'collection' =>  ['displayName' => 'Museum collections', 'isConcise' => false],
                'type' =>        ['displayName' => 'Object type', 'isConcise' => true ],
                'period' =>      ['displayName' => 'Period', 'isConcise' => true],
                'provenience' => ['displayName' => 'Provenience', 'isConcise' => false],
            ],
            'Textual Data' => [
                'genre' =>      ['displayName' => 'Genre', 'isConcise' => true],
                'language' =>   ['displayName' => 'Language', 'isConcise' => true],
            ],
            'Publication' => [
                'author' => ['displayName' => 'Authors', 'isConcise' => false],
                //'date' =>   ['displayName' => 'Date of Publication', 'isConcise' => true ],
            ],
        ];
        // phpcs:enable CakePHP.WhiteSpace.TabAndSpace.DoubleSpace

        $this->set('mapTiles', $mapTiles);
        $this->set('filters', $filters);
    }

    /**
     * Accepts only POST requests and returns filter tags or filtered provenience data
     */
    public function post()
    {
        $this->autoRender = false;
        $this->getRequest()->allowMethod('post');

        //If filter tags are needed
        if ($this->getRequest()->getData('getFilter')) {
            $query = $this->getFilter($this->getRequest()->getData('filter'));
        } else { //If filtered provenience data is needed
            $options = [];

            //Join data
            if ($this->getRequest()->getData('author')) {
                $options['join'] = [
                    [ 'table' => 'entities_publications', 'conditions' => ['entities_publications.entity_id = Artifacts.id', "entities_publications.table_name = 'artifacts'"]],
                    [ 'table' => 'authors_publications', 'conditions' => 'entities_publications.publication_id = authors_publications.publication_id' ],
                ];
            }

            //Specifying the full provenience data
            $query = TableRegistry::getTableLocator()->get('Artifacts')->find('all', $options);
            $query->select(['pId' => 'provenience_id', 'aCount' => $query->func()->count('Artifacts.id'),
                'name' => 'Proveniences.provenience', 'location' => 'CONCAT(Locations.location_longitude_wgs1984, \',\', Locations.location_latitude_wgs1984)'])
                ->contain(['Proveniences.Locations'])
                ->group('provenience_id')
                ->order('provenience_id');

            //Specifying the told filters
            if ($this->getRequest()->getData('material')) {
                $query->matching('Materials', function ($q) {
                    return $q->where(['Materials.id IN' => $this->getRequest()->getData('material')]);
                });
            }
            if ($this->getRequest()->getData('collection')) {
                $query->matching('Collections', function ($q) {
                    return $q->where(['Collections.id IN' => $this->getRequest()->getData('collection')]);
                });
            }
            if ($this->getRequest()->getData('type')) {
                $query->matching('ArtifactTypes', function ($q) {
                    return $q->where(['ArtifactTypes.id IN' => $this->getRequest()->getData('type')]);
                });
            }
            if ($this->getRequest()->getData('period')) {
                $query->matching('Periods', function ($q) {
                    return $q->where(['Periods.id IN' => $this->getRequest()->getData('period')]);
                });
            }
            if ($this->getRequest()->getData('provenience')) {
                $query->where(['Proveniences.id IN' => $this->getRequest()->getData('provenience')]);
            }
            if ($this->getRequest()->getData('genre')) {
                $query->matching('Genres', function ($q) {
                    return $q->where(['genre_id IN' => $this->getRequest()->getData('genre')]);
                });
            }
            if ($this->getRequest()->getData('language')) {
                $query->matching('Languages', function ($q) {
                    return $q->where(['language_id IN' => $this->getRequest()->getData('language')]);
                });
            }
            if ($this->getRequest()->getData('author')) {
                $query->where(['authors_publications.author_id IN' => $this->getRequest()->getData('author')]);
            }
        }

        //Encoding the result of the query and sending it back
        $jsonResult = json_encode($query);

        return $this->getResponse()->withType('json')
            ->withStringBody($jsonResult);
    }

    /**
     * Returns the tag of the filter
     *
     * @param string $filterName The code-name of the filter whose tags are needed
     * @return \Cake\ORM\Query $query
     */
    private function getFilter($filterName)
    {
        switch ($filterName) {
            case 'material':
                $query = TableRegistry::getTableLocator()->get('ArtifactsMaterials')->find();
                $query->select(['id' => 'material_id', 'filter' => 'Materials.material', 'aCount' => $query->func()->count('artifact_id')])
                    ->contain('Materials')
                    ->group('material_id')
                    ->order('Materials.material');
                break;

            case 'collection':
                $query = TableRegistry::getTableLocator()->get('ArtifactsCollections')->find();
                $query->select(['id' => 'collection_id', 'filter' => 'Collections.collection', 'aCount' => $query->func()->count('artifact_id')])
                    ->contain('Collections')
                    ->group('collection_id')
                    ->order('Collections.collection');
                break;

            case 'type':
                $query = TableRegistry::getTableLocator()->get('Artifacts')->find();
                $query->select(['id' => 'ArtifactTypes.id', 'filter' => 'ArtifactTypes.artifact_type', 'aCount' => $query->func()->count('Artifacts.id')])
                    ->contain('ArtifactTypes')
                    ->group('ArtifactTypes.id')
                    ->order('ArtifactTypes.artifact_type');
                break;

            case 'period':
                $query = TableRegistry::getTableLocator()->get('Artifacts')->find();
                $query->select(['id' => 'Periods.id', 'filter' => 'Periods.period', 'aCount' => $query->func()->count('Artifacts.id')])
                    ->contain('Periods')
                    ->group('Periods.id')
                    ->order('Periods.sequence');
                break;

            case 'provenience':
                $query = TableRegistry::getTableLocator()->get('Artifacts')->find();
                $query->select(['id' => 'Proveniences.id', 'filter' => 'Proveniences.provenience', 'aCount' => $query->func()->count('Artifacts.id')])
                    ->contain('Proveniences')
                    ->group('Proveniences.id')
                    ->order('Proveniences.provenience');
                break;

            case 'genre':
                $query = TableRegistry::getTableLocator()->get('ArtifactsGenres')->find();
                $query->select(['id' => 'genre_id', 'filter' => 'Genres.genre', 'aCount' => $query->func()->count('artifact_id')])
                    ->contain('Genres')
                    ->group('genre_id')
                    ->order('Genres.genre');
                break;

            case 'language':
                $query = TableRegistry::getTableLocator()->get('ArtifactsLanguages')->find();
                $query->select(['id' => 'language_id', 'filter' => 'Languages.language', 'aCount' => $query->func()->count('artifact_id')])
                    ->contain('Languages')
                    ->group('language_id')
                    ->order('Languages.language');
                break;

            case 'author':
                $options['join'] = [
                    [ 'table' => 'authors_publications', 'conditions' => 'entities_publications.publication_id = authors_publications.publication_id'],
                    [ 'table' => 'authors', 'conditions' => 'authors.id = authors_publications.author_id'],
                ];

                $query = TableRegistry::getTableLocator()->get('EntitiesPublications')->find('all', $options)->where("entities_publications.table_name = 'artifacts'");
                $query->select(['id' => 'authors.id', 'filter' => 'authors.author', 'aCount' => $query->func()->count('entities_publications.entity_id')])
                    ->group('id')
                    ->order('filter');
                break;

            default:
                throw new Exception('Invalid filter name: ' . $filterName);
        }

        return $query;
    }
}
