<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\UnauthorizedException;

/**
 * ArtifactAssets Controller
 *
 * @property \App\Model\Table\ArtifactAssetsTable $ArtifactAssets
 * @method \App\Model\Entity\ArtifactAsset[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ApiComponent $Api
 * @property \App\Controller\Component\GranularAccessComponent $GranularAccess
 */
class ArtifactAssetsController extends AppController
{
    /**
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);
        $this->loadComponent('GranularAccess');

        $this->Authentication->allowUnauthenticated(['index', 'view', 'resolve', 'authorize']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $conditions = $this->GranularAccess->canViewPrivateImage() ? [] : [
            'ArtifactAssets.is_public' => true,
        ];

        $this->paginate = [
            'contain' => [
                'Artifacts',
                'Authors',
                'Publications',
            ],
            'order' => [
                'ArtifactAssets.artifact_id' => 'ASC',
                'ArtifactAssets.asset_type' => 'ASC',
            ],
            'conditions' => $conditions,
        ];

        if ($this->Api->requestsFormat(['LinkedData', 'TableExport'])) {
            $this->paginate['limit'] = 10000;
            $this->paginate['maxLimit'] = 10000;
        }

        $assets = $this->paginate($this->ArtifactAssets);

        $this->set('artifactAssets', $assets);
        $this->set('_serialize', 'artifactAssets');
        $this->set('_displayField', 'path');
        $this->set('isAdmin', $this->GranularAccess->isAdmin());
    }

    /**
     * View method
     *
     * @param string|null $id Artifact Asset id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @throws \Cake\Http\Exception\UnauthorizedException When record not available to user.
     */
    public function view($id)
    {
        $asset = $this->ArtifactAssets->get($id, ['contain' => [
            'Artifacts.Collections',
            'Authors',
            'Publications',
        ]]);

        if (!$this->GranularAccess->canViewAsset($asset)) {
            throw new UnauthorizedException();
        }

        $this->ArtifactAssets->Artifacts->loadInto($asset->artifact, ['ArtifactAssets']);
        $this->set('asset', $asset);
        $this->set('isAdmin', $this->GranularAccess->isAdmin());
        $this->set('_serialize', 'asset');
    }

    /**
     * Resolve asset by path
     *
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function resolve()
    {
        $path = $this->getRequest()->getQuery('path');

        if (is_null($path)) {
            return $this->getResponse()->withStatus(404);
        }

        /** @var \App\Model\Entity\ArtifactAsset $asset */
        $asset = $this->ArtifactAssets->find('path', [$path])->contain(['Artifacts'])->first();

        if (!$this->GranularAccess->canViewAsset($asset)) {
            $status = $this->Authentication->getIdentity() ? 403 : 401;

            return $this->getResponse()->withStatus($status);
        }

        $target = $this->getRequest()->getQuery('target');
        if ($target === 'reader') {
            return $this->redirect(['controller' => 'Artifacts', 'action' => 'reader', $asset->artifact_id, $asset->id]);
        } else {
            return $this->redirect(['action' => 'view', $asset->id]);
        }
    }

    /**
     * Authorize image access
     *
     * @param string|null $id Artifact Asset id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function authorize()
    {
        $path = $this->getRequest()->getQuery('path');
        /** @var \App\Model\Entity\ArtifactAsset $asset */
        $asset = $this->ArtifactAssets->find('path', [$path])->contain(['Artifacts'])->first();

        // Image 404 are handled in nginx
        if (is_null($asset) || $this->GranularAccess->canViewAsset($asset)) {
            return $this->getResponse()->withStatus(200);
        }

        if ($this->Authentication->getIdentity()) {
            return $this->getResponse()->withStatus(403);
        } else {
            return $this->getResponse()->withStatus(401);
        }
    }
}
