<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * SignReadings Controller
 *
 * @property \App\Model\Table\SignReadingsTable $SignReadings
 * @method \App\Model\Entity\SignReading[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 * @property \App\Controller\Component\ApiComponent $Api
 */
class SignReadingsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Api', ['features' => ['LinkedData', 'TableExport']]);
        $this->loadComponent('Search');

        // Set access for public.
        $this->Authentication->allowUnauthenticated(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $query = $this->SignReadings->find('search', [
            'search' => $this->request->getQueryParams(),
            'contain' => ['Periods', 'Proveniences', 'Languages', 'SignReadingsComments'],
        ]);

        $signReadings = $this->paginate($query);
        $access_granted = $this->RequestAccess->checkUserRoles([1]);

        $this->set(compact('signReadings', 'access_granted'));
        $this->set('_serialize', 'signReadings');
    }

    /**
     * View method
     *
     * @param string|null $id Sign Reading id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $signReading = $this->SignReadings->get($id, [
            'contain' => ['Periods', 'Proveniences', 'Languages', 'SignReadingsComments'],
        ]);
        $access_granted = $this->RequestAccess->checkUserRoles([1]);

        $this->set(compact('access_granted'));
        $this->set('signReading', $signReading);
        $this->set('_serialize', 'signReading');
    }
}
