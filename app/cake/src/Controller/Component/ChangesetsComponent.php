<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Controller\Controller;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Session;
use Cake\I18n\FrozenTime;
use Cake\Routing\Router;

/**
 * @property \Cake\Controller\Component\FlashComponent $Flash
 */
class ChangesetsComponent extends Component
{
    protected $components = ['Flash'];
    protected Controller $controller;
    protected Session $session;

    protected const SESSION_VARIABLE = 'UpdateEvents.changesets';
    protected const SESSION_VARIABLE_BLOCKING = 'UpdateEvents.blockingChangeset';

    public function __construct(ComponentRegistry $registry, array $config = [])
    {
        parent::__construct($registry, $config);

        $this->controller = $this->getController();
        $this->session = $this->controller->getRequest()->getSession();

        // "Rescue" edits in the old format
        $updates = $this->getAll();

        if (!is_null($this->session->read('ArtifactsUpdates.uploads'))) {
            $updates[] = $this->create(
                ['artifact'],
                $this->session->consume('ArtifactsUpdates.uploads'),
                $this->session->consume('ArtifactsUpdates.errors')
            );
        }

        if (!is_null($this->session->read('ArtifactAssetAnnotations.uploads'))) {
            $updates[] = $this->create(
                ['visual_annotation'],
                $this->session->consume('ArtifactAssetAnnotations.uploads'),
                $this->session->consume('ArtifactAssetAnnotations.errors')
            );
        }

        if (!is_null($this->session->read('EntitiesUpdates.uploads'))) {
            foreach ($this->session->consume('EntitiesUpdates.uploads') as $update) {
                $updates[] = $this->create(['other_entity'], [$update]);
            }
        }

        if (!is_null($this->session->read('EntitiesUpdates.pending'))) {
            $updates[] = $this->create(['other_entity'], $this->session->consume('EntitiesUpdates.pending'));
        }

        if (!is_null($this->session->read('Inscriptions.uploadType'))) {
            $updates[] = $this->create(
                [$this->session->consume('Inscriptions.uploadType')],
                $this->session->consume('Inscriptions.uploads') ?? [],
                $this->session->consume('Inscriptions.errors')
            );
        }

        $this->setAll($updates);
    }

    public function create(array $types, array $updates, ?array $errors = null): array
    {
        $changesets = $this->getAll();
        do {
            $id = random_int(0, 0xffff);
        } while (array_key_exists($id, $changesets));

        $hasErrors = !is_null($errors) && count($errors) > 0;
        foreach ($updates as $update) {
            if ($update->hasErrors() || (method_exists($update, 'validate') && !$update->validate())) {
                $hasErrors = true;
            }
        }

        return [
            'id' => $id,
            'created' => FrozenTime::now(),
            'update_type' => $types,
            'updates' => $updates,
            'hasErrors' => $hasErrors,
            'errors' => $errors ?? [],
        ];
    }

    public function merge(array $changesets): array
    {
        $merge = array_values($changesets)[0];
        foreach (array_slice($changesets, 1) as $changeset) {
            $merge['id'] = min($changeset['id'], $merge['id']);
            $merge['created'] = $changeset['created']->min($merge['created']);
            $merge['update_type'] = array_unique(array_merge($changeset['update_type'], $merge['update_type']));
            $merge['updates'] = array_merge($changeset['updates'], $merge['updates']);
            $merge['hasErrors'] = $changeset['hasErrors'] || $merge['hasErrors'];
            $merge['errors'] = array_merge($changeset['errors'], $merge['errors']);
        }

        return $merge;
    }

    public function has(mixed $id): bool
    {
        if (!is_int($id) && (!is_string($id) || !is_numeric($id))) {
            return false;
        }

        return $this->session->check(self::SESSION_VARIABLE . '.' . $id);
    }

    public function get(mixed $id): array
    {
        $changeset = $this->session->read(self::SESSION_VARIABLE . '.' . $id);

        if (!is_array($changeset)) {
            throw new RecordNotFoundException();
        }

        return $changeset;
    }

    public function getAll(): array
    {
        return $this->session->read(self::SESSION_VARIABLE) ?? [];
    }

    public function set(array $changeset): void
    {
        $this->session->write(self::SESSION_VARIABLE . '.' . $changeset['id'], $changeset);
    }

    public function setAll(array $changesets): void
    {
        $this->session->write(self::SESSION_VARIABLE, $changesets);
    }

    public function getRedraftable(mixed $id): ?array
    {
        if (!$this->has($id)) {
            return null;
        }

        $changeset = $this->get($id);

        if (!$this->canRedraft($changeset)) {
            return null;
        }

        return $changeset;
    }

    public function canRedraft(array $changeset): bool
    {
        $url = Router::url($this->getRedraftUrl($changeset));
        $currentUrl = $this->controller->getRequest()->getRequestTarget();

        return $url === $currentUrl;
    }

    public function getRedraftUrl(array $changeset): ?array
    {
        if ($changeset['update_type'] === ['atf']) {
            $url = ['controller' => 'Inscriptions', 'action' => 'add', '?' => ['tab' => 'atf']];
        } elseif ($changeset['update_type'] === ['annotation']) {
            $url = ['controller' => 'Inscriptions', 'action' => 'add', '?' => ['tab' => 'conll']];
        } elseif ($changeset['update_type'] === ['artifact'] && count($changeset['updates']) === 1) {
            $update = $changeset['updates'][0];
            $url = ['controller' => 'Artifacts'];

            if ($update->has('artifact_id')) {
                $url['action'] = 'edit';
                $url[] = $update->artifact_id;
            } else {
                $url['action'] = 'add';
            }
        }

        // else if ($changeset['update_type'] === ['other_entity'] && count($changeset['updates']) === 1) {
        //     $update = $changeset['updates'][0];
        //     $url = ['controller' => Inflector::camelize($update->entity_table)];
        //
        //     if ($update->has('entity_id')) {
        //         $url['action'] = 'edit';
        //         $url[] = $update->entity_id;
        //     } else {
        //         $url['action'] = 'add';
        //     }
        // }

        // TODO redraft merges
        // TODO bulk upload

        if (!isset($url) || is_null($url)) {
            return null;
        }

        $url['?']['changeset'] = $changeset['id'];

        return $url;
    }

    public function handleBlockingChangeset(): void
    {
        $blockingChangeset = $this->getBlockingChangeset();
        if (is_null($blockingChangeset)) {
            return;
        }

        if (!array_key_exists($blockingChangeset, $this->getAll())) {
            $this->setBlockingChangeset(null);

            return;
        }

        $request = $this->controller->getRequest();
        if (!$request->is(['post', 'put', 'patch'])) {
            $this->Flash->warning(__('You already have an unsaved changeset. Click "Upload" to save these changes first, or click "Upload later" to submit multiple changes at the same time.'));
            $this->controller->redirect([
                'prefix' => false,
                'controller' => 'Changesets',
                'action' => 'view',
                $blockingChangeset,
                '?' => ['redirect' => $request->getRequestTarget()],
            ]);
        }
    }

    public function getBlockingChangeset(): int|null
    {
        return $this->session->read(self::SESSION_VARIABLE_BLOCKING);
    }

    public function setBlockingChangeset(array|null $changeset): void
    {
        if (is_null($changeset)) {
            $this->session->delete(self::SESSION_VARIABLE_BLOCKING);
        } else {
            $this->session->write(self::SESSION_VARIABLE_BLOCKING, $changeset['id']);
        }
    }
}
