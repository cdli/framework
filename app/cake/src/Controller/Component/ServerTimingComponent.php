<?php
declare(strict_types=1);

namespace App\Controller\Component;

use App\Utility\ServerTimings\ServerTimings;
use Cake\Controller\Component;
use Cake\Event\EventInterface;

class ServerTimingComponent extends Component
{
    protected array $timers = [];
    protected ?ServerTimings $timings;

    public function initialize(array $config): void
    {
        $this->timings = $this->getController()->getRequest()->getAttribute('serverTimings');
    }

    public function start(string $name, $description = null): void
    {
        if (!is_null($this->timings)) {
            $this->timers[$name] = $this->timings->create($name, $description);
            $this->timers[$name]->start();
        }
    }

    public function stop(string $name)
    {
        if (array_key_exists($name, $this->timers)) {
            $this->timers[$name]->stop();
        }
    }

    public function startup(EventInterface $event)
    {
        $this->start('action');
    }

    public function beforeRender(EventInterface $event)
    {
        $this->stop('action');
        $this->start('render');
    }

    public function shutdown(EventInterface $event)
    {
        $this->stop('render');
    }
}
