<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller\Component;

use App\Model\Entity\Article;
use App\Model\Entity\Artifact;
use App\Model\Entity\ArtifactAsset;
use App\Model\Entity\UpdateEvent;
use Cake\Controller\Component;
use Cake\ORM\ResultSet;
use Cake\ORM\TableRegistry;

/**
 * @property \Authentication\Controller\Component\AuthenticationComponent $Authentication
 * @property \App\Controller\Component\RequestAccessComponent $RequestAccess
 */
class GranularAccessComponent extends Component
{
    public $components = ['RequestAccess', 'Authentication.Authentication'];

    /**
     * @var \Cake\ORM\Table
     */
    protected $Users;

    /**
     * @var \Cake\ORM\Table
     */
    protected $Artifacts;

    /**
     * @var \Cake\ORM\Table
     */
    protected $UpdateEvents;

    public function initialize(array $config): void
    {
        $tableLocator = TableRegistry::getTableLocator();
        $this->Artifacts = $tableLocator->get('Artifacts');
        $this->UpdateEvents = $tableLocator->get('UpdateEvents');
        $this->Users = $tableLocator->get('Users');
    }

    /**
     * Load inscriptions and related artifacts when allowed.
     *
     * @param \App\Model\Entity\Artifact $artifact
     */
    public function amendArtifact(Artifact $artifact): void
    {
        // Load related artifacts (composite/seal and the reverse)
        if ($this->canViewPrivateArtifact()) {
            $this->Artifacts->loadInto($artifact, [
                'Witnesses',
                'Impressions',
                'Composites',
                'Seals',
            ]);
        } else {
            $this->Artifacts->loadInto($artifact, [
                'Witnesses' => ['conditions' => ['Witnesses.is_public' => true]],
                'Impressions' => ['conditions' => ['Impressions.is_public' => true]],
                'Composites' => ['conditions' => ['Composites.is_public' => true]],
                'Seals' => ['conditions' => ['Seals.is_public' => true]],
            ]);
        }

        // Load related inscriptions
        if ($artifact->is_atf_public || $this->canViewPrivateInscriptions()) {
            $this->Artifacts->loadInto($artifact, ['Inscriptions']);
        }

        if (is_array($artifact->witnesses)) {
            foreach ($artifact->witnesses as $witness) {
                if ($witness->is_atf_public || $this->canViewPrivateInscriptions()) {
                    $this->Artifacts->loadInto($witness, ['Inscriptions']);
                }
            }
        }

        // Load artifact shadow
        if ($this->isAdmin()) {
            $this->Artifacts->loadInto($artifact, ['ArtifactsShadow']);
        }
    }

    public function amendArtifactAssets(Artifact $artifact): void
    {
        if ($this->canViewPrivateImage()) {
            $this->Artifacts->loadInto($artifact, ['ArtifactAssets']);
        } else {
            $this->Artifacts->loadInto($artifact, [
                'ArtifactAssets' => ['conditions' => ['is_public' => true]],
            ]);
        }
    }

    /**
     * Load artifact updates and inscriptions when allowed.
     *
     * @param \App\Model\Entity\UpdateEvent $event
     */
    public function amendUpdateEvent($event): void
    {
        // Artifacts
        $conditions = ['update_events_id' => $event->id];
        $results = $this->UpdateEvents->ArtifactsUpdates->find()->where($conditions)->contain(['Artifacts'])->toArray();
        if (count($results) > 0) {
            $this->UpdateEvents->ArtifactsUpdates->loadInto($results, [
                'Artifacts' => [
                    'Archives',
                    'ArtifactsShadow',
                    'ArtifactTypes',
                    'Collections',
                    'Composites',
                    'ExternalResources',
                    'Genres',
                    'Languages',
                    'Materials',
                    'Origins',
                    'Periods',
                    'Proveniences',
                    'Publications',
                    'Seals',
                ],
            ]);
            $this->amendArtifactsUpdate($results);
        }
        $event->artifacts_updates = $results;

        // Visual annotations
        $conditions = ['update_event_id' => $event->id];
        $results = $this->UpdateEvents->ArtifactAssetAnnotations->find()->where($conditions)->contain(['Bodies'])->toArray();
        if (count($results) > 0) {
            $this->UpdateEvents->ArtifactAssetAnnotations->loadInto($results, ['ArtifactAssets.Artifacts']);
        }
        $event->artifact_asset_annotations = $results;

        // Other entities
        $event->entities_updates = $this->UpdateEvents->EntitiesUpdates->find()->where(['update_event_id' => $event->id])->toArray();

        // Inscriptions
        $conditions = ['update_event_id' => $event->id];
        $results = $this->UpdateEvents->Inscriptions->find()->where($conditions)->contain(['Artifacts'])->toArray();
        if (count($results) > 0) {
            $this->UpdateEvents->Inscriptions->loadInto($results, ['Artifacts', 'UpdateEvents']);
            foreach ($results as $inscription) {
                $oldInscription = $this->UpdateEvents->Inscriptions->getPreviousInscriptions($inscription);
                $inscription->artifact->set('inscription', $oldInscription);
            }
        }
        $event->inscriptions = $results;

        // Check access
        foreach ($event->artifacts_updates as $i => $artifactsUpdate) {
            if ($this->canViewPrivateArtifact()) {
                continue;
            }
            if ($artifactsUpdate->has('artifact') && $artifactsUpdate->artifact->is_public) {
                continue;
            }
            if (is_null($artifactsUpdate->artifact_id) && $artifactsUpdate->is_public !== '0') {
                continue;
            }

            unset($event->artifacts_updates[$i]);
            $event->missingPrivateChanges = true;
        }

        foreach ($event->artifact_asset_annotations as $i => $annotation) {
            if ($this->canViewAsset($annotation->artifact_asset) && $this->canViewArtifact($annotation->artifact_asset->artifact)) {
                continue;
            }

            unset($event->artifact_asset_annotations[$i]);
            $event->missingPrivateChanges = true;
        }

        foreach ($event->inscriptions as $i => $inscription) {
            $canViewInscription = $this->canViewPrivateInscriptions() || $inscription->artifact->is_atf_public;
            if ($canViewInscription && $this->canViewArtifact($inscription->artifact)) {
                continue;
            }

            unset($event->inscriptions[$i]);
            $event->missingPrivateChanges = true;
        }
    }

    /**
     * Censor private fields.
     *
     * @param mixed $update
     */
    public function amendArtifactsUpdate($update): void
    {
        if ($this->isAdmin()) {
            return;
        }

        if (is_array($update) || $update instanceof ResultSet) {
            foreach ($update as $single_update) {
                $this->amendArtifactsUpdate($single_update);
            }

            return;
        }

        foreach (array_keys(Artifact::$privateFlatFields) as $key) {
            $update->unset($key);
        }
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->RequestAccess->checkUserRoles([1]);
    }

    /**
     * @return bool
     */
    public function canSubmitEdits()
    {
        if (!$this->Authentication->getIdentity()) {
            return false;
        }

        /** @var \App\Model\Entity\User $user */
        $user = $this->Authentication->getIdentity();

        return $user->has('author_id');
    }

    /**
     * @return bool
     */
    public function canAccessEdits(UpdateEvent $event)
    {
        if ($event->status == 'approved' || $event->status == 'submitted') {
            return true;
        }

        if (!$this->Authentication->getIdentity()) {
            return false;
        }

        return $this->canReviewEdits() || $event->isCreatedBy($this->Authentication->getIdentity());
    }

    /**
     * @return bool
     */
    public function canViewAsset(ArtifactAsset $asset)
    {
        if ($asset->is_public) {
            return true;
        }

        return $this->canViewPrivateImage();
    }

    /**
     * @return bool
     */
    public function canViewArticle(Article $article)
    {
        // Article is published
        if ($article->article_status === 3) {
            return true;
        }

        if (!$this->Authentication->getIdentity()) {
            return false;
        }

        // User is admin or editor
        if ($this->RequestAccess->checkUserRoles([1, 12])) {
            return true;
        }

        // Article is at proofing stage and user is author
        if ($article->article_status === 2 && in_array($this->Authentication->getIdentityData('author_id'), array_column($article->authors, 'id'))) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function canViewArtifact($artifact)
    {
        if ($artifact->is_public) {
            return true;
        }

        return $this->canViewPrivateArtifact();
    }

    /**
     * @return bool
     */
    public function canReviewEdits()
    {
        return $this->RequestAccess->checkUserRoles([1, 2]);
    }

    /**
     * @return bool
     */
    public function canViewPrivateArtifact()
    {
        return $this->RequestAccess->checkUserRoles([1, 4]);
    }

    /**
     * @return bool
     */
    public function canViewPrivateImage()
    {
        return $this->RequestAccess->checkUserRoles([1, 7]);
    }

    /**
     * @return bool
     */
    public function canViewPrivateInscriptions()
    {
        return $this->RequestAccess->checkUserRoles([1, 5]);
    }
}
