<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Event\Event;
use Cake\Routing\Router;

/**
 * @property \Cake\Controller\Component\PaginatorComponent $Paginator
 */
class RestPaginatorComponent extends Component
{
    public $components = ['Paginator'];

    public function beforeRender(Event $event)
    {
        $controller = $this->_registry->getController();

        if (!$controller->viewBuilder()->hasVar('_serialize')) {
            return;
        }

        $params = $this->Paginator->getPagingParams();

        if (empty($params)) {
            return;
        }

        $params = array_values($params)[0];

        $links = [
            $this->buildUrl('1', $params, 'first'),
            $this->buildUrl($params['page'], $params, 'current'),
        ];

        // Page count is not available when using SimplePaginator
        if ($params['pageCount'] !== 0) {
            $links[] = $this->buildUrl($params['pageCount'], $params, 'last');
        }

        if ($params['prevPage']) {
            $links[] = $this->buildUrl($params['page'] - 1, $params, 'prev');
        }

        if ($params['nextPage']) {
            $links[] = $this->buildUrl($params['page'] + 1, $params, 'next');
        }

        // Do not add header if it is too long
        $approxHeaderLength = strlen(implode('', $links));
        if ($approxHeaderLength > 4000) {
            return;
        }

        $controller->setResponse($controller->getResponse()->withHeader('Link', $links));
    }

    private function buildUrl($page, $params, $rel)
    {
        $urlParams = [
            'page' => $page,
            'limit' => $params['perPage'],
        ];

        if (array_key_exists('nextSearchAfter', $params) && $rel === 'next') {
            $urlParams['search_after'] = base64_encode(json_encode($params['nextSearchAfter']));
        }

        $urlParams = array_merge($this->getController()->getRequest()->getQueryParams(), $urlParams);
        $url = Router::url(['?' => $urlParams], true);

        return '<' . $url . '>; rel="' . $rel . '"';
    }
}
