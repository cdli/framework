<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Search\Controller\Component\SearchComponent as BaseComponent;

class SearchComponent extends BaseComponent
{
    public function beforeRender()
    {
        parent::beforeRender();

        $model = $this->getController()->loadModel($this->getConfig('modelClass'));
        /**
         * @var \Search\Manager $manager
         * @psalm-suppress UndefinedInterfaceMethod
         */
        $manager = $model->searchManager();

        $searchFields = [];
        foreach ($manager->getFilters() as $field => $filter) {
            $fieldOptions = $filter->getConfig('formControlOptions', ['type' => 'text']);
            if (!is_array($fieldOptions)) {
                $fieldOptions = $fieldOptions();
            }
            $searchFields[$field] = $fieldOptions;
        }

        $this->getController()->set(compact('searchFields'));
    }
}
