<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Event\EventInterface;
use Cake\Http\Exception\RedirectException;

/**
 * @property \Authentication\Controller\Component\AuthenticationComponent $Authentication
 * @property \Cake\Controller\Component\FlashComponent $Flash
 */
class RequestAccessComponent extends Component
{
    protected $_defaultConfig = [
        'authError' => 'You are not authorized to access that location.',
    ];

    public $components = ['Flash', 'Authentication.Authentication'];

    public function beforeFilter(EventInterface $event): void
    {
        $request = $this->getController()->getRequest();
        $prefix = $request->getParam('prefix');

        if ($prefix === null) {
            return;
        }

        if ($prefix === 'Admin') {
            if ($this->checkUserRoles([1])) {
                return;
            }

            $controller = $request->getParam('controller');
            $action = $request->getParam('action');

            if (
                $this->checkUserRoles([2])
                && in_array($controller, ['Home', 'Authors', 'Ckeconnector', 'Inscriptions', 'Search'])
            ) {
                return;
            }

            if (
                $this->checkUserRoles([12])
                && in_array($controller, ['Home', 'Authors', 'Ckeconnector', 'Articles'])
            ) {
                if (!($controller === 'Authors' && $action === 'delete')) {
                    return;
                }
            }
        }

        $this->Flash->error($this->getConfig('authError'));

        throw new RedirectException($this->getController()->referer('/', true));
    }

    /**
     * Check if logged in user has the given roles.
     */
    public function checkUserRoles($rolesToBeChecked): bool
    {
        $rolesToBeChecked = (array)$rolesToBeChecked;

        $identity = $this->Authentication->getIdentity();
        if ($identity === null) {
            return false;
        }

        $roles = (array)$identity->roles;
        $checkIfRoleExistsForAccess = array_intersect($roles, $rolesToBeChecked);
        if (!empty($checkIfRoleExistsForAccess)) {
            return true;
        }

        return false;
    }
}
