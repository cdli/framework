<?php
declare(strict_types=1);

namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;

/**
 * MoveOjsToCdli command.
 *
 * @property \App\Model\Table\SubmissionsTable $Submissions
 * @property \App\Model\Table\ArticlesTable $Articles
 * @property \App\Model\Table\AuthorsTable $Authors
 * @property \App\Model\Table\AuthorsOjsTable $AuthorsOjs
 * @property \App\Model\Table\ArticlesAuthorsTable $ArticlesAuthors
 * @property \App\Model\Table\PublicationsOjsTable $PublicationsOjs
 */
class MoveOjsToCdliCommand extends Command
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Submissions');
        $this->loadModel('Articles');
        $this->loadModel('Authors');
        $this->loadModel('AuthorsOjs');
        $this->loadModel('ArticlesAuthors');
        $this->loadModel('PublicationsOjs');
    }

    /**
     * Hook method for defining this command's option parser.
     *
     * @see https://book.cakephp.org/4/en/console-commands/commands.html#defining-arguments-and-options
     * @param \Cake\Console\ConsoleOptionParser $parser The parser to be defined
     * @return \Cake\Console\ConsoleOptionParser The built parser.
     */
    public function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->addArgument('id', [
            'help' => 'What is id',
        ]);

        return $parser;
    }

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|void|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $authors_ojs = $this->AuthorsOjs->find('all', [
            'contain' => ['AuthorSettings'],
        ]);
        foreach ($authors_ojs as $author_ojs) {
            $author_cdli = $this->Authors->find()->where(['ojs_author_id' => $author_ojs->author_id])->first();
            $author_name_cdli = $this->Authors->find()->where(['author' => $author_ojs->author_settings[5]->setting_value . ', ' . $author_ojs->author_settings[6]->setting_value])->first();
            if (empty($author_cdli) && empty($author_name_cdli)) {
                $author = $this->Authors->newEmptyEntity();
                $author->author = $author_ojs->author_settings[5]->setting_value . ', ' . $author_ojs->author_settings[6]->setting_value;
                $author->first = $author_ojs->author_settings[6]->setting_value;
                $author->last = $author_ojs->author_settings[5]->setting_value;
                $author->email = $author_ojs->email;
                $author->institution = $author_ojs->author_settings[4]->setting_value;
                $author->orcid_id = $author_ojs->author_settings[1]->setting_value;
                $author->ojs_author_id = $author_ojs->author_id;
                $this->Authors->save($author);
            }
        }

        $submissions = $this->Submissions->find('all', [
            'contain' => ['PublicationSettings'],
        ]);
        foreach ($submissions as $submission) {
            $type = $this->PublicationsOjs->find()->where(['publication_id' => $submission->submission_id])->extract('section_id')->first();
            $article = $this->Articles->find()->where(['submission_id' => $submission->submission_id])->first();
            $file1 = glob("../srv/app/cake/webroot/pubs/ojs/1/articles/$submission->submission_id/submission/productionReady/*.tex");
            if (!empty($file1[0])) {
                $file = fopen($file1[0], 'r');
                $contents = fread($file, filesize($file1[0]));
            }
            $file1_pdf = glob("../srv/app/cake/webroot/pubs/ojs/1/articles/$submission->submission_id/submission/productionReady/*.pdf");
            if (!empty($file1_pdf[0])) {
                if ($type == 1) {
                    copy($file1_pdf[0], "../srv/app/cake/webroot/pubs/cdlj/$submission->submission_id.pdf");
                    $link = "$submission->submission_id.pdf";
                } else {
                    copy($file1_pdf[0], "../srv/app/cake/webroot/pubs/cdlb/$submission->submission_id.pdf");
                    $link = "$submission->submission_id.pdf";
                }
            }
            if (empty($article)) {
                if ($submission->stage_id == 5) {
                    $article = $this->Articles->newEmptyEntity();
                    $article->title = $submission->publication_settings[3]->setting_value . ' ' . $submission->publication_settings[6]->setting_value;
                    if ($type == 1) {
                        $article->article_type = 'cdlj';
                    } else {
                        $article->article_type = 'cdlb';
                    }
                    $article->content_html = '';
                    if (!empty($contents)) {
                        $article->content_latex = $contents;
                    } else {
                        $article->content_latex = '';
                    }
                    $article->is_pdf_uploaded = true;
                    $article->is_published = 0;
                    $article->article_status = 1;
                    if (!empty($link)) {
                        $article->pdf_link = $link;
                    } else {
                        $article->pdf_link = '';
                    }
                    $article->created_by = 0;
                    $article->created = $submission->date_submitted;
                    $article->modified = $submission->last_modified;
                    $article->submission_id = $submission->submission_id;
                    $this->Articles->save($article);

                    $sequence = 1;
                    $author_ojs_ids = $this->AuthorsOjs->find('all', [
                        'contain' => ['AuthorSettings'],
                    ])->where(['publication_id' => $submission->submission_id]);
                    foreach ($author_ojs_ids as $author_ojs_id) {
                        $author_cdli_id = $this->Authors->find('all')->where(['author' => $author_ojs_id->author_settings[5]->setting_value . ', ' . $author_ojs_id->author_settings[6]->setting_value])->first();
                        $article_autor = $this->ArticlesAuthors->newEmptyEntity();
                        $article_autor->article_id = $article->id;
                        $article_autor->author_id = $author_cdli_id->id;
                        $article_autor->sequence = $sequence++;
                        $this->ArticlesAuthors->save($article_autor);
                    }
                }
            }
        }
    }
}
