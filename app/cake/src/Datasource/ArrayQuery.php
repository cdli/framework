<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Datasource;

use Cake\Datasource\QueryInterface;
use Cake\Datasource\RepositoryInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Table;
use Exception;

class ArrayQuery implements QueryInterface
{
    protected $_array = [];
    protected $_options = [];
    protected $_repository = null;

    public function __construct(array $array)
    {
        $this->_array = $array;
        $this->_repository = new Table([
            'alias' => 'string',
        ]);
    }

    public function all(): ResultSetInterface
    {
        return new ArrayResultSet($this->toArray());
    }

    public function first()
    {
        return $this->toArray()[0];
    }

    public function count(): int
    {
        return count($this->_array);
    }

    public function toArray(): array
    {
        $offset = 0;
        if (array_key_exists('offset', $this->_options)) {
            $offset = $this->_options['offset'];
        }

        $limit = null;
        if (array_key_exists('limit', $this->_options)) {
            $limit = $this->_options['limit'];
        }

        if (array_key_exists('page', $this->_options)) {
            if (empty($limit)) {
                $limit = 25;
            }

            $offset += $limit * ($this->_options['page'] - 1);
        }

        return array_slice($this->_array, $offset, $limit);
    }

    public function applyOptions(array $options)
    {
        $this->_options = $this->_options + $options;

        return $this;
    }

    public function limit($num)
    {
        $this->_options['limit'] = $num;

        return $this;
    }

    public function offset($num)
    {
        $this->_options['offset'] = $num;

        return $this;
    }

    public function page(int $num, ?int $limit = null)
    {
        $this->_options['page'] = $num;

        if (!array_key_exists('limit', $this->_options) || is_null($this->_options['limit'])) {
            $this->limit(is_null($limit) ? 25 : $limit);
        }

        return $this;
    }

    // No-ops

    public function select($fields, bool $overwrite = false)
    {
        throw new Exception('Not implemented');
    }

    public function aliasField(string $field, ?string $alias = null): array
    {
        return [];
    }

    public function aliasFields(array $fields, ?string $defaultAlias = null): array
    {
        return [];
    }

    public function find(string $finder, array $options = [])
    {
        throw new Exception('Not implemented');
    }

    public function where($conditions = null, array $types = [], bool $overwrite = false)
    {
        throw new Exception('Not implemented');
    }

    public function order($fields, $overwrite = false)
    {
        throw new Exception('Not implemented');
    }

    public function repository(RepositoryInterface $repository)
    {
        $this->_repository = $repository;

        return $this;
    }

    public function getRepository(): ?RepositoryInterface
    {
        return $this->_repository;
    }
}
