<?php
declare(strict_types=1);

namespace App\Datasource;

use Cake\Datasource\Paginator;
use Cake\Datasource\QueryInterface;
use Cake\Datasource\RepositoryInterface;

class NaturalSortPaginator extends Paginator
{
    protected $fields = [
        'Publications.designation',
        'Artifacts.designation',
        'Seals.designation',
        'Impressions.designation',
        'Composites.designation',
        'Witnesses.designation',
        'EntitiesPublications.exact_reference',
    ];

    protected function getQuery(RepositoryInterface $object, ?QueryInterface $query = null, array $data): QueryInterface
    {
        foreach ($this->fields as $field) {
            if (isset($data['options']['order'][$field])) {
                $query->order([
                    'NATURAL_SORT_KEY(' . $field . ')' => $data['options']['order'][$field],
                ]);
                unset($data['options']['order'][$field]);
            }
        }

        return parent::getQuery($object, $query, $data);
    }
}
