<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Datasource;

use Cake\Core\InstanceConfigTrait;
use Cake\Datasource\Paginator;
use Cake\Datasource\QueryInterface;
use Cake\Datasource\RepositoryInterface;

class DiffPaginator extends Paginator
{
    use InstanceConfigTrait;

    protected function getQuery(RepositoryInterface $object, ?QueryInterface $query = null, array $data): QueryInterface
    {
        $data['options']['offset'] = ($data['options']['page'] - 1) * $data['options']['limit'];
        $data['options']['limit'] += 1;
        unset($data['options']['page']);

        return parent::getQuery($object, $query, $data);
    }

    protected function buildParams(array $data): array
    {
        $paging = parent::buildParams($data);
        if ($paging['current'] == $paging['perPage'] + 1) {
            $paging['current'] -= 1;
        }

        return $paging;
    }
}
