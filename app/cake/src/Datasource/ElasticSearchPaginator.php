<?php
declare(strict_types=1);

namespace App\Datasource;

use Cake\Datasource\Paginator;
use Cake\Datasource\QueryInterface;
use Cake\Datasource\RepositoryInterface;
use Cake\Datasource\ResultSetInterface;

/**
 * Paginator to use the `search_after` parameter in ElasticSearch.
 */
class ElasticSearchPaginator extends Paginator
{
    public function paginate(object $object, array $params = [], array $settings = []): ResultSetInterface
    {
        $results = parent::paginate($object, $params, $settings);

        if ($object instanceof QueryInterface) {
            $query = $object;
            $object = $query->getRepository();
        }

        $alias = $object->getAlias();
        $last = $results->last();

        if (!is_null($last)) {
            $this->_pagingParams[$alias]['nextSearchAfter'] = $last['sort'];
        }

        return $results;
    }

    protected function getCount(QueryInterface $query, array $data): ?int
    {
        if (method_exists($query, 'countAll')) {
            return $query->countAll();
        }

        return parent::getCount($query, $data);
    }

    protected function extractData(RepositoryInterface $object, array $params, array $settings): array
    {
        $data = parent::extractData($object, $params, $settings);

        if (array_key_exists('search_after', $params)) {
            $searchAfter = json_decode(base64_decode($params['search_after']), true);
            if (is_null($searchAfter)) {
                throw new \ErrorException('"search_after" is invalid');
            }
            $data['options']['searchAfter'] = $searchAfter;
        }

        return $data;
    }

    protected function getQuery(RepositoryInterface $object, ?QueryInterface $query = null, array $data): QueryInterface
    {
        $query = parent::getQuery($object, $query, $data);

        if (array_key_exists('searchAfter', $data['options']) && method_exists($query, 'searchAfter')) {
            $query = $query->searchAfter($data['options']['searchAfter']);
            $query = $query->offset(0);
        }

        return $query;
    }
}
