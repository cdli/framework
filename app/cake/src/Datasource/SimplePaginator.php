<?php
declare(strict_types=1);

namespace App\Datasource;

use Cake\Datasource\QueryInterface;
use Cake\Datasource\RepositoryInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\Datasource\SimplePaginator as CoreSimplePaginator;

/**
 * Enhanced SimplePaginator.
 *
 * This class properly indicates whether "next page" is available.
 * Core's SimplePaginator has "next page" as "true" even when already on the last page.
 * It works by fetching "limit + 1" records and if records fetched > limit it means
 * next page is available.
 */
class SimplePaginator extends CoreSimplePaginator
{
    public function paginate(object $object, array $params = [], array $settings = []): ResultSetInterface
    {
        $results = parent::paginate($object, $params, $settings);
        $pagingParams = current($this->_pagingParams);

        return new ArrayResultSet(array_slice($results->toArray(), 0, $pagingParams['perPage']));
    }

    protected function getQuery(RepositoryInterface $object, ?QueryInterface $query = null, array $data): QueryInterface
    {
        $query = parent::getQuery($object, $query, $data);

        return $query->limit($data['options']['limit'] + 1);
    }

    protected function getCount(QueryInterface $query, array $data): ?int
    {
        if ((int)$data['options']['page'] === 1) {
            return $query->count();
        } else {
            return null;
        }
    }

    protected function buildParams(array $data): array
    {
        $limit = $data['options']['limit'];

        $paging = [
            'count' => $data['count'],
            'current' => $data['numResults'],
            'perPage' => $limit,
            'page' => $data['options']['page'],
            'requestedPage' => $data['options']['page'],
        ];

        $hasNext = $paging['current'] > $paging['perPage'];
        if ($paging['current']) {
            $paging['current'] = $paging['current'] - 1;
        }

        $paging = $this->addPageCountParams($paging, $data);
        $paging = $this->addStartEndParams($paging, $data);
        $paging = $this->addPrevNextParams($paging, $data);
        $paging = $this->addSortingParams($paging, $data);

        $paging['nextPage'] = $hasNext;

        $paging += [
            'limit' => $data['defaults']['limit'] != $limit ? $limit : null,
            'scope' => $data['options']['scope'],
            'finder' => $data['finder'],
        ];

        return $paging;
    }
}
