<?php // phpcs:ignore SlevomatCodingStandard.TypeHints.DeclareStrictTypes.DeclareStrictTypesMissing

namespace App\Datasource;

use Cake\Core\Configure;
use Cake\Datasource\QueryInterface;
use Cake\Datasource\RepositoryInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\Http\Client;
use Cake\ORM\Table;
use ErrorException;
use Exception;

class ElasticSearchQuery implements QueryInterface
{
    public array $atfQueries = [];
    public array $atfOptions = [];

    protected $_repository = null;
    protected $_options = [];
    protected $_access = [];
    protected $_select = [];

    protected array $_query = [];
    protected array $_aggregations = [];
    protected array $_order = ['_score' => ['order' => 'desc']];

    public function __construct(string $index, $access = [])
    {
        $this->_repository = new Table(['alias' => $index]);
        $this->_query = ['match_all' => (object)[]];
        $this->_access = $access;
    }

    public function find(string $finder, array $options = [])
    {
        if ($finder === 'query') {
            $this->_query = [
                'query_string' => [
                    'query' => $options['query'],
                    'default_operator' => 'and',
                ],
            ];
        }

        $data = array_filter($options['query'], function ($field) use ($finder) {
            return self::$searchFields[$field] === $finder;
        }, ARRAY_FILTER_USE_KEY);

        if ($finder === 'simple') {
            $this->_query = $this->_parseSimpleQuery($data);
        } elseif ($finder === 'advanced') {
            $this->_query = $this->_parseAdvancedQuery($data);
        }

        return $this;
    }

    public function where($conditions = null, array $types = [], bool $overwrite = false)
    {
        if (is_null($conditions) || (is_array($conditions) && count($conditions) === 0)) {
            return $this;
        }

        if (!is_array($conditions) || count($types) > 0) {
            throw new ErrorException('Only array $conditions are supported; $types is not supported');
        }

        if (array_keys($this->_query)[0] !== 'bool') {
            $this->_query = ['bool' => ['must' => $this->_query]];
        }

        foreach ($conditions as $filter => $values) {
            $buckets = self::$filters[$filter]['buckets'];
            $field = self::transformFieldName($filter);

            $alternatives = [];
            foreach ($values as $value) {
                if ($value === self::$missingValue) {
                    $alternatives[] = ['bool' => ['must_not' => ['exists' => ['field' => $field]]]];
                } elseif ($buckets === 'terms') {
                    $alternatives[] = ['term' => ["$field.keyword" => $value]];
                } elseif (array_key_exists($value, $buckets['filters'])) {
                    $alternatives[] = $buckets['filters'][$value];
                } elseif ($value === $buckets['other_bucket_key']) {
                    $alternatives[] = [
                        'bool' => [
                            'must_not' => array_values($buckets['filters']),
                        ],
                    ];
                }
            }

            $query = $this->_makeBoolQuery($alternatives, 'should');
            $query = $this->_transformFieldQuery($field, $query);

            if (!array_key_exists('filter', $this->_query['bool']) || array_key_exists(0, $this->_query['bool']['filter']) || count($this->_query['bool']['filter']) === 0) {
                $this->_query['bool']['filter'][] = $query;
            } else {
                $this->_query['bool']['filter'] = [$this->_query['bool']['filter'], $query];
            }
        }

        return $this;
    }

    public function order($fields, $overwrite = false)
    {
        if ($overwrite) {
            $this->_order = [];
        }

        if (is_string($fields)) {
            $fields = [$fields];
        }

        foreach ($fields as $field => $order) {
            if (is_numeric($field)) {
                $field = $order;
                $order = self::$sortFields[$field]['order'];
                if (in_array($field, self::$_termFields)) {
                    $field = "$field.keyword";
                }
            }

            $this->_order[] = [$field => $order];
        }

        return $this;
    }

    public function aggregate(array $filters)
    {
        foreach ($filters as $filter => $settings) {
            $field = self::transformFieldName($filter);
            if ($settings['buckets'] === 'terms') {
                $this->_aggregations[$filter] = [
                    'terms' => [
                        'field' => $field . '.keyword',
                        'order' => ['_key' => 'asc'],
                        'exclude' => [''],
                        'missing' => self::$missingValue,
                        'size' => 2000,
                    ],
                ];

                // Handle special sorting: if 'order' key is set, the minimum value
                // of that field in artifacts in the bucket is taken and used for
                // sorting.
                if (array_key_exists('order', $settings)) {
                    $this->_aggregations[$filter]['terms']['order'] = [
                        'bucket_sort' => 'asc',
                    ];
                    $this->_aggregations[$filter]['aggs'] = [
                        'bucket_sort' => [
                            'min' => [
                                'field' => $settings['order'],
                            ],
                        ],
                    ];
                }

                // Handle terms aggregations of nested fields
                if (self::isNestedField($field)) {
                    $this->_aggregations[$filter] = [
                        'nested' => ['path' => self::getFieldPath($field)],
                        'aggs' => [
                            'nested' => $this->_aggregations[$filter] + [
                                'aggs' => ['root' => ['reverse_nested' => (object)[]]],
                            ],
                        ],
                    ];
                }
            } else {
                $this->_aggregations[$filter] = [
                    'filters' => $settings['buckets'],
                ];
            }
        }

        return $this;
    }

    public function all(): ResultSetInterface
    {
        // Prepare pagination
        $limit = array_key_exists('limit', $this->_options) ? $this->_options['limit'] : 25;
        $offset = array_key_exists('offset', $this->_options) ? $this->_options['offset'] : 0;

        // Prepare ElasticSearch input
        $query = [
            'from' => $offset,
            'size' => $limit,
            'aggs' => $this->_aggregations,
            'sort' => $this->_order,
            'query' => $this->_transformQuery($this->_query),
        ];

        if (array_key_exists('search_after', $this->_options)) {
            $query['search_after'] = $this->_options['search_after'];
        }

        if (count($this->_select)) {
            $query['_source'] = $this->_select;
        }

        $query = json_encode($query, JSON_THROW_ON_ERROR);

        // Interface with ElasticSearch
        $index = $this->getRepository()->getAlias();
        $request = new Client();
        $request = $request->post(Configure::read('ServiceUrls.elasticsearch') . "/$index/_search", $query, ['type' => 'json']);
        $request = $request->getJson();

        // Process results
        if (array_key_exists('error', $request)) {
            throw new ErrorException(self::_formatError($request['error']));
        }

        return new ElasticSearchResultSet($request);
    }

    protected function _parseSimpleQuery(array $query): array
    {
        if (!array_key_exists('simple-value', $query)) {
            return ['match_all' => (object)[]];
        }

        $alternatives = [['bool' => ['must' => []]]];

        foreach ($query['simple-value'] as $index => $value) {
            if ($index > 0 && isset($query['simple-op'][$index - 1]) && $query['simple-op'][$index - 1] === 'OR') {
                $alternatives[] = ['bool' => ['must' => []]];
            }
            $field = $query['simple-field'][$index] ?? 'keyword';
            $partialQuery = $this->_parseSimpleQueryField($field, $value);
            if (!is_null($partialQuery) && $value !== '') {
                $alternatives[count($alternatives) - 1]['bool']['must'][] = $partialQuery;
            }
        }

        // Simplify
        foreach ($alternatives as $index => $alternative) {
            if (count($alternative['bool']['must']) === 1) {
                $alternatives[$index] = $alternative['bool']['must'][0];
            } elseif (count($alternative['bool']['must']) === 0) {
                unset($alternatives[$index]);
            }
        }

        if (count($alternatives) === 0) {
            return ['match_all' => (object)[]];
        }

        return $this->_makeBoolQuery($alternatives, 'should');
    }

    protected function _parseSimpleQueryField(string $field, string $value)
    {
        if (array_key_exists($field, self::$simpleFields)) {
            $alternatives = array_map(function ($field) use ($value) {
                $field = self::transformFieldName($field);
                $fieldQuery = $this->_getFieldQuery($field, $value);

                return $this->_transformFieldQuery($field, $fieldQuery);
            }, self::$simpleFields[$field]);

            return $this->_makeBoolQuery($alternatives, 'should');
        }

        return null;
    }

    protected function _parseAdvancedQuery(array $query): array
    {
        foreach ($query as $field => $value) {
            if (str_starts_with($field, 'atf_transliteration_')) {
                $this->atfOptions[substr($field, strlen('atf_transliteration_'))] = $value;
                unset($query[$field]);
            }
        }

        $requirements = [];
        foreach ($query as $field => $value) {
            $field = self::transformFieldName($field);

            // Special case for the internal-only cdli_id field
            if ($field === 'cdli_id') {
                $value = str_replace(',', '%OR%', $value);
            }

            $ors = [];
            foreach (preg_split('/\s*%OR%\s*/', $value) as $or) {
                $ands = [];
                foreach (preg_split('/\s*%AND%\s*/', $or) as $and) {
                    $ands[] = $this->_getFieldQuery($field, $and);
                }
                $ors[] = $this->_makeBoolQuery($ands, 'must');
            }
            $fieldQuery = $this->_makeBoolQuery($ors, 'should');

            // Manually add nesting for single-line mode
            if ($field === 'atf_transliteration' && isset($this->atfOptions['mode']) && $this->atfOptions['mode'] === 'line') {
                $fieldQuery = $this->_transformFieldQueryNesting('atf_transliteration_lines.text', $fieldQuery);
            }

            $requirements[] = $this->_transformFieldQuery($field, $fieldQuery);
        }

        return $this->_makeBoolQuery($requirements, 'must');
    }

    /**
     * Unquoted searches: specific rules for searching identifiers, transliteration
     *
     * @param string $field
     * @param string $value
     * @return array
     */
    protected function _getFieldQuery(string $field, string $value)
    {
        // If the field is one of the fields for which ascii-folding is enabled,
        // and the query does not use diacritics (or any non-ASCII characters),
        // match diacritics to their ascii-folded equivalents.
        if (in_array($field, self::$_asciiFields) && !preg_match('/[^\\x20-\\x7F]/u', $value)) {
            $field .= '_ascii';
        }

        if ($value === '') {
            $query = ['match_all' => (object)[]];
        } elseif (preg_match('/^".+"$/', $value)) {
            // Literal searches
            $value = substr($value, 1, -1);
            $query = $this->_getFieldQueryLiteral($field, $value);
        } elseif (preg_match('/^\/.+\/$/', $value) && in_array($field, self::$_termFields)) {
            // Regex searches
            $value = preg_replace('/^\/|\/$/', '.*', $value);
            $query = ['regexp' => ["$field.keyword" => ['value' => $value, 'case_insensitive' => true]]];
        } elseif (!preg_match('/(^(\\[\\\*\?]|[^\\?*])+$)/', $value) && in_array($field, self::$_termFields)) {
            // Wildcard searches
            $query = ['wildcard' => ["$field.keyword" => ['value' => $value, 'case_insensitive' => true]]];
        } elseif (in_array($field, self::$_paddedFields)) {
            // Museum numbers:
            //   - query "BM 1" should match "BM 00001"
            $value = preg_quote($value);
            $value = preg_replace('/\b([A-Za-z]+)\s*0*(\d+)\b/', '${1} *0*$2', $value);
            $value = ".*$value.*";
            $query = ['regexp' => ["$field.keyword" => ['value' => $value, 'flags' => 'NONE', 'case_insensitive' => true]]];
        } elseif ($field === 'id') {
            // CDLI IDs (P-numbers):
            //   - query "1" should match "P000001" but not "P111111"
            // Note that search index contains "1" so prefix needs to be removed.
            $value = preg_replace('/^P0*/', '', $value);
            if (preg_match('/^\d+$/', $value)) {
                $query = ['term' => [$field => ['value' => $value]]];
            } else {
                // 'id' field is numeric and does not support non-numeric queries
                $query = ['match_none' => (object)[]];
            }
        } elseif ($field === 'composite_no' || $field === 'witness_composite_no' || $field === 'all_composite_no') {
            // CDLI composite IDs (Q-numbers):
            //   - query "1" should match "Q000001"
            $value = preg_replace('/^Q0*/', '', $value);
            $value = 'Q' . str_pad($value, 6, '0', STR_PAD_LEFT);
            $query = ['term' => ["$field.keyword" => ['value' => $value]]];
        } elseif ($field === 'seal_no' || $field === 'impression_seal_no' || $field === 'all_seal_no') {
            // CDLI seal IDs (S-numbers):
            //   - query "1" should match "S000001"
            //   - query "1" should match "S000001.1"
            //   - query "1.2" should not match "S000001.1"
            $value = preg_replace('/^S0*/', '', $value);
            $value = explode('.', $value);
            $value[0] = 'S' . str_pad($value[0], 6, '0', STR_PAD_LEFT);
            $value = count($value) > 1 ? implode('.', $value) : $value[0] . '*';
            $query = ['wildcard' => ["$field.keyword" => ['value' => $value]]];
        } elseif ($field === 'collection') {
            // Collection names should be split on spaces
            $value = preg_quote($value, '/');
            $value = preg_replace('/ +/', ' (.+ )?', $value);
            $query = ['regexp' => ["$field.keyword" => ['value' => ".*$value.*", 'flags' => 'NONE', 'case_insensitive' => true]]];
        } elseif ($field === 'created' || $field === 'modified') {
            // Ranges
            $query = $this->_getFieldQueryRange($field, $value);
        } elseif ($field === 'atf_transliteration') {
            $query = $this->_getFieldQueryTransliteration($value);
        } else {
            // Otherwise: treat as literal search
            $query = $this->_getFieldQueryLiteral($field, $value);
        }

        return $query;
    }

    /**
     * Range searches
     *
     * @param string $field
     * @param string $value
     * @return array
     */
    protected function _getFieldQueryRange(string $field, string $value)
    {
        if (!preg_match_all('/(>=?|<=?)?([^\s<>=]+)/', $value, $matches, PREG_SET_ORDER)) {
            return ['match_none' => (object)[]];
        }

        if ($field === 'created' || $field === 'modified') {
            $pattern = '/^\d{4}-\d{2}-\d{2}(T\d{2}:\d{2}:\d{2}(\.\d+)?Z?)?$/';
        } else {
            $pattern = '/^\d+(\.\d+)?$/';
        }

        $parts = [];
        foreach ($matches as $match) {
            if (!preg_match($pattern, $match[2])) {
                continue;
            }

            switch ($match[1]) {
                case '>=':
                    $parts['gte'] = $match[2];
                    break;
                case '>':
                    $parts['gt'] = $match[2];
                    break;
                case '<=':
                    $parts['lte'] = $match[2];
                    break;
                case '<':
                    $parts['lt'] = $match[2];
                    break;
                default:
                    return ['term' => [$field => $match[2]]];
            }
        }

        return ['range' => [$field => (object)$parts]];
    }

    /**
     * Literal searches
     *
     * @param string $field
     * @param string $value
     * @return array
     * @psalm-suppress InvalidNullableReturnType
     */
    protected function _getFieldQueryLiteral(string $field, string $value)
    {
        // For highlighting
        if (str_starts_with($field, 'atf_')) {
            $this->atfQueries[] = [$field, '/' . preg_quote($value, '/') . '/i'];
        }

        if ($value === '') {
            return ['match_all' => (object)[]];
        } elseif (in_array($field, self::$_termFields)) {
            // Term search
            // Escape wildcards if necessary
            if (preg_match('/[*?]/', $value)) {
                $value = preg_quote($value, '/');

                return ['regexp' => ["$field.keyword" => ['value' => ".*$value.*", 'flags' => 'NONE', 'case_insensitive' => true]]];
            } else {
                return ['wildcard' => ["$field.keyword" => ['value' => "*$value*", 'case_insensitive' => true]]];
            }
        } elseif ($field === 'id') {
            if (preg_match('/^\d+$/', $value)) {
                return ['term' => [$field => ['value' => $value]]];
            } else {
                // 'id' field is numeric and does not support non-numeric queries
                return ['match_none' => (object)[]];
            }
        } elseif ($value !== '') {
            // Otherwise: regular (multi-word) search
            return ['match_phrase' => [$field => ['query' => $value]]];
        }
    }

    protected function _transformFieldQuery(string $field, array $query)
    {
        if (array_key_exists($field, self::$_fieldAccessCategories)) {
            $accessCategory = self::$_fieldAccessCategories[$field];
            if (!$this->_access[$accessCategory]) {
                if ($accessCategory === 'atf') {
                    if (self::isNestedField($field)) {
                        $query = $this->_transformFieldQueryNesting($field, $query);
                    }

                    return [
                        'bool' => [
                            'must' => [$query],
                            'filter' => ['term' => ['is_atf_public' => true]],
                        ],
                    ];
                } elseif ($accessCategory === 'asset') {
                    $query = [
                        'bool' => [
                            'must' => [$query],
                            'filter' => ['term' => ['asset.is_public' => true]],
                        ],
                    ];

                    return $this->_transformFieldQueryNesting('asset.is_public', $query);
                }
            }
        }

        if (self::isNestedField($field)) {
            $query = $this->_transformFieldQueryNesting($field, $query);
        }

        return $query;
    }

    protected function _transformFieldQueryNesting(string $field, array $query)
    {
        return [
            'nested' => [
                'path' => self::getFieldPath($field),
                'query' => $query,
            ],
        ];
    }

    protected function _getFieldQueryTransliteration(string $value)
    {
        $options = $this->atfOptions + [
            'mode' => 'full',
            'case_sensitive' => false,
            'sign_permutation' => false,
        ];

        $caseInsensitive = $options['sign_permutation'] || !$options['case_sensitive'];

        if ($options['sign_permutation']) {
            $signs = (new Client())->post(
                Configure::read('ServiceUrls.jtf') . '/jtf-lib/api/getSignnamesATFLINE',
                json_encode(['atf' => $value]),
                ['type' => 'json']
            )->getStringBody();
            // Fix jtf-lib uppercasing lowercase parts of sign name
            $signs = preg_replace_callback('/@[A-Z]/', function ($matches) {
                return strtolower($matches[0]);
            }, $signs);
        }

        if (isset($signs) && $signs !== '') {
            preg_match_all('/([^ ]+)| /u', $signs, $tokens);
        } else {
            preg_match_all('/(([st],|\p{L})+(\d+|ₓ)?)|\P{L}/u', $value, $tokens);
        }

        $signReadings = [];
        if ($options['sign_permutation']) {
            $signReadings = (new Client())->post(
                Configure::read('ServiceUrls.signReadings') . '/permutations',
                json_encode(array_values(array_filter($tokens[1]))),
                ['type' => 'json']
            )->getJson();
        }

        $tokenPatterns = array_map(function ($token) use ($options, $signReadings) {
            // Normally, '-' and ':' should match either. In sign permutation spaces
            // (word delimiters) are also used interchangeably.
            $delimiters = $options['sign_permutation'] ? '[-: }]' : '[-:]';
            if (preg_match("/^$delimiters$/", $token)) {
                return $delimiters;
            }

            $alternatives = [$token];
            if ($options['sign_permutation'] && array_key_exists($token, $signReadings)) {
                $alternatives = $signReadings[$token];
            }

            foreach ($alternatives as $index => $token) {
                $token = strtr($token, [
                    'Š' => 'SZ',
                    'š' => 'sz',
                    'SH' => 'SZ',
                    'sh' => 'sz',
                    'C' => 'SZ',
                    'c' => 'sz',
                    'Ṣ' => 'S,',
                    'ṣ' => 's,',
                    'Ṭ' => 'T,',
                    'ṭ' => 't,',
                    'Ḫ' => 'H',
                    'ḫ' => 'h',
                    'J' => 'G',
                    'j' => 'g',
                    'ₓ' => 'x',
                    '×' => 'x',
                ]);
                $token = preg_quote($token, '/');
                $token = str_replace('ₓ', '\d+', $token);
                $alternatives[$index] = $token;
            }

            return count($alternatives) > 1 ? '(' . implode('|', $alternatives) . ')' : $alternatives[0];
        }, $tokens[0]);

        $signBoundary = '[^A-Za-z0-9\n\',.]';
        $searchPattern = $highlightPattern = implode("$signBoundary*", $tokenPatterns);

        // If the first token is a word, it must be at the beginning or prepended by a symbol
        if (!preg_match('/^\P{L}$/u', $tokens[0][0])) {
            $highlightPattern = "(?<=^|$signBoundary)$highlightPattern";
            $searchPattern = "(.*$signBoundary)?$searchPattern";
        } else {
            $searchPattern = ".*$searchPattern";
        }

        // If the last token is a word, it must be at the end or followed by a symbol
        if (!preg_match('/^\P{L}$/u', $tokens[0][count($tokens[0]) - 1])) {
            $highlightPattern = "$highlightPattern(?=$|$signBoundary)";
            $searchPattern = "$searchPattern($signBoundary.*)?";
        } else {
            $searchPattern = "$searchPattern.*";
        }

        // Include highlight pattern
        $this->atfQueries[] = ['atf_transliteration', "/$highlightPattern/" . ($caseInsensitive ? 'i' : '')];

        // Create search query
        $field = $options['mode'] === 'line' ? 'atf_transliteration_lines.text' : 'atf_transliteration';

        return [
            'regexp' => [
                "$field.keyword" => [
                    'value' => $searchPattern,
                    'flags' => 'NONE',
                    'case_insensitive' => $caseInsensitive,
                ],
            ],
        ];
    }

    protected function _transformQuery(array $query)
    {
        if (!$this->_access['artifact']) {
            if (array_keys($query)[0] !== 'bool') {
                $query = ['bool' => ['must' => $query]];
            }

            if (!array_key_exists('filter', $query['bool']) || array_key_exists(0, $query['bool']['filter'])) {
                $query['bool']['filter'][] = ['term' => ['is_public' => true]];
            } else {
                $query['bool']['filter'] = [
                    $query['bool']['filter'],
                    ['term' => ['is_public' => true]],
                ];
            }
        }

        return $query;
    }

    protected function _makeBoolQuery(array $parts, string $operator)
    {
        if (count($parts) === 1) {
            return $parts[0];
        } elseif ($operator === 'should') {
            return ['bool' => ['should' => $parts, 'minimum_should_match' => 1]];
        } else {
            return ['bool' => ['must' => $parts]];
        }
    }

    public function searchAfter(mixed $offset)
    {
        $this->_options['search_after'] = $offset;

        return $this;
    }

    public function countAll(): int
    {
        $query = ['query' => $this->_transformQuery($this->_query)];

        // Interface with ElasticSearch
        $index = $this->getRepository()->getAlias();
        $request = new Client();
        $request = $request->post(Configure::read('ServiceUrls.elasticsearch') . "/$index/_count", json_encode($query), ['type' => 'json']);
        $request = $request->getJson();

        // Process results
        if (array_key_exists('error', $request)) {
            throw new ErrorException(self::_formatError($request['error']));
        }

        return $request['count'];
    }

    // Mandatory functions

    public function first()
    {
        return $this->all()->first();
    }

    public function count(): int
    {
        return min($this->countAll(), 10000);
    }

    public function toArray(): array
    {
        return $this->all()->toArray();
    }

    public function applyOptions(array $options)
    {
        $this->_options = $this->_options + $options;

        if (array_key_exists('page', $options)) {
            $this->page($options['page']);
        }

        return $this;
    }

    public function limit($num)
    {
        $this->_options['limit'] = $num;

        return $this;
    }

    public function offset($num)
    {
        $this->_options['offset'] = $num;

        return $this;
    }

    public function page(int $num, ?int $limit = null)
    {
        $this->_options['page'] = $num;

        if (is_null($limit)) {
            $limit = array_key_exists('limit', $this->_options) ? $this->_options['limit'] : 25;
        } else {
            $this->limit($limit);
        }

        $this->offset($limit * ($num - 1));

        return $this;
    }

    public function select($fields, bool $overwrite = false)
    {
        if ($overwrite) {
            $this->_select = [];
        }

        if (is_string($fields)) {
            $fields = [$fields];
        } elseif (is_callable($fields)) {
            $fields = $fields($this);
        } elseif (!is_array($fields)) {
            // Table, Association (not particularly relevant here)
            throw new Exception('Not implemented');
        }

        $this->_select = array_merge($this->_select, $fields);

        return $this;
    }

    public function aliasField(string $field, ?string $alias = null): array
    {
        return [];
    }

    public function aliasFields(array $fields, ?string $defaultAlias = null): array
    {
        return [];
    }

    public function repository(RepositoryInterface $repository)
    {
        $this->_repository = $repository;

        return $this;
    }

    public function getRepository(): ?RepositoryInterface
    {
        return $this->_repository;
    }

    // Protected functions

    protected static function _formatError(array $error)
    {
        $lines = ['[' . $error['type'] . '] ' . $error['reason']];
        if (array_key_exists('root_cause', $error)) {
            $lines[] = "\xc2\xa0\xc2\xa0Cause:";
            foreach ($error['root_cause'] as $error) {
                foreach (explode("\n", self::_formatError($error)) as $line) {
                    $lines[] = "\xc2\xa0\xc2\xa0" . $line;
                }
            }
        }

        return implode("\n", $lines);
    }

    public static function transformFieldName(string $field)
    {
        return preg_replace('/^(publication|asset|update|atf_translation|atf_transliteration_lines|external_resource)_/', '$1.', $field);
    }

    public static function isNestedField(string $field)
    {
        return str_contains($field, '.');
    }

    public static function getFieldPath(string $field)
    {
        $parts = explode('.', $field);
        $parts = array_slice($parts, 0, -1);

        return count($parts) ? implode('.', $parts) : null;
    }

    public static array $simpleFields = [
        'keyword' => [
            'accession_no',
            'alternative_years',
            'annotation',
            'archive',
            'artifact_comments',
            'artifact_preservation',
            'artifact_type',
            'asset.artifact_aspect',
            'asset.file_format',
            'asset.type',
            'asset.annotations',
            'atf_comments',
            'atf_structure',
            'atf_transcription',
            'atf_translation.text',
            'atf_transliteration',
            'collection',
            'composite_no',
            'condition_description',
            'dates_referenced',
            'designation',
            'elevation',
            'excavation_no',
            'external_resource.external_resource',
            'external_resource.url',
            'external_resource.key',
            'findspot_comments',
            'findspot_square',
            'genre',
            'genre_comments',
            'id',
            'impression_seal_no',
            'language',
            'material',
            'material_aspect',
            'material_color',
            'museum_no',
            'period',
            'period_comments',
            'provenience',
            'provenience_comments',
            'publication.address',
            'publication.authors',
            'publication.bibtexkey',
            'publication.book_title',
            'publication.comments',
            'publication.designation',
            'publication.editors',
            'publication.entry_type',
            'publication.exact_reference',
            'publication.journal',
            'publication.pages',
            'publication.publisher',
            'publication.series',
            'publication.title',
            'publication.type',
            'publication.volume',
            'publication.year',
            'region',
            'seal_no',
            'stratigraphic_level',
            'surface_preservation',
            'update.authors',
            'update.comments',
            'update.external_resource',
            'witness_composite_no',
            'written_in',
        ],
        'publication' => [
            'publication.exact_reference',
            'publication.designation',
            'publication.bibtexkey',
            'publication.year',
            'publication.publisher',
            'publication.school',
            'publication.series',
            'publication.title',
            'publication.book_title',
            'publication.chapter',
            'publication.journal',
            'publication.editors',
            'publication.authors',
        ],
        'collection' => [
            'collection',
        ],
        'provenience' => [
            'provenience',
            'written_in',
            'region',
        ],
        'period' => [
            'period',
        ],
        'transliteration' => [
            'atf_transliteration',
        ],
        'translation' => [
            'atf_translation.text',
        ],
        'id' => [
            'publication.bibtexkey',
            'publication.designation',
            'publication.exact_reference',

            'id',
            'composite_no',
            'seal_no',
            'witness_composite_no',
            'impression_seal_no',
            'designation',
            'museum_no',
            'excavation_no',
            'accession_no',
        ],
    ];

    protected static array $_fieldAccessCategories = [
        'atf' => 'atf',
        'atf_transliteration' => 'atf',
        'atf_transliteration_lines' => 'atf',
        'atf_transliteration_sign_names' => 'atf',
        'atf_translation.text' => 'atf',
        'atf_transcription' => 'atf',
        'atf_structure' => 'atf',
        'atf_comments' => 'atf',
        'jtf' => 'atf',
        'annotation' => 'atf',
        'is_atf2conll_diff_resolved' => 'atf',

        'asset.type' => 'asset',
        'asset.artifact_aspect' => 'asset',
        'asset.file_format' => 'asset',
        'asset.annotations' => 'asset',
    ];

    protected static array $_termFields = [
        'accession_no',
        'all_composite_no',
        'all_seal_no',
        'archive',
        'archive_ascii',
        'artifact_preservation',
        'artifact_type',
        'asset.artifact_aspect',
        'asset.file_format',
        'asset.type',
        'asset.annotations',
        'atf_comments',
        'atf_structure',
        'atf_transliteration',
        'cdli_id',
        'collection',
        'composite_no',
        'dates_referenced',
        'dates_referenced_ascii',
        'designation',
        'designation_ascii',
        'excavation_no',
        'external_resource.external_resource',
        'external_resource.url',
        'external_resource.key',
        'genre',
        'impression_seal_no',
        'language',
        'material',
        'material_aspect',
        'material_color',
        'museum_no',
        'period',
        'provenience',
        'provenience_ascii',
        'publication.authors',
        'publication.authors_ascii',
        'publication.designation',
        'publication.designation_ascii',
        'publication.editors',
        'publication.editors_ascii',
        'publication.entry_type',
        'publication.exact_reference',
        'publication.exact_reference_ascii',
        'publication.journal',
        'publication.publisher',
        'publication.series',
        'publication.type',
        'publication.year',
        'region',
        'seal_no',
        'surface_preservation',
        'update.authors',
        'update.authors_ascii',
        'update.external_resource',
        'witness_composite_no',
        'written_in_ascii',
        'written_in',
    ];

    protected static array $_paddedFields = [
        'accession_no',
        'designation',
        'designation_ascii',
        'excavation_no',
        'museum_no',
        'publication.designation',
        'publication.designation_ascii',
    ];

    protected static array $_asciiFields = [
        'archive',
        'designation',
        'dates_referenced',
        'provenience',
        'publication_designation',
        'publication_exact_reference',
        'publication_title',
        'publication_authors',
        'publication_editors',
        'update_event_authors',
        'written_in',
    ];

    public static array $searchFields = [
        // Simple search
        'simple-value' => 'simple',
        'simple-field' => 'simple',
        'simple-op' => 'simple',

        // Advanced search
        'publication_designation' => 'advanced',
        'publication_authors' => 'advanced',
        'publication_editors' => 'advanced',
        'publication_year' => 'advanced',
        'publication_title' => 'advanced',
        'publication_type' => 'advanced',
        'publication_publisher' => 'advanced',
        'publication_series' => 'advanced',
        'publication_exact_reference' => 'advanced',
        'publication_bibtexkey' => 'advanced',
        'artifact_type' => 'advanced',
        'material' => 'advanced',
        'material_aspect' => 'advanced',
        'material_color' => 'advanced',
        'external_resource_external_resource' => 'advanced',
        'external_resource_url' => 'advanced',
        'external_resource_key' => 'advanced',
        'collection' => 'advanced',
        'provenience' => 'advanced',
        'written_in' => 'advanced',
        'archive' => 'advanced',
        'period' => 'advanced',
        'artifact_comments' => 'advanced',
        'dates_referenced' => 'advanced',
        'atf_transliteration' => 'advanced',
        'atf_transliteration_mode' => 'advanced',
        'atf_transliteration_case_sensitive' => 'advanced',
        'atf_transliteration_sign_permutation' => 'advanced',
        'atf_translation_text' => 'advanced',
        'atf_transcription' => 'advanced',
        'atf_comments' => 'advanced',
        'atf_structure' => 'advanced',
        'genre' => 'advanced',
        'genre_comments' => 'advanced',
        'language' => 'advanced',
        'asset_annotations' => 'advanced',
        'designation' => 'advanced',
        'museum_no' => 'advanced',
        'accession_no' => 'advanced',
        'excavation_no' => 'advanced',
        'cdli_id' => 'advanced',
        'id' => 'advanced',
        'seal_no' => 'advanced',
        'impression_seal_no' => 'advanced',
        'all_seal_no' => 'advanced',
        'composite_no' => 'advanced',
        'witness_composite_no' => 'advanced',
        'all_composite_no' => 'advanced',
        'update_authors' => 'advanced',
        'update_external_resource' => 'advanced',
        'created' => 'advanced',
        'modified' => 'advanced',
    ];

    public static string $missingValue = 'no value';
    public static $filters = [
        // Object data
        'period' => [
            'section' => 'artifact',
            'label' => 'Period',
            'buckets' => 'terms',
            'order' => 'period_sequence',
        ],
        'material' => [
            'section' => 'artifact',
            'label' => 'Material',
            'buckets' => 'terms',
        ],
        'artifact_type' => [
            'section' => 'artifact',
            'label' => 'Artifact type',
            'buckets' => 'terms',
        ],
        'provenience' => [
            'section' => 'artifact',
            'label' => 'Provenience',
            'buckets' => 'terms',
        ],
        'collection' => [
            'section' => 'artifact',
            'label' => 'Museum Collections',
            'buckets' => 'terms',
        ],
        'composite_seal' => [
            'section' => 'artifact',
            'label' => 'Composite/seal',
            'buckets' => [
                'filters' => [
                    'seal' => ['exists' => ['field' => 'seal_no']],
                    'seal impression' => ['exists' => ['field' => 'impression_seal_no']],
                    'composite' => ['exists' => ['field' => 'composite_no']],
                    'composite witness' => ['exists' => ['field' => 'witness_composite_no']],
                ],
                'other_bucket_key' => 'other',
            ],
        ],

        // Text-related data
        'language' => [
            'section' => 'textual',
            'label' => 'Language',
            'buckets' => 'terms',
        ],
        'genre' => [
            'section' => 'textual',
            'label' => 'Genre',
            'buckets' => 'terms',
        ],
        // TODO: disabled for now, see https://gitlab.com/cdli/framework/-/merge_requests/747#note_1190462701
        // 'dates_referenced' => [
        //     'section' => 'textual',
        //     'label' => 'Dates',
        //     'buckets' => 'terms'
        // ],

        // Publications
        'publication_authors' => [
            'section' => 'publication',
            'label' => 'Authors',
            'buckets' => 'terms',
        ],
        'publication_year' => [
            'section' => 'publication',
            'label' => 'Date of publication',
            'buckets' => 'terms',
        ],
        'publication_journal' => [
            'section' => 'publication',
            'label' => 'Journal',
            'buckets' => 'terms',
        ],
        'publication_editors' => [
            'section' => 'publication',
            'label' => 'Editors',
            'buckets' => 'terms',
        ],
        'publication_series' => [
            'section' => 'publication',
            'label' => 'Series',
            'buckets' => 'terms',
        ],
        'publication_publisher' => [
            'section' => 'publication',
            'label' => 'Publisher',
            'buckets' => 'terms',
        ],

        // Updates
        'update_external_resource' => [
            'section' => 'Credits',
            'label' => 'Contributing project',
            'buckets' => 'terms',
        ],

        // Data availability
        'asset_type' => [
            'section' => 'data',
            'label' => 'Artifact images',
            'buckets' => 'terms',
        ],
        'chemical_data' => [
            'section' => 'data',
            'label' => 'Chemical data',
            'buckets' => [
                'filters' => [
                    'With' => ['term' => ['chemical_data' => true]],
                ],
                'other_bucket_key' => 'Without',
            ],
        ],
        'external_resource_external_resource' => [
            'section' => 'data',
            'label' => 'External link',
            'buckets' => 'terms',
        ],
        'asset_annotations' => [
            'section' => 'data',
            'label' => 'Image Annotations',
            'buckets' => [
                'filters' => [
                    'With' => [
                        'bool' => [
                            'must' => [
                                'nested' => [
                                    'path' => 'asset',
                                    'query' => ['exists' => ['field' => 'asset.annotations']],
                                ],
                            ],
                        ],
                    ],
                ],
                'other_bucket_key' => 'Without',
            ],
        ],
        'publication' => [
            'section' => 'data',
            'label' => 'Publications',
            'buckets' => [
                'filters' => [
                    'With' => [
                        'bool' => [
                            'must' => [
                                'nested' => [
                                    'path' => 'publication',
                                    'query' => ['match_all' => ['boost' => 1]],
                                ],
                            ],
                        ],
                    ],
                ],
                'other_bucket_key' => 'Without',
            ],
        ],
        'atf_transliteration' => [
            'section' => 'data',
            'label' => 'Transliteration',
            'buckets' => [
                'filters' => [
                    'With' => ['exists' => ['field' => 'atf_transliteration']],
                ],
                'other_bucket_key' => 'Without',
            ],
        ],
        'annotation' => [
            'section' => 'data',
            'label' => 'Text Annotations',
            'buckets' => [
                'filters' => [
                    'With' => ['exists' => ['field' => 'annotation']],
                ],
                'other_bucket_key' => 'Without',
            ],
        ],
        'atf_transcription' => [
            'section' => 'data',
            'label' => 'Transcription',
            'buckets' => [
                'filters' => [
                    'With' => ['exists' => ['field' => 'atf_transcription']],
                ],
                'other_bucket_key' => 'Without',
            ],
        ],
        'atf_translation' => [
            'section' => 'data',
            'label' => 'Translation',
            'buckets' => [
                'filters' => [
                    'With' => [
                        'bool' => [
                            'must' => [
                                'nested' => [
                                    'path' => 'atf_translation',
                                    'query' => ['match_all' => ['boost' => 1]],
                                ],
                            ],
                        ],
                    ],
                ],
                'other_bucket_key' => 'Without',
            ],
        ],
        'atf_translation_language' => [
            'section' => 'data',
            'label' => 'Translation Language',
            'buckets' => 'terms',
        ],
    ];

    public static array $sortFields = [
        '_score' => [
            'full' => true,
            'compact' => false,
            'label' => 'Relevance',
            'order' => 'desc',
        ],
        'id' => [
            'full' => false,
            'compact' => true,
            'label' => 'CDLI number',
            'order' => 'asc',
        ],
        'designation' => [
            'full' => true,
            'compact' => true,
            'label' => 'Primary publication',
            'order' => 'asc',
        ],
        'composite_no' => [
            'full' => true,
            'compact' => true,
            'label' => 'Composite number',
            'order' => 'asc',
        ],
        'seal_no' => [
            'full' => true,
            'compact' => true,
            'label' => 'Seal number',
            'order' => 'asc',
        ],
        'museum_no' => [
            'full' => true,
            'compact' => true,
            'label' => 'Museum number',
            'order' => 'asc',
        ],
        'collection' => [
            'full' => true,
            'compact' => true,
            'label' => 'Collection(s)',
            'order' => 'asc',
        ],
        'provenience' => [
            'full' => true,
            'compact' => true,
            'label' => 'Provenience',
            'order' => 'asc',
        ],
        'period_sequence' => [
            'full' => true,
            'compact' => true,
            'label' => 'Period',
            'order' => 'asc',
        ],
        'artifact_type' => [
            'full' => true,
            'compact' => true,
            'label' => 'Artifact Type',
            'order' => 'asc',
        ],
        'accession_no' => [
            'full' => false,
            'compact' => true,
            'label' => 'Accession number',
            'order' => 'asc',
        ],
        'excavation_no' => [
            'full' => false,
            'compact' => true,
            'label' => 'Excavation number',
            'order' => 'asc',
        ],
        'genre' => [
            'full' => false,
            'compact' => true,
            'label' => 'Genre(s)',
            'order' => 'asc',
        ],
        'archive' => [
            'full' => false,
            'compact' => true,
            'label' => 'Archive',
            'order' => 'asc',
        ],
        'dates_referenced' => [
            'full' => false,
            'compact' => true,
            'label' => 'Dates referenced',
            'order' => 'asc',
        ],
        'written_in' => [
            'full' => false,
            'compact' => true,
            'label' => 'Written in',
            'order' => 'asc',
        ],
        'material' => [
            'full' => false,
            'compact' => true,
            'label' => 'Material(s)',
            'order' => 'asc',
        ],
    ];
}
