<?php

$finder = PhpCsFixer\Finder::create()
    ->exclude('templates')
    ->exclude('tmp')
    ->exclude('vendor')
    ->in(__DIR__ . '/app/cake');

$config = new PhpCsFixer\Config();

return $config
    ->setFinder($finder);
