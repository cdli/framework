# Changelog

## Phoenix beta 2024-11-28

### API

- Correctly return HTTP 404 status

### Tools

- Fix issue with "Loading" message in SPARQL client

### Admin functionality

- Improve site status dashboard

## Phoenix beta 2024-11-27

### Artifacts

- Fix a glitch in the RTI viewer
- Offer token list downloads with token frequencies as TSV

### Crowdsourcing

- Allow artifacts to have multiple IDs in the same external resource

### Bibliography

- Fix display of certain names in references

### API

- Allow pagination beyond 10000 results in search results

## Phoenix beta 2024-11-05

### API

- Update RDF of links between artifacts and materials

### Tools

- Fix issue with long URLs for SPARQL client
- Fix issue with empty values in SPARQL client results table
- List SPARQL client in resources/tools menu
- Fix Commodity Viz tool

## Phoenix beta 2024-11-01

### Tools

- Update the SPARQL client to account for changes in the endpoint

## Phoenix beta 2024-10-31

### API

- Update the RDF output

## Phoenix beta 2024-10-26

### Search

- Fix issue where a search query with no results showed an error instead

## Phoenix beta 2024-10-18

### Tools

- Create a SPARQL client for the CDLI SPARQL endpoint

## Phoenix beta 2024-10-17

### Artifacts

- The reader page was redesigned to match the image annotations viewer
- Revisions in the credits table now link to changes to that artifact
  instead of the whole update
- Authors and publications can now be credited for images

## Phoenix beta 2024-10-14

### Artifacts

- Improve display of image copyright

### Crowdsourcing

- Fix editing of locations and places

### CDLI Developers

- Include example configuration for a SPARQL server

## Phoenix beta 2024-10-09

### Search

- Automatically fix certain common mistakes in search URLs
- Fix export of JTF files from search

### CDLI Developers

- Revise linked data
- Improve performance of various APIs

## Phoenix beta 2024-09-20

### Search

- Pagination automatically resets when changing filters or sort order

### Artifacts

- Add page showing recent changes to the ATF

### Bibliography

- Gracefully handle errors when generating bibliographies
- Allow unbalanced brackets in the BibTeX key
- Include external resources (DOI, ISBN, etc.) in JSON export

### Crowdsourcing

- Fix searching of update type in update events

## Phoenix beta 2024-09-16

### Bibliography

- Fixed import of external resources from CSV files

## Phoenix beta 2024-08-12

### Bibliography

- Added button to delete a publication
- Added early warnings for some issues with a given publication upload

### CDLI Developers

- Fixed access to retired artifacts for admins

## Phoenix beta 2024-07-26

### Artifacts

- Fix issue preventing viewing of composite scores

### Bibliography

- Show bibtex key for new publications on upload error
- Increase maximum length of title field
- Increase maximum length of series field

### CDLI Developers

- Fix error in site status dashboard
- Show more information and filtering options in site status dashboard

## Phoenix beta 2024-06-09

### Search

- Mapped 'j' to 'g' in transliteration search.
- Ensured 'no value' category ends up at the bottom of the list of values in search filters.
- Changed the default ordering of results so they are sorted by P number, ascending.
- Fix issue where the wrong exact reference was displayed alongside the primary publication.
- Added `created` and `modified` datetime fields.
- Removed old, unused code for search settings.

### Bibliography

- Improved the layout of the journal pages to make the list of articles easier to look through.
- Fix the hyperlink of authors in the publications table.

### Image annotations

- Fixed "overwrite" mode of annotations upload.
- Fixed diff table of annotations upload.

### Sponsors

- Fixed the order of sponsors in the index.
- Improved the display of images in the index.
- Improved the layout of the sponsors view page.

### Users

- Update the guidance for people to register to the site.

### API

- Added update events to the JSON API.

### Admin functionality

- Refactored the edit functionality of sponsors.
- Improved the CDLJ/CDLB LaTeX converter to handle certain syntax errors in tables.
- Fixed the display of EXIF data on images
- Improved the site status dashboard.
- Added a page to directly check the data in the search index.

## Phoenix beta 2024-05-23

### Bibliography

- Display references with hyperlinked URLs and DOIs.
- Fix display of exact reference in artifacts view.
- Fixed merging of publications with no designation.

### Crowdsourcing

- Add revision number/hyperlink to diff of other entities.
- Improve diff of other entities.
- Do not let non-admins merge updates with private changes.

### CDLI developers

- Set limit to amount of `Server-Timings` debug data.
