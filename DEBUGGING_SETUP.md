# Enabling step debugging of cakePHP application using XDebug

*Note: The following is tested on Windows 10 Home Edition and with PHPStorm 2021.3.3. For debugging with other tools such as VS Code, google "VS Code XDebug".*

It is possible to use XDebug for step debugging on the cakePHP application. Many development tools (Phpstorm, VS Code, etc.) supports listening to XDebug signals to halt program on breakpoints. This way, the execution and logic of the program can be traced, which is beneficial for getting to know the framework and further development.

Step 1: Enable internet access for the ``app_cake`` container, to allow installation of XDebug through ``apk add``. This is done by modifying ``docker-compose-org.yml``. In the section of ``app_cake``, add ``- nodelocal`` to the network section. That specific part of your ``.yml`` file should look like this:

```dockerfile
app_cake:
    # image: registry.gitlab.com/cdli/framework/app_cake:0.4.0
    build:
      context: ../app/cake
      dockerfile: Dockerfile
    hostname: app_cake
    networks:
      - nodelocal-private
      # The extra line that is added
      - nodelocal 
```
*Note: To be safe it is best to not commit this change of the ``docker-compose.org.yml`` file.*

Step 2: Install XDebug. Enter the console of the ``app_cake`` container from Docker Desktop, and enter the command ``apk add php8-pecl-xdebug``. If a success message appears, it means XDebug has been installed.

Step 3: Modify configuration files for XDebug. By default, XDebug is disabled, so it is necessary to open the ``/etc/php8/conf.d/50_xdebug.ini`` using ``vi``.

You'll need to change the file to the following:

```
;Uncomment and configure mode https://xdebug.org/docs/all_settings#xdebug.mode
zend_extension=xdebug.so
xdebug.mode=debug
xdebug.client_host=host.docker.internal
xdebug.start_with_request=yes
```

This enables XDebug functionalities.

Step 4: In PHPStorm, set a breakpoint in the PHP source file you want to debug. In the top-right corner, click on the button with a telephone and a bug. This enables listening to incoming XDebug connections.

Step 5: Open up the corresponding web page. A window in PHP would show up indicating incoming connection from XDebug. Click on Accept to enable debugging.

Step 6: If the breakpoint is held for too long, you may not see the page load, but will see a ``504 Gateway Timed Out``. This is because the breakpoint is blocking the program for too long. To solve this issue, add a line ``fastcgi_read_timeout 3600;`` to the ``/dev/conf/nginx.conf`` file. This lengthens the timeout limit to an hour, which should be enough for all intents and purposes.

*Note: It is also best to not commit this change to the nginx configuration file.*


